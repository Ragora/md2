package CONN_CT_PATCH
{
	function GameConnection::onConnect(%client, %name, %raceGender, %skin, %voice, %voicePitch)
	{
		schedule(0, 0, "update_player_count");
		parent::onConnect(%client, %name, %raceGender, %skin, %voice, %voicePitch);
	}

	function GameConnection::onDrop(%client, %reason)
	{
		schedule(0, 0, "update_player_count");
		parent::onDrop(%client, %reason);
	}


	function AIConnection::onAIDrop(%client)
	{
		schedule(0, 0, "update_player_count");
		parent::onAIDrop(%client);
	}

	function AIConnection::onAIConnect(%client, %name, %team, %skill, %offense, %voice, %voicePitch)
	{
		schedule(0, 0, "update_player_count");
		parent::onAIConnect(%client, %name, %team, %skill, %offense, %voice, %voicePitch);
	}


	function update_player_count()
	{
		$HostGamePlayerCount = ClientGroup.getCount();
	}
};
if (!isActivePackage(CONN_CT_PATCH))
	activatePackage(CONN_CT_PATCH);