    if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//**************************************************************
// SHRIKE SCOUT FLIER
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************

datablock AudioProfile(ScoutFlyerThrustSound)
{
   filename    = "fx/vehicles/shrike_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(ScoutFlyerEngineSound)
{
   filename    = "fx/vehicles/shrike_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterFire)
{
   filename    = "fx/vehicles/shrike_blaster.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterProjectile)
{
   filename    = "fx/weapons/shrike_blaster_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

//**************************************************************
// LIGHTS
//**************************************************************

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

Meltdown.MassVelMod[ScoutFlyer] = 1.0;
Meltdown.MaxDmgMod[ScoutFlyer] = 1.5;
Meltdown.ShieldPctMod[ScoutFlyer] = 1.25;

datablock FlyingVehicleData(ScoutFlyer) : VehicleDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_air_scout.dts";
   multipassenger = false;
   computeCRC = true;

   vehicleType = $VehicleMask::Shrike;
   hullType = $VehicleHull::Shrike;
   
   debrisShapeName = "vehicle_air_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   mountArmorsAllowed[ 0 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Engineer ;	// +soph
   
   cameraMaxDist = 15;
   cameraOffset = 2.5;
   cameraLag = 0.9;
   explosion = VehicleExplosion;
	explosionDamage = 0.75;
	explosionRadius = 25.0;

   keepReticle = true;
   
   maxDamage = 3.0;		//was 2.0  -soph
   destroyedLevel = 3.0;	//was 2.0  -soph

   isShielded = true;
   energyPerDamagePoint = 180;
   maxEnergy = 250;      // Afterburner and any energy weapon pool
   minDrag = 30;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.825;

   maxAutoSpeed = 10;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 350;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 215;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't` whack out at very slow speeds
   
   // Maneuvering
   maxSteeringAngle = 3;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 6;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 4;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 3600;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1450;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 660;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 225;

   // Turbo Jet
   jetForce = 3500;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 16;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 2.6;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 2.15;

   // Rigid body
   mass = 138*Meltdown.MassVelMod[ScoutFlyer];        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 25;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 55.56;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 32;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.03;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 23.0; //18? you sick bastard! Death~bot
   collDamageMultiplier   = 0.0125; //0.0125; keep it low...

   //
   minTrailSpeed = 30;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = ShrikeEngineEmitter;
   downJetEmitter = TurboJetEmitter;

   //
   jetSound = ScoutFlyerThrustSound;
   engineSound = ScoutFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;
   
   //
   softSplashSoundVelocity = 10.0; 
   mediumSplashSoundVelocity = 15.0;   
   hardSplashSoundVelocity = 20.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound; 
    
   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -3.5 -0.25";
   damageLevelTolerance[0] = 0.5;
   damageLevelTolerance[1] = 0.75;
   numDmgEmitterAreas = 1;

   //
   max[chaingunAmmo] = 1000;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_scout_grey";
   targetNameTag = 'Shrike';
   targetTypeTag = 'Turbograv';
   sensorData = AWACPulseSensor;
   sensorRadius = AWACPulseSensor.detectRadius;
   sensorColor = "255 194 9";

   numWeapons = 2;
   damageModPerMount = 0.0;
   
   checkRadius = 5.5;
   observeParameters = "1 10 10";

   runningLight[0] = ShrikeLight1;
//   runningLight[1] = ShrikeLight2;

   shieldEffectScale = "0.937 1.125 0.60";

   // Ammo counts
   max[DiscAmmo] = 60;		// = 40; -soph
   max[PCRAmmo] = 70;
   max[PlasmaAmmo] = 80;	// = 100; -soph
   max[ChaingunAmmo] = 300;
};

datablock TurretData(ShrikeShieldCap)
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = 1000;
   destroyedLevel          = 1000;

   thetaMin                = 70;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 75;	// maxCapacitorEnergy      = 150;  -soph
   capacitorRechargeRate   = 0.25;	// capacitorRechargeRate   = 0.5625; -soph
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 0;

   targetNameTag           = '';
   targetTypeTag           = '';
};

//**************************************************************
// WEAPONS
//**************************************************************

datablock ShapeBaseImageData(ScoutChaingunPairImage)
{
   className = WeaponImage;
   shapeFile = "turret_aa_large.dts";
   item      = Chaingun;
//   ammo   = ChaingunAmmo;
   projectile = ScoutChaingunBullet;
   projectileType = TracerProjectile;
   mountPoint = 10;
   offset = "1 1 0.7";
   rotation = "0 1 0 32";

   usesEnergy = true;
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 5;
   fireEnergy = 10;
   fireTimeout = 100;

   stateName[0]             = "Activate";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = ShrikeBlasterFire;
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(ScoutChaingunImage) : ScoutChaingunPairImage
{
   offset = "-1 1 0.7";
   rotation = "0 1 0 -32";
   fireTimeout = 100;
   stateScript[3]           = "onTriggerDown";
   stateScript[5]           = "onTriggerUp";
   stateScript[6]           = "onTriggerUp";
};

datablock ShapeBaseImageData(ScoutChaingunParam)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = ScoutChaingunBullet;
   projectileType = TracerProjectile;
}; 

// ?????

datablock ShockwaveData(BomberFusionShockwave)
{
   width = 3;
   numSegments = 24;
   numVertSegments = 6;
   velocity = 15;
   acceleration = 30.0;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 0.8 0.5";
   colors[1] = "0.4 0.4 0.8 0.5";
   colors[2] = "0.4 0.4 0.8 0.5";
};

datablock ExplosionData(BExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = plasmaExpSound;
   shockwave      = BomberFusionShockwave;

   faceViewer     = true;
   explosionScale = "1 1 1";

   particleEmitter = DiscExplosionEmitter;
   particleDensity = 300;
   particleRadius = 2.5;

   shakeCamera = true;
   camShakeFreq = "10.0 11.0 10.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock LinearFlareProjectileData(BomberFusionBolt)
{
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.25;
   damageRadius        = 14;
   radiusDamageType    = $DamageType::BellyTurret;
   kickBackStrength    = 1500;

   sound               = discProjectileSound;

   explosion           = "BExplosion";
   splash              = DiscSplash;

   baseEmitter       = CoolBlueTrailEmitter;
   dryVelocity       = 150.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "3.0 3.0 3.0";
   numFlares         = 30;
   flareColor        = "0.13 0.41 0.93";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function BomberFusionBolt::onExplode( %data , %proj , %pos , %mod ) 	// +[soph]
{									// + from gl logic
	if( %proj.bkSourceObject )					// +
		if( isObject(%proj.bkSourceObject ) )			// +
			%proj.sourceObject = %proj.bkSourceObject ;
	if( %proj.damageMod > 0 )					// +
		EMPBurstExplosion(%proj , %pos , %data.damageRadius * 0.9 , %data.indirectDamage * %proj.damageMod / 2 , 0 , %proj.sourceObject , $DamageType::EMP ) ;
        else								// +
		EMPBurstExplosion(%proj , %pos , %data.damageRadius * 0.9 , %data.indirectDamage / 2 , 0 , %proj.sourceObject , $DamageType::EMP ) ;
	Parent::onExplode( %data , %proj , %pos , %mod ) ;		// +
}									// +[/soph]

datablock AudioProfile(PulseCannonFire)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioClose3d;
   preload = true;
};
