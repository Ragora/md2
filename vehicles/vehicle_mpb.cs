//**************************************************************
// JERICHO FORWARD BASE (Mobile Point Base)
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************

datablock AudioProfile(MPBEngineSound)
{
   filename    = "fx/vehicles/mpb_thrust.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(MPBThrustSound)
{
   filename    = "fx/vehicles/mpb_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(MobileBaseDeploySound)
{
   filename    = "fx/vehicles/MPB_deploy.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(MobileBaseUndeploySound)
{
   filename    = "fx/vehicles/MPB_undeploy_turret.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(MobileBaseTurretDeploySound)
{
   filename    = "fx/vehicles/MPB_deploy_turret.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(MobileBaseTurretUndeploySound)
{
   filename    = "fx/vehicles/MPB_undeploy_turret.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(MobileBaseStationDeploySound)
{
   filename    = "fx/vehicles/MPB_deploy_station.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(MobileBaseStationUndeploySound)
{
   filename    = "fx/vehicles/MPB_close_lid.wav";
   description = AudioClose3d;
   preload = true;
};


//**************************************************************
// LIGHTS
//**************************************************************

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************
datablock SensorData(MPBDeployedSensor) : VehiclePulseSensor
{
   jams = true;
   jamsOnlyGroup = true;
   jamsUsingLOS = false;
   jamRadius = 50;
};

datablock WheeledVehicleData(MobileBaseVehicle) : VehicleDamageProfile
{
   spawnOffset = "0 0 1.0";
   renderWhenDestroyed = false;

   catagory = "Vehicles";
   shapeFile = "vehicle_land_mpbase.dts";
   multipassenger = false;
   computeCRC = true;

   debrisShapeName = "vehicle_land_mpbase_debris.dts";
   debris = ShapeDebris;

   drag = 0.0;
   density = 20.0;

   mountPose[0] = sitting;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   mountArmorsAllowed[ 0 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::MagIon | $ArmorMask::Blastech | $ArmorMask::Engineer ;	// +soph

   cantAbandon = 1;
   cantTeamSwitch = 1;
   
   cameraMaxDist = 20;
   cameraOffset = 6;
   cameraLag = 1.5;
   explosion = LargeGroundVehicleExplosion;
   explosionDamage = 2;
   explosionRadius = 15;

   maxSteeringAngle = 0.3;  // 20 deg.

   // Used to test if the station can deploy
   stationPoints[1] = "-2.3 -7.38703 -0.65";
   stationPoints[2] = "-2.3 -11.8 -0.65";
   stationPoints[3] = "0 -7.38703 -0.65";
   stationPoints[4] = "0 -11.8 -0.65";
   stationPoints[5] = "2.3 -7.38703 -0.65";
   stationPoints[6] = "2.3 -11.8 -0.65";

   // Rigid Body
   mass = 2000;
   bodyFriction = 0.8;
   bodyRestitution = 0.5;
   minRollSpeed = 3;
   gyroForce = 400;
   gyroDamping = 0.3;
   stabilizerForce = 10;
   minDrag = 10;
   softImpactSpeed = 15;       // Play SoftImpact Sound
   hardImpactSpeed = 25;      // Play HardImpact Sound

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 12;
   speedDamageScale = 0.060;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 18;
   collDamageMultiplier   = 0.070;

   // Engine
   engineTorque = 7.0 * 745;
   breakTorque = 7.0 * 745;
   maxWheelSpeed = 20;

   // Springs
   springForce = 8000;
   springDamping = 1300;
   antiSwayForce = 6000;
   staticLoadScale = 2;

   // Tires
   tireRadius = 1.6;
   tireFriction = 10.0;
   tireRestitution = 0.5;
   tireLateralForce = 3000;
   tireLateralDamping = 400;
   tireLateralRelaxation = 1;
   tireLongitudinalForce = 12000;
   tireLongitudinalDamping = 600;
   tireLongitudinalRelaxation = 1;
   tireEmitter = TireEmitter;

   //   
   maxDamage = 15.0;
   destroyedLevel = 15.0;
   armorShrug = 0.03 ;	// +soph

   isShielded = true;
   energyPerDamagePoint = 100;
   maxEnergy = 600;
   jetForce = 2800;
   minJetEnergy = 60;
   jetEnergyDrain = 2.75;
   rechargeRate = 1.0;

   jetSound = MPBThrustSound;
   engineSound = MPBEngineSound;
   squeelSound = AssaultVehicleSkid;
   softImpactSound = GravSoftImpactSound;
   hardImpactSound = HardImpactSound;
   wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 5.0; 
   mediumSplashSoundVelocity = 8.0;   
   hardSplashSoundVelocity = 12.0;   
   exitSplashSoundVelocity = 8.0;
   
   exitingWater      = VehicleExitWaterSoftSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeMediumSplashSound; 

   minMountDist = 3;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = HeavyDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 0.5 0.0 ";
   damageEmitterOffset[1] = "-3.0 0.5 0.0 ";
   damageLevelTolerance[0] = 0.3;
   damageLevelTolerance[1] = 0.7;
   numDmgEmitterAreas = 2;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDGroundMPBIcon;
   cmdMiniIconName = "commander/MiniIcons/com_mpb_grey";
   targetNameTag = 'Jericho';
   targetTypeTag = 'MPB';
   sensorData = VehiclePulseSensor;

   checkRadius = 7.5225;
   
   observeParameters = "1 12 12";

//   runningLight[0] = MPBLight1;
//   runningLight[1] = MPBLight2;

   shieldEffectScale = "0.85 1.2 0.7";
};

//**************************************************************
// WEAPONS
//**************************************************************

datablock SensorData(MPBTurretMissileSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = false;
   detectsActiveJammed = false;
   detectsCloaked = false;
   detectionPings = true;
   detectRadius = 200;
};

datablock TurretData(MobileTurretBase)
{
   className      = VehicleTurret;
   catagory       = "Turrets";
   shapeFile      = "turret_base_mpb.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxDamage      = MobileBaseVehicle.maxDamage;
   destroyedLevel = MobileBaseVehicle.destroyedLevel;
   
   thetaMin      = 15;
   thetaMax      = 140;

   energyPerDamagePoint = 33;
   inheritEnergyFromMount = true;
   firstPersonOnly = true;

   sensorColor = "0 212 45";
   sensorData = MPBTurretMissileSensor;
   sensorRadius = MPBTurretMissileSensor.detectRadius;
   cmdCategory = "Tactical";
   cmdMiniIconName = "commander/MiniIcons/com_turret_grey";
   targetNameTag = 'Jericho';
   targetTypeTag = 'Turret';

   canControl = true;
};

function initMPBFieldUpdate(%obj)
{
   if(!isObject(%obj))
      return;

//   %field = new StaticShape()		// -[soph]
//   {
//       scale = "14 14 14";
//       dataBlock = "WarpBubble";
//   };

//   %field.setPosition(%obj.getPosition());
//   %field.playThread(0, "deploy");
//   %obj.field = %field;		// -[/soph]

   %obj.barrel = MissileBarrelLarge;	// +[soph]
   %obj.mpb_transform = %obj.getTransform() ;
   %obj.mpb_vector = "0 0 0" ;		// +
   %obj.mpb_speed = 0 ;		// +[/soph]

   MPBFieldUpdate(%obj);
}

function MPBFieldUpdate(%obj)
{
   if(!isObject(%obj))
      return;

   if(%obj.deployed)
   {
      if( !%obj.inertialDampener )					// +[soph]
      {									// freaky hacks here
         %obj.inertialDampener = true;

         if( !%obj.beacon.isMounted() )
            %obj.mountObject( %obj.beacon , 12 );

         if( isObject( %obj.turret ) )
            if( %obj.barrel !$= "" )
               %obj.turret.mountImage( %obj.barrel , 0 , false );
            else
               %obj.turret.mountImage( MissileBarrelLarge , 0 , false );
         else
error( "scripts/vehicles/vehicle_mpb.cs MPBFieldUpdate: MPB turret absent!" );
      }									// +[/soph]

      %obj.shutdown = false;

//      objectReflectProjectiles(%obj, 25, "", true);

      if( isObject( %obj.turret ) )			// +[soph]
      {							// +
         %startPos = %obj.getWorldBoxCenter();		// +
         %endPos = vectorAdd(%startPos, vectorScale( getVectorFromObjects( %obj.turret , %obj ) , 5 ) );		
							// + mpb 'gravity is a bitch' logic 
         %rayCastObj = containerRayCast( %startPos , %endPos , $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType , 0 );
							// +
         if(!%rayCastObj)				// +
         {						// +
            %position = %obj.getPosition() ;		// +
            %obj.setFrozenState( false ) ;		// +
            if( isObject( %obj.shield ) )		// +
            {						// +
               %obj.shield.open() ;			// +
               %obj.shield.setTransform( "0 0 -4000 0 0 0" ) ;
            }						// +
            schedule( 32 , 0 , MPBDropCheck , %obj ) ;	// +
         }						// +
      }							// +[soph]

      InitContainerRadiusSearch(%obj.getPosition(), 125, $TypeMasks::PlayerObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType);	// InitContainerRadiusSearch(%obj.getPosition(), 125, $TypeMasks::PlayerObjectType | $TypeMasks::StaticShapeObjectType); -soph

      while((%int = ContainerSearchNext()) != 0)
      {
         if(%int.team == %obj.team)
         {
            %dist = getDistance3D(%obj.position, %int.position); // are we there yet?

            if( %dist < 125 && !%int.ignoreDED && %int != %obj )	// if(%dist < 125 && %int != %obj && !%int.isShieldBeacon) -soph
            {
               if(!%int.ignoreRepairThis)
                  if(%int.getType() & $TypeMasks::PlayerObjectType)	// [soph]
                  {							// same logic as ded loop
                     if(!%int.dead)
                        %int.takeDamage(-0.002 * %int.repairKitFactor);	// %int.takeDamage(-0.00175); - soph //*(%dist/200)); // dist percent
                  }
                  else							// 
                     %int.takeDamage(-0.0025);				// [/soph]
            }

//            if(%int.Angeling || %int.MASCing || %int.isShieldedBASS || %int.mayishieldyou) // and so... the story begins.
//            {
//               if(%dist <= 125)
//                  %int.inDEDField = true; // within acceptable limits
//               else
//                  %int.inDEDField = false; // signal too w34k
//            }
         }
   //      else
   //      {
   //         if(%dist < 275) // end of range
   //            %int.takeDamage(0.0025); //*(%dist/200)); // dist percent
   //      }
      }
   }
   else
   {
      %obj.inertialDampener = false;

//      %position = %obj.getPosition();					// +[soph]
//      if( %obj.lastPosition !$= "" )
//      {
//         %dist = getDistance3D( %position, %obj.lastPosition );	// logic to kill falling mpbs
//         if( %dist > 500 )
//            %obj.getDataBlock().damageObject( %obj, 0 , %position , 0.5 , $DamageType::Crash );
//      }
//      if( isObject( %obj.shield ) )					// dropcheck exception
//      {
//         if( %obj.shield.getTransform() !$= "0 0 -4000 0 0 0" )
//            %obj.lastPosition = %position;
//      }
//      else
//         %obj.lastPosition = %position;				// +[/soph]

      if(!%obj.shutdown)
      {
         %obj.shutdown = true;
         shutdownMPBFieldUpdate(%obj);
      }
   }

   if(isObject(%obj))
   {
      %vector = %obj.getVelocity() ;				// +[soph]
      %speed = vectorLen( %vector ) ;				// + fall failsafe 
      if( %speed > 350 && getWord( %obj.getposition() , 2 ) < -9999 )
         %obj.damage( %obj , %obj.getPosition() , 0.125 , $DamageType::Default ) ;
      if( %speed > 55 && %speed > %obj.mpb_speed * 3 )		// + hack fix for mpb engine bug
      {								// +
         %obj.setTransform( %obj.mpb_transform ) ;		// +
         %obj.setVelocity( "0 0 0" ) ;				// +
         %obj.setVelocity( %obj.mpb_vector ) ;			// +
      }								// +
      else							// +
      {								// +
         %obj.mpb_transform = %obj.getTransform() ;		// +
         %obj.mpb_vector = %vector ;				// +
         %obj.mpb_speed = %speed ;				// +
      }								// +[/soph]

      if(!%obj.getDamageState() !$= "Destroyed")
         schedule(250, 0, MPBFieldUpdate, %obj);
      else
         shutdownMPBFieldUpdate(%obj);
   }
}

function MPBDropCheck( %obj )			// +[soph]
{						// + new section to allow MPBs to deploy on stuff
   if( !isObject( %obj ) )			// + then falls if/when stuff goes poof
      return;					// +
						// +
   if( %obj.dropCheckThread !$= "" )		// +
      cancel( %obj.dropCheckThread ) ;		// +
						// +
   %position = %obj.getPosition() ;		// +
   %data = %obj.getDataBlock().getName() ;	// +
						// +
   if( isObject( %obj.station.trigger ) )	// +
      %obj.station.trigger.setTransform( %obj.station.getTransform() );
						// +
   if( %obj.lastPosition !$= %position )	// + .lastposition is updated every 250 in MPBFieldUpdate
   {						// +
      %obj.lastPosition = %position ;		// +
      %obj.dropCheckThread = schedule( 500 , 0 , MPBDropCheck , %obj );
   }						// +
   else						// +
   {						// +
      %obj.dropCheckThread = "" ;		// +
      if( !%obj.getMountNodeObject( 0 ) )	// +
      {						// +
         if( !isObject( %obj.shield ) )		// +
         {					// +
            %obj.shield = new forceFieldBare()	// +
            {					// +
               scale = "1.22 1.8 1.1" ;		// +
               dataBlock = "defaultTeamSlowFieldBare";
               team = %obj.team ;		// +
            };					// +
         }					// +
         %obj.shield.setTransform( %obj.getSlotTransform( 3 ) ) ;            
         %obj.shield.close() ;			// +
         %obj.setFrozenState( true ) ;		// +
      }						// +
   }						// +
}						// +

function MobileBaseVehicle::onImpact( %data , %vehicleObject , %collidedObject , %vec , %vecLen )
{						// + mpb through-terrain fix hack
   if( %vecLen > 25 && %vecLen > %vehicleObject.mpb_speed * 2 )
   {						// + unreasonable vel and accel
      %time = getSimTime() ;			// + note: %vecLen = 100 zeros shields, = 350 instagibs
      if( %time > %vehicleObject.mbp_recoveryflag )
      {						// +
         %vehicleObject.setTransform( %vehicleObject.mpb_transform ) ;
         %vehicleObject.setVelocity( "0 0 0" ) ;
         %vehicleObject.setVelocity( %vehicleObject.mpb_vector ) ;
         %vehicleObject.mpb_vector = vectorScale( %vehicleObject.mpb_vector , 2 / 3 ) ;
         %vehicleObject.mbp_recoveryflag = %time ;
         return ;				// +
      }						// +
   }						// +
						// +
   Parent::onImpact( %data , %vehicleObject , %collidedObject , %vec , %vecLen ) ;
}						// +[/soph]

function shutdownMPBFieldUpdate(%obj)
{
  // if(isObject(%obj.field))	// -soph
  //    %obj.field.delete();	// -soph
//   InitContainerRadiusSearch(%obj.getPosition(), 400, $TypeMasks::PlayerObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::StaticShapeObjectType);
//
//   while((%int = ContainerSearchNext()) != 0)
//   {
//      if(%int.team == %obj.team) // it died... set all to false
//         %int.inDEDField = false;
//   }
}