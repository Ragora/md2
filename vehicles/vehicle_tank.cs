if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//**************************************************************
// BEOWULF ASSAULT VEHICLE
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************

datablock AudioProfile(AssaultVehicleSkid)
{
   filename    = "fx/vehicles/tank_skid.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(AssaultVehicleEngineSound)
{
   filename    = "fx/vehicles/tank_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(AssaultVehicleThrustSound)
{
   filename    = "fx/vehicles/tank_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(TankChaingunProjectile)
{
   filename    = "fx/weapons/chaingun_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(AssaultTurretActivateSound)
{
    filename    = "fx/vehicles/tank_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(AssaultChaingunDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(AssaultMortarDryFireSound)
{
   filename    = "fx/weapons/mortar_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(AssaultMortarFireSound)
{
   filename    = "fx/vehicles/tank_mortar_fire.wav";
   description = AudioClose3d;
   preload = true;
};

//**************************************************************
// LIGHTS
//**************************************************************

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

//Meltdown.MassVelMod[AssaultVehicle] = 3.0; //not used on hover
Meltdown.MaxDmgMod[AssaultVehicle] = 1.5;
Meltdown.ShieldPctMod[AssaultVehicle] = 1.5; //-Nite-  was 1.0 a tad extra shield

datablock HoverVehicleData(AssaultVehicle) : GroundVehicleDamageProfile
{
   spawnOffset = "0 0 4";

   vehicleType = $VehicleMask::Beowulf;
   hullType = $VehicleHull::Ground;
   
   floatingGravMag = 3.5;

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_tank.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_land_assault_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag = 0.0;
   density = 0.9;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 2;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   mountArmorsAllowed[ 0 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Engineer ;	// +[soph]
   mountArmorsAllowed[ 1 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Blastech | $ArmorMask::Engineer ;
												// +[/soph]
   cameraMaxDist = 20;
   cameraOffset = 3;
   cameraLag = 1.5;
   explosion = LargeGroundVehicleExplosion;
	explosionDamage = 2.5;
	explosionRadius = 25;

   maxSteeringAngle = 0.5;  // 20 deg.

   maxDamage = 12.0;										// 10.0; -soph
   destroyedLevel = 12.0;									// 10.0; -soph
   armorShrug = 0.02 ;										// +soph

   isShielded = true;
   rechargeRate = 1.0;
   energyPerDamagePoint = 200;
   maxEnergy = 400;
   minJetEnergy = 15;
   jetEnergyDrain = 2.75 ; 									// 2.0; -soph

   // Rigid Body
   mass = 1500;
   bodyFriction = 0.8;
   bodyRestitution = 0.5;
   minRollSpeed = 3;
   gyroForce = 400;
   gyroDamping = 0.3;
   stabilizerForce = 20;
   minDrag = 10;

   softImpactSpeed = 999;       // Play SoftImpact Sound
   hardImpactSpeed = 1024;      // Play HardImpact Sound

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 999;
   speedDamageScale = 0;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 45;
   collDamageMultiplier   = 0.15;								// 0.25; -soph

   dragForce            = 2;
   vertFactor           = 0.0;
   floatingThrustFactor = 0.15;

   mainThrustForce    = 60 ;									// 75; -soph
   reverseThrustForce = 40 ;									// 50; -soph
   strafeThrustForce  = 32 ;									// 40; -soph
   turboFactor        =  2 ;									// 1.63; -soph

   brakingForce = 15 ;										// 25; -soph
   brakingActivationSpeed = 4;

   stabLenMin = 3.25;
   stabLenMax = 4;
   stabSpringConstant  = 50;
   stabDampingConstant = 20;

   gyroDrag = 20;
   normalForce = 20;
   restorativeForce = 10;
   steeringForce = 15;
   rollForce  = 5;
   pitchForce = 2;

   dustEmitter = TankDustEmitter;
   triggerDustHeight = 3.5;
   dustHeight = 1.0;
   dustTrailEmitter = TireEmitter;
   dustTrailOffset = "0.0 -1.0 0.5";
   triggerTrailHeight = 3.6;
   dustTrailFreqMod = 15.0;

   jetSound         = AssaultVehicleThrustSound;
   engineSound      = AssaultVehicleEngineSound;
   floatSound       = AssaultVehicleSkid;
   softImpactSound  = GravSoftImpactSound;
   hardImpactSound  = HardImpactSound;
   wheelImpactSound = WheelImpactSound;

   forwardJetEmitter = TankJetEmitter;

   //
   softSplashSoundVelocity = 15.0;
   mediumSplashSoundVelocity = 30.0;
   hardSplashSoundVelocity = 60.0;
   exitSplashSoundVelocity = 20.0;

   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound;

   minMountDist = 4;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "2.5 -2.0 2.5 ";							// damageEmitterOffset[0] = "0.0 -1.5 3.5 "; -soph
   damageEmitterOffset[1] = "-2.5 -2.0 2.5 ";							// damageEmitterOffset[1] = "0.0 -1.5 3.5 "; -soph
   damageLevelTolerance[0] = 0.5;
   damageLevelTolerance[1] = 0.75;
   numDmgEmitterAreas = 2 ;									// numDmgEmitterAreas = 1; -soph

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDGroundTankIcon;
   cmdMiniIconName = "commander/MiniIcons/com_tank_grey";
   targetNameTag = 'Beowulf';
   targetTypeTag = 'Assault Vehicle';
   sensorData = VehiclePulseSensor;

   checkRadius = 5.5535;
   observeParameters = "1 10 10";
//   runningLight[0] = TankLight1;
//   runningLight[1] = TankLight2;
//   runningLight[2] = TankLight3;
//   runningLight[3] = TankLight4;
   shieldEffectScale = "0.9 1.0 0.6";
   showPilotInfo = 1;

  // damageMod = 0.5;										// +soph
  // damageModSlot[0] = 0.5;									// 1.5; -soph
//   damageModSlot[1] = -0.5;									// -soph
   
//   damageModPerMount = 0.5;
};

//**************************************************************
// WEAPONS
//**************************************************************

datablock TurretData(TankShieldCap)
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = 1000;
   destroyedLevel          = 1000;

   thetaMin                = 70;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 200;
   capacitorRechargeRate   = 0.35;
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 0;

   targetNameTag           = '';
   targetTypeTag           = '';
};

//-------------------------------------
// ASSAULT CHAINGUN CHARACTERISTICS
//-------------------------------------

datablock TurretData(AssaultPlasmaTurret) : TurretDamageProfile
{
   className      = VehicleTurret;
   catagory       = "Turrets";
   shapeFile      = "Turret_tank_base.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxEnergy               = 1;
   maxDamage               = AssaultVehicle.maxDamage;
   destroyedLevel          = AssaultVehicle.destroyedLevel;
   repairRate              = 0;
   
   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 1.0;

   thetaMin = 0;
   thetaMax = 115;

   inheritEnergyFromMount = true;
   firstPersonOnly = true;
   useEyePoint = true;
   numWeapons = 2;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   targetNameTag = 'Beowulf Main';
   targetTypeTag = 'Turret';
};

//--------------------------------------------------------------------------
// Weapon
//-------------------------------------- 

//datablock TurretImageData(TankAABarrel1)	// -[soph]
//{						// - removed, reclaimed
//   shapeFile = "turret_aa_large.dts";
//
//   projectileType = TracerProjectile;
//   projectile = AABullet;
//   usesEnergy = true;
//   fireEnergy = 4.0;
//   minEnergy = 4.0;
//   emap = true;
//   offset = "0.8 0 0";
//   
//   // Turret parameters
//   activationMS         = 175; // z0dd - ZOD, 3/27/02. Was 250. Amount of time it takes turret to unfold
//   deactivateDelayMS    = 500;
//   thinkTimeMS          = 140; // z0dd - ZOD, 3/27/02. Was 200. Amount of time before turret starts to unfold (activate)
//   degPerSecTheta       = 600;
//   degPerSecPhi         = 1080;
//   attackRadius         = 300;
//
//   // State transitions
//   stateName[0]                     = "Activate";
//   stateTransitionOnNotLoaded[0]    = "Dead";
//   stateTransitionOnLoaded[0]       = "ActivateReady";
//
//   stateName[1]                     = "ActivateReady";
////   stateSequence[1]                 = "Activate";
//   stateSound[1]                    = AASwitchSound;
//   stateTimeoutValue[1]             = 1.0;
//   stateTransitionOnTimeout[1]      = "Ready";
//   stateTransitionOnNotLoaded[1]    = "Deactivate";
//   stateTransitionOnNoAmmo[1]       = "NoAmmo";
//   stateScript[1]                   = "AAActivateReady";
//
//   stateName[2]                     = "Ready";
//   stateTransitionOnNotLoaded[2]    = "Deactivate";
//   stateTransitionOnTriggerDown[2]  = "Fire1";
//   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateScript[2]                   = "AAReady";
//
//   stateName[3]                     = "Fire1";
//   stateTransitionOnTimeout[3]      = "Reload1";
//   stateTimeoutValue[3]             = 0.1;
//   stateFire[3]                     = true;
//   stateRecoil[3]                   = LightRecoil;
//   stateAllowImageChange[3]         = false;
//   stateSequence[3]                 = "Fire1";
//   stateSound[3]                    = AAFireSound;
//   stateScript[3]                   = "onFire";
//
//   stateName[4]                     = "Reload1";
//   stateTimeoutValue[4]             = 0.1;
//   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
//   stateTransitionOnTimeout[4]      = "Fire2";
//   stateTransitionOnNotLoaded[4]    = "Deactivate";
//   stateTransitionOnNoAmmo[4]       = "NoAmmo";
//
//   stateName[5]                     = "Fire2";
//   stateTransitionOnTimeout[5]      = "Reload2";
//   stateTimeoutValue[5]             = 0.1;
//   stateFire[5]                     = true;
//   stateRecoil[5]                   = LightRecoil;
//   stateAllowImageChange[5]         = false;
//   stateSequence[5]                 = "Fire2";
//   stateSound[5]                    = AAFireSound;
//   stateScript[5]                   = "onFire";
//
//   stateName[6]                     = "Reload2";
//   stateTimeoutValue[6]             = 0.1;
//   stateAllowImageChange[6]         = false;
//   stateSequence[6]                 = "Reload";
//   stateTransitionOnTimeout[6]      = "Ready";
//   stateTransitionOnNotLoaded[6]    = "Deactivate";
//   stateTransitionOnNoAmmo[6]       = "NoAmmo";
//
//   stateName[7]                     = "Deactivate";
//   stateSequence[7]                 = "Activate";
//   stateDirection[7]                = false;
//   stateTimeoutValue[7]             = 1.0;
//   stateTransitionOnLoaded[7]       = "ActivateReady";
//   stateTransitionOnTimeout[7]      = "Dead";
//
//   stateName[8]                    = "Dead";
//   stateTransitionOnLoaded[8]      = "ActivateReady";
//
//   stateName[9]                    = "NoAmmo";
//   stateTransitionOnAmmo[9]        = "Reload2";
//   stateSequence[9]                = "NoAmmo";
//};						// -[/soph]

//datablock TurretImageData(TankAABarrel2) : TankAABarrel1
//{
//   offset = "-2 0 0";
//};

function TankAABarrel1::onFire(%data, %obj, %slot)
{
     Parent::onFire(%data, %obj, %slot);
    // %obj.setImageTrigger(%slot+1, true);	// -soph
}

function TankAABarrel2::onFire(%data, %obj, %slot)
{
     Parent::onFire(%data, %obj, %slot);
     %obj.setImageTrigger(%slot, false);   
}

//-------------------------------------
// ASSAULT MORTAR CHARACTERISTICS
//-------------------------------------

datablock TurretImageData(AssaultMortarTurretBarrel)
{
   shapeFile = "turret_tank_barrelmortar.dts";
   mountPoint = 0;

   usesEnergy = true;
   useMountEnergy = true;
   fireEnergy = 1;
   minEnergy = -1;
   offset = "-0.5 0 0";
   useCapacitor = true;

   // Turret parameters
//   activationMS                  = 1000;
//   deactivateDelayMS             = 500;
//   thinkTimeMS                   = 140;
//   degPerSecTheta                = 600;
//   degPerSecPhi                  = 1080;
//   attackRadius                  = 75;

   activationMS      = 1000;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 300;
   degPerSecPhi      = 500;
   attackRadius      = 100;

   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activate";
   stateSound[1]                 = MBLSwitchSound;
   stateTimeoutValue[1]          = 2;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 2.0;	// 3.0; -soph
   stateFire[3]                = true;
//   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
//   stateSequence[3]            = "Fire";
//   stateSound[3]               = AssaultMortarFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.05;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 2;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

datablock LinearFlareProjectileData(CometCannonBlast)
{
   directDamage        = 0.56;
   hasDamageRadius     = true;
   indirectDamage      = 1.99;
   damageRadius        = 4;
   radiusDamageType    = $DamageType::CometCannon;
   kickBackStrength    = 2000;

   sound               = discProjectileSound;

   explosion           = PlasmaBarrelBoltExplosion; //"BExplosion";
   splash              = DiscSplash;

   baseEmitter       = WadBlueTrailEmitter; //CoolBlueTrailEmitter;
   dryVelocity       = 300.0;
   wetVelocity       = 300.0;
   velInheritFactor  = 0.01;
   fizzleTimeMS      = 1400;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "3.0 3.0 3.0";
   numFlares         = 30;
   flareColor        = "0.13 0.41 0.93";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function AssaultMortarTurretBarrel::onFire(%data, %obj, %slot)
{
//   %data.lightStart = getSimTime();

//   if(isObject(%obj.lastMortar))
//   {
//      TFlakTrigger(%obj.lastMortar);
//      return;
//   }

//   if(!%obj.flakEnabled)
//       return;

   if(%obj.getObjectMount().turretObject.getCapacitorLevel() >= 100 )					// 75) -soph
   {
      %vehicle = %obj.getObjectMount();
      %p = new LinearFlareProjectile()
      {
         dataBlock        = CometCannonBlast;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      }; 
      MissionCleanup.add(%p);

       %vehicle.getMountNodeObject( 0 ).setCollisionTimeout( %p ) ;					// +soph
      %vehicle.turretObject.setCapacitorLevel(%vehicle.turretObject.getCapacitorLevel() - 100 ) ;	// 75); -soph
      %obj.setImageTrigger(%slot, true);
      spawnProjectile(%p, LinearFlareProjectile, CCDisplayCharge);
      projectileTrail(%p, 150, LinearFlareProjectile, GaussTrailCharge, true);
//      schedule(200, %p, CCPassingDamage, %p);            
//      projectileTrail(%p, 50, LinearFlareProjectile, IonCannonTrailCharge);
//      projectileTrail(%p, 64, LinearFlareProjectile, ReaverCharge, true);
//      projectileTrail(%p, 64, LinearFlareProjectile, MitziAnnihalator, true);
//      %obj.play3d(StarHammerFireSound);
        %obj.play3D(AAFireSound);
//        %obj.applyKick(-2000);
      %obj.flakEnabled = false;
//      %obj.getObjectMount().applyImpulse(%obj.getObjectMount().getPosition(), VectorScale(%obj.getMuzzleVector(%slot), -35000));


   //   %obj.lastMortar = %p;
   //   MissionCleanup.add(%p);

      if(%obj.client)
         %obj.client.projectile = %p;

//       schedule(2000, 0, "tEnableFlak", %obj);
   }
   else
      %obj.play3d(MortarDryFireSound);
}

datablock TurretImageData(TSMortarTurretBarrel) : AssaultMortarTurretBarrel
{
   shapeFile                        = "turret_fusion_large.dts";					// "weapon_plasma.dts"; -soph
   offset                           = "-0.1 -1 -0.45";							// "0 0 0"; -soph
   rotation                         = "0 1 0 180" ;							// -soph

   mountPoint = 0;
};

function TSMortarTurretBarrel::onFire(%data, %obj, %slot)
{
//   %data.lightStart = getSimTime();

//   if(isObject(%obj.lastMortar))
//   {
//      TFlakTrigger(%obj.lastMortar);
//      return;
//   }

//   if(!%obj.flakEnabled)
//       return;

   if(%obj.getObjectMount().turretObject.getCapacitorLevel() >= 100 ) 					// 75) -soph
   {
      %vehicle = %obj.getObjectMount();
      %p = new LinearFlareProjectile()
      {
         dataBlock        = CometCannonBlast;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      }; 
      MissionCleanup.add(%p);

      %vehicle.getMountNodeObject( 0 ).setCollisionTimeout( %p ) ;					// +soph
      %vehicle.turretObject.setCapacitorLevel(%vehicle.turretObject.getCapacitorLevel() - 100 ) ;	// 75); -soph
      %obj.setImageTrigger(%slot, true);
      spawnProjectile(%p, LinearFlareProjectile, CCDisplayCharge);
      projectileTrail(%p, 150, LinearFlareProjectile, GaussTrailCharge, true);
      schedule(200, %p, CCPassingDamage, %p);            
//      projectileTrail(%p, 50, LinearFlareProjectile, IonCannonTrailCharge);
//      projectileTrail(%p, 64, LinearFlareProjectile, ReaverCharge, true);
//      projectileTrail(%p, 64, LinearFlareProjectile, MitziAnnihalator, true);
//      %obj.play3d(StarHammerFireSound);
        %obj.play3D(AAFireSound);
//        %obj.applyKick(-2000);
      %obj.flakEnabled = false;
//      %obj.getObjectMount().applyImpulse(%obj.getObjectMount().getPosition(), VectorScale(%obj.getMuzzleVector(%slot), -35000));


   //   %obj.lastMortar = %p;
   //   MissionCleanup.add(%p);

      if(%obj.client)
         %obj.client.projectile = %p;

//       schedule(2000, 0, "tEnableFlak", %obj);
   }
   else
      %obj.play3d(MortarDryFireSound);
}

datablock TurretImageData(AssaultTurretParam)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = TurboDisc ;		// ImpactMortar; -soph
   projectileType = LinearProjectile ;	// GrenadeProjectile; -soph

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 250;
   deactivateDelayMS = 500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 1080;
   degPerSecPhi      = 1080;
   
   attackRadius      = 180 ;	// 75; -soph
};              

datablock ShapeBaseImageData( TankPilotChaingunImage )
{
   className = WeaponImage;
   shapeFile = "turret_assaulttank_plasma.dts";
   item      = Chaingun;
   projectile = MitziSingularityBlast;
   projectileType = LinearFlareProjectile;
   mountPoint = 10;       //?
   offset = "-2.5 3 -1.1" ;				// "0 4.5 -3"; -soph
   rotation = "0 1 0 16" ;				// "0 1 0 180"; -soph

   usesEnergy = false ;
   useMountEnergy = true;
   minEnergy = 20 ;					// minEnergy = 35; -soph
   fireEnergy = 20 ;					// fireEnergy = 35; -soph

//   //--------------------------------------		// -[soph]
//   stateName[0]             = "Activate";
//   stateAllowImageChange[0] = false;
//   stateTimeoutValue[0]        = 0.05;
//   stateTransitionOnTimeout[0] = "Ready";
//   stateTransitionOnNoAmmo[0]  = "NoAmmo";
//   //--------------------------------------
//   stateName[1]       = "Ready";
//   stateSpinThread[1] = Stop;
//   stateSequence[1]         = "Activate";
//   stateTransitionOnTriggerDown[1] = "Spinup";
//   stateTransitionOnNoAmmo[1]      = "NoAmmo";
//   //--------------------------------------
//   stateName[2]               = "NoAmmo";
//   stateTransitionOnAmmo[2]   = "Ready";
//   stateSpinThread[2]         = Stop;
//   stateTransitionOnTriggerDown[2] = "DryFire";
//   //--------------------------------------
//   stateName[3]         = "Spinup";
//   stateSpinThread[3]   = SpinUp;
//   stateTimeoutValue[3]          = 0.01;
//   stateWaitForTimeout[3]        = false;
//   stateTransitionOnTimeout[3]   = "Fire";
//   stateTransitionOnTriggerUp[3] = "Spindown";
//   //--------------------------------------
//   stateName[4]             = "Fire";
//   stateSpinThread[4]       = FullSpeed;
//   stateRecoil[4]           = LightRecoil;
//   stateSequence[4]         = "Fire";
//   stateAllowImageChange[4] = false;
//   stateScript[4]           = "onFire";
//   stateFire[4]             = true;
//   stateSound[4]            = AAFireSound;
//   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
//   // above.
//   stateTimeoutValue[4]          = 0.75; 
//   stateTransitionOnTimeout[4]   = "checkState";
//   //--------------------------------------
//   stateName[5]       = "Spindown";
//   stateSpinThread[5] = SpinDown;
//   stateTimeoutValue[5]            = 0.01;
//   stateWaitForTimeout[5]          = false;
//   stateTransitionOnTimeout[5]     = "Ready";
//   stateTransitionOnTriggerDown[5] = "Spinup";
//   //--------------------------------------
//   stateName[6]       = "EmptySpindown";
////   stateSound[6]      = ChaingunSpindownSound;
//   stateSpinThread[6] = SpinDown;
//   stateTransitionOnAmmo[6]   = "Ready";
//   stateTimeoutValue[6]        = 0.01;
//   stateTransitionOnTimeout[6] = "NoAmmo";
//   //--------------------------------------
//   stateName[7]       = "DryFire";
//   stateSound[7]      = ShrikeBlasterDryFireSound;
//   stateTransitionOnTriggerUp[7] = "NoAmmo";
//   stateTimeoutValue[7]        = 0.25;
//   stateTransitionOnTimeout[7] = "NoAmmo";
//
//   stateName[8] = "checkState";
//   stateTransitionOnTriggerUp[8] = "Spindown";
//   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
//   stateTimeoutValue[8]          = 0.01;
//   stateTransitionOnTimeout[8]   = "ready";		// -[/soph]

   stateName[ 0 ]			= "Activate" ;	// +[soph]
   stateSequence[ 0 ]			= "Deploy";
   stateAllowImageChange[ 0 ]		= false ;
   stateTimeoutValue[ 0 ]		= 1 ;
   stateTransitionOnTimeout[ 0 ] 	= "Idle" ;

   stateName[ 1 ]	   		= "Idle" ;
   stateTransitionOnTriggerDown[ 1 ] 	= "Spinup" ;

   stateName[ 2 ]	   		= "Spinup" ;
   stateSequence[ 2 ]		 	= "Activate" ;
   stateScript[ 2 ]		   	= "onSpinup" ;
   stateTimeoutValue[ 2 ]		= 2 ;
   stateSound[ 2 ]			= MBLSwitchSound ;
   stateTransitionOnTriggerUp[ 2 ] 	= "SpinDown" ;
   stateTransitionOnTimeout[ 2 ] 	= "Ready" ;
   
   stateName[ 3 ]			= "Ready" ;
   stateTransitionOnTriggerUp[ 3 ] 	= "SpinDown" ;
//   stateTransitionOnNoAmmo[ 3 ]	  	= "DryFire" ;
   stateTransitionOnTimeout[ 3 ] 	= "Fire" ;

   stateName[ 4 ]			= "Fire" ;
   stateRecoil[ 4 ]		   	= LightRecoil ;
   stateAllowImageChange[ 4 ] 		= false ;
   stateScript[ 4 ]		   	= "onFire" ;
   stateFire[ 4 ]			= true ;
//   stateSound[ 4 ]			= AAFireSound ;
   stateTimeoutValue[ 4 ]		= 0.25 ; 
   stateTransitionOnTimeout[ 4 ]   	= "SwitchBarrel" ;

   stateName[ 5 ] 			= "SwitchBarrel" ;
   stateTransitionOnTriggerUp[ 5 ] 	= "SpinDown" ;
   stateTransitionOnTimeout[ 5 ]   	= "FirePair" ;

   stateName[ 6 ]			= "FirePair" ;
   stateAllowImageChange[ 6 ] 		= false ;
   stateScript[ 6 ]		   	= "onFirePair" ;
   stateTimeoutValue[ 6 ]		= 0.25 ; 
   stateTransitionOnTimeout[ 6 ]   	= "Ready" ;

   stateName[ 7 ]                 	= "Spindown" ;
   stateSequence[ 7 ]                 	= "Activate" ;
   stateDirection[ 7 ]                	= false ;
   stateScript[ 7 ]		   	= "onSpindown" ;
   stateSound[ 7 ]			= MobileBaseStationDeploySound ;
   stateTimeoutValue[ 7 ]		= 2 ;
   stateTransitionOnTriggerDown[ 7 ] 	= "SpinUp" ;
   stateTransitionOnTimeout[ 7 ] 	= "Idle" ;

//   stateName[ 8 ]			= "DryFire" ;
//   stateSound[ 8 ]			= ShrikeBlasterDryFireSound ;
//   stateTimeoutValue[ 8 ]		= 0.25 ; 
//   stateTransitionOnTimeout[ 8 ]	= "SwitchBarrel" ;
};

datablock ShapeBaseImageData( TankPilotChaingunImagePair )
{

   className = WeaponImage ;
   shapeFile = "turret_assaulttank_plasma.dts" ;
   item      = Chaingun ;
   projectile = MitziSingularityBlast ;
   projectileType = LinearFlareProjectile ;
   mountPoint = 10 ;
   offset = "2.5 3 -1.1" ;
   rotation = "0 1 0 -16" ;

   usesEnergy = false ;
   useMountEnergy = true ;
   minEnergy = 20 ;
   fireEnergy = 20 ;

   stateName[ 0 ]			= "Activate" ;
   stateSequence[ 0 ]			= "Deploy";
   stateAllowImageChange[ 0 ]		= false ;
   stateTimeoutValue[ 0 ]		= 3 ;
   stateTransitionOnTimeout[ 0 ] 	= "Idle" ;

   stateName[ 1 ]	   		= "Idle" ;
   stateTransitionOnTriggerDown[ 1 ] 	= "Spinup" ;

   stateName[ 2 ]	   		= "Spinup" ;
   stateSequence[ 2 ]		 	= "Activate" ;
   stateScript[ 2 ]		   	= "onSpinup" ;
   stateTimeoutValue[ 2 ]		= 2 ;
   stateSound[ 2 ]			= MBLSwitchSound ;
   stateTransitionOnTriggerUp[ 2 ] 	= "SpinDown" ;
   stateTransitionOnTimeout[ 2 ] 	= "Ready" ;
   
   stateName[ 3 ]			= "Ready" ;
   stateTransitionOnTriggerUp[ 3 ] 	= "SpinDown" ;
   stateTransitionOnTimeout[ 3 ] 	= "FirePair" ;

   stateName[ 4 ]			= "Fire" ;
   stateRecoil[ 4 ]		   	= LightRecoil ;
   stateAllowImageChange[ 4 ] 		= false ;
   stateScript[ 4 ]		   	= "onFire" ;
   stateFire[ 4 ]			= true ;
//   stateSound[ 4 ]			= AAFireSound ;
   stateTimeoutValue[ 4 ]		= 0.25 ; 
   stateTransitionOnTimeout[ 4 ]   	= "Ready" ;

   stateName[ 5 ] 			= "SwitchBarrel" ;
   stateTransitionOnTimeout[ 5 ]   	= "Fire" ;
//   stateTransitionOnNoAmmo[ 5 ]	  	= "DryFire" ;
   stateTransitionOnTriggerUp[ 5 ] 	= "SpinDown" ;

   stateName[ 6 ]			= "FirePair" ;
   stateAllowImageChange[ 6 ] 		= false ;
   stateScript[ 6 ]		   	= "onFirePair" ;
   stateTimeoutValue[ 6 ]		= 0.25 ; 
   stateTransitionOnTimeout[ 6 ]   	= "SwitchBarrel" ;

   stateName[ 7 ]                 	= "Spindown" ;
   stateSequence[ 7 ]                 	= "Activate" ;
   stateDirection[ 7 ]                	= false ;
   stateScript[ 7 ]		   	= "onSpindown" ;
   stateSound[ 7 ]			= MobileBaseStationDeploySound ;
   stateTimeoutValue[ 7 ]		= 2 ;
   stateTransitionOnTriggerDown[ 7 ] 	= "SpinUp" ;
   stateTransitionOnTimeout[ 7 ] 	= "Idle" ;

//   stateName[ 8 ]			= "DryFire" ;
//   stateSound[ 8 ]			= ShrikeBlasterDryFireSound ;
//   stateTimeoutValue[ 8 ]		= 0.25 ; 
//   stateTransitionOnTimeout[ 8 ]	= "Ready" ;
};

function TankPilotChaingunImage::onSpinUp( %data , %obj , %slot )
{
   %obj.pilotCannonSkip[ %slot + 1 ] = %obj.pilotCannonSkip[ %slot ] ? false : true ;
//   %obj.setImageTrigger( %slot + 1 , true ) ;
}

//function TankPilotChaingunImage::onSpinDown( %data , %obj , %slot )
//{
//   %obj.setImageTrigger( %slot + 1 , false ) ;
//}

function TankPilotChaingunImage::onFire( %data , %obj , %slot )
{
   switch( %obj.pilotCannonMode )
   {
      case 0 :
         TankPilotCannonLoop( %data , %obj , %slot , 5 ) ;
      case 1 :
         TankPilotCannonFire( %data , %obj , %slot ) ;
      case 2 :
         %obj.pilotCannonSkip[ %slot ] = %obj.pilotCannonSkip[ %slot ] ? false : true ;
         if( %obj.pilotCannonSkip[ %slot ] )
            TankPilotCannonFire( %data , %obj , %slot ) ;
   }
}

function TankPilotChaingunImage::onFirePair( %data , %obj , %slot )
{
   if( !%obj.pilotCannonMode )
      TankPilotCannonLoop( %data , %obj , %slot , 5 ) ;
}

function TankPilotChaingunImagePair::onFire( %data , %obj , %slot )
{
   switch( %obj.pilotCannonMode )
   {
      case 0 :
         TankPilotCannonLoop( %data , %obj , %slot , 5 ) ;
      case 1 :
         TankPilotCannonFire( %data , %obj , %slot ) ;
   }
}

function TankPilotChaingunImagePair::onFirePair( %data , %obj , %slot )
{
   switch( %obj.pilotCannonMode )
   {
      case 0 :
         TankPilotCannonLoop( %data , %obj , %slot , 4 ) ;
      case 2 :
         %obj.pilotCannonSkip[ %slot ] = %obj.pilotCannonSkip[ %slot ] ? false : true ;
         if( %obj.pilotCannonSkip[ %slot ] )
            TankPilotCannonFire( %data , %obj , %slot ) ;
   }
}

function TankPilotCannonLoop( %data , %obj , %slot , %num ) 
{
   if( !isObject( %obj ) )
      return ;

   TankPilotCannonFire( %data , %obj , %slot ) ;
   if( %num-- > 0 && %obj.getEnergyLevel() > 5 )
      schedule( 63 , 0 , TankPilotCannonLoop , %data , %obj , %slot , %num ) ;
}

function TankPilotCannonFire( %data , %obj , %slot ) 
{
   %pilot = %obj.getMountNodeObject( 0 ) ;
   if( !%pilot || %pliot.dead )
   {
      %obj.setImageTrigger( 6 , false ) ;
      %obj.setImageTrigger( 7 , false ) ;
   }

   switch( %obj.pilotCannonMode )
   {
      case 0:
         %enUse = 4 ;
         %vector = %obj.getForwardVector() ;
         %projectileClass = LinearProjectile ;
         %projectileName = RAXXFlame ;
         %fireSound = "" ;	// PlasmaSwitchSound ;
         %dryFireSound = "" ;	// PlasmaDryFireSound ;
         %speedMod = 1.25 ;
      case 1:
         %enUse = 22.5 ;
         %vector = %obj.getMuzzleVector( %slot ) ;
         %projectileClass = LinearFlareProjectile ;
         %projectileName = MitziSingularityBlast ;
         %fireSound = AAFireSound ;
         %dryFireSound = ShrikeBlasterDryFireSound ;
         %speedMod = 0.9 ;
      case 2: 
         %enUse = 60 ;
         %vector = %obj.getMuzzleVector( %slot ) ;
         %projectileClass = LinearFlareProjectile ;
         %projectileName = MitziEMPBlast ;
         %fireSound = PlasmaFireSound ;
         %dryFireSound = GrenadeDryFireSound ;
         %speedMod = 1 ;
   }
   %energy = %obj.getEnergyLevel() ;
   if( %energy < %enUse )
   {
      if( %dryFireSound !$= "" )
         serverPlay3D( %dryFireSound , %obj.getSlotTransform( %slot ) ) ;
      return;
   }
   else
   {
      %obj.setEnergyLevel( %energy - %enUse ) ;
      if( %fireSound !$= "" )
         serverPlay3D( %fireSound , %obj.getSlotTransform( %slot ) ) ;
   }

   %p = new ( %projectileClass )() {
      dataBlock        = %projectileName ;
      initialDirection = vectorScale( %vector , %speedMod ) ;
      initialPosition  = %obj.getMuzzlePoint( %slot ) ;
      sourceObject     = %obj ;
      sourceSlot       = %slot ;
      vehicleObject    = %obj ;
   } ;
   %p.sourceObject = %pilot ;
   %p.damageMod = %speedMod ;
   MissionCleanup.add( %p ) ;

   // AI hook
   if( %obj.client )
      %obj.client.projectile = %p ;
}							// +[/soph]

datablock ShapeBaseImageData(TankPilotChaingunParam)
{
   mountPoint = 6;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = ImpactMortar;
   projectileType = GrenadeProjectile;
};

function AssaultVehicle::onTrigger(%data, %obj, %trigger, %state)     // there.
{
     if(%trigger == 0)
     {
          switch(%state)
          {
               case 0:
                    %obj.fireWeapon = false;
                    %obj.setImageTrigger(6, false);
                    %obj.setImageTrigger( 7 , false ) ;	// +soph
               case 1:
                    %obj.fireWeapon = true;
                    %obj.setImageTrigger(6, true);
                    %obj.setImageTrigger( 7 , true ) ;	// +soph
          }
     }
}
