if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(TurbocatShocklance)
{
   className = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   item      = Chaingun;
   projectile = HBPlasmaBolt;
   projectileType = LinearFlareProjectile;
   mountPoint = 10;
   offset = "0 0.75 0.175";
   rotation = "0 1 0 0";

   usesEnergy = true;
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 5;	// 20;	-soph
   fireEnergy = 35;	// 100;	-soph
   fireTimeout = 1500;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = ShockLanceDryFireSound;
   stateTimeoutValue[4]          = 1.5;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

function ScoutVehicle::onTrigger(%data, %obj, %trigger, %state)
{
   // data = ScoutFlyer datablock
   // obj = ScoutFlyer object number
   // trigger = 0 for "fire", 1 for "jump", 3 for "thrust"
   // state = 1 for firing, 0 for not firing
   if(%trigger == 0)
   {
      switch (%state)
      {
         case 0:
            %obj.fireWeapon = false;
            %obj.setImageTrigger(2, false);
         case 1:
            %obj.fireWeapon = true;
               %obj.setImageTrigger(2, true);
      }
   }
}

function TurbocatShocklance::onFire(%this, %obj, %slot)
{
   %projectile = "BasicShocker2"; //-Nite-
   %hitEnergy = %this.fireEnergy ;	// 100; -soph
   %missEnergy = %this.minEnergy ;	// 20; -soph
   
   %useEnergyObj = %obj.getObjectMount();
   %mountDamageMod = 0;

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : %obj;

  %muzzlePos = %obj.getMuzzlePoint(%slot);
  %muzzleVec = %obj.getMuzzleVector(%slot);

  %endPos  = VectorAdd(%muzzlePos, VectorScale(%muzzleVec, 30));

  %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
   $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
   $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;

  %everythingElseMask = $TypeMasks::TerrainObjectType |
             $TypeMasks::InteriorObjectType |
             $TypeMasks::ForceFieldObjectType |
             $TypeMasks::StaticObjectType |
             $TypeMasks::MoveableObjectType |
             $TypeMasks::DamagableItemObjectType;

  // did I miss anything? players, vehicles, stations, gens, sensors, turrets
  %hit = ContainerRayCast(%muzzlePos, %endPos, %damageMasks | %everythingElseMask, %obj);

  %noDisplay = true;
  if(%obj.getEnergyLevel() > %hitEnergy)
     %noDisplay = false;

  %player = %obj.getMountNodeObject(0);
//  echo(%player SPC %player.getDatablock().getName() SPC %obj.getObjectMount() SPC %obj SPC %obj.getDatablock().getName());
  
  if(%hit && !%noDisplay)
  {
//   %obj.setEnergyLevel(%obj.getEnergyLevel() - %hitEnergy);	// moving down... -soph

   %hitobj = getWord(%hit, 0);
   %hitpos = getWord(%hit, 1) @ " " @ getWord(%hit, 2) @ " " @ getWord(%hit, 3);

   if ( %hitObj.getType() & %damageMasks )
   {
     %obj.setEnergyLevel(%obj.getEnergyLevel() - %hitEnergy);	// ...to here -soph

     if( !%hitobj.inertialDampener && !%hitobj.getDatablock().forceSensitive )		// +soph
        %hitobj.applyImpulse(%hitpos, VectorScale(%muzzleVec, %projectile.impulse));
     %obj.playAudio(0, ShockLanceHitSound);

     // This is truly lame, but we need the sourceobject property present...
     %p = new ShockLanceProjectile() {
      dataBlock    = %projectile;
      initialDirection = %obj.getMuzzleVector(%slot);
      initialPosition = %obj.getMuzzlePoint(%slot);
      sourceObject   = %obj;
      sourceSlot    = %slot;
      targetId     = %hit;
      bkSourceObject = %player;
     };
     MissionCleanup.add(%p);

     %damageMultiplier = 1.0;

     if(%hitObj.getDataBlock().getClassName() $= "PlayerData")
     {
      // Now we see if we hit from behind...
      %forwardVec = %hitobj.getForwardVector();
      %objDir2D  = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0";
      %objPos   = %hitObj.getPosition();
      %dif    = VectorSub(%objPos, %muzzlePos);
      %dif    = getWord(%dif, 0) @ " " @ getWord(%dif, 1) @ " 0";
      %dif    = VectorNormalize(%dif);
      %dot    = VectorDot(%dif, %objDir2D);

      // 120 Deg angle test...
      // 1.05 == 60 degrees in radians
      if (%dot >= mCos(1.05)) {
        // Rear hit

        %damageMultiplier = 12 ;			// = %hitObj.getDatablock().isBattleAngel ? 8 : 6; -soph
      }
     }

      %totalDamage = 1.5 * %damageMultiplier ;		// = 3.0 * %damageMultiplier; -soph
      
  // Vehicle Damage Modifier
   if(%vehicle)
     %totalDamage *= 1 + %vehicle.damageMod;

     %hitObj.getDataBlock().damageObject(%hitobj, %player, %hitpos, %totalDamage, $DamageType::ShockLance);
     return;
   }
  }

  %noDisplay = true;

  if( %noDisplay )
  {
   // Miss
   %obj.setEnergyLevel(%obj.getEnergyLevel() - %missEnergy);
   %obj.playAudio(0, ShockLanceMissSound);

   %p = new ShockLanceProjectile() {
     dataBlock    = %projectile;
     initialDirection = %obj.getMuzzleVector(%slot);
     initialPosition = %obj.getMuzzlePoint(%slot);
     sourceObject   = %obj;
     sourceSlot    = %slot;
   };
   MissionCleanup.add(%p);

  }
}

function ScoutVehicle::playerDismounted(%data, %obj, %player)
{
   %obj.fireWeapon = false;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(3, false);
   %obj.setImageTrigger(4, false);
   %obj.setImageTrigger(5, false);
   %obj.setImageTrigger(6, false);
   %obj.setImageTrigger(7, false);

   setTargetSensorGroup(%obj.getTarget(), %obj.team);
}

datablock ShapeBaseImageData( ViperMissileParam )
{
   mountPoint = 4 ;
   offset = "0 0.25 0.25" ;
   rotation = "1 0 0 -25" ;
   shapeFile = "turret_muzzlepoint.dts" ;

   isSeeker     = true ;
   seekRadius   = 500 ;
   maxSeekAngle = 50 ;
   seekTime     = 1.15 ;
   minSeekHeat  = 0.75 ;
   useTargetAudio = false ;
   minTargetingDistance = 5 ;
};

datablock ShapeBaseImageData( ViperImage )
{
   className = WeaponImage ;
   shapeFile = "weapon_missile.dts" ;
   offset = "-0.125 -0.9 0.875" ;
   rotation = "-0.25075 0.935066 0.25055 93.7983" ;
   emap = true ;
   mountPoint = 10 ;

   usesEnergy = true ;
   minEnergy = 50 ;
   fireEnergy = 25 ;
   
   // State Data
   stateName[ 0 ]                     = "Preactivate" ;
   stateTransitionOnLoaded[ 0 ]       = "Activate" ;
   stateTransitionOnNoAmmo[ 0 ]       = "NoAmmo" ;
   stateSequence[ 0 ]                 = "Deploy" ;

   stateName[ 1 ]                     = "Activate" ;
   stateTransitionOnTimeout[ 1 ]      = "Ready" ;
   stateTimeoutValue[ 1 ]             = 0.5 ;

   stateName[ 2 ]                     = "Ready" ;
   stateTransitionOnNoAmmo[ 2 ]       = "NoAmmo" ;
   stateSequence[ 2 ]                 = "Deploy" ;
   stateTransitionOnTriggerDown[ 2 ]  = "Fire" ;

   stateName[ 3 ]                     = "Fire" ;
   stateTransitionOnTimeout[ 3 ]      = "Reload" ;
   stateTimeoutValue[ 3 ]             = 2.5 ;
   stateScript[ 3 ]                   = "onFire" ;
   stateFire[ 3 ]                     = true ;
   stateRecoil[ 3 ]                   = LightRecoil ;
   stateAllowImageChange[ 3 ]         = false ;
   stateSequence[ 3 ]                 = "fire" ;

   stateName[ 4 ]                     = "Reload" ;
   stateTransitionOnNoAmmo[ 4 ]       = "NoAmmo" ;
   stateTransitionOnTimeout[ 4 ]      = "Ready" ;
   stateTimeoutValue[ 4 ]             = 1.0 ;
   stateAllowImageChange[ 4 ]         = false ;
   stateSequence[ 4 ]                 = "Reload" ;

   stateName[ 5 ]                     = "NoAmmo" ;
   stateTransitionOnAmmo[ 5 ]         = "Reload" ;
   stateTransitionOnTriggerDown[ 5 ]  = "DryFire" ;

   stateName[ 6 ]                     = "DryFire" ;
   stateSound[ 6 ]                    = MortarDryFireSound ;
   stateTimeoutValue[ 6 ]             = 1.0 ;
   stateTransitionOnTimeout[ 6 ]      = "NoAmmo" ;
};

datablock ShapeBaseImageData( ViperImagePair )
{
   className = WeaponImage ;
   shapeFile = "weapon_missile.dts" ;
   offset = "0.425 -0.75 0.6" ;
   rotation = "1 0 0 -30" ;
   emap = true ;
   mountPoint = 10 ;

   usesEnergy = true ;
   minEnergy = 25 ;
   fireEnergy = 25 ;
   
   // State Data
   stateName[ 0 ]                     = "Preactivate" ;
   stateTransitionOnLoaded[ 0 ]       = "Activate" ;
   stateTransitionOnNoAmmo[ 0 ]       = "NoAmmo" ;
   stateSequence[ 0 ]                 = "Deploy" ;

   stateName[ 1 ]                     = "Activate" ;
   stateTransitionOnTimeout[ 1 ]      = "Ready" ;
   stateTimeoutValue[ 1 ]             = 0.5 ;

   stateName[ 2 ]                     = "Ready" ;
//   stateTransitionOnNoAmmo[ 2 ]       = "NoAmmo" ;
   stateSequence[ 2 ]                 = "Deploy" ;
   stateTransitionOnTriggerDown[ 2 ]  = "Prefire" ;

   stateName[ 3 ]                     = "Prefire" ;
   stateTransitionOnTimeout[ 3 ]      = "Prefirecheck" ;
   stateTimeoutValue[ 3 ]             = 0.5 ;
   stateAllowImageChange[ 3 ]         = false ;

   stateName[ 4 ]                     = "Prefirecheck" ;
   stateTransitionOnNoAmmo[ 4 ]       = "NoAmmo" ;
   stateTransitionOnAmmo[ 4 ]         = "Fire" ;

   stateName[ 5 ]                     = "Fire" ;
   stateTransitionOnTimeout[ 5 ]      = "Reload" ;
   stateTimeoutValue[ 5 ]             = 1.9 ;
   stateScript[ 5 ]                   = "onFire" ;
   stateFire[ 5 ]                     = true ;
   stateRecoil[ 5 ]                   = LightRecoil ;
   stateAllowImageChange[ 5 ]         = false ;
   stateSequence[ 5 ]                 = "fire" ;

   stateName[ 6 ]                     = "Reload" ;
   stateTransitionOnNoAmmo[ 6 ]       = "NoAmmo" ;
   stateTransitionOnTimeout[ 6 ]      = "Ready" ;
   stateTimeoutValue[ 6 ]             = 1.0 ;
   stateAllowImageChange[ 6 ]         = false ;
   stateSequence[ 6 ]                 = "Reload" ;

   stateName[ 7 ]                     = "NoAmmo" ;
   stateTransitionOnAmmo[ 7 ]         = "Reload" ;
   stateTransitionOnTriggerDown[ 7 ]  = "DryFire" ;

   stateName[ 8 ]                     = "DryFire" ;
   stateSound[ 8 ]                    = MortarDryFireSound ;
   stateTimeoutValue[ 8 ]             = 1.0 ;
   stateTransitionOnTimeout[ 8 ]      = "NoAmmo" ;
};

function ViperImage::onFire( %this , %obj , %slot )
{
	%position = %obj.getMuzzlePoint( %slot ) ;

	%energy = %obj.getEnergyLevel() ;
	if( %energy > %this.fireEnergy )
        {
		%obj.setEnergyLevel( %energy - %this.fireEnergy ) ;
		serverPlay3D( SRM4FireSound , %position ) ;
        }
	else
	{
		serverPlay3D( MissileDryFireSound, %position ) ;
		return ;
	}

	%direction = vectorScale( %obj.getForwardVector() , 2.5 ) ;
	%direction = vectorSub( %obj.getWorldBoxCenter() , %direction ) ;
	%drawPosition = vectorAdd( %position , %obj.getMuzzlePoint( %slot + 1 ) ) ;
	%drawPosition = vectorScale( %drawPosition , 0.5 ) ;
	%direction = vectorSub( %drawPosition , %direction ) ;
	%p = new SeekerProjectile()
	{
		dataBlock        = StreakSRM4Missile ;
		initialDirection = %direction ;
		initialPosition  = %position ;
		sourceObject     = %obj ;
		sourceSlot       = %slot ;
		vehicleObject    = %obj ;
		accuFire         = true ;
	};
	MissionCleanup.add( %p ) ;
	MissileSet.add( %p ) ;
	%target = %obj.getLockedTarget() ;
	if( %target )
		if( %target.getDatablock().jetfire )
			assignTrackerTo( %target , %p ) ;
		else
			%p.setObjectTarget( %target ) ;
	else if( %obj.isLocked() )
		%p.setPositionTarget( %obj.getLockedPosition() ) ;
	else
		%p.setNoTarget() ;

	%obj.setImageTrigger( 3 , true ) ;
}

function ViperImagePair::onFire( %this , %obj , %slot )
{
	%position = %obj.getMuzzlePoint( %slot ) ;

	%energy = %obj.getEnergyLevel() ;
	if( %energy > %this.fireEnergy )
        {
		%obj.setEnergyLevel( %energy - %this.fireEnergy ) ;
		serverPlay3D( SRM4FireSound , %position ) ;
        }
	else
	{
		serverPlay3D( MissileDryFireSound, %position ) ;
		return ;
	}

	%direction = vectorScale( %obj.getForwardVector() , 2.5 ) ;
	%direction = vectorSub( %obj.getWorldBoxCenter() , %direction ) ;
	%drawPosition = vectorAdd( %position , %obj.getMuzzlePoint( %slot - 1 ) ) ;
	%drawPosition = vectorScale( %drawPosition , 0.5 ) ;
	%direction = vectorSub( %drawPosition , %direction ) ;
	%p = new SeekerProjectile()
	{
		dataBlock        = StreakSRM4Missile ;
		initialDirection = %direction ;
		initialPosition  = %position ;
		sourceObject     = %obj ;
		sourceSlot       = %slot ;
		vehicleObject    = %obj ;
		accuFire         = true ;
	};
	MissionCleanup.add( %p ) ;
	MissileSet.add( %p ) ;
	%target = %obj.getLockedTarget() ;
	if( %target )
		if( %target.getDatablock().jetfire )
			assignTrackerTo( %target , %p ) ;
		else
			%p.setObjectTarget( %target ) ;
	else if( %obj.isLocked() )
		%p.setPositionTarget( %obj.getLockedPosition() ) ;
	else
		%p.setNoTarget() ;
	%obj.setImageTrigger( 3 , false ) ;
}