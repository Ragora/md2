if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//**************************************************************
// WILDCAT GRAV CYCLE
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************

datablock AudioProfile(ScoutSqueelSound)
{
   filename    = "fx/vehicles/outrider_skid.wav";
   description = ClosestLooping3d;
   preload = true;
};

// Scout
datablock AudioProfile(ScoutEngineSound)
{
   filename    = "fx/vehicles/outrider_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(ScoutThrustSound)
{
   filename    = "fx/vehicles/outrider_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

//**************************************************************
// LIGHTS
//**************************************************************

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

datablock HoverVehicleData(ScoutVehicle) : VehicleDamageProfile
{
   spawnOffset = "0 0 1";

   floatingGravMag = 3.5;

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_scout.dts";
   computeCRC = true;

   vehicleType = $VehicleMask::Wildcat ;				// +soph
   hullType = $VehicleHull::Wildcat ;					// +soph

   debrisShapeName = "vehicle_grav_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag = 0.0;
   density = 0.9;
   maxSteeringAngle = 0.65;

   mountPose[0] = scoutRoot;

   mountArmorsAllowed[ 0 ] = $ArmorMask::Light | $ArmorMask::MagIon ;	// +soph

   cameraMaxDist = 5.0;
   cameraOffset = 0.7;
   cameraLag = 0.5;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   explosion = TFlakMortarExplosion;
	explosionDamage = 0.35;						// = 0.5; -soph
	explosionRadius = 12.0;						// = 15.0; -soph

   lightOnly = 1;

   maxDamage = 0.9;							// = 1.5; -soph
   destroyedLevel = 0.9;						// = 1.5; -soph

   isShielded = true;							// = false; -soph
   rechargeRate = 0.25;							// = 0.7; -soph
   energyPerDamagePoint = 222;						// = 150; -soph
   maxEnergy = 100;							// = 300; -soph
   minJetEnergy = 5;							// = 15; -soph
   jetEnergyDrain = 0.45;						// = 1.3; -soph

   // Rigid Body
   mass = 400;
   bodyFriction = 0.1;
   bodyRestitution = 0.5;  
   softImpactSpeed = 60;       // Play SoftImpact Sound
   hardImpactSpeed = 88;      // Play HardImpact Sound

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 50;
   speedDamageScale = 0.01;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 40.0; //10? you sick bastard! Death~bot
   collDamageMultiplier   = 0.0225;
   //
   dragForce            = 1.5; // 0.55555555555555555555
   vertFactor           = 0.0;
   floatingThrustFactor = 0.45;

   mainThrustForce    = 67.5;
   reverseThrustForce = 48;
   strafeThrustForce  = 34;
   turboFactor        = 1.5;

   brakingForce = 10;
   brakingActivationSpeed = 4;

   stabLenMin = 2.25;
   stabLenMax = 3.75;
   stabSpringConstant  = 30;
   stabDampingConstant = 16;

   gyroDrag = 8;
   normalForce = 30;
   restorativeForce = 20;
   steeringForce = 15;
   rollForce  = 7;
   pitchForce = 4;

   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 2.5;
   dustHeight = 1.0;
   dustTrailEmitter = TireEmitter;
   dustTrailOffset = "0.0 -1.0 0.5";
   triggerTrailHeight = 3.6;
   dustTrailFreqMod = 15.0;

   jetSound         = ScoutSqueelSound;
   engineSound      = ScoutEngineSound;
   floatSound       = ScoutThrustSound;
   softImpactSound  = GravSoftImpactSound;
   hardImpactSound  = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 10.0; 
   mediumSplashSoundVelocity = 20.0;   
   hardSplashSoundVelocity = 30.0;   
   exitSplashSoundVelocity = 10.0;
   
   exitingWater      = VehicleExitWaterSoftSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterSoftSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeSoftSplashSound; 

   max[chaingunAmmo] = 1000;						// +soph

   minMountDist = 4;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -1.5 0.5 ";
   damageLevelTolerance[0] = 0.5;
   damageLevelTolerance[1] = 0.75;
   numDmgEmitterAreas = 1;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;
   
   forwardJetEmitter = WildcatJetEmitter;

   cmdCategory = Tactical;
   cmdIcon = CMDHoverScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_landscout_grey";
   targetNameTag = 'WildCat';
   targetTypeTag = 'Grav Cycle';
   sensorData = VehiclePulseSensor;

   numWeapons = 2;							// +soph

   checkRadius = 1.7785;
   observeParameters = "1 10 10";

//   runningLight[0] = WildcatLight1;
//   runningLight[1] = WildcatLight2;
//   runningLight[2] = WildcatLight3;

   shieldEffectScale = "0.9375 1.125 0.6";

   max[ChaingunAmmo] = 300;						// +soph
};

function resolveWingTriggerTimeout(%obj)
{
   if(isObject(%obj))
      %obj.wingTriggerTimeout = 0;
}

function xxTurboCat::onTrigger(%data, %obj, %trigger, %state)
{
   // trigger = 0 for "fire", 1 for "jump", 3 for "thrust"
   // state = 1 for firing, 0 for not firing
   if(%trigger == 0)
   {
      if(!%obj.wingTriggerTimeout)
      {
         if(%obj.wingsOut)
         {
            %obj.unmountImage(2);
            %obj.unmountImage(3);
            %obj.unmountImage(6);
            %obj.unmountImage(7);
            %obj.mountImage(TurbocatLeftDecal, 6);
            %obj.mountImage(TurbocatRightDecal, 7);
   
            %obj.play3D(MissileReloadSound);
            %obj.wingsOut = false;
            %obj.wingTriggerTimeout = 500;
            schedule(%obj.wingTriggerTimeout, 0, resolveWingTriggerTimeout, %obj);
         }
         else
         {
            %obj.unmountImage(6);
            %obj.unmountImage(7);
            %obj.mountImage(TurbocatLeftWingDecal, 2);
            %obj.mountImage(TurbocatRightWingDecal, 3);
            %obj.mountImage(TurbocatLeftFoldDecal, 6);
            %obj.mountImage(TurbocatRightFoldDecal, 7);

            %obj.play3D(MissileReloadSound);
            %obj.wingsOut = true;
            %obj.wingTriggerTimeout = 500;
            schedule(%obj.wingTriggerTimeout, 0, resolveWingTriggerTimeout, %obj);
         }
      }
   }
}

function TurboCat::onTrigger(%data, %obj, %trigger, %state)
{
   // trigger = 0 for "fire", 1 for "jump", 3 for "thrust"
   // state = 1 for firing, 0 for not firing
   if(%trigger == 0)
   {
     if(%state)
          TurboCatBoost(%obj);
   }
}
//**************************************************************
// WEAPONS
//**************************************************************

function TurboCatBoost(%obj)
{
   if(isObject(%obj))
   {
      if(%obj.getMountNodeObject(0))
      {
         if((%obj.lastBoostTime + 6000) <= getSimTime())
         {
            resetInfoBar(%obj, 1);

            %nvel = %obj.getVelocity();
            %xvel = velToSingle(%nVel);
            %vel = msToKPH(%xvel);

            if(%vel <= 800)
            {
               %vec = vectorScale(%obj.getMuzzleVector(0), 120); // Adding to the current velocity..
               %force = %obj.getMass();
               %newVec = VectorScale(%vec, %force);
               %obj.applyImpulse(%obj.getTransform(), %newVec);
               %obj.lastBoostTime = getSimTime();
               %obj.play3D(BombExplosionSound);
               %obj.takeDamage(getRandom(1, 2) / 10);
//               bottomPrint(%obj.getMountNodeObject(0).client, "Solid rocket booster ignited! "@%obj.numBoosts@" uses left.", 5, 1);
               spawnProjectile(%obj, LinearFlareProjectile, CCDisplayCharge);
            }
            else
               bottomPrint(%obj.getMountNodeObject(0).client, "You are travelling too fast to use a rocket booster.", 5, 1);
         }
         else
            bottomPrint(%obj.getMountNodeObject(0).client, "Solid rocket booster is reloading.", 5, 1);
      }
   }
}

