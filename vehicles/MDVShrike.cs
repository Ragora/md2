if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

function ScoutFlyer::onTrigger(%data, %obj, %trigger, %state)
{
     commandToClient(%client, 'setAmmoHudCount', %obj.secondaryAmmo );	// +soph
     switch$ ( %trigger )	// rewrote most of this to allow for mine-key reloading -soph
     {
          case 0:
               if(%obj.selectedWeapon == 1)
                    eval($VehicleWeaponClass[%obj.primaryWeaponID]@"::onTrigger("@%data@", "@%obj@", "@%state@");");
               else if(%obj.selectedWeapon == 2)
                    eval($VehicleWeaponClass[%obj.secondaryWeaponID]@"::onTrigger("@%data@", "@%obj@", "@%state@");");
          case 5:
               if( %state )
                    if( %obj.getInventory( %obj.secondaryAmmo ) < %obj.getDatablock().max[%obj.secondaryAmmo] )
                    {
                         if(%obj.getEnergyLevel() >= %data.maxEnergy)
                              %obj.setInventory( %obj.secondaryAmmo , 0 );	
      	                 %data.reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %obj.secondaryAmmo, 8, 0);
                    }
                    else
                         if ( %obj.secondaryWeaponID != $VehicleWeapon::Puma ) 
                              messageClient(%obj.getMountNodeObject(0).client, 'MsgReloadFullEnergy', '\c2Cannot reload; secondary weapon banks already full.');
     }
}

function xScoutFlyer::onTrigger(%data, %obj, %trigger, %state)
{
     if(%trigger == 0)
     {
        if(%obj.lastFireTime + %data.fireTimeout > getSimTime()) // double fire cheat
             return;
             
          if(%obj.selectedWeapon == 1)
          {
               %obj.setImageTrigger(5, false);
               %obj.setImageTrigger(6, false);

               switch(%state)
               {
                   case 0:
                   %obj.fireWeapon = false;
                   %obj.setImageTrigger(2, false);
                   %obj.setImageTrigger(3, false);
                   case 1:
                   %obj.fireWeapon = true;
//                   if(%obj.nextWeaponFire == 2)
//                   {
                         %obj.setImageTrigger(2, true);
                         %obj.setImageTrigger(3, true);
//                   }
//                   else
//                   {
//                         %obj.setImageTrigger(2, false);
//                         %obj.setImageTrigger(3, true);
//                   }
               }
          }
          else
          {
               %obj.setImageTrigger(2, false);
               %obj.setImageTrigger(3, false);

               switch(%state)
               {
                   case 0:
                   %obj.fireWeapon = false;
                   %obj.setImageTrigger(5, false);
                   %obj.setImageTrigger(6, false);
                   case 1:
                   %obj.fireWeapon = true;
                   if(%obj.nextWeaponFire == 5)
                   {
                         %obj.setImageTrigger(5, true);
                         %obj.setImageTrigger(6, false);
                   }
                   else
                   {
                         %obj.setImageTrigger(5, false);
                         %obj.setImageTrigger(6, true);
                   }
               }
          }
     }
}

function ScoutFlyer::playerDismounted(%data, %obj, %player)
{
   %obj.fireWeapon = false;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(3, false);
   %obj.setImageTrigger(5, false);
   %obj.setImageTrigger(6, false);

   if(%player.isCloaked())
      %player.setCloaked(false);

   setTargetSensorGroup(%obj.getTarget(), %obj.team);
}

/* Here's the thing
 * T2 doesn't play nice with batch commenting
 * And I'm too lazy to go about commenting out the rest of this file
 * So  you know what?
 * FUCK YOU.  SYNTAX ERROR!
 */

function ScoutChaingunImage::onFire(%data,%obj,%slot)
{
   // obj = ScoutFlyer object number
   // slot = 2
   if(%obj.lastFireTime + %data.fireTimeout > getSimTime()) // double fire cheat
        return;

   if(%obj.fireThread)
      cancel(%obj.fireThread);

   %obj.lastFireTime = getSimTime();
   Parent::onFire(%data,%obj,%slot);
   %obj.nextWeaponFire = 6;
   %obj.fireThread = schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function ScoutChaingunPairImage::onFire(%data,%obj,%slot)
{
   // obj = ScoutFlyer object number
   // slot = 3

   if(%obj.fireThread)
      cancel(%obj.fireThread);

   %obj.lastFireTime = getSimTime();
   Parent::onFire(%data,%obj,%slot);
   %obj.nextWeaponFire = 5;
   %obj.lastFiredTime = getSimTime();
   %obj.fireThread = schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}							// -[soph]

datablock TurretData(SSTurret) : TurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = ScoutFlyer.maxDamage;
   destroyedLevel          = ScoutFlyer.destroyedLevel;

   thetaMin                = 50;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 0.8;

   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 1;

   targetNameTag           = 'Shrike Belly';
   targetTypeTag           = 'Turret';
};

datablock TurretImageData(SSTurretBarrel)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;

   projectileType = TracerProjectile;
   projectile = MoonBikeEnergyBullet;

   usesEnergy                       = true;
   useCapacitor                     = true;
   useMountEnergy                   = true;
   fireEnergy                       = 8.0;
   minEnergy                        = 8.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   // State transitions
   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "WaitFire1";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = BomberTurretActivateSound;

   stateName[1]                     = "WaitFire1";
   stateTransitionOnTriggerDown[1]  = "Fire1";
   stateTransitionOnNoAmmo[1]       = "NoAmmo1";

   stateName[2]                     = "Fire1";
   stateTransitionOnTimeout[2]      = "Reload1";
   stateTimeoutValue[2]             = 0.05;
   stateFire[2]                     = true;
   stateRecoil[2]                   = LightRecoil;
   stateAllowImageChange[2]         = false;
   stateSequence[2]                 = "Fire";
   stateScript[2]                   = "onFire";
   stateSound[2]                    = BomberTurretFireSound;

   stateName[3]                     = "Reload1";
   stateSequence[3]                 = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateAllowImageChange[3]         = false;
   stateTransitionOnTimeout[3]      = "WaitFire2";
   stateTransitionOnNoAmmo[3]       = "NoAmmo1";

   stateName[4]                     = "NoAmmo1";
   stateTransitionOnAmmo[4]         = "Reload1";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateSequence[4]                 = "NoAmmo";
   stateSequence[4] = "NoAmmo1";

   stateTransitionOnTriggerDown[4]  = "DryFire1";

   stateName[5]                     = "DryFire1";
   stateSound[5]                    = BomberTurretDryFireSound;
   stateTimeoutValue[5]             = 0.5;
   stateTransitionOnTimeout[5]      = "NoAmmo1";

   stateName[6]                     = "WaitFire2";
   stateTransitionOnTriggerDown[6]  = "Fire2";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateTransitionOnNoAmmo[6]       = "NoAmmo";
   stateTransitionOnNoAmmo[6] = "NoAmmo2";

   stateName[7]                     = "Fire2";
   stateTransitionOnTimeout[7]      = "Reload2";
   stateTimeoutValue[7]             = 0.05;
   stateScript[7]                   = "FirePair";

   stateName[8]                     = "Reload2";
   stateSequence[8]                 = "Reload";
   stateTimeoutValue[8]             = 0.1;
   stateAllowImageChange[8]         = false;
   stateTransitionOnTimeout[8]      = "WaitFire1";
   stateTransitionOnNoAmmo[8]       = "NoAmmo2";

   stateName[9]                     = "NoAmmo2";
   stateTransitionOnAmmo[9]         = "Reload2";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateSequence[9]                 = "NoAmmo";
   stateSequence[9] = "NoAmmo2";

   stateTransitionOnTriggerDown[9]  = "DryFire2";

   stateName[10]                     = "DryFire2";
   stateSound[10]                    = BomberTurretDryFireSound;
   stateTimeoutValue[10]             = 0.5;
   stateTransitionOnTimeout[10]      = "NoAmmo2";
};

datablock TurretImageData(SSTurretBarrelPair)
{
   shapeFile                = "turret_belly_barrell.dts";
   mountPoint               = 1;

   projectileType = TracerProjectile;
   projectile = MoonBikeEnergyBullet;

   usesEnergy                       = true;
   useCapacitor                     = true;
   useMountEnergy                   = true;
   fireEnergy                       = 8.0;
   minEnergy                        = 8.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 0.05;
   stateFire[1]                     = true;
   stateRecoil[1]                   = LightRecoil;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
   stateSound[1]                    = BomberTurretFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
};

datablock TurretImageData(SAIAimingTurretBarrel)
{
   shapeFile            = "turret_muzzlepoint.dts";
   mountPoint           = 3;

   projectile           = MoonBikeEnergyBullet;

   // Turret parameters
   activationMS         = 1000;
   deactivateDelayMS    = 1500;
   thinkTimeMS          = 200;
   degPerSecTheta       = 500;
   degPerSecPhi         = 800;

   attackRadius         = 75;
};

function SSTurretBarrel::firePair(%this, %obj, %slot)
{
   %obj.setImageTrigger( 3, true);
}

function SSTurretBarrelPair::stopFire(%this, %obj, %slot)
{
   %obj.setImageTrigger( 3, false);
}

function SSTurret::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function SSTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function SSTurret::onTrigger(%data, %obj, %trigger, %state)
{
   //error("onTrigger: trigger = " @ %trigger @ ", state = " @ %state);
   //error("obj = " @ %obj @ ", class " @ %obj.getClassName());
   switch (%trigger)
   {
      case 0:
         %obj.fireTrigger = %state;
         if(%obj.selectedWeapon == 1)
         {
            %obj.setImageTrigger(4, false);
            if(%obj.getImageTrigger(6))
            {
               %obj.setImageTrigger(6, false);
               ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
            }
            if(%state)
               %obj.setImageTrigger(2, true);
            else
               %obj.setImageTrigger(2, false);
         }
         else if(%obj.selectedWeapon == 2)
         {
            %obj.setImageTrigger(2, false);
            if(%obj.getImageTrigger(6))
            {
               %obj.setImageTrigger(6, false);
               ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
            }
            if(%state)
               %obj.setImageTrigger(4, true);
            else
               %obj.setImageTrigger(4, false);
         }
         else
         {
            %obj.setImageTrigger(2, false);
            %obj.setImageTrigger(4, false);
            if(%state)
               %obj.setImageTrigger(6, true);
            else
            {
               %obj.setImageTrigger(6, false);
               BomberTargetingImage::deconstruct(%obj.getMountedImage(6), %obj);
            }
         }

      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function SSTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(4, false);
   if(%obj.getImageTrigger(6))
   {
      %obj.setImageTrigger(6, false);
      ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
   }
   %client = %obj.getControllingClient();
   %client.player.isBomber = false;
//   %client.player.setControlObject(%client.player);
   %client.player.mountVehicle = false;
//   %client.player.getDataBlock().doDismount(%client.player);
   if(%client.player.getState() !$= "Dead")
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
}