if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//==============================================================
// MISCELLANEOUS VEHICLE SOUNDS AND EFFECTS
//
// - Sounds
// - Water Splash Effects
// - Tire Dust Particles
// - Lift-Off Particles
// - Small Heavy Damage Smoke
// - Heavy Damage Smoke
// - Light Heavy Damage Smoke
// - Vehicle Smoke Spike (for debris)
// - Vehicle Medium Explosion Smoke
// - Vehicle Large Ground Explosion Smoke
// - Debris Fire Particles
// - Debris Smoke Particles
// - Debris (pieces)
// - Explosions (including Shield Impacts)
// - Vehicle Sensors
// - Flier Contrails
// - Bomb Explosions
//
//==============================================================

//--------------------------------------------------------------
// SOUNDS
//--------------------------------------------------------------

datablock AudioProfile(SoftImpactSound)
{
   filename    = "fx/vehicles/crash_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(HardImpactSound)
{
   filename    = "fx/vehicles/crash_hard.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(GravSoftImpactSound)
{
   filename    = "fx/vehicles/crash_grav_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BombExplosionSound)
{
   filename    = "fx/vehicles/bomber_bomb_impact.wav";
   description = AudioBomb3d;
   preload = true;
};


//--------------------------------------------------------------
// WATER SPLASH EFFECTS
//--------------------------------------------------------------

datablock ParticleData(VehicleFoamParticle)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 400;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 4;
   sizes[2]      = 6;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(VehicleFoamEmitter)
{
   ejectionPeriodMS = 40;
   periodVarianceMS = 0;
   ejectionVelocity = 10.0;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "VehicleFoamParticle";
};


datablock ParticleData( VehicleFoamDropletsParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 800;
   lifetimeVarianceMS   = 300;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 8;
   sizes[1]      = 3;
   sizes[2]      = 0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( VehicleFoamDropletsEmitter )
{
   ejectionPeriodMS = 34;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   particles = "VehicleFoamDropletsParticle";
};
   
//--------------------------------------------------------------
// TIRE DUST PARTICLES
//--------------------------------------------------------------
 
datablock ParticleData(TireParticle)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.1;
   inheritedVelFactor   = 0.1;
   constantAcceleration = 0.0;
   lifetimeMS           = 6000;
   lifetimeVarianceMS   = 1000;
   useInvAlpha          = true;
   spinRandomMin        = -45.0;
   spinRandomMax        = 45.0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 0.6";
   colors[1]     = "0.46 0.46 0.36 0.0";
   sizes[0]      = 1.5;
   sizes[1]      = 7.00;
};

datablock ParticleEmitterData(TireEmitter)
{
   ejectionPeriodMS = 160;
   periodVarianceMS = 20;
   ejectionVelocity = 2;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 20;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   useEmitterColors = true;
   particles = "TireParticle";
};

//--------------------------------------------------------------
// LIFTOFF PARTICLES
//--------------------------------------------------------------

datablock ParticleData(TankDust)
{
   dragCoefficient      = 1.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 0.0";
   colors[1]     = "0.46 0.46 0.36 0.4";
   colors[2]     = "0.46 0.46 0.36 0.0";
   sizes[0]      = 3.2;
   sizes[1]      = 4.6;
   sizes[2]      = 7.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(TankDustEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 0;
   ejectionVelocity = 20.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 90;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   useEmitterColors = true;
   particles = "TankDust";
};

datablock ParticleData(LargeVehicleLiftoffDust)
{
   dragCoefficient      = 1.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 150;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 1.0";
   colors[1]     = "0.46 0.46 0.36 1.0";
   colors[2]     = "0.46 0.46 0.36 1.0";
   sizes[0]      = 3.2;
   sizes[1]      = 4.6;
   sizes[2]      = 7.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(LargeVehicleLiftoffDustEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;
   ejectionVelocity = 15.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 90;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   useEmitterColors = true;
   particles = "LargeVehicleLiftoffDust";
};

datablock ParticleData(VehicleLiftoffDust)
{
   dragCoefficient      = 1.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 150;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 1.0";
   colors[1]     = "0.46 0.46 0.36 1.0"; // 0.4
   colors[2]     = "0.46 0.46 0.36 1.0";
   sizes[0]      = 1.2;
   sizes[1]      = 2.6;
   sizes[2]      = 3.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(VehicleLiftoffDustEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;
   ejectionVelocity = 10.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 90;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   useEmitterColors = true;
   particles = "VehicleLiftoffDust";
};

//--------------------------------------------------------------
// Damage bubbles
//--------------------------------------------------------------
datablock ParticleData(DamageBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.04;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "special/bubbles";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.2;
   sizes[1]      = 0.8;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DamageBubbles)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "DamageBubbleParticle";
};

//--------------------------------------------------------------
// SMALL HEAVY DAMAGE SMOKE
//--------------------------------------------------------------

datablock ParticleData(SmallHeavyDamageSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.2;
   sizes[1]      = 0.8;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SmallHeavyDamageSmoke)
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "SmallHeavyDamageSmokeParticle";
};

//--------------------------------------------------------------
// HEAVY DAMAGE SMOKE
//--------------------------------------------------------------

datablock ParticleData(HeavyDamageSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.3 0.3 0.3 0.7";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.2;
   sizes[1]      = 2.6;
   sizes[2]      = 4.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(HeavyDamageSmoke)
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 6;
   ejectionVelocity = 4.0;
   velocityVariance = 0.5;
   ejectionOffset   = 1.5;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "HeavyDamageSmokeParticle";
};

//--------------------------------------------------------------
// SMALL LIGHT DAMAGE SMOKE
//--------------------------------------------------------------

datablock ParticleData(SmallLightDamageSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.5 0.5 0.5 0.4";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 1.6;
   sizes[2]      = 3.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SmallLightDamageSmoke)
{
   ejectionPeriodMS = 40;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "SmallLightDamageSmokeParticle";
};

//--------------------------------------------------------------
// LIGHT DAMAGE SMOKE
//--------------------------------------------------------------

datablock ParticleData(LightDamageSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.5;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.5 0.5 0.5 0.7";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1.2;
   sizes[1]      = 2.6;
   sizes[2]      = 4.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(LightDamageSmoke)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 6;
   ejectionVelocity = 4.0;
   velocityVariance = 0.5;
   ejectionOffset   = 1.5;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "LightDamageSmokeParticle";
};

//--------------------------------------------------------------
// VEHICLE SMOKE SPIKE (for debris)
//--------------------------------------------------------------

datablock ParticleData( VSmokeSpike )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;  
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.4 0.4 0.4 1.0";
   colors[1]     = "0.3 0.3 0.3 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 1.0;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( VSmokeSpikeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "VSmokeSpike";
};


//--------------------------------------------------------------
// VEHICLE MEDIUM EXPLOSION SMOKE
//--------------------------------------------------------------

datablock ParticleData(VehicleMESmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.5;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 6.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(VehicleMESmokeEMitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;

   ejectionVelocity = 6.25;
   velocityVariance = 0.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "VehicleMESmoke";
};

//--------------------------------------------------------------
// VEHICLE LARGE GROUND EXPLOSION SMOKE
//--------------------------------------------------------------

datablock ParticleData(VehicleLGESmoke)
{
   dragCoeffiecient     = 0.3;
   gravityCoefficient   = -0.5;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.5 0.5";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 6.0;
   sizes[1]      = 6.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(VehicleLGESmokeEMitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;

   ejectionVelocity = 7.25;
   velocityVariance = 0.25;

   ejectionOffset = 5.0;

   thetaMin         = 0.0;
   thetaMax         = 80.0;

   lifetimeMS       = 350;

   particles = "VehicleLGESmoke";
};

//--------------------------------------------------------------
// DEBRIS FIRE PARTICLES
//--------------------------------------------------------------

datablock ParticleData(DebrisFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DebrisFireEmitter)
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 1;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "DebrisFireParticle";
};

//--------------------------------------------------------------
// DEBRIS SMOKE PARTICLES
//--------------------------------------------------------------

datablock ParticleData( DebrisSmokeParticle )
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;  
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 3;
   sizes[2]      = 5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( DebrisSmokeEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = true;

   particles = "DebrisSmokeParticle";
};

//--------------------------------------------------------------
// DEBRIS (pieces)
//--------------------------------------------------------------

datablock AudioProfile(VehicleExplosionSound)
{
   filename = "fx/explosions/vehicle_explosion.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ExplosionData(DebrisExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile = plasmaExpSound;
   faceViewer = true;
   explosionScale = "0.4 0.4 0.4";
};

datablock ExplosionData(PartsExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile = discExpSound;
   faceViewer = true;
   explosionScale = "0.4 0.4 0.4";
};

datablock DebrisData( VehicleFireballDebris )
{
   emitters[0] = DebrisSmokeEmitter;
   emitters[1] = DebrisFireEmitter;

   explosion = DebrisExplosion;
   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 100.0;
   lifetimeVariance = 30.0;

   numBounces = 0;
   bounceVariance = 0;
};             

datablock DebrisData( VSpikeDebris )
{
   emitters[0] = VSmokeSpikeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.3;
   lifetimeVariance = 0.02;
};

datablock DebrisData( ShapeDebris )
{
   explosion = PartsExplosion;
   explodeOnMaxBounce = true;
   emitters[0] = DebrisSmokeEmitter;

   elasticity = 0.40;
   friction = 0.5;

   lifetime = 25.0;
   lifetimeVariance = 0.0;

   minSpinSpeed = 60;
   maxSpinSpeed = 600;

   numBounces = 4;
   bounceVariance = 2;

//   staticOnMaxBounce = true;

   useRadiusMass = true;
   baseRadius = 1.0;

   velocity = 17.0;
   velocityVariance = 7.0;
};             

datablock ShockwaveData(VehicleShockwave)
{
   width = 12.5;
   numSegments = 20;
   numVertSegments = 4;
   velocity = 10;
   acceleration = 35.0;
   lifetimeMS = 2500;
   height = 3.0;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.8 0.8 0.8 1.0";
   colors[1] = "0.8 0.5 0.2 0.6";
   colors[2] = "1.0 0.5 0.5 0.0";
};

datablock ShockwaveData(AnShockwave)
{
   width = 15;
   numSegments = 32;
   numVertSegments = 8;
   velocity = 48;
   acceleration = 50.0;
   lifetimeMS = 1800;
   height = 5.0;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.8 0.6 1.0";
   colors[1] = "0.8 0.5 0.2 0.6";
   colors[2] = "1.0 0.3 0.3 0.0";
};

datablock ParticleData( VehicleCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.357;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 1.2;
   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 100;
   textureName          = "special/crescent3";
   colors[0]     = "0.7 0.7 1.0 1.0";
   colors[1]     = "0.7 0.7 1.0 0.5";
   colors[2]     = "0.7 0.7 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 4.0;
   sizes[2]      = 8.0;
   times[0]      = 0.0;
   times[1]      = 1.0;
   times[2]      = 2.0;
};

datablock ParticleEmitterData( VehicleCrescentEmitter )
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "VehicleCrescentParticle";
};

//**************************************************************
// EXPLOSIONS
//**************************************************************

datablock ExplosionData(VSpikeExplosion)
{
   debris = VSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 15;
   debrisNumVariance = 6;
   debrisVelocity = 30.0;
   debrisVelocityVariance = 7.0;
};

datablock ExplosionData(VehicleExplosion)
{
   explosionShape = "mortar_explosion.dts";
   playSpeed = 0.5;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;
   sizes[0] = "7 7 7";
   sizes[1] = "7 7 7";
   times[0] = 0.0;
   times[1] = 1.0;

   emitter[0] = VehicleMESmokeEmitter;
   emitter[1] = VehicleCrescentEmitter;
   shockwave = VehicleShockwave;
   
   debris = VehicleFireballDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 80;
   debrisNum = 8;
   debrisNumVariance = 4;
   debrisVelocity = 20.0;
   debrisVelocityVariance = 5.0;

   subExplosion = VSpikeExplosion;

   shakeCamera = true;
   camShakeFreq = "11.0 13.0 9.0";
   camShakeAmp = "40.0 40.0 40.0";
   camShakeDuration = 0.7;
   camShakeRadius = 25.0;
};

datablock ExplosionData(VLSpikeExplosion)
{
   debris = VSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 10;
   debrisNumVariance = 6;
   debrisVelocity = 50.0;
   debrisVelocityVariance = 10.0;
};

datablock ExplosionData(VLGSpikeExplosion)
{
   debris = VSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 70;
   debrisNum = 10;
   debrisNumVariance = 6;
   debrisVelocity = 50.0;
   debrisVelocityVariance = 10.0;
};

datablock ExplosionData(LargeGroundVehicleExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.5;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;
   sizes[0] = "3 3 3";
   sizes[1] = "3 3 3";
   times[0] = 0.0;
   times[1] = 1.0;

   emitter[0] = VehicleLGESmokeEMitter;
   emitter[1] = VehicleCrescentEmitter;
   shockwave = VehicleShockwave;

   debris = VehicleFireballDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 60;
   debrisNum = 10;
   debrisNumVariance = 5;
   debrisVelocity = 20.0;
   debrisVelocityVariance = 5.0;

   subExplosion = VLGSpikeExplosion;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 30.0;
};

datablock ExplosionData(DiscVehicleExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.5;
   soundProfile = discExpSound;
   faceViewer = true;
   sizes[0] = "20 20 20";
   sizes[1] = "20 20 20";
   times[0] = 0.0;
   times[1] = 1.0;

   emitter[0] = VehicleLGESmokeEMitter;
   emitter[1] = VehicleCrescentEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 30.0;
};

datablock ExplosionData(LargeAirVehicleExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.5;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;

   sizes[0] = "24 24 24";
   sizes[1] = "24 24 24";
   times[0] = 0.0;
   times[1] = 1.0;

   emitter[0] = VehicleLGESmokeEMitter;
   emitter[1] = VehicleCrescentEmitter;
   shockwave = VehicleShockwave;

   debris = VehicleFireballDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 80;
   debrisNum = 12;
   debrisNumVariance = 6;
   debrisVelocity = 20.0;
   debrisVelocityVariance = 5.0;

   subExplosion = VLSpikeExplosion;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 30.0;
};

//--------------------------------------------------------------
// BOMB EXPLOSION
//--------------------------------------------------------------
datablock ParticleData(VehicleBombExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 1050;
   lifetimeVarianceMS   = 250;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 3;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(VehicleBombExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 32;
   velocityVariance = 10;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "VehicleBombExplosionParticle";
};

datablock ShockwaveData(VehicleBombShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 16.0;
   acceleration = 40.0;
   lifetimeMS = 650;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.8 0.4 0.50";
   colors[1] = "1.0 0.6 0.3 0.25";
   colors[2] = "1.0 0.4 0.2 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ExplosionData(VehicleBombSubExplosion1)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;

};             

datablock ExplosionData(VehicleBombSubExplosion2)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 7.0;

   playSpeed = 1.0;

   sizes[0] = "3.0 3.0 3.0";
   sizes[1] = "4.0 4.0 4.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(VehicleBombSubExplosion3)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 0;

   offset = 0.0;

   playSpeed = 0.7;


   sizes[0] = "3.0 3.0 3.0";
   sizes[1] = "7.0 7.0 7.0";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(VehicleBombExplosion)
{
   soundProfile   = BombExplosionSound;
   particleEmitter = VehicleBombExplosionEmitter;
   particleDensity = 250;
   particleRadius = 1.25;
   faceViewer = true;

   shockwave = VehicleBombShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = VehicleBombSubExplosion1;
   subExplosion[1] = VehicleBombSubExplosion2;
   subExplosion[2] = VehicleBombSubExplosion3;

   shakeCamera = true;
   camShakeFreq = "5.0 7.0 5.0";
   camShakeAmp = "80.0 80.0 80.0";
   camShakeDuration = 1.0;
   camShakeRadius = 30.0;
};

datablock SeekerProjectileData(BomberCruiseMissile)
{
   scale = "10.0 10.0 10.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_grav_scout.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 3.5;
   damageRadius        = 40.0;
   radiusDamageType    = $DamageType::BomberBombs;
   kickBackStrength    = 5500;

   explosion           = "VehicleBombExplosion";
   underwaterExplosion = VehicleBombExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 200 ;		// = 100; -soph
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 30000 ;	// 20000; epic range -soph
   muzzleVelocity      = 30.0;
   maxVelocity         = 500 ;		// -[soph] 200.0; won't reach this speed 
   turningSpeed        = 18 ;		// - 60.0; flying bomb
   acceleration        = 6.0 ;		// -[/soph] 10.0; slow and steady 

   proximityRadius     = 7 ;		// = 4; -soph

   terrainAvoidanceSpeed         = 30 ;	// -[soph] = 180;
   terrainScanAhead              = 55 ;	// - = 25;
   terrainHeightFail             = 25 ;	// - = 12;
   terrainAvoidanceRadius        = 99 ;	// -[/soph] = 100;

   flareDistance = 1 ;			// 200; -soph
   flareAngle    = 1 ;			// 30; -soph

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = true ;		// = false; -soph
   flechetteDelayMs = 250 ;		// = 50; -soph
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};


function BomberCruiseMissile::onExplode( %data , %proj , %pos , %mod ) 	// +[soph]
{									// + from gl logic
	%proj.exploded = true ;						// +
									// +
	if( %proj.bkSourceObject )					// +
		if( isObject( %proj.bkSourceObject ) )			// +
			%proj.sourceObject = %proj.bkSourceObject ;	// +
									// +
	%modifier = 1 ;							// +
									// +
	if( %proj.vehicleMod > 0 )					// +
		%modifier *= %proj.vehicleMod ;				// +
									// +
	if( %proj.damageMod > 0 )					// +
		%modifier *= %proj.damageMod ;				// +
          								// +
	if( %data.passengerDamageMod > 0 && %proj.passengerCount )	// +
 		%modifier *= 1 + ( %proj.passengerCount * %data.passengerDamageMod ) ;
									// +
	if( %proj.rangeMod > 0 )					// +
		%rangeMod = %proj.rangeMod ;				// +
	else								// +
		%rangeMod = 1 ;						// +
									// +
	EMPBurstExplosion( %proj , %pos , %data.damageRadius * %rangeMod / 2 , %data.indirectDamage * %modifier / 12 , 0 , %proj.sourceObject , $DamageType::EMP ) ;
	RadiusExplosion( %proj , %pos , %data.damageRadius * %rangeMod , %data.indirectDamage * %modifier , %data.kickBackStrength , %proj.sourceObject , %data.radiusDamageType ) ;
}									// +[/soph]


//--------------------------------------------------------------
// VEHICLE SENSORS
//--------------------------------------------------------------

datablock SensorData(VehiclePulseSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = false;
   detectsActiveJammed = false;
   detectsCloaked = false;
   detectionPings = true;
   detectRadius = 20;
};

datablock SensorData(AWACPulseSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = true;
   detectsActiveJammed = false;
   detectsCloaked = true;
   detectionPings = true;
   detectRadius = 425;
};

//--------------------------------------------------------------
// FLIER CONTRAILS
//--------------------------------------------------------------

datablock ParticleData(ContrailParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 3200;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.6 0.6 0.6 0.5";
   colors[1]     = "0.2 0.2 0.2 0";
   sizes[0]      = 0.6;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(ContrailEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 4;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "ContrailParticle";
};

datablock ParticleData(TurboJetParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 200;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 4;
   sizes[1]      = 8;
   sizes[2]      = 12;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(TurboJetEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TurboJetParticle";
};

datablock ParticleData(FlyerJetParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.6";
   colors[1]     = "0.3 0.3 0.5 0";
   sizes[0]      = 2;
   sizes[1]      = 6;
};

datablock ParticleEmitterData(FlyerJetEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "FlyerJetParticle";
};

//--------------------------------------------------------------
// Hover Jet Emitters
//--------------------------------------------------------------

datablock ParticleData(TankJetParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.6";
   colors[1]     = "0.3 0.3 0.5 0";
   sizes[0]      = 2;
   sizes[1]      = 6;
};

datablock ParticleEmitterData(TankJetEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 1.0;
   ejectionOffset   = 0.25;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TankJetParticle";
};

datablock ParticleData(WildcatJetParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 100;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.6";
   colors[1]     = "0.3 0.3 0.5 0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(WildcatJetEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 1.0;
   ejectionOffset   = 0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "WildcatJetParticle";
};


//--------------------------------------------------------------
// Vehicle Splash Sounds
//--------------------------------------------------------------
//EXIT WATER
datablock AudioProfile(VehicleExitWaterSoftSound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(VehicleExitWaterMediumSound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(VehicleExitWaterHardSound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

//IMPACT WATER
datablock AudioProfile(VehicleImpactWaterSoftSound)
{
   filename    = "fx/armor/general_water_smallsplash2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(VehicleImpactWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(VehicleImpactWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

//WATER WAKE
datablock AudioProfile(VehicleWakeSoftSplashSound)
{
   filename    = "fx/vehicles/wake_wildcat.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(VehicleWakeMediumSplashSound)
{
   filename    = "fx/vehicles/wake_shrike_n_tank.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(VehicleWakeHardSplashSound)
{
   filename    = "fx/vehicles/wake_shrike_n_tank.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(AnFly)
{
   filename    = "fx/vehicles/htransport_boost.wav";
   description = AudioDefaultLooping3d;
};

datablock ParticleData(AnExplosionSmoke)
{
   dragCoeffiecient     = 1;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 7500;
   lifetimeVarianceMS   = 7499;

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.7 0.7 0.7 0.65";
   colors[2]     = "0.25 0.25 0.25 0.375";
   colors[3]     = "0.1 0.1 0.1 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 12.0;
   sizes[2]      = 15.0;
   sizes[3]      = 19.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(AnSmokeEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;

   ejectionVelocity = 16;
   velocityVariance = 4;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 500;

   particles = "AnExplosionSmoke";
};

datablock ParticleData(AnSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 15;
   lifetimeMS           = 3500; //1500
   lifetimeVarianceMS   = 500;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AnSparkEmitter)
{
   ejectionPeriodMS = 1; //1
   periodVarianceMS = 0;
   ejectionVelocity = 5; //16
   velocityVariance = 0;
   ejectionOffset   = 0.0;
   thetaMin         = 90;
   thetaMax         = 90; //180
   phiReferenceVel  = 0;
   phiVariance      = 360; //360
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 425; //750
   particles = "AnSparks";
   //ejectionPeriodMS = 1;  //meh..testing
   //periodVarianceMS = 0;
   //ejectionVelocity = 32;
   //velocityVariance = 12;
   //ejectionOffset   = 1.0;
   //thetaMin         = 0;
   //thetaMax         = 180;
   //phiReferenceVel  = 0;
   //phiVariance      = 360;
   //overrideAdvances = false;
   //orientParticles  = true;
   //lifetimeMS       = 1500;
   //particles = "AnSparks";
};

datablock ParticleData( AnDebrisSmokeParticle )
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.05;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.1 0.1 0.1 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 4;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( AnDebrisSmokeEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = false;

   particles = "AnDebrisSmokeParticle";
};

datablock DebrisData(AnFireballDebris)
{
   emitters[0] = AnDebrisSmokeEmitter;
   emitters[1] = DebrisFireEmitter;

   explosion = DebrisExplosion;
   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 100.0;
   lifetimeVariance = 50.0;

   numBounces = 1;
   bounceVariance = 1;
};

datablock ShockwaveData(AnUWShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 24.0;
   acceleration = 40.0;
   lifetimeMS = 1250;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 1.0";
   colors[1] = "0.25 0.2 0.9 0.35";
   colors[2] = "0 0.4 0.75 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(ANSubExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 0;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
   subExplosion = VLGSpikeExplosion;
};

datablock ExplosionData(ANUWSubExplosion)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
   subExplosion = VLGSpikeExplosion;
};

datablock ExplosionData(AnExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.1;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;

   sizes[0] = "30 30 30";
   sizes[1] = "30 30 30";
   times[0] = 0.0;
   times[1] = 1.0;

   emitter[0] = AnSmokeEmitter;
   emitter[2] = AnSparkEmitter;
   shockwave = AnShockwave;

   debris = AnFireballDebris;
   debrisThetaMin = 0;
   debrisThetaMax = 80;
   debrisNum = 30;
   debrisNumVariance = 10;
   debrisVelocity = 40.0;
   debrisVelocityVariance = 15.0;

   subExplosion[0] = ANSubExplosion;

   shakeCamera = true;
   camShakeFreq = "10.0 10.0 10.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 40.0;
};

datablock ExplosionData(AnUWExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.1;
   soundProfile = VehicleExplosionSound;
   faceViewer = true;

   sizes[0] = "30 30 30";
   sizes[1] = "30 30 30";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = ANUWSubExplosion;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "140.0 140.0 140.0";
   camShakeDuration = 1.3;
   camShakeRadius = 30.0;
};

datablock LinearFlareProjectileData(MitziAnnihalator)
{
   scale               = "50.0 50.0 50.0";
   faceViewer          = true;
   directDamage        = 0;
   directDamageType    = $DamageType::Annihalator;
   hasDamageRadius     = true;
   indirectDamage      = 7.5;
   damageRadius        = 45;
   kickBackStrength    = 32500;
   radiusDamageType    = $DamageType::Annihalator;

   explosion           = "AnExplosion";
   underwaterExplosion = "AnUWExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 30.0;
   wetVelocity       = 10.0;
   velInheritFactor  = 0.01;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   size[0]           = 1;
   size[1]           = 70;
   size[2]           = 25;
   numFlares         = 50;
   flareColor        = "0.5 0.5 0.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/blasterboltcross.png";

   sound      = AnFly;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = false;
   lightRadius = 8.0;
   lightColor  = "0.7 0.7 0.2";
};

datablock ExplosionData(TFlakMortarExplosion)
{
   soundProfile   = BombExplosionSound;

   particleEmitter = VehicleBombExplosionEmitter;
   particleDensity = 250;
   particleRadius = 1.25;
   faceViewer = true;

   sizes[0] = "9 9 9";
   sizes[1] = "9 9 9";
   times[0] = 0.0;
   times[1] = 1.0;

   shockwave = ImpactMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[3] = VehicleBombSubExplosion1;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = ImpactCrescentEmitter;

   debris = VSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 15;
   debrisNumVariance = 6;
   debrisVelocity = 30.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock LinearProjectileData(MFDiscProjectile)
{
   projectileShapeName = "disc.dts";
   emitterDelay        = -1;
   directDamageType    = $DamageType::MultiFusor;
   directDamage        = 0.5;	// 0.9; -soph
   hasDamageRadius     = true;
   indirectDamage      = 0.2;	// 0.0; -soph
   damageRadius        = 5;
   radiusDamageType    = $DamageType::MultiFusor;
   kickBackStrength    = 825;

   sound 				= discProjectileSound;
   explosion           = "DiscSExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 210;
   wetVelocity       = 150;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 4000;

   activateDelayMS = 32;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock AudioProfile(BomberTurretFireSound)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock ShockwaveData(AutoCannonImpactShockwave)
{
   width = 0.5;
   numSegments = 24;
   numVertSegments = 24;
   velocity = 5;
   acceleration = 5;
   lifetimeMS = 500;
   height = 0.1;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ParticleData(AutoCannonDebrisParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  100;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = "0.8 0.8 0.8 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.225;
   sizes[1]      = 0.3;
   sizes[2]      = 0.325;
   times[0]      = 0.0;
   times[1]      = 0.25;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AutoCannonDebrisEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;
   thetaMax         = 40.0;
   particles = "AutoCannonDebrisParticle";
};

datablock DebrisData(AutoCannonDebris)
{
   emitters[0] = AutoCannonDebrisEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.75;
   lifetimeVariance = 0.25;

   numBounces = 1;
};

datablock ParticleData(AutoCannonSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 0.4;
   sizes[2]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AutoCannonSparkEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 4;
   ejectionVelocity = 8;
   velocityVariance = 4.0;
   thetaMax         = 80;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "AutoCannonSparks";
};

datablock ExplosionData(AutoCannonExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 2.0;
   //soundProfile = ChaingunImpact;
   faceViewer = true;

   shockwave = AutoCannonImpactShockwave;
   emitter[0] = AutoCannonSparkEmitter;

   debris = AutoCannonDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 82;
   debrisNum = 3;
   debrisVelocity = 6.0;
   debrisVelocityVariance = 2.0;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock TracerProjectileData(AutoCannonBullet)
{
   projectileShapeName = "chaingun_shot.dts";
   doDynamicClientHits = true;

   directDamage        = 0.0;	// = 0.125; -soph
   hasDamageRadius     = true;
   indirectDamage      = 0.15;	// = 0.055; -soph
   damageRadius        = 3;
   radiusDamageType    = $DamageType::AutoCannon;
   kickBackStrength    = 25;

   explosion           = "AutoCannonExplosion";
   underwaterExplosion = "AutoCannonExplosion";
   splash              = ChaingunSplash;
   sound 			     = ChaingunProjectile;

   dryVelocity       = 280;	// 200.0; -soph
   wetVelocity       = 70;	// 50.0; -soph
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2500;	// = 3000; -soph
   lifetimeMS        = 2500;	// = 3000; -soph
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 500;

//   baseEmitter     = GaussBulletEmitter;
   tracerLength    = 9.11;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/blasterBolt";
	tracerTex[1]  	 = "special/blasterBoltCross";
	tracerWidth     = 0.2;
   crossSize       = 0.421;
   crossViewAng    = 0.7;
   renderCross     = true;
};

function AutoCannonBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   if(detectIsWet(%position))
      serverPlay3d(getRandomChaingunWetSound(), %position);
   else
      serverPlay3d(getRandomChaingunSound(), %position);

   Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
   %projectile.exploded = true;	// tag for mech autocannon use -soph
}

function AtCCleanup( %projectile )
{
   if ( isObject( %projectile ) && !%projectile.exploded ) 
      %projectile.delete();
}

function MAutoCannonBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   if(detectIsWet(%position))
      serverPlay3d(getRandomChaingunWetSound(), %position);
   else
      serverPlay3d(getRandomChaingunSound(), %position);

   Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

datablock TracerProjectileData(AutoCannonAPBullet)
{
   projectileShapeName = "chaingun_shot.dts";
//   doDynamicClientHits = true;

   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 3.0;
   radiusDamageType    = $DamageType::AutoCannonAP;
   kickBackStrength    = 150;

   explosion           = "GaussExplosion";
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
   splash              = ChaingunSplash;
   sound 			   = ChaingunProjectile;

   dryVelocity       = 180.0;	// 300.0; -soph
   wetVelocity       = 120.0;	// 200.0; -soph
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   baseEmitter     = GaussBulletEmitter;
   tracerLength    = 4;	// 5; -soph
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
};

function AutoCannonAPBullet::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{
     Parent::onExplode( %data , %proj , %pos , %mod ) ;
     IncendiaryExplosion( %pos , %data.damageradius , 0.5 , %proj.sourceObject , true ) ;
}									// +[/soph]

datablock TracerProjectileData(TurretChaingunBullet)
{
   doDynamicClientHits = true;

   directDamage        = 0.12 ;		// = 0.1; -soph
   directDamageType    = $DamageType::Bullet;
   explosion           = "VulcanExplosion";
   underwaterExplosion = "ChaingunUnderwaterExplosion";
   splash              = ChaingunSplash;
   splash              = ChaingunSplash;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 350.0;
   wetVelocity       = 200.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 750;
   lifetimeMS        = 750;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 750;

   tracerLength    = 10;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 221.0/255.0 @ " " @ 225.0/255.0 @ " " @ 130.0/255.0 @ " 0.925";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.15;
   crossSize       = 0.20;
   crossViewAng    = 0.91;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

function TurretChaingunBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   if(detectIsWet(%position))
      serverPlay3d(getRandomChaingunWetSound(), %position);
   else
      serverPlay3d(getRandomChaingunSound(), %position);

   Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------

datablock DebrisData(AutoShellDebris)
{
   shapeName = "weapon_missile_casement.dts";

   lifetime = 5.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

datablock AudioProfile(AssaultChaingunFireSound)
{
   filename    = "fx/vehicles/tank_chaingun.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock ParticleData(ExtendedAAExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   olors[0]     = "0.8 0.5 0.5 0.5";
   colors[1]     = "0.7 0.4 0.4 0.8";
   colors[2]     = "0.6 0.3 0.3 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ExtendedAAExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 70;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ExtendedAAExplosionParticle1";
};

datablock ParticleData(ExtendedAAExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   olors[0]     = "0.8 0.5 0.5 0.5";
   colors[1]     = "0.7 0.4 0.4 0.8";
   colors[2]     = "0.6 0.3 0.3 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ExtendedAAExplosionEmitter2)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "ExtendedAAExplosionParticle2";
};

datablock ExplosionData(ExtendedAAExplosion)
{
   soundProfile   = blasterExpSound;
   emitter[0]     = ExtendedAAExplosionEmitter;
   emitter[1]     = ExtendedAAExplosionEmitter2;
};

datablock TracerProjectileData(MoonBikeEnergyBullet)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.19 ;	// = 0.225; -soph
   kickBackStrength    = 0;
   directDamageType    = $DamageType::AATurret;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;

   dryVelocity       = 675.0;
   wetVelocity       = 337.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 700;
   lifetimeMS        = 750;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "0.1 0.1 1.0 0.75";
	tracerTex[0]  	 = "special/blueSpark";//"special/tracer00";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.8;
   crossSize       = 1.9;
   crossViewAng    = 0.7;
   renderCross     = true;
   emap = true;
};

datablock TracerProjectileData(KMPlusBullet)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.4;	// 0.3; -soph
   kickBackStrength    = 0;
   directDamageType    = $DamageType::AATurret;
   explosion           = "ExtendedAAExplosion";
   splash              = ChaingunSplash;

   dryVelocity       = 700.0;
   wetVelocity       = 400.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 700;
   lifetimeMS        = 750;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 350;

   tracerLength    = 22;
   tracerAlpha     = true;
   tracerMinPixels = 3;
   tracerColor     = "1.0 0.1 0.1 0.75";
	tracerTex[0]  	 = "special/blasterBolt";//"special/tracer00";
	tracerTex[1]  	 = "small_circle";//"special/tracercross";
   tracerWidth     = 0.45;
   crossSize       = 2.15;
   crossViewAng    = 0.1;
   renderCross     = true;
   emap = true;
};

datablock StaticShapeData(WarpBubble)
{
   shapeFile = "grenade_flare.dts";
};

datablock ExplosionData(MBSingularityExplosion)  : FireballAtmosphereBoltExplosion
{
   shakeCamera = false;
};

datablock ParticleData(MiniStreakParticle)
{
   dragCoefficient      = 1.25;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 500;
   textureName          = "particleTest";
   colors[0]     = "1 1 1 1";
   colors[1]     = "0.1 0.1 0.1 0";
   sizes[0]      = 1.0;
   sizes[1]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(MiniStreakEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 20;
   thetaMax         = 20;
   phiReferenceVel  = 720;
   phiVariance      = 0;
   overrideAdvances = false;
   particles = "MiniStreakParticle";
};

datablock LinearFlareProjectileData(MitziSingularityBlast)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "3.0 3.0 3.0";
   faceViewer          = true;
   directDamageType    = $DamageType::MB;
   directDamage        = 0.3;
   hasDamageRadius     = true;
   indirectDamage      = 0.275;
   damageRadius        = 7.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::MB;

   explosion           = "MBSingularityExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MiniStreakEmitter; //ContrailEmitter;

   dryVelocity       = 180.0; // -nite- was 150
   wetVelocity       = 300.0;       // faster uw
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 15000;
   lifetimeMS        = 15000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.8 0.9";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

datablock TracerProjectileData(VehicleSpikeBolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.0;
   directDamageType    = $DamageType::OutdoorDepTurret;
   hasDamageRadius     = true;
   indirectDamage      = 0.4;
   damageRadius        = 4.0;
   kickBackStrength    = 0.0;
   radiusDamageType    = $DamageType::OutdoorDepTurret;
   sound          	   = BlasterProjectileSound;
   explosion           = PlasmaBoltExplosion;

   dryVelocity       = 200.0;
   wetVelocity       = 100.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 6000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = 100;

   tracerLength    = 5;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

datablock TracerProjectileData(SpikeRifleBolt)
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.05;
   directDamageType    = $DamageType::OutdoorDepTurret;
   hasDamageRadius     = true;
   indirectDamage      = 0.45;
   damageRadius        = 4.5;
   kickBackStrength    = 3000.0;
   radiusDamageType    = $DamageType::OutdoorDepTurret;
   sound          	   = BlasterProjectileSound;
   explosion           = PlasmaBoltExplosion;

   dryVelocity       = 500.0;
   wetVelocity       = 275.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   tracerLength    = 10;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

datablock ParticleData(PPCParticle)
{
   dragCoefficient      = 1.25;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 500;
   textureName          = "special/BlueImpact";
   colors[0]     = "0.3 0.3 1 1";
   colors[1]     = "0.1 0.1 0.1 0";
   sizes[0]      = 1.0;
   sizes[1]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(PPCEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 20;
   thetaMax         = 20;
   phiReferenceVel  = 720;
   phiVariance      = 0;
   overrideAdvances = false;
   particles = "PPCParticle";
};

datablock ExplosionData(PPCExplosion)  : FireballAtmosphereBoltExplosion
{
   shakeCamera = false;
};

datablock LinearFlareProjectileData(PPCBolt)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "3.0 3.0 3.0";
   faceViewer          = true;
   directDamageType    = $DamageType::PPC;
   directDamage        = 3.0;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 7.0;
   kickBackStrength    = 500;
//   radiusDamageType    = $DamageType::MB;

   explosion           = "PPCExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = PPCEmitter; //ContrailEmitter;

   dryVelocity       = 500.0; // -nite- was 150
   wetVelocity       = 600.0;       // faster uw
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 15000;
   lifetimeMS        = 15000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 20;
   flareColor        = "0.3 0.3 1.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/BlueImpact";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

datablock TargetProjectileData(MedLaserProj)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 200;	// = 150; -soph
   beamColor           	= "0.1 1.0 0.1";
								
   startBeamWidth			= 0.60;
   pulseBeamWidth 	   = 0.65;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 12.0;
   pulseLength         	= 0.3;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

datablock TargetProjectileData(HeavyLaserProj)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 300;
   beamColor           	= "1.0 0.1 0.1";
								
   startBeamWidth			= 1.0;
   pulseBeamWidth 	   = 1.0;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.45;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

datablock ShockwaveData(GaussTrailShockwave)
{
   width = 2.5;
   numSegments = 16;
   numVertSegments = 6;
   velocity = 10;
   acceleration = -20;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(GaussTrailExplosion)
{
//   soundProfile   = plasmaExpSound;
   shockwave = GaussTrailShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(GaussTrailCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 750;

   explosion           = "GaussTrailExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock AudioProfile(PCRFireSound)
{
   filename    = "fx/weapons/cg_water4.wav";//misc/shieldh1
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(PCRSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 500;
   textureName          = "special/bigspark";
   colors[0]     = "0.0 0.4 0.4 1.0";
   colors[1]     = "0.1 0.3 0.5 0.5";
   colors[2]     = "0.05 0.2 0.15 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(PCRSparksEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 13;
   velocityVariance = 5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "PCRSparks";
};

//----------------------------------------------------
//  Explosion
//----------------------------------------------------

datablock ParticleData(UnderwaterPhaserCExplosionSmoke)
{
   dragCoeffiecient     = 80.0;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.025;

   constantAcceleration = -0.75;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.2 0.9 1.0 1.0";
   colors[1]     = "0.4 1.0 0.9 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterPhaserCExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 5.25;
   velocityVariance = 0.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "UnderwaterPhaserCExplosionSmoke";
};

datablock ParticleData(UnderwaterPhaserCSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/droplet";
   colors[0]     = "0.2 0.9 1.0 1.0";
   colors[1]     = "0.2 0.9 1.0 1.0";
   colors[2]     = "0.2 0.9 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.25;
   sizes[2]      = 0.25;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterPhaserCSparkEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 10;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterPhaserCSparks";
};

datablock ExplosionData(UnderwaterPhaserCSubExplosion1)
{
   offset = 1.0;
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;
};

datablock ExplosionData(UnderwaterPhaserCSubExplosion2)
{
   offset = 1.0;
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;
};

datablock ExplosionData(UnderwaterPhaserCExplosion)
{
//   soundProfile   = GrenadeExplosionSound;

   soundProfile = blasterExpSound;
//   emitter[0] = PCRSparksEmitter;
   
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;

   subExplosion[0] = UnderwaterPhaserCSubExplosion1;
   subExplosion[1] = UnderwaterPhaserCSubExplosion2;

   shakeCamera = true;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

datablock ParticleData( PhaserCDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.2 0.5 0.1 0.8";
   colors[2]     = "0.1 0.3 0.05 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( PhaserCDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "PhaserCDebrisSmokeParticle";
};

datablock DebrisData(PhaserCDebris)
{
   emitters[0] = PhaserCDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 1;
};

datablock ParticleData(PhaserCExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_003";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 1.000000";
    colors[1] = "0.192000 0.700787 0.088000 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(PhaserCExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 10.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "PhaserCExplosionParticle";
};

datablock ParticleData(PhaserCSparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "0.253749 1.000000  0.253534 1.000000";
    colors[1] = "0.253749 1.000000  0.253534 0.524231";
    colors[2] = "0.253749 1.000000  0.253534 0.000000";
    sizes[0] = 0.733871;
    sizes[1] = 0.733871;
    sizes[2] = 0.75;
};

datablock ParticleEmitterData(PhaserCSparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 18;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 250;
    particles = "PhaserCSparksParticle";
};

datablock ExplosionData(PhaserCExplosion)
{
   soundProfile   = GrenadeExplosionSound;

   emitter[0] = PhaserCExplosionEmitter;
   emitter[1] = PhaserCSparksEmitter;

   debris = PhaserCDebris;
   debrisThetaMin = 0;
   debrisThetaMax = 180;
   debrisNum = 6;
   debrisVelocity = 25.0;
   debrisVelocityVariance = 7.5;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 5.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

datablock TracerProjectileData(PCRBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.0;
   explosion           = "UnderwaterPhaserCExplosion";
   hasDamageRadius     = true;
   indirectDamage      = 0.21;
   damageRadius        = 6.0;
   kickBackStrength    = 250;
   radiusDamageType    = $DamageType::Phaser;
   splash              = ChaingunSplash;

   sound 				= ChaingunProjectile;

   dryVelocity       = 325.0;
   wetVelocity       = 175.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 1.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 20.0;
   tracerAlpha     = true;
   tracerMinPixels = 6;
   tracerColor     = "0.05 0.6 0.8 0.768";
	tracerTex[0]  	 = "flarebase";
	tracerTex[1]  	 = "flarebase";
	tracerWidth     = 0.6;
   crossSize       = 1.0;
   crossViewAng    = 0.99;
   renderCross     = true;
};

datablock EnergyProjectileData(SmallIONBeam)
{
   emitterDelay        = -1;
   directDamage        = 0.1;
   directDamageType    = $DamageType::IonCannon;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 1.0;

   explosion           = "IonBlueExplosion";
   splash              = BlasterSplash;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 250;//200; --- 250 minimum? (ST)

   muzzleVelocity    = 500;

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = 500;
   wetVelocity       = 500;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1600;

   hasLight    = true;
   lightRadius = 15.0;//30.0; --- Range is 1-20 (ST)
   lightColor  = "0.1 0.2 1.0";

   scale = "1 1 1";
   crossViewAng = 0.8;
   crossSize = 1.0;

   ignoreReflection = true;

   lifetimeMS     = 1600;
   blurLifetime   = 0.05;
   blurWidth      = 0.05;
   blurColor = "0.0 0.0 0.4 1.0";

   texture[0] = "special/shrikeBolt";
   texture[1] = "special/shrikeBoltCross";
};
