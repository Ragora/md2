if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//**************************************************************
// THUNDERSWORD BOMBER
//**************************************************************
//**************************************************************
// SOUNDS
//**************************************************************

datablock AudioProfile(BomberFlyerEngineSound)
{
   filename    = "fx/vehicles/bomber_engine.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberFlyerThrustSound)
{
   filename    = "fx/vehicles/bomber_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberTurretActivateSound)
{
   filename    = "fx/vehicles/bomber_turret_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberTurretDryFireSound)
{
   filename    = "fx/vehicles/bomber_turret_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombProjectileSound)
{
   filename    = "fx/vehicles/bomber_bomb_projectile.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(BomberBombDryFireSound)
{
   filename    = "fx/vehicles/bomber_bomb_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(BomberBombFireSound)
{
   filename    = "fx/vehicles/bomber_bomb_reload.wav";
   description = AudioClose3d;
   preload = true;
};

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

datablock FlyingVehicleData(BomberFlyer) : VehicleDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_air_bomber.dts";
   multipassenger = true;
   computeCRC = true;

   vehicleType = $VehicleMask::Thundersword;
   hullType = $VehicleHull::Thundersword;
   
   weaponNode = 1;

   debrisShapeName = "vehicle_air_bomber_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 3;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;
   isProtectedMountPoint[7] = true;
   mountArmorsAllowed[ 0 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Engineer ;	// +[soph]
   mountArmorsAllowed[ 1 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Heavy | $ArmorMask::Blastech | $ArmorMask::Engineer | $ArmorMask::BattleAngel ;
   mountArmorsAllowed[ 2 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Heavy | $ArmorMask::MagIon | $ArmorMask::Blastech | $ArmorMask::Engineer | $ArmorMask::BattleAngel ;
												// +[/soph]
   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 1.5;
	explosionRadius = 50.0;

   maxDamage = 8.0;     // Total health
   destroyedLevel = 8.0;   // Damage textures show up at this health level
   armorShrug = 0.01 ;	// +soph

   isShielded = true;
   energyPerDamagePoint = 59;
   maxEnergy = 400;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1800;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.8;

   // Turbo Jet
   jetForce = 7500;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 40.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 3.4;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 2.0;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds
   
   // Maneuvering
//   maxSteeringAngle = 5;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
//   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
//   verticalSurfaceForce = 8;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maxSteeringAngle = 5;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 6;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 4;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 4700;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1300;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 425;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   
//   steeringForce = 1100;         // Steering jets (force applied when you move the mouse)
//   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
//   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
//   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 150;

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0";
   damageLevelTolerance[0] = 0.5;
   damageLevelTolerance[1] = 0.75;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 350;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   minImpactSpeed = 8;      // If hit ground at speed above this then it's an impact. Meters/second
   softImpactSpeed = 10;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.
   speedDamageScale = 0.035;

   collDamageThresholdVel = 30.0;
   collDamageMultiplier   = 0.0125;

   //
   minTrailSpeed = 15;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = TurboJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = BomberFlyerThrustSound;
   engineSound = BomberFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0;
   mediumSplashSoundVelocity = 30.0;
   hardSplashSoundVelocity = 60.0;
   exitSplashSoundVelocity = 20.0;

   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Thundersword';
   targetTypeTag = 'Cruiser';
   sensorData = VehiclePulseSensor;

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   showPilotInfo = 1;

   damageMod = 0.5;
   updateFrames = true;   
//   damageModPerMount = 0.25;
   damageModSlot[0] = 0.5;
//   damageModSlot[1] = 0.2;
//   damageModSlot[2] = 0.15;

};

datablock TurretData(CruiserShieldCap)
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = 1000;
   destroyedLevel          = 1000;

   thetaMin                = 70;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 400;
   capacitorRechargeRate   = 0.60;	// = 0.8; post blast-bugfix, this is a little high -soph
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 0;

   targetNameTag           = '';
   targetTypeTag           = '';
};

//**************************************************************
// WEAPONS
//**************************************************************

//-------------------------------------
// BOMBER BELLY TURRET GUN (projectile)
//-------------------------------------

//-------------------------------------
// BOMBER BELLY TURRET CHARACTERISTICS
//-------------------------------------

datablock TurretData(BomberTurret) : TurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = BomberFlyer.maxDamage;
   destroyedLevel          = BomberFlyer.destroyedLevel;

   thetaMin                = 70;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 0.9;
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 3;

   targetNameTag           = 'Thundersword Belly';
   targetTypeTag           = 'Turret';
};

//datablock TurretImageData(AIAimingTurretBarrel) 	// -[soph]
//{							// no longer used
//   shapeFile            = "turret_muzzlepoint.dts";	// all appearances commented out
//   mountPoint           = 3;				// -
							// -
//   projectile           = BomberFusionBolt;		// -
							// -
   // Turret parameters					// -
//   activationMS         = 1000;			// -
//   deactivateDelayMS    = 1500;			// -
//   thinkTimeMS          = 200;			// -
//   degPerSecTheta       = 500;			// -
//   degPerSecPhi         = 800;			// -
   							// -
//   attackRadius         = 75;				// -
//};							// -[/soph]


datablock TurretImageData(AIAimingSeekingBarrel)
{
   shapeFile            = "turret_muzzlepoint.dts";
   mountPoint           = 9;

   projectile           = TurboDisc ;			// BomberFusionBolt; -soph

   // Turret parameters
   activationMS         = 1000;
   deactivateDelayMS    = 1500;
   thinkTimeMS          = 200;
   degPerSecTheta       = 500;
   degPerSecPhi         = 800;

   attackRadius         = 240 ;				// = 75; -soph
   
//   isSeeker     = true;				// -[soph]
//   seekRadius   = 600;				// -
//   maxSeekAngle = 359;				// -
//   seekTime     = 0.75;				// -
//   minSeekHeat  = 0.5;				// -
//   useTargetAudio = false;				// -
//   minTargetingDistance = 0;				// -[/soph]
};

//-------------------------------------
// BOMBER BOMB PROJECTILE
//-------------------------------------

datablock BombProjectileData(BomberBomb)
{
   projectileShapeName = "bomb.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.675;
   damageRadius        = 45;
   radiusDamageType    = $DamageType::BomberBombs;
   kickBackStrength    = 6000;

   explosion           = "VehicleBombExplosion";
   velInheritFactor    = 1.0;

   grenadeElasticity = 0.25;
   grenadeFriction   = 0.4;
   armingDelayMS     = 2000;
   muzzleVelocity    = 0.1;
   drag              = 0.3;

   minRotSpeed       = "60.0 0.0 0.0";
   maxRotSpeed       = "80.0 0.0 0.0";
   scale             = "1.25 1.25 1.25";

   sound = BomberBombProjectileSound;
   
//   passengerDamageMod = 0.999;
};

//-------------------------------------
// BOMBER BOMB CHARACTERISTICS
//-------------------------------------

datablock StaticShapeData(DropBombs)
{
   catagory             = "Turrets";
   shapeFile            = "bombers_eye.dts";
   maxDamage            = 1.0;
   disabledLevel        = 0.6;
   destroyedLevel       = 0.8;
};

datablock TurretImageData(BomberBombImage)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "2 -4 -0.5";
   mountPoint                       = 10;

   projectile                       = BomberBomb;
   projectileType                   = BombProjectile;
   usesEnergy                       = true;
   useMountEnergy                   = true;
   useCapacitor                     = true;

   fireEnergy                       = 45.0;
   minEnergy                        = 45.0;

   
   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "WaitFire1";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";

   stateName[1]                     = "WaitFire1";
   stateTransitionOnTriggerDown[1]  = "Fire1";
   stateTransitionOnNoAmmo[1]       = "NoAmmo1";

   stateName[2]                     = "Fire1";
   stateTransitionOnTimeout[2]      = "Reload1";
   stateTimeoutValue[2]             = 0.20;	// 0.32; -soph
   stateFire[2]                     = true;
   stateAllowImageChange[2]         = false;
   stateSequence[2]                 = "Fire";
   stateScript[2]                   = "onFire";
   stateSound[2]                    = BomberBombFireSound;

   stateName[3]                     = "Reload1";
   stateSequence[3]                 = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateAllowImageChange[3]         = false;
   stateTransitionOnTimeout[3]      = "WaitFire2";
   stateTransitionOnNoAmmo[3]       = "NoAmmo1";

   stateName[4]                     = "NoAmmo1";
   stateTransitionOnAmmo[4]         = "Reload1";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateSequence[4]                 = "NoAmmo";
   stateSequence[4] = "NoAmmo1";

   stateTransitionOnTriggerDown[4]  = "DryFire1";

   stateName[5]                     = "DryFire1";
   stateSound[5]                    = BomberBombDryFireSound;
   stateTimeoutValue[5]             = 0.5;
   stateTransitionOnTimeout[5]      = "NoAmmo1";

   stateName[6]                     = "WaitFire2";
   stateTransitionOnTriggerDown[6]  = "Fire2";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateTransitionOnNoAmmo[6]       = "NoAmmo";
   stateTransitionOnNoAmmo[6] = "NoAmmo2";

   stateName[7]                     = "Fire2";
   stateTransitionOnTimeout[7]      = "Reload2";
   stateTimeoutValue[7]             = 0.20;	// 0.32; -soph
   stateScript[7]                   = "FirePair";

   stateName[8]                     = "Reload2";
   stateSequence[8]                 = "Reload";
   stateTimeoutValue[8]             = 0.1;
   stateAllowImageChange[8]         = false;
   stateTransitionOnTimeout[8]      = "WaitFire1";
   stateTransitionOnNoAmmo[8]       = "NoAmmo2";

   stateName[9]                     = "NoAmmo2";
   stateTransitionOnAmmo[9]         = "Reload2";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateSequence[9]                 = "NoAmmo";
   stateSequence[9] = "NoAmmo2";

   stateTransitionOnTriggerDown[9]  = "DryFire2";

   stateName[10]                     = "DryFire2";
   stateSound[10]                    = BomberBombDryFireSound;
   stateTimeoutValue[10]             = 0.5;
   stateTransitionOnTimeout[10]      = "NoAmmo2";
};

datablock TurretImageData(BomberBombPairImage)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "-2 -4 -0.5";
   mountPoint                       = 10;

   projectile                       = BomberBomb;
   projectileType                   = BombProjectile;
   usesEnergy                       = true;
   useMountEnergy                   = true;
   useCapacitor                     = true;
   fireEnergy                       = 45.0;
   minEnergy                        = 45.0;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 0.13;
   stateFire[1]                     = true;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
   stateSound[1]                    = BomberBombFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
                         
};

datablock TurretImageData(BomberTurretBarrel)
{
   shapeFile                        = "turret_belly_barrell.dts";
   mountPoint                       = 0;

   projectileType = TracerProjectile;
   projectile = KMPlusBullet;

   usesEnergy                       = true;
   useCapacitor                     = true;
   useMountEnergy                   = true;
   fireEnergy                       = 12.0;
   minEnergy                        = 12.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   // State transitions
   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "WaitFire1";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = BomberTurretActivateSound;

   stateName[1]                     = "WaitFire1";
   stateTransitionOnTriggerDown[1]  = "Fire1";
   stateTransitionOnNoAmmo[1]       = "NoAmmo1";

   stateName[2]                     = "Fire1";
   stateTransitionOnTimeout[2]      = "Reload1";
   stateTimeoutValue[2]             = 0.1;
   stateFire[2]                     = true;
   stateRecoil[2]                   = LightRecoil;
   stateAllowImageChange[2]         = false;
   stateSequence[2]                 = "Fire";
   stateScript[2]                   = "onFire";
   stateSound[2]                    = AAFireSound; //BomberTurretFireSound;

   stateName[3]                     = "Reload1";
   stateSequence[3]                 = "Reload";
   stateTimeoutValue[3]             = 0.05;
   stateAllowImageChange[3]         = false;
   stateTransitionOnTimeout[3]      = "WaitFire2";
   stateTransitionOnNoAmmo[3]       = "NoAmmo1";

   stateName[4]                     = "NoAmmo1";
   stateTransitionOnAmmo[4]         = "Reload1";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateSequence[4]                 = "NoAmmo";
   stateSequence[4] = "NoAmmo1";

   stateTransitionOnTriggerDown[4]  = "DryFire1";

   stateName[5]                     = "DryFire1";
   stateSound[5]                    = BomberTurretDryFireSound;
   stateTimeoutValue[5]             = 0.5;
   stateTransitionOnTimeout[5]      = "NoAmmo1";

   stateName[6]                     = "WaitFire2";
   stateTransitionOnTriggerDown[6]  = "Fire2";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateTransitionOnNoAmmo[6]       = "NoAmmo";
   stateTransitionOnNoAmmo[6] = "NoAmmo2";

   stateName[7]                     = "Fire2";
   stateTransitionOnTimeout[7]      = "Reload2";
   stateTimeoutValue[7]             = 0.1;
   stateScript[7]                   = "FirePair";

   stateName[8]                     = "Reload2";
   stateSequence[8]                 = "Reload";
   stateTimeoutValue[8]             = 0.05;
   stateAllowImageChange[8]         = false;
   stateTransitionOnTimeout[8]      = "WaitFire1";
   stateTransitionOnNoAmmo[8]       = "NoAmmo2";

   stateName[9]                     = "NoAmmo2";
   stateTransitionOnAmmo[9]         = "Reload2";
   // ---------------------------------------------
   // z0dd - ZOD, 5/8/02. Incorrect parameter value
   //stateSequence[9]                 = "NoAmmo";
   stateSequence[9] = "NoAmmo2";

   stateTransitionOnTriggerDown[9]  = "DryFire2";

   stateName[10]                     = "DryFire2";
   stateSound[10]                    = BomberTurretDryFireSound;
   stateTimeoutValue[10]             = 0.5;
   stateTransitionOnTimeout[10]      = "NoAmmo2";
};

//datablock ShapeBaseImageData(BomberTargetingComputer)
//{
//   mountPoint = 4;
//   offset = "0 0.1 -0.3";
//   rotation = "0 1 0 180";
//   shapeFile = "turret_muzzlepoint.dts";

//   isSeeker     = true;
//   seekRadius   = 250;
//   maxSeekAngle = 90;
//   seekTime     = 0.05;
//   minSeekHeat  = 0.5;
//   useTargetAudio = false;
//   minTargetingDistance = 5;
//};

datablock TurretImageData(BomberTurretBarrelPair)
{
   shapeFile                = "turret_belly_barrelr.dts";
   mountPoint               = 1;

   projectileType = TracerProjectile;
   projectile = KMPlusBullet;

   usesEnergy                       = true;
   useCapacitor                     = true;
   useMountEnergy                   = true;
   fireEnergy                       = 12.0;
   minEnergy                        = 12.0;

   // Turret parameters
   activationMS                     = 1000;
   deactivateDelayMS                = 1500;
   thinkTimeMS                      = 200;
   degPerSecTheta                   = 360;
   degPerSecPhi                     = 360;

   attackRadius                     = 75;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 0.1;
   stateFire[1]                     = true;
   stateRecoil[1]                   = LightRecoil;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
   stateSound[1]                    = AAFireSound; //BomberTurretFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.05;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
};

datablock TurretImageData(BomberAutoCannon)	// +[soph]
{						// + basically reusing most of this stuff
   shapeFile                        = "turret_belly_barrell.dts" ;
   mountPoint                       = 0 ;	// +
   scale                            = "1 0.75 0.75" ;
						// +
   projectileType = TracerProjectile ;		// +
   projectile = AutoCannonBullet ;		// +
						// +
   usesEnergy                       = true ;	// +
   useCapacitor                     = true ;	// +
   useMountEnergy                   = true ;	// +
   fireEnergy                       = 5.0 ;	// +
   minEnergy                        = 5.0 ;	// +
						// +
   // Turret parameters				// +
   activationMS                     = 1000 ;	// +
   deactivateDelayMS                = 1500 ;	// +
   thinkTimeMS                      = 200 ;	// +
   degPerSecTheta                   = 360 ;	// +
   degPerSecPhi                     = 360 ;	// +
						// +
   attackRadius                     = 75 ;	// +
						// +
   casing              = AutoShellDebris ;	// +
   shellExitDir        = "0.5 0.0 1.0" ;	// +
   shellExitOffset     = "0.15 -0.56 -0.1" ;	// +
   shellExitVariance   = 15.0 ;			// +
   shellVelocity       = 1.0 ;			// +
						// +
   projectileSpread = 5.0 / 1000.0 ;		// +
						// +
   // State transitions				// +
   stateName[0]                    = "Activate" ;
   stateTransitionOnTimeout[0]     = "WaitFire1" ;
   stateTimeoutValue[0]            = 0.5 ;	// +
   stateSequence[0]                = "Activate" ;
   stateSound[0]                   = BomberTurretActivateSound ;
						// +
   stateName[1]                    = "WaitFire1" ;
   stateTransitionOnTriggerDown[1] = "Fire1" ;	// +
   stateTransitionOnNoAmmo[1]      = "NoAmmo1" ;
						// +
   stateName[2]                    = "Fire1" ;	// +
   stateTransitionOnTimeout[2]     = "WaitFire2" ;
   stateTransitionOnNoAmmo[2]      = "NoAmmo1" ;
   stateTimeoutValue[2]            = 0.06 ;	// +
   stateFire[2]                    = true ;	// +
   stateRecoil[2]                  = LightRecoil ;
   stateAllowImageChange[2]        = false ;	// +
   stateSequence[2]                = "Fire" ;	// +
   stateScript[2]                  = "onFire" ;	// +
						// +
   stateName[3]                    = "NoAmmo1" ;
   stateTransitionOnAmmo[3]        = "WaitFire2" ;
   stateSequence[3]                = "NoAmmo1" ;
   stateTransitionOnTriggerDown[3] = "DryFire1" ;
						// +
   stateName[4]                    = "DryFire1" ;
   stateSound[4]                   = BomberTurretDryFireSound ;
   stateTimeoutValue[4]            = 0.5 ;	// +
   stateTransitionOnTimeout[4]     = "NoAmmo1" ;
						// +
   stateName[5]                    = "WaitFire2" ;
   stateTransitionOnTriggerDown[5] = "Fire2" ;	// +
   stateTransitionOnNoAmmo[5]      = "NoAmmo2" ;
						// +
   stateName[6]                    = "Fire2" ;	// +
   stateTransitionOnTimeout[6]     = "WaitFire1" ;
   stateTransitionOnNoAmmo[6]      = "NoAmmo2" ;
   stateTimeoutValue[6]            = 0.06 ;	// +
   stateScript[6]                  = "FirePair" ;
						// +
   stateName[7]                    = "NoAmmo2" ;
   stateTransitionOnAmmo[7]        = "WaitFire1" ;
   stateSequence[7]                = "NoAmmo2" ;
   stateTransitionOnTriggerDown[7] = "DryFire2" ;
						// +
   stateName[8]                    = "DryFire2" ;
   stateSound[8]                   = BomberTurretDryFireSound ;
   stateTimeoutValue[8]            = 0.5 ;	// +
   stateTransitionOnTimeout[8]     = "NoAmmo2" ;
};						// +
						// +
function BomberAutoCannon::onFire( %this , %obj , %slot )
{						// +
   %obj.play3D( FChaingunFireSound ) ;		// +
   Parent::onFire( %this , %obj , %slot ) ;	// +
}						// +
						// +
function BomberAutoCannon::FirePair( %this , %obj , %slot )
{						// +
   %obj.setImageTrigger( 3 , true ) ;		// +
}						// +
						// +
function BomberAutoCannonPair::onFire( %this , %obj , %slot )
{						// +
   %obj.play3D( FChaingunFireSound ) ;		// +
   Parent::onFire( %this , %obj , %slot ) ;	// +
   %obj.setImageTrigger( 3 , false ) ;		// +
}						// +
						// +
datablock TurretImageData(BomberAutoCannonPair)	// +
{						// +
   shapeFile                = "turret_belly_barrelr.dts" ;
   mountPoint               = 1 ;		// +
   scale                    = "1 0.75 0.75" ;	// +
						// +
   projectileType = TracerProjectile ;		// +
   projectile = AutoCannonBullet ;		// +
						// +
   usesEnergy                       = true ;	// +
   useCapacitor                     = true ;	// +
   useMountEnergy                   = true ;	// +
   fireEnergy                       = 5.0 ;	// +
   minEnergy                        = 5.0 ;	// +
						// +
   // Turret parameters				// +
   activationMS                     = 1000 ;	// +
   deactivateDelayMS                = 1500 ;	// +
   thinkTimeMS                      = 200 ;	// +
   degPerSecTheta                   = 360 ;	// +
   degPerSecPhi                     = 360 ;	// +
						// +
   attackRadius                     = 75 ;	// +
						// +
   casing              = AutoShellDebris ;	// +
   shellExitDir        = "-0.5 0.0 1.0" ;	// +
   shellExitOffset     = "-0.15 -0.56 -0.1" ;	// +
   shellExitVariance   = 15.0 ;			// +
   shellVelocity       = 1.0 ;			// +
						// +
   projectileSpread = 5.0 / 1000.0 ;		// +
						// +
   stateName[0]                     = "WaitFire" ;
   stateTransitionOnTriggerDown[0]  = "Fire" ;	// +

   stateName[1]                     = "Fire" ;	// +
   stateTransitionOnTimeout[1]      = "WaitFire" ;
   stateTimeoutValue[1]             = 0.06 ;	// +
   stateFire[1]                     = true ;	// +
   stateRecoil[1]                   = LightRecoil ;
   stateAllowImageChange[1]         = false ;	// +
   stateSequence[1]                 = "Fire" ;	// +
   stateScript[1]                   = "onFire" ;
						// +
};						// +[/soph]

datablock ShapeBaseImageData(BomberCruiseMissileA)
{
   shapeFile = "vehicle_grav_scout.dts";

   mountPoint = 10;
   offset = "2.0 -2.5 -0.75";
   rotation = "0 1 0 -180"; // calcThisRotD("0 -125 180");//"0 0 1 180";
};

datablock ShapeBaseImageData(BomberCruiseMissileB) : BomberCruiseMissileA
{
   offset = "-2.0 -2.5 -0.75";
   rotation = "0 1 0 180";
};

datablock TurretImageData(BomberPilotCannon)	// +[soph]
{						// forward cannons for the Thundersword
   shapeFile                        = "turret_aa_large.dts";

   projectileType                   = TracerProjectile;
   projectile                       = KMPlusBullet;
   usesEnergy                       = true;
   fireEnergy                       = 10.0;
   minEnergy                        = 10.0;
   emap                             = true;
   offset                           = "2.0 -5.0 0.5";
   
   // Turret parameters
   activationMS                     = 175; // z0dd - ZOD, 3/27/02. Was 250. Amount of time it takes turret to unfold
   deactivateDelayMS                = 500;
   thinkTimeMS                      = 140; // z0dd - ZOD, 3/27/02. Was 200. Amount of time before turret starts to unfold (activate)
   degPerSecTheta                   = 600;
   degPerSecPhi                     = 1080;
   attackRadius                     = 300;

   // State transitions
   stateName[0]                     = "Activate";
   stateTransitionOnNotLoaded[0]    = "Dead";
   stateTransitionOnLoaded[0]       = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateSequence[0]                 = "Activate";
   stateSound[1]                    = AASwitchSound;
   stateTimeoutValue[1]             = 1.0;
   stateTransitionOnTimeout[1]      = "Ready";
   stateTransitionOnNotLoaded[1]    = "Deactivate";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";
   stateScript[1]                   = "AAActivateReady";

   stateName[2]                     = "Ready";
   stateSequence[1]                 = "Deploy";
   stateTransitionOnNotLoaded[2]    = "Deactivate";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateScript[2]                   = "AAReady";

   stateName[3]                     = "Fire";
   stateScript[3]                   = "onFire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.075;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Deploy";	// "Fire1";
   stateSound[3]                    = AAFireSound;

//   stateName[4]                     = "FirePt2";
//   stateTransitionOnTimeout[4]      = "Reload";
//   stateTimeoutValue[4]             = 0.05;
//   stateFire[4]                     = true;
//   stateRecoil[4]                   = LightRecoil;
//   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Fire2";

   stateName[4]                     = "Reload";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
   stateTransitionOnTimeout[4]      = "ReadyPair" ;
   stateTransitionOnNotLoaded[4]    = "Deactivate";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";

   stateName[5]                     = "ReadyPair" ;
//   stateSequence[5]                 = "Deploy" ;
   stateTransitionOnNotLoaded[5]    = "Deactivate" ;
   stateTransitionOnTriggerDown[5]  = "FirePair" ;
   stateTransitionOnNoAmmo[5]       = "NoAmmo" ;
   stateScript[5]                   = "AAReady";

   stateName[6]                     = "FirePair" ;
   stateTimeoutValue[6]             = 0.1 ;
   stateAllowImageChange[6]         = false ;
   stateScript[6]                   = "onFirePair";
   stateTransitionOnTimeout[6]      = "Ready" ;
   stateTransitionOnNotLoaded[6]    = "Deactivate" ;
   stateTransitionOnNoAmmo[6]       = "NoAmmo" ;

   stateName[7]                     = "Deactivate";
   stateSequence[7]                 = "Activate";
   stateDirection[7]                = false;
   stateTimeoutValue[7]             = 1.0;
   stateTransitionOnLoaded[7]       = "ActivateReady";
   stateTransitionOnTimeout[7]      = "Dead";

   stateName[8]                    = "Dead";
   stateTransitionOnLoaded[8]      = "ActivateReady";

   stateName[9]                    = "NoAmmo";
   stateTransitionOnAmmo[9]        = "Ready" ;
   stateSequence[9]                = "NoAmmo";
};

datablock TurretImageData(BomberPilotCannonPair) : BomberPilotCannon
{
   offset                           = "-2.0 -5.0 0.5" ;

   stateTimeoutValue[4]             = 0.1 ;
   stateTransitionOnTimeout[4]      = "Ready" ;
};

function TSwordPilotCannon::onTrigger(){}	// console spam

function BomberPilotCannon::onFire( %data , %obj , %slot )
{
     Parent::onFire( %data , %obj , %slot ) ;
     %obj.lastProjectile.bkSourceObject = %obj.getMountNodeObject( 0 ) ;
}

function BomberPilotCannon::onFirePair( %data , %obj , %slot )
{
     %obj.setImageTrigger( %slot+1 , true ) ;
}

function BomberPilotCannonPair::onFire( %data , %obj , %slot )
{
     Parent::onFire( %data , %obj , %slot ) ;
     %obj.setImageTrigger( %slot , false ) ;
     %obj.lastProjectile.bkSourceObject = %obj.getMountNodeObject( 0 ) ;
}						// +[/soph]

datablock TurretImageData(BomberTargetingImage)
{
   shapeFile                        = "turret_muzzlepoint.dts";
   offset                           = "-2 -4 -0.5";
   mountPoint                       = 10;

//   projectile                       = IncendiaryBomb;
//   projectileType                   = BombProjectile;
   usesEnergy                       = true;
   useMountEnergy                   = true;
   useCapacitor                     = true;
   fireEnergy                       = 125.0;
   minEnergy                        = 125.0;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 2.0;
   stateFire[1]                     = true;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
//   stateSound[1]                    = NukeBombFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
};

function BomberTargetingImage::onFire(%data,%obj,%slot)
{
//      Parent::onFire(%data,%obj,%slot);

     handleRaptorLaunch(%data, %obj.getObjectMount());
}

function handleRaptorLaunch(%data, %obj)
{
     if(%obj.RaptorLaunchTimeout)
          return;

     if(%obj.RaptorReady[6])
          launchRaptorMissile(%obj, 6);
     else if(%obj.RaptorReady[7])
          launchRaptorMissile(%obj, 7);
     else
          %obj.play3d(BomberTurretDryFireSound);
}

function launchRaptorMissile(%obj, %slot)
{
     %player = %obj.getMountNodeObject(1);
     %vehicle = %obj;

     %fwdVec = %obj.getForwardVector();
     %point = %obj.getMuzzlePoint(%slot);
//     %point = vectorAdd(vectorScale(vectorNormalize(%fwdVec), 8.0), %point);		// -soph
     %offset = vectorNormalize( vectorSub( %point , %obj.getWorldBoxCenter() ) ) ;	// +[soph]
     %point = vectorAdd( %point , %offset ) ;						// +
											// +
     %muzzleVector = %obj.turretObject.getMuzzleVector( 0 ) ;				// +
     %vector = VectorNormalize( %muzzleVector ) ; 					// +
     %vector = VectorScale( %vector , 10 ) ;						// +
     %start = vectorAdd( %obj.turretObject.getMuzzlePoint( 0 ) , %vector ) ; 		// +
     %vector = VectorScale( %vector , 200 ) ;						// +
     %end = VectorAdd( %start , %vector ) ;						// +
     %result = containerRayCast( %start , %end , $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType , %obj ) ;
     if( getWord( %result , 0 ) )							// +
        %positionTarget = vectorSub( getWords( %result , 1 , 3 ) , %muzzleVector ) ;	// +
     else										// +
        %positionTarget = %end ;							// +[/soph]
 
//     %FFRObject = new StaticShape()
//     {
//          dataBlock        = mReflector;
//     };
//     %FFRObject.setPosition(%point);
//     %FFRObject.schedule(32, delete);

     %p = new SeekerProjectile()
     {
         dataBlock        = "BomberCruiseMissile";
         initialDirection = %offset ;							// -soph = %obj.turretObject.getMuzzleVector(0); //%fwdVec; 
         initialPosition  = %point;
//         sourceObject     = %FFRObject;
         sourceObject     = %obj ;							// -soph = %player; 
         sourceSlot       = %slot;
         vehicleObject    = %obj;
         vehicleMod       = %vehicle.damageMod;
//         bkSourceObject   = %player;
      };

   MissionCleanup.add(%p);
   MissileSet.add(%p);
//   %obj.setCollisionTimeout(%p); // fixes object hitting yourself

//   %target = %obj.getMountNodeObject(10).getLockedTarget();				// -soph

   %locked = false;

//   if(%target)									// -[soph]
//   {											// -
//      if(%target.getDatablock().jetfire)						// -
//         assignTrackerTo(%target, %p);						// -
//      else										// -
//         %p.setObjectTarget(%target);							// -
//         										// -
//      %locked = true;									// -
//   }											// -
//   else if(%obj.isLocked())								// -
//      %p.setPositionTarget(%obj.getLockedPosition());					// -[/soph]

   %p.sourceObject = %player ;								// +[soph]
   if( %positionTarget !$= "" && %positionTarget !$= "0 0 0" )				// +
      %p.setPositionTarget( %positionTarget ) ;						// +[/soph]
   else
      %p.setNoTarget();

//   if(%lockedd)									// -[soph]
//   {											// - outdated, also bugged
//        %vec = %obj.getMuzzleVector(0);						// -
//        %pos = %obj.getMuzzlePoint(0);						// -
//        %nVec = VectorNormalize(%vec);						// -
//        %scVec = VectorScale(%nVec, 300);						// -
//        %end = VectorAdd(%pos, %scVec);						// -
//        %result = containerRayCast(%pos, %end, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType, %obj);
//        %target = getWord(%result, 0);						// -
//        %targetPos = posFromRaycast(%result);						// -
//        										// -
//        if(isObject(%target))								// -
//          %p.setPositionTarget(%targetPos);						// -
//   }											// -[/soph]

//     if(%target)
//          commandToClient(%player.client, 'BottomPrint', "Raptor Missile launched at" SPC detag(%target.getName(), 5, 1);
//     else
//          commandToClient(%player.client, 'BottomPrint', "Raptor Missile launched!", 5, 1);

     %obj.RaptorReady[%slot] = false;
     %obj.play3D(MechRocketFireSound);
     %obj.play3D(MechFire2Sound);
     %obj.unmountImage(%slot);
     %obj.RaptorLaunchTimeout = true;
     schedule(30000, %obj, "resetRaptorMissile", %obj, %slot);
     schedule(5000, %obj, "resetRaptorTimeout", %obj);
}

function resetRaptorTimeout(%obj)
{
     %obj.RaptorLaunchTimeout = false;
}

function resetRaptorMissile(%obj, %slot)
{
     %obj.RaptorReady[%slot] = true;

     if(%slot == 6)
     {
           %obj.mountImage(BomberCruiseMissileA, 6);
           %obj.play3D(MissileReloadSound);
     }
     else if(%slot == 7)
     {
           %obj.mountImage(BomberCruiseMissileB, 7);
           %obj.play3D(MissileReloadSound);
     }
}

function BomberTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(4, false);
   if(%obj.getImageTrigger(6))
   {
      %obj.setImageTrigger(6, false);
      ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
   }
   %client = %obj.getControllingClient();
   %client.player.isBomber = false;
   commandToClient(%client, 'endBomberSight');
//   %client.player.setControlObject(%client.player);
   %client.player.mountVehicle = false;
//   %client.player.getDataBlock().doDismount(%client.player);
   if(%client.player.getState() !$= "Dead")
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
}

//----------------------------------------------------------------------------
datablock TurretData(BomberFlyerTailTurret) : TurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_base_large.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = 2.5;	// BomberFlyer.maxDamage; -soph
   destroyedLevel          = 2.5;	// BomberFlyer.destroyedLevel; -soph
   disabledLevel           = 1.5;	// +[soph]
   explosion               = TurretExplosion;
   expDmgRadius            = 5.0;
   expDamage               = 0.5;
   expImpulse              = 3000.0;	// +[/soph]

   thetaMin                = 0;
   thetaMax                = 150;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 1.0;

   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
//   numWeapons              = 2;

   offset = "-4.75 -9 0";
   rotation = "0 1 0 65"; //calcThisRotD("0 37.5 180");

   targetNameTag           = 'Tailgun' ;	// 'BomberFlyer'; -soph
   targetTypeTag           = 'Turret';

   debrisShapeName = "debris_generic.dts";	// +soph
   debris = TurretDebris;			// +soph
};

function BomberFlyerTailTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      if( %targetObject.getDamageState() !$= "Destroyed" )
         if( %amount > 0 )
            if( $DamageGroup[%damageType] & $DamageGroupMask::IgnoreShield )
            {
               %targetObject.applyDamage( %amount ) ;
               return ;
            }
            else
               if( %vehicle.lastDamageProj $= %projectile )
               {
                  if( %vehicle.lastDamageAmount > %amount )
                  {					// main body hit, turret splashed
                     %shieldSoak = %vehicle.lastDamageAmount - %vehicle.shieldBleed ;
                     if( %shieldSoak > %amount )	// shields already absorbed damage
                        return ;
                     else
                     {					// shields were insufficient, turret takes damage
                        %targetObject.applyDamage( %amount - %shieldSoak ) ;
                        return ;
                     }
                  }
                  else
                  {					// turret hit, main body splashed
                     %shieldSoak = %vehicle.lastDamageAmount - %vehicle.shieldBleed ;
                     %amount -= %shieldSoak ;
                     if( !isObject( %vehicle.repulsorField ) )
                     {
                        if( %vehicle.shieldCap )
                           %amount = %vehicle.getDataBlock().shieldCapCheck( %vehicle , %position , %amount , %damageType ) ;
                        else
                           %amount = %vehicle.getDataBlock().checkShields( %vehicle , %position , %amount , %damageType ) ;
                     }
                     if( %amount > 0 )
                        %targetObject.applyDamage( %amount ) ;
                  }
               }
               else if( %vehicle.lastHitPosition $= %position )
               {
                  if( %vehicle.lastPosDamageAmount > %amount )
                  {					// main body hit, turret splashed
                     %shieldSoak = %vehicle.lastPosDamageAmount - %vehicle.shieldBleed ;
                     if( %shieldSoak > %amount )	// shields already absorbed damage
                        return ;
                     else
                     {					// shields were insufficient, turret takes damage
                        %targetObject.applyDamage( %amount - %shieldSoak ) ;
                        return ;
                     }
                  }
                  else
                  {					// turret hit, main body splashed
                     %shieldSoak = %vehicle.lastPosDamageAmount - %vehicle.shieldBleed ;
                     %amount -= %shieldSoak ;
                     if( !isObject( %vehicle.repulsorField ) )
                     {
                        if( %vehicle.shieldCap )
                           %amount = %vehicle.getDataBlock().shieldCapCheck( %vehicle , %position , %amount , %damageType ) ;
                        else
                           %amount = %vehicle.getDataBlock().checkShields( %vehicle , %position , %amount , %damageType ) ;
                     }
                     if( %amount > 0 )
                        %targetObject.applyDamage( %amount ) ;
                  }
               }
               else
               {
                  %vehicle.getDataBlock().damageObject( %vehicle , %sourceObject , %position , %amount , %damageType , %vec , %client , %projectile ) ;
                  %targetObject.applyDamage( %vehicle.shieldBleed );
               }
}

function BomberFlyerTailTurret::onDisabled( %data , %obj , %prevState )
{
   if( %prevState $= "Enabled" )
   {
      %pilot = %obj.getObjectMount().getMountNodeObject(0);
      if( isObject( %pilot ) )
         commandToClient( %pilot.client , 'CenterPrint' , "TAIL TURRET FAILURE" , 3 , 1 );
   }
   Parent::onDisabled( %data , %obj , %prevState );
}

function BomberFlyerTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function BomberFlyerTailTurret::onTrigger(%data, %obj, %trigger, %state)
{
   switch (%trigger)
   {
      case 0:
         %obj.fireTrigger = %state;
         if(%obj.selectedWeapon == 1)
         {
            %obj.setImageTrigger(4, false);
            if(%state)
               %obj.setImageTrigger(2, true);
            else
               %obj.setImageTrigger(2, false);
         }
         else
         {
            %obj.setImageTrigger(2, false);
            if(%state)
               %obj.setImageTrigger(4, true);
            else
               %obj.setImageTrigger(4, false);
         }
      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function BomberFlyerTailTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(4, false);
   %client = %obj.getControllingClient();
// %client.setControlObject(%client.player);
   %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   %client.player.mountVehicle = false;
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
   %client.player.ctrlVT = "";         
//   %client.player.getDataBlock().doDismount(%client.player);
   %client.player.isBomber = false;
   commandToClient(%client, 'endBomberSight');
}

function BomberFlyerTailTurret::doDismount(){}

datablock ShapeBaseImageData(BomberFlyerEngine1) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
//   shapeFile = "turret_assaulttank_plasma.dts";
   offset = "-4.75 -11 0";
   rotation = "0 1 0 65"; //calcThisRotD("0 37.5 180");

//   stateName[0]             = "Activate";
//   stateSequence[0]         = "Activate";

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.1; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(BomberFlyerEngine2) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "4.75 -11 0";
   rotation = "0 1 0 -65"; //calcThisRotD("0 37.5 180");

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.1; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(BomberFlyerEngine3) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "-4.75 -11 -2";
   rotation = "0 1 0 115"; //calcThisRotD("0 37.5 180");

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.1; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(BomberFlyerEngine4) : MainAPEImage
{
   shapeFile = "turret_muzzlepoint.dts";
   offset = "4.75 -11 -2";
   rotation = "0 1 0 -115"; //calcThisRotD("0 37.5 180");

   stateEmitter[3]       = "FlyerJetEmitter"; // emitter
   stateEmitterTime[3]       = 0.1; // 100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(BomberFlyerDecal1) : EngineAPEImage
{
   shapeFile = "vehicle_grav_scout.dts";
   offset = "-4.75 -8 0";
   rotation = "0 1 0 65"; //calcThisRotD("0 37.5 180");
};

datablock ShapeBaseImageData(BomberFlyerDecal2) : BomberFlyerDecal1
{
   offset = "4.75 -8 0";
   rotation = "0 1 0 -65";
};

datablock ShapeBaseImageData(BomberFlyerDecal3) : BomberFlyerDecal1
{
   offset = "-4.75 -8 -2";
   rotation = "0 1 0 115";
};

datablock ShapeBaseImageData(BomberFlyerDecal4) : BomberFlyerDecal1
{
   offset = "4.75 -8 -2";
   rotation = "0 1 0 -115";
};

datablock StaticShapeData(BomberFlyerStaticF)
{
   shapeFile = "vehicle_air_bomber.dts";

   dynamicType = $TypeMasks::VehicleObjectType;

   maxDamage = BomberFlyer.maxDamage;
   destroyedLevel = BomberFlyer.destroyedLevel;

   targetNameTag = 'Thundersword';
   targetTypeTag = 'Cruiser';
};

function BomberFlyerStaticF::onDamage(%data, %obj)
{
//   %newDamageVal = %obj.getDamageLevel();	// -[soph]
//   if(%obj.lastDamageVal !$= "")		// gutted to plug D+D bug
//      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)   
//         %obj.getObjectMount().setDamageLevel(%newDamageVal);
//   %obj.lastDamageVal = %newDamageVal;	// -[/soph]
}

function BomberFlyerStaticF::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();

   if(%vehicle)	
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function BomberFlyer::processTickUpdate(%data, %obj)
{
   if(isObject(%obj) && isObject(%obj.frame))
   {
      %speed = vectorLen(%obj.getVelocity());

      if(%speed > 20)
      {
         %obj.frame.setImageTrigger(0, true);
         %obj.frame.setImageTrigger(1, true);
         %obj.frame.setImageTrigger(2, true);
         %obj.frame.setImageTrigger(3, true);
         
         if(%speed > 40)
         {
              %obj.frame.setImageTrigger(4, true);
              %obj.frame.setImageTrigger(5, true);
              %obj.frame.setImageTrigger(6, true);
              %obj.frame.setImageTrigger(7, true);
         }
         else
         {
              %obj.frame.setImageTrigger(4, false);
              %obj.frame.setImageTrigger(5, false);
              %obj.frame.setImageTrigger(6, false);
              %obj.frame.setImageTrigger(7, false);         
         }
      }
      else
      {
         %obj.frame.setImageTrigger(0, false);
         %obj.frame.setImageTrigger(1, false);
         %obj.frame.setImageTrigger(2, false);
         %obj.frame.setImageTrigger(3, false);      
         %obj.frame.setImageTrigger(4, false);
         %obj.frame.setImageTrigger(5, false);
         %obj.frame.setImageTrigger(6, false);
         %obj.frame.setImageTrigger(7, false);
      }
   }
}

function BomberFlyer::processFrameUpdate(%data, %obj)
{
   if(isObject(%obj) && isObject(%obj.frame))
   {
      %obj.mountObject(%obj.frame, 7);
      %obj.frame.setDamageLevel(%obj.getDamageLevel());
   }
}

function BomberFlyer::swapTurretControl(%data, %obj, %player)
{
     if(!%obj.rearTurret)
          return;
   
     if(%obj.turretSelectedNode == 10)
     {
          %obj.turretSelectedNode = 2;
          %turret = %obj.getMountNodeObject(%obj.turretSelectedNode);
          %player.vehicleTurret = %turret;
          %player.turreteer = %turret;
          %player.ctrlVT = %turret;
          resetControlVT(%player);                
          %player.setControlObject(%turret);
          %player.client.setObjectActiveImage(%turret, 2);
     }
     else
     {
          %obj.turretSelectedNode = 10;
          %turret = %obj.getMountNodeObject(%obj.turretSelectedNode);
          %player.vehicleTurret = %turret;
          %player.turreteer = %turret;
          %player.ctrlVT = %turret;
          resetControlVT(%player);                          
          %player.setControlObject(%turret);
          %player.client.setObjectActiveImage(%turret, 0);
     }
}
