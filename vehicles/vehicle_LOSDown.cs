if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//**************************************************************
// LOSDown Transporter
//**************************************************************

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

Meltdown.MassVelMod[LOSDownFlyer] = 1.0; //4.0
Meltdown.MaxDmgMod[LOSDownFlyer] = 2.0;
Meltdown.ShieldPctMod[LOSDownFlyer] = 1.0;

datablock FlyingVehicleData(LOSDownFlyer) : VehicleDamageProfile
{
   spawnOffset = "0 0 2";

   vehicleType = $VehicleMask::Nightshade;
   hullType = $VehicleHull::Thundersword;
   
   catagory = "Vehicles";
   shapeFile = "vehicle_air_bomber.dts";
   multipassenger = true;
   computeCRC = true;

   weaponNode = 1;

   debrisShapeName = "vehicle_air_bomber_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
   mountPose[1] = sitting;
   numMountPoints = 3;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;

   mountArmorsAllowed[ 0 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Engineer ;	// +[soph]
   mountArmorsAllowed[ 1 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Heavy | $ArmorMask::Blastech | $ArmorMask::Engineer | $ArmorMask::BattleAngel ;
   mountArmorsAllowed[ 2 ] = $ArmorMask::Light | $ArmorMask::Medium | $ArmorMask::Heavy | $ArmorMask::MagIon | $ArmorMask::Blastech | $ArmorMask::Engineer | $ArmorMask::BattleAngel ;
												// +[/soph]
   cameraMaxDist = 22;
   cameraOffset = 5;
   cameraLag = 1.0;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 1.5;
	explosionRadius = 25.0;

   maxDamage = 4.0;     // Total health
   destroyedLevel = 4.0;   // Damage textures show up at this health level

   isShielded = true;
   energyPerDamagePoint = 41.6;
   maxEnergy = 300;      // Afterburner and any energy weapon pool
   minDrag = 60;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 1800;        // Angular Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.8;

   // Turbo Jet
   jetForce = 6250*Meltdown.MassVelMod[LOSDownFlyer];      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 40.0;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 4.0;       // Energy use of the afterburners (low number is less drain...can be fractional)
   vertThrustMultiple = 2.0;

   // Auto stabilize speed
   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 1500;      // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't whack out at very slow speeds

   // Maneuvering
   maxSteeringAngle = 5;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 5;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 8;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 4700*Meltdown.MassVelMod[LOSDownFlyer];      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1100;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 300;      // Steering jets (how much you heel over when you turn)
   rollForce = 8;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 5;        // Height off the ground at rest
   createHoverHeight = 3;  // Height off the ground when created
   maxForwardSpeed = 500;

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = OnFireEmitter;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.5;
   damageLevelTolerance[1] = 0.75;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 375*Meltdown.MassVelMod[LOSDownFlyer];        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   minImpactSpeed = 8;      // If hit ground at speed above this then it's an impact. Meters/second
   softImpactSpeed = 10;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 25;    // Sound hooks. This is the hard hit.
   speedDamageScale = 0.035;

   collDamageThresholdVel = 20.0;
   collDamageMultiplier   = 0.025;

   //
   minTrailSpeed = 15;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = TurboJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = MPBThrustSound;
   engineSound = BomberFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 15.0;
   mediumSplashSoundVelocity = 30.0;
   hardSplashSoundVelocity = 60.0;
   exitSplashSoundVelocity = 20.0;

   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingBomberIcon;
   cmdMiniIconName = "commander/MiniIcons/com_bomber_grey";
   targetNameTag = 'Nightshade';
   targetTypeTag = 'Support Cruiser';
  // sensorData = VehiclePulseSensor;								// -soph
   sensorData = AWACPulseSensor;								// +[soph]
   sensorRadius = AWACPulseSensor.detectRadius;							// +
   sensorColor = "255 194 9";									// +[/soph]

   checkRadius = 7.1895;
   observeParameters = "1 10 10";
   showPilotInfo = 1;
   
   isNightshade = true;

   damageMod = 0.5;										// +soph
//   damageModPerMount = 0.1;									// -soph
   damageModSlot[0] = 0.5;									// +soph
//   damageModSlot[2] = 0.2;									// -soph
};

datablock TurretData(NightshadeShieldCap)
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = 1000;
   destroyedLevel          = 1000;

   thetaMin                = 70;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 200;
   capacitorRechargeRate   = 0.5;
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 0;

   targetNameTag           = '';
   targetTypeTag           = '';
};

//**************************************************************
// WEAPONS
//**************************************************************

//-------------------------------------
// BOMBER BELLY TURRET GUN (projectile)
//-------------------------------------

//-------------------------------------
// BOMBER BELLY TURRET CHARACTERISTICS
//-------------------------------------

datablock TurretData(LOSDownTurret) : TurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_belly_base.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = LOSDownFlyer.maxDamage;
   destroyedLevel          = LOSDownFlyer.destroyedLevel;

   thetaMin                = 90;
   thetaMax                = 180;

   // capacitor
   maxCapacitorEnergy      = 250;
   capacitorRechargeRate   = 0.7;
   
   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
   numWeapons              = 3;

   targetNameTag           = 'Nightshade Support';
   targetTypeTag           = 'Turret';
};

datablock TargetProjectileData(IonTAGBeam)
{
   directDamage        = 0.0;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;

   maxRifleRange       = 300;
   beamColor           = "1.0 0.1 0.1";
//   coupleBeam           = 0; // new

   startBeamWidth    = 0.20;
   pulseBeamWidth    = 0.25;
   beamFlareAngle    = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   textureName[0]      = "special/nonlingradient";
   textureName[1]      = "special/flare";
   textureName[2]      = "special/pulse";
   textureName[3]      = "special/blasterHit"; //"special/BlasterBoltCross";
//   beacon               = true;

   hasLight = true;
   lightColor = IonTAGBeam.BeamColor;
   lightRadius = 6;
};

datablock TargetProjectileData(RMSTAGBeam)
{
   directDamage        = 0.0;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;

   maxRifleRange       = 300;
   beamColor           = "1.0 1.0 0.1";	// "0.1 1.0 0.1"; -soph
//   coupleBeam           = 0; // new

   startBeamWidth    = 0.20;
   pulseBeamWidth    = 0.25;
   beamFlareAngle    = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   textureName[0]      = "special/nonlingradient";
   textureName[1]      = "special/flare";
   textureName[2]      = "special/pulse";
   textureName[3]      = "special/blasterHit"; //"special/BlasterBoltCross";
//   beacon               = true;

   hasLight = true;
   lightColor = RMSTAGBeam.BeamColor;
   lightRadius = 6;
};

function LOSDownTurret::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function LOSDownTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function LOSDownTurret::onTrigger(%data, %obj, %trigger, %state)
{
   switch (%trigger)
   {
      case 0:
         %obj.fireTrigger = %state;

         if(%obj.selectedWeapon == 1)
         {
            %obj.setImageTrigger(6, false);
            if(%state)
               %obj.setImageTrigger(2, true);
            else
               %obj.setImageTrigger(2, false);
         }
         else if(%obj.selectedWeapon == 2)
         {
            %obj.setImageTrigger(2, false);
            if(%state)
               %obj.setImageTrigger(4, true);
            else
               %obj.setImageTrigger(4, false);
         }
         else
         {
            %obj.setImageTrigger(4, false);
            if(%state)
               %obj.setImageTrigger(6, true);
            else
               %obj.setImageTrigger(6, false);
         }
      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function LOSDownTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(1, false);
	%obj.setImageTrigger( 2 , false ) ;	// + [soph]
	%obj.setImageTrigger( 4 , false ) ;	// +
	%obj.setImageTrigger( 6 , false ) ;	// + [/soph]

   %client = %obj.getControllingClient();
   
   if(!isObject(%client.player))
     return;
   
   %client.player.isBomber = false;
   commandToClient(%client, 'endBomberSight');
//   %client.player.setControlObject(%client.player);
   %client.player.mountVehicle = false;
//   %client.player.getDataBlock().doDismount(%client.player);

   if(%client.player.getState() !$= "Dead")
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);

   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
}

//datablock TurretImageData(LOSDownTeleporterImage)
//{
//   shapeFile                        = "turret_tank_barrelchain.dts";
//   offset                           = "0 0 0";
//   mountPoint                       = 1;
//
////   projectile                       = NuclearBomb;
////   projectileType                   = BombProjectile;
//   usesEnergy                       = true;
//   useMountEnergy                   = true;
//   useCapacitor                     = true;
//   fireEnergy                       = 0;
//   minEnergy                        = -1;
//
//   stateName[0]                     = "WaitFire";
//   stateTransitionOnTriggerDown[0]  = "Fire";
//
//   stateName[1]                     = "Fire";
//   stateTransitionOnTimeout[1]      = "StopFire";
//   stateTimeoutValue[1]             = 1.5;
//   stateFire[1]                     = true;
//   stateAllowImageChange[1]         = false;
//   stateSequence[1]                 = "Fire";
//   stateScript[1]                   = "onFire";
////   stateSound[1]                    = NukeBombFireSound;
//
//   stateName[2]                     = "StopFire";
//   stateTimeoutValue[2]             = 0.1;
//   stateTransitionOnTimeout[2]      = "WaitFire";
//   stateScript[2]                   = "stopFire";
//};

datablock ShapeBaseImageData( NightshadeSkyhookImage )
{
   className                        = WeaponImage ;
   shapeFile                        = "turret_muzzlepoint.dts" ;
   offset                           = "0 0 0" ;
   mountPoint                       = 1 ;

   projectileType                   = ELFProjectile ;
   deleteLastProjectile             = true ;
   emap                             = true ;

	usesEnergy                  = true ;
 	minEnergy                   = 3 ;

   stateName[0]                     = "Activate" ;
   stateSequence[0]                 = "Activate" ;
   stateSound[0]                    = ELFGunSwitchSound ;
   stateTimeoutValue[0]             = 0.5 ;
   stateTransitionOnTimeout[0]      = "ActivateReady" ;

   stateName[1]                     = "ActivateReady" ;
   stateTransitionOnAmmo[1]         = "Ready" ;
   stateTransitionOnNoAmmo[1]       = "NoAmmo" ;

   stateName[2]                     = "Ready" ;
   stateTransitionOnNoAmmo[2]       = "NoAmmo" ;
   stateTransitionOnTriggerDown[2]  = "CheckWet" ;

   stateName[3]                     = "Fire" ;
   stateEnergyDrain[3]              = 9.5 ;
   stateFire[3]                     = true ;
   stateAllowImageChange[3]         = false ;
   stateScript[3]                   = "onFire" ;
   stateTransitionOnTriggerUp[3]    = "Deconstruction" ;
   stateTransitionOnNoAmmo[3]       = "Deconstruction" ;

   stateName[4]                     = "NoAmmo" ;
   stateTransitionOnAmmo[4]         = "Ready" ;

   stateName[5]                     = "Deconstruction" ;
   stateScript[5]                   = "deconstruct" ;
   stateTransitionOnTimeout[5]      = "Ready" ;
   stateTransitionOnNoAmmo[6]       = "NoAmmo" ;

   stateName[6]                     = "DryFire" ;
   stateSound[6]                    = ElfFireWetSound ;
   stateTimeoutValue[6]             = 0.5 ;
   stateTransitionOnTimeout[6]      = "Ready" ;

   stateName[7]                     = "CheckWet" ;
   stateTransitionOnWet[7]          = "DryFire" ;
   stateTransitionOnNotWet[7]       = "Fire" ;
};

function NightshadeSkyhookImage::onFire( %data , %obj , %slot )
{
	%data.lightStart = getSimTime() ;

	%useEnergyObj = %obj.getObjectMount() ;

	if( !%useEnergyObj )
		%useEnergyObj = %obj.getObjectMount() ;
	else
		%vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0 ;

	%p = new ELFProjectile() {
 		dataBlock		= StasisELF ;
		initialDirection	= %obj.getMuzzleVector( %slot ) ;
 		initialPosition		= %obj.getMuzzlePoint( %slot ) ;
 		sourceObject		= %obj ;
 		sourceSlot 		= %slot ;
 		vehicleObject		= %vehicle ;
	};

 	if ( isObject( %obj.lastProjectile ) && %obj.deleteLastProjectile )
		%obj.lastProjectile.delete() ;

 	%obj.lastProjectile = %p ;
 	%obj.deleteLastProjectile = %data.deleteLastProjectile ;
 	MissionCleanup.add( %p ) ;
 
 	if(%obj.client)
		%obj.client.projectile = %p ;

 	if( !%p.hasTarget() )
 	{
		%obj.playAudio( 0 , ELFFireWetSound ) ;

		%q = new ShockLanceProjectile()
		{
 			dataBlock		= "BasicShocker" ;
 			initialDirection 	= %obj.getMuzzleVector( %slot ) ;
 			initialPosition		= %obj.getMuzzlePoint( %slot ) ;
 			sourceObject 		= %obj ;
 			sourceSlot 		= %slot ;
		};

		MissionCleanup.add( %q ) ;
 	}
 	return %p ;
}

function shadePointDetectArea(%pos)
{
   InitContainerRadiusSearch(%pos, 5, $TypeMasks::PlayerObjectType);

   %pl = ContainerSearchNext();

   if(%pl)
      return %pl;
   else
      return 0;
}

//function LOSDownTeleporterImage::onFire(%data,%obj,%slot)		// -soph
function LOSDownTeleporterImageSimulation( %data , %obj , %slot )	// +soph
{
//     if(%obj.getCapacitorLevel() < 20)
//     {
//          commandToClient(%obj.getControllingClient(), 'BottomPrint', "Not enough energy for teleportation.", 3, 1);
//          %obj.play3D(MortarDryFireSound);
//     }
//     else
//          %obj.setCapacitorLevel(%obj.getCapacitorLevel() - 20);
          
   %bomber = %obj.getObjectMount();
   %seat = %bomber.getMountNodeObject(2);

   if(isObject(%seat))
      %loaded = true;
   else
      %loaded = false;

   if(%loaded)
   {
      %vec = %obj.getMuzzleVector(%slot);
      %pos = %obj.getMuzzlePoint(%slot);
      %nVec = VectorNormalize(%vec);
      %scVec = VectorScale(%nVec, 250);
      %end = VectorAdd(%pos, %scVec);
      %searchResult = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);

      %result = firstWord(%searchResult);

      if(!isObject(%result))
          %result = shadePointDetectArea(getMuzzleRaycastPt(%obj, 0, 250));

      if(isObject(%result))
      {
         if(%result.client)
            LDTBeginTeleportE(%bomber, %result, %bomber.getSlotPosition(2));
         else
         {
            %endp = getMuzzleRaycastPt(%obj, 0, 250);
            %endPoint = vectorAdd(%endp, vectorNeg(vectorScale(%vec, 2)));
            %vec = vectorNeg(%obj.getMuzzleVector(0));
            LDTBeginTeleportL(%bomber, %seat, %endPoint);
         }
      }
      else
      {
         %endp = getMuzzleRaycastPt(%obj, 0, 250);
         %endPoint = vectorAdd(%endp, vectorNeg(vectorScale(%vec, 2)));
         LDTBeginTeleportL(%bomber, %seat, %endpoint);
      }
   }
   else
   {
      %vec = %obj.getMuzzleVector(%slot);
      %pos = %obj.getMuzzlePoint(%slot);
      %nVec = VectorNormalize(%vec);
      %scVec = VectorScale(%nVec, 250);
      %end = VectorAdd(%pos, %scVec);
      %searchResult = containerRayCast(%pos, %end, $TypeMasks::PlayerObjectType, %obj); //  | $TypeMasks::TurretObjectType
      %result = firstWord(%searchResult);

      if(!isObject(%result))
          %result = shadePointDetectArea(getMuzzleRaycastPt(%obj, 0, 250));
                
      if(isObject(%result))
      {
         if((%result.client) && !%result.isTacticalMech)
            LDTBeginTeleportE(%bomber, %result, %bomber.getSlotPosition(2));
      }
   }

   %t = new TargetProjectile()
   {
          dataBlock        = "BasicTargeter";
          initialDirection = %obj.getMuzzleVector(%slot);
          initialPosition  = %obj.getMuzzlePoint(%slot);
          sourceObject     = %obj;
          sourceSlot       = %slot;
          vehicleObject    = 0;
   };
   MissionCleanup.add(%t);
   schedule(250, 0, "killit", %t);
}

function LDTBeginTeleportL(%bomber, %obj, %pos)
{
   %obj.teleportStartFX(true);
   %obj.play3d(TeleportSound);
   schedule(2000, 0, LDTFinishTeleL, %bomber, %obj, %pos);
}

function LDTFinishTeleL(%bomber, %obj, %pos)
{
   %seat = %bomber.getMountNodeObject(2);

   if(isObject(%seat) && %seat.getType() & $TypeMasks::PlayerObjectType)
   {
      %seat.getDataBlock().doDismount(%seat, true);
      %seat.setPosition(vectorAdd(%seat.position, "0 0 2"));
   }
   else if(%seat.getType() & $TypeMasks::TurretObjectType)
   {
       %bomber.unmountObject(2);
       %seat.setPosition(%pos);
       %seat.teleportEndFX();
       return;
   }

   %obj.setPosition(vectorAdd(%pos, "0 0 2"));
   %obj.teleportEndFX();
}

function LDTBeginTeleportE(%bomber, %obj, %pos)
{
   %obj.teleportStartFX(true);
   %obj.play3d(TeleportSound);
   schedule(2000, 0, LDTFinishTeleE, %bomber, %obj, %pos);
}

function LDTFinishTeleE(%bomber, %obj, %pos)
{
   %seat = %bomber.getMountNodeObject(2);

   if(isObject(%seat))
   {
      %seat.getDataBlock().doDismount(%seat, true);
      %seat.setPosition(vectorAdd(%seat.position, "0 0 2"));
   }

   %obj.setPosition(%bomber.getSlotPosition(2));
   if(%obj.getType() & $TypeMasks::TurretObjectType)
      %bomber.mountObject(%obj, 2);
   %obj.teleportEndFX();
}

datablock TurretImageData(LOSDownRMSImage)
{
   shapeFile                        = "turret_tank_barrelchain.dts";
   offset                           = "0 0 0";
   mountPoint                       = 1;

//   projectile                       = NuclearBomb;
//   projectileType                   = BombProjectile;
   usesEnergy                       = true;
   useMountEnergy                   = true;
   useCapacitor                     = true;
   fireEnergy                       = 0;
   minEnergy                        = -1;

   stateName[0]                     = "WaitFire";
   stateTransitionOnTriggerDown[0]  = "Fire";

   stateName[1]                     = "Fire";
   stateTransitionOnTimeout[1]      = "StopFire";
   stateTimeoutValue[1]             = 1.5;
   stateFire[1]                     = true;
   stateAllowImageChange[1]         = false;
   stateSequence[1]                 = "Fire";
   stateScript[1]                   = "onFire";
//   stateSound[1]                    = NukeBombFireSound;

   stateName[2]                     = "StopFire";
   stateTimeoutValue[2]             = 0.1;
   stateTransitionOnTimeout[2]      = "WaitFire";
   stateScript[2]                   = "stopFire";
};

// 0 RMS 1 MANTA
function fireNightshadeBeacon(%data, %obj, %slot, %which)
{
     %vehicle = %obj.getObjectMount();
     %pilot = %vehicle.getMountNodeObject(0);
     %gunner = %vehicle.getMountNodeObject(1);
     %team = %gunner.team;
     %proj = %which ? "IonTAGBeam" : "RMSTAGBeam";

     if(!%pilot)
     {
          commandToClient(%gunner.client, 'BottomPrint', "Cannot fire targeting laser without pilot.", 5, 1);
          %obj.play3D(MortarDryFireSound);
          return;
     }

     // Prevent beacon spam
     if(isObject(%gunner.swbeacon))
          %gunner.swbeacon.delete();

     %manta = $MantaSat[%team];
     %rms = $RMSSilo[%team];

     %weapon = %which ? %manta : %rms;

   // Validate firing here
     if(isObject(%weapon))
     {
          if(%weapon.reloading)
          {
               %msg = %which ? "MANTA Ion Cannon is cooling down..." : "RMS Silo is fuelling.";		// : "Nuke Silo is refuelling..."; -soph
               commandToClient(%gunner.client, 'BottomPrint', %msg, 5, 1);
               %obj.play3D(MortarDryFireSound);
               return;
          }

          if(!%weapon.isLoaded)
          {
               %msg = %which ? "The MANTA is not loaded with a battery." : "RMS Silo warheads are not yet ready.";	// : "The Nuke Silo is not loaded with a warhead."; -soph
               commandToClient(%gunner.client, 'BottomPrint', %msg, 5, 1);
               %obj.play3D(MortarDryFireSound);
               return;
          }
     }
     else
     {
          %msg = %which ? "There is no MANTA Satellite in orbit." : "There is no RMS silo deployed.";	// "Nuke Silo has not been deployed."; -soph
          commandToClient(%gunner.client, 'BottomPrint', %msg, 5, 1);
          %obj.play3D(MortarDryFireSound);
          return;

     }

   if(%data.usesEnergy)
   {
      if(%data.useMountEnergy)
         %energy = %vehicle.getEnergyLevel();
      else
         %energy = %obj.getEnergyLevel();

      if(%data.useCapacitor && %data.usesEnergy)
         if(%vehicle.turretObject.getCapacitorLevel() < %data.minEnergy)
            return;

      else if(%energy < %data.minEnergy)
         return;
   }

   %t = new TargetProjectile()
   {
          dataBlock        = %proj;
          initialDirection = %obj.getMuzzleVector(%slot);
          initialPosition  = %obj.getMuzzlePoint(%slot);
          sourceObject     = %obj;
          sourceSlot       = %slot;
          vehicleObject    = %obj;
   };
   MissionCleanup.add(%t);
   %t.schedule(250, delete);
   
   %vec = %obj.getMuzzleVector(%slot);
   %pos = %obj.getMuzzlePoint(%slot);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, 300);
   %end = VectorAdd(%pos, %scVec);
   %searchResult = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);

   %result = firstWord(%searchResult);

   if(%result)
   {
        %rpos = posFromRaycast(%searchResult);

        if(%weapon == %manta) // temporary
        {											// +soph
             %time = 8000;									// -[soph]
//        else		
//             %time = 8000;

//             %deplbecn = addObject(%gunner, "DeployableWarpBeacon", %rpos);
//             %gunner.swbeacon = %deplbecn;
//             %deplbecn.setScale("10 10 10");
//             %deplbecn.playThread(0, "ambient");
//             %deplbecn.schedule(%time+1000, setDamageState, Destroyed);
//             %deplbecn.schedule(%time+1100, setPosition, "0 0 10000");
//             %deplbecn.schedule(%time+1350, delete);
//             %deplbecn.universalResistFactor = 1000;

//             %deplbecn.ionTagBeacon = new BeaconObject()
//             {
//               dataBlock = "SuperWeaponBeacon";
//               beaconType = "vehicle";
//               position = %rpos;
//             };

//             %deplbecn.ionTagBeacon.playThread($AmbientThread, "ambient");
//             %deplbecn.ionTagBeacon.team = %team;
//             %deplbecn.ionTagBeacon.sourceObject = %gunner;
//             %deplbecn.ionTagBeacon.setTarget(%team);
//             MissionCleanup.add(%deplbecn.ionTagBeacon);

//             %beacon = %deplbecn.ionTagBeacon;
//             %deplbecn.ionTagBeacon = "";
//             %beacon.schedule(%time, delete);							// -[/soph]

             %deplbecn = 0 ;	// +soph
        }
        else			// +[soph]
        {
             %time = 250 ;
             %deplbecn = 0 ;
        }			// +[/soph]
        schedule(%time, %deplbecn , superWeaponValidateFire, %gunner, %team, %rpos, %which);	// %deplbecn.getPosition(), %which); -soph
   }

//   %obj.play3D(MBLFireSound);

   if(%data.usesEnergy)
   {
      if(%data.useMountEnergy)
      {
         if( %data.useCapacitor )
            %vehicle.turretObject.setCapacitorLevel( %vehicle.turretObject.getCapacitorLevel() - %data.fireEnergy );
         else
            %useEnergyObj.setEnergyLevel(%energy - %data.fireEnergy);
      }
      else
         %obj.setEnergyLevel(%energy - %data.fireEnergy);
   }
   else
      %obj.decInventory(%data.ammo,1);
}

function SuperWeaponBeaconPlacer::onExplode(%data, %proj, %pos, %mod)
{

}

function superWeaponValidateFire(%obj, %team, %pos, %weapon)
{
     if( !isObject(%obj) )
          return;

     %rms = $RMSSilo[%team];
     %manta = $MantaSat[%team];

     %platform = %weapon ? %manta : %rms ;
//     if(%weapon == %rms && %rms.reloading || %weapon == %manta && %manta.reloading)	//  -soph
     if( %platform.reloading ) 								// +soph
          return;

     %reloadtime = 30;
     %name = "NULL";
          
     if(!%weapon)
     { 
//          %rms.targetPos = %pos;
            %name = "Remote Missile Silo";
          
//          if(%rms.loadType == 1)
//          {
//               sequenceRMSS(%rms, %obj);
//               %reloadtime = 10;
//          }
//          else if(%rms.loadType == 2)
//               sequenceRMSSingle(%rms, %obj);

//          %time = %rms.lockduration;
          
//          if(%rms.charges $= "" || %rms.charges == 0)
//               cooldownMR(%team, %reloadtime, 0);
//          else
//          {
//               %rms.charges--;
//               bottomPrint(%obj.client, %rms.charges SPC "RMS Missiles left.", 10, 1);
//          }
          messageClient( %plyr.client , 'MsgDEDWarmup' , '\c5You have deployed your team\'s Defense+ Device, activation in 60 seconds.' );
          messageTeamExcept( %plyr.client , 'MsgDEDWarmup', '\c5%1 has deployed your team\'s Defense+ Device, activation in 60 seconds.' , %plyr.client.name );
          RMSSQuadFire( %rms , %obj , %pos );
          return;
     }
     else
     {
          %pos2 = vectorAdd(%manta.getPosition(), "0 0 -10");
          %pointDir = getVectorFromPoints(%pos2, %pos);
          %name = "MANTA Ion Cannon";
          
          %FFRObject = new StaticShape()
          {
               dataBlock        = mReflector;
          };
          MissionCleanup.add(%FFRObject);
          %FFRObject.setPosition(%pos2);
          %FFRObject.schedule(32, delete);

          if(%manta.loadType == 1)
          {
               MANTAFireIonBlast(%obj, %FFRObject, %pointdir, %pos2);
               %reloadtime = 2;
          }
          else if(%manta.loadType == 2)
          {
               MANTAFireIonNuke(%obj, %FFRObject, %pointdir, %pos2);
               %reloadtime = 30;
          }
          else if(%manta.loadType == 3)
               MANTAFireIonRain(%obj, %FFRObject, %pointdir, %pos2);
               
          %time = %manta.cooldownTime;

          if(%manta.charges $= "" || %manta.charges == 0)
               cooldownMR(%team, %reloadtime, 1);
          else
          {
               %manta.charges--;
               bottomPrint(%obj.client, %manta.charges SPC "Battery charges left.", 10, 1);
          }
     }
     
      if((%weapon && %manta.loadType == 3) || (!%weapon && %RMS.loadType == 2))
          return;

      for(%i = 0; %i < ClientGroup.getCount(); %i++)
          %clObj = ClientGroup.getObject(%i);
             if(!%clObj.isAIControlled() && %clObj.team == %team)
                  centerPrint(%clObj, "Friendly" SPC %name SPC "fired!" SPC %reloadtime SPC "minutes remaining until next fire.", 10, 1);
}

function resetMRTime(%team, %which)
{
     %name = "NULL";
     
     if(%which)
     {
          %name = "MANTA Ion Cannon";
          $MantaSat[%team].reloading = false;
     }
     else
     {
          %name = "Remote Missile Silo";     
          $RMSSilo[%team].reloading = false;
     }
     
      for(%i = 0; %i < ClientGroup.getCount(); %i++)
          %clObj = ClientGroup.getObject(%i);
             if(!%clObj.isAIControlled() && %clObj.team == %team)
                  bottomPrint(%clObj, %name SPC "has cooled down, and can now be used again.", 10, 1);
}

function cooldownMR(%team, %time, %which)
{
     %cdtime = %time * 60 * 1000;
     
     if(%which)
     {
          $MantaSat[%team].reloading = true;
          $MantaSat[%team].isLoaded = false;
          $MantaSat[%team].loadType = 0;
     }
     else
     {
          $RMSSilo[%team].reloading = true;
          $RMSSilo[%team].isLoaded = false;
          $RMSSilo[%team].loadType = false;
     }
          
     schedule(%cdtime, 0, resetMRTime, %team, %which);
}

function LOSDownRMSImage::onFire(%data,%obj,%slot)
{
  if( %slot == 2 )		// +[soph]
     LOSDownTeleporterImageSimulation( %data , %obj , %slot ) ;
  else if( %slot == 6 )
     fireNightshadeBeacon( %data , %obj , %slot , 1) ;
  else				// +[/soph]
     fireNightshadeBeacon(%data, %obj, %slot, 0);
}

//datablock TurretImageData(LOSDownMantaImage)
//{
//   shapeFile                        = "turret_tank_barrelchain.dts";
//   offset                           = "0 0 0";
//   mountPoint                       = 1;
//
////   projectile                       = NuclearBomb;
////   projectileType                   = BombProjectile;
//   usesEnergy                       = true;
//   useMountEnergy                   = true;
//   useCapacitor                     = true;
//   fireEnergy                       = 0;
//   minEnergy                        = -1;
//
//   stateName[0]                     = "WaitFire";
//   stateTransitionOnTriggerDown[0]  = "Fire";
//
//   stateName[1]                     = "Fire";
//   stateTransitionOnTimeout[1]      = "StopFire";
//   stateTimeoutValue[1]             = 1.5;
//   stateFire[1]                     = true;
//   stateAllowImageChange[1]         = false;
//   stateSequence[1]                 = "Fire";
//   stateScript[1]                   = "onFire";
////   stateSound[1]                    = NukeBombFireSound;
//
//   stateName[2]                     = "StopFire";
//   stateTimeoutValue[2]             = 0.1;
//   stateTransitionOnTimeout[2]      = "WaitFire";
//   stateScript[2]                   = "stopFire";
//};

//function LOSDownMANTAImage::onFire(%data,%obj,%slot)
//{
//     fireNightshadeBeacon(%data, %obj, %slot, 1);
//}

//datablock StaticShapeData(SuperWeaponBeacon)	// removed -[soph]
//{
//   shapeFile = "turret_muzzlepoint.dts";
//   targetNameTag = 'beacon';
//   isInvincible = true;
//
//   dynamicType = $TypeMasks::SensorObjectType;
//};						// -[/soph]
