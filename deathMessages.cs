if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

/////////////////////////////////////////////////////////////////////////////////////////////////
// %1 = Victim's name                                                                          //
// %2 = Victim's gender (value will be either "him" or "her")                                  //
// %3 = Victim's possessive gender	(value will be either "his" or "her")                      //
// %4 = Killer's name                                                                          //
// %5 = Killer's gender (value will be either "him" or "her")                                  //
// %6 = Killer's possessive gender (value will be either "his" or "her")                       //
// %7 = implement that killed the victim (value is the object number of the bullet, disc, etc) //
/////////////////////////////////////////////////////////////////////////////////////////////////

$DeathMessageCampingCount = 1;
$DeathMessageCamping[0] = '\c0%1 was killed for camping near the Nexus.';
																						   
 //Out of Bounds deaths
$DeathMessageOOBCount = 1;
$DeathMessageOOB[0] = '\c0%1 was killed for loitering outside the mission area.';

$DeathMessageLavaCount = 4;
$DeathMessageLava[0] = '\c0%1\'s last thought before falling into the lava : \'Oops\'.';
$DeathMessageLava[1] = '\c0%1 wanted to experience death by lava first hand.';
$DeathMessageLava[2] = '\c0%1 forgot to put on %3 Lava Shield before jumping into the red hot lava.';
$DeathMessageLava[3] = '\c0%1 dives face first into the molten kool-aid... oh wait.';

$DeathMessageLightningCount = 3;
$DeathMessageLightning[0] = '\c0%1 was struck down by lightning!';
$DeathMessageLightning[1] = '\c0%1 caught a lightning bolt!';
$DeathMessageLightning[2] = '\c0%1 stuck %3 finger in Mother Nature\'s light socket.';

//these used when a player presses ctrl-k
$DeathMessageSuicideCount = 5;
$DeathMessageSuicide[0] = '\c0%1 shows off %3 mad dying skills!';
$DeathMessageSuicide[1] = '\c0%1 wanted to make sure %3 gun was loaded.';
$DeathMessageSuicide[2] = '\c0%1 was last heard shouting "death before dishonor!!"';
$DeathMessageSuicide[3] = '\c0%1 goes for the not-so quick and dirty respawn.';
$DeathMessageSuicide[4] = '\c0%1 falls on %3 spinfusor.';

$DeathMessageVectorAccidentCount = 5;	// -soph
$DeathMessageVectorAccident[0] = '\c0%1 materializes in something hard.';
$DeathMessageVectorAccident[1] = '\c0%1 becomes one with the scenery.';
$DeathMessageVectorAccident[2] = '\c0%1 pulls a Han Solo, without the carbonite.';
$DeathMessageVectorAccident[3] = '\c0%1 gave %3 life for interior decor.';
$DeathMessageVectorAccident[4] = '\c0%1 stuck %3 body where it didn\'t belong.';

$DeathMessageVehPadCount = 1;
$DeathMessageVehPad[0] = '\c0%1 got caught in a vehicle\'s spawn field.'; 

$DeathMessageFFPowerupCount = 1;
$DeathMessageFFPowerup[0] = '\c0%1 got caught up in a forcefield during power up.';

$DeathMessageRogueMineCount = 1;
$DeathMessageRogueMine[$DamageType::Mine, 0] = '\c0%1 is all mine.';
$DeathMessageRogueMine[ $DamageType::BTMine , 0 ] = '\c0%1 popped on a BT mine.';	// +soph

//these used when a player kills himself (other than by using ctrl - k)
$DeathMessageSelfKillCount = 5;
$DeathMessageSelfKill[$DamageType::Blaster, 0] = '\c0%1 kills %2self with a blaster.';	
$DeathMessageSelfKill[$DamageType::Blaster, 1] = '\c0%1 makes a note to watch out for blaster ricochets.';
$DeathMessageSelfKill[$DamageType::Blaster, 2] = '\c0%1\'s blaster kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::Blaster, 3] = '\c0%1 deftly guns %2self down with %3 own blaster.';
$DeathMessageSelfKill[$DamageType::Blaster, 4] = '\c0%1 has a fatal encounter with %3 own blaster.';

$DeathMessageSelfKill[$DamageType::Pulse, 0] =  '\c0%1 plays with %3 pulse waves in a lethal way.';	// '\c0%1 plays with %3 pulse bolts in a lethal way.'; -soph
$DeathMessageSelfKill[$DamageType::Pulse, 1] =  '\c0%1 learns what those pretty azure lights were.';	// '\c0%1 learns what those pretty green lights were.'; -soph
$DeathMessageSelfKill[$DamageType::Pulse, 2] =  '\c0%1 dances in a electric blue shower.';		// '\c0%1 dances in a shower of green.'; -soph
$DeathMessageSelfKill[$DamageType::Pulse, 3] =  '\c0%1 is the missing link.';
$DeathMessageSelfKill[$DamageType::Pulse, 4] =  '\c0%1 bathes in a pool of %3 own sapphire death.';	// '\c0%1 swallows a green barrage of %3 own link bolts.'; -soph

$DeathMessageSelfKill[$DamageType::Bullet, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::Bullet, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bullet, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::Bullet, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::Bullet, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessageSelfKill[$DamageType::ChargedBullet, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::ChargedBullet, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::ChargedBullet, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::ChargedBullet, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::ChargedBullet, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessageSelfKill[$DamageType::Ripper, 0] = '\c0%1 slices %2self with %3 own Ripper disc!';
$DeathMessageSelfKill[$DamageType::Ripper, 1] = '\c0%1 learns not to play chicken with sharp objects.';
$DeathMessageSelfKill[$DamageType::Ripper, 2] = '\c0%1 took a bad bounce from %3 Ripper disc.';
$DeathMessageSelfKill[$DamageType::Ripper, 3] = '\c0%1 kills %2self with a Ripper.';
$DeathMessageSelfKill[$DamageType::Ripper, 4] = '\c0%1 chops %3 own head off with a Ripper!';	// %2 -> %3 -soph

$DeathMessageSelfKill[$DamageType::Protron, 0] = '\c0%1 kills %2self with a Protron.';
$DeathMessageSelfKill[$DamageType::Protron, 1] = '\c0%1 makes a note to watch out for deadly Protron waves.';
$DeathMessageSelfKill[$DamageType::Protron, 2] = '\c0%1\'s Protron kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::Protron, 3] = '\c0%1 deftly guns %2self down with %3 own Protron.';
$DeathMessageSelfKill[$DamageType::Protron, 4] = '\c0%1 has a fatal encounter with %3 own Protron.';

$DeathMessageSelfKill[$DamageType::BlueBlaster, 0] = '\c0%1 kills %2self with a blue blaster.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 1] = '\c0%1 makes a note to watch out for blue blaster ricochets.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 2] = '\c0%1\'s blue blaster kills its hapless owner.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 3] = '\c0%1 deftly guns %2self down with %3 own blue blaster.';
$DeathMessageSelfKill[$DamageType::BlueBlaster, 4] = '\c0%1 has a fatal encounter with %3 own blue blaster.';

$DeathMessageSelfKill[$DamageType::Plasma, 0] = '\c0%1 kills %2self with plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::Plasma, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Plasma, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Plasma, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::RAXX, 0] = '\c0%1 roasts %2self.';
$DeathMessageSelfKill[$DamageType::RAXX, 1] = '\c0%1 turns %2self into cajun meat chunks.';
$DeathMessageSelfKill[$DamageType::RAXX, 2] = '\c0%1 tries to act like a fire eater, but failed.';
$DeathMessageSelfKill[$DamageType::RAXX, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::RAXX, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::FlameTurret, 0] = '\c0%1 roasts %2self.';
$DeathMessageSelfKill[$DamageType::FlameTurret, 1] = '\c0%1 turns %2self into cajun meat chunks.';
$DeathMessageSelfKill[$DamageType::FlameTurret, 2] = '\c0%1 tries to act like a fire eater, but failed.';
$DeathMessageSelfKill[$DamageType::FlameTurret, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::FlameTurret, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::Burn, 0] = '\c0%1 kills %2self with plasma.';
$DeathMessageSelfKill[$DamageType::Burn, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::Burn, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::Burn, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::Burn, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::PlasmaCannon, 0] = '\c0%1 kills %2self with a plasma cannon.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::PlasmaCannon, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::ProtronFire, 0] = '\c0%1 administered %2self a hot coal suppository.';	// +[soph]
$DeathMessageSelfKill[$DamageType::ProtronFire, 1] = '\c0%1 charcoal grilled %2self.';
$DeathMessageSelfKill[$DamageType::ProtronFire, 2] = '\c0%1 caught %3 own cinders.';
$DeathMessageSelfKill[$DamageType::ProtronFire, 3] = '\c0%1 gave %2self a hotfoot.';
$DeathMessageSelfKill[$DamageType::ProtronFire, 4] = '\c0%1 tried firewalking.  It didn\'t end well.';		// +[/soph]

$DeathMessageSelfKill[$DamageType::BurnLoop, 0] = '\c0%1 kills %2self with plasma.';
$DeathMessageSelfKill[$DamageType::BurnLoop, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::BurnLoop, 2] = '\c0%1 swallows a white-hot mouthful of %3 own plasma.';
$DeathMessageSelfKill[$DamageType::BurnLoop, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::BurnLoop, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::Disc, 0] = '\c0%1 kills %2self with a disc.';
$DeathMessageSelfKill[$DamageType::Disc, 1] = '\c0%1 catches %3 own spinfusor disc.';
$DeathMessageSelfKill[$DamageType::Disc, 2] = '\c0%1 heroically falls on %3 own disc.';
$DeathMessageSelfKill[$DamageType::Disc, 3] = '\c0%1 helpfully jumps into %3 own disc\'s explosion.';
$DeathMessageSelfKill[$DamageType::Disc, 4] = '\c0%1 plays Russian roulette with %3 spinfusor.';

$DeathMessageSelfKill[$DamageType::Grenade, 0] = '\c0%1 destroys %2self with a grenade!';   //applies to hand grenades *and* grenade launcher grenades
$DeathMessageSelfKill[$DamageType::Grenade, 1] = '\c0%1 took a bad bounce from %3 own grenade!';
$DeathMessageSelfKill[$DamageType::Grenade, 2] = '\c0%1 pulled the pin a shade early.';
$DeathMessageSelfKill[$DamageType::Grenade, 3] = '\c0%1\'s own grenade turns on %2.';
$DeathMessageSelfKill[$DamageType::Grenade, 4] = '\c0%1 blows %2self up real good.';

$DeathMessageSelfKill[$DamageType::Mortar, 0] = '\c0%1 kills %2self with a mortar!';
$DeathMessageSelfKill[$DamageType::Mortar, 1] = '\c0%1 hugs %3 own big green boomie.';
$DeathMessageSelfKill[$DamageType::Mortar, 2] = '\c0%1 mortars %2self all over the map.';
$DeathMessageSelfKill[$DamageType::Mortar, 3] = '\c0%1 experiences %3 mortar\'s payload up close.';
$DeathMessageSelfKill[$DamageType::Mortar, 4] = '\c0%1 suffered the wrath of %3 own mortar.';

$DeathMessageSelfKill[$DamageType::StarHammer, 0] = '\c0%1 kills %2self with a Star Hammer!';
$DeathMessageSelfKill[$DamageType::StarHammer, 1] = '\c0%1 destroys %2self with a starhammer.';
$DeathMessageSelfKill[$DamageType::StarHammer, 2] = '\c0%1 starhammers %2self to pieces.';
$DeathMessageSelfKill[$DamageType::StarHammer, 3] = '\c0%1 experiences %3 Star Hammer\'s payload up close.';
$DeathMessageSelfKill[$DamageType::StarHammer, 4] = '\c0%1 gracefully smoked %2self with a Star Hammer!.';

$DeathMessageSelfKill[$DamageType::Missile, 0] = '\c0%1 kills %2self with a missile!';
$DeathMessageSelfKill[$DamageType::Missile, 1] = '\c0%1 runs a missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::Missile, 2] = '\c0%1 tests the missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 3] = '\c0%1 achieved missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::Missile, 4] = '\c0%1 gracefully smoked %2self with a missile!';

$DeathMessageSelfKill[$DamageType::ElectroMissile, 0] = '\c0%1 kills %2self with an energy missile!';
$DeathMessageSelfKill[$DamageType::ElectroMissile, 1] = '\c0%1 runs an energy missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::ElectroMissile, 2] = '\c0%1 tests the missile\'s electrical charge on %2self.';
$DeathMessageSelfKill[$DamageType::ElectroMissile, 3] = '\c0%1 achieved an energy missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::ElectroMissile, 4] = '\c0%1 gracefully electrocuted %2self with an energy missile!';

$DeathMessageSelfKill[$DamageType::EMP, 0] = '\c0%1 gracefully electrocuted %2self with an EMP!';
$DeathMessageSelfKill[$DamageType::EMP, 1] = '\c0%1 feels the wrath of %3 own electromagnetical blast.';
$DeathMessageSelfKill[$DamageType::EMP, 2] = '\c0%1 kills %2self with an EMP!';
$DeathMessageSelfKill[$DamageType::EMP, 3] = '\c0%1 gracefully electrocuted %2self with an EMP!';
$DeathMessageSelfKill[$DamageType::EMP, 4] = '\c0%1 feels the wrath of %3 own electromagnetical blast.';

$DeathMessageSelfKill[$DamageType::Mine, 0] = '\c0%1 kills %2self with a mine!';
$DeathMessageSelfKill[$DamageType::Mine, 1] = '\c0%1\'s mine violently reminds %2 of its existence.';
$DeathMessageSelfKill[$DamageType::Mine, 2] = '\c0%1 plants a decisive foot on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 3] = '\c0%1 fatally trips on %3 own mine!';
$DeathMessageSelfKill[$DamageType::Mine, 4] = '\c0%1 makes a note not to run over %3 own mines.';

$DeathMessageSelfKill[$DamageType::BTMine, 0] = '\c0%1 kills %2self with a mine!';
$DeathMessageSelfKill[$DamageType::BTMine, 1] = '\c0%1\'s mine violently reminds %2 of its existence.';
$DeathMessageSelfKill[$DamageType::BTMine, 2] = '\c0%1 plants a decisive foot on %3 own mine!';
$DeathMessageSelfKill[$DamageType::BTMine, 3] = '\c0%1 fatally trips on %3 own mine!';
$DeathMessageSelfKill[$DamageType::BTMine, 4] = '\c0%1 makes a note not to run over %3 own mines.';

$DeathMessageSelfKill[$DamageType::SatchelCharge, 0] = '\c0%1 goes out with a bang!';  //applies to most explosion types
$DeathMessageSelfKill[$DamageType::SatchelCharge, 1] = '\c0%1 blows %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 3] = '\c0%1 experiences explosive decompression!';
$DeathMessageSelfKill[$DamageType::SatchelCharge, 4] = '\c0%1 splashes all over the map.';

$DeathMessageSelfKill[$DamageType::Flak, 0] = '\c0%1 goes out with a bang!';  //applies to most explosion types
$DeathMessageSelfKill[$DamageType::Flak, 1] = '\c0%1 blows %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::Flak, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::Flak, 3] = '\c0%1 caught %2self in fatal flak.';
$DeathMessageSelfKill[$DamageType::Flak, 4] = '\c0%1 flaks %2self all over the map.';

$DeathMessageSelfKill[$DamageType::Ground, 0] = '\c0%1 forgot to tie %3 bungie cord.';
$DeathMessageSelfKill[$DamageType::Ground, 1] = '\c0%1 finds gravity unforgiving.';
$DeathMessageSelfKill[$DamageType::Ground, 2] = '\c0%1 craters on impact.';
$DeathMessageSelfKill[$DamageType::Ground, 3] = '\c0%1 makes quite an impact upon the ground.';
$DeathMessageSelfKill[$DamageType::Ground, 4] = '\c0%1 loses a game of chicken with the ground.';

$DeathMessageSelfKill[$DamageType::MB, 0] = '\c0%1 lets %3 Mitzi Blast eat %2 alive.';
$DeathMessageSelfKill[$DamageType::MB, 1] = '\c0%1 was bit by %3 own Mitzi blast.';
$DeathMessageSelfKill[$DamageType::MB, 2] = '\c0%1 vaporizes %2self.';
$DeathMessageSelfKill[$DamageType::MB, 3] = '\c0%1\'s own Mitzi ran %2 down.';
$DeathMessageSelfKill[$DamageType::MB, 4] = '\c0%1 could not dodge the Mitzi Blast in time.';

$DeathMessageSelfKill[$DamageType::MitziBooster, 0] = '\c0%1 lets %3 Mitzi Blast eat %2 alive.';
$DeathMessageSelfKill[$DamageType::MitziBooster, 1] = '\c0%1 was bit by %3 own Mitzi blast.';
$DeathMessageSelfKill[$DamageType::MitziBooster, 2] = '\c0%1 vaporizes %2self.';
$DeathMessageSelfKill[$DamageType::MitziBooster, 3] = '\c0%1\'s own Mitzi ran %2 down.';
$DeathMessageSelfKill[$DamageType::MitziBooster, 4] = '\c0%1 could not dodge the Mitzi Blast in time.';

$DeathMessageSelfKill[$DamageType::IonCannon, 0] = '\c0%1 lets %3 Ion Cannon eat %2 alive.';
$DeathMessageSelfKill[$DamageType::IonCannon, 1] = '\c0%1 was zapped by %3 own Ion Cannon.';
$DeathMessageSelfKill[$DamageType::IonCannon, 2] = '\c0%1 gets lit up by %3 own Ion Cannon blast.';
$DeathMessageSelfKill[$DamageType::IonCannon, 3] = '\c0%1\'s own Ion Cannon ran %2 down.';
$DeathMessageSelfKill[$DamageType::IonCannon, 4] = '\c0%1 could not dodge the Ion Cannon blast in time.';

$DeathMessageSelfKill[$DamageType::Phaser, 0] = '\c0%1 experiences Phaser decompression.';
$DeathMessageSelfKill[$DamageType::Phaser, 1] = '\c0%1 gets lit up by %3 own Phaser-Compressed blast.';
$DeathMessageSelfKill[$DamageType::Phaser, 2] = '\c0%1 melts by %3 own Phaser.';
$DeathMessageSelfKill[$DamageType::Phaser, 3] = '\c0%1 gets lit up by %3 own Phaser-Compressed blast.';
$DeathMessageSelfKill[$DamageType::Phaser, 4] = '\c0%1 phasers %3self out of existance!';

//$DeathMessageSelfKill[$DamageType::PWT, 0] = '\c0%1 smacks %2self with a Pulse Wave Torpedo.';	// -[soph]
//$DeathMessageSelfKill[$DamageType::PWT, 1] = '\c0%1 is caught in the radius of %3 torpedo.';		// -
//$DeathMessageSelfKill[$DamageType::PWT, 2] = '\c0%1\'s Pulse Wave Torpedo turns on %2.';		// -
//$DeathMessageSelfKill[$DamageType::PWT, 3] = '\c0%1 is hit by %3 own ghetto depth charge.';		// -
//$DeathMessageSelfKill[$DamageType::PWT, 4] = '\c0%1 turned %2self to shrimp food.';			// -
//													// -
//$DeathMessageSelfKill[$DamageType::Disruptor, 0] = '\c0%1 blows %3 own brains out with a disruptor disc!';
//$DeathMessageSelfKill[$DamageType::Disruptor, 1] = '\c0%1 sent himself to the next dimension!';	// -
//$DeathMessageSelfKill[$DamageType::Disruptor, 2] = '\c0%1 gets lit up by %3 own Disruptor blast.';	// -
//$DeathMessageSelfKill[$DamageType::Disruptor, 3] = '\c0%1 kills %2self with a Disruptor.';		// -
//$DeathMessageSelfKill[$DamageType::Disruptor, 4] = '\c0%1 kills %2self with a Disruptor.';		// -[/soph]

$DeathMessageSelfKill[$DamageType::PoisonLoop, 0] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::PoisonLoop, 1] = '\c0%1 dies with green flesh open.';
$DeathMessageSelfKill[$DamageType::PoisonLoop, 2] = '\c0%1 heard "USE THE REPAIR KIT!" from far off.';
$DeathMessageSelfKill[$DamageType::PoisonLoop, 3] = '\c0%1 was killed by %3 own virus.';
$DeathMessageSelfKill[$DamageType::PoisonLoop, 4] = '\c0%1 was poisoned.';

$DeathMessageSelfKill[$DamageType::EnergyBlast, 0] = '\c0%1 blows %3 own brains out with an energy blast!';
$DeathMessageSelfKill[$DamageType::EnergyBlast, 1] = '\c0%1 learns not to play with concentrated energy.';
$DeathMessageSelfKill[$DamageType::EnergyBlast, 2] = '\c0%1 took a heaping helping of energy.';
$DeathMessageSelfKill[$DamageType::EnergyBlast, 3] = '\c0%1 kills %2self with an Energy Rifle.';
$DeathMessageSelfKill[$DamageType::EnergyBlast, 4] = '\c0%1 overloaded %3 own reactor with an blast of energy.';

$DeathMessageSelfKill[$DamageType::Gauss, 0] = '\c0%1 blows %2self to pieces with %3 mass driver.';
$DeathMessageSelfKill[$DamageType::Gauss, 1] = '\c0%1 blew %3 own brain by mass driver bolts.';
$DeathMessageSelfKill[$DamageType::Gauss, 2] = '\c0%1 learns that mass driver can hurt.';
$DeathMessageSelfKill[$DamageType::Gauss, 3] = '\c0%1 got too close to %3 mass driver explosion.';
$DeathMessageSelfKill[$DamageType::Gauss, 4] = '\c0%1 blew %3 own brain by mass driver bolts.';

$DeathMessageSelfKill[$DamageType::AutoCannon, 0] = '\c0%1 blows %2self to pieces with %3 autocannon bolt.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 1] = '\c0%1 blew %3 own brain by autocannon bolt.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 2] = '\c0%1 learns that autocannon bolts can hurt.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 3] = '\c0%1 got too close to %3 autocannon explosion.';
$DeathMessageSelfKill[$DamageType::AutoCannon, 4] = '\c0%1 blew %3 own brain by autocannon bolts.';

$DeathMessageSelfKill[$DamageType::Annihalator, 0] = '\c0%1 nukes %2self with %3 Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by a nearby Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 2] = '\c0%1 used the force on %2self.';
$DeathMessageSelfKill[$DamageType::Annihalator, 3] = '\c0%1 nukes %2self with %3 Annihalator.';
$DeathMessageSelfKill[$DamageType::Annihalator, 4] = '\c0%1 is broken down into %3 component atoms by %3 own Annihalator!';

$DeathMessageSelfKill[$DamageType::Ecstacy, 0] = '\c0%1 lunches on %3 own Ecstacy sandwich.';
$DeathMessageSelfKill[$DamageType::Ecstacy, 1] = '\c0%1 was caught in %3 Ecstacy blast radius.';
$DeathMessageSelfKill[$DamageType::Ecstacy, 2] = '\c0%1 gets drilled big-time by %3 own Ecstacy.';
$DeathMessageSelfKill[$DamageType::Ecstacy, 3] = '\c0%1 takes in a lil too much Ecstacy.';
$DeathMessageSelfKill[$DamageType::Ecstacy, 4] = '\c0%1 wanted to make sure %3 Ecstacy was charged.';

$DeathMessageSelfKill[$DamageType::BellyTurret, 0] = '\c0%1 blows %3 skull clean off with the pulse cannon.';
$DeathMessageSelfKill[$DamageType::BellyTurret, 1] = '\c0%1 smacks %2self with the frying pans.';
$DeathMessageSelfKill[$DamageType::BellyTurret, 2] = '\c0%1 headbangs with %3 frying pans.';
$DeathMessageSelfKill[$DamageType::BellyTurret, 3] = '\c0%1 wanted to make sure %2 pulse cannons were loaded.';
$DeathMessageSelfKill[$DamageType::BellyTurret, 4] = '\c0%1 sorely missed with the pulse cannons.';

$DeathMessageSelfKill[$DamageType::PAC, 0] = '\c0%1 blows %3 own brains out with a PAC fusion pulse blast!';
$DeathMessageSelfKill[$DamageType::PAC, 1] = '\c0%1 had %3 PAC facing the wrong way.';
$DeathMessageSelfKill[$DamageType::PAC, 2] = '\c0%1 shows off %3 mad dying skills!';
$DeathMessageSelfKill[$DamageType::PAC, 3] = '\c0%1 wanted to make sure %3 PAC was charged.';
$DeathMessageSelfKill[$DamageType::PAC, 4] = '\c0%1 was last heard shouting "death before dishonor!!"';

$DeathMessageSelfKill[$DamageType::MitziTransparent, 0] = '\c0%1 lets %3 Mitzi Blast eat %2 alive.';
$DeathMessageSelfKill[$DamageType::MitziTransparent, 1] = '\c0%1 was bit by %3 own Mitzi blast.';
$DeathMessageSelfKill[$DamageType::MitziTransparent, 2] = '\c0%1 vaporizes %2self.';
$DeathMessageSelfKill[$DamageType::MitziTransparent, 3] = '\c0%1\'s own Mitzi ran %2 down.';
$DeathMessageSelfKill[$DamageType::MitziTransparent, 4] = '\c0%1 could not dodge the Mitzi Blast in time.';

$DeathMessageSelfKill[$DamageType::Shocklance, 0] = '\c0%1 reaps a harvest of %2self with the shock blaster.';
$DeathMessageSelfKill[$DamageType::Shocklance, 1] = '\c0%1 takes in a mouthful of shock blaster.';
$DeathMessageSelfKill[$DamageType::Shocklance, 2] = '\c0%1 self-serves %2self some shock blaster powerup.';
$DeathMessageSelfKill[$DamageType::Shocklance, 3] = '\c0%1 eliminates %2self in close combat.';
$DeathMessageSelfKill[$DamageType::Shocklance, 4] = '\c0%1 stuck %3 finger in %3 reactor port.';

$DeathMessageSelfKill[$DamageType::MortarTurret, 0] = '\c0%1 kills %2self with a heavy mortar!';
$DeathMessageSelfKill[$DamageType::MortarTurret, 1] = '\c0%1 hugs %3 own big green boomie.';
$DeathMessageSelfKill[$DamageType::MortarTurret, 2] = '\c0%1 heavy mortars %2self all over the map.';
$DeathMessageSelfKill[$DamageType::MortarTurret, 3] = '\c0%1 experiences %3 heavy mortar\'s payload up close.';
$DeathMessageSelfKill[$DamageType::MortarTurret, 4] = '\c0%1 suffered the wrath of %3 own heavy mortar.';

$DeathMessageSelfKill[$DamageType::PlasmaTurret, 0] = '\c0%1 kills %2self with a heavy plasma cannon.';
$DeathMessageSelfKill[$DamageType::PlasmaTurret, 1] = '\c0%1 turns %2self into plasma-charred briquettes.';
$DeathMessageSelfKill[$DamageType::PlasmaTurret, 2] = '\c0%1 swallows a white-hot mouthful of %3 own heavy plasma.';
$DeathMessageSelfKill[$DamageType::PlasmaTurret, 3] = '\c0%1 immolates %2self.';
$DeathMessageSelfKill[$DamageType::PlasmaTurret, 4] = '\c0%1 experiences the joy of cooking %2self.';

$DeathMessageSelfKill[$DamageType::MissileTurret, 0] = '\c0%1 kills %2self with a turret missile!';
$DeathMessageSelfKill[$DamageType::MissileTurret, 1] = '\c0%1 runs a turret missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::MissileTurret, 2] = '\c0%1 tests the turret missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::MissileTurret, 3] = '\c0%1 achieved turret missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::MissileTurret, 4] = '\c0%1 gracefully smoked %2self with a turret missile!';

$DeathMessageSelfKill[$DamageType::OutdoorDepTurret, 0] = '\c0%1 pokes %2self in a nono place with the spike.';
$DeathMessageSelfKill[$DamageType::OutdoorDepTurret, 1] = '\c0%1 spikes %2self.';
$DeathMessageSelfKill[$DamageType::OutdoorDepTurret, 2] = '\c0%1 manages to kill %2self with the spike.';
$DeathMessageSelfKill[$DamageType::OutdoorDepTurret, 3] = '\c0%1 feels the burn from %3 own spike.';
$DeathMessageSelfKill[$DamageType::OutdoorDepTurret, 4] = '\c0%1 spikes %3 mind.';

$DeathMessageSelfKill[$DamageType::DaishiReactor, 0] = '\c0%1 overloaded and exploded.';		// '\c0%1 goes out with a big explosive bang!'; -soph
$DeathMessageSelfKill[$DamageType::DaishiReactor, 1] = '\c0%1 blew a gasket.';				// '\c0%1 was feeling rather volatile today.'; -soph
$DeathMessageSelfKill[$DamageType::DaishiReactor, 2] = '\c0%1 went critical.';				// '\c0%1 gives %2self a super explosive hug!'; -soph
$DeathMessageSelfKill[$DamageType::DaishiReactor, 3] = '\c0%1 suffered a core breach and melted down.';	// '\c0%1\'s explosive outburst caught %2 with %3 pants down.';
$DeathMessageSelfKill[$DamageType::DaishiReactor, 4] = '\c0%1 couldn\'t take the heat.';		// '\c0%1 is instantly vaporized by %3 own explosive outburst.'; -soph	

//used when a player is killed by a teammate
$DeathMessageTeamKillCount = 1;	
$DeathMessageTeamKill[$DamageType::Blaster, 0] = '\c0%4 TEAMKILLED %1 with a blaster!';
$DeathMessageTeamKill[$DamageType::BlueBlaster, 0] = '\c0%4 TEAMKILLED %1 with a blue blaster!';
$DeathMessageTeamKill[$DamageType::Plasma, 0] = '\c0%4 TEAMKILLED %1 with a plasma rifle!';
$DeathMessageTeamKill[$DamageType::Bullet, 0] = '\c0%4 TEAMKILLED %1 with a chaingun!';
$DeathMessageTeamKill[$DamageType::ChargedBullet, 0] = '\c0%4 TEAMKILLED %1 with a chaingun!';
$DeathMessageTeamKill[$DamageType::Disc, 0] = '\c0%4 TEAMKILLED %1 with a spinfusor!';
$DeathMessageTeamKill[$DamageType::Grenade, 0] = '\c0%4 TEAMKILLED %1 with a grenade!';
$DeathMessageTeamKill[$DamageType::Laser, 0] = '\c0%4 TEAMKILLED %1 with a laser rifle!';
$DeathMessageTeamKill[$DamageType::Elf, 0] = '\c0%4 TEAMKILLED %1 with an ELF projector!';
$DeathMessageTeamKill[$DamageType::Mortar, 0] = '\c0%4 TEAMKILLED %1 with a mortar!';
$DeathMessageTeamKill[$DamageType::Missile, 0] = '\c0%4 TEAMKILLED %1 with a missile!';
$DeathMessageTeamKill[$DamageType::Shocklance, 0] = '\c0%4 TEAMKILLED %1 with a shocklance!';
$DeathMessageTeamKill[$DamageType::Mine, 0] = '\c0%4 TEAMKILLED %1 with a mine!';
$DeathMessageTeamKill[$DamageType::BTMine, 0] = '\c0%4 TEAMKILLED %1 with a mine!';
$DeathMessageTeamKill[$DamageType::SatchelCharge, 0] = '\c0%4 blew up TEAMMATE %1!';
$DeathMessageTeamKill[$DamageType::Impact, 0] = '\c0%4 runs down TEAMMATE %1!';

$DeathMessageTeamKill[$DamageType::MB, 0] = '\c0%4 TEAMKILLED %1 with a Mitzi Blast Cannon!';
$DeathMessageTeamKill[$DamageType::DenseMitzi, 0] = '\c0%4 TEAMKILLED %1 with a Mitzi Blast Cannon!';
$DeathMessageTeamKill[$DamageType::MitziBooster, 0] = '\c0%4 TEAMKILLED %1 with a Mitzi Blast Cannon!';
$DeathMessageTeamKill[$DamageType::MitziTransparent, 0] = '\c0%4 TEAMKILLED %1 with a Mitzi Blast Cannon!';
$DeathMessageTeamKill[$DamageType::PBW, 0] = '\c0%4 TEAMKILLED %1 with a PBW!';
$DeathMessageTeamKill[$DamageType::Phaser, 0] = '\c0%4 TEAMKILLED %1 with a Phaser Compression Rifle!';
$DeathMessageTeamKill[$DamageType::Sniper, 0] = '\c0%4 TEAMKILLED %1 with a Sniper Rifle!';
$DeathMessageTeamKill[$DamageType::ElectroMissile, 0] = '\c0%4 TEAMKILLED %1 with an Energy Missile!';
$DeathMessageTeamKill[$DamageType::PWT, 0] = '\c0%4 TEAMKILLED %1 with a PWT Launcher!';
$DeathMessageTeamKill[$DamageType::Ripper, 0] = '\c0%4 TEAMKILLED %1 with a Ripper!';
$DeathMessageTeamKill[$DamageType::EnergyBlast, 0] = '\c0%4 TEAMKILLED %1 with an Energy Rifle!';
$DeathMessageTeamKill[$DamageType::Gauss, 0] = '\c0%4 TEAMKILLED %1 with a Mass Driver!';
$DeathMessageTeamKill[$DamageType::AutoCannon, 0] = '\c0%4 TEAMKILLED %1 with an AutoCannon Bolt!';
$DeathMessageTeamKill[$DamageType::Annihalator, 0] = '\c0%4 TEAMKILLED %1 with an Annihalator!';
$DeathMessageTeamKill[$DamageType::Burn, 0] = '\c0%4 TEAMKILLED %1 with a Fire!';
$DeathMessageTeamKill[$DamageType::Poison, 0] = '\c0%4 TEAMKILLED %1 with a Poison Dose!';
$DeathMessageTeamKill[$DamageType::BellyTurret, 0] = '\c0%4 TEAMKILLED %1 by a pulse blast.';
$DeathMessageTeamKill[$DamageType::Ecstacy, 0] = '\c0%4 TEAMKILLED %1 by ecstacy.';
$DeathMessageTeamKill[$DamageType::Protron, 0] = '\c0%4 TEAMKILLED %1 with a Protron.';
$DeathMessageTeamKill[$DamageType::PlasmaCannon, 0] = '\c0%4 TEAMKILLED %1 with a Plasma Cannon.';
$DeathMessageTeamKill[$DamageType::ProtronFire, 0] = '\c0%4 TEAMKILLED %1 with a fiery Protron.';	// +soph
$DeathMessageTeamKill[$DamageType::Pulse, 0] = '\c0%4 TEAMKILLED %1 with a Pulse bolt.';
$DeathMessageTeamKill[$DamageType::Burn, 0] = '\c0%4 TEAMKILLED %1 with a Plasma Gun.';
$DeathMessageTeamKill[$DamageType::Flak, 0] = '\c0%4 TEAMKILLED %1 with fatal flak!';
$DeathMessageTeamKill[$DamageType::IndoorDepTurret, 0] = '\c0%4 TEAMKILLED %1 with shotgun pellets.';
$DeathMessageTeamKill[$DamageType::MortarTurret, 0] = '\c0%4 TEAMKILLED %1 with a heavy mortar.';	// +[soph]
$DeathMessageTeamKill[$DamageType::RAXX, 0] = '\c0%4 TEAMKILLED %1 with a flamethrower.';
$DeathMessageTeamKill[$DamageType::ElectroMissile, 0] = '\c0%4 TEAMKILLED %1 with an energy missile.';
$DeathMessageTeamKill[$DamageType::MechRocket, 0] = '\c0%4 TEAMKILLED %1 with a mech rocket.' ;		// '\c0%4 TEAMKILLED %1 with an energy missile.'; -soph
$DeathMessageTeamKill[$DamageType::MechHowitzer, 0] = '\c0%4 TEAMKILLED %1 with a howitzer rocket.' ;	// '\c0%4 TEAMKILLED %1 with an energy missile.'; -soph
$DeathMessageTeamKill[$DamageType::CometCannon, 0] = '\c0%4 TEAMKILLED %1 with a comet blast.' ;	// '\c0%4 TEAMKILLED %1 with a starhammmer.'; -soph
$DeathMessageTeamKill[$DamageType::Starhammer, 0] = '\c0%4 TEAMKILLED %1 with a starhammmer.';		// +
$DeathMessageTeamKill[ $DamageType::MechStomp , 0 ] = '\c0%4 TEAMKILLED %1 with a mech-sized boot.' ;	// +[/soph]

//these used when a player is killed by an enemy
$DeathMessageCount = 5;
$DeathMessage[$DamageType::Blaster, 0] = '\c0%4 kills %1 with a blaster.';
$DeathMessage[$DamageType::Blaster, 1] = '\c0%4 pings %1 to death.';
$DeathMessage[$DamageType::Blaster, 2] = '\c0%1 gets a pointer in blaster use from %4.';
$DeathMessage[$DamageType::Blaster, 3] = '\c0%4 fatally embarrasses %1 with %6 pea shooter.';
$DeathMessage[$DamageType::Blaster, 4] = '\c0%4 unleashes a terminal blaster barrage into %1.';

$DeathMessageSelfKill[$DamageType::Pulse, 0] =  '\c0%1 plays with %3 pulse waves in a lethal way.';	// '\c0%1 plays with %3 pulse bolts in a lethal way.'; -soph
$DeathMessageSelfKill[$DamageType::Pulse, 1] =  '\c0%1 learns what those pretty azure lights were.';	// '\c0%1 learns what those pretty green lights were.'; -soph
$DeathMessageSelfKill[$DamageType::Pulse, 2] =  '\c0%1 dances in a electric blue shower.';		// '\c0%1 dances in a shower of green.'; -soph
$DeathMessageSelfKill[$DamageType::Pulse, 3] =  '\c0%1 is the missing link.';
$DeathMessageSelfKill[$DamageType::Pulse, 4] =  '\c0%1 bathes in a pool of %3 own turquoise death.';	// '\c0%1 swallows a green barrage of %3 own link bolts.'; -soph

$DeathMessage[$DamageType::Pulse, 0] = '\c0%4 drowns %1 in a flood of pulse waves.' ;			// '\c0%4 somehow kills %1 with a barrage of pulse bolts.'; -soph
$DeathMessage[$DamageType::Pulse, 1] = '\c0%4 shows %1 the ultramarine side of death.';			// '\c0%4 shows %1 the green side of death.'; -soph
$DeathMessage[$DamageType::Pulse, 2] = '\c0%4 serves %1 a cerulean feast.' ;				// '\c0%4 invites %1 to a green feast.'; -soph
$DeathMessage[$DamageType::Pulse, 3] = '\c0%1 drowns in %4\'s pulse bath.' ;				// '\c0%1 gets pulsed out of existance thanks to %4.'; -soph
$DeathMessage[$DamageType::Pulse, 4] = '\c0%4 leaves %1 singing the blues.' ;				// '\c0%1 gets pelted from %4\'s pulse bolts.'; -soph

$DeathMessage[$DamageType::RMSS, 0] = '\c0%1 did not notice the missile swarm in time.';		// +[soph]
$DeathMessage[$DamageType::RMSS, 1] = '\c0%1 should have paid more attention to that yellow beam.';
$DeathMessage[$DamageType::RMSS, 2] = '\c0%1 failed to vacate the cruise missile strike zone.';
$DeathMessage[$DamageType::RMSS, 3] = '\c0%1 got caught in the missile rain without %3 umbrella.';
$DeathMessage[$DamageType::RMSS, 4] = '\c0%1 was obliterated by base-to-base cruise missiles.';		// +[/soph]

$DeathMessage[$DamageType::Protron, 0] = '\c0%4 kills %1 with a Protron.';
$DeathMessage[$DamageType::Protron, 1] = '\c0%1 gets bombarded by %4\'s Protron.';
$DeathMessage[$DamageType::Protron, 2] = '\c0%1 gets a pointer in Protron use from %4.';
$DeathMessage[$DamageType::Protron, 3] = '\c0%4 entices %1 to try a faceful of Protron.';
$DeathMessage[$DamageType::Protron, 4] = '\c0%4 unleashes a terminal Protron barrage into %1.';

$DeathMessage[$DamageType::Ripper, 0] = '\c0%1 gets sliced up with %4\'s ripper.';
$DeathMessage[$DamageType::Ripper, 1] = '\c0%4 feeds %1 the business end of %6\ Ripper.';	// removed %6 possessive -soph
$DeathMessage[$DamageType::Ripper, 2] = '\c0%4\'s Ripper chops %1 in half.';
$DeathMessage[$DamageType::Ripper, 3] = '\c0%4 chops %1 to pieces in close combat.';
$DeathMessage[$DamageType::Ripper, 4] = '\c0%1 played dodgeball with a Ripper Disc... and lost.';

$DeathMessage[$DamageType::BlueBlaster, 0] = '\c0%4 kills %1 with a blue blaster.';
$DeathMessage[$DamageType::BlueBlaster, 1] = '\c0%4 blasters %1 right into the ground.';
$DeathMessage[$DamageType::BlueBlaster, 2] = '\c0%1 gets a pointer in blue blaster use from %4.';
$DeathMessage[$DamageType::BlueBlaster, 3] = '\c0%4 fatally embarrasses %1 with %6 pea shooter.';
$DeathMessage[$DamageType::BlueBlaster, 4] = '\c0%4 unleashes a terminal blue blaster barrage into %1.';

$DeathMessage[$DamageType::Plasma, 0] = '\c0%4 roasts %1 with the plasma rifle.';
$DeathMessage[$DamageType::Plasma, 1] = '\c0%4 asks %1: "Need a light?"';
$DeathMessage[$DamageType::Plasma, 2] = '\c0%4 entices %1 to try a faceful of plasma.';
$DeathMessage[$DamageType::Plasma, 3] = '\c0%4 introduces %1 to the plasma immolation dance.';
$DeathMessage[$DamageType::Plasma, 4] = '\c0%4 slaps The Hot Kiss of Death on %1.';

$DeathMessage[$DamageType::RAXX, 0] = '\c0%4 roasts %1 with the RAXX.';
$DeathMessage[$DamageType::RAXX, 1] = '\c0%4 gooses %1 with %6 RAXX.';
$DeathMessage[$DamageType::RAXX, 2] = '\c0%4 entices %1 to check %6 pilot light.';
$DeathMessage[$DamageType::RAXX, 3] = '\c0%4 introduces %1 to 5000 degrees of fun.';
$DeathMessage[$DamageType::RAXX, 4] = '\c0%1 steps into %4\'s stream of flames.';

$DeathMessage[$DamageType::FlameTurret, 0] = '\c0%4 roasts %1 with the RAXX Turret.';
$DeathMessage[$DamageType::FlameTurret, 1] = '\c0%4 gooses %1 with %6 RAXX Turret.';
$DeathMessage[$DamageType::FlameTurret, 2] = '\c0%4 entices %1 to check %6 pilot light.';
$DeathMessage[$DamageType::FlameTurret, 3] = '\c0%4 introduces %1 to 10000 degrees of fun.';
$DeathMessage[$DamageType::FlameTurret, 4] = '\c0%1 steps into %4\'s stream of flames.';

$DeathMessage[$DamageType::Burn, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessage[$DamageType::Burn, 1] = '\c0%1 forgot to put out %4\'s flames.';
$DeathMessage[$DamageType::Burn, 2] = '\c0%4 sets %1 on fire.';
$DeathMessage[$DamageType::Burn, 3] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';
$DeathMessage[$DamageType::Burn, 4] = '\c0%4 slaps The Hot Kiss of Death on %1.';

$DeathMessage[$DamageType::BurnLoop, 0] = '\c0%4 converts %1 into a living bonfire.';
$DeathMessage[$DamageType::BurnLoop, 1] = '\c0%1 forgot to put out %4\'s flames.';
$DeathMessage[$DamageType::BurnLoop, 2] = '\c0%4 sets %1 on fire.';
$DeathMessage[$DamageType::BurnLoop, 3] = '\c0%4 roasts some s\'mores over %1\'s burning corpse.';
$DeathMessage[$DamageType::BurnLoop, 4] = '\c0%4 slaps The Hot Kiss of Death on %1.';

$DeathMessage[$DamageType::Bullet, 0] = '\c0%4 rips %1 up with the chaingun.';
$DeathMessage[$DamageType::Bullet, 1] = '\c0%4 happily chews %1 into pieces with %6 chaingun.';
$DeathMessage[$DamageType::Bullet, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';
$DeathMessage[$DamageType::Bullet, 3] = '\c0%1 suffers a serious hosing from %4\'s chaingun.';
$DeathMessage[$DamageType::Bullet, 4] = '\c0%4 bestows the blessings of %6 chaingun on %1.';

$DeathMessage[$DamageType::ChargedBullet, 0] = '\c0%4 rips %1 up with %6 charged chaingun.';
$DeathMessage[$DamageType::ChargedBullet, 1] = '\c0%4 happily chews %1 into charred pieces with %6 chaingun.';
$DeathMessage[$DamageType::ChargedBullet, 2] = '\c0%4 administers a dose of charged lead to %1.';
$DeathMessage[$DamageType::ChargedBullet, 3] = '\c0%1 suffers a serious hosing from %4\'s electro chaingun.';
$DeathMessage[$DamageType::ChargedBullet, 4] = '\c0%4 bestows the blessings of %6 electro chaingun on %1.';

$DeathMessage[$DamageType::IndoorDepTurret, 0] = '\c0%4 rips %1 up with the Shotgun.';
$DeathMessage[$DamageType::IndoorDepTurret, 1] = '\c0%1 lets freedom ring with a Shotgun blast from %4.';
$DeathMessage[$DamageType::IndoorDepTurret, 2] = '\c0%4 flaks %1 with a wall of Clamp bolts.';
$DeathMessage[$DamageType::IndoorDepTurret, 3] = '\c0%1 becomes %4\'s latest clamp target.';
$DeathMessage[$DamageType::IndoorDepTurret, 4] = '\c0%4 decimates %1 with the Shotgun.';

$DeathMessage[$DamageType::Disc, 0] = '\c0%4 demolishes %1 with the spinfusor.';
$DeathMessage[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessage[$DamageType::Disc, 2] = '\c0%4 shares a little blue friend with %1.';
$DeathMessage[$DamageType::Disc, 3] = '\c0%4 puts a little spin into %1.';
$DeathMessage[$DamageType::Disc, 4] = '\c0%1 becomes one of %4\'s greatest hits.';

$DeathMessage[$DamageType::Grenade, 0] = '\c0%4 eliminates %1 with a grenade.';   //applies to hand grenades *and* grenade launcher grenades
$DeathMessage[$DamageType::Grenade, 1] = '\c0%4 blows up %1 real good!';
$DeathMessage[$DamageType::Grenade, 2] = '\c0%1 gets annihilated by %4\'s grenade.';
$DeathMessage[$DamageType::Grenade, 3] = '\c0%1 receives a kaboom lesson from %4.';
$DeathMessage[$DamageType::Grenade, 4] = '\c0%4 turns %1 into grenade salad.'; 

$DeathMessage[$DamageType::Laser, 0] = '\c0%1 becomes %4\'s latest pincushion.';
$DeathMessage[$DamageType::Laser, 1] = '\c0%4 picks off %1 with %6 laser rifle.';
$DeathMessage[$DamageType::Laser, 2] = '\c0%4 uses %1 as the targeting dummy in a sniping demonstration.';						
$DeathMessage[$DamageType::Laser, 3] = '\c0%1 finally realized what that red dot was.';
$DeathMessage[$DamageType::Laser, 4] = '\c0%4 Lightly Amplifies %1\'s Stimulated Emission of Radiation.';

$DeathMessage[$DamageType::Elf, 0] = '\c0%4 fries %1 with the ELF projector.';
$DeathMessage[$DamageType::Elf, 1] = '\c0%4 bug zaps %1 with %6 ELF.';
$DeathMessage[$DamageType::Elf, 2] = '\c0%1 learns the shocking truth about %4\'s ELF skills.';
$DeathMessage[$DamageType::Elf, 3] = '\c0%4 electrocutes %1 without a sponge.';
$DeathMessage[$DamageType::Elf, 4] = '\c0%4\'s ELF projector leaves %1 a crispy critter.';

$DeathMessage[$DamageType::Nosferatu, 0] = '\c0%4 ended %1.' ;		// '\c0%4 strips %1\'s armor with the Nosferatu.';		-[soph]
$DeathMessage[$DamageType::Nosferatu, 1] = '\c0%4 destroyed %1.' ;	// '\c0%4 sucks away %1\'s armor with %6 Nosferatu.';		now used as emergency default deathmessages
$DeathMessage[$DamageType::Nosferatu, 2] = '\c0%4 murdered %1.' ;	// '\c0%1 loses %3 essence to %4\'s Nosferatu.';
$DeathMessage[$DamageType::Nosferatu, 3] = '\c0%4 made %1 dead.' ;	// '\c0%4 steals %1\'s soul.';
$DeathMessage[$DamageType::Nosferatu, 4] = '\c0%4 put %1 down.' ;	// '\c0%4\'s Nosferatu transforms %1 into a pile of dust.';	-[/soph]

$DeathMessage[$DamageType::EMP, 0] = '\c0%4 fries %1 with an EMP blast.';
$DeathMessage[$DamageType::EMP, 1] = '\c0%4 zaps %1 with %6 EMP.';
$DeathMessage[$DamageType::EMP, 2] = '\c0%1 learns the shocking truth about %4\'s nearby EMP.';
$DeathMessage[$DamageType::EMP, 3] = '\c0%4 electrocutes %1 without a sponge.';
$DeathMessage[$DamageType::EMP, 4] = '\c0%4\'s EMP blast leaves %1 a crispy critter.';

$DeathMessage[$DamageType::IonCannon, 0] = '\c0%4 fries %1 with the orbital Ion Cannon.';			// '\c0%4 fries %1 with the MANTA Ion Cannon.'; -soph
$DeathMessage[$DamageType::IonCannon, 1] = '\c0%1 gets annihilated by %4\'s Ion Cannon.';
$DeathMessage[$DamageType::IonCannon, 2] = '\c0%1 learns the shocking truth about %4\'s orbital Ion Cannon.';	// '\c0%1 learns the shocking truth about %4\'s MANTA Ion Cannon.'; -soph
$DeathMessage[$DamageType::IonCannon, 3] = '\c0%4 delivers an Ion blast straight to %1.';
$DeathMessage[$DamageType::IonCannon, 4] = '\c0%4\'s Ion Cannon leaves %1 a crispy critter.';

$DeathMessage[$DamageType::Mortar, 0] = '\c0%4 obliterates %1 with the mortar.';
$DeathMessage[$DamageType::Mortar, 1] = '\c0%4 drops a mortar round right in %1\'s lap.';
$DeathMessage[$DamageType::Mortar, 2] = '\c0%4 delivers a mortar payload straight to %1.';
$DeathMessage[$DamageType::Mortar, 3] = '\c0%4 offers a little "heavy love" to %1.';
$DeathMessage[$DamageType::Mortar, 4] = '\c0%1 stumbles into %4\'s mortar reticle.';

$DeathMessage[$DamageType::StarHammer, 0] = '\c0%4 obliterates %1 with the Star Hammer.';
$DeathMessage[$DamageType::StarHammer, 1] = '\c0%4 chalks up another Star Hammer kill, courtesy of %1.';
$DeathMessage[$DamageType::StarHammer, 2] = '\c0%4 delivers a Star Hammer blast straight to %1.';
$DeathMessage[$DamageType::StarHammer, 3] = '\c0%4\'s Star Hammer blast leaves %1 nothin\' but smokin\' boots.';
$DeathMessage[$DamageType::StarHammer, 4] = '\c0%4 hammers %1 gooooood.';

$DeathMessage[ $DamageType::BlastHammer , 0 ] = '\c0%4 obliterates %1 with whatever the hell that was.' ;	// +[soph]
$DeathMessage[ $DamageType::BlastHammer , 1 ] = '\c0%4 makes a bloody smear of %1 .' ;				// +
$DeathMessage[ $DamageType::BlastHammer , 2 ] = '\c0%4 brings down the point-blank HOLY SHIT down on %1.' ;	// +
$DeathMessage[ $DamageType::BlastHammer , 3 ] = '\c0%4 explodes, %1 dies.' ;					// +
$DeathMessage[ $DamageType::BlastHammer , 4 ] = '\c0%4 goes nuclear and makes %1 very, VERY dead.' ;		// +[/soph]

$DeathMessage[$DamageType::Missile, 0] = '\c0%4 intercepts %1 with a missile.';
$DeathMessage[$DamageType::Missile, 1] = '\c0%4 watches %6 missile touch %1 and go boom.';
$DeathMessage[$DamageType::Missile, 2] = '\c0%1 rides %4\'s rocket.';
$DeathMessage[$DamageType::Missile, 3] = '\c0%1 took %4\'s missile where the sun don\'t shine.';
$DeathMessage[$DamageType::Missile, 4] = '\c0%4\'s missile rains little pieces of %1 all over the ground.';

$DeathMessage[$DamageType::ElectroMissile, 0] = '\c0%4 intercepts %1 with an energy missile.';
$DeathMessage[$DamageType::ElectroMissile, 1] = '\c0%4 watches %6 energy missile touch %1 and go boom.';
$DeathMessage[$DamageType::ElectroMissile, 2] = '\c0%1 rides %4\'s electro-rocket.';
$DeathMessage[$DamageType::ElectroMissile, 3] = '\c0%1 gets buttraped by %4\'s energy missile.';
$DeathMessage[$DamageType::ElectroMissile, 4] = '\c0%4\'s energy missile electrocutes %1 without a sponge.';

$DeathMessage[$DamageType::Flak, 0] = '\c0%4 intercepts %1 with flak.';
$DeathMessage[$DamageType::Flak, 1] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessage[$DamageType::Flak, 2] = '\c0%1 rides %4\'s flak charge.';
$DeathMessage[$DamageType::Flak, 3] = '\c0%4 delivers a flak payload straight to %1.';
$DeathMessage[$DamageType::Flak, 4] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';

$DeathMessage[$DamageType::Shocklance, 0] = '\c0%4 reaps a harvest of %1 with the shocklance.';
$DeathMessage[$DamageType::Shocklance, 1] = '\c0%4 feeds %1 the business end of %6 shocklance.';
$DeathMessage[$DamageType::Shocklance, 2] = '\c0%4 stops %1 dead with the shocklance.';
$DeathMessage[$DamageType::Shocklance, 3] = '\c0%4 eliminates %1 in close combat.';
$DeathMessage[$DamageType::Shocklance, 4] = '\c0%4 ruins %1\'s day with one zap of a shocklance.';

$DeathMessage[$DamageType::Mine, 0] = '\c0%4 reminds %1 that a mine is a terrible thing to waste.';
$DeathMessage[$DamageType::Mine, 1] = '\c0%1 doesnt see %4\'s mine in time.';
$DeathMessage[$DamageType::Mine, 2] = '\c0%4 gives %1 a piece of %6 mine.';
$DeathMessage[$DamageType::Mine, 3] = '\c0%1 puts his foot on %4\'s mine.';
$DeathMessage[$DamageType::Mine, 4] = '\c0%1 stepped on a toe-popper.';

$DeathMessage[$DamageType::BTMine, 0] = '\c0%4 reminds %1 that an anti-blastech mine is a terrible thing to waste.';
$DeathMessage[$DamageType::BTMine, 1] = '\c0%1 doesnt see %4\'s anti-blastech mine in time.';
$DeathMessage[$DamageType::BTMine, 2] = '\c0%4 gives %1 a piece of %6 anti-blastech mine.';
$DeathMessage[$DamageType::BTMine, 3] = '\c0%1 puts his foot on %4\'s anti-blastech mine.';
$DeathMessage[$DamageType::BTMine, 4] = '\c0%1 stepped on an anti-blastech toe-popper.';

$DeathMessage[$DamageType::MitziTransparent, 0] = '\c0%1 is the victim of %4\'s Mitzi Blast.';
$DeathMessage[$DamageType::MitziTransparent, 1] = '\c0%4 gets under %1\'s shields with a Mitzi Transparency Blast.';
$DeathMessage[$DamageType::MitziTransparent, 2] = '\c0%4 stops %1 dead with the Mitzi Blast Cannon.';
$DeathMessage[$DamageType::MitziTransparent, 3] = '\c0%4 says to %1: "Mitzi. Kills bugs dead."!';
$DeathMessage[$DamageType::MitziTransparent, 4] = '\c0%4 jams some Mitzi in  %1\'s face.';

$DeathMessage[$DamageType::SatchelCharge, 0] = '\c0%4 buys %1 a ticket to the moon.';  //satchel charge only
$DeathMessage[$DamageType::SatchelCharge, 1] = '\c0%4 blows %1 into tiny bits.';
$DeathMessage[$DamageType::SatchelCharge, 2] = '\c0%4 makes %1 a hugely explosive offer.';
$DeathMessage[$DamageType::SatchelCharge, 3] = '\c0%4 turns %1 into a cloud of satchel-vaporized armor.';
$DeathMessage[$DamageType::SatchelCharge, 4] = '\c0%4\'s satchel charge leaves %1 nothin\' but smokin\' boots.';

$DeathMessage[ $DamageType::BomberBombs , 0 ] = '\c0%4\'s cruise missile chases down %1.' ;		// +[soph]
$DeathMessage[ $DamageType::BomberBombs , 1 ] = '\c0%4 has a raptor in %6 pocket, and it\'s very happy to see %1.' ;
$DeathMessage[ $DamageType::BomberBombs , 2 ] = '\c0%4 turns %1 into a mess of blood and feathers.' ;	// +
$DeathMessage[ $DamageType::BomberBombs , 3 ] = '\c0%4 explodes %1 with a massive cruise missile.' ;	// +
$DeathMessage[ $DamageType::BomberBombs , 4 ] = '\c0%1 fails to dodge %4\'s fat freakin\' missile.' ;	// +[/soph]

$DeathMessageHeadshotCount = 3;
$DeathMessageHeadshot[$DamageType::Laser, 0] = '\c0%4 aerates %1\'s brain with %6 laser.';
$DeathMessageHeadshot[$DamageType::Laser, 1] = '\c0%4 pops %1\'s head like a cheap balloon.';
$DeathMessageHeadshot[$DamageType::Laser, 2] = '\c0%1 loses %3 head over %4\'s laser skill.';

$DeathMessageHeadshot[ $DamageType::Sniper , 0 ] = '\c0%4 nails %1 right between the eyes.' ;		// +[soph]
$DeathMessageHeadshot[ $DamageType::Sniper , 1 ] = '\c0%4 gives %1 a long-range lobotomy.' ;		// +
$DeathMessageHeadshot[ $DamageType::Sniper , 2 ] = '\c0%1 catches %4\'s sniper shell with %3 face.' ;	// +[/soph]
 
// MD Damagetypes
$DeathMessage[$DamageType::FissionBlaster, 0] = '\c0%4 caught %1 by surprise with a fission blaster.';
$DeathMessage[$DamageType::FissionBlaster, 1] = '\c0%4\'s fission blaster took out %1.';
$DeathMessage[$DamageType::FissionBlaster, 2] = '\c0%4 blasted %1 with a fission blaster.';
$DeathMessage[$DamageType::FissionBlaster, 3] = '\c0%4 caught %1 by surprise with a fission blaster.';
$DeathMessage[$DamageType::FissionBlaster, 4] = '\c0%4 blasted %1 with a fission blaster.';

$DeathMessage[$DamageType::MB, 0] = '\c0%1 is the victim of %4\'s Mitzi Blast.';
$DeathMessage[$DamageType::MB, 1] = '\c0%4 jams some Mitzi in %1\'s face.';
$DeathMessage[$DamageType::MB, 2] = '\c0%4 stops %1 dead with the Mitzi Blast Cannon.';
$DeathMessage[$DamageType::MB, 3] = '\c0%4 says to %1: "Mitzi. Kills bugs dead!"';
$DeathMessage[$DamageType::MB, 4] = '\c0%4\'s Mitzi gives %1 a bad case of static cling.';

$DeathMessage[$DamageType::MitziBooster, 0] = '\c0%1 is the victim of %4\'s Mitzi Booster Shot.';
$DeathMessage[$DamageType::MitziBooster, 1] = '\c0%1 gets smashed to bits and pieces %4\'s Mitzi Booster.';
$DeathMessage[$DamageType::MitziBooster, 2] = '\c0%4 stomps %1 flat with the Mitzi Booster.';
$DeathMessage[$DamageType::MitziBooster, 3] = '\c0%4 says to %1: "Hey look, the wall likes you!"';
$DeathMessage[$DamageType::MitziBooster, 4] = '\c0%4\'s Mitzi knocks %1 out homerun style!';

$DeathMessage[$DamageType::PBW, 0] = '\c0%4 gives %1 the smackdown with %6 PBW.';
$DeathMessage[$DamageType::PBW, 1] = '\c0%4\'s PBW punches that fatal hole through %1.';
$DeathMessage[$DamageType::PBW, 2] = '\c0%4\'s PBW beams %1 well... apart really.';
$DeathMessage[$DamageType::PBW, 3] = '\c0%4 blasts %1 to kibbles \'n bits.';
$DeathMessage[$DamageType::PBW, 4] = '\c0%4 makes %1\'s day with a zap of %6 PBW.';

$DeathMessage[$DamageType::Phaser, 0] = '\c0%4 melts %1 with %6 Phaser Compression Rifle.';
$DeathMessage[$DamageType::Phaser, 1] = '\c0%4 says to %1: "You zigged when you should have zagged".';
$DeathMessage[$DamageType::Phaser, 2] = '\c0%1 is the victim of %4\'s high-intensity phaser blast.';
$DeathMessage[$DamageType::Phaser, 3] = '\c0%1 catches a few sparks from %4\'s phaser.';
$DeathMessage[$DamageType::Phaser, 4] = '\c0%4 blasts %1 with %6 Phaser Compression Rifle.';

$DeathMessage[$DamageType::Sniper, 0] = '\c0%4\'s crack shot puts a hole in %1.' ;		// '\c0%1 never saw it coming from %4.'; -soph
$DeathMessage[$DamageType::Sniper, 1] = '\c0%1 catches %4\'s sniper shell with %3 body.' ;	// '\c0%4 gives %1 a shot right between the eyes.'; -soph
$DeathMessage[$DamageType::Sniper, 2] = '\c0%4 stops %1 dead with a sniper round.' ;		// '\c0%4 stops %1 dead with the Sniper Rifle.'; -soph
$DeathMessage[$DamageType::Sniper, 3] = '\c0%4 takes a pot shot at %1.';
$DeathMessage[$DamageType::Sniper, 4] = '\c0%4 picks off %1 from afar.';

$DeathMessage[$DamageType::PoisonLoop, 0] = '\c0%1 dies with green flesh open from %4\'s poison.';
$DeathMessage[$DamageType::PoisonLoop, 1] = '\c0%4 gives %1 a deadly dose of poison.';
$DeathMessage[$DamageType::PoisonLoop, 2] = '\c0%4 gives %1 "the clap".';
$DeathMessage[$DamageType::PoisonLoop, 3] = '\c0%1 takes %4\'s two pills, and doesn\'t call in the morning.';
$DeathMessage[$DamageType::PoisonLoop, 4] = '\c0%4 poisons %1.';

$DeathMessage[$DamageType::Poison, 0] = '\c0%1 dies with green flesh open from %4\'s poison.';
$DeathMessage[$DamageType::Poison, 1] = '\c0%4 gives %1 a deadly dose of poison.';
$DeathMessage[$DamageType::Poison, 2] = '\c0%4 gives %1 "the clap".';
$DeathMessage[$DamageType::Poison, 3] = '\c0%1 takes %4\'s two pills, and doesn\'t call in the morning.';
$DeathMessage[$DamageType::Poison, 4] = '\c0%4 poisons %1.';

$DeathMessage[$DamageType::PlasmaCannon, 0] = '\c0%4 electrocutes %1 with the plasma cannon.';
$DeathMessage[$DamageType::PlasmaCannon, 1] = '\c0%4 asks %1: "Need a light?"';
$DeathMessage[$DamageType::PlasmaCannon, 2] = '\c0%4 entices %1 to try a faceful of turret-style plasma.';
$DeathMessage[$DamageType::PlasmaCannon, 3] = '\c0%4 introduces %1 to the electro-plasmic induction dance.';
$DeathMessage[$DamageType::PlasmaCannon, 4] = '\c0%4 burns and fries %1 with the Plasma Cannon.';

$DeathMessage[$DamageType::ProtronFire, 0] = '\c0%4 tossed some cinders in %1\'s face.';		// +[soph]
$DeathMessage[$DamageType::ProtronFire, 1] = '\c0%4 gave %1 an ashen look.';
$DeathMessage[$DamageType::ProtronFire, 2] = '\c0%1 caught a facefull of %4\'s coals.';
$DeathMessage[$DamageType::ProtronFire, 3] = '\c0%4 sorta-kinda set %1 a little bit on fire-ish.';
$DeathMessage[$DamageType::ProtronFire, 4] = '\c0%1 caught %4\'s VERY hot potato.';			// +[/soph

$DeathMessage[$DamageType::EnergyBlast, 0] = '\c0%4 blasts %1 with a Energy Blast.';
$DeathMessage[$DamageType::EnergyBlast, 1] = '\c0%4\'s Energy Rifle feeds %1 an excessive amount of power.';
$DeathMessage[$DamageType::EnergyBlast, 2] = '\c0%1 is the victim of %4\'s concentrated energy blast.';
$DeathMessage[$DamageType::EnergyBlast, 3] = '\c0%1 never knew that %4\'s Energy Rifle could hurt so much.';
$DeathMessage[$DamageType::EnergyBlast, 4] = '\c0%4 smacks %1 with a red hot Energy Blast.';

$DeathMessage[$DamageType::Gauss, 0] = '\c0%4 blows %1 away with %6 Mass Driver.';
$DeathMessage[$DamageType::Gauss, 1] = '\c0%4 happily blows %1 into pieces with %6 Mass Driver.';
$DeathMessage[$DamageType::Gauss, 2] = '\c0%4 chunks %1 apart with %6 Mass Driver.';
$DeathMessage[$DamageType::Gauss, 3] = '\c0%1 fragments into a fine mist from %4\'s Mass Driver.';
$DeathMessage[$DamageType::Gauss, 4] = '\c0%4 rocks %1\'s socks with a Mass Driver.';

$DeathMessage[$DamageType::AutoCannon, 0] = '\c0%4 shreds %1 up with an AutoCannon.';
$DeathMessage[$DamageType::AutoCannon, 1] = '\c0%4 happily blows %1 into pieces with %6 AutoCannon.';
$DeathMessage[$DamageType::AutoCannon, 2] = '\c0%4 commands %1 to dance the AutoCannon dance.';
$DeathMessage[$DamageType::AutoCannon, 3] = '\c0%1 suffers a serious hosing from %4\'s AutoCannon.';
$DeathMessage[$DamageType::AutoCannon, 4] = '\c0%4 blows chunks away from %1 with %6 autocannon.';

$DeathMessage[$DamageType::Annihalator, 0] = '\c0%4 nukes %1 with %6 Annihalator.';
$DeathMessage[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by %4\'s nearby Annihalator.';
$DeathMessage[$DamageType::Annihalator, 2] = '\c0%1 felt the sun\'s burning rays from %4\'s Annihalator barrage.';
$DeathMessage[$DamageType::Annihalator, 3] = '\c0%4 nukes %1 with %6 Annihalator.';
$DeathMessage[$DamageType::Annihalator, 4] = '\c0%4 blows away %1 with %6 Annihalator.';

$DeathMessage[$DamageType::Nuke, 0] = '\c0A concussive front blows %1 into itty bitty chunks.';
$DeathMessage[$DamageType::Nuke, 1] = '\c0%1 is shattered by a penetrating shockwave.';
$DeathMessage[$DamageType::Nuke, 2] = '\c0%1 is smashed into the middle of next week by a blast wave.';
$DeathMessage[$DamageType::Nuke, 3] = '\c0An immense explosion splatters %1.';
$DeathMessage[$DamageType::Nuke, 4] = '\c0%1 is pancaked by the force of a massive blast!';

$DeathMessage[$DamageType::ShrikeBlaster, 0] = '\c0%1 lunches on a Shrike Blaster sandwich, courtesy of %4.';
$DeathMessage[$DamageType::ShrikeBlaster, 1] = '\c0%4\'s Shrike Blaster barrage blaster catches %1 with %3 pants down.';
$DeathMessage[$DamageType::ShrikeBlaster, 2] = '\c0%1 gets drilled big-time by %4\'s Shrike Blaster.';
$DeathMessage[$DamageType::ShrikeBlaster, 3] = '\c0%1 lunches on a Shrike Blaster sandwich, courtesy of %4.';
$DeathMessage[$DamageType::ShrikeBlaster, 4] = '\c0%4\'s Shrike Blaster barrage blaster catches %1 with %3 pants down.';

$DeathMessage[$DamageType::Ecstacy, 0] = '\c0%1 lunches on a Ecstacy sandwich, courtesy of %4.';
$DeathMessage[$DamageType::Ecstacy, 1] = '\c0%4\'s Ecstacy barrage catches %1 with %3 pants down.';
$DeathMessage[$DamageType::Ecstacy, 2] = '\c0%1 gets drilled big-time by %4\'s Ecstacy.';
$DeathMessage[$DamageType::Ecstacy, 3] = '\c0%1 lunches on a Ecstacy sandwich, courtesy of %4.';
$DeathMessage[$DamageType::Ecstacy, 4] = '\c0%4\'s Ecstacy barrage catches %1 with %3 pants down.';

$DeathMessage[$DamageType::PAC, 0] = '\c0%1 eats a big helping of %4\'s PAC bolt.';
$DeathMessage[$DamageType::PAC, 1] = '\c0%4 plants a fusion pulse bolt in %1\'s belly.';
$DeathMessage[$DamageType::PAC, 2] = '\c0%1 fails to evade %4\'s deft PAC pulse barrage.';
$DeathMessage[$DamageType::PAC, 3] = '\c0%1 eats a big helping of %4\'s PAC fusion pulse bolt.';
$DeathMessage[$DamageType::PAC, 4] = '\c0%4 plants a fusion pulse in %1\'s belly.';

$DeathMessage[$DamageType::AATurret, 0] = '\c0%1 lunches on a KM blaster sandwich, courtesy of %4.';
$DeathMessage[$DamageType::AATurret, 1] = '\c0%4\'s KM blaster barrage blaster catches %1 with %3 pants down.';
$DeathMessage[$DamageType::AATurret, 2] = '\c0%1 gets drilled big-time by %4\'s KM blaster.';
$DeathMessage[$DamageType::AATurret, 3] = '\c0%1 gets railed apart from %4\'s expert KM blaster usage.';
$DeathMessage[$DamageType::AATurret, 4] = '\c0%4\'s KM blaster burns away %1 with ease.';

$DeathMessage[$DamageType::BellyTurret, 0] = '\c0%4 spreads the pulse cannon love real thick on %1.';
$DeathMessage[$DamageType::BellyTurret, 1] = '\c0%4 thwaps %1 with %6 frying pans.';
$DeathMessage[$DamageType::BellyTurret, 2] = '\c0%1 had a Godzilla flashback from seeing %4\'s pulse cannons.';
$DeathMessage[$DamageType::BellyTurret, 3] = '\c0%1 gets knocked out from %4\'s one-two punch of the pulse cannons.';
$DeathMessage[$DamageType::BellyTurret, 4] = '\c0%1 attempted to make friends with %4\'s pulse cannons... and failed.';

$DeathMessage[$DamageType::OutdoorDepTurret, 0] = '\c0%4\'s spike neatly drills %1.';
$DeathMessage[$DamageType::OutdoorDepTurret, 1] = '\c0%1 dies under %4\'s spike love.';
$DeathMessage[$DamageType::OutdoorDepTurret, 2] = '\c0%1 is chewed up by %4\'s spike.';
$DeathMessage[$DamageType::OutdoorDepTurret, 3] = '\c0%1 feels the burn from %4\'s spike.';
$DeathMessage[$DamageType::OutdoorDepTurret, 4] = '\c0%1 is nailed by %4\'s spike.';

$DeathMessage[$DamageType::PlasmaTurret, 0] = '\c0%4 torches %1 with a heavy plasma blast!';
$DeathMessage[$DamageType::PlasmaTurret, 1] = '\c0%4 fries %1 with a heavy plasma blast!';
$DeathMessage[$DamageType::PlasmaTurret, 2] = '\c0%4 lights up %1 with a heavy plasma blast!';
$DeathMessage[$DamageType::PlasmaTurret, 3] = '\c0%4 fries %1 with a heavy plasma blast!';
$DeathMessage[$DamageType::PlasmaTurret, 4] = '\c0%4 lights up %1 with a heavy plasma blast!';

$DeathMessage[$DamageType::ElfTurret, 0] = '\c0%1 gets zapped by ELF gunner %4.';
$DeathMessage[$DamageType::ElfTurret, 1] = '\c0%1 gets barbecued by ELF gunner %4.';
$DeathMessage[$DamageType::ElfTurret, 2] = '\c0%1 gets shocked by ELF gunner %4.';
$DeathMessage[$DamageType::ElfTurret, 3] = '\c0%1 gets barbecued by ELF gunner %4.';
$DeathMessage[$DamageType::ElfTurret, 4] = '\c0%1 gets shocked by ELF gunner %4.';

$DeathMessage[$DamageType::MortarTurret, 0] = '\c0%1 is annihilated by %4\'s heavy mortar.';
$DeathMessage[$DamageType::MortarTurret, 1] = '\c0%1 is blown away by %4\'s heavy mortar.';
$DeathMessage[$DamageType::MortarTurret, 2] = '\c0%1 is pureed by %4\'s heavy mortar.';
$DeathMessage[$DamageType::MortarTurret, 3] = '\c0Whoops! %1 + %4\'s heavy mortar = Dead %1.';
$DeathMessage[$DamageType::MortarTurret, 4] = '\c0%1 learns the happy explosion dance from %4\'s heavy mortar.';

$DeathMessage[$DamageType::MissileTurret, 0] = '\c0%4 shows %1 a new world of pain with a missile turret.';
$DeathMessage[$DamageType::MissileTurret, 1] = '\c0%4 pops %1 with a missile turret.';
$DeathMessage[$DamageType::MissileTurret, 2] = '\c0%4\'s missile turret lights up %1\'s, uh, ex-life.';
$DeathMessage[$DamageType::MissileTurret, 3] = '\c0%4\'s missile turret makes armored chowder out of %1.';
$DeathMessage[$DamageType::MissileTurret, 4] = '\c0%1 has a brief, explosive fling with %4\'s missile turret.';

$DeathMessage[ $DamageType::MechStomp , 0 ] = '\c0%4 steps on %1\'s... everything.' ;			// +[soph]
$DeathMessage[ $DamageType::MechStomp , 1 ] = '\c0%4 stomps %1\'s face.' ;				// +
$DeathMessage[ $DamageType::MechStomp , 2 ] = '\c0%4 turns %1 into tribal toejam.' ;			// +
$DeathMessage[ $DamageType::MechStomp , 3 ] = '\c0%4 walks all over %1.' ;				// +
$DeathMessage[ $DamageType::MechStomp , 4 ] = '\c0%4 puts %6 foot down on %1\'s existence.' ;		// +[/soph]

// z0dd - ZOD, 8/25/02. Added Lance rear shot messages
$DeathMessageRearshotCount = 3;
$DeathMessageRearshot[$DamageType::ShockLance, 0] = '\c0%4 delivers a backdoor Lance to %1.';
$DeathMessageRearshot[$DamageType::ShockLance, 1] = '\c0%4 sends high voltage up %1\'s bum.';
$DeathMessageRearshot[$DamageType::ShockLance, 2] = '\c0%1 receives %4\'s rear-entry Lance attack.';

// Custom Death Messages
$DeathMessage[$DamageType::BAReactor, 0] = '\c0%1 is annihilated by %4\'s Battle Angel reactor overload.';
$DeathMessage[$DamageType::BAReactor, 1] = '\c0%1 is blown away by %4\'s reactor overload.';
$DeathMessage[$DamageType::BAReactor, 2] = '\c0%1 contacts %4\'s exploding Battle Angel and goes boom!';
$DeathMessage[$DamageType::BAReactor, 3] = '\c0Ouch! %1 + %4\'s overloaded Battle Angel reactor = Dead %1.';
$DeathMessage[$DamageType::BAReactor, 4] = '\c0%1 gets a fatal booster shot from %4\'s reactor explosion.';

$DeathMessage[$DamageType::DaishiReactor, 0] = '\c0%1 is annihilated by %4\'s explosive overload.';
$DeathMessage[$DamageType::DaishiReactor, 1] = '\c0%1 is blown away by %4\'s breach.';
$DeathMessage[$DamageType::DaishiReactor, 2] = '\c0%1 gives %4 a super explosive hug!';
$DeathMessage[$DamageType::DaishiReactor, 3] = '\c0%1 didn\'t expect %4\'s sudden outburst.';
$DeathMessage[$DamageType::DaishiReactor, 4] = '\c0%1 is instantly vaporized by %4\'s explosive outburst.';

$DeathMessage[$DamageType::MechReactor, 0] = '\c0%1 is annihilated by %4\'s Mech reactor overload.';
$DeathMessage[$DamageType::MechReactor, 1] = '\c0%1 is blown away by %4\'s exploding Mech.';
$DeathMessage[$DamageType::MechReactor, 2] = '\c0%1 contacts %4\'s exploding Mech and goes boom!';
$DeathMessage[$DamageType::MechReactor, 3] = '\c0Ouch! %1 + %4\'s overloaded Mech reactor = Dead %1.';
$DeathMessage[$DamageType::MechReactor, 4] = '\c0%1 gets a fatal booster shot from %4\'s fallen Mech.';

$DeathMessage[$DamageType::PunchBeam, 0] = '\c0%4 melts %1 with the EPC Punch Beam.';
$DeathMessage[$DamageType::PunchBeam, 1] = '\c0%4 punch a hole through %1 with %6 EPC Punch Beam.';
$DeathMessage[$DamageType::PunchBeam, 2] = '\c0%4\'s charged EPC Punch Beam makes short work of %1.';
$DeathMessage[$DamageType::PunchBeam, 3] = '\c0%1 fell victim to %4\'s expert EPC Punch Beam skills.';
$DeathMessage[$DamageType::PunchBeam, 4] = '\c0%4 knocks out %1 with %6 charged red beam of doom.';

$DeathMessage[$DamageType::MechLaser, 0] = '\c0%4\'s Mech Laser torches off %1\'s hair.';
$DeathMessage[$DamageType::MechLaser, 1] = '\c0%4\'s deadly aim with the Mech Laser singes %1.';
$DeathMessage[$DamageType::MechLaser, 2] = '\c0%1 gets spotlighted by %4\'s Mech Laser.';						
$DeathMessage[$DamageType::MechLaser, 3] = '\c0%1 is instantly vaporized by %4\'s Mech Laser.';
$DeathMessage[$DamageType::MechLaser, 4] = '\c0%4 plays laser light tag with %1.';

$DeathMessage[$DamageType::MechMissile, 0] = '\c0%4 shows %1 a new world of pain with %6 Mech Missile Rack.';
$DeathMessage[$DamageType::MechMissile, 1] = '\c0%4 pops %1 with %6 Mech Missile Rack.';
$DeathMessage[$DamageType::MechMissile, 2] = '\c0%4\'s missile rack lights up %1\'s, uh, ex-life.';
$DeathMessage[$DamageType::MechMissile, 3] = '\c0%4\'s missile rack makes armored chowder out of %1.';
$DeathMessage[$DamageType::MechMissile, 4] = '\c0%1 has a brief, explosive fling with %4\'s Mech Missile Rack.';

$DeathMessageSelfKill[$DamageType::MechMissile, 0] = '\c0%1 kills %2self with a Mech missile!';
$DeathMessageSelfKill[$DamageType::MechMissile, 1] = '\c0%1 runs a Mech missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::MechMissile, 2] = '\c0%1 tests the Mech missile\'s shaped charge on %2self.';
$DeathMessageSelfKill[$DamageType::MechMissile, 3] = '\c0%1 somehow achieved Mech missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::MechMissile, 4] = '\c0%1 gracefully smoked %2self with a Mech missile!';

$DeathMessage[$DamageType::MegaMissile, 0] = '\c0%4 intercepts %1 with a Mega missile.';
$DeathMessage[$DamageType::MegaMissile, 1] = '\c0%4 cackles as %6 Mega missile tackles %1.';
$DeathMessage[$DamageType::MegaMissile, 2] = '\c0%1 pays a special visit to %4\'s big bertha.';
$DeathMessage[$DamageType::MegaMissile, 3] = '\c0%1 took a heaping helping of %4\'s missile where the sun don\'t shine.';
$DeathMessage[$DamageType::MegaMissile, 4] = '\c0%4\'s Mega missile rains what little pieces of %1 that remains all over the ground.';

$DeathMessageSelfKill[$DamageType::MegaMissile, 0] = '\c0%1 kills %2self with a Mega missile!';
$DeathMessageSelfKill[$DamageType::MegaMissile, 1] = '\c0%1 runs a Mega missile up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::MegaMissile, 2] = '\c0%1 self-tested %3 Mega missile\'s payload.';
$DeathMessageSelfKill[$DamageType::MegaMissile, 3] = '\c0%1 achieved a Mega missile lock on %2self.';
$DeathMessageSelfKill[$DamageType::MegaMissile, 4] = '\c0%1 sat on %3 own Mega missile!';

$DeathMessage[$DamageType::DenseMitzi, 0] = '\c0%1 is the victim of %4\'s Dense Mitzi Blast.';
$DeathMessage[$DamageType::DenseMitzi, 1] = '\c0%4 jams some Dense Mitzi in %1\'s face.';
$DeathMessage[$DamageType::DenseMitzi, 2] = '\c0%4 mutates %1 with the radiation ball of love.';
$DeathMessage[$DamageType::DenseMitzi, 3] = '\c0%4 shares an orange glowing friend with %1.';
$DeathMessage[$DamageType::DenseMitzi, 4] = '\c0%4\'s Dense Mitzi gives %1 a bad case of radiation poisoning.';

$DeathMessageSelfKill[$DamageType::DenseMitzi, 0] = '\c0%1 lets %3 Dense Mitzi Blast eat %2 alive.';
$DeathMessageSelfKill[$DamageType::DenseMitzi, 1] = '\c0%1 was bit by %3 own Dense Mitzi blast.';
$DeathMessageSelfKill[$DamageType::DenseMitzi, 2] = '\c0%1 vaporizes %2self.';
$DeathMessageSelfKill[$DamageType::DenseMitzi, 3] = '\c0%1\'s own Dense Mitzi ran %2 down.';
$DeathMessageSelfKill[$DamageType::DenseMitzi, 4] = '\c0%1 could not dodge the Dense Mitzi Blast in time.';

$DeathMessage[$DamageType::PhaserCannon, 0] = '\c0%4 melts %1 with %6 Phaser Cannon.';
$DeathMessage[$DamageType::PhaserCannon, 1] = '\c0%4 says to %1: "You zigged when you should have zagged".';
$DeathMessage[$DamageType::PhaserCannon, 2] = '\c0%1 is the victim of %4\'s ultra high-intensity phaser blast.';
$DeathMessage[$DamageType::PhaserCannon, 3] = '\c0%1 catches a few sparks from %4\'s Phaser Cannon.';
$DeathMessage[$DamageType::PhaserCannon, 4] = '\c0%4 blasts %1 with %6 Phaser Cannon.';

$DeathMessageSelfKill[$DamageType::PhaserCannon, 0] = '\c0%1 experiences Phaser decompression.';
$DeathMessageSelfKill[$DamageType::PhaserCannon, 1] = '\c0%1 gets lit up by %3 own Phaser-Compressed blast.';
$DeathMessageSelfKill[$DamageType::PhaserCannon, 2] = '\c0%1 melts by %3 own Phaser.';
$DeathMessageSelfKill[$DamageType::PhaserCannon, 3] = '\c0%1 gets lit up by %3 own Phaser-Compressed blast.';
$DeathMessageSelfKill[$DamageType::PhaserCannon, 4] = '\c0%1 phasers %3self out of existance!';

$DeathMessage[$DamageType::MechRocket, 0] = '\c0%4 intercepts %1 with a mech rocket.';
$DeathMessage[$DamageType::MechRocket, 1] = '\c0%4 violently fragments %1 to death with %6 Mech Rocket.';
$DeathMessage[$DamageType::MechRocket, 2] = '\c0%1 catches %4\'s rockets.';
$DeathMessage[$DamageType::MechRocket, 3] = '\c0%1 is the victim of %4\'s expert Mech Rocket aim.';
$DeathMessage[$DamageType::MechRocket, 4] = '\c0%4\'s mech rockets knock %1 into next week.';

$DeathMessageSelfKill[$DamageType::MechRocket, 0] = '\c0%1 kills %2self with a mech rocket!';
$DeathMessageSelfKill[$DamageType::MechRocket, 1] = '\c0%1 runs a mech rocket up %3 own tailpipe.';
$DeathMessageSelfKill[$DamageType::MechRocket, 2] = '\c0%1 accidentally presses the trigger on %3 Mech Rocket Launcher.';
$DeathMessageSelfKill[$DamageType::MechRocket, 3] = '\c0%1 was found dead after looking down the barrel of %3 mech rocket launcher.';
$DeathMessageSelfKill[$DamageType::MechRocket, 4] = '\c0%1 attempted a rocket jump... and failed!';

$DeathMessage[$DamageType::MechHowitzer, 0] = '\c0%4 wrecks %1 hard with a howitzer.';
$DeathMessage[$DamageType::MechHowitzer, 1] = '\c0%4 pops %1 with a howitzer shell.';
$DeathMessage[$DamageType::MechHowitzer, 2] = '\c0%1 gets extra friendly with %4\'s howitzer shells.';
$DeathMessage[$DamageType::MechHowitzer, 3] = '\c0%4\'s howitzer gives %1 a hot lead injection.';
$DeathMessage[$DamageType::MechHowitzer, 4] = '\c0%1 has a very brief and rough fling with %4\'s howitzer.';

$DeathMessageSelfKill[$DamageType::MechHowitzer, 0] = '\c0%1 kills %2self with a howitzer!';
$DeathMessageSelfKill[$DamageType::MechHowitzer, 1] = '\c0%1 somehow kills %2self with a howitzer.';
$DeathMessageSelfKill[$DamageType::MechHowitzer, 2] = '\c0%1 was last seen looking down the barrel of %3 mech rocket launcher.';
$DeathMessageSelfKill[$DamageType::MechHowitzer, 3] = '\c0%1 somehow kills %2self with a howitzer.';
$DeathMessageSelfKill[$DamageType::MechHowitzer, 4] = '\c0%1 kills %2self with a howitzer!';

$DeathMessage[$DamageType::MechMinigun, 0] = '\c0%4 declares %1 holier than thou after a bout with %6 Mech Minigun.';
$DeathMessage[$DamageType::MechMinigun, 1] = '\c0%4 turns %1 into a wooden target dummy with the Mech Minigun.';
$DeathMessage[$DamageType::MechMinigun, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';
$DeathMessage[$DamageType::MechMinigun, 3] = '\c0%1 suffers a serious hosing from %4\'s minigun.';
$DeathMessage[$DamageType::MechMinigun, 4] = '\c0%4 gives %1 a fatal kinetic shower.';

$DeathMessageSelfKill[$DamageType::MechMinigun, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::MechMinigun, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::MechMinigun, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::MechMinigun, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::MechMinigun, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessage[$DamageType::MultiFusor, 0] = '\c0%4 demolishes %1 with the multifusor.';
$DeathMessage[$DamageType::MultiFusor, 1] = '\c0%4\'s multifusor fury of death was too much for %1 to handle.';
$DeathMessage[$DamageType::MultiFusor, 2] = '\c0%4 shares a few little blue friends with %1.';
$DeathMessage[$DamageType::MultiFusor, 3] = '\c0%1 is the victim of %4\'s fast multifusor barrage.';
$DeathMessage[$DamageType::MultiFusor, 4] = '\c0%4 plays skeet shoot with %1\'s corpse.';

$DeathMessageSelfKill[$DamageType::MultiFusor, 0] = '\c0%1 kills %2self with a disc.';
$DeathMessageSelfKill[$DamageType::MultiFusor, 1] = '\c0%1 catches %3 own multifusor disc.';
$DeathMessageSelfKill[$DamageType::MultiFusor, 2] = '\c0%1 heroically falls on %3 own disc.';
$DeathMessageSelfKill[$DamageType::MultiFusor, 3] = '\c0%1 helpfully jumps into %3 own disc\'s explosion.';
$DeathMessageSelfKill[$DamageType::MultiFusor, 4] = '\c0%1 plays Russian roulette with %3 multifusor.';

$DeathMessage[$DamageType::ShockCannon, 0] = '\c0%4 calls down the thunder and fury upon %1 with %6 shock cannon.';
$DeathMessage[$DamageType::ShockCannon, 1] = '\c0%4 asks %1: "Who wants a body massage"?';
$DeathMessage[$DamageType::ShockCannon, 2] = '\c0%1 does the electric slide after touching %4\'s shock ball.';
$DeathMessage[$DamageType::ShockCannon, 3] = '\c0%4 jams %1 into %6 reactor port.';
$DeathMessage[$DamageType::ShockCannon, 4] = '\c0%1 gains %4\'s shock ball, and short circuits.';

$DeathMessageSelfKill[$DamageType::ShockCannon, 0] = '\c0%1 tried to jimmy rig %3 shock blaster, and died.';
$DeathMessageSelfKill[$DamageType::ShockCannon, 1] = '\c0%1 overloaded %3 shock blaster cannon.';
$DeathMessageSelfKill[$DamageType::ShockCannon, 2] = '\c0%1 tests the shock cannon\'s electrical output on %2self.';
$DeathMessageSelfKill[$DamageType::ShockCannon, 3] = '\c0%1 achieved a higher power, then suddenly exploded.';
$DeathMessageSelfKill[$DamageType::ShockCannon, 4] = '\c0%1 gracefully electrocuted %2self with a shock blaster!';

$DeathMessage[$DamageType::StarHammerFlak, 0] = '\c0%4 finishes off %1 a passing star hammer.';
$DeathMessage[$DamageType::StarHammerFlak, 1] = '\c0%4 watches %6 starhammer blow %1 to pieces.';
$DeathMessage[$DamageType::StarHammerFlak, 2] = '\c0%1 tries to dodge but fails to evade %4\'s star hammer.';
$DeathMessage[$DamageType::StarHammerFlak, 3] = '\c0%4 delivers a fatal hammer to %1\'s head.';
$DeathMessage[$DamageType::StarHammerFlak, 4] = '\c0%4\'s star hammer pops %1 to death.';

$DeathMessageSelfKill[$DamageType::StarHammerFlak, 0] = '\c0%1 goes out with a bang!';  //applies to most explosion types
$DeathMessageSelfKill[$DamageType::StarHammerFlak, 1] = '\c0%1 hammers %2self into tiny bits and pieces.';
$DeathMessageSelfKill[$DamageType::StarHammerFlak, 2] = '\c0%1 explodes in that fatal kind of way.';
$DeathMessageSelfKill[$DamageType::StarHammerFlak, 3] = '\c0%1 caught %2self in %3 own star hammer flak trail.';
$DeathMessageSelfKill[$DamageType::StarHammerFlak, 4] = '\c0%1 hammers %2self all over the map.';

$DeathMessage[$DamageType::PPC, 0] = '\c0%4 reaps a harvest of %1 with the PPC.';
$DeathMessage[$DamageType::PPC, 1] = '\c0%4 feeds %1 the business end of %6 PPC.';
$DeathMessage[$DamageType::PPC, 2] = '\c0%4 stops %1 dead with the PPC.';
$DeathMessage[$DamageType::PPC, 3] = '\c0%1 falls victim to %4\'s lightning bolts.';
$DeathMessage[$DamageType::PPC, 4] = '\c0%1 recieves a shocking revelation about %4\'s PPC skills.';

$DeathMessageSelfKill[$DamageType::PPC, 0] = '\c0%1 kills %2self with a PPC!';
$DeathMessageSelfKill[$DamageType::PPC, 1] = '\c0%1 experiments with electricity.';
$DeathMessageSelfKill[$DamageType::PPC, 2] = '\c0%1 tests the PPC\'s output on %2self.';
$DeathMessageSelfKill[$DamageType::PPC, 3] = '\c0%1 sticks %3 finger in the Mech\'s reactor port.';
$DeathMessageSelfKill[$DamageType::PPC, 4] = '\c0%1 gracefully electrocuted %2self with a PPC!';

$DeathMessage[$DamageType::AutoCannonAP, 0] = '\c0%4 bolts %1 down with an incendiary shell.';
$DeathMessage[$DamageType::AutoCannonAP, 1] = '\c0%4 happily burns %1 into meaty chunks with %6 flaming shells.';
$DeathMessage[$DamageType::AutoCannonAP, 2] = '\c0%4 hailstorms fire and brimstone upon %1.';
$DeathMessage[$DamageType::AutoCannonAP, 3] = '\c0%1 suffers a serious hosing from %4\'s flaming barrage.';
$DeathMessage[$DamageType::AutoCannonAP, 4] = '\c0%4 bestows the blessings of %6 firestorm on %1.';

$DeathMessageSelfKill[$DamageType::AutoCannonAP, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::AutoCannonAP, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::AutoCannonAP, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::AutoCannonAP, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::AutoCannonAP, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessage[$DamageType::DaishiTC, 0] = '\c0%4 rips %1 up with the turbo chaingun.';
$DeathMessage[$DamageType::DaishiTC, 1] = '\c0%4 happily chews %1 into pieces with %6 turbo chaingun.';
$DeathMessage[$DamageType::DaishiTC, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';
$DeathMessage[$DamageType::DaishiTC, 3] = '\c0%1 suffers a serious hosing from %4\'s turbo chaingun.';
$DeathMessage[$DamageType::DaishiTC, 4] = '\c0%4 bestows the blessings of %6 turbo chaingun on %1.';

$DeathMessageSelfKill[$DamageType::DaishiTC, 0] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::DaishiTC, 1] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::DaishiTC, 2] = '\c0%1 manages to kill %2self with a reflected bullet.';
$DeathMessageSelfKill[$DamageType::DaishiTC, 3] = '\c0%1 happily chews %2self into pieces.';
$DeathMessageSelfKill[$DamageType::DaishiTC, 4] = '\c0%1 manages to kill %2self with a reflected bullet.';

$DeathMessage[$DamageType::CometCannon, 0] = '\c0%1 eats a big helping of %4\'s Comet Cannon blast.';
$DeathMessage[$DamageType::CometCannon, 1] = '\c0%4 plants a comet bolt in %1\'s chest.';
$DeathMessage[$DamageType::CometCannon, 2] = '\c0%1 fails to evade %4\'s deft Comet Cannon barrage.';
$DeathMessage[$DamageType::CometCannon, 3] = '\c0%1 helpfully jumps into %4\'s Comet Cannon.';
$DeathMessage[$DamageType::CometCannon, 4] = '\c0%1 gets knocked into next week from %4\'s Comet Cannon.';

$DeathMessageSelfKill[$DamageType::CometCannon, 0] = '\c0%1 plays catch with %3 Comet Cannon.';
$DeathMessageSelfKill[$DamageType::CometCannon, 1] = '\c0%1 plays catch with %3 Comet Cannon.';
$DeathMessageSelfKill[$DamageType::CometCannon, 2] = '\c0%1 plays catch with %3 Comet Cannon.';
$DeathMessageSelfKill[$DamageType::CometCannon, 3] = '\c0%1 plays catch with %3 Comet Cannon.';
$DeathMessageSelfKill[$DamageType::CometCannon, 4] = '\c0%1 plays catch with %3 Comet Cannon.';

$DeathMessage[$DamageType::RFL, 0] = '\c0%4 melts %1 with the laser assault rifle.';
$DeathMessage[$DamageType::RFL, 1] = '\c0%4 fast laser pulses vaporize %1.';
$DeathMessage[$DamageType::RFL, 2] = '\c0%4\'s laser assault rifle carves %6 initials into %1.';
$DeathMessage[$DamageType::RFL, 3] = '\c0%1 recieves a free face melting, courtesy of %4\'s laser assault rifle.';
$DeathMessage[$DamageType::RFL, 4] = '\c0%4 shines %6 flashlight upon %1.';

$DeathMessage[$DamageType::RFLPlasma, 0] = '\c0%4\'s plasma beam torches off %1\'s hair.';
$DeathMessage[$DamageType::RFLPlasma, 1] = '\c0%4\'s deadly aim with the plasma beam singes %1.';
$DeathMessage[$DamageType::RFLPlasma, 2] = '\c0%1 goes down in a fiery blaze from %4\'s plasma beam.';						
$DeathMessage[$DamageType::RFLPlasma, 3] = '\c0%1 is instantly vaporized by %4\'s plasma beam.';
$DeathMessage[$DamageType::RFLPlasma, 4] = '\c0%1 plays dodge with %1\'s plasma-powered laser assault rifle.';

$DeathMessage[$DamageType::MegaBlaster, 0] = '\c0%4 annihilates %1 with the mega blaster.';
$DeathMessage[$DamageType::MegaBlaster, 1] = '\c0%4 teaches %1 that %6 mega blaster should be feared.';
$DeathMessage[$DamageType::MegaBlaster, 2] = '\c0%1 underestimates the power of %4\'s mega blaster.';
$DeathMessage[$DamageType::MegaBlaster, 3] = '\c0%1 is liberated from %3 armor thanks to %4\'s mega blaster.';
$DeathMessage[$DamageType::MegaBlaster, 4] = '\c0%4 takes careful aim, and nails %1 with %6 mega blaster.';

$DeathMessageSelfKill[$DamageType::MegaBlaster, 0] = '\c0%1 plays catch with %3 mega blaster.';
$DeathMessageSelfKill[$DamageType::MegaBlaster, 1] = '\c0%1 tries to cuddle with %3 mega blaster... and failed!';
$DeathMessageSelfKill[$DamageType::MegaBlaster, 2] = '\c0%1 plays catch with %3 mega blaster.';
$DeathMessageSelfKill[$DamageType::MegaBlaster, 3] = '\c0%1 tries to cuddle with %3 mega blaster... and failed!';
$DeathMessageSelfKill[$DamageType::MegaBlaster, 4] = '\c0%1 plays catch with %3 mega blaster.';

$DeathMessage[$DamageType::ShieldPulse, 0] = '\c0%4 expands and crushes %1.';
$DeathMessage[$DamageType::ShieldPulse, 1] = '\c0%4\'s shield expansion crushes %1.';
$DeathMessage[$DamageType::ShieldPulse, 2] = '\c0%1 gets all 2-dimensional from %4\'s shield expansion.';						
$DeathMessage[$DamageType::ShieldPulse, 3] = '\c0%1 turns into a tribal pizza thanks to %4\'s shield expansion.';
$DeathMessage[$DamageType::ShieldPulse, 4] = '\c0%1 couldn\'t take the pressure.';

//These used when a player is run over by a vehicle
$DeathMessageVehicleCount = 5;
$DeathMessageVehicle[0] = '\c0%4 says to %1: "Hey! You scratched my paint job!".';
$DeathMessageVehicle[1] = '\c0%1 acquires that run-down feeling from %4.';
$DeathMessageVehicle[2] = '\c0%4 shows %1 %6 new ride.';
$DeathMessageVehicle[3] = '\c0%1 makes a painfully close examination of %4\'s front bumper.';
$DeathMessageVehicle[4] = '\c0%1\'s messy death leaves a mark on %4\'s vehicle finish.';

$DeathMessageVehicleCrashCount = 5;
$DeathMessageVehicleCrash[ $DamageType::Crash, 0 ] = '\c0%1 fails to eject in time.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 1 ] = '\c0%1 becomes one with his vehicle dashboard.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 2 ] = '\c0%1 drives under the influence of death.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 3 ] = '\c0%1 makes a perfect three hundred point landing.';
$DeathMessageVehicleCrash[ $DamageType::Crash, 4 ] = '\c0%1 heroically pilots his vehicle into something really, really hard.';

$DeathMessageVehicleUnmannedCount = 3;
$DeathMessageVehicleUnmanned[0] = '\c0%1 gets in the way of a runaway vehicle.';
$DeathMessageVehicleUnmanned[1] = '\c0%1 is the victim of roadrage.';
$DeathMessageVehicleUnmanned[2] = '\c0%1 is struck down by a suicide run vehicle.';

$DeathMessageVehicleFriendlyCount = 3;
$DeathMessageVehicleFriendly[0] = '\c0%1 gets in the way of a friendly vehicle.';
$DeathMessageVehicleFriendly[1] = '\c0Sadly, a friendly vehicle turns %1 into roadkill.';
$DeathMessageVehicleFriendly[2] = '\c0%1 becomes an unsightly ornament on a team vehicle\'s hood.';

//These used when a player is killed by a nearby equipment explosion
$DeathMessageExplosionCount = 3;
$DeathMessageExplosion[0] = '\c0%1 is blown away by a nearby explosion!';
$DeathMessageExplosion[1] = '\c0%1 stood a little too close to the action!';
$DeathMessageExplosion[2] = '\c0%1 got blasted to tiny pieces!';

//These used when an automated turret kills an  enemy player
$DeathMessageTurretKillCount = 3;

$DeathMessageTurretKill[$DamageType::PlasmaTurret, 0] = '\c0%1 is killed by a plasma turret.';
$DeathMessageTurretKill[$DamageType::PlasmaTurret, 1] = '\c0%1 wonders what all those pretty blue lights are.';
$DeathMessageTurretKill[$DamageType::PlasmaTurret, 2] = '\c0%1 is fried by a plasma turret.';

$DeathMessageTurretKill[$DamageType::Plasma, 0] = '\c0%1 is seared by a streaking turret fireball.';	// '\c0%1 is killed by a streaking fireball turret.'; -soph
$DeathMessageTurretKill[$DamageType::Plasma, 1] = '\c0%1 caught several facefulls of turret fire.';	// '\c0%1 wonders what all those balls of fire are.'; -soph
$DeathMessageTurretKill[$DamageType::Plasma, 2] = '\c0%1 is Mario\'d to death by a fireball turret.';

$DeathMessageTurretKill[$DamageType::BurnLoop, 0] = '\c0A turret set %1\'s pants on fire.';		// +[soph]
$DeathMessageTurretKill[$DamageType::BurnLoop, 0] = '\c0%1 got a warm welcome from a fireball turret.';
$DeathMessageTurretKill[$DamageType::BurnLoop, 0] = '\c0A fire turret served up %1 well-done.';		// +[/soph]

$DeathMessageTurretKill[$DamageType::Burn, 0] = '\c0A fireball turret set %1\'s pants on fire.';
$DeathMessageTurretKill[$DamageType::Burn, 1] = '\c0%1 is left with a burning sensaton.';
$DeathMessageTurretKill[$DamageType::Burn, 2] = '\c0%1 gets a warm welcome from a fireball turret.';

$DeathMessageTurretKill[$DamageType::Laser, 0] = '\c0A laser sentry picks off %1 with deadly accuracy.';
$DeathMessageTurretKill[$DamageType::Laser, 1] = '\c0A laser sentry uses %1 as the targeting dummy in a systems check.';
$DeathMessageTurretKill[$DamageType::Laser, 2] = '\c0%1 finally realized what that red dot was.';

$DeathMessageTurretKill[$DamageType::AATurret, 0] = '\c0%1 is killed by an AA turret.';
$DeathMessageTurretKill[$DamageType::AATurret, 1] = '\c0%1 is shot down by an AA turret.';
$DeathMessageTurretKill[$DamageType::AATurret, 2] = '\c0%1 takes fatal flak from an AA turret.';

$DeathMessageTurretKill[$DamageType::Flak, 0] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessageTurretKill[$DamageType::Flak, 1] = '\c0%4 delivers a flak payload straight to %1.';
$DeathMessageTurretKill[$DamageType::Flak, 2] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';

$DeathMessageTurretKill[$DamageType::ElfTurret, 0] = '\c0%1 is killed by an ELF turret.';
$DeathMessageTurretKill[$DamageType::ElfTurret, 1] = '\c0%1 is zapped by an ELF turret.';
$DeathMessageTurretKill[$DamageType::ElfTurret, 2] = '\c0%1 is short-circuited by an ELF turret.';

$DeathMessageTurretKill[$DamageType::MortarTurret, 0] = '\c0%1 is killed by a mortar turret.';
$DeathMessageTurretKill[$DamageType::MortarTurret, 1] = '\c0%1 enjoys a mortar turret\'s attention.';
$DeathMessageTurretKill[$DamageType::MortarTurret, 2] = '\c0%1 is blown to kibble by a mortar turret.';

$DeathMessageTurretKill[$DamageType::MissileTurret, 0] = '\c0%1 is killed by a missile turret.';
$DeathMessageTurretKill[$DamageType::MissileTurret, 1] = '\c0%1 is shot down by a missile turret.';
$DeathMessageTurretKill[$DamageType::MissileTurret, 2] = '\c0%1 is blown away by a missile turret.';

$DeathMessageTurretKill[$DamageType::IndoorDepTurret, 0] = '\c0%1 is killed by a clamp turret.';
$DeathMessageTurretKill[$DamageType::IndoorDepTurret, 1] = '\c0%1 gets burned by a clamp turret.';
$DeathMessageTurretKill[$DamageType::IndoorDepTurret, 2] = '\c0A clamp turret eliminates %1.';

$DeathMessageTurretKill[$DamageType::OutdoorDepTurret, 0] = '\c0A spike turret neatly drills %1.';
$DeathMessageTurretKill[$DamageType::OutdoorDepTurret, 1] = '\c0%1 gets taken out by a spike turret.';
$DeathMessageTurretKill[$DamageType::OutdoorDepTurret, 2] = '\c0%1 dies under a spike turret\'s love.';

$DeathMessageTurretKill[$DamageType::SentryTurret, 0] = '\c0%1 didn\'t see that Sentry turret, but it saw %2...';
$DeathMessageTurretKill[$DamageType::SentryTurret, 1] = '\c0%1 needs to watch for Sentry turrets.';
$DeathMessageTurretKill[$DamageType::SentryTurret, 2] = '\c0%1 now understands how Sentry turrets work.';

$DeathMessageTurretKill[$DamageType::Disc, 0] = '\c0%4 serves %1 a blue plate special.';
$DeathMessageTurretKill[$DamageType::Disc, 1] = '\c0%4 shares a little blue friend with %1.';
$DeathMessageTurretKill[$DamageType::Disc, 2] = '\c0%4 puts a little spin into %1.';

$DeathMessageTurretKill[$DamageType::Phaser, 0] = '\c0%1 is melted by a Phaser Turret.';
$DeathMessageTurretKill[$DamageType::Phaser, 1] = '\c0A Phaser Turret says to %1: "You zigged when you should have zagged".';
$DeathMessageTurretKill[$DamageType::Phaser, 2] = '\c0%1 is the victim of a high-intensity phaser turret blast.';

$DeathMessageTurretKill[$DamageType::MB, 0] = '\c0%1 is the victim of a Mitzi Turret\'s Blast.';
$DeathMessageTurretKill[$DamageType::MB, 1] = '\c0A Mitzi Turret flyswats %1.';
$DeathMessageTurretKill[$DamageType::MB, 2] = '\c0A Mitzi Turret stops %1 dead.';

$DeathMessageTurretKill[$DamageType::MitziTransparent, 0] = '\c0%1 is the victim of a Mitzi Turret\'s Blast.';
$DeathMessageTurretKill[$DamageType::MitziTransparent, 1] = '\c0A Mitzi Turret flyswats %1.';
$DeathMessageTurretKill[$DamageType::MitziTransparent, 2] = '\c0A Mitzi Turret stops %1 dead.';

$DeathMessageTurretKill[$DamageType::Bullet, 0] = '\c0A Chaingun Turret happily chews %1 into pieces with %6 chaingun.';
$DeathMessageTurretKill[$DamageType::Bullet, 1] = '\c0A Chaingun Turret administers a dose of Vitamin Lead to %1.';
$DeathMessageTurretKill[$DamageType::Bullet, 2] = '\c0%1 suffers a serious hosing from A Chaingun Turret.';

$DeathMessageTurretKill[$DamageType::EnergyBlast, 0] = '\c0An Energy Turret pelts %1 with a Energy Blast.';
$DeathMessageTurretKill[$DamageType::EnergyBlast, 1] = '\c0%1 is the victim of concentrated energy.';
$DeathMessageTurretKill[$DamageType::EnergyBlast, 2] = '\c0An Energy Turret smacks %1 with a hot Energy Blast.';

$DeathMessageTurretKill[$DamageType::FlameTurret, 0] = '\c0%1 is now extra crispy thanks to a RAXX turret.';
$DeathMessageTurretKill[$DamageType::FlameTurret, 1] = '\c0A RAXX turret bakes %1 in 10000 degree stream of plasma.';
$DeathMessageTurretKill[$DamageType::FlameTurret, 2] = '\c0A RAXX turret broils %1 to a nice golden brown.';

$DeathMessageTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.'; 
$DeathMessageTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.'; 
$DeathMessageTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DeathMessageTurretKill[ $DamageType::AutoCannon , 0 ] = '\c0%4 brings down the Autocannon rain on %1.' ;	// +[soph]
$DeathMessageTurretKill[ $DamageType::AutoCannon , 1 ] = '\c0%4 drops two barrels of bolts on %1.' ;		// +
$DeathMessageTurretKill[ $DamageType::AutoCannon , 2 ] = '\c0%1 caught a few of %4\'s shells.' ;		// +[/soph]


//used when a player is killed by a teammate controlling a turret
$DeathMessageCTurretTeamKillCount = 1;	
$DeathMessageCTurretTeamKill[$DamageType::PlasmaTurret, 0] = '\c0%4 TEAMKILLED %1 with a plasma turret!';

$DeathMessageCTurretTeamKill[$DamageType::AATurret, 0] = '\c0%4 TEAMKILLED %1 with an AA turret!';

$DeathMessageCTurretTeamKill[$DamageType::ELFTurret, 0] = '\c0%4 TEAMKILLED %1 with an ELF turret!';

$DeathMessageCTurretTeamKill[$DamageType::MortarTurret, 0] = '\c0%4 TEAMKILLED %1 with a mortar turret!';

$DeathMessageCTurretTeamKill[$DamageType::MissileTurret, 0] = '\c0%4 TEAMKILLED %1 with a missile turret!';

$DeathMessageCTurretTeamKill[$DamageType::IndoorDepTurret, 0] = '\c0%4 TEAMKILLED %1 with a clamp turret!';

$DeathMessageCTurretTeamKill[$DamageType::OutdoorDepTurret, 0] = '\c0%4 TEAMKILLED %1 with a spike turret!';

$DeathMessageCTurretTeamKill[$DamageType::SentryTurret, 0] = '\c0%4 TEAMKILLED %1 with a sentry turret!';

$DeathMessageCTurretTeamKill[$DamageType::BomberBombs, 0] = '\c0%4 TEAMKILLED %1 in a bombastic explosion of raining death.';

$DeathMessageCTurretTeamKill[$DamageType::BellyTurret, 0] = '\c0%4 TEAMKILLED %1 by a pulse blast.';

$DeathMessageCTurretTeamKill[$DamageType::TankChainGun, 0] = '\c0%4 TEAMKILLED %1 with his tank\'s chaingun.';

$DeathMessageCTurretTeamKill[$DamageType::TankMortar, 0] = '\c0%4 TEAMKILLED %1 by lobbing the BIG green death from a vehicle.';	// 'tank' -soph

$DeathMessageCTurretTeamKill[$DamageType::ShrikeBlaster, 0] = '\c0%4 TEAMKILLED %1 by strafing from a Shrike.';

$DeathMessageCTurretTeamKill[$DamageType::Ecstacy, 0] = '\c0%4 TEAMKILLED %1 by strafing from a Shrike.';

$DeathMessageCTurretTeamKill[$DamageType::MPBMissile, 0] = '\c0%4 TEAMKILLED %1 when the MPB locked onto him.';

$DeathMessageCTurretTeamKill[$DamageType::Gat, 0] = '\c0%4 TEAMKILLED %1 by gatting %2 to death.'; //Death~bot

$DeathMessageCTurretTeamKill[$DamageType::FlameTurret, 0] = '\c0%4 TEAMKILLED %1 by baking %2 to death.'; //Death~bot
 

//used when a player is killed by an uncontrolled, friendly turret
$DeathMessageCTurretAccdtlKillCount = 1; 
$DeathMessageCTurretAccdtlKill[$DamageType::PlasmaTurret, 0] = '\c0%1 got in the way of a plasma turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::AATurret, 0] = '\c0%1 got in the way of an AA turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::ELFTurret, 0] = '\c0%1 got in the way of an ELF turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::MortarTurret, 0] = '\c0%1 got in the way of a mortar turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::MissileTurret, 0] = '\c0%1 got in the way of a missile turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::IndoorDepTurret, 0] = '\c0%1 got in the way of a clamp turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::OutdoorDepTurret, 0] = '\c0%1 got in the way of a spike turret!';

$DeathMessageCTurretAccdtlKill[$DamageType::SentryTurret, 0] = '\c0%1 got in the way of a Sentry turret!';


//these messages for owned or controlled turrets
$DeathMessageCTurretKillCount = 3;
$DeathMessageCTurretKill[$DamageType::PlasmaTurret, 0] = '\c0%4 torches %1 with a plasma turret!'; 
$DeathMessageCTurretKill[$DamageType::PlasmaTurret, 1] = '\c0%4 fries %1 with a plasma turret!';
$DeathMessageCTurretKill[$DamageType::PlasmaTurret, 2] = '\c0%4 lights up %1 with a plasma turret!';

$DeathMessageCTurretKill[$DamageType::Plasma, 0] = '\c0%1 is killed by %4\'s streaking fireball turret.';
$DeathMessageCTurretKill[$DamageType::Plasma, 1] = '\c0%4 roasts %1 with a great ball of fire.';
$DeathMessageCTurretKill[$DamageType::Plasma, 2] = '\c0%4 goes fireflower on %1\'s ass.';

$DeathMessageCTurretKill[$DamageType::AATurret, 0] = '\c0%4 shoots down %1 with an AA turret.';
$DeathMessageCTurretKill[$DamageType::AATurret, 1] = '\c0%1 gets shot down by %4\'s AA turret.';
$DeathMessageCTurretKill[$DamageType::AATurret, 2] = '\c0%4 takes out %1 with an AA turret.';

$DeathMessageCTurretKill[$DamageType::ElfTurret, 0] = '\c0%1 gets zapped by ELF gunner %4.';
$DeathMessageCTurretKill[$DamageType::ElfTurret, 1] = '\c0%1 gets barbecued by ELF gunner %4.';
$DeathMessageCTurretKill[$DamageType::ElfTurret, 2] = '\c0%1 gets shocked by ELF gunner %4.';

$DeathMessageCTurretKill[$DamageType::MortarTurret, 0] = '\c0%1 is annihilated by %4\'s mortar turret.';
$DeathMessageCTurretKill[$DamageType::MortarTurret, 1] = '\c0%1 is blown away by %4\'s mortar turret.';
$DeathMessageCTurretKill[$DamageType::MortarTurret, 2] = '\c0%1 is pureed by %4\'s mortar turret.';

$DeathMessageCTurretKill[$DamageType::Protron, 0] = '\c0%4 is bombarded by %1 with a Protron.';
$DeathMessageCTurretKill[$DamageType::Protron, 1] = '\c0%1 gets a pointer in Protron use from %4.';
$DeathMessageCTurretKill[$DamageType::Protron, 2] = '\c0%4 unleashes a terminal Protron barrage into %1.';

$DeathMessageCTurretKill[$DamageType::Annihalator, 0] = '\c0%4 nukes %1 with %6 Annihalator Bomb.';
$DeathMessageCTurretKill[$DamageType::Annihalator, 1] = '\c0%1 is broken down into %3 component atoms by %4\'s Annihalator Bomb.';
$DeathMessageCTurretKill[$DamageType::Annihalator, 2] = '\c0%4 used the force on %1.';

$DeathMessageCTurretKill[$DamageType::Flak, 0] = '\c0%4 watches %6 flak charge blow %1 to pieces.';
$DeathMessageCTurretKill[$DamageType::Flak, 1] = '\c0%4 delivers a flak payload straight to %1.';
$DeathMessageCTurretKill[$DamageType::Flak, 2] = '\c0%4\'s flak rains little pieces of %1 all over the ground.';

$DeathMessageCTurretKill[$DamageType::MissileTurret, 0] = '\c0%4 shows %1 a new world of pain with a missile turret.';
$DeathMessageCTurretKill[$DamageType::MissileTurret, 1] = '\c0%4 pops %1 with a missile turret.';
$DeathMessageCTurretKill[$DamageType::MissileTurret, 2] = '\c0%4\'s missile turret lights up %1\'s, uh, ex-life.';

$DeathMessageCTurretKill[$DamageType::IndoorDepTurret, 0] = '\c0%1 is chewed up and spat out by %4\'s clamp turret.';
$DeathMessageCTurretKill[$DamageType::IndoorDepTurret, 1] = '\c0%1 is knocked out by %4\'s clamp turret.';
$DeathMessageCTurretKill[$DamageType::IndoorDepTurret, 2] = '\c0%4\'s clamp turret drills %1 nicely.';

$DeathMessageCTurretKill[$DamageType::OutdoorDepTurret, 0] = '\c0%1 is chewed up by %4\'s spike turret.';
$DeathMessageCTurretKill[$DamageType::OutdoorDepTurret, 1] = '\c0%1 feels the burn from %4\'s spike turret.';
$DeathMessageCTurretKill[$DamageType::OutdoorDepTurret, 2] = '\c0%1 is nailed by %4\'s spike turret.';

$DeathMessageCTurretKill[$DamageType::SentryTurret, 0] = '\c0%4 caught %1 by surprise with a turret.';
$DeathMessageCTurretKill[$DamageType::SentryTurret, 1] = '\c0%4\'s turret took out %1.';
$DeathMessageCTurretKill[$DamageType::SentryTurret, 2] = '\c0%4 blasted %1 with a turret.';

$DeathMessageCTurretKill[$DamageType::BomberBombs, 0] = '\c0%1 catches %4\'s bomb in both teeth.'; 
$DeathMessageCTurretKill[$DamageType::BomberBombs, 1] = '\c0%4 leaves %1 a smoking bomb crater.'; 
$DeathMessageCTurretKill[$DamageType::BomberBombs, 2] = '\c0%4 bombs %1 back to the 20th century.';

$DeathMessageCTurretKill[$DamageType::BellyTurret, 0] = '\c0%1 eats a big helping of %4\'s pulse bolt.';
$DeathMessageCTurretKill[$DamageType::BellyTurret, 1] = '\c0%4 plants a pulse bolt in %1\'s belly.';
$DeathMessageCTurretKill[$DamageType::BellyTurret, 2] = '\c0%1 fails to evade %4\'s deft pulse cannon barrage.';

$DeathMessageCTurretKill[$DamageType::TankChainGun, 0] = '\c0%1 enjoys the rich, metallic taste of %4\'s tank slug.';
$DeathMessageCTurretKill[$DamageType::TankChainGun, 1] = '\c0%4\'s tank chaingun plays sweet music all over %1.';
$DeathMessageCTurretKill[$DamageType::TankChainGun, 2] = '\c0%1 receives a stellar exit wound from %4\'s tank slug.';

$DeathMessageCTurretKill[$DamageType::TankMortar, 0] = '\c0Whoops! %1 + %4\'s vehicular mortar = Dead %1.';			// 'tank' -[soph]
$DeathMessageCTurretKill[$DamageType::TankMortar, 1] = '\c0%1 learns the happy explosion dance from %4\'s vehicular mortar.';
$DeathMessageCTurretKill[$DamageType::TankMortar, 2] = '\c0%4\'s vehicular mortar has a blast with %1.';			// -[/soph]

$DeathMessageCTurretKill[$DamageType::Mortar, 0] = '\c0%4 obliterates %1 with the mortar.';
$DeathMessageCTurretKill[$DamageType::Mortar, 1] = '\c0%4 offers a little "heavy love" to %1.';
$DeathMessageCTurretKill[$DamageType::Mortar, 2] = '\c0%1 stumbles into %4\'s mortar reticle.';

$DeathMessageCTurretKill[$DamageType::ShrikeBlaster, 0] = '\c0%1 lunches on a Shrike blaster sandwich, courtesy of %4.';
$DeathMessageCTurretKill[$DamageType::ShrikeBlaster, 1] = '\c0%4\'s Shrike blaster catches %1 with %3 pants down.';
$DeathMessageCTurretKill[$DamageType::ShrikeBlaster, 2] = '\c0%1 gets drilled big-time by %4\'s Shrike blaster.';

$DeathMessageCTurretKill[$DamageType::Grenade, 0] = '\c0%1 got thumped with %4\'s grenade spread.';	// +[soph]
$DeathMessageCTurretKill[$DamageType::Grenade, 1] = '\c0%4 dropped some frag ordinance on %1.';
$DeathMessageCTurretKill[$DamageType::Grenade, 2] = '\c0%1 caught %4\'s air-to-ground grenades.';	// +[/soph]

$DeathMessageCTurretKill[$DamageType::StarHammer, 0] = '\c0%4 obliterates %1 with the Star Hammer.';
$DeathMessageCTurretKill[$DamageType::StarHammer, 1] = '\c0%4 delivers a Star Hammer blast straight to %1.';
$DeathMessageCTurretKill[$DamageType::StarHammer, 2] = '\c0%4\'s Star Hammer blast leaves %1 nothin\' but smokin\' boots.';

$DeathMessageCTurretKill[$DamageType::BlastHammer, 0] = '\c0%4 obliterates %1 with the Blast Hammer.';
$DeathMessageCTurretKill[$DamageType::BlastHammer, 1] = '\c0%4 chalks up another Blast Hammer victim, courtesy of %1.';
$DeathMessageCTurretKill[$DamageType::BlastHammer, 2] = '\c0%4 rides down on %6 Blast Hammer and smacks %1.';

$DeathMessageCTurretKill[$DamageType::MechRocket, 0] = '\c0%4 intercepts %1 with a mech rocket.';
$DeathMessageCTurretKill[$DamageType::MechRocket, 1] = '\c0%4 violently fragments %1 to death with %6 Mech Rocket.';
$DeathMessageCTurretKill[$DamageType::MechRocket, 2] = '\c0%4\'s mech rockets knock %1 into next week.';

$DeathMessageCTurretKill[$DamageType::Ecstacy, 0] = '\c0%1 lunches on a Shrike blaster sandwich, courtesy of %4.';
$DeathMessageCTurretKill[$DamageType::Ecstacy, 1] = '\c0%4\'s Shrike blaster catches %1 with %3 pants down.';
$DeathMessageCTurretKill[$DamageType::Ecstacy, 2] = '\c0%1 gets drilled big-time by %4\'s Shrike blaster.';

$DeathMessageCTurretKill[$DamageType::MPBMissile, 0] = '\c0%1 intersects nicely with %4\'s MPB Missile.';
$DeathMessageCTurretKill[$DamageType::MPBMissile, 1] = '\c0%4\'s MPB Missile makes armored chowder out of %1.';
$DeathMessageCTurretKill[$DamageType::MPBMissile, 2] = '\c0%1 has a brief, explosive fling with %4\'s MPB Missile.';

$DeathMessageCTurretKill[$DamageType::Gauss, 0] = '\c0%4 shreds %1 up with the gauss.';
$DeathMessageCTurretKill[$DamageType::Gauss, 1] = '\c0%4 happily blows %1 into pieces with %6 gauss.';
$DeathMessageCTurretKill[$DamageType::Gauss, 2] = '\c0%1 suffers a serious hosing from %4\'s gauss.';

$DeathMessageCTurretKill[$DamageType::Phaser, 0] = '\c0%4 melts %1 with %6 Phaser Cannon.';
$DeathMessageCTurretKill[$DamageType::Phaser, 1] = '\c0%4 says to %1: "You zigged when you should have zagged".';
$DeathMessageCTurretKill[$DamageType::Phaser, 2] = '\c0%1 is the victim of %4\'s ultra high-intensity phaser blast.';

$DeathMessageCTurretKill[$DamageType::PhaserCannon, 0] = '\c0%4 melts %1 with %6 Phaser Cannon.';
$DeathMessageCTurretKill[$DamageType::PhaserCannon, 1] = '\c0%4 says to %1: "You zigged when you should have zagged".';
$DeathMessageCTurretKill[$DamageType::PhaserCannon, 2] = '\c0%1 is the victim of %4\'s ultra high-intensity phaser blast.';

$DeathMessageCTurretKill[$DamageType::MB, 0] = '\c0%1 is the victim of %4\'s Mitzi Blast.';
$DeathMessageCTurretKill[$DamageType::MB, 1] = '\c0%4 bites %1 with a Mitzi blast.';
$DeathMessageCTurretKill[$DamageType::MB, 2] = '\c0%4 stops %1 dead with the Mitzi Blast Cannon.';

$DeathMessageCTurretKill[$DamageType::MitziTransparent, 0] = '\c0%1 is the victim of %4\'s Mitzi Blast.';
$DeathMessageCTurretKill[$DamageType::MitziTransparent, 1] = '\c0%4\'s Mitzi fatally punches %1 through shields and all.';
$DeathMessageCTurretKill[$DamageType::MitziTransparent, 2] = '\c0%4 stops %1 dead with the Mitzi Blast Cannon.';

$DeathMessageCTurretKill[$DamageType::Laser, 0] = '\c0%4 picks off %1 with a laser sentry.';
$DeathMessageCTurretKill[$DamageType::Laser, 1] = '\c0%4 uses %1 as the targeting dummy in a systems check.';
$DeathMessageCTurretKill[$DamageType::Laser, 2] = '\c0%1 finally realized what that red dot was.';

$DeathMessageCTurretKill[$DamageType::Missile, 0] = '\c0%1 rides %4\'s rocket.';
$DeathMessageCTurretKill[$DamageType::Missile, 1] = '\c0%1 gets buttraped by %4\'s missile.';
$DeathMessageCTurretKill[$DamageType::Missile, 2] = '\c0%4\'s missile rains little pieces of %1 all over the ground.';

$DeathMessageCTurretKill[$DamageType::Disc, 0] = '\c0%4 demolishes %1 with the spinfusor.';
$DeathMessageCTurretKill[$DamageType::Disc, 1] = '\c0%4 serves %1 a blue plate special.';
$DeathMessageCTurretKill[$DamageType::Disc, 2] = '\c0%4 shares a little blue friend with %1.';

$DeathMessageCTurretKill[$DamageType::FlameTurret, 0] = '\c0%4 rings %1 up as extra crispy recipe.'; //Death~bot
$DeathMessageCTurretKill[$DamageType::FlameTurret, 1] = '\c0%4\'s RAXX turret demonstrates 5000 degree goodness on %1.'; //Death~bot
$DeathMessageCTurretKill[$DamageType::FlameTurret, 2] = '\c0%4\'s RAXX turret roasts %1 dead.'; //Death~bot

$DeathMessageCTurretKill[$DamageType::Bullet, 0] = '\c0%4 rips %1 up with the chaingun.';
$DeathMessageCTurretKill[$DamageType::Bullet, 1] = '\c0%4 happily chews %1 into pieces with %6 chaingun.';
$DeathMessageCTurretKill[$DamageType::Bullet, 2] = '\c0%4 administers a dose of Vitamin Lead to %1.';

$DeathMessageCTurretKill[$DamageType::EnergyBlast, 0] = '\c0%4 pelts %1 with a Energy Blast.';
$DeathMessageCTurretKill[$DamageType::EnergyBlast, 1] = '\c0%1 is the victim of concentrated energy.';
$DeathMessageCTurretKill[$DamageType::EnergyBlast, 2] = '\c0%4 smacks %1 with a hot Energy Blast.';

$DeathMessageCTurretKill[$DamageType::PBW, 0] = '\c0%4 gives %1 the smackdown with %3 heavy PBW.';		// 'tank' -[soph]
$DeathMessageCTurretKill[$DamageType::PBW, 1] = '\c0%4\'s vehicular PBW beams %1 well... apart really.';
$DeathMessageCTurretKill[$DamageType::PBW, 2] = '\c0%4\'s vehicular PBW blasts %1 to kibbles \'n bits.';	// [/soph]

$DeathMessageCTurretKill[$DamageType::PlasmaCannon, 0] = '\c0%4 electrocutes %1 with the plasma cannon.';
$DeathMessageCTurretKill[$DamageType::PlasmaCannon, 1] = '\c0%4 entices %1 to try a faceful of turret-style plasma.';
$DeathMessageCTurretKill[$DamageType::PlasmaCannon, 2] = '\c0%4 burns and fries %1 with the Plasma Cannon.';

$DeathMessageCTurretKill[$DamageType::Blaster, 0] = '\c0%4 kills %1 with a turbo blaster.';
$DeathMessageCTurretKill[$DamageType::Blaster, 1] = '\c0%1 gets a pointer in blaster use from %4.';
$DeathMessageCTurretKill[$DamageType::Blaster, 2] = '\c0%4 unleashes a terminal turbo blaster barrage into %1.';

$DeathMessageCTurretKill[$DamageType::CometCannon, 0] = '\c0%1 eats a big helping of %4\'s comet cannon.';
$DeathMessageCTurretKill[$DamageType::CometCannon, 1] = '\c0%4 shares a comet with %1.';
$DeathMessageCTurretKill[$DamageType::CometCannon, 2] = '\c0%1 makes friends and promptly explodes from %4\'s comet cannon.';

$DeathMessageTurretSelfKillCount = 3;
$DeathMessageTurretSelfKill[0] = '\c0%1 somehow kills %2self with a turret.';
$DeathMessageTurretSelfKill[1] = '\c0%1 apparently didn\'t know the turret was loaded.';
$DeathMessageTurretSelfKill[2] = '\c0%1 helps his team by killing himself with a turret.';

$bogusDeathMessage = $DeathMessage[ %banana , 0 ] ;	// craziest bugfix hack EVER -soph