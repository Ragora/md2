if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// TYPES OF ALLOWED DAMAGE
//--------------------------------------------------------------------------

$DamageType::Default    		 = 0;
$DamageType::Blaster			    = 1;
$DamageType::Plasma     		 = 2;
$DamageType::Bullet     		 = 3;
$DamageType::Disc				    = 4;
$DamageType::Grenade			    = 5;
$DamageType::Laser				 = 6;     // NOTE: This value is referenced directly in code.  DO NOT CHANGE!
$DamageType::ELF				    = 7;
$DamageType::Mortar				 = 8;
$DamageType::Missile			    = 9;
$DamageType::ShockLance			 = 10;
$DamageType::Mine				    = 11;
$DamageType::Explosion			 = 12;
$DamageType::Impact				 = 13;	// Object to object collisions
$DamageType::Ground				 = 14;	// Object to ground collisions
$DamageType::Turret				 = 15;

$DamageType::PlasmaTurret 		 = 16;
$DamageType::AATurret	 		 = 17;
$DamageType::ElfTurret 			 = 18;
$DamageType::MortarTurret 		 = 19;
$DamageType::MissileTurret 	 = 20;
$DamageType::IndoorDepTurret	 = 21;
$DamageType::OutdoorDepTurret	 = 22;
$DamageType::SentryTurret		 = 23;

$DamageType::OutOfBounds		 = 24;
$DamageType::Lava				    = 25;

$DamageType::ShrikeBlaster		 = 26;
$DamageType::BellyTurret		 = 27;
$DamageType::BomberBombs		 = 28;
$DamageType::TankChaingun		 = 29;
$DamageType::TankMortar			 = 30;
$DamageType::SatchelCharge	 	 = 31;
$DamageType::MPBMissile			 = 32;
$DamageType::Lightning         = 33;
$DamageType::VehicleSpawn      = 34;
$DamageType::ForceFieldPowerup = 35;
$DamageType::Crash             = 36;

$DamageType::MB                = 37;
$DamageType::PBW               = 38;
$DamageType::Phaser            = 39;
$DamageType::Sniper            = 40;
$DamageType::ElectroMissile    = 41;
$DamageType::PWT               = 42;
$DamageType::PlasmaCannon      = 43;
$DamageType::EnergyBlast       = 44;
$DamageType::Nuke              = 45;
$DamageType::Gauss             = 46;
$DamageType::Annihalator       = 47;
$DamageType::EMP               = 48;
$DamageType::Burn              = 49;
$DamageType::BurnLoop          = 50;
$DamageType::Poison            = 51;
$DamageType::PoisonLoop        = 52;
$DamageType::MitziTransparent  = 53;
$DamageType::BlueBlaster       = 54;
$DamageType::Flak              = 55;
$DamageType::Protron           = 56;
$DamageType::Ripper            = 57;
$DamageType::Ecstacy           = 58;
$DamageType::Shotgun           = 59;
$DamageType::BTMine            = 60;
$DamageType::AutoCannon        = 61;
$DamageType::RAXX              = 62;
$DamageType::IonCannon         = 63;
$DamageType::StarHammer        = 64;
$DamageType::Pulse             = 66;
$DamageType::RMSS              = 67;
$DamageType::FlameTurret       = 69;
$DamageType::PAC               = 70;
$DamageType::ChargedBullet     = 71;
$DamageType::ProtronFire       = 72 ;	// +soph
$DamageType::MechStomp         = 73 ;	// +soph

$DamageType::Water             = 97;
$DamageType::NexusCamping	   = 98;
$DamageType::Suicide	       = 99;

// Specific Death Message defines
$DamageType::BAReactor         = 100;
$DamageType::DaishiReactor     = 101;
$DamageType::MechReactor       = 102;
$DamageType::PunchBeam         = 103;
$DamageType::MechLaser         = 104;
$DamageType::MechMissile       = 105;
$DamageType::MegaMissile       = 106;
$DamageType::DenseMitzi        = 107;
$DamageType::PhaserCannon      = 108;
$DamageType::MechRocket        = 109;
$DamageType::MechHowitzer      = 110;
$DamageType::MechMinigun       = 111;
$DamageType::MultiFusor        = 112;
$DamageType::ShockCannon       = 113;
$DamageType::BlastHammer       = 114;
$DamageType::PPC               = 115;
$DamageType::AutoCannonAP      = 116;
$DamageType::DaishiTC          = 117;
$DamageType::CometCannon       = 118;
$DamageType::RFL               = 119;
$DamageType::RFLPlasma         = 120;
$DamageType::MegaBlaster       = 121;
$DamageType::ShieldPulse       = 122;
$DamageType::FissionBlaster    = 123;
$DamageType::Nosferatu         = 124;
$DamageType::MitziBooster      = 125;
$DamageType::VectorAccident    = 126;	// for the vector -soph

$InheritDamageProfile[$DamageType::BAReactor] = $DamageType::Mortar;
$InheritDamageProfile[$DamageType::DaishiReactor] = $DamageType::RAXX;
$InheritDamageProfile[$DamageType::MechReactor] = $DamageType::Mortar;
$InheritDamageProfile[$DamageType::PunchBeam] = $DamageType::Laser;
$InheritDamageProfile[$DamageType::MechLaser] = $DamageType::Laser;
$InheritDamageProfile[$DamageType::MechMissile] = $DamageType::Missile;
$InheritDamageProfile[$DamageType::MegaMissile] = $DamageType::Mortar;
$InheritDamageProfile[$DamageType::DenseMitzi] = $DamageType::MB;
$InheritDamageProfile[$DamageType::PhaserCannon] = $DamageType::Phaser;
$InheritDamageProfile[$DamageType::MechRocket] = $DamageType::Missile;
$InheritDamageProfile[$DamageType::MechHowitzer] = $DamageType::MissileTurret;
$InheritDamageProfile[$DamageType::MechMinigun] = $DamageType::Bullet;
$InheritDamageProfile[$DamageType::MultiFusor] = $DamageType::Disc;
$InheritDamageProfile[$DamageType::ShockCannon] = $DamageType::Shocklance;
$InheritDamageProfile[$DamageType::BlastHammer] = $DamageType::StarHammer;
$InheritDamageProfile[$DamageType::PPC] = $DamageType::ELF;
$InheritDamageProfile[$DamageType::AutoCannonAP] = $DamageType::Plasma;
$InheritDamageProfile[$DamageType::CometCannon] = $DamageType::BellyTurret;
$InheritDamageProfile[$DamageType::DaishiTC] = $DamageType::Bullet;
$InheritDamageProfile[$DamageType::RFL] = $DamageType::Laser;
$InheritDamageProfile[$DamageType::RFLPlasma] = $DamageType::Plasma;
$InheritDamageProfile[$DamageType::MegaBlaster] = $DamageType::Blaster;
$InheritDamageProfile[$DamageType::ShieldPulse] = $DamageType::Default;
$InheritDamageProfile[$DamageType::FissionBlaster] = $DamageType::Blaster;
$InheritDamageProfile[$DamageType::Nosferatu] = $DamageType::ELF;
$InheritDamageProfile[$DamageType::MitziBooster] = $DamageType::MB;

// Damage Groups

function getDamageGroup(%dmgType)
{
     %type = $DamageGroup[%dmgType];
     
     if(%type & $DamageGroupMask::Energy)
          return $DamageGroupMask::Energy;
     else if(%type & $DamageGroupMask::Explosive)
          return $DamageGroupMask::Explosive;
     else if(%type & $DamageGroupMask::Kinetic)
          return $DamageGroupMask::Kinetic;
     else if(%type & $DamageGroupMask::Plasma)
          return $DamageGroupMask::Plasma;
     else if(%type & $DamageGroupMask::Mitzi)
          return $DamageGroupMask::Mitzi;
     else if(%type & $DamageGroupMask::Poison)
          return $DamageGroupMask::Poison;                                                            
     else
          return $DamageGroupMask::Misc;
}

$DamageGroupMask::Misc             = 1 << 0;
$DamageGroupMask::Energy           = 1 << 1;
$DamageGroupMask::Explosive        = 1 << 2;
$DamageGroupMask::Kinetic          = 1 << 3;
$DamageGroupMask::Plasma           = 1 << 4;
$DamageGroupMask::Mitzi            = 1 << 5;
$DamageGroupMask::Poison           = 1 << 6;
$DamageGroupMask::HitSound         = 1 << 7;
$DamageGroupMask::IgnoreShield     = 1 << 8;
$DamageGroupMask::IgnoreRepulsor   = 1 << 9;
$DamageGroupMask::IgnoreSoulShield = 1 << 10;
$DamageGroupMask::ExternalMeans    = 1 << 11;

// Damage Type Mask definitions
$DamageGroup[$DamageType::Default] = $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Blaster] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::Plasma] = $DamageGroupMask::Plasma | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Bullet] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Disc] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Grenade] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Laser] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::ELF] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::Mortar] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Missile] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::ShockLance] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::Mine] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Explosion] = $DamageGroupMask::Explosive | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Impact] = $DamageGroupMask::Kinetic | $DamageGroupMask::Misc | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::Ground] = $DamageGroupMask::Kinetic | $DamageGroupMask::Misc | $DamageGroupMask::IgnoreRepulsor | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Turret] = $DamageGroupMask::Misc;
$DamageGroup[$DamageType::PlasmaTurret] = $DamageGroupMask::Plasma | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::AATurret] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::ElfTurret] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::MortarTurret] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MissileTurret] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::IndoorDepTurret] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::OutdoorDepTurret] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::SentryTurret] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::OutOfBounds] = $DamageGroupMask::Misc | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor | $DamageGroupMask::IgnoreSoulShield | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Lava] = $DamageGroupMask::Plasma | $DamageGroupMask::IgnoreRepulsor;		// = $DamageGroupMask::Plasma | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor; -soph
$DamageGroup[$DamageType::ShrikeBlaster] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::BellyTurret] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::BomberBombs] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::TankChaingun] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::TankMortar] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::SatchelCharge] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MPBMissile] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Lightning] = $DamageGroupMask::Energy | $DamageGroupMask::Misc | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::VehicleSpawn] = $DamageGroupMask::Misc | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::ForceFieldPowerup] = $DamageGroupMask::Misc | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor | $DamageGroupMask::IgnoreSoulShield | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Crash] = $DamageGroupMask::Kinetic | $DamageGroupMask::Misc | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::MB] = $DamageGroupMask::Mitzi | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::PBW] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::Phaser] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Sniper] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::ElectroMissile] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::PWT] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::PlasmaCannon] = $DamageGroupMask::Plasma | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::ProtronFire] = $DamageGroupMask::Plasma ;					// +soph
$DamageGroup[$DamageType::EnergyBlast] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Nuke] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::Gauss] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Annihalator] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::EMP] = $DamageGroupMask::Energy | $DamageGroupMask::IgnoreShield;
$DamageGroup[$DamageType::Burn] = $DamageGroupMask::Plasma | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::BurnLoop] = $DamageGroupMask::Plasma | $DamageGroupMask::IgnoreShield;
$DamageGroup[$DamageType::Poison] = $DamageGroupMask::Misc | $DamageGroupMask::Poison | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::PoisonLoop] = $DamageGroupMask::Misc | $DamageGroupMask::Poison | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::MitziTransparent] = $DamageGroupMask::Mitzi | $DamageGroupMask::HitSound | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::BlueBlaster] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Flak] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Protron] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Ripper] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Ecstacy] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Shotgun] = $DamageGroupMask::Kinetic;
$DamageGroup[$DamageType::BTMine] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::AutoCannon] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::RAXX] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::IonCannon] = $DamageGroupMask::Energy;
$DamageGroup[$DamageType::StarHammer] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Pulse] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::RMSS] = $DamageGroupMask::Explosive;
$DamageGroup[$DamageType::FlameTurret] = $DamageGroupMask::Plasma;
$DamageGroup[$DamageType::PAC] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::ChargedBullet] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Water] = $DamageGroupMask::Misc | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreSoulShield;
$DamageGroup[$DamageType::NexusCamping] = $DamageGroupMask::Misc | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreSoulShield | $DamageGroupMask::ExternalMeans;
$DamageGroup[$DamageType::Suicide] = $DamageGroupMask::Misc | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreSoulShield | $DamageGroupMask::ExternalMeans;

$DamageGroup[$DamageType::BAReactor] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::DaishiReactor] = $DamageGroupMask::Plasma ;					// -soph| $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MechReactor] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::PunchBeam] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MechLaser] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MechMissile] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MegaMissile] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::DenseMitzi] = $DamageGroupMask::Mitzi | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::PhaserCannon] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MechRocket] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MechHowitzer] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MechMinigun] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MultiFusor] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::ShockCannon] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::BlastHammer] = $DamageGroupMask::Explosive | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::PPC] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::AutoCannonAP] = $DamageGroupMask::Plasma | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::DaishiTC] = $DamageGroupMask::Kinetic | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::RFL] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::RFLPlasma] = $DamageGroupMask::Plasma | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::MegaBlaster] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound | $DamageGroupMask::IgnoreShield | $DamageGroupMask::IgnoreRepulsor;
$DamageGroup[$DamageType::FissionBlaster] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;
$DamageGroup[$DamageType::Nosferatu] = $DamageGroupMask::Energy | $DamageGroupMask::HitSound;


$DamageTypeText[0] = 'default';
$DamageTypeText[1] = 'blaster';
$DamageTypeText[2] = 'plasma';
$DamageTypeText[3] = 'chaingun';
$DamageTypeText[4] = 'disc';
$DamageTypeText[5] = 'grenade';
$DamageTypeText[6] = 'laser';
$DamageTypeText[7] = 'ELF';
$DamageTypeText[8] = 'mortar';
$DamageTypeText[9] = 'missile';
$DamageTypeText[10] = 'shocklance';
$DamageTypeText[11] = 'mine';
$DamageTypeText[12] = 'explosion';
$DamageTypeText[13] = 'impact';
$DamageTypeText[14] = 'ground';
$DamageTypeText[15] = 'turret';
$DamageTypeText[16] = 'plasma turret';
$DamageTypeText[17] = 'AA turret';
$DamageTypeText[18] = 'ELF turret';
$DamageTypeText[19] = 'mortar turret';
$DamageTypeText[20] = 'missile turret';
$DamageTypeText[21] = 'clamp turret';
$DamageTypeText[22] = 'spike turret';
$DamageTypeText[23] = 'sentry turret';
$DamageTypeText[24] = 'out of bounds';
$DamageTypeText[25] = 'lava';
$DamageTypeText[26] = 'shrike blaster';
$DamageTypeText[27] = 'belly turret';
$DamageTypeText[28] = 'bomber bomb';
$DamageTypeText[29] = 'tank chaingun';
$DamageTypeText[30] = 'tank mortar';
$DamageTypeText[31] = 'satchel charge';
$DamageTypeText[32] = 'MPB missile';
$DamageTypeText[33] = 'lighting';
$DamageTypeText[34] = 'vehicle spawn';
$DamageTypeText[35] = 'forceField';
$DamageTypeText[36] = 'crash';
$DamageTypeText[ $DamageType::MB ] = 'mitzi cannon' ;			// +[soph]
$DamageTypeText[ $DamageType::PBW ] = 'particle beam' ;			// +
$DamageTypeText[ $DamageType::Phaser ] = 'phaser' ;			// +
$DamageTypeText[ $DamageType::Sniper ] = 'sniper shell' ;		// +
$DamageTypeText[ $DamageType::ElectroMissile ] = 'soul missile' ;	// +
$DamageTypeText[ $DamageType::PWT ] = 'fuck you' ;			// +
$DamageTypeText[ $DamageType::PlasmaCannon ] = 'heavy plasma' ;		// +
$DamageTypeText[ $DamageType::EnergyBlast ] = 'energy blast' ;		// +
$DamageTypeText[ $DamageType::Nuke ] = 'shockwave' ;			// +
$DamageTypeText[ $DamageType::Gauss ] = 'mass driver' ;			// +
$DamageTypeText[ $DamageType::Annihalator ] = 'annihilator' ;		// +
$DamageTypeText[ $DamageType::EMP ] = 'fuck you more' ;			// +
$DamageTypeText[ $DamageType::Burn ] = 'thermal lance' ;		// +
$DamageTypeText[ $DamageType::BurnLoop ] = 'burn' ;			// +
$DamageTypeText[ $DamageType::Poison ] = 'poison lance' ;		// +
$DamageTypeText[ $DamageType::PoisonLoop ] = 'poison' ;			// +
$DamageTypeText[ $DamageType::MitziTransparent ] = 'MSTB' ;		// +
$DamageTypeText[ $DamageType::BlueBlaster ] = 'blue blaster' ;		// +
$DamageTypeText[ $DamageType::Flak ] = 'flak' ;				// +
$DamageTypeText[ $DamageType::Protron ] = 'protron' ;			// +
$DamageTypeText[ $DamageType::Ripper ] = 'ripper disc' ;		// +
$DamageTypeText[ $DamageType::Ecstacy ] = 'EPC shotgun' ;		// +
$DamageTypeText[ $DamageType::Shotgun ] = 'pack shotgun' ;		// +
$DamageTypeText[ $DamageType::BTMine ] = 'scattermine' ;		// +
$DamageTypeText[ $DamageType::AutoCannon ] = 'autocannon' ;		// +
$DamageTypeText[ $DamageType::RAXX ] = 'flamethrower' ;			// +
$DamageTypeText[ $DamageType::IonCannon ] = 'ion beam' ;		// +
$DamageTypeText[ $DamageType::StarHammer ] = 'starhammer' ;		// +
$DamageTypeText[ $DamageType::Pulse ] = 'protron pulse' ;		// +
$DamageTypeText[ $DamageType::RMSS ] = 'cruise missile' ;		// +
$DamageTypeText[ $DamageType::FlameTurret ] = 'fireball' ;		// +
$DamageTypeText[ $DamageType::PAC ] = 'EPC bomb' ;			// +
$DamageTypeText[ $DamageType::ChargedBullet ] = 'not used' ;		// +
$DamageTypeText[ $DamageType::ProtronFire ] = 'protron torch' ;		// +
$DamageTypeText[ $DamageType::MechStomp ] = 'mech stomp' ;		// +
$DamageTypeText[ $DamageType::Water ] = 'water' ;			// +
$DamageTypeText[ $DamageType::NexusCamping ] = 'nexus camping' ;	// +
$DamageTypeText[ $DamageType::Suicide ] = 'suicide' ;			// +
$DamageTypeText[ $DamageType::BAReactor ] = 'BA detonation' ;		// +
$DamageTypeText[ $DamageType::DaishiReactor ] = 'breach' ;		// +
$DamageTypeText[ $DamageType::MechReactor ] = 'mech detonation' ;	// +
$DamageTypeText[ $DamageType::PunchBeam ] = 'EPC beam' ;		// +
$DamageTypeText[ $DamageType::MechLaser ] = 'mech laser' ;		// +
$DamageTypeText[ $DamageType::MechMissile ] = 'mech missile' ;		// +
$DamageTypeText[ $DamageType::MegaMissile ] = 'heavy missile' ;		// +
$DamageTypeText[ $DamageType::DenseMitzi ] = 'dense mitzi' ;		// +
$DamageTypeText[ $DamageType::PhaserCannon ] = 'heavy phaser' ;		// +
$DamageTypeText[ $DamageType::MechRocket ] = 'BA rockets' ;		// +
$DamageTypeText[ $DamageType::MechHowitzer ] = 'kinetic rockets' ;	// +
$DamageTypeText[ $DamageType::MechMinigun ] = 'BA minigun' ;		// +
$DamageTypeText[ $DamageType::MultiFusor ] = 'rapid disc' ;		// +
$DamageTypeText[ $DamageType::ShockCannon ] = 'shock blaster' ;		// +
$DamageTypeText[ $DamageType::BlastHammer ] = 'heavy hammer' ;		// +
$DamageTypeText[ $DamageType::PPC ] = 'lightning gun' ;			// +
$DamageTypeText[ $DamageType::AutoCannonAP ] = 'incendiary autocannon' ;
$DamageTypeText[ $DamageType::DaishiTC ] = 'wat' ;			// +
$DamageTypeText[ $DamageType::CometCannon ] = 'cannon comet' ;		// +
$DamageTypeText[ $DamageType::RFL ] = 'laser assault rifle' ;		// +
$DamageTypeText[ $DamageType::RFLPlasma ] = 'obsolete' ;		// +
$DamageTypeText[ $DamageType::MegaBlaster ] = 'mega blaster' ;		// +
$DamageTypeText[ $DamageType::ShieldPulse ] = 'shield shockwave' ;	// +
$DamageTypeText[ $DamageType::FissionBlaster ] = 'fission blaster' ;	// +
$DamageTypeText[ $DamageType::Nosferatu ] = 'not a thing' ;		// +
$DamageTypeText[ $DamageType::MitziBooster ] = 'booster' ;		// +
$DamageTypeText[ $DamageType::VectorAccident ] = 'vector mishap' ;	// +[/soph]

// ##### PLEASE DO NOT REORDER THE DAMAGE PROFILE TABLES BELOW #####
// (They are set up in the same order as the 'Weapons Matrix.xls' sheet for ease of reference when balancing)

$UDP::UniversalDamage = 1.0;

datablock SimDataBlock(UniversalDamageProfile) // All damages go here
{
   damageScale[$DamageType::Blaster] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Blaster] = $UDP::UniversalDamage;
   damageScale[$DamageType::Bullet] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Bullet] = $UDP::UniversalDamage;
   damageScale[$DamageType::ChargedBullet] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::ChargedBullet] = $UDP::UniversalDamage * 2.0;
   damageScale[$DamageType::ELF] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::ELF] = $UDP::UniversalDamage;
   damageScale[$DamageType::ShockLance] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::ShockLance] = $UDP::UniversalDamage;
   damageScale[$DamageType::Laser] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Laser] = $UDP::UniversalDamage;
   damageScale[$DamageType::ShrikeBlaster] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::ShrikeBlaster] = $UDP::UniversalDamage;
   damageScale[$DamageType::BellyTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::BellyTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::AATurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::AATurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::IndoorDepTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::IndoorDepTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::OutdoorDepTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::OutdoorDepTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::SentryTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::SentryTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::Disc] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Disc] = $UDP::UniversalDamage;
   damageScale[$DamageType::Grenade] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Grenade] = $UDP::UniversalDamage;
   damageScale[$DamageType::Mine] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Mine] = $UDP::UniversalDamage;
   damageScale[$DamageType::Missile] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Missile] = $UDP::UniversalDamage;
   damageScale[$DamageType::Mortar] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Mortar] = $UDP::UniversalDamage;
   damageScale[$DamageType::Plasma] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Plasma] = $UDP::UniversalDamage;
   damageScale[$DamageType::BomberBombs] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::BomberBombs] = $UDP::UniversalDamage;
   damageScale[$DamageType::TankChaingun] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::TankChaingun] = $UDP::UniversalDamage;
   damageScale[$DamageType::TankMortar] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::TankMortar] = $UDP::UniversalDamage;
   damageScale[$DamageType::MissileTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::MissileTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::MortarTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::MortarTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::PlasmaTurret] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::PlasmaTurret] = $UDP::UniversalDamage;
   damageScale[$DamageType::SatchelCharge] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::SatchelCharge] = $UDP::UniversalDamage;
   damageScale[$DamageType::Default] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Default] = $UDP::UniversalDamage;
   damageScale[$DamageType::Impact] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Impact] = $UDP::UniversalDamage;
   damageScale[$DamageType::Ground] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Ground] = $UDP::UniversalDamage;
   damageScale[$DamageType::Explosion] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Explosion] = $UDP::UniversalDamage;
   damageScale[$DamageType::Lightning] = $UDP::UniversalDamage * 4;
   shieldDamageScale[$DamageType::Lightning] = $UDP::UniversalDamage * 10;

   damageScale[$DamageType::BurnLoop] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::BurnLoop] = $UDP::UniversalDamage;
   damageScale[$DamageType::PoisonLoop] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::PoisonLoop] = $UDP::UniversalDamage;
   damageScale[$DamageType::AutoCannon] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::AutoCannon] = $UDP::UniversalDamage;
   damageScale[$DamageType::Ecstacy] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Ecstacy] = $UDP::UniversalDamage;
   damageScale[$DamageType::Protron] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Protron] = $UDP::UniversalDamage;
   damageScale[$DamageType::Flak] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Flak] = $UDP::UniversalDamage;
   damageScale[$DamageType::BlueBlaster] = $UDP::UniversalDamage * 2.0;
   shieldDamageScale[$DamageType::BlueBlaster] = 0;
   damageScale[$DamageType::MitziTransparent] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::MitziTransparent] = 0;
   damageScale[$DamageType::Poison] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Poison] = $UDP::UniversalDamage;
   damageScale[$DamageType::Burn] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Burn] = $UDP::UniversalDamage;
   damageScale[$DamageType::EMP] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::EMP] = $UDP::UniversalDamage;
   damageScale[$DamageType::Annihalator] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Annihalator] = $UDP::UniversalDamage;
   damageScale[$DamageType::Gauss] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Gauss] = $UDP::UniversalDamage;
   damageScale[$DamageType::Nuke] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Nuke] = $UDP::UniversalDamage;
   damageScale[$DamageType::EnergyBlast] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::EnergyBlast] = $UDP::UniversalDamage;
   damageScale[$DamageType::PlasmaCannon] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::PlasmaCannon] = $UDP::UniversalDamage;
   damageScale[$DamageType::ProtronFire] = $UDP::UniversalDamage;	// +soph
   shieldDamageScale[$DamageType::ProtronFire] = $UDP::UniversalDamage;	// +soph
   damageScale[$DamageType::ElectroMissile] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::ElectroMissile] = $UDP::UniversalDamage;
   damageScale[$DamageType::Sniper] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Sniper] = $UDP::UniversalDamage;
   damageScale[$DamageType::Phaser] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Phaser] = $UDP::UniversalDamage;
   damageScale[$DamageType::PBW] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::PBW] = $UDP::UniversalDamage;
   damageScale[$DamageType::MB] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::MB] = $UDP::UniversalDamage;
   damageScale[$DamageType::BTMine] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::BTMine] = $UDP::UniversalDamage;
   damageScale[$DamageType::RAXX] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::RAXX] = $UDP::UniversalDamage;
   damageScale[$DamageType::IonCannon] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::IonCannon] = $UDP::UniversalDamage;
   damageScale[$DamageType::StarHammer] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::StarHammer] = $UDP::UniversalDamage;
   damageScale[$DamageType::Pulse] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Pulse] = $UDP::UniversalDamage;
   damageScale[$DamageType::Ripper] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Ripper] = $UDP::UniversalDamage;
   damageScale[$DamageType::PAC] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::PAC] = $UDP::UniversalDamage;   
};


//----------------------------------------------------------------------------
// VEHICLE DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(VehicleDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::PlasmaCannon] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::PlasmaCannon] = $UDP::UniversalDamage;
};

//**** BEOWULF TANK ****
datablock SimDataBlock(TankDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	= 4 ;
};

//----------------------------------------------------------------------------
// TURRET DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(TurretDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::StarHammer] = 1.0;
   shieldDamageScale[$DamageType::StarHammer] = 1.0;
};

//----------------------------------------------------------------------------
// STATIC SHAPE DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(StaticShapeDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Explosion] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Explosion] = $UDP::UniversalDamage;
};

datablock SimDataBlock(BlastParametricsDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Bullet] = $UDP::UniversalDamage;
   damageScale[$DamageType::Grenade] = $UDP::UniversalDamage;
};

datablock SimDataBlock(BombardmentDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Disc] = 0;
   shieldDamageScale[$DamageType::Disc] = 0;
   damageScale[$DamageType::Grenade] = 0;
   shieldDamageScale[$DamageType::Grenade] = 0;
   damageScale[$DamageType::Mine] = 0;
   shieldDamageScale[$DamageType::Mine] = 0;
   damageScale[$DamageType::Missile] = 0;
   shieldDamageScale[$DamageType::Missile] = 0;
   damageScale[$DamageType::Mortar] = 0;
   shieldDamageScale[$DamageType::Mortar] = 0;
   damageScale[$DamageType::BomberBombs] = 0;
   shieldDamageScale[$DamageType::BomberBombs] = 0;
   damageScale[$DamageType::MissileTurret] = 0;
   shieldDamageScale[$DamageType::MissileTurret] = 0;
   damageScale[$DamageType::MortarTurret] = 0;
   shieldDamageScale[$DamageType::MortarTurret] = 0;
   damageScale[$DamageType::Explosion] = 0;
   shieldDamageScale[$DamageType::Explosion] = 0;

   damageScale[$DamageType::Nuke] 		= 0;
   shieldDamageScale[$DamageType::Nuke] 	= 0;
   damageScale[$DamageType::Gauss] 		= 0;
   shieldDamageScale[$DamageType::Gauss] 	= 0;
   damageScale[$DamageType::AutoCannon] = 0;
   shieldDamageScale[$DamageType::AutoCannon] = 0;
   damageScale[$DamageType::Annihalator] 	= 0;
   shieldDamageScale[$DamageType::Annihalator]= 0;
   damageScale[$DamageType::Flak] 		= 0;
   shieldDamageScale[$DamageType::Flak] 	= 0; 
   damageScale[$DamageType::StarHammer] = 0.0;
   shieldDamageScale[$DamageType::StarHammer] = 0.0;
   damageScale[$DamageType::RMSS] = 0.0;
   shieldDamageScale[$DamageType::RMSS] = 0.0;
};
