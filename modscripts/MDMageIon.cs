// Mage-Ion concept

function toggleMageIon(%client, %player, %newArmor)
{
     %toggle = %newArmor.armorMask & $ArmorMask::MagIon ? true : false;
     
     if(%toggle)
     {
          if(!%player.isMageIon)
          {
               %player.isMageIon = true;
               %player.maxSoulCount = 10;
               
               if(%player.soulCount $= "")
               {
                  %player.soulCount = 0;
                  %player.maxSoulPassive = false;
               }

               %player.soulWeight = 0.05;						// +soph

//               commandToClient(%client, 'setAmmoHudCount', "Souls:" SPC %player.soulCount);
//               mageIonThreadLoop(%player, %client);
          }
          
//          calculateSoulBonuses(%player, %player.soulCount);				// -soph
          schedule( 32, %player, calculateSoulBonuses, %player, %player.soulCount );	// +soph
     }
     else if(%player.isMageIon)
     {
          %player.isMageIon = false;
          %player.soulCount = "";
          calculateSoulBonuses(%player, 0);
//          %player.meditateTrigger = false;    
//          cancel(%player.mageIonThread);
         
//          %player.mageIonThread = "";
     }
}

function mageIonThreadLoop(%obj, %client)
{
     if(isObject(%obj) && %obj.isMageIon)
     {
          // For some reason, T2 does strange things with null values ("") so this is necessary:
          %flag = %obj.getMountedImage($FlagSlot);
          %flagMounted = isObject(%flag) ? (%flag.noSpecialRegen == true ? true : false) : false;
          %vehicleMounted = isObject(%obj.getObjectMount());
          
          if(!%flagMounted && !%vehicleMounted && %obj.meditateTrigger)
          {
               if(!%obj.meditating)
               {
                    %obj.meditating = true;
                    %obj.playShieldEffect("0 0 1");
                    
                    %base = new StaticShape()
                    {
                         dataBlock = "mReflector";
                    };
               
                    %base2 = new StaticShape()
                    {
                         dataBlock = "DaishiDeployPad";
                         scale = "0.25 0.25 0.25";
                    };
               
                    %base2.startFade(1, 0, true);
                    %base.setPosition(vectorAdd(%obj.getPosition(), "0 0 0.001"));
                    %base2.setPosition(%obj.getPosition());
                    %obj.basePad = %base;
                    %obj.basePad2 = %base2;
                    %base.mountObject(%obj, 1);
                    %base.daishiUnmountBypass = true;
                    %base.noJump = true;
               
                    commandToClient(%obj.client,'SetDefaultVehicleKeys', true);
                    commandToClient(%obj.client,'SetPilotVehicleKeys', true);
                    commandToClient(%obj.client,'SetPassengerVehicleKeys', true);

                    %obj.prevRecharge = %obj.getRechargeRate();
                    %obj.setRechargeRate(%obj.prevRecharge * 15);
                    %obj.setImageTrigger($BackpackSlot, false);                    
               }

               zapEffect(%obj, "BlueShift");               
               
               %energyPct = %obj.getEnergyPct();
               %pct = mFloor(%energyPct * 100);
               %graph = createColorGraph("|", 35, %energyPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mCeil(%obj.getEnergyLevel()); // SPC %pct@"%";
               %text = %pct == 100 ? %graph SPC "[Full]" : %graph SPC "[Charging]";
     
               bottomPrint(%obj.client, ">> Meditating PSI-Energy <<" NL %text, 3, 2);
          }
          else
          {
               if(%obj.meditating)
               {
                    %obj.meditating = false;
                    %obj.playShieldEffect("0 0 -1");
                    %obj.setRechargeRate(%obj.prevRecharge);
                    
                    if(isObject(%obj.basePad))
                    {
                         %obj.basePad.unmountObject(1);
                         %obj.basePad.delete();
                         %obj.basePad2.delete();
                    }
               }
          }
          
          %obj.mageIonThread = schedule(200, %obj, mageIonThreadLoop, %obj, %client);
     }
}

function MageRegisterKill(%obj)
{
     if(%obj.dead)
          return;
     
     zapEffect(%obj, "HealGreen");

     %soulDelta = %obj.efficiencyMod ? 3 : 2;
     %obj.soulCount += %soulDelta;
     
     addStatTrack(%obj.client.guid, "totalsoulpower", %soulDelta);
     
     if(%obj.soulCount > %obj.maxSoulCount)
     {
          %remainder = %obj.soulCount - %obj.maxSoulCount;
          %obj.soulCount = %obj.maxSoulCount;

          %obj.setDamageLevel(%obj.getDamageLevel() - (%obj.getDatablock().maxDamage * (%remainder * 0.05)));
     }
     
     %obj.setEnergyLevel(%obj.getEnergyLevel() + (%obj.getDatablock().maxEnergy * 0.1));
     calculateSoulBonuses(%obj, %obj.soulCount);
          
     if(!%obj.client.isAIControlled())
     {
          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);
     
          activateDeploySensorGrn(%obj);
          schedule(1000, %obj, deactivateDeploySensor, %obj);

          bottomPrint(%obj.client, "Detected unit death, harvesting soul power..." NL "Soul Power:" SPC %obj.soulCount@"/"@%obj.maxSoulCount, 5, 2);
     }     
}

function MageRegisterBotKill(%obj)
{
     if(%obj.dead)
          return;
     
     zapEffect(%obj, "HealGreen");

     %obj.setEnergyLevel(%obj.getEnergyLevel() + (%obj.getDatablock().maxEnergy * 0.1));          

     if(!%obj.client.isAIControlled())
     {
          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);
     
          activateDeploySensorGrn(%obj);
          schedule(1000, %obj, deactivateDeploySensor, %obj);

          bottomPrint(%obj.client, "Detected bot death, harvesting soul energy.", 5, 1);
     }     
}

function calculateSoulBonuses(%obj, %amount)
{
     %data = %obj.getDatablock();

     // Max Soul Passive bonus, determined on delta
     //%obj.maxSoulPassive = (%obj.soulCount == %obj.maxSoulCount);					// -soph
     if( %obj.soulCount == %obj.maxSoulCount )								// +soph
          %obj.maxSoulPassive = true;									// +soph

//     %obj.setRechargeRate(%data.rechargeRate + (%data.rechargeRate * (0.15 * %obj.soulCount)));	// -soph
     %obj.setRechargeRate( %data.rechargeRate + ( %data.rechargeRate * ( 0.125 * %obj.soulCount ) ) ) ;	// +soph
     
     if(%obj.powerMod)
          %obj.damageMod = 1.1 + (%obj.soulCount * 0.05);
	else												// +soph
		%obj.damageMod = 0 ;									// +soph
}

function MageRegisterHit(%obj, %col, %amount)
{
//     %rnd = getRandom();							// -soph
     if( %col.team != %obj.team && !( %col.team == 0 && !%col.player ) )	// if( %col.team != %obj.team && %col.team != 0 ) -soph
          if(%amount > 0.3 )							// && %rnd < 0.1) -soph
          {
               if( getRandom() < %obj.soulWeight )				// +[soph]
               {								// +
                    %obj.soulWeight /= 2 ;					// +
                    if( %obj.soulWeight < 0.05 )				// +
                         %obj.soulWeight = 0.05 ;				// +[/soph]
                    %obj.soulCount++;
               
                    if(%obj.soulCount > %obj.maxSoulCount)
                    {
                         %remainder = %obj.soulCount - %obj.maxSoulCount;
                         %obj.soulCount = %obj.maxSoulCount;
     
                         %obj.setDamageLevel(%obj.getDamageLevel() - (%obj.getDatablock().maxDamage * (%remainder * 0.05)));
                    }
     
                    %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
                    commandToClient(%obj.client, 'setAmmoHudCount', %graph);
                    
                    calculateSoulBonuses(%obj, %obj.soulCount);
               }
               else								// +[soph]
                    %obj.soulWeight += mSqRt( mSqRt( %amount * 100 ) ) / 100 ;	// + possible 0.0234 to 0.0538 
										// +[/soph]
          }

     if( %obj.powerMod && %obj.maxSoulPassive && %amount > 0.2 )		// else if(%obj.powerMod && %obj.maxSoulPassive && %rnd < 0.2 && %amount > 0.2) -soph
     {
          if( getRandom() < ( %obj.soulCount * 0.03 ) )				// +soph
          {
               zapEffect(%obj, "MBDenseLance");
          
               %p = new LinearFlareProjectile()
               {
                   dataBlock        = ReaverCharge;
                   initialDirection = %col.getForwardVector();
                   initialPosition  = %col.getWorldBoxCenter();
                   sourceObject     = %col;
                   sourceSlot       = 0;
                   bkSourceObject   = %obj;
               };
          
               MissionCleanup.add(%p);     
               %p.sourceObject = %obj;
          }
     }
}

function MageCalculateShields(%targetObject, %position, %amount, %damageType)
{
   if(!isObject(%targetObject))
      return 0;

//   if($DamageGroup[%damageType] & $DamageGroupMask::IgnoreShield)
//      return %amount;

   if(%targetObject.guardianMod)
   {
//     if(%targetObject.maxSoulPassive)							// -[soph]
//     {										// -
//         if(getRandom() < ( 0.02 * %obj.soulCount ) )					// -
//         {										// -
//              commandToClient(%obj.client, 'BottomPrint', ">> Soul Mirror activated, reflected "@mFloor(%amount * 100)@" HP damage <<", 4, 1);
//              %obj.guardianShield = true;						// -
//              zapEffect(%obj, "BlueShift");						// -
//              return 0;								// -
//         }										// -
//     }										// -[/soph]
     if(%targetObject.guardianAbsorb)
     {
          %targetObject.setDamageLevel(%targetObject.getDamageLevel() - (%amount *= 0.05));
          %targetObject.playShieldEffect("0 0 1");
          return 0;
     }
     else
     {
          if( %targetObject.maxSoulPassive && getRandom() < ( 0.02 * %targetObject.soulCount ) )	// should not supercede absorb +soph
          {
//              commandToClient(%targetObject.client, 'BottomPrint', ">> Soul Mirror activated, reflected "@mFloor(%amount * 100)@" HP damage <<"  4, 1);
              %targetObject.guardianShield = true;
              zapEffect(%targetObject, "BlueShift");
              return 0;	
          }
//        %retDmg = %amount * (1 - (0.1 + %soulCount * 0.05));				// -soph
//        return %retDmg;								// -soph
          return ( %amount * ( 1 - ( 0.2 + ( %targetObject.soulCount * 0.0425 ) ) ) ) ;	// +soph
     }
   }
   else if(%targetObject.archMageMod)
   {
        if( %amount <= ( 0.02 * %targetObject.soulCount ) )				// if(%amount <= 0.2) -soph
           return 0;
   }
//   else										// -soph
        return %amount;
}

datablock LinearFlareProjectileData(PSIDetectCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 15;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 750;

   explosion           = "ConcMortarExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function triggerPSIDetectWave(%data, %obj)
{
     %p = mageIonProcessFire(%data, %obj, 0, 0, 0, LinearFlareProjectile, PSIDetectCharge, 10000, 1, "", "", "PlasmaFireWetSound");

     if(%p)
          mageIonDetecting(%obj);
}

$MageDetectProjectiles = "SeekerProjectile" SPC "BombProjectile";

function mageIonDetecting(%obj)
{
     if(isObject(%obj))
     {
          InitContainerRadiusSearch(%obj.getWorldBoxCenter(), 25, $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

          while((%int = ContainerSearchNext()) != 0)
          {
               if(isReflectableProjectile(%int, $MageDetectProjectiles))
               {
                    if(!isObject(%obj.reflectorObj))
                         %obj.reflectorObj = createReflectorForObject(%obj);

                    %FFRObject = %obj.reflectorObj;

                    %FFRObject.setPosition(%int.getPosition());

                    if(!isObject(%int.sourceObject))
                         %src = %int.bkSourceObject;
                    else
                         %src = %int.sourceObject;

                    %p = new LinearFlareProjectile()
                    {
                         dataBlock         = "ReaverCharge";
                         initialDirection  = "0 0 1";
                         initialPosition   = %int.getPosition();
                         sourceObject      = %FFRObject;
                         sourceSlot        = %int.sourceSlot;
                    };

                    MissionCleanup.add(%p);
                    FlareSet.add(%p);
                    %obj.useEnergy(10);
                    %int.delete();
               }
               else if(%int.getType() & $TypeMasks::PlayerObjectType && getRandom() > 0.5)
                    EMPObject(%int);
               else
                    continue;
          }

          if(isObject(%obj.reflectorObj))
               %obj.reflectorObj.schedule(32, delete);
     }
}

// Mage Ion modes:
$WeaponModeCount[MagicMissile] = 3;								// = 4; [soph]
$WeaponMode[MagicMissile, 0] = "Seeker";							// "25 HP / 1 SP 10 Energy";
$WeaponMode[MagicMissile, 1] = "Screamer";							// "50 HP / 1 SP 20 Energy";
$WeaponMode[MagicMissile, 2] = "Howler";							// "75 HP / 1 SP 30 Energy";
$WeaponMode[MagicMissile, 3] = "Shard Swarm";							// "100 HP / 1 SP 40 Energy";
//$WeaponMode[MagicMissile, 4] = "125 HP / 1 SP 50 Energy";					// --
$WeaponModeDescription[MagicMissile, 0] = "A missile of pure energy";				// yay flavor text
$WeaponModeDescription[MagicMissile, 1] = "Seeker amplified with the pain of others";
$WeaponModeDescription[MagicMissile, 2] = "Hunts with the fury of a dying curse";
$WeaponModeDescription[MagicMissile, 3] = "Exhausts your energy to unleash a flurry of shards";
$WeaponModeData[MagicMissile, 0] = "Damage: 50 HP | Cost: 40 Energy";
$WeaponModeData[MagicMissile, 1] = "Damage: 175 HP | Cost: 1 Soul, 50 Energy";
$WeaponModeData[MagicMissile, 2] = "Damage: 375 HP | Cost: 2 Souls, 60 Energy";
$WeaponModeData[MagicMissile, 3] = "Damage: 25x HP | Cost: 1 Soul, All remaining Energy";	// [/soph]
$WeaponModeName[MagicMissile] = "Soul Missile Launcher [Energy]";

$WeaponModeCount[SoulComet] = 1;
$WeaponMode[SoulComet, 0] = "Single Blast";
$WeaponMode[SoulComet, 1] = "Dual Blast";
$WeaponModeDescription[SoulComet, 0] = "Astral force at your fingertips";
$WeaponModeDescription[SoulComet, 1] = "Greater power, less control";
$WeaponModeData[SoulComet, 0] = "Damage: 255  | Cost: 2 Souls";
$WeaponModeData[SoulComet, 1] = "Damage: 2 x 255  | Cost: 3 Souls";
$WeaponModeName[SoulComet] = "Soul Comet [Energy]";

$WeaponModeCount[SoulShocker] = 2;
$WeaponMode[SoulShocker, 0] = "Single Shocker";
$WeaponMode[SoulShocker, 1] = "Dual Shocker";
$WeaponMode[SoulShocker, 2] = "Triple Shocker";
$WeaponModeDescription[SoulShocker, 0] = "Death has never been so shocking";
$WeaponModeDescription[SoulShocker, 1] = "Double the balls for double the fun";
$WeaponModeDescription[SoulShocker, 2] = "A stimulating electric massage with an unhappy ending";
$WeaponModeData[SoulShocker, 0] = "Damage: 53 +9/shock | Cost: 3 Souls";
$WeaponModeData[SoulShocker, 1] = "Damage: 2 x 53 +9/shock  | Cost: 5 Souls";
$WeaponModeData[SoulShocker, 2] = "Damage: 3 x 53 +9/shock  | Cost: 6 Souls";
$WeaponModeName[SoulShocker] = "Soul Shocker [Energy]";

$WeaponModeCount[SoulHammer] = 1;							// +soph
//$WeaponData[SoulHammer] = "Damage: 170 + 20x HP | Cost: 2 Souls";			// -soph
$WeaponMode[SoulHammer, 0] = "Single Hamma";						// +[soph]
$WeaponMode[SoulHammer, 1] = "FFFFFFFFFFFFFF-";
$WeaponModeDescription[SoulHammer, 0] = "Weapon of the gods";
$WeaponModeDescription[SoulHammer, 1] = "Give your life for glory!";
$WeaponModeData[SoulHammer, 0] = "Damage: 170  | Cost: 2 Souls";
$WeaponModeData[SoulHammer, 1] = "Damage: HOLY SHIT  | Cost: 10 Souls and Thou";	// +[/soph]
$WeaponModeName[SoulHammer] = "Soul Hammer [Explosive]";

$WeaponModeCount[SoulStrike] = 1;
$WeaponMode[SoulStrike, 0] = "Single Shot";						// "100-125 HP / 1 soul power"; -[soph]
$WeaponMode[SoulStrike, 1] = "Full Discharge";						// "125-600 HP / all remaining soul power";
$WeaponModeDescription[SoulStrike, 0] = "Hurls a single, powerful blast";		// +[soph]
$WeaponModeDescription[SoulStrike, 1] = "Exhausts your soul power for one devastating blow";
$WeaponModeData[SoulStrike, 0] = "Damage: 175 HP | Cost: 1 Soul";			// +
$WeaponModeData[SoulStrike, 1] = "Damage: 125-1250 HP | Cost: All remaining Souls";	// +[/soph]
$WeaponModeName[SoulStrike] = "Soul Swarmer [Phasing Mitzi]";				// "Soul Swarmer [Mitzi]"; -[/soph]

//
exec("scripts/modscripts/mage/projectiles.cs");
exec("scripts/modscripts/mage/weapons.cs");
