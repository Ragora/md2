if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//==> Player External Hook
// Scripts

function BASelfDestruct(%obj)
{
   %p = new LinearFlareProjectile()
   {
      dataBlock        = BAArmorBlowout;
      initialDirection = vectorRandom();
      initialPosition  = %obj.getPosition();
      sourceObject     = %obj;
      sourceSlot       = 0;
   };
   MissionCleanup.add(%p);
   %p.ignoreReflections = true ;				// +soph
   schedule( 400 , 0 , BASelfDestructPart2 , %obj , %p );	// +soph

  // %obj.setDamageFlash(0.75);					// -[soph]
  // %obj.blowup();						// moved to projectile onexplode
  // %obj.blowedup = true;					// -[/soph]
//   %obj.getDatablock().damageObject(%obj, %obj, %obj.position, 1000, %p.getDatablock().radiusDamageType, %obj.getVelocity(), false);
}

function BASelfDestructPart2( %obj , %proj )
{
   if( isObject( %obj ) )
   {
      if( isObject( %proj ) )
         %proj.setPosition( %obj.getPosition() ) ;
      trackerUpdateCheck( %obj ) ;				// fix for target corpse tagging +soph
   }
}

function DaiSelfDestruct(%obj, %ext, %dt)
{
   %isExt = isObject(%ext.client);				// _ +soph

   %target = %isExt ? %ext : %obj;				// _ _ +soph

   %vector = vectorNormalize( %obj.getMuzzleVector( 0 ) );	// +soph
   %p = new LinearFlareProjectile()
   {
      dataBlock        = DaiArmorBlowout;
      initialDirection = %vector ;				// vectorRandom(); -soph
      initialPosition  = %obj.getPosition();
      sourceObject     = %obj;
      sourceSlot       = 0;
      damageFactor     = 125 ;					// +soph
      bkSourceObject   = %target ;				// +soph
   };
   %p.ignoreReflections = true ;				// +soph
   MissionCleanup.add(%p);

   %p = new LinearFlareProjectile()				// +[soph]
   {								// +
      dataBlock        = DaiArmorBlowout ;			// +
      initialDirection = vectorScale( %vector , -1 ) ;		// +
      initialPosition  = %obj.getPosition() ;			// +
      sourceObject     = %obj ;					// +
      sourceSlot       = 0 ;					// +
      damageFactor     = 125 ;					// +
      bkSourceObject   = %target ;				// +
   } ;								// +
   %p.ignoreReflections = true ;				// +
   MissionCleanup.add( %p ) ;					// +[/soph]

//   %isExt = isObject(%ext.client);				// ^ -soph
   %type = %isExt ? %dt : %p.getDatablock().radiusDamageType;
//   %target = %isExt ? %ext : %obj;				// ^ ^ -soph
   
   %obj.setDamageFlash(0.75);
   %obj.blowup();
   %obj.blowedup = true;
   %obj.getDatablock().damageObject(%obj, %target, %obj.position, 1000, %type, %obj.getVelocity(), false);
}

function TurboChargerBreach( %object , %source , %damageType , %vector )	// +[soph]
{										// +
   if( !%source || !isObject( %source ) )					// +
       %source = %object ;							// +
										// +
   if( !%damageType )								// +
      %damageType = $Damagetype::DaishiReactor ;				// +
   										// +
   if( %vector $= "" )								// +
   {										// +
      %vector = %object.getForwardVector() ;					// +
      if( %vector $= "" )							// +
         %vector = "0 0 1" ;							// +
      %vector = getWord( %vector , 0 ) SPC getWord( %vector , 1 )  SPC "0" ;	// +
   }										// +
										// +
   %object.setInventory( %object.getMountedImage( $BackpackSlot ).item , 0 );	// +
  // %object.unmountImage( $BackpackSlot );					// +
   										// +
   %position = %object.getWorldBoxCenter();					// +
   %position = getWord( %position , 0) SPC getWord( %position , 1 ) SPC ( getWord( %position , 2 ) + 1 );
										// +
   %p = new LinearFlareProjectile()						// +
   {										// +
      dataBlock        = DaiArmorBlowout;					// +
      initialDirection = vectorScale( %vector , -1 );				// +
      initialPosition  = %position ;						// +
      sourceObject     = %object;						// +
      sourceSlot       = 0;							// +
      damageFactor     = %object.getEnergyLevel() + 45;				// +
      bkSourceObject   = %source;						// +
      variableType     = %damageType;						// +
   };										// +
   %p.ignoreReflections = true ;						// +
   MissionCleanup.add(%p);							// +
}										// +[soph]

function TMSelfDestruct(%obj , %detonate )					// (%obj) -soph
{
   if( %obj.owner )								// +[soph]
      %source = %obj.owner.player ;						// +
   else										// +
      %source = %obj ;								// +[/soph]

   %p = new LinearFlareProjectile()
   {
      dataBlock        = BTArmorBlowout;
      initialDirection = vectorRandom();
      initialPosition  = %obj.getPosition();
      sourceObject     = %obj; 
      sourceSlot       = 0;
   };
   %p.ignoreReflections = true ;						// +soph
   %p.sourceObject = %source ;							// +soph
    MissionCleanup.add(%p);

   %obj.setDamageFlash(0.75);
   if( %detonate )								// +soph
   {
      %obj.blowup();
      %obj.blowedup = true;
   }
//   %obj.getDatablock().damageObject(%obj, %obj, %obj.position, 1000, %p.getDatablock().radiusDamageType, %obj.getVelocity(), false);	// -soph
}

function BTSelfDestruct(%obj)
{
   %p = new LinearFlareProjectile()
   {
      dataBlock        = BTArmorBlowout;
      initialDirection = vectorRandom();
      initialPosition  = %obj.getPosition();
      sourceObject     = %obj;
      sourceSlot       = 0;
   };
   %p.ignoreReflections = true ;						// +soph
   MissionCleanup.add(%p);

   %obj.setDamageFlash(0.75);
   %obj.blowup();
   %obj.blowedup = true;
//   %obj.getDatablock().damageObject(%obj, %obj, %obj.position, 1000, %p.getDatablock().radiusDamageType, %obj.getVelocity(), false);
}

function BAExplode(%targetObject)
{
   loopDamageFX(%targetObject, 4, 19, 4, LinearFlareProjectile, BADeathCharge);
   schedule(getRandom(6000, 4000), 0, "BASelfDestruct", %targetObject);
}

function MountAPEsToObject(%obj)
{
   if(isObject(%obj))
   {
//      echo("APEs mounted.");
      createSlotExtension(%obj, Meltdown.APE_mountPoint, "SlotExtension2"); // APE Slot Extension (SL2 - muzzlepoint)
      %obj.slotExtension[Meltdown.APE_mountPoint].isSyn = true; // avoid EMP

      %obj.slotExtension[Meltdown.APE_mountPoint].mountImage("EMPAPEImage", Meltdown.APE_EMP_Mount);
      %obj.slotExtension[Meltdown.APE_mountPoint].mountImage("FireVisAPEImage", Meltdown.APE_FireVis_Mount);
      %obj.slotExtension[Meltdown.APE_mountPoint].mountImage("FireSmokeAPEImage", Meltdown.APE_FireSmoke_Mount);
   }
   else
      error("Error! Cannot mount an APE to a non-existant object!");
}

//function Armor::onAdd(%data,%obj)
//{
//   Parent::onAdd(%data, %obj);
//   // Vehicle timeout
//   %obj.mountVehicle = true;
//
//   // Default dynamic armor stats
//   %obj.setRechargeRate(%data.rechargeRate);
//   %obj.setRepairRate(0);
//
//   %obj.setSelfPowered();
//
//   MountAPEsToObject(%obj);
//}

function Armor::onAdd(%data,%obj)
{
   Parent::onAdd(%data, %obj);
   // Vehicle timeout
   %obj.mountVehicle = true;

   // Default dynamic armor stats
   %obj.setRechargeRate(%data.rechargeRate);
   %obj.setRepairRate(0);

   %obj.setSelfPowered();
   
   %obj.universalResistFactor = 1.0;
   %obj.shieldStrengthFactor = 1.0;
   %obj.damageMod = 1.0;
   %obj.preventFlagCaps = false;
   %obj.heatShielded = false;
   %obj.hasAmmoPack = false;   
   %obj.statusFlags = 0;
   %obj.repairKitFactor = 1.0;
   %obj.isAlive = true;
   %obj.soulshieldException = false;
   %obj.damaged = false;
   %obj.dead = false;
   %obj.isGenuinePlayer = true;

   %obj.dynamicResistance[$DamageGroupMask::Misc] = 1.0;
   %obj.dynamicResistance[$DamageGroupMask::Energy] = 1.0;
   %obj.dynamicResistance[$DamageGroupMask::Explosive] = 1.0;
   %obj.dynamicResistance[$DamageGroupMask::Kinetic] = 1.0;
   %obj.dynamicResistance[$DamageGroupMask::Plasma] = 1.0;
   %obj.dynamicResistance[$DamageGroupMask::Mitzi] = 1.0;
   %obj.dynamicResistance[$DamageGroupMask::Poison] = 1.0;
   
   schedule(1000, %obj, cloneCheck, %obj);
}

function Armor::onMount(%this, %obj, %vehicle, %node)
{
   %vehicle.passengerCount++;

   if( %vehicle.damageMod > 0 ) {}	// because !0.5 registers as true wtf -soph
   else
      %vehicle.damageMod = 1;
      
   if( %vehicle.defenseMod > 0 ) {}	// better put this here too just in case -soph
   else
      %vehicle.defenseMod = 1;      

   %vehicleData = %vehicle.getDatablock();

   if(%vehicleData.damageModPerMount)
     %vehicle.damageMod += %vehicleData.damageModPerMount;

   if(%vehicleData.defenseModPerMount)
     %vehicle.defenseMod += %vehicleData.defenseModPerMount;     

   if(%vehicleData.damageModSlot[%node])
     %vehicle.damageMod += %vehicleData.damageModSlot[%node];

 //  if( %vehicleData.damageMod )	// -soph
 //    %vehicle.damageMod = %vehicleData.damageMod;

//  if(%obj.holdingFlag && !%vehicle.getDatablock().multiPassenger)	// -[soph]
//   {
//        %flag = %obj.holdingFlag;
//        %flag.setVelocity("0 0 0");
//        %flag.setPosition(getRandomT(10000) SPC getRandomT(10000) SPC getRandomN(10000, 5000));
//        %flag.setCollisionTimeout(%obj);
//        %obj.throwObject(%flag);
//        messageClient(%obj.client, 'MsgCannotPilotWithFlag', '\c2Only multi-passenger vehicles can hold a flag.');
//   }									// -[/soph]
   
   if(%node == 0)
   {
      // Node 0 is the pilot's pos.
      %obj.setTransform("0 0 0 0 0 1 0");
      %obj.setActionThread(%vehicle.getDatablock().mountPose[%node],true,true);

      if(!%obj.inStation)
         %obj.lastWeapon = (%obj.getMountedImage($WeaponSlot) == 0 ) ? "" : %obj.getMountedImage($WeaponSlot).getName().item;

       %obj.unmountImage($WeaponSlot);
      commandToClient( %player.client , 'setAmmoHudCount' , "" ) ;

      if(!%obj.client.isAIControlled())
      {
         %obj.setControlObject(%vehicle);
         %obj.client.setObjectActiveImage(%vehicle, 2);
      }

      //E3 respawn...

      if(%obj == %obj.lastVehicle.lastPilot && %obj.lastVehicle != %vehicle)
      {
         schedule(15000, %obj.lastVehicle,"vehicleAbandonTimeOut", %obj.lastVehicle);
          %obj.lastVehicle.lastPilot = "";
      }
      if(%vehicle.lastPilot !$= "" && %vehicle == %vehicle.lastPilot.lastVehicle)
            %vehicle.lastPilot.lastVehicle = "";

      %vehicle.abandon = false;
      %vehicle.lastPilot = %obj;
      %obj.lastVehicle = %vehicle;

      // update the vehicle's team
      if((%vehicle.getTarget() != -1) && %vehicle.getDatablock().cantTeamSwitch $= "")
      {
         setTargetSensorGroup(%vehicle.getTarget(), %obj.client.getSensorGroup());
         if( %vehicle.turretObject > 0 )
            setTargetSensorGroup(%vehicle.turretObject.getTarget(), %obj.client.getSensorGroup());
      }

      // Send a message to the client so they can decide if they want to change view or not:
      commandToClient( %obj.client, 'VehicleMount' );
   }
   else
   {
      // tailgunner/passenger positions
      if(%vehicle.getDataBlock().mountPose[%node] !$= "")
         %obj.setActionThread(%vehicle.getDatablock().mountPose[%node]);
      else
         %obj.setActionThread("root", true);
   }
   // -------------------------------------------------------------------------
   // z0dd - ZOD, 6/18/02. announce to any other passengers that you've boarded
   if(%vehicle.getDatablock().numMountPoints > 1)
   {
      %nodeName = findNodeName(%vehicle, %node); // function in vehicle.cs
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
      {
         if (%vehicle.getMountNodeObject(%i) > 0)
         {
            messageClient( %vehicle.getMountNodeObject(%i).client, 'MsgShowPassenger', '\c3%1\c2 has boarded in the \c3%2\c2 position.', %obj.client.name, %nodeName, true );
            commandToClient( %vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, true);
         }
      }
   }
   //make sure they don't have any packs active
//    if ( %obj.getImageState( $BackpackSlot ) $= "activate")
//       %obj.use("Backpack");
   if (%obj.getImageTrigger($BackpackSlot) && !%obj.bassActive && %obj.getMountedImage($BackpackSlot).item !$= "SynomiumPack")	// bugfix +soph
      %obj.setImageTrigger($BackpackSlot, false);

   //AI hooks
   %obj.client.vehicleMounted = %vehicle;
   AIVehicleMounted(%vehicle);
   if(%obj.client.isAIControlled())
      %this.AIonMount(%obj, %vehicle, %node);
}

function Armor::onUnmount(%this, %obj, %vehicle, %node)
{
   %vehicle.passengerCount--;
   
   %vehicleData = %vehicle.getDatablock();

   if(%vehicleData.damageModPerMount)
     %vehicle.damageMod -= %vehicleData.damageModPerMount;

   if(%vehicleData.defenseModPerMount)
     %vehicle.defenseMod -= %vehicleData.defenseModPerMount;
          
   if(%vehicleData.damageModSlot[%node])
     %vehicle.damageMod -= %vehicleData.damageModSlot[%node];

   if ( %node == 0 )
   {
      commandToClient( %obj.client, 'VehicleDismount' );
      commandToClient(%obj.client, 'removeReticle');

      if(%obj.inv[%obj.lastWeapon])
         %obj.use(%obj.lastWeapon);

      if(%obj.getMountedImage($WeaponSlot) == 0)
         %obj.selectWeaponSlot( 0 );

     // eject firing bugfix									// + [soph]
      if( %vehicle.fireWeapon )									// + 
         %vehicleData.onTrigger( %vehicle , 0 , 0 ) ;						// + [/soph]

      //Inform gunner position when pilot leaves...
      //if(%vehicle.getDataBlock().showPilotInfo !$= "")
      //   if((%gunner = %vehicle.getMountNodeObject(1)) != 0)
      //      commandToClient(%gunner.client, 'PilotInfo', "PILOT EJECTED", 6, 1);
   }
   else if( %node == 1 )
      if( %vehicle.turretObject )
         if( %vehicle.turretObject.fireTrigger )
            %vehicle.turretObject.getDatablock().onTrigger( %vehicle.turretObject , 0 , 0 ) ;	// +soph 
   
   %obj.isBomber = "";
   
   // ----------------------------------------------------------------------
   // z0dd - ZOD, 6/18/02. announce to any other passengers that you've left
   if(%vehicle.getDatablock().numMountPoints > 1)
   {
      %nodeName = findNodeName(%vehicle, %node); // function in vehicle.cs
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
      {
         if (%vehicle.getMountNodeObject(%i) > 0)
         {
            messageClient( %vehicle.getMountNodeObject(%i).client, 'MsgShowPassenger', '\c3%1\c2 has ejected from the \c3%2\c2 position.', %obj.client.name, %nodeName, false );
            commandToClient( %vehicle.getMountNodeObject(%i).client, 'showPassenger', %node, false);
         }
      }
   }
   //AI hooks
   %obj.client.vehicleMounted = "";
   if(%obj.client.isAIControlled())
      %this.AIonUnMount(%obj, %vehicle, %node);
}

function Armor::doDismount(%this, %obj, %forced)
{
   %mount = %obj.getObjectMount();

   // This function is called by player.cc when the jump trigger
   // is true while mounted
   if (!%mount)
      return;

   if(%mount.noJump)
      return;

   if(isObject(%mount.shield))
      %mount.shield.delete();

   commandToClient(%obj.client,'SetDefaultVehicleKeys', false);

   if(%obj.getObjectMount().daishiUnmountBypass)
   {
        commandToClient(%obj.client, 'setHudMode', 'Standard', "", 0);
        %obj.unmount();
        %obj.setPosition(%obj.deployPos);
        return;
   }

   // Position above dismount point

   %pos    = getWords(%obj.getTransform(), 0, 2);
   %oldPos = %pos;
   %vec[0] = " 0  0  1";
   %vec[1] = " 0  0  1";
   %vec[2] = " 0  0 -1";
   %vec[3] = " 1  0  0";
   %vec[4] = "-1  0  0";
   %numAttempts = 5;
   %success     = -1;
   %impulseVec  = "0 0 0";
   if (%obj.getObjectMount().getDatablock().hasDismountOverrides() == true)
   {
      %vec[0] = %obj.getObjectMount().getDatablock().getDismountOverride(%obj.getObjectMount(), %obj);
      %vec[0] = MatrixMulVector(%obj.getObjectMount().getTransform(), %vec[0]);
   }
   else
   {
      %vec[0] = MatrixMulVector( %obj.getTransform(), %vec[0]);
   }

   %pos = "0 0 0";
   for (%i = 0; %i < %numAttempts; %i++)
   {
      %pos = VectorAdd(%oldPos, VectorScale(%vec[%i], 5));  // z0dd - ZOD, 4/24/02. More ejection clearance. 5 was 3
      if (%obj.checkDismountPoint(%oldPos, %pos))
      {
         %success = %i;
         %impulseVec = %vec[%i];
         break;
      }
   }
   if (%forced && %success == -1)
   {
      %pos = %oldPos;
   }

   // hide the dashboard HUD and delete elements based on node
   commandToClient(%obj.client, 'setHudMode', 'Standard', "", 0);
   // Unmount and control body
   if(%obj.vehicleTurret)
      %obj.vehicleTurret.getDataBlock().playerDismount(%obj.vehicleTurret);
   %obj.unmount();
   if(%obj.mVehicle)
      %obj.mVehicle.getDataBlock().playerDismounted(%obj.mVehicle, %obj);

   // bots don't change their control objects when in vehicles
   if(!%obj.client.isAIControlled())
   {
      %vehicle = %obj.getControlObject();
      %obj.setControlObject(0);
   }

   %obj.mountVehicle = false;
   %obj.schedule(1500, "setMountVehicle", true);  // z0dd - ZOD, 3/29/02. Reduce time between being able to mount vehicles . Was 4000

   // Position above dismount point
   %obj.setTransform(%pos);
   %obj.playAudio(0, UnmountVehicleSound);
   %obj.applyImpulse(%pos, VectorScale(%impulseVec, %obj.getDataBlock().mass * 3));
   %obj.setPilot(false);
   %obj.vehicleTurret = "";

   // -----------------------------------------------------
   // z0dd - ZOD, 4/8/02. Set player velocity when ejecting
   %vel = %obj.getVelocity();
   %vec = vectorDot(%vel, vectorNormalize(%vel));
   if(%vec > 50)
   {
      %scale = 50 / %vec;
      %obj.setVelocity(VectorScale(%vel, %scale));
   }
   // -----------------------------------------------------

   if( %obj.client.observeCount > 0 )			// +soph
      resetObserveFollow( %obj.client , false ) ;	// +soph
}

function Armor::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
        //error("Armor::damageObject( "@%data@", "@%targetObject@", "@%sourceObject@", "@%position@", "@%amount@", "@%damageType@", "@%momVec@" )");
    // DarkDragonDX: Fix for People being flung by nukes in practice mode
    if (%targetObject.pdead && $votePracticeModeon && %damageType == $DamageType::Nuke) // Ghost got nuked
	%targetObject.schedule(600, "setVelocity", "0 0 0");
  
//   %targetObject.lastDamageTime = getSimTime(); // moving this down to serve another use -soph // observer damage hack

   if(%targetObject.invincible || (%targetObject.pdead && $votePracticeModeOn))		// || %targetObject.dead) -soph
      return;

   if( %targetObject.dead )			// +[soph] %targetObject.gibValue 
   {
      if( %targetObject.getArmorType() !$= $ArmorMask::BattleAngel && !%targetObject.blowedUp )
      {
         if( $DamageGroup[%damageType] & $DamageGroupMask::Explosive || %damageType == $DamageType::Crash )
            %gibFactor = 1.5;
         else if( %damageType == $DamageType::Ground || %damageType == $DamageType::Impact )
            %gibFactor = 1;
         else
            %gibFactor = 0.5;
         if( $DamageGroup[ %damageType ] & $DamageGroupMask::Plasma || $DamageGroup[ %damageType ] & $DamageGroupMask::Mitzi )
            %noGib = true ;
         if( %gibFactor > 0 && %amount * %gibFactor >= %targetObject.gibValue && !%noGib )
         {
            %targetObject.setMomentumVector( %targetObject.getVelocity() );
            %targetObject.blowup() ;
            %targetObject.blowedUp = true;
            if( %targetObject.deathFiring )
            {
               %targetObject.setImageTrigger( 0 , false ) ;
               %targetObject.deathFiring = false ;
            }
         }
         else
         {
             %gibFactor *= %amount ;
             %targetObject.gibValue -= %gibFactor * %gibFactor / ( %targetObject.gibValue ) ; 
             %gibFactor = %targetObject.getDatablock().maxDamage / 15 ;
             if( %targetObject.gibValue < %gibFactor )
                %targetObject.gibValue = %gibFactor ;
         }
      }
      return;
   }						// +[/soph]

//   if(%targetObject.inJetfire)
//      return;
//   if(($DamageGroup[%damageType] & $DamageGroupMask::Plasma) && )
//   if((%damageType == $DamageType::Plasma || %damageType == $DamageType::FlameTurret || %damageType == $DamageType::Burn || %damageType == $DamageType::RAXX || %damageType == $DamageType::BurnLoop) && %targetObject.heatShielded)
     //      return;

   //if(%damageType == $DamageType::Plasma || %damageType == $DamageType::RAXX)
    //  %targetObject.jp_heatcount += 5000; // >8)
// -Nite- Jetpack Heat stuff
  // if(%damageType == $DamageType::Burn)
    //  %targetObject.jp_heatcount += 100;

   %sourceObject = isObject(%sourceObject) ? %sourceObject : 0;
   %player = %targetObject.getType() & $TypeMasks::PlayerObjectType;
   
   //----------------------------------------------------------------
   // z0dd - ZOD, 6/09/02. Check to see if this vehcile is destroyed,
   // if it is do no damage. Fixes vehicle ghosting bug. We do not
   // check for isObject here, destroyed objects fail it even though
   // they exist as objects, go figure.
   if(%damageType == $DamageType::Impact)
      if(%sourceObject && %sourceObject.getDamageState() $= "Destroyed")
         return;

   if (%targetObject.isMounted() && %targetObject.scriptKilled $= "")
   {
      %mount = %targetObject.getObjectMount();
      if(%mount.team == %targetObject.team)
      {
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found] )					// && (%damageType != $DamageType::PoisonLoop || %damageType != $DamageType::BurnLoop)) -soph
            {
               if( %damageType != $DamageType::PoisonLoop && %damageType != $DamageType::BurnLoop )	// now here +soph
               {
                  %mount.getDataBlock().damageObject(%mount, %sourceObject, %position, %amount, %damageType);
                  return;
               }
            }
            else
               %amount *= 0.5; // cut in half
         }
      }
   }

   %targetClient = %targetObject.getOwnerClient();
   %scType = %sourceObject ? %sourceObject.getType() : 0;
   
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else if(%scType)
   {
      if(%scType & $TypeMasks::VehicleObjectType)
          %sourceClient = %sourceObject.getMountNodeObject(0).client;
      else if(%scType & $TypeMasks::TurretObjectType)
          %sourceClient = %sourceObject.getControllingClient();  
      else if(%scType & $TypeMasks::PlayerObjectType)
          %sourceClient = %sourceObject.client;            
      else
          %sourceClient = %sourceObject.getOwnerClient();        
   }
   else 
      %sourceClient = 0;

//   %sourceClient = !%sourceClient ? %sourceObject.getMountNodeObject(0).client : %sourceClient;
//   %sourceClient = !%sourceClient ? %sourceObject.getControllingClient() : %sourceClient;
   
   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if(isObject(%sourceClient))
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(%damageType == $DamageType::Suicide)
      %sourceTeam = 0;
   //--------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/8/02. Check to see if this turret has a valid owner, if not clear the owner varible.
   //else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   //   %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else if(%sourceClient && %scType & $TypeMasks::TurretObjectType)
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      
      if(%sourceObject.owner !$="" && (%sourceObject.owner.team != %sourceObject.team || !isObject(%sourceObject.owner)))
         %sourceObject.owner = "";
   }
   // End z0dd - ZOD
   //--------------------------------------------------------------------------------------------------------------------
   else if( isObject(%sourceObject) &&
   	( %sourceObject.getType() & $TypeMasks::VehicleObjectType ))
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

//   if(%targetObject.getDataBlock().armorMask & $ArmorMask::Daishi && %targetObject.daishiFieldVisible) // this is a horrible hack
//   {
//     if($DamageGroup[%damageType] & $DamageGroupMask::IgnoreRepulsor)
//          %amount = %amount;
//     else
//        if(%targetObject.isShielded && !%targetObject.overdriveMode)
//             %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);     
//   }
//   else
     
     if(!isObject(%targetObject.projectedShield))
     {
          if(%targetObject.isMageIon)
               %amount = MageCalculateShields(%targetObject, %position, %amount, %damageType);
          else if(%targetObject.isShielded && !%targetObject.overdriveMode)
               %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);
     }
     else
     {
          %targetObject.playShieldEffect("0 0 1");
          %targetObject.projectedShield.getDatablock().damageObject(%targetObject.projectedShield, %sourceObject, %position, %amount, %damageType);
          %amount *= 0.25;
     }

     if(%targetObject.guardianShield)
     {
          if(%sourceObject != %targetObject)
          {
               %sourceObject.getDatablock().damageObject(%sourceObject, %targetObject, %position, %amount, %damageType);
               %targetObject.guardianShield = false;
               return;
          }
     }

   if(%targetObject.universalResistFactor !$= "")
     %amount *= %targetObject.universalResistFactor;

   if(%amount > 0.05 && %targetObject.BAPackID == 5)
   {
//        if(getRandom() < 0.05)	// replacing this all -[soph]
//        {
//                   %targetObject.ignoreDED = true;
//                   %targetObject.setMomentumVector(vectorNormalize(%targetObject.getVelocity()));
//                   Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);
//                   schedule(32, 0, "DaiSelfDestruct", %targetObject, %sourceObject, %damageType);
//                   %targetObject.deathFiring = false;
//                   return;
//         }				// -[/soph]

     // adapted shocklance code 	// +[soph]
      %forwardVec = %targetObject.getForwardVector();
      %objDir2D  = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0";
      %objPos   = %targetObject.getPosition();
      if( %position !$= "" )
      {
         %dif    = VectorSub( %objPos, %position );
         %dif    = getWord(%dif, 0) @ " " @ getWord(%dif, 1) @ " 0";
         %dif    = VectorNormalize(%dif);
         %dot    = VectorDot(%dif, %objDir2D);
         if ( %dot >= mCos( 1 ) )
           %chance = msqrt( %amount * 100 ) * ( %targetObject.getHeat() + 0.25 ) / 25;
         else
           %chance = 0;
      }
      else
         %chance = ( msqrt( %amount * 100 ) / 100 ) + ( %targetObject.getHeat() / 10 );
      if( getRandom() < %chance )
      {
         TurboChargerBreach( %targetObject , %sourceObject , %damageType , %objDir2D );
         if( %targetObject.dead )
            return;
      }					// +[/soph]
   }

//   if(!$teamdamage && %targetObject.team == %sourceObject.team)
//       return;

//   if(%targetObject == %sourceObject || %sourceObject.client.team != %targetObject.client.team)
//   {

//      if(%damageType == $DamageType::EMP && %amount > 0.05)					// -[soph]
//           %targetObject.EMPObject(%sourceObject);						// -
//      else if(%damageType == $DamageType::Poison && !%targetObject.isShielded && %amount > 0)	// -
//           %targetObject.PoisonObject(%sourceObject);						// -[/soph]

     // if(%damageType == $DamageType::RAXX)							// -[soph]
     //    if(getRandom(100) > 85)								// tackled by incendiaryexplosion in mdexternal.cs
     //          %targetObject.BurnObject(%sourceObject);					// -[/soph]
//   }
   
   if(%amount == 0)
      return;
   else if( %targetObject.armorShrug > 0 )							// +[soph]
   {												// +
      %amount -= %obj.armorShrug ;								// +
      if( %amount < 0 )										// +
         return ;										// +
   }												// +[/soph]
   

   if(%sourceObject && %sourceObject.isMageIon)
       MageRegisterHit(%sourceObject, %targetObject, %amount);

   if( %targetObject.getArmorType() & $ArmorMask::Blastech && 			// Set the damage flash, if not blastech and without power. - MrKeen
       getDamageGroup( %damageType ) & $DamageGroupMask::Explosive )		// +[soph]
   {										// +
      if( %targetObject.getEnergyLevel() > 5 ) 					// +
      {										// +;
         %drainFactor = %amount * 30 ;						// +
         %drainFactor *= mSqRt( %targetObject.dynamicResistance[ getDamageGroup( %damageType ) ] ) ;
         %targetObject.useEnergy( %drainFactor ) ; 				// +
         %targetObject.playShieldEffect( "0 0 0" ) ; 				// +[/soph]
         if( !%targetObject.client.isAIControlled() )         
         {
            activateDeploySensorRed( %targetObject ) ;
            schedule( 500 , %targetObject , deactivateDeploySensor , %targetObject ) ;  
            %drainFactor = mFloor( %drainFactor * 100 ) / 100 ;			// +soph
            %energyPct = %targetObject.getEnergyPct() ;
               %graph = createColorGraph( "|" , 35 , %energyPct , "<color:FF0000>" , "<color:FFFF00>" , "<color:00FF00>" , "<color:777777>" , 0.3 , 0.6 ) SPC mCeil( %targetObject.getEnergyLevel() ) SPC "KW" ; // SPC %pct@"%";
                    
            bottomPrint( %targetObject.client , "Blastech shield absorbed " @ %drainFactor @ "KW damage - capacitor status:" SPC mFloor( 100 * %energyPct ) SPC "%" , 3 , 1 ) ;
         }
         return ;
      }
      else
         %amount *= %targetObject.dynamicResistance[ getDamageGroup( %damageType ) ] ;  
   }
   else
   {
      if(!%targetObject.isMageIon)
      {
           %damageScale = $InheritDamageProfile[%damageType] > 0 ? %data.damageScale[$InheritDamageProfile[%damageType]] : %data.damageScale[%damageType];
     
           if(%damageScale !$= "")
              %amount *= %damageScale;
      }              

      %amount *= %targetObject.dynamicResistance[getDamageGroup(%damageType)];         
   }

   %flash = %targetObject.getDamageFlash() + ((%amount / %data.maxDamage) * 2);
   if(%flash > 0.75)
      %flash = 0.75;

   %previousDamage = %targetObject.getDamagePercent();
   %targetObject.setDamageFlash(%flash);

   %time = getSimTime();			// this is used for retroactive soul shield +soph

   if((%targetObject.isMageIon || %targetObject.soulShield) && %amount >= %targetObject.getDamageLeft())
   {
       if($DamageGroup[%damageType] & $DamageGroupMask::IgnoreSoulShield || %targetObject.soulshieldException || %targetObject.scriptKilled)
       {
          echo("[FLYTRAP] Caught bug: Soulshield suicide, handling exception");
       }
       else
       {
            if(%targetObject.isMageIon)
            {
                 %req = 5;
                 
                 if(%targetObject.soulCount >= %req)
                 {
                    %targetObject.soulCount -= %req;
                    
                    %graph = createGraph("*", %targetObject.maxSoulCount, (%targetObject.soulCount/%targetObject.maxSoulCount), "-");
                    commandToClient(%targetObject.client, 'setAmmoHudCount', %graph);
               
                    activateDeploySensorRed(%targetObject);
                    schedule(1000, %targetObject, deactivateDeploySensor, %targetObject);                    
                    %targetObject.playShieldEffect("0 0 1");
//                    %saves = mFloor(%targetObject.soulCount * 0.5);														// -soph
//                    bottomPrint(%targetObject.client, "You use soul power to protect you from otherwise deadly effects," SPC %saves SPC "remaining saves.", 5, 1);		// -soph
                    bottomPrint( %targetObject.client , "Your soul power deflects a fatal hit.\nYou have" SPC %targetObject.soulCount SPC "souls remaining." , 5 , 1 ) ;	// +soph

                   // retroactive soul shield					
                    if( %targetObject.lastDamageTime + 30 > %time )		// +[soph]
                         %targetObject.setDamageLevel( %targetObject.lastDamageLevel ) ;
                    %targetObject.invincible = true ;				// +
                    %targetObject.setDamageFlash( 0 ) ;				// +
                    %targetObject.setInvincibleMode( 0.1 , 0.1 ) ;		// +
                    zapEffect( %targetObject , "BlueShift" ) ;			// +
                    messageClient( %targetObject.client , 'MsgSoulShieldUsed' , '\c2* %1 souls consumed - death prevented.' , %req ) ;
                    schedule( 100 , 0 , soulShieldFade , %targetObject ) ;	// +
                    calculateSoulBonuses( %obj ) ;				// +[/soph]

                    return;
                 }
                 else
                 {
                    echo("[FLYTRAP] Caught bug: Soulshield suicide, handling exception");
                 }
            }
            else
            {
                 %targetObject.soulShield = false ;				// +soph
                 %targetObject.setInventory(SoulShieldMod, 0);
                 %targetObject.playShieldEffect("0 0 1"); 
                 bottomPrint(%targetObject.client, "The Soul Shield has protected you from damage that would have otherwise killed you! It's effects wear off...", 5, 2);

                // retroactive soul shield			
                 if( %targetObject.lastDamageTime + 30 > %time )		// +[soph]	
                      %targetObject.setDamageLevel( %targetObject.lastDamageLevel ) ;
                 %targetObject.invincible = true ;				// +
                 %targetObject.setDamageFlash( 0 ) ;				// +
                 %targetObject.setInvincibleMode( 0.1 , 0.1 ) ;			// +
                 zapEffect( %targetObject , "BlueShift" ) ;			// +
                 messageClient( %targetObject.client , 'MsgSoulShieldUsed' , '\c2* Soul Shield consumed - death prevented.' ) ;
                 schedule( 100 , 0 , soulShieldFade , %targetObject ) ;		// +[/soph]

                 return;
            }
       }
   }

   if(($DamageGroup[%damageType] & $DamageGroupMask::IgnoreSoulShield || %targetObject.scriptKilled) && %targetObject.finalStandMode)
   {
        echo("[FLYTRAP] Caught bug: Final Stand suicide, handling exception");
   }
   else if(%targetObject.finalStandMode && %amount >= %targetObject.getDamageLeft() )	// && %targetObject.inDEDField) -soph
   {
        if(!%targetObject.overdriveMode)
          BattleAngelFinalStand(%targetObject.getDatablock(), %targetObject);
        
        %targetObject.fs_lastDamagedBy = %sourceObject;
        %targetObject.fs_lastDamagedType = %damageType;
        
        return;
   }
   				
   if( %targetObject.isMageIon || %targetObject.soulShield )			// +[soph]
        if( %targetObject.lastDamageTime + 30 < %time )				// +
             %targetObject.lastDamageLevel = %targetObject.getDamageLevel();	// +
   %targetObject.lastDamageTime = %time;					// +/[soph]
   %targetObject.applyDamage(%amount);

   Game.onClientDamaged(%targetClient, %sourceClient, %damageType, %sourceObject);

   if(%player)
        %dead = %targetObject.getState() $= "Dead" ? true : false;
   
//   echo(%dead SPC %targetObject.damaged SPC $DamageGroup[%damageType] & $DamageGroupMask::ExternalMeans);
   if(%dead && %targetObject.damaged && $DamageGroup[%damageType] & $DamageGroupMask::ExternalMeans)
   {
        %sourceObject = isObject(%targetObject.client.lastDamagedByPlayer) ? (%targetClient.lastDamagedByPlayer == %targetObject ? 0 : %targetObject.client.lastDamagedByPlayer) : 0;
        
        if(%sourceObject)
        {
             %sourceClient = %targetObject.client.lastDamagedBy;
             %damageType = %targetObject.client.lastDamageType;
//             echo("[DEBUG] Running suicide reassignment code ->" SPC %sourceObject SPC %sourceClient SPC %damageType);             
        }
   }
   else
   {
        if($DamageGroup[%damageType] & $DamageGroupMask::ExternalMeans)
        {
          // no spam
        }
        else if(%sourceObject)
            %targetObject.damaged = true;
        
        if(%sourceObject)
        {
             %targetClient.lastDamagedBy = %sourceObject.client;
             %targetClient.lastDamaged = getSimTime();
             %targetClient.lastDamageType = %damageType;
             %targetClient.lastDamageTeam = %sourceTeam;
             %targetClient.lastDamagedByPlayer = %sourceObject;
        }
   }
   
   //now call the "onKilled" function if the client was... you know...
   if(%dead)
   {
      %targetObject.dead = true;
      %targetObject.tetherActive = false;

      if(isObject(%sourceObject))
      {
//           if(%damageType == $DamageType::EnergyBlast && %sourceObject != %targetObject)					// -soph
//               %sourceObject.setDamageLevel(%sourceObject.getDamageLevel() - (%sourceObject.getDatablock().maxDamage * 0.2));	// -soph
               
           if(%sourceObject.getDatablock().isBlastech && %sourceObject.getState() !$= "Dead")
           {
               blastechRegisterKill(%sourceObject , %targetObject );	// adding victim info +soph
               
               if(!%sourceObject.client.isAIControlled())
               {
                    %energyPct = %sourceObject.getEnergyPct();
                    %graph = createColorGraph("|", 35, %energyPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mCeil(%sourceObject.getEnergyLevel()) SPC "KW"; // SPC %pct@"%";
                 
                    bottomPrint(%sourceObject.client, "BlasTech armor detected unit destruction, harvesting energy:" NL %graph, 3, 2);
               }
           }

           if(%sourceObject.isMageIon && %sourceObject.getState() !$= "Dead")
           {
               if( !%targetObject.client.isAIControlled() || getPlayerCount( 1 ) == 1 )						// if(!%targetObject.client.isAIControlled()) -soph
                    MageRegisterKill(%sourceObject);
               else
                    MageRegisterBotKill(%sourceObject);           
           }
           
//           if(%damageType == $DamageType::BellyTurret && %sourceObject.getType() & $TypeMasks::PlayerObjectType)		// -soph
//               %sourceObject.useEnergy(%sourceObject.getDatablock().maxEnergy * -0.1);					// -soph
      }          
      
      if(%targetObject.fake)
         %targetObject.sourceClient.setControlObject(%targetObject.sourceClient.player);

      if(%targetObject.getDatablock().isBattleAngel && !%targetObject.scriptKilled)
      {
         %targetObject.ignoreDED = true;
//         %targetObject.setMomentumVector(%momVec);
         %targetObject.setMomentumVector( %targetObject.getVelocity() );	// .setMomentumVector(vectorNormalize(%targetObject.getVelocity())); -soph
         Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);

         if(!%targetObject.BACritical)
         {
              loopDamageFX(%targetObject, 4, 19, 4, LinearFlareProjectile, ReaverCharge); //BAMiniExplodeCharge);
              schedule(getRandom(4000, 6000), %targetObject, "BASelfDestruct", %targetObject);
         }
         else
              schedule(32, %targetObject, "BASelfDestruct", %targetObject);
                       
         %targetObject.deathFiring = false;
         return;
      }
        //-Nite- removed Jetpack blowup
//      if(%targetObject.jetpackMounted && %targetObject.jp_heatcount > 0)
//      {
//         %targetObject.setMomentumVector(%momVec);
//         Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);
//         BASelfDestruct(%targetObject);
//         return;
//      }

      // where did this guy get it?
      %damLoc = %targetObject.getDamageLocation(%position);

//       should this guy be blown apart?
//      if($DamageGroup[%damageType] & $DamageGroupMask::Explosive)		// -[soph]
//      {
//         if( %previousDamage >= 0.1 )						// -[/soph] // only if <= 37 percent damage remaining

      %targetObject.gibValue = %targetObject.getDatablock().maxDamage * 2;	// +[soph]
      if( %damageType == $DamageType::Crash )					// +
         %gibFactor = 4.0 ;							// +		
      else if( $DamageGroup[ %damageType ] & $DamageGroupMask::Explosive )	// +
         %gibFactor = 2.25 ;							// +		
      else if( %damageType == $DamageType::Ground || %damageType == $DamageType::Impact )
         %gibFactor = 1.5 ;							// +
      else if( %damageType == $DamageType::PBW )				// +
         %gibFactor = 1.0 ;							// +	
      else if( $DamageGroup[ %damageType ] & $DamageGroupMask::Kinetic )	// +
         %gibFactor = 0.275 ;							// +
      else									// +
         %gibFactor = 0.175 ;							// +
										// +
      if( $DamageGroup[ %damageType ] & $DamageGroupMask::Plasma || $DamageGroup[ %damageType ] & $DamageGroupMask::Mitzi )
         %noGib = true ;							// +
      if( %amount * %gibFactor >= %targetObject.gibValue && !%noGib )		// +
      {										// +[/soph]
//         %targetObject.setMomentumVector(%momVec);
         %targetObject.setMomentumVector( %targetObject.getVelocity() ) ;	// vectorNormalize(%targetObject.getVelocity())); -soph
         %targetObject.schedule( 32 , blowup ) ;				// .blowup(); -soph
         %targetObject.blowedUp = true;
      }										// +[soph]
      else									// +
      {										// +
         %gibFactor *= %amount ;						// +
         %targetObject.gibValue -= %gibFactor * %gibFactor / ( 2 * %targetObject.gibValue ) ; 
										// +[/soph]
//      else if(%damageType == $DamageType::PBW)				// -[soph]
//      {									// -
//         if(getRandom(100) > 25) // 25% chance of not blowing up		// -
//         {									// -
//            %targetObject.setMomentumVector(vectorNormalize(%targetObject.getVelocity()));
//            %targetObject.blowup();						// -
//           // %targetObject.schedule(100, "blowup");				// -
//            %targetObject.blowedUp = true;					// -
//         }									// -
//         else									// -
//         {									// -
//             if(!%targetObject.blowedUp && getRandom(100) > 40)		// -
//                %targetObject.deathFiring = true;				// -
//         }									// -
//      }                                                     			// -
//         if( !%targetObject.blowedUp )					// -[/soph]
            if($DamageGroup[%damageType] & $DamageGroupMask::Plasma)		// else if($DamageGroup[%damageType] & $DamageGroupMask::Plasma) -soph
            {
//         %targetObject.setMomentumVector(%momVec);

//         if(!%targetObject.blowedUp)						// -soph
               %targetObject.burnObject(%sourceObject , 4.0 ) ;			// val +soph

//         if(getRandom(100) > 40)						// -soph
               %targetObject.deathFiring = true;
            }
            else if( %damageType == $DamageType::VehicleSpawn && !(%targetObject.pdead && $votePracticeMode))      // this should be funny...
            {
               %targetObject.setMomentumVector("0 0 1");
               %targetObject.blowup();
               %targetObject.blowedUp = true;
            }
            else if( %damageType == $DamageType::PPC ||				// +[soph]
                     %damageType == $DamageType::ShockCannon )			// +
            {									// +
               %targetObject.setCloaked( true ) ;				// +
               %targetObject.startFade( 1500 , 0 , true ) ;			// +
               %targetObject.zapObject() ;					// +
               %targetObject.blowedUp = true;					// +
            }        								// +[/soph]
            else
            {
//            if(!%targetObject.blowedUp && getRandom(100) > 40)		// -soph
               if( %targetObject.getImageTrigger( $WeaponSlot ) )		// +soph
                  %targetObject.deathFiring = true;
            }
      }

      // If we were killed, max out the flash
      %targetObject.setDamageFlash(0.75);

      if(%targetObject.blowedUp)
         %targetClient.die1stPerson = false;
      else
         %targetClient.die1stPerson = true;

//      %targetObject.ignoreRepairThis = true;
       if(%targetObject.DESetSet)
          %targetObject.DESetSet.remove(%targetObject);
//      if(!%targetObject.blowedUp) // fix for later
//      {
//         %obj.setImageTrigger(0, true);
//         schedule(5000, 0, "setImageTrigger", 0, false);
//      }

      %damLoc = %targetObject.getDamageLocation(%position);
      Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);
      trackerUpdateCheck( %targetObject ) ;	// fix for target corpse tagging +soph

   }
   else if ( %amount > 0.1 )
   {
      if( %targetObject.station $= "" && %targetObject.isCloaked() )
      {
           deCloak( %targetObj );		// +soph
//         %targetObject.setCloaked( false );	// -[soph]
//         if( %targetObject.getMountedImage($BackpackSlot).item $= "CloakingPack" && %targetObject.getImageState($BackpackSlot) $= "activate" )	// plugging cloak exploit -soph
						// -[/soph]
            %targetObject.reCloak = %targetObject.schedule( 500, "setCloaked", true );
      }

      playPain( %targetObject );
   }
}

function trackerUpdateCheck( %obj )		// +[soph]
{						// + hack fix for persistant corpse tagging 
   if( isObject( %obj ) )			// +
   {						// +
      %obj.team = 0 ;				// +
      %obj.setPassiveJammed( true ) ;		// +
      %obj.setTarget( -1 ) ;			// +
      if( !%obj.clearTracker )			// +
         %obj.clearTracker = schedule( $CorpseTimeoutValue , 0 , clearTracker , %obj ) ;
   }						// +
}						// +
						// +
function clearTracker( %obj )			// +
{						// +
   if( isObject( %obj ) && %obj.dead )		// +
   {						// +
      cancel( %obj.clearTracker ) ;		// +
      %obj.clearTracker = 0 ;			// +
						// +
      if( %obj.client.player == %obj )		// +
         %obj.client.player = 0 ;		// +
      %obj.team = 0 ;				// +
      %obj.client = 0 ;				// +
      %obj.setTarget( -1 ) ;			// +
   }						// +
}						// +
						// +
function soulShieldFade( %this )		// +
{						// +
   %this.invincible = false;			// +
}						// +
						// +
function deathFiringLoop( %player , %timeElapsed )
{						// +
	if( !isObject( %player ) )		// +
		return ;			// +
	%bool = %player.getImageTrigger( 0 ) ? false : true ;
	if( %player.blowedUp )			// +
	{					// +
		%player.setImageTrigger( 0 , false ) ;
		%player.deathFiring = false ;	// +
		return ; 			// +
	}					// +
						// +
	%time = %bool ? getRandom( 50 , 150 ) : getRandom( 10 , 50 ) ;
	%time *= 10 ;				// +
	%timeElapsed += %time ;			// +
	if( %timeElapsed < 5000 )		// +
	{					// +
		%player.setImageTrigger( 0 , %bool ) ;
		schedule( %time , 0 , deathFiringLoop , %player , %timeElapsed ) ;
	}					// +
	else					// +
	{					// +
		%player.setImageTrigger( 0 , false ) ;
		%player.deathFiring = false ;	// +
	}					// +
}						// +[/soph]

function Armor::onDisabled(%this,%obj,%state)
{
     %obj.isAlive = false;
     
     if(isObject(%obj.tetherBeacon))
          %obj.tetherBeacon.delete();

     if(isObject(%obj.t))
          %obj.t.delete();

   if(%obj.blowedUp)					// if(%obj.isFlaming && %obj.blowedUp) -soph
   {
      if( %obj.isFlaming )				// +soph
      {
         %obj.isFlaming = false;
         %obj.burntime = 1;
//      %obj.deleteSlotExtension(Meltdown.APE_mountPoint);
      }
      trackerUpdateCheck( %obj ) ;
      cancel( %obj.clearTracker ) ;
      schedule( 500 , 0 , clearTracker , %obj ) ;	
      %obj.schedule($CorpseTimeoutValue, "delete");	// +[soph]
   }							// +
   else							// +
   {							// +
      if( !$corpseTrackerCount )			// +
         $corpseTrackerCount = 0 ;			// +
      $corpseTrackerList[ $corpseTrackerCount ] = %obj ;
      $corpseTrackerCount++ ;				// +
      %time = getSimTime() ;				// +
      if( %time > $corpseTrackerLoop + ( 1.5 * $corpseTrackerFrequency ) )
         corpseTrackerLoop() ;				// +
      else if( %time < $corpseTrackerLoop )		// +
         $corpseTrackerLoop = %time ;			// +
   }							// +[/soph]

//   %fadeTime = 1000;					// -[soph]
//   %obj.startFade( %fadeTime, ($CorpseTimeoutValue) - %fadeTime, true );
//   %obj.schedule($CorpseTimeoutValue, "delete");	// -[/soph]
}

function corpseTrackerLoop()				// +[soph]
{							// + CorpseTracker main update loop
   if( !$corpseTrackerLoop  )				// +
   {							// +
      if( !$corpseTrackerLimit )			// + system defaults
         $corpseTrackerLimit = 45 ;			// +
      if( !$corpseTrackerPrunePercent )			// +
         $corpseTrackerPrunePercent = 3 ;		// +
      if( !$corpseTrackerFrequency )			// +
         $corpseTrackerFrequency = 30 * 1000 ;		// +
   }							// +
   $corpseTrackerLoop = getSimTime() ;			// +
							// +
   %j = 0 ;						// +
   for( %i = 0 ; %i < $corpseTrackerCount ; %i++ )	// + pruner
   {							// +
      %int = $corpseTrackerList[ %i ] ;			// +
      if( isObject( %int ) && %int.dead )		// +
         if( !%int.blowedUp )				// +
         {						// +
            $corpseTrackerList[ %j ] = %int ;		// +
            %j++ ;					// +
         }						// +
         else						// +
         {						// +
            if( %int.clearTracker )			// +
            {						// +
               $corpseTrackerList[ %j ] = %int ;	// +
               %j++ ;					// +
            }						// +
            else 					// +
               schedule( %i * 32 , 0 , corpseClear , %int ) ;
         }						// +
   }							// +
   $corpseTrackerCount = %j ;				// +
							// +
   							// +
   if( $corpseTrackerCount  > $corpseTrackerLimit )	// +
   {							// +
      for( %i = 0 ; %i < ( $corpseTrackerCount * $corpseTrackerPrunePercent / 100 ) ; %i++ )
      {							// +
         if( $corpseTrackerList[ %i ].clearTracker )	// +
            clearTracker( $corpseTrackerList[ %i ] ) ;	// +
         schedule( getRandom() * $CorpseTimeoutValue , 0 , corpseCleanup , $corpseTrackerList[ %i ] );
      }							// +
echo( "CorpseTracker: limit exceeded, cleaning up" SPC %i SPC "oldest bodies" ) ;
   }							// +
							// +
   if( $corpseTrackerCount > 0 )			// +
      schedule( $corpseTrackerFrequency , 0 , corpseTrackerLoop ) ;
}							// +
							// +
function corpseCleanup( %corpse )			// +
{							// +
   if( %corpse.clearTracker )				// +
      cancel( %corpse.clearTracker ) ;			// +
   if( isObject( %corpse ) )				// +
   {							// +
      %corpse.team = 0 ;				// +
      clearTracker( %corpse ) ;				// +
      %corpse.startFade( 2500 , 0 , true ) ;		// +
      %corpse.invincible = true ;			// +
      schedule( 2500 , 0 , corpseClear , %corpse ) ;	// +
   }							// +
}							// +
							// +
function corpseClear( %corpse )				// +
{							// +
   if( isObject( %corpse ) )				// +
      %corpse.delete();					// +
}							// +[/soph]

function Armor::onImpact(%data, %playerObject, %collidedObject, %vec, %vecLen)
{
//   echo("::onImpact" SPC %playerObject SPC %collidedObject SPC %vec SPC %vecLen);
//   
//   if(%playerObject.isMech)
//   {
//        %data.damageObject(%playerObject, 0, VectorAdd(%playerObject.getPosition(),%vec), %vecLen * %data.speedDamageScale * 0.333, $DamageType::Ground);
//        %collidedObject.getDatablock().damageObject(%playerObject, 0, VectorAdd(%playerObject.getPosition(),%vec), %vecLen, $DamageType::Crash);           
//   }
//   else
        %data.damageObject(%playerObject, 0, VectorAdd(%playerObject.getPosition(),%vec), %vecLen * %data.speedDamageScale, $DamageType::Ground);
}

function Armor::onCollision(%this,%obj,%col,%forceVehicleNode, %forceLoot)
{
   if (%obj.getState() $= "Dead")
      return;

//   echo("::onCollision" SPC %obj SPC %col);
   
   %dataBlock = %col.getDataBlock();
   %className = %dataBlock.className;
   %client = %obj.client;
   // player collided with a vehicle
   %node = -1;
   if (%forceVehicleNode !$= "" || (%className $= WheeledVehicleData || %className $= FlyingVehicleData || %className $= HoverVehicleData) &&
         %obj.mountVehicle && %obj.getState() $= "Move" && %col.mountable && !%obj.inStation && %col.getDamageState() !$= "Destroyed") {

      //if the player is an AI, he should snap to the mount points in node order,
      //to ensure they mount the turret before the passenger seat, regardless of where they collide...
      if (%obj.client.isAIControlled())
      {
         %transform = %col.getTransform();   

         //either the AI is *required* to pilot, or they'll pick the first available passenger seat
         if (%client.pilotVehicle)
         {
            //make sure the bot is in light armor
            if (%client.player.getArmorSize() $= "Light")
            {
               //make sure the pilot seat is empty
               if(!%col.getMountNodeObject(0))
                  %node = 0;
            }
         }
         else
            %node = findAIEmptySeat(%col, %obj);
      }
      else
         %node = findEmptySeat(%col, %obj, %forceVehicleNode);

//     echo("findEmptySeat ->" SPC %node);
//      %cantPilot = %this.armorMask & $ArmorMask::Blastech | $ArmorMask::MagIon;

      //now mount the player in the vehicle
//      if(!%cantPilot && %node >= 0)
      if(%node >= 0)
      {
         // players can't be pilots, bombardiers or turreteers if they have
         // "large" packs -- stations, turrets, turret barrels
         if(hasLargePack(%obj)) {
            // check to see if attempting to enter a "sitting" node
            if(nodeIsSitting(%datablock, %node)) {
               // send the player a message -- can't sit here with large pack
               if(!%obj.noSitMessage)
               {
                  %obj.noSitMessage = true;
                  %obj.schedule(2000, "resetSitMessage");
                  messageClient(%obj.client, 'MsgCantSitHere', '\c2Pack too large, can\'t occupy this seat.~wfx/misc/misc.error.wav');
               }
               return;
            }
         }
         if(%col.noEnemyControl && %obj.team != %col.team)
            return;

	    // DarkDragonDX: Can't Pilot anything if you're "dead"
	   if (%node == 0 && $votePracticeModeOn && %obj.pdead)
	   {
		if (%obj.noSitMessage)
			return;

		%obj.noSitMessage = true;
                %obj.schedule(2000, "resetSitMessage");
		messageClient(%obj.client, 'MsgCantSitHere', "\c2You are out and therefore cannot pilot vehicles. Go back to home base. ~wfx/misc/misc.error.wav");
		return;
	   }
            
         commandToClient(%obj.client,'SetDefaultVehicleKeys', true);
         //If pilot or passenger then bind a few extra keys
         if(%node == 0)
            commandToClient(%obj.client,'SetPilotVehicleKeys', true);
         else
            commandToClient(%obj.client,'SetPassengerVehicleKeys', true);

         if(!%obj.inStation)
            %col.lastWeapon = ( %col.getMountedImage($WeaponSlot) == 0 ) ? "" : %col.getMountedImage($WeaponSlot).getName().item;
         else
            %col.lastWeapon = %obj.lastWeapon;

         if(%node == 0 && %classname $= FlyingVehicleData )	// if(%node == 0)-soph
              ejectFlag(%obj);	              

         %col.mountObject(%obj,%node);
         %col.playAudio(0, MountVehicleSound);
         %obj.mVehicle = %col;

			// if player is repairing something, stop it
			if(%obj.repairing)
				stopRepairing(%obj);

         //this will setup the huds as well...
         %dataBlock.playerMounted(%col,%obj, %node);
      }
   }
   else if (%className $= "Armor" || (%className $= "pack" && %forceLoot)) {
      // player has collided with another player
      if(%col.getClassName() $= "Item" || (%col.getState() $= "Dead" && !%col.blowedUp)) {	// if(%col.getState() $= "Dead") { -soph
	  // DarkDragonDX: Don't loot if you're "Dead"
	 if (%obj.pdead && $votePracticeModeOn)
		return;

         %gotSomething = false;
         // it's corpse-looting time!
         // weapons -- don't pick up more than you are allowed to carry!
	 if (%col.getClassName() !$= "Item")
		 for(%i = 0; ( %obj.weaponCount < %obj.getDatablock().maxWeapons ) && $InvWeapon[%i] !$= ""; %i++) 
		 {
		    %weap = $NameToInv[$InvWeapon[%i]];
		    if ( %col.hasInventory( %weap ) )
		    {
		       if ( %obj.incInventory(%weap, 1) > 0 )
		       {
		          %col.decInventory(%weap, 1);
		          %gotSomething = true;
		          messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %weap.pickUpName);
		       }
		    }
		 }
         // targeting laser:
         if ( %col.getClassName() !$= "Item" && %col.hasInventory( "TargetingLaser" ) )
         {
            if ( %obj.incInventory( "TargetingLaser", 1 ) > 0 )
            {
               %col.decInventory( "TargetingLaser", 1 );
               %gotSomething = true;
               messageClient( %obj.client, 'MsgItemPickup', '\c0You picked up a targeting laser.' );
            }
         }
         // ammo
         for(%j = 0; $ammoType[%j] !$= ""; %j++)
         {
            %ammoAmt = %col.inv[$ammoType[%j]];
            if(%ammoAmt)
            {
               // incInventory returns the amount of stuff successfully grabbed
               %grabAmt = %obj.incInventory($ammoType[%j], %ammoAmt);
               if ( %this.isEngineer )						// +[soph]
               {								// + engineer exception
                    %packmax = AmmoPack.max[ $ammoType[ %j ] ] ;		// + this overwrites the one in scripts/player.cs?
                    if( %packmax < 20 )						// +
                       %packmax = 2 ;						// +
                    else if( %packmax < 50 )					// +
                       %packmax = 10 ;						// +
                    else if( %packmax < 100 )					// +
                       %packmax = 25 ;						// +
                    else							// +
                       %packmax = 50 ;						// +
                    %max = %this.max[ $ammoType[ %j ] ] + %packmax ;		// +
                    if ( %obj.getInventory( $ammoType[ %j ] ) > %max )		// +
                    {								// +
                         %grabAmt -= ( %obj.getInventory( $ammoType[ %j ] ) - %max ) ;
                         %obj.setInventory( $ammoType[ %j ] , %max ) ;		// +
                    }								// +
               }								// +[/soph]
               if(%grabAmt > 0)
               {
                  %col.decInventory($ammoType[%j], %grabAmt);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', $ammoType[%j].pickUpName);
                  %obj.client.setWeaponsHudAmmo($ammoType[%j], %obj.getInventory($ammoType[%j]));
               }
            }
         }
         if( %obj.srm4active && %col.inv[ MissileLauncherAmmo ] > 0 )		// +[soph]
         {									// + ammo conversions for packguns
            %amt = %obj.incInventory( SRM4Ammo , %col.inv[ MissileLauncherAmmo ] ) ;
            if( %amt > 0 )							// + for packguns that use similar ammo to primaries
            {									// +
               %col.decInventory( MissileLauncherAmmo , %amt ) ;		// +
               messageClient( %obj.client , 'MsgItemPickupConvert' , '\c0You converted %1 into %2.' , MissileLauncherAmmo.pickUpName , SRM4Ammo.pickUpName ) ;
               %gotSomething = true ;						// +
            }									// +
         }									// +
         %backImage = %obj.getMountedImage( $BackpackSlot ) ;			// +
         if( %backImage.item $= "AutoCCannonPack" && %col.inv[ AutoCannonAmmo ] > 0 )
         {									// +
            %amt = %obj.incInventory( ACPackAmmo , %col.inv[ AutoCannonAmmo ] ) ;
            if( %amt > 0 )							// +
            {									// +
               %col.decInventory( AutoCannonAmmo , %amt ) ;			// +
               messageClient( %obj.client , 'MsgItemPickupConvert' , '\c0You converted %1 into %2.' , AutoCannonAmmo.pickUpName , ACPackAmmo.pickUpName ) ;
               %gotSomething = true ;						// +
            }									// +
         }									// +
         if( %backImage.item $= "DiscCannonPack" && %col.inv[ DiscAmmo ] > 0 )	// +
         {									// +
            %amt = %obj.incInventory( DiscPackAmmo, %col.inv[ DiscAmmo ] ) ;	// +
            if( %amt > 0 )							// +
            {									// +
               %col.decInventory( DiscAmmo , %amt ) ;				// +
               messageClient( %obj.client , 'MsgItemPickupConvert' , '\c0You converted %1 into %2.' , DiscAmmo.pickUpName , DiscPackAmmo.pickUpName ) ;
               %gotSomething = true ;						// +
            }									// +
         }									// +[/soph]

         // figure out what type, if any, grenades the (live) player has
         %playerGrenType = "None";
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++) {
            %gren = $NameToInv[$InvGrenade[%x]];
            %playerGrenAmt = %obj.inv[%gren];
            if(%playerGrenAmt > 0)
            {
               %playerGrenType = %gren;
               break;
            }
         }
         // grenades
         for(%k = 0; $InvGrenade[%k] !$= ""; %k++)
         {
            %gren = $NameToInv[$InvGrenade[%k]];
            %corpseGrenAmt = %col.inv[%gren];
            // does the corpse hold any of this grenade type?
            if(%corpseGrenAmt)
            {
               // can the player pick up this grenade type?
               if((%playerGrenType $= "None") || (%playerGrenType $= %gren))
               {
                  %taken = %obj.incInventory(%gren, %corpseGrenAmt);
                  if(%taken > 0)
                  {
                     %col.decInventory(%gren, %taken);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %gren.pickUpName);
                     %obj.client.setInventoryHudAmount(%gren, %obj.getInventory(%gren));
                  }
               }
               break;
            }
         }

         // figure out what type, if any, mines the (live) player has
         if( !%obj.getArmorType() !$= $ArmorMask::BattleAngel | $ArmorMask::MagIon )	// +soph
         {										// i guess i should do this now
            %playerMineType = "None";
            for(%y = 0; $InvMine[%y] !$= ""; %y++)
            {
               %mType = $NameToInv[$InvMine[%y]];
               %playerMineAmt = %obj.inv[%mType];
               if(%playerMineAmt > 0)
               {
                  %playerMineType = %mType;
                  break;
               }
            }
            // mines
            for(%l = 0; $InvMine[%l] !$= ""; %l++)
            {
               %mine = $NameToInv[$InvMine[%l]];
               %mineAmt = %col.inv[%mine];
               if(%mineAmt) {
                  if((%playerMineType $= "None") || (%playerMineType $= %mine))
                  {
                     %grabbed = %obj.incInventory(%mine, %mineAmt);
                     if(%grabbed > 0)
                     {
                        %col.decInventory(%mine, %grabbed);
                        %gotSomething = true;
                        messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %mine.pickUpName);
                        %obj.client.setInventoryHudAmount(%mine, %obj.getInventory(%mine));
                     }
                  }
                  break;
               }
            }
         }
         // beacons
         %beacAmt = %col.inv[Beacon];
         if(%beacAmt)
         {
            %bTaken = %obj.incInventory(Beacon, %beacAmt);
            if(%bTaken > 0)
            {
               %col.decInventory(Beacon, %bTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', Beacon.pickUpName);
               %obj.client.setInventoryHudAmount(Beacon, %obj.getInventory(Beacon));
            }
         }
         // repair kit
         %rkAmt = %col.inv[RepairKit];
         if(%rkAmt)
         {
            %rkTaken = %obj.incInventory(RepairKit, %rkAmt);
            if(%rkTaken > 0)
            {
               %col.decInventory(RepairKit, %rkTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', RepairKit.pickUpName);
               %obj.client.setInventoryHudAmount(RepairKit, %obj.getInventory(RepairKit));
            }
         }
      }
      if(%gotSomething)
      {
         %val = %obj.getInventory( %obj.getMountedImage( $BackpackSlot ).ammo );	// +soph
         if( %val && !%obj.client.isAIControlled() )					// +soph
            %obj.client.updateSensorPackText( %val );					// .updateSensorPackText( %obj.getInventory( %obj.getMountedImage( $BackpackSlot ).ammo ) ); -soph
         %col.playAudio(0, CorpseLootingSound);
      }
   }
}

// keen - more streamlined version
function Player::getArmorSize(%this)
{
   %data = %this.getDataBlock(); //.getName();
   
   if(%data.armorMask & $ArmorMask::Light)
     return "Light";
   else if(%data.armorMask & $ArmorMask::Medium)
      return "Medium";
   else if(%data.armorMask & $ArmorMask::Heavy)
      return "Heavy";
   else if(%data.armorMask & $ArmorMask::MagIon)
      return "MagIon";
   else if(%data.armorMask & $ArmorMask::Blastech)
      return "Blastech";
   else if(%data.armorMask & $ArmorMask::Engineer)
      return "Engineer";
   else if(%data.armorMask & $ArmorMask::BattleAngel)
      return "BattleAngel";
   else if(%data.armorMask & $ArmorMask::Daishi)
      return "Daishi";
   else if(%data.armorMask & $ArmorMask::TacticalMech)
      return "TacticalMech";
   else
      return "Unknown";
}

function Player::getArmorType(%this)
{
   return %this.getDataBlock().armorMask;
}

function Player::old_getArmorSize(%this)
{
   // return size as "Light","Medium", "Heavy"
   %dataBlock = %this.getDataBlock().getName();
   if (getSubStr(%dataBlock, 0, 5) $= "Light")
      return "Light";
   else if (getSubStr(%dataBlock, 0, 6) $= "Medium")
      return "Medium";
   else if (getSubStr(%dataBlock, 0, 5) $= "Heavy")
      return "Heavy";
   else if (getSubStr(%dataBlock, 0, 6) $= "MagIon")
      return "MagIon";
   else if (getSubStr(%dataBlock, 0, 8) $= "Blastech")
      return "Blastech";
   else if (getSubStr(%dataBlock, 0, 8) $= "Engineer")
      return "Engineer";
   else if (getSubStr(%dataBlock, 0, 11) $= "BattleAngel")
      return "BattleAngel";
   else if (getSubStr(%dataBlock, 0, 6) $= "Daishi")
      return "Daishi";
   else if (%dataBlock $= "AssaultEAXMech")
      return "TacticalMech";
   else
      return "Unknown";
}

function Player::use(%this, %data)
{
   if(%this.isTacticalMech)
   {
       %this.getDatablock().processUse(%this, %data);
       return;
   }

   // If player is in a station then he can't use any items
   if(%this.station !$= "")
      return false;

   if(%this.getDatablock().armorMask == $ArmorMask::MagIon && %data $= "TargetingLaser")
   {
       IonCSLSelect(%this, 0);
       return;
   }

   // Convert the word "Backpack" to whatever is in the backpack slot.
   if ( %data $= "Backpack" )
   {
      if ( %this.inStation )
         return false;

      if(%this.isPilot())
      {
            vehicleModuleTrigger(%this, true );		// false); -soph
            return;
      }
      else if(%this.getObjectMount() && %this.isWeaponOperator())
      {
            vehicleModuleTrigger(%this, false );	// true); -soph
            return;
      }

      if (%this.isWeaponOperator())
      {
            messageClient( %this.client, 'MsgCantUsePack', '\c2You can\'t use your pack while in a weaponry position.~wfx/misc/misc.error.wav' );
            return false;
      }

      %image = %this.getMountedImage( $BackpackSlot );
      if ( %image )
         %data = %image.item;
   }

   // Can't use some items when piloting or your a weapon operator
   if ( %this.isPilot() || %this.isWeaponOperator() )
      if ( %data !$= "RepairKit" )
         return;

   return ShapeBase::use( %this, %data );
}

function Player::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.lDamageSchedule !$= "")	// +[soph]
   {					// making sure there aren't other active loops
      cancel(%obj.lDamageSchedule);
      %obj.lDamageSchedule = "";
   }
   if( !%obj.isWet )
   {
      %damageAmount *= 0.95;
      if( %damageAmount < 0.001 )
         return;
   }					// +[/soph]
   if(!%obj.heatShielded)   //haha!
   {
      if(%obj.getState() !$= "Dead")
      {
         %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
         %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
      }
      else
         %obj.lDamageSchedule = "";
   }
   else
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
}

function Armor::onEnterLiquid(%data, %obj, %coverage, %type)
{
   %obj.isWet = true;
   
   %shouldBounce = false;
   switch(%type)
   {
      case 0:
          enterWaterClearIncen(%obj);
      case 1:
          enterWaterClearIncen(%obj);
      case 2:
          enterWaterClearIncen(%obj);
      case 3:
          enterWaterClearIncen(%obj);
      // DarkDragonDX: Dead Players shouldn't get stuck in Lava
      case 4:
         //Lava
         %obj.liquidDamage(%data, $DamageLava, $DamageType::Lava);
	 if (%obj.pdead && $votePracticeModeOn)
		%shouldBounce = true;
      case 5:
         //Hot Lava
         %obj.liquidDamage(%data, $DamageHotLava, $DamageType::Lava);
	 if (%obj.pdead && $votePracticeModeOn)
		%shouldBounce = true;
      case 6:
         //Crusty Lava
         %obj.liquidDamage(%data, $DamageCrustyLava, $DamageType::Lava);
	 if (%obj.pdead && $votePracticeModeOn)
		%shouldBounce = true;
      case 7:
         //Quick Sand
   }

   if (%shouldBounce)
   {
	%position = %obj.getPosition();
	%liquid = containerraycast(vectorAdd(%position, "0 0 2000"),vectorAdd(%position, "0 0 -9999"), $TypeMasks::WaterObjectType, 0);
	%surface_position = getWords(%liquid, 1, 3);
	%liquid = getword(%liquid, 0);
	%viscosity = %liquid.viscosity;

	%depth = vectorDist(%position, %surface_position);
	%velocity = %depth * 3 * %viscosity;

	%verticalVelocity = getWord(%obj.getVelocity(), 2);
	if (%verticalVelocity < 0)
		%velocity -= %verticalVelocity;

	// DarkDragonDX: Shieldless shrike speed, fastest thing in the game
	if (%velocity > 223.889)
	{
		%velocity = 223.889;
		%obj.setPosition(vectorAdd(%surface_position, "0 0 30"));
	}

	%obj.setVelocity(vectorAdd(getWords(%obj.getVelocity(), 0, 1), "0 0 " @ %velocity));
   }
}

function Armor::onLeaveLiquid(%data, %obj, %type)
{
   %obj.isWet = false;

   switch(%type)
   {
      case 0:
         //Water
      case 1:
         //Ocean Water
      case 2:
         //River Water
      case 3:
         //Stagnant Water
      case 4:
         //Lava
      case 5:
         //Hot Lava
      case 6:
         //Crusty Lava
      case 7:
         //Quick Sand
   }

   if(%obj.lDamageSchedule !$= "")
   {
      cancel(%obj.lDamageSchedule);
      %obj.lDamageSchedule = "";
   }
}

function Armor::onTrigger(%data, %player, %triggerNum, %val)
{
   
   if (%triggerNum == 4)
   {
      // Throw grenade
      if (%val == 1)
      {
         %player.grenTimer = 1;
      }
      else
      {
         if (%player.grenTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            %player.use(Grenade);
            %player.grenTimer = 0;
         }
      }
   }
   else if (%triggerNum == 5)
   {
      if(%player.activeModule && %val)
      {
          call(%player.activeModuleScript, %data, %player);
          return;
      }

      %veh = %player.getObjectMount();
      if(%veh && %veh.getType() & $TypeMasks::VehicleObjectType)
      {
          %veh.getDatablock().handleMineOption(%veh, %player, findVehiclePlayerSlot(%veh, %player), %val);
          return;
      }

      if( %player.isMageIon )		// +[soph]
      {					// + magions now get infinite EMP mines
         if (%val == 1)			// +
            %player.mineTimer = 1;	// +
         else				// +
         {				// +
            if (%player.mineTimer == 0)	// +
            { }				// +
            else			// +
            {				// +
               %nrg = %player.getEnergyLevel() ;
               %cost = %obj.efficiencyMod ? 24 : 32 ;
               if( %nrg > %cost )	// + though they cost energy
               {			// +
                  %player.inv[ EMPMine ] = 1 ;
                  %player.use( EMPMine ) ;
                  %player.setEnergyLevel( %nrg - %cost );
               }			// +
            }				// +
         }				// +
      }					// +
      else				// +[/soph]

      // Throw mine
      if (%val == 1)
         %player.mineTimer = 1;
      else
      {
         if (%player.mineTimer == 0)
         {
            // Bad throw for some reason
         }
         else
         {
            if(%player.getInventory(LargeMine))
               %player.use(LargeMine);
            else if(%player.getInventory(CloakMine))
               %player.use(CloakMine);
            else if(%player.getInventory(EMPMine))
               %player.use(EMPMine);
            else if(%player.getInventory(IncendiaryMine))
               %player.use(IncendiaryMine);
            else if(%player.getInventory(BlastechMine))
            {
               %player.inv[ Mine ] = 1 ;
               %player.use( Mine ) ;
               %player.inv[ BlastechMine ]-- ;
            }
            else
               %player.use(Mine);

            %player.mineTimer = 0;
         }
      }
   }
   else if (%triggerNum == 2)
   {
      if(%val == 1)
         %player.isJumping = true;
      else
         %player.isJumping = false;

      if(%data.isDaishi && %val)
      {
         daishiHandleDeploy(%data, %player);
         return;
      }
   }
   else if (%triggerNum == 3)
   {
      // val = 1 when jet key (LMB) first pressed down
      // val = 0 when jet key released
      // MES - do we need this at all any more?
      if(%val == 1)
         %player.isJetting = true;
      else
         %player.isJetting = false;
   }
}

function nearbyGen(%pl)
{
   if(isObject(%pl))
   {
     InitContainerRadiusSearch(%pl.getWorldBoxCenter(), 32, $TypeMasks::GeneratorObjectType);

     while((%int = ContainerSearchNext()) != 0)
     {
          if(%int && %int.getDamageState() $= "Enabled")
             return true;
          else continue;
     }
     return false;
   }
}

function nearbyBGen(%pl)// DOH!!!!!!!!!!!!!!!!!!!!1111
{
   if(isObject(%pl))
   {
      InitContainerRadiusSearch(%pl.getWorldBoxCenter(), 16, $TypeMasks::StaticShapeObjectType);

      while((%int = ContainerSearchNext()) != 0)
      {
         if(%int.getDatablock().depGenTime == 12 && %int.getDamageState() $= "Enabled")
            return true;
      }
      return false;
   }
}

function BlastechRegisterKill(%obj, %victim)	// added victim +soph
{
     if(!%obj.client.isAIControlled())
     {
          activateDeploySensorGrn(%obj);
          schedule(1000, %obj, deactivateDeploySensor, %obj);
     }
     
     zapEffect(%obj, "HealGreen");
    // %obj.setEnergyLevel(%obj.getEnergyLevel() + (%obj.getDatablock().maxEnergy * 0.05));	// -[soph]
    // %obj.setDamageLevel(%obj.getDamageLevel() - ( %obj.getDatablock().maxDamage * 0.1 ));	// -[/soph]
     if( %victim.getDatablock().isBlastech )							// +[soph]
          %obj.setEnergyLevel( %obj.getEnergyLevel() + ( %victim.getDatablock().maxEnergy / 6 ) );
     else											// energy bonus based on victim
          %obj.setEnergyLevel( %obj.getEnergyLevel() + ( %victim.getDatablock().maxEnergy * 2.5 ) );
     %obj.setDamageLevel(%obj.getDamageLevel() - ( %victim.getDatablock().maxDamage * 0.1 ));	// same for health +[/soph]

}

function BlastechRecharge(%obj)
{
   if(isObject(%obj) && %obj.getDatablock().isBlastech)
   {
      if(%obj.bt_binswitch $= "")
         %obj.bt_binswitch = true;

      if(%obj.getState() !$= "Dead")
      {
         // For some reason, T2 does strange things with null values ("") so this is necessary:
         %flag = %obj.getMountedImage($FlagSlot);
         %flagMounted = isObject(%flag) ? (%flag.noSpecialRegen == true ? true : false) : false;
         
         InitContainerRadiusSearch( %obj.getWorldBoxCenter() , 45 , $TypeMasks::GeneratorObjectType ) ;
         %power = 0 ;
         while( ( %int = ContainerSearchNext() ) != 0 )
            if( %int && %int.getDamageState() $= "Enabled" )
            {
               %distance = containerSearchCurrRadDamageDist() ;
               if( %distance < 45 )
                  %power += 45 - %distance ;
            }

         if( %power )
         {
            if( %power > 45 )
               %power = 45 ;
            %obj.setEnergyLevel( %obj.getEnergyLevel() + ( %power / 2 ) ) ;
            %obj.playShieldEffect("0 0 1");

            if(!%obj.client.isAIControlled() && !%obj.inStation)
            {
                 %energyPct = %obj.getEnergyPct();
                 %pct = mFloor(%energyPct * 100);
                 %graph = createColorGraph("|", 35, %energyPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mCeil(%obj.getEnergyLevel()) SPC "KW"; // SPC %pct@"%";
                 %text = %pct == 100 ? %graph SPC "[Full]" : %graph SPC "[Charging]";
     
                 bottomPrint(%obj.client, "Blastech Capacitor Status" NL %text, 3, 2);
            }
            
            schedule(100, 0, "BlastechRecharge", %obj);
         }
         else
         {
            if(!%obj.client.isAIControlled())
            {
                 if(%obj.getEnergyLevel() <= 2)
                 {
                    %obj.bt_binswitch = %obj.bt_binswitch == true ? false : true;
                    %color = %obj.bt_binswitch == true ? "<color:FFFF22>" : "<color:FF2222>";
                    bottomPrint(%obj.client, %color@"ERROR! <color:FFFFFF>BlasTech protection<color:42dbea> has failed! Not enough energy. Get to a\ngenerator or you can use your Targeting Laser to drain energy from objects.", 5, 2);
                 }
                 else if(%obj.getEnergyPct() < 0.1)
                 {
                    %energyPct = %obj.getEnergyPct();
                    %pct = mFloor(%energyPct * 100);
                    %graph = createColorGraph("|", 35, %energyPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mCeil(%obj.getEnergyLevel()) SPC "KW"; // SPC %pct@"%";
                    %text = %graph SPC "[Discharging]";
                             
     //               %pct = mFloor(%obj.getEnergyPct() * 100);
                    bottomPrint(%obj.client, "Warning! Low energy! Get to a power source soon to recharge." NL %text, 5, 2);
                 }
            }
            else
            {
               %takingFire = ( %targetObject.lastDamageTime + 10000 < getSimTime() ) ? 1 : 0 ;
               if( !%takingFire && !%obj.botBTRegenBonus && %obj.getEnergyPct() < 0.1 )
               {
                  %obj.setRechargeRate( %obj.getRechargeRate() + 0.1 ) ;
                  %obj.botBTRegenBonus = true ;
               }
               else if( %obj.botBTRegenBonus && ( %obj.getEnergyPct() > 0.9 || %takingFire ) )
               {
                  %obj.setRechargeRate( %obj.getRechargeRate() - 0.1 ) ;
                  %obj.botBTRegenBonus = false;
               }
            }
            
            schedule(250, 0, "BlastechRecharge", %obj);
         }
      }
   }
}

$MAGMD3StartSpeed = 4.0;
$MAGMD3MaxSpeed = 60.0;
$MAGMD3AccelSpeed = 1.333333;
$MAGMD3TurboBoost = 20.0;

function MagIonMD3JetTest(%obj, %data)
{
   if(%obj.miState)
      %obj.miState = false;
   else
   {
      %obj.miState = true;
      %obj.MAGMD3MaxSpeed = $MAGMD3MaxSpeed;
      magIonMD3JetLoop(%obj);
   }
}

function magIonMD3JetLoop(%obj)
{
   if(isObject(%obj))
   {
      if(%obj.getEnergyLevel() > 3)
      {
         if(%obj.miState)
         {
            %vel = %obj.getVelocity();
            %sVel = velToSingle(%vel, true);
            %vec = vectorNormalize(%obj.getMuzzleVector(0));

            %turbolight = %obj.isJetting ? "On" : "Off";

            if(%sVel < $MAGMD3StartSpeed)
               %sVel = $MAGMD3StartSpeed;

            if(%obj.isJetting)
               %obj.MAGMD3MaxSpeed += $MAGMD3MaxSpeed;
            else
               %obj.MAGMD3MaxSpeed = $MAGMD3MaxSpeed;

            %pos = %obj.getPosition();
            %scVec = "0 0 -10000";
            %end = VectorAdd(%pos, %scVec);
            %searchResult = containerRayCast(%pos, %end, $TypeMasks::DefaultLOSType, %obj);
            %raycastPt = posFrRaycast(%searchResult);
            %scanz = getWord(%raycastPt, 2);
            %objz = getWord(%pos, 2);
            %height = %objz - %scanz;

            if(%height < 10 || %height > 200)
            {
//               echo("height breach" SPC %height);
               %vec = getWord(%vec, 0) SPC getWord(%vec, 1) SPC 0;

               if(%height < 10)
                  %obj.setPosition(getWord(%pos, 0) SPC getWord(%pos, 1) SPC (getWord(%raycastPt, 2) + 5));
               else
                  %obj.setPosition(getWord(%pos, 0) SPC getWord(%pos, 1) SPC (getWord(%raycastPt, 2) + 200));
            }

            if(%sVel < %obj.MAGMD3MaxSpeed)
               %obj.setVelocity(vectorScale(%vec, (%sVel+$MAGMD3AccelSpeed)));
            else if(%sVel > %obj.MAGMD3MaxSpeed)
               %obj.setVelocity(vectorScale(%vec, (%sVel-$MAGMD3AccelSpeed)));
            else
               %obj.setVelocity(vectorScale(%vec, $MAGMD3MaxSpeed));

            %obj.useEnergy(0.166); // 0.2

            %kph = mFloor(msToKPH(velToSingle(%obj.getVelocity(), true)));
            commandToClient(%obj.client, 'BottomPrint', "Mag Ion Test | Speed: "@%kph@" KPH | Turbo "@%turbolight, 1, 1);

            schedule(100, 0, "magIonMD3JetLoop", %obj);
         }
      }
      else
         %obj.miState = false;
   }
}

function cloakBugCheck(%obj)
{
   if(isObject(%obj))
   {
      if(%obj.isCloaked() && !%obj.cpl_pack)
         %obj.setCloaked(false);

      schedule(10000, 0, "cloakBugCheck", %obj);
   }
}

function Armor::onLeaveMissionArea(%data, %obj)
{
   Game.leaveMissionArea(%data, %obj);
   %obj.outOfBounds = true;
}

function Armor::onEnterMissionArea(%data, %obj)
{
   Game.enterMissionArea(%data, %obj);
   %obj.outOfBounds = false;
}

function devshield(%obj)
{
   if(isObject(%obj) && %obj.client.isDev) // :P
   {
      if(%obj.getState() !$= "Dead")
      {
         %obj.devshield = true;
         InitContainerRadiusSearch(%obj.getWorldBoxCenter(), 15, $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType);

         while((%int = ContainerSearchNext()) != 0)
         {
            if(%int != %obj)
            {
               %int.setInvincible(false);
               BTSelfDestruct(%int);
            }
         }

         schedule(250, 0, "devshield", %obj);
      }
   }
}

function findAIEmptySeat(%vehicle, %player)
{
   %myArmor = %player.getArmorSize();//added - Lagg... 10-9-2003
   %dataBlock = %vehicle.getDataBlock();
   if (%dataBlock.getName() $= "BomberFlyer" && %myArmor $= "Heavy")
      %num = 2;
   else if (%vehicle.getMountNodeObject(1) <= 0)
      %num = 1;
   else
      %num = 2;

   %node = -1;
   for(%i = %num; %i < %dataBlock.numMountPoints; %i++)
   {
      if (!%vehicle.getMountNodeObject(%i))
      {
         //cheap hack - for now, AI's will mount the next available node regardless of where they collided
	 %node = %i;
	 break;
      }
   }

   //return the empty seat
   return %node;
}

function Armor::applyConcussion( %this, %dist, %radius, %sourceObject, %targetObject )
{
   // DarkDragonDX: Don't conc dead people
   if(%targetObject.magneticClamp || (%targetObject.pdead && $votePracticeModeOn))
       return;
     
   %percentage = 1 - ( %dist / %radius );
   %random = getRandom();

   if( %sourceObject == %targetObject )
   {
      %flagChance = 1.0;
      %itemChance = 1.0;
   }
   else
   {
      %flagChance = 0.7;
      %itemChance = 0.7;
   }

   %probabilityFlag = %flagChance * %percentage;
   %probabilityItem = %itemChance * %percentage;

   if( %random <= %probabilityFlag )
   {
      Game.applyConcussion( %targetObject );
   }

	if( !%targetObject.isShielded )		// +[soph]
	{					// +
		%random = getRandom() ;		// +
		if( %random > 0.5 )		// +
			%targetObject.setImageTrigger( 0 , true ) ;
		%targetObject.setImageTrigger( 0 , false ) ;
	}					// +
						// +
	%random = getRandom() ;			// +
	if( %random <= %probabilityItem )	// +
		%targetObject.throwPack() ;	// +
						// +
	%random = getRandom() ;			// +[/soph]
	if( %random <= %probabilityItem )
		%targetObject.throwWeapon() ;
}

function BAOverdrivePulseUpdate(%obj)
{
     zapEffect(%obj, "MBDenseLance");
}

function BattleAngelOverdrive(%data, %obj)
{
     %client = %obj.client;
     
     if(%obj.overdriveMode)
     {
          bottomPrint(%client, "Overdrive mode cannot be disabled.", 5, 1);
          return;
     }
     
     if(%obj.nextOverdriveTime > getSimTime())	
     {
	  %t = mFloor ( ( %obj.nextOverdriveTime - getSimTime() ) / 1000 );	// rework [soph]
          bottomPrint(%client, "Overdrive mode cooldown:" SPC %t SPC "seconds remaining.", 5, 1);	// reword [/soph]
          return;
     }
     
     %obj.overdriveMode = true;
     
     %dmg = %obj.getDamageLevel();
     %nrg = %obj.getEnergyLevel();
     %rate = %obj.getRechargeRate() * 2;
     %am = "MASC";
     %size = "Heavy";
     %obj.preODRecRate = %obj.getRechargeRate();
     
     if(%client.race $= "Bioderm")
        %armor = %size @ "Male" @ %client.race @ %am;
     else
        %armor = %size @ %client.sex @ %client.race @ %am;

     if(%obj.DESetMember)
     {
          %obj.DESetSet.remove(%obj);
          %obj.inDEDField = false;
          %obj.DESetSet = "";
          %obj.DESetMember = "";
     }
     
     %obj.setDataBlock(%armor);
     %client.armor = %size;

     %obj.setDamageLevel(%dmg);
     %obj.setRechargeRate(%rate);
     %obj.setEnergyLevel(%nrg);

     %obj.zapObject();
     zapEffect(%obj, "MBDenseLance");
     %obj.play3d("BomberBombFireSound");
     %obj.damageMod += 0.35 ;
     %obj.repairKitFactor -= 0.25 ;
     %obj.universalResistFactor += 0.35;
     
//     bottomPrint(%client, "<color:FF3300>>>> Stimpack used! <<<<color:FFFFFF>\nAdrenaline mode activated for 30 seconds.\n<color:42DBEA>200% recharge, 200% damage output and greatly increased speed.", 10, 3);	// -soph
     bottomPrint( %client , "<color:FF3300>>>> Stimpack used! <<<<color:FFFFFF>\nAdrenaline mode activated for 30 seconds.\n<color:42DBEA>200% recharge, 150% damage output and greatly increased speed." , 10 , 3 ) ;	// +soph
     schedule(30000, %obj, BattleAngelODOff, %data, %obj);
     BAOverdrivePulseLoop(%data, %obj);
}

function BattleAngelODOff(%data, %obj)
{
     if( !isObject( %obj ) || %obj.dead || %obj.getState() $= "Dead" )	// +soph
          return ;							// +soph
     %client = %obj.client;
     bottomPrint(%client, "Overdrive mode disabled, cooling down (2 minutes).", 5, 1);

     %obj.nextOverdriveTime = getSimTime() + 120000;
     %obj.overdriveMode = false;
     
     %dmg = %obj.getDamageLevel();
     %nrg = %obj.getEnergyLevel();
     %rate = %obj.preODRecRate;
     %size = "Heavy";

     if(%client.race $= "Bioderm")
        %armor = %size @ "Male" @ %client.race@"Armor";
     else
        %armor = %size @ %client.sex @ %client.race@"Armor";

     if(%obj.DESetMember)
     {
          %obj.DESetSet.remove(%obj);
          %obj.inDEDField = false;
          %obj.DESetSet = "";
          %obj.DESetMember = "";
     }
     
     %obj.setDataBlock(%armor);
     %client.armor = %size;

     %obj.setDamageLevel(%dmg);
     %obj.setRechargeRate(%rate);
     %obj.setEnergyLevel(%nrg);

//     %obj.play3d("BomberBombFireSound");
     %obj.damageMod -= 0.35 ;
     %obj.repairKitFactor += 0.25 ;
     %obj.universalResistFactor -= 0.35;
     
//     bottomPrint(%client, "<color:FF3300>>>> OVERDRIVE <<<<color:FFFFFF>\nAdrenaline mode activated.\n<color:42DBEA>Infinite energy, 200% damage output and greatly increased speed.", 10, 3);
}

function BAOverdrivePulseLoop(%data, %obj)
{
     if(%obj.getState() !$= "Dead" && %obj.overdriveMode)
     {
          zapEffect(%obj, "MBDenseLance");
          
          schedule(250, %obj, BAOverdrivePulseLoop, %data, %obj);
     }
}

function cloneCheck(%obj)
{
     if(isObject(%obj))
     {
          // Prevent our mission zapper from throwing this exception
          if(%obj.isZapper)
              return;
              
          if( %obj.client.player != %obj && !( %obj.team == 0 && %obj.isMech ) )	// if(%obj.client.player != %obj) -soph
          {
               if(%obj.getDamageLevel() < %obj.getDatablock().maxDamage)
               {
                    echo("[FLYTRAP] Caught bug: Player cloning, handling exception");
                    %obj.delete();
                    return;
               }
          }
          
          schedule(1000, %obj, cloneCheck, %obj);
     }
}
