if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Overrides for console spam

function TerrainBlock::isMounted()
{
      //prevent console errors
      return false;
}

function InteriorInstance::isMounted()
{
      //prevent console errors
      return false;
}

function AIConnection::getDataBlock()
{
      //prevent console errors
      return 0;
}

function AIConnection::getControllingClient()
{
      //prevent console errors
      return 0;
}

function GameConnection::getDatablock()
{
      //prevent console errors
      return 0;
}

function GameConnection::getControllingClient()
{
      //prevent console errors
      return 0;
}

function InteriorInstance::isHidden()
{
      //prevent console errors
      return false;
}

function GameConnection::isMounted()
{
    //prevent console errors
    return false;
}

// Old emitter functions
function attachEmitter(%obj, %emitter, %mod)
{
   Meltdown.emitterID++;

   %pos = "0 0 1";
   %vel = vectorScale(%obj.getVelocity(), 0.5);

   if(!%mod)
   {
      %ctr = %obj.getWorldBoxCenter();
      %pos = vectorAdd(%ctr, %vel);
   }
   else
      %pos = vectorAdd(%mod, %vel);

   createLifeEmitter(%pos, %emitter, 35);
   %obj.attachedEmitter[Meltdown.emitterID] = %emitter;

   schedule(32, %obj, updateAttachedEmitter, %obj, %emitter, %pos);
}

function checkForAttachedEmitter(%obj, %emitter)
{
   if(!%obj.attachedEmitterCount)
      return false;

   for(%i = 0; %i < %obj.attachedEmitterCount; %i++)
   {
      echo("updateLoop");
      if(%obj.attachedEmitter[%i] $= %emitter)
         return %i;
   }
   return false;
}

function updateAttachedEmitter(%obj, %emitter, %pt)
{
   echo("updateInit");
   if(checkForAttachedEmitter(%obj, %emitter))
   {
      echo("update");
      %vel = vectorScale(%obj.getVelocity(), 0.5);
      %pos = vectorAdd(%pt, %vel);

      createLifeEmitter(%pos, %emitter, 35);

      schedule(32, %obj, updateAttachedEmitter, %obj, %emitter, %pt);
   }
}

function deleteAttachedEmitter(%obj, %emitter)
{
   if(isObject(%obj))
   {
      %index = checkForAttachedEmitter(%obj, %emitter);

      if(%index)
         %obj.attachedEmitter[%index] = "";
   }
}

function mVecToRot(%vec)
{
   if(isObject(Meltdown))
   {
      if(!isObject(Meltdown.vectorBuddy))
      {
         MissionCleanup.add(%vecBuddy);
         Meltdown.vectorBuddy = %vecbuddy;
      }

      Meltdown.vectorBuddy.setDeployRotation("0 0 0", %vec);
      return Meltdown.vectorBuddy.getRotation();
   }
   return "0 0 1 0";
}

function createSlotExtension(%obj, %slot, %shapeName)
{
    if(!%slot) // default slot
      %slot = 0;

    if(%shapeName $= "")
       %shapeName = mReflector;

    %ext = new StaticShape()
    {
       dataBlock = %shapeName;
    };

    MissionCleanup.add(%ext);
    %ext.isExtension = true;

    %obj.mountObject(%ext, %slot);
    %obj.slotExtension[%slot] = %ext;
    %obj.slotExtension[%slot].srcObj = %obj;
}

function getRandomChaingunSound()
{
   %rnd = getRandom(7);

   switch(%rnd)
   {
      case 0:
         return "ChaingunImpact";
      case 1:
         return "ChaingunImpact1";
      case 2:
         return "ChaingunImpact2";
      case 3:
         return "ChaingunImpact3";
      case 4:
         return "ChaingunImpact4";
      case 5:
         return "ChaingunImpact5";
      case 6:
         return "ChaingunImpact6";
      default:
         return "ChaingunImpact";
   }
}

function getRandomChaingunWetSound()
{
   %rnd = getRandom(3);

   switch(%rnd)
   {
      case 0:
         return "ChaingunImpact";
      case 1:
         return "ChaingunWaterImpact";
      case 2:
         return "ChaingunWaterImpact2";
      case 3:
         return "ChaingunWaterImpact3";
      default:
         return "ChaingunImpact";
   }
}

function detectIsWet(%pos)
{
   InitContainerRadiusSearch(%pos, 0.1, $TypeMasks::WaterObjectType);

   if(ContainerSearchNext())
      return true;
   else
      return false;
}

function fixskytick()
{
 if(isobject(Sky))
 {
  Sky.stormFog(0.1, 0.1);
 }

  schedule(30000, 0, fixskytick);
}

function fixSky()
{
    if(!$fixskythread)
    {
     $fixskythread = true;
     fixskytick();
    }
}

function projectileTrail(%obj, %time, %projType, %proj, %bSkipFirst, %offset)
{
   if(isObject(%obj) && (isObject(%obj.sourceObject) || isObject(%obj.bkSourceObject)))
   {
      if(%obj.bkSourceObject)
         if(isObject(%obj.bkSourceObject))
            %obj.sourceObject = %obj.bkSourceObject;

      if(!%obj.lastBlinkPos)
         %obj.lastBlinkPos = "65536 65536 65536";

      if(%bSkipFirst)
      {
         schedule(%time, 0, "projectileTrail", %obj, %time, %projType, %proj, false, %offset);
         return;
      }

      if(%offset)
         %pos = vectorAdd(%obj.position, vectorScale(%obj.initialDirection, %offset));
      else
         %pos = %obj.position;

      if(vectorCompare(%pos, %obj.lastBlinkPos)) //same position...BUG!
         return;

      %p = new (%projType)()
      {
         dataBlock        = %proj;
         initialDirection = %obj.initialDirection;
         initialPosition  = %pos;
         sourceObject     = %obj.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
      %p.ignoreReflections = true ;	// +soph
//      %p.sourceObject = 
      
      if(!%obj.trailThread)
      {
         %obj.trailThread = true;
         %obj.trailThreadTime = %time;
         %obj.trailThreadProjType = %projType;
         %obj.trailThreadProj = %proj;
         %obj.trailThreadOffset = %offset;
      }


      schedule(%time, 0, projectileTrail, %obj, %time, %projType, %proj, false, %offset);
      %obj.lastBlinkPos = %pos;
   }
}

function calcSpreadVector(%vector, %val) // (%val / 1000)
{
   %x = (getRandom() - 0.5) * 2 * 3.1415926 * (%val / 1000);
   %y = (getRandom() - 0.5) * 2 * 3.1415926 * (%val / 1000);
   %z = (getRandom() - 0.5) * 2 * 3.1415926 * (%val / 1000);
   %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
   return MatrixMulVector(%mat, %vector);
}

function createReflectorForObject(%obj)
{
     %ref = new StaticShape()
     {
          dataBlock        = mReflector;
     };

     MissionCleanup.add(%ref);
     reflectorLifetimeCheck(%ref, %obj);

     return %ref;
}

function reflectorLifetimeCheck(%ref, %obj)
{
     if(isObject(%obj) && isObject(%ref))
          schedule(250, %ref, reflectorLifetimeCheck, %ref, %obj);
     else if(isObject(%ref))
          %ref.schedule(32, delete);
}

function getSightInfo(%obj, %dist, %mask)
{
   if(%mask $= "")
      %mask = $TypeMasks::EverythingType;

   %vec = %obj.getEyeVector();
   %pos = %obj.getEyePoint();
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, %mask, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return %target;
   else
      return false;
}

function getSightInfoNeg(%obj, %dist, %mask)
{
   if(%mask $= "")
      %mask = $TypeMasks::EverythingType;

   %vec = vectorNeg(%obj.getEyeVector());
   %pos = %obj.getEyePoint();
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, %mask, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return %target;
   else
      return false;
}

$PI = 3.14159; // Torque only allows for 6-digit numbers, inc. sig figure digits

function calcThisRot(%thisRot)
{
   %Mrot = MatrixCreateFromEuler(%thisRot);
   %rot = getWord(%Mrot, 3) SPC getWord(%Mrot, 4) SPC getWord(%Mrot, 5) SPC mCeil(mRadtoDeg(getWord(%Mrot, 6)));

   return %rot;
//   echo("calcThisRot:" SPC %thisRot SPC "rotation = \"" @ %rot @ "\"\;");
}

function calcThisRotD(%this)
{
   %rx = mDegToRad(getWord(%this, 0));
   %ry = mDegToRad(getWord(%this, 1));
   %rz = mDegToRad(getWord(%this, 2));

   %thisRot = %rx SPC %ry SPC %rz;

   %Mrot = MatrixCreateFromEuler(%thisRot);
   %rot = getWord(%Mrot, 3) SPC getWord(%Mrot, 4) SPC getWord(%Mrot, 5) SPC mCeil(mRadtoDeg(getWord(%Mrot, 6)));

   return %rot;
//   echo("calcThisRot:" SPC %thisRot SPC "rotation = \"" @ %rot @ "\"\;");
}

function calcThisRotV(%this)
{
   %rx = mDegToRad(getWord(%this, 0));
   %ry = mDegToRad(getWord(%this, 1));
   %rz = mDegToRad(getWord(%this, 2));

   %thisRot = %rx SPC %ry SPC %rz;

   %Mrot = MatrixCreateFromEuler(%thisRot);
   %rot = getWord(%Mrot, 3) SPC getWord(%Mrot, 4) SPC getWord(%Mrot, 5) SPC mCeil(mRadtoDeg(getWord(%Mrot, 6)));

   return %rot;
//   echo("calcThisRot:" SPC %thisRot SPC "rotation = \"" @ %rot @ "\"\;");
}

function deleteSlotExtension(%obj, %slot)
{
   if(!%slot) // f00lpr00f
      %slot = 0;

   %ext = %obj.getMountNodeObject(%slot);
   if(!%ext)
      return;

   if(%ext.isExtension)
      %ext.delete();

   %obj.slotExtension[%slot] = "";
}

function isSlotExtension(%obj)
{
   if(isObject(%obj))
      if(%obj.isExtension)
         return true;

   return false;
}

function ShapeBase::createSlotExtension(%obj, %slot)
{
     createSlotExtension(%obj, %slot);
}

function ShapeBase::deleteSlotExtension(%obj, %slot)
{
     deleteSlotExtension(%obj, %slot);
}

datablock ShapeBaseImageData(MainAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
//   item = Plasma; // hehe
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

//   casing              = ReaverShellDebris;
//   shellExitDir        = "1.0 0.3 1.0";
//   shellExitOffset     = "0.15 -0.56 -0.1";
//   shellExitVariance   = 15.0;
//   shellVelocity       = 3.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[2] = "Idle";
   stateTransitionOnTriggerDown[2] = "Emit";

   stateName[3] = "Emit";
   stateTransitionOnTimeout[3] = "Idle";
   stateTimeoutValue[3] = 0.032; // min 32ms recharge time
   stateFire[3] = true;

   stateEmitter[3]       = "ChaingunFireSmoke"; // emitter
   stateEmitterTime[3]       = 0.1; //100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
//   stateScript[3]           = "onFire";
//   stateEjectShell[3]       = true;
};

datablock ShapeBaseImageData(EngineAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
//   item = Plasma; // hehe
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

//   casing              = ReaverShellDebris;
//   shellExitDir        = "1.0 0.3 1.0";
//   shellExitOffset     = "0.15 -0.56 -0.1";
//   shellExitVariance   = 15.0;
//   shellVelocity       = 3.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[1] = "Idle";
   stateTransitionOnTriggerDown[1] = "JetOn";

   stateName[2] = "JetOn";
   stateTransitionOnTriggerDown[2] = "JetMaintain";
   stateSequence[2] = "ActivateBack";
   stateDirection[2] = true;
   stateTimeoutValue[2] = 0.5;

   stateName[3] = "JetMaintain";
   stateTransitionOnTriggerUp[3] = "JetOff";
   stateTransitionOnTimeout[3]   = "JetMaintain";
   stateTimeoutValue[3] = 0.01;
   stateSequence[3] = "MaintainBack";
   stateFire[3] = true;

   stateName[4] = "JetOff";
   stateTransitionOnTriggerDown[4] = "JetOn";
   stateSequence[4] = "ActivateBack";
   stateDirection[4] = false;
   stateTimeoutValue[4] = 0.5;
   stateWaitForTimeout[4]          = false;
   stateTransitionOnTimeout[4]     = "Idle";
};

function capValue(%val, %cap)
{
   if(%val > %cap)
      %val = %cap;

   if(%val < 0)
      return %val *= -1;

   return %val;
}

function datablockInfo() // datablockInfo
{
   %blocks = DataBlockGroup.getCount();
   echo("Number of Datablocks:");
   echo(%blocks);
//   echo("Having more than 1975 datablocks is not reccomended. Degraded performance, likelyhood to crash, map incompatibility... ");
   echo("Having more than 1975 datablocks is not recommended. Degraded performance, likelyhood to crash, map incompatibility... "); // (ST)
   echo("Current Datablock Capacity:");
   echo(mCeil((%blocks / 2048)*100)@"%");

   if(%blocks <= 1975)
      echo("The mod is safe and should be compatible with most maps.");
   else
      echo("The mod is not safe and map compatibility is starting to lack. Remove some datablocks.");

   echo("After 1950 datablocks, you cannot use the reload() command in an online server or clients will drop.");
   echo("Once you hit 100% (2048 datablocks), you or anyone else can no longer play.");
}

function RipDatablocks()
{
   return;

   %count = DataBlockGroup.getCount();
   for(%i = 0; %i < %count; %i++)
      DataBlockGroup.getObject(%i).save("scripts/datablocks/db"@%i@".txt");

   echo(%count@" datablocks saved.");
}

// Old code
$g_line100 = "----------------------------------------------------------------------------------------------------";
$g_space100 = "                                                                                                    ";
$g_line200 = "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";

function graphBar100(%val, %invArrow)
{
   %value = capValue(%val, 100);

   %graph = getSubStr($g_line100, 0, %value);
   %left = 100 - %value;
   %leftSpace = getSubStr($g_space100, 0, %left);

   if(%invArrow)
      %out = "["@%graph@"<"@%leftSpace@"]";
   else
      %out = "["@%graph@">"@%leftSpace@"]";

   return %out;
}

function graphBar100R(%val, %gStart, %yStart, %dir)
{
   %value = capValue(%val, 100);

   %graph = getSubStr($g_line100, 0, %value);
   %left = 100 - %value;
   %leftSpace = getSubStr($g_space100, 0, %left);

   if(%gStart $= "")
      %gStart = 33;

   if(%yStart $= "")
      %yStart = 66;

   if(%dir !$= "")
      %dir = true;

   if(%dir)
   {
      if(%value >= %gStart)
         %color = "FF0000";
      else if(%value >= %yStart)
         %color = "FFFF00";
      else
         %color = "00FF00";
   }
   else
   {
      if(%value <= %gStart)
         %color = "00FF00";
      else if(%value <= %yStart)
         %color = "FFFF00";
      else
         %color = "FF0000";
   }

   if(%dir)
      %out = "<color:42dbea>[<color:"@%color@">"@%graph@">"@%leftSpace@"<color:42dbea>]";
   else
      %out = "<color:42dbea>[<color:"@%color@">"@%graph@"<"@%leftSpace@"<color:42dbea>]";

//         %out = "<color:42dbea>[<color:"@%color@">"@%graph@">"@%leftSpace@"<color:42dbea>]";
   return %out;
}

function graphBar100C(%val, %r, %y, %dir)
{
   %value = capValue(%val, 100);

   %graph = getSubStr($g_line100, 0, %value);
   %left = 100 - %value;
   %leftSpace = getSubStr($g_space100, 0, %left);

   if(%r $= "")
      %r = 33;

   if(%y $= "")
      %y = 66;

   if(!%dir)
      %arrow = ">";
   else
      %arrow = "<";

   if(%value <= %r)
      %color = "FF0000";
   else if(%value <= %y)
      %color = "FFFF00";
   else
      %color = "00FF00";

   %out = "<color:42dbea>[<color:"@%color@">"@%graph@""@%arrow@""@%leftSpace@"<color:42dbea>]";
   return %out;
}

// Text Graphing Functions

function DecToHex(%num)
{
   %w[1] = mFloor(%num / 16);
   %w[2] = %num - (%w[1] * 16);

   for (%i=1;%i<3;%i++)
   {
      if (%w[%i] == 10) %w[%i] = "A";
      if (%w[%i] == 11) %w[%i] = "B";
      if (%w[%i] == 12) %w[%i] = "C";
      if (%w[%i] == 13) %w[%i] = "D";
      if (%w[%i] == 14) %w[%i] = "E";
      if (%w[%i] == 15) %w[%i] = "F";
   }
   return %w[1] @ %w[2];
}

function displayExample(%client, %example)
{
   if(%example == 1)
      flashWindowDynamic(%client, "This has been a public service announcement from the Good Lord.", 75, 1, 2, true, "<color:fc9999>");
   else if(%example == 2)
      graphWindowDynamic(%client, "How are you gentlemen? All your base are belong to us. You have no chance to survive make your time!", 100, 2, 2, true, "<color:777777>");
   else if(%example == 3)
      lineWindowDynamic(%client, "This message will self destruct in 10 seconds.", 100, 2, 2, true);
   else if(%example == 4)
      graphWindowDynamic(%client, "Welcome to DynaBlade's Meltdown2 "@Meltdown2.Version@" "@%client.namebase@"!\n"@$MOTD, 50, 3, 4, true);
   else if(%example == 5)
      graphWindowDynamicAll("This message will self destruct in 10 seconds.", 100, 1, 2, true, "<color:777777>");
   else if(%example == 6)
      lineWindowDynamicAll("This has been a public service announcement from the Good Lord.", 100, 1, 2, true, "<color:fc9999>");
   else if(%example == 7)
      flashWindowDynamicAll("How are you gentlemen? All your base are belong to us. You have no chance to survive make your time!", 100, 2, 2, true, "<color:777777>");
}

function graphWindowDynamic(%client, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   if(isObject(%client))
   {
      if(%client.hasMD2Client)
      {
         commandToClient(%client, 'graphWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
         return;
      }

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;
//         capValue(%count, 255);

         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3); // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);

//         %pos = %count--;
//         %grn = %count+20;
//         %col = DecToHex(%count)@""@DecToHex(%count)@""@DecToHex(%count);
//         %strc1 = "<color:"@%col@">";

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            commandToClient(%client, 'CenterPrint', %colorTag@""@%out@""@%msfhdr, %time+1, %lines);
         else
            commandToClient(%client, 'BottomPrint', %colorTag@""@%out@""@%msfhdr, %time+1, %lines);

         schedule(%timeMS, %client, graphWindowDynamic, %client, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            commandToClient(%client, 'CenterPrint', %colorTag@""@%message, %timeout, %lines);
         else
            commandToClient(%client, 'BottomPrint', %colorTag@""@%message, %timeout, %lines);
      }
   }
}

function flashWindowDynamic(%client, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   if(isObject(%client))
   {
      if(%client.hasMD2Client)
      {
         commandToClient(%client, 'flashWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
         return;
      }

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 75;

      if(%colorTag $= "")
         %colorTag = "<color:42dbea>";

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;

         if((%count - 5) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 5);

         %nLength = %length - %count;

         if(%nLength < 0)
            %nLength = 0;

         if(%count < 5)
            %hcount = %count;
         else
            %hcount = 5;

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, %hcount)@""@%colorTag; // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);
         %end = getSubStr(%message, %count, %nlength);

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            commandToClient(%client, 'CenterPrint', %colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);
         else
            commandToClient(%client, 'BottomPrint', %colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);

         schedule(%timeMS, %client, flashWindowDynamic, %client, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            commandToClient(%client, 'CenterPrint', %colorTag@""@%message, %timeout, %lines);
         else
            commandToClient(%client, 'BottomPrint', %colorTag@""@%message, %timeout, %lines);
      }
   }
}

function lineWindowDynamic(%client, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   if(isObject(%client))
   {
      if(%client.hasMD2Client)
      {
         commandToClient(%client, 'lineWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
         return;
      }

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      if(%colorTag $= "")
         %colorTag = "<color:42dbea>";

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;
//         capValue(%count, 255);
         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %nLength = %length - %count;

         if(%nLength < 0)
            %nLength = 0;

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3)@""@%colorTag; // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);
         %end = getSubStr($g_line200, %count, %nlength);

//         %pos = %count--;
//         %grn = %count+20;
//         %col = DecToHex(%count)@""@DecToHex(%count)@""@DecToHex(%count);
//         %strc1 = "<color:"@%col@">";

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            commandToClient(%client, 'CenterPrint', %colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);
         else
            commandToClient(%client, 'BottomPrint', %colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);

         schedule(%timeMS, %client, lineWindowDynamic, %client, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            commandToClient(%client, 'CenterPrint', %colorTag@""@%message, %timeout, %lines);
         else
            commandToClient(%client, 'BottomPrint', %colorTag@""@%message, %timeout, %lines);
      }
   }
}

function graphWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
//      %message = addTaggedString(%message);
   clientGraphWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;
//         capValue(%count, 255);

         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3); // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);

//         %pos = %count--;
//         %grn = %count+20;
//         %col = DecToHex(%count)@""@DecToHex(%count)@""@DecToHex(%count);
//         %strc1 = "<color:"@%col@">";

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            centerPrintAll(%colorTag@""@%out@""@%msfhdr, %time+1, %lines);
         else
            bottomPrintAll(%colorTag@""@%out@""@%msfhdr, %time+1, %lines);

         schedule(%timeMS, 0, graphWindowDynamicAll, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            centerPrintAll(%colorTag@""@%message, %timeout, %lines);
         else
            bottomPrintAll(%colorTag@""@%message, %timeout, %lines);
      }
}

function flashWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   clientFlashWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 75;

      if(%colorTag $= "")
         %colorTag = "<color:42dbea>";

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;
         if((%count - 5) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 5);

         %nLength = %length - %count;

         if(%nLength < 0)
            %nLength = 0;

         if(%count < 5)
            %hcount = %count;
         else
            %hcount = 5;

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, %hcount)@""@%colorTag; // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);
         %end = getSubStr(%message, %count, %nlength);

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            centerPrintAll(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);
         else
            bottomPrintAll(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);

         schedule(%timeMS, 0, flashWindowDynamicAll, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            centerPrintAll(%colorTag@""@%message, %timeout, %lines);
         else
            bottomPrintAll(%colorTag@""@%message, %timeout, %lines);
      }
}

function lineWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   clientLineWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      if(%colorTag $= "")
         %colorTag = "<color:42dbea>";

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;
//         capValue(%count, 255);
         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %nLength = %length - %count;

         if(%nLength < 0)
            %nLength = 0;

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3)@""@%colorTag; // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);
         %end = getSubStr($g_line200, %count, %nlength);

//         %pos = %count--;
//         %grn = %count+20;
//         %col = DecToHex(%count)@""@DecToHex(%count)@""@DecToHex(%count);
//         %strc1 = "<color:"@%col@">";

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            centerPrintAll(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);
         else
            bottomPrintAll(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);

         schedule(%timeMS, 0, lineWindowDynamicAll, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            centerPrintAll(%colorTag@""@%message, %timeout, %lines);
         else
            bottomPrintAll(%colorTag@""@%message, %timeout, %lines);
      }
}

function centerPrintAll( %message, %time, %lines )
{
   if( %lines $= "" || ((%lines > 3) || (%lines < 1)) )
      %lines = 1;

   %count = ClientGroup.getCount();
   for (%i = 0; %i < %count; %i++)
	{
		%cl = ClientGroup.getObject(%i);
      if( !%cl.isAIControlled() && !%cl.excludeFromWDA)
         commandToClient( %cl, 'centerPrint', %message, %time, %lines );
      else
         %cl.excludeFromWDA = false;
   }
}

function bottomPrintAll( %message, %time, %lines )
{
   if( %lines $= "" || ((%lines > 3) || (%lines < 1)) )
      %lines = 1;

   %count = ClientGroup.getCount();
	for (%i = 0; %i < %count; %i++)
	{
		%cl = ClientGroup.getObject(%i);
      if( !%cl.isAIControlled() && !%cl.excludeFromWDA)
         commandToClient( %cl, 'bottomPrint', %message, %time, %lines );
      else
         %cl.excludeFromWDA = false;
   }
}

$DefaultReflectableProjectles = "TracerProjectile" SPC "EnergyProjectile" SPC "LinearProjectile" SPC "LinearFlareProjectile" SPC "GrenadeProjectile" SPC "BombProjectile";
$RepulsorBeaconReflectable = "TracerProjectile" SPC "EnergyProjectile" SPC "LinearProjectile" SPC "LinearFlareProjectile" SPC "GrenadeProjectile" SPC "BombProjectile" SPC "SeekerProjectile";

function isReflectableProjectile(%obj, %mask)
{
   if( %obj.exploded )						// +soph
      return false;						// +soph

   if(%mask $= "")
      %mask = $DefaultReflectableProjectles;

   if( %obj.getDataBlock().getName() $= "MechHowitzerMissile" )	// i hate having to have a specific exception +soph
      return true;						// but there's no reason to toss another flag +soph

   for(%i = 0; (%type = getWord(%mask, %i)) !$= ""; %i++)
   {
      if(%obj.getClassName() $= %type && !%obj.ignoreReflections && !%obj.getDatablock().ignoreReflection)
         return true;
   }
   return false;
}

function instanceReflectProjectiles(%pos, %radius, %reflectType)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::ProjectileObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.reflected)
         continue;

      if(isReflectableProjectile(%int, %reflectType))
      {
//         echo(%int);
//         echo(%int.getDatablock());
//         echo(%int.getDatablock().getName()); // name of projectile (discProjectile)
//         echo(%int.getDatablock().getClassName()); // name of Projectile Datablock type (LinearProjectileData)
//         echo(%int.getClassName()); // name of Projectile for spawning (LinearProjectile)
//         echo(%int.getType()); // returns type of object it is
//         %int.dump();

//           %FFRObject = new StaticShape()
//           {
//              dataBlock = mReflector;
//           };
//           MissionCleanup.add(%FFRObject);
          if(!isObject(%obj.reflectorObj))
               %obj.reflectorObj = createReflectorForObject(%obj);
               
          %FFRObject = %obj.reflectorObj;
           %FFRObject.setPosition(%int.getPosition());

           if(%int.getClassName() $= "BombProjectile")
               %vector = vectorScale(calcSpreadVector(vectorNeg(%int.initialDirection), 48), 350);
           else
               %vector = calcSpreadVector(vectorNeg(%int.initialDirection), 48);

           if(!isObject(%int.sourceObject))
               %src = %int.bkSourceObject;
           else
               %src = %int.sourceObject;

           if( %int.originTime )					// +[soph]
           {								// +
              %airTimeMod = getSimTime() - %int.originTime ; 		// +
              %data = %int.getDatablock().getName() ;			// +
              if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
                 %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
              else							// +
                 %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
              %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;	// +
           }								// +[/soph]

           %p = new(%int.getClassName())()
           {
              dataBlock         = %int.getDatablock().getName();
              initialDirection  = %vector; //vectorNeg(%int.initialDirection);
              initialPosition   = %int.getPosition();
              sourceObject      = %FFRObject;
              sourceSlot        = %int.sourceSlot;
              reflected         = true;
              bkSourceObject    = %src;
              starburstMode     = %int.starburstMode;
              damageMod         = %int.damageMod;
              vehicleMod        = %int.vehicleMod;
           };
           MissionCleanup.add(%p);

           %p.sourceObject = %src;

           if( %src.lastMortar == %int )				// if(%src.lastMortar !$= "") -soph
               %src.lastMortar = %p;

           %p.originTime = %int.originTime ;				// +soph
         
           if(%int.trailThread)
              projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);

           if(%int.proxyThread)
              rBeginProxy(%p);
      
           if(%int.lanceThread)
           {        
               SBLanceRandomThread(%p);
               %p.lanceThread = true;
           }

           createLifeEmitter(%int.getPosition(), "PCRSparkEmitter", 750); // "PCRSparkEmitter"

           %int.delete();
//           %FFRObject.schedule(32, delete);
      }
      else continue;
   }
}

function objectReflectProjectiles(%obj, %radius, %reflectType, %damageObject, %idm, %ddm)
{
   if(%reflectType $= "")
      %reflectType = $RepulsorBeaconReflectable;

   if(%idm $= "")
      %idm = 0.25;

   if(%ddm $= "")
      %ddm = 0.025;

   %src = %obj.linkedShieldSource;
   
   if(!isObject(%src))
     return;
   
   if(!%src.isPowered())
     return;
     
   %refPos = %obj.getWorldBoxCenter();
   
   InitContainerRadiusSearch(%refPos, %radius, $TypeMasks::ProjectileObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(vectorDist(%int.getWorldBoxCenter(), %obj.getWorldBoxCenter()) < (%radius * 0.8)) // within 80% of the total radius of the reflector
         %int.noreflect = true;

      if(%int.lastReflectedFrom == %obj)
         continue;

      if(isObject(%int.sourceObject) && %int.sourceObject.team == %obj.team)
           if(%int.sourceObject.getDatablock().armorMask & $ArmorMask::Engineer | $ArmorMask::BattleAngel)
               continue;

      if(isReflectableProjectile(%int, %reflectType))
      {
//         echo(%int);
//         echo(%int.getDatablock());
//         echo(%int.getDatablock().getName()); // name of projectile (discProjectile)
//         echo(%int.getDatablock().getClassName()); // name of Projectile Datablock type (LinearProjectileData)
//         echo(%int.getClassName()); // name of Projectile for spawning (LinearProjectile)
//         echo(%int.getType()); // returns type of object it is
//         %int.dump();

          %coverage = calcExplosionCoverage(%refPos, %int, $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticShapeObjectType);
          
          if(%coverage == 0)
               continue;
         
          if(!isObject(%obj.reflectorObj))
               %obj.reflectorObj = createReflectorForObject(%obj);
               
          %FFRObject = %obj.reflectorObj;
         %FFRObject.setPosition(%int.getPosition());

// --- Find reflection angle (ST)
         //%vector = vectorNeg(%int.initialDirection); // Cooking it off wont allow the enemy to pick it up and throw it right back at ya
         //%x = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
         //%y = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
         //%z = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
         //%mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
         //%vector = MatrixMulVector(%mat, %vector);

         if(!%int.noreflect)
         {
            %pVec = VectorNormalize(%int.initialDirection);
            %ctrPos = VectorAdd(%obj.getPosition(), "0 0 3");
            %vecFromCtr = VectorNormalize(VectorSub(%int.getPosition(), %ctrPos)); // --- Acts as surface normal (ST)

            // --- Thanks to Mostlikely
            %flip = VectorDot(%vecFromCtr, %pVec);
            %flip = VectorScale(%vecFromCtr, %flip);
            %newVec = VectorAdd(%pVec, VectorScale(%flip, -2));
            // --- ...again LOL (ST)
            %newPos = VectorAdd(%int.getPosition(), %newVec);
   // --- End reflection angle (ST)

            if(%int.getClassName() $= "BombProjectile")
               %newVec = vectorScale(%newVec, 350);

           if(!isObject(%int.sourceObject))
               %src = %int.bkSourceObject;
           else
               %src = %int.sourceObject;

            if( %int.originTime )					// +[soph]
            {								// +
               %airTimeMod = getSimTime() - %int.originTime ; 		// +
               %data = %int.getDatablock().getName() ;			// +
               if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
                  %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
               else							// +
                  %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
               %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;	// +
            }								// +[/soph]

            %p = new(%int.getClassName())()
            {
               dataBlock         = %int.getDatablock().getName();
               initialDirection  = %newVec; // --- (ST) %vector; //vectorNeg(%int.initialDirection);
               initialPosition   = %newPos; // --- (ST) %int.getPosition();
               sourceObject      = %FFRObject;
               sourceSlot        = %int.sourceSlot;
               lastReflectedFrom = %obj;
               bkSourceObject    = %src;
               starburstMode     = %int.starburstMode;
               damageMod         = %int.damageMod;
               vehicleMod        = %int.vehicleMod;
            };
            MissionCleanup.add(%p);

            %p.sourceObject = %src;

           if( %src.lastMortar == %int )				// if(%src.lastMortar !$= "") -soph
               %src.lastMortar = %p;
                           
           %p.originTime = %int.originTime ;				// +soph

            if(%int.trailThread)
               projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);

            if(%int.proxyThread)
               rBeginProxy(%p);

            if(%int.blackholeThread)
               blackHoleLoop(%p);

           if(%int.lanceThread)
           {        
               SBLanceRandomThread(%p);
               %p.lanceThread = true;
           }

   //         createLifeEmitter(%int.getPosition(), "PCRSparkEmitter", 750);
            createLifeEmitter(%newPos, "PCRSparkEmitter", 750);

            if(%damageObject)
            {
               if(isObject(%src))
               {
                    if( %int.getDatablock().hasDamageRadius && %int.getDatablock().indirectDamage > ( 2 * %int.getDatablock().directDamage ) )	// if(%int.getDatablock().hasDamageRadius) -soph
                       %obj.applyDamage(%int.getDatablock().indirectDamage);
                    else
                       %obj.applyDamage(%int.getDatablock().directDamage);
               }
            }

            %int.delete();
            %FFRObject.schedule($Host::RepulsorTickRate, delete);
         }
         else
         {
            if(%damageObject)
            {
               if(isObject(%src))
               {
                    if( %int.getDatablock().hasDamageRadius && %int.getDatablock().indirectDamage > ( 2 * %int.getDatablock().directDamage ) )	// if(%int.getDatablock().hasDamageRadius) -soph
                       %obj.applyDamage(%int.getDatablock().indirectDamage);
                    else
                       %obj.applyDamage(%int.getDatablock().directDamage);
               }
            }

            %int.delete();
//            %FFRObject.schedule(32, delete);
         }
      }
      else continue;
   }
}

function comparePositions(%pos1, %pos2)
{
   return mFloor(vectorDot(vectorNormalize(%pos1), vectorNormalize(%pos2)) * 100);
}

function zapVehicle(%obj, %lanceData)
{
     %pilot = %obj.getMountNodeObject(0);

     if(!isObject(%pilot))
          %pilot = 0;
          
          if(!isObject($MissionZapper))
          {
               %z = new Player()
               {
                    dataBlock = "LightMaleHumanArmor";
               };
               %z.setInvincible(true);
               %z.startFade(1, 0, true);
               %z.setTransform("0 0 -1000 1 0 0 0");
               MissionCleanup.add(%z);
               %z.isZapper = true;
               $MissionZapper = %z;
          }

     %zapper = !%pilot ? $MissionZapper : %pilot;
     %pos = %pilot ? %pilot.getPosition() : %obj.getPosition();
     
          if(!%pilot)
          {
               $MissionZapper.setPosition(vectorAdd(%pos, "0 0 -1"));
               %zapper.setVelocity("0 0 0");
          }
                   
          %zap = new ShockLanceProjectile()
          {
               dataBlock        = %lanceData;
               initialDirection = "0 0 -0.5"; //getVectorFromPoints(%targetObject.ffrobject.getPosition(), %targetObject.getWorldBoxCenter());
               initialPosition  = vectorAdd(%pos, "0 0 0.6"); //%targetObject.ffrobject.getPosition(); //%targetObject.getWorldBoxCenter();
               sourceObject     = %zapper;
               sourceSlot       = 7;
               targetId         = %obj;
          };
          MissionCleanup.add(%zap);
          
          if(!%pilot)
               $MissionZapper.setPosition("0 0 -1000");          
}

function zapStaticObject(%obj, %lanceData)
{
     if(!isObject($MissionZapper))
     {
               %z = new Player()
               {
                    dataBlock = "LightMaleHumanArmor";
               };
               %z.setInvincible(true);
               %z.startFade(1, 0, true);
               %z.setTransform("0 0 -1000 1 0 0 0");
               %z.isZapper = true;
               MissionCleanup.add(%z);
               $MissionZapper = %z;
     }

     %pos = %obj.getPosition();
     $MissionZapper.setPosition(vectorAdd(%pos, "0 0 -1"));
     $MissionZapper.setVelocity("0 0 0");
                   
          %zap = new ShockLanceProjectile()
          {
               dataBlock        = %lanceData;
               initialDirection = "0 0 -0.5"; 
               initialPosition  = vectorAdd(%pos, "0 0 0.6");
               sourceObject     = $MissionZapper;
               sourceSlot       = 7;
               targetId         = %obj;
          };
          MissionCleanup.add(%zap);

     $MissionZapper.setPosition("0 0 -1000");          
}

function zapEffect(%obj, %lanceData)
{
     %obj.zap = new ShockLanceProjectile()
     {
          dataBlock        = %lanceData;
          initialDirection = "0 0 0";
          initialPosition  = %obj.getWorldBoxCenter();
          sourceObject     = %obj;
          sourceSlot       = 0;
          targetId         = %obj;
     };
     
     MissionCleanup.add(%obj.zap);
}

function deCloak( %obj )
{
   if( %obj.reCloak )
   {
      cancel( %obj.reCloak ) ;
      %obj.reCloak = 0 ;
   }
   %obj.setCloaked( false ) ; 
}
