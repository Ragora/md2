if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Meltdown2 Variables
//-Nite-1501 tweaked the ammount of ammo protron gets in the inv stations..now gets half of max
// Game

function DefaultGame::clearDeployableMaxes(%game)
{
   for(%i = 0; %i <= %game.numTeams; %i++)
   {
      $TeamDeployedCount[%i, TurretIndoorDeployable] = 0;
      $TeamDeployedCount[%i, TurretOutdoorDeployable] = 0;
      $TeamDeployedCount[%i, PulseSensorDeployable] = 0;
      $TeamDeployedCount[%i, MotionSensorDeployable] = 0;
      $TeamDeployedCount[%i, InventoryDeployable] = 0;
      $TeamDeployedCount[%i, DeployedCamera] = 0;
      $TeamDeployedCount[%i, MineDeployed] = 0;
      $TeamDeployedCount[%i, TargetBeacon] = 0;
      $TeamDeployedCount[%i, MarkerBeacon] = 0;
      $TeamDeployedCount[%i, VehiclePadPack] = 0;
      $TeamDeployedCount[%i, TurretBasePack] = 0;
      $TeamDeployedCount[%i, TelePack] = 0;
     // $TeamDeployedCount[%i, WildcatPack] = 0;  -Nite- Drones ?
      $TeamDeployedCount[%i, DeployableInvHumanPack] = 0;
      $TeamDeployedCount[%i, ShieldGeneratorPack] = 0;
      $TeamDeployedCount[%i, DefenderDesignator] = 0;
//      $TeamDeployedCount[%i, DetPack] = 0;  -Nite- nuke
      $TeamDeployedCount[%i, TapperPack] = 0;
      $TeamDeployedCount[%i, HoverBasePack] = 0;
      $TeamDeployedCount[%i, BorgCubePack] = 0;
      $TeamDeployedCount[%i, DefenseBasePack] = 0;
      $TeamDeployedCount[%i, ShieldBeaconPack] = 0;
      $TeamDeployedCount[%i, TeamchatBeaconPack] = 0;
      $TeamDeployedCount[%i, RepulsorBeaconPack] = 0;
      $TeamDeployedCount[%i, JammerBeaconPack] = 0;
      $TeamDeployedCount[%i, DeployableGeneratorPack] = 0;
      $TeamDeployedCount[%i, DefEnhancePack] = 0;
//      $TeamDeployedCount[%i, TacMechPack] = 0;
      $TeamDeployedCount[%i, BlastWallPack] = 0;
      $TeamDeployedCount[%i, BlastDoorPack] = 0;
      $TeamDeployedCount[%i, DeployableSentryPack] = 0;
//      $TeamDeployedCount[%i, LaserSentryPack] = 0;
      $TeamDeployedCount[%i, MantaSatPack] = 0;
      $TeamDeployedCount[%i, RMSSPack] = 0; // Death~Bot
   }
}

$BackpackHudData[18, itemDataName] = "HeatShieldPack";
$BackpackHudData[18, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[19, itemDataName] = "GravitronPack";
$BackpackHudData[19, bitmapName] = "gui/hud_new_packsatchel";
$BackpackHudData[20, itemDataName] = "SensorJamMod";
$BackpackHudData[20, bitmapName] = "gui/hud_new_packsensjam";
$BackpackHudData[21, itemDataName] = "MagIonHeatPack";
$BackpackHudData[21, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[22, itemDataName] = "PolarArmorPlatePack";
$BackpackHudData[22, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[23, itemDataName] = "MagIonPack";
$BackpackHudData[23, bitmapName] = "gui/hud_new_packsatchel";
$BackpackHudData[24, itemDataName] = "GravArmorPlatePack";
$BackpackHudData[24, bitmapName] = "gui/hud_new_packsatchel";
$BackpackHudData[25, itemDataName] = "VehiclePadPack";
$BackpackHudData[25, bitmapName] = "gui/hud_new_packrepair";
$BackpackHudData[26, itemDataName] = "TurretBasePack";
$BackpackHudData[26, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[27, itemDataName] = "LavaShieldMod";
$BackpackHudData[27, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[28, itemDataName] = "DefEnhancePack";
$BackpackHudData[28, bitmapName] = "gui/hud_new_packinventory";
$BackpackHudData[29, itemDataName] = "PlasmaCannonPack";
$BackpackHudData[29, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[30, itemDataName] = "DaiTurretDWPack";
$BackpackHudData[30, bitmapName] = "gui/hud_new_packinventory";
$BackpackHudData[31, itemDataName] = "DiscCannonPack";
$BackpackHudData[31, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[32, itemDataName] = "GrenadeCannonPack";
$BackpackHudData[32, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[33, itemDataName] = "ShotgunCannonPack";
$BackpackHudData[33, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[34, itemDataName] = "PlasmaCannonPack";
$BackpackHudData[34, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[35, itemDataName] = "TelePack";
$BackpackHudData[35, bitmapName] = "gui/hud_new_packrepair";
$BackpackHudData[36, itemDataName] = "PulseCannonPack";
$BackpackHudData[36, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[37, itemDataName] = "RepulsorBeaconPack";
$BackpackHudData[37, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[38, itemDataName] = "AutoCCannonPack";
$BackpackHudData[38, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[39, itemDataName] = "DeployableInvHumanPack";
$BackpackHudData[39, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[40, itemDataName] = "JetfirePack";
$BackpackHudData[40, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[41, itemDataName] = "MegaDiscPack";
$BackpackHudData[41, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[42, itemDataName] = "ShieldGeneratorPack";
$BackpackHudData[42, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[43, itemDataName] = "BlasterCannonPack";
$BackpackHudData[43, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[44, itemDataName] = "SynomiumPack";
$BackpackHudData[44, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[45, itemDataName] = "StarHammerPack";
$BackpackHudData[45, bitmapName] = "gui/hud_new_packammo";
$BackpackHudData[46, itemDataName] = "SlipstreamPack";
$BackpackHudData[46, bitmapName] = "gui/hud_new_packcloak";
$BackpackHudData[47, itemDataName] = "SRM1Pack";		// +soph
$BackpackHudData[47, bitmapName] = "gui/hud_missiles";		// +soph
//$BackpackHudData[47, itemDataName] = "SoulShieldPack";	// -soph
//$BackpackHudData[47, bitmapName] = "gui/hud_new_packshield";	// -soph
//$BackpackHudData[48, itemDataName] = "MagIonSoulShieldPack";
//$BackpackHudData[48, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[48, itemDataName] = "SRM2Pack";		// +soph
$BackpackHudData[48, bitmapName] = "gui/hud_missiles";		// +soph
$BackpackHudData[49, itemDataName] = "SRM4Pack";
$BackpackHudData[49, bitmapName] = "gui/hud_missiles";
$BackpackHudData[50, itemDataName] = "ShockBlasterPack";
$BackpackHudData[50, bitmapName] = "gui/hud_plasma";
$BackpackHudData[51, itemDataName] = "CometCannonPack";
$BackpackHudData[51, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[52, itemDataName] = "InventoryPack";
$BackpackHudData[52, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[53, itemDataName] = "ShieldBeaconPack";
$BackpackHudData[53, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[54, itemDataName] = "LaserBeamerPack";
$BackpackHudData[54, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[55, itemDataName] = "TurbogravPack";
$BackpackHudData[55, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[56, itemDataName] = "BorgCubePack";
$BackpackHudData[56, bitmapName] = "gui/hud_new_packrepair";
$BackpackHudData[57, itemDataName] = "JammerBeaconPack";
$BackpackHudData[57, bitmapName] = "gui/hud_new_packsensjam";
$BackpackHudData[58, itemDataName] = "DefenseBasePack";
$BackpackHudData[58, bitmapName] = "gui/hud_new_packrepair";
$BackpackHudData[59, itemDataName] = "HoverBasePack";
$BackpackHudData[59, bitmapName] = "gui/hud_new_packrepair";
$BackpackHudData[60, itemDataName] = "EnergyBarrelPack";
$BackpackHudData[60, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[61, itemDataName] = "VehicleEnergyMod";
$BackpackHudData[61, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[62, itemDataName] = "TeamchatBeaconPack";
$BackpackHudData[62, bitmapName] = "gui/hud_new_packsensjam";
$BackpackHudData[63, itemDataName] = "FlareMod";
$BackpackHudData[63, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[66, itemDataName] = "TacMechPack";
$BackpackHudData[66, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[64, itemDataName] = "BlastWallPack";
$BackpackHudData[64, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[65, itemDataName] = "BlastDoorPack";
$BackpackHudData[65, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[66, itemDataName] = "DeployableSentryPack";
$BackpackHudData[66, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[67, itemDataName] = "DualGrenadeCannonPack";
$BackpackHudData[67, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[68, itemDataName] = "MantaSatPack";
$BackpackHudData[68, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[69, itemDataName] = "MANTAInfernalPack";
$BackpackHudData[69, bitmapName] = "gui/hud_new_packammo";
$BackpackHudData[70, itemDataName] = "RMSSPack";
$BackpackHudData[70, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[71, itemDataName] = "MANTACapacitorPack";
$BackpackHudData[71, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[72, itemDataName] = "RMSSaturnVAmmoPack";
$BackpackHudData[72, bitmapName] = "gui/hud_new_packammo";
$BackpackHudData[73, itemDataName] = "MANTANukePack";
$BackpackHudData[73, bitmapName] = "gui/hud_new_packammo";
$BackpackHudData[74, itemDataName] = "VectorPortPack";
$BackpackHudData[74, bitmapName] = "gui/hud_new_packcloak";
$BackpackHudData[75, itemDataName] = "RMSScatterpackAmmoPack";
$BackpackHudData[75, bitmapName] = "gui/hud_new_packammo";

$BackpackHudCount = 76;  //-Nite- with taking out all this stuff we free'd      up some room.

$InventoryHudData[8, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[8, itemDataName] = MortarGrenade;
$InventoryHudData[8, ammoDataName] = MortarGrenade;
$InventoryHudData[8, slot]         = 0;
$InventoryHudData[9, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[9, itemDataName] = EMPGrenade;
$InventoryHudData[9, ammoDataName] = EMPGrenade;
$InventoryHudData[9, slot]         = 0;
$InventoryHudData[10, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[10, itemDataName] = FireGrenade;
$InventoryHudData[10, ammoDataName] = FireGrenade;
$InventoryHudData[10, slot]         = 0;
$InventoryHudData[11, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[11, itemDataName] = PoisonGrenade;
$InventoryHudData[11, ammoDataName] = PoisonGrenade;
$InventoryHudData[11, slot]         = 0;
$InventoryHudData[12, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[12, itemDataName] = WhiteoutGrenade;
$InventoryHudData[12, ammoDataName] = WhiteoutGrenade;
$InventoryHudData[12, slot]         = 0;
$InventoryHudData[13, bitmapName]   = "gui/hud_mine";
$InventoryHudData[13, itemDataName] = LargeMine;
$InventoryHudData[13, ammoDataName] = LargeMine;
$InventoryHudData[13, slot]         = 1;
$InventoryHudData[14, bitmapName]   = "gui/hud_mine";
$InventoryHudData[14, itemDataName] = CloakMine;
$InventoryHudData[14, ammoDataName] = CloakMine;
$InventoryHudData[14, slot]         = 1;
$InventoryHudData[15, bitmapName]   = "gui/hud_mine";
$InventoryHudData[15, itemDataName] = EMPMine;
$InventoryHudData[15, ammoDataName] = EMPMine;
$InventoryHudData[15, slot]         = 1;
$InventoryHudData[16, bitmapName]   = "gui/hud_mine";
$InventoryHudData[16, itemDataName] = IncendiaryMine;
$InventoryHudData[16, ammoDataName] = IncendiaryMine;
$InventoryHudData[16, slot]         = 1;

$InventoryHudCount = 17;

$ControlObjectReticle[PhaserBarrelLarge, bitmap] = "ret_blaster";
$ControlObjectReticle[PhaserBarrelLarge, frame] = true;
$ControlObjectReticle[MitziBarrelLarge, bitmap] = "ret_elf";
$ControlObjectReticle[MitziBarrelLarge, frame] = true;
$ControlObjectReticle[DiscBarrelLarge, bitmap] = "ret_disc";
$ControlObjectReticle[DiscBarrelLarge, frame] = true;
$ControlObjectReticle[ChaingunBarrelLarge, bitmap] = "ret_chaingun";
$ControlObjectReticle[ChaingunBarrelLarge, frame] = true;
$ControlObjectReticle[EnergyBarrelLarge, bitmap] = "ret_mortor";
$ControlObjectReticle[EnergyBarrelLarge, frame] = true;

// Inventory Cycle List

function ShapeBase::hasAmmo(%this, %weapon)
{
   if(%this.disableSwitch)
     return false;
     
   switch$(%weapon)
   {
      case Blaster:
         return (true);  //self charge
      case Plasma:
         return( %this.getInventory( PlasmaAmmo ) > 0 );
      case Chaingun:
         return( %this.getInventory( ChaingunAmmo ) > 0 );
      case Disc:
         return( %this.getInventory( DiscAmmo ) > 0 );
      case GrenadeLauncher:
         return( %this.getInventory( GrenadeLauncherAmmo ) > 0 );
      case SniperRifle:
         return true;
//         return( %this.getInventory( EnergyPack ) );
      case ELFGun:
         return( true );
      case Mortar:
         return( %this.getInventory( MortarAmmo ) > 0 );
      case MissileLauncher:
         return ( %this.getInventory( MissileLauncherAmmo ) > 0 ) ? true : ( ( %this.getInventory( SRM4Ammo ) > 0 ) && ( %this.getInventory( SRM1Pack ) || %this.getInventory( SRM2Pack ) || %this.getInventory( SRM4Pack ) ) );	// return( %this.getInventory( MissileLauncherAmmo ) > 0 ); -soph
      case ShockLance:
         return( true );
      // This is our kinda hacky way to keep from cycling to the targeting laser:
      case TargetingLaser:
         return( false );
      case EnergyRifle:
         return( %this.getInventory( EnergyRifleAmmo ) > 0 );
      case MBCannon:
         return(true);   // self-charge weapon.
      case RFL:
         return(true);   // self-charge weapon.
      case FissionRifle:
         return(true);
      case Protron:
         return(true);
//         return( %this.getInventory( ProtronAmmo ) > 0 );
//      case TetherLauncher:
//         return( %this.getInventory( TetherLauncherAmmo ) > 0 );
      case PCR:
         return(true);   // self-charge weapon.
      case RAXX:
         return( %this.getInventory( RAXXAmmo ) > 0 );
      case MissileBarrelGun:
         return(true);
      case PBW:
         return( %this.getInventory( PBWAmmo ) > 0 );
      case MagMissileLauncher:
         return(true);
      case MortarBarrelGun:
         return(true);
      case Multifusor:
         return(%this.getInventory(DiscAmmo) > 0);
      case Starburst:
         return(%this.getInventory(StarburstAmmo) > 0);
      case AutoCannon:
         return(%this.getInventory(AutoCannonAmmo) > 0);
      case Reassembler:
         return true;
      case EcstacyCannon:
         return true;
      case MechMinigun:
         return(%this.getInventory(MechMinigunAmmo) > 0);
      case MechRocketGun:
         return(%this.getInventory(MechRocketGunAmmo) > 0);
      case PAC:
         return true;
      case PlasmaCannon:
         return true;
      case LaserCannonGun:
         return true;
      case PhaserCannon:
         return true;
      case SpikeRifle:
         return(%this.getInventory(SpikeAmmo) > 0);
      case MegaBlasterCannon:
         return true;
//      case BlasterBarrelGun:
//         return true;
//      case ChainBarrelGun:
//         return true;
//      case PhaserBarrelGun:
//         return true;
      default:
         return true;
         
//         warn("\"" @ %weapon @ "\" has not been added to the cycle list!");
//         return false;
   }
}

function ShapeBase::clearInventory(%this)
{
   %this.setInventory(RepairKit,0);

   %this.setInventory(Mine,0);
   %this.setInventory(LargeMine,0);
   %this.setInventory(CloakMine,0);
   %this.setInventory(EMPMine,0);
   %this.setInventory(IncendiaryMine,0);
   %this.setInventory(BlastechMine,0);		// +soph
   %this.setInventory(Grenade,0);
   %this.setInventory(FlashGrenade,0);
   %this.setInventory(ConcussionGrenade,0);
   %this.setInventory(FlareGrenade,0);
   %this.setInventory(MortarGrenade, 0);
   %this.setInventory(EMPGrenade, 0);
   %this.setInventory(PoisonGrenade, 0);
   %this.setInventory(FireGrenade, 0);
   %this.setInventory(CameraGrenade,0);		// +soph

   %this.setInventory(Blaster,0);
   %this.setInventory(Plasma,0);
   %this.setInventory(Disc,0);
   %this.setInventory(Chaingun, 0);
   %this.setInventory(Mortar, 0);
   %this.setInventory(GrenadeLauncher, 0);
   %this.setInventory(MissileLauncher, 0);
   %this.setInventory(SniperRifle, 0);
   %this.setInventory(TargetingLaser, 0);
   %this.setInventory(ELFGun, 0);
   %this.setInventory(ShockLance, 0);

   %this.setInventory(EnergyRifle, 0);
   %this.setInventory(EnergyRifleAmmo, 0);
   %this.setInventory(LaserCannonGun, 0);
   %this.setInventory(Protron, 0);
   %this.setInventory(ProtronAmmo, 0);
   %this.setInventory(MBCannon, 0);
   %this.setInventory(MBCannonCapacitor, 0);
   %this.setInventory(RFL, 0);
   %this.setInventory(RFLAmmo, 0);   
   %this.setInventory(MegaBlasterCannon, 0);
   %this.setInventory(PCC, 0);
   %this.setInventory(PCCAmmo, 0);
   %this.setInventory(SpikeRifle, 0);   
   %this.setInventory(SpikeAmmo, 0);      
   %this.setInventory(PCR, 0);
   %this.setInventory(PCRAmmo, 0);
   %this.setInventory(PBW, 0);
   %this.setInventory(PBWAmmo, 0);
   %this.setInventory(RAXX, 0);
   %this.setInventory(RAXXAmmo, 0);
   %this.setInventory(Sniper3006Ammo, 0);
//   %this.setInventory(MagMissileLauncher, 0);
   %this.setInventory(Starburst, 0);
   %this.setInventory(Multifusor, 0);
   %this.setInventory(EcstacyCannon, 0);
   %this.setInventory(MechMinigun, 0);
   %this.setInventory(MechMinigunAmmo, 0);
   %this.setInventory(MechRocketGun, 0);
   %this.setInventory(MechRocketGunAmmo, 0);
   %this.setInventory(PlasmaCannon, 0);
   %this.setInventory(MissileBarrelGun, 0);
   %this.setInventory(StarHammerAmmo, 0);
   %this.setInventory(AutoCannon, 0);
   %this.setInventory(AutoCannonAmmo, 0);
   %this.setInventory(SRM4Ammo, 0);
   
   %this.setInventory(PlasmaAmmo,0);
   %this.setInventory(ChaingunAmmo, 0);
   %this.setInventory(DiscAmmo, 0);
   %this.setInventory(GrenadeLauncherAmmo, 0);
   %this.setInventory(MissileLauncherAmmo, 0);
   %this.setInventory(MortarAmmo, 0);
   %this.setInventory(Beacon, 0);

   %this.setInventory(MagicMissile, 0);
   %this.setInventory(MitziDeathRay, 0);
   %this.setInventory(SoulShocker, 0);
   %this.setInventory(SoulComet, 0);
   %this.setInventory(SoulDecay, 0);
   %this.setInventory(SoulHammer, 0);
   %this.setInventory(SoulStrike, 0);

   // take away any pack the player has
   %curPack = %this.getMountedImage($BackpackSlot);
   if(%curPack > 0)
      %this.setInventory(%curPack.item, 0);

   %curMod = %this.currentArmorMod;

   if(%this.getInventory(%curMod) > 0)
      %this.setInventory(%curMod, 0);
   if( %this.getInventory( SoulShieldMod ) > 0 )	// cleanup stray soul shields +soph
      %this.setInventory( SoulShieldMod , 0 );		// +soph
}

function splitFromTop()
{      
   // and any armor mods
   %this.setInventory(NoneArmorMod, 0);
   %this.setInventory(GravArmorPlatePack, 0);
   %this.setInventory(PolarArmorPlatePack, 0);
   %this.setInventory(MortarAdaptorMod, 0);
   %this.setInventory(DaishiPowerMod, 0);
   %this.setInventory(AmpMod, 0);
   %this.setInventory(ShockExtendMod, 0);
   %this.setInventory(PowerRecircMod, 0);
   %this.setInventory(InertialDampenerPlating, 0);
   %this.setInventory(ElectronFieldPlating, 0);
   %this.setInventory(ReactiveArmorPlating, 0);
   %this.setInventory(MagPackClampMod, 0);
   %this.setInventory(SoulShieldMod, 0);
   %this.setInventory(SubspaceRegenMod, 0);
   %this.setInventory(ThermalArmorPlating, 0);

   %this.setInventory(BAOverdriveMod, 0);
   %this.setInventory(ShieldHardenerMod, 0);
   %this.setInventory(BASSMod, 0);
   %this.setInventory(JetfireMod, 0);
   %this.setInventory(AmmoHarvesterMod, 0);
   %this.setInventory(FinalStandMod, 0);
   %this.setInventory(ProjectileAccelMod, 0);
   %this.setInventory(ShieldPulseMod, 0);

   %this.setInventory(EfficiencyMod, 0);
   %this.setInventory(PowerMod, 0);
   %this.setInventory(GuardianMod, 0);
}

// Inv Hud Data

//==============================================================================
// Armors

$InvArmor[0] = "Magnetic Ion";
$NameToInv["Magnetic Ion"]  = "MagIon";
$InvArmor[1] = "Scout";
$NameToInv["Scout"]  = "Light";
$InvArmor[2] = "Field Engineer";
$NameToInv["Field Engineer"]  = "Engineer";
$InvArmor[3] = "Blastech";
$NameToInv["Blastech"] = "Blastech";
$InvArmor[4] = "Assault";
$NameToInv["Assault"] = "Medium";
$InvArmor[5] = "Juggernaut";
$NameToInv["Juggernaut"]  = "Heavy";
$InvArmor[6] = "Battle Angel";
$NameToInv["Battle Angel"]  = "BattleAngel";

//==============================================================================
// Weapons

$InvWeapon[0] = "AutoCannon";
$NameToInv["AutoCannon"] = "AutoCannon";
$InvWeapon[1] = "Blaster";
$NameToInv["Blaster"] = "Blaster";
$InvWeapon[2] = "Chaingun";
$NameToInv["Chaingun"] = "Chaingun";
$InvWeapon[3] = "ELF Projector";
$NameToInv["ELF Projector"] = "ELFGun";
$InvWeapon[4] = "E.P.C.";			// $InvWeapon[4] = "Energy Projector Cannon"; -soph
$NameToInv["E.P.C."] = "EcstacyCannon";		// $NameToInv["Energy Projector Cannon"] = "EcstacyCannon"; -soph
$InvWeapon[5] = "Energy Rifle";
$NameToInv["Energy Rifle"] = "EnergyRifle";
$InvWeapon[6] = "Fusion Mortar";
$NameToInv["Fusion Mortar"] = "Mortar";
$InvWeapon[7] = "Grenade Launcher";
$NameToInv["Grenade Launcher"] = "GrenadeLauncher";
$InvWeapon[8] = "Heavy Plasma";			// $InvWeapon[8] = "Heavy Plasma Cannon"; -soph
$NameToInv["Heavy Plasma"] = "PlasmaCannon";	// $NameToInv["Heavy Plasma Cannon"] = "PlasmaCannon";	-soph
$InvWeapon[9] = "KM Blaster Rifle";
$NameToInv["KM Blaster Rifle"] = "LaserCannonGun";
$InvWeapon[10] = "Laser Assault Rifle";
$NameToInv["Laser Assault Rifle"] = "RFL";
$InvWeapon[11] = "Laser/Sniper Rifle";
$NameToInv["Laser/Sniper Rifle"] = "SniperRifle";
$InvWeapon[12] = "Mech Minigun";
$NameToInv["Mech Minigun"] = "MechMinigun";
$InvWeapon[13] = "Mech Rocket Launcher";
$NameToInv["Mech Rocket Launcher"] = "MechRocketGun";
$InvWeapon[14] = "Mega Blaster";
$NameToInv["Mega Blaster"] = "MegaBlasterCannon";
$InvWeapon[15] = "Mini Missile Launcher";
$NameToInv["Mini Missile Launcher"] = "MagMissileLauncher";
$InvWeapon[16] = "Missile Launcher";
$NameToInv["Missile Launcher"] = "MissileLauncher";
$InvWeapon[17] = "Mitzi Blast Cannon";
$NameToInv["Mitzi Blast Cannon"] = "MBCannon";
$InvWeapon[18] = "Multifusor";
$NameToInv["Multifusor"] = "Multifusor";
$InvWeapon[19] = "PBW";
$NameToInv["PBW"] = "PBW";
$InvWeapon[20] = "Phaser Rifle";
$NameToInv["Phaser Rifle"] = "PCR";
$InvWeapon[21] = "Spike Rifle";
$NameToInv["Spike Rifle"] = "SpikeRifle";
$InvWeapon[22] = "Plasma Rifle";
$InvWeapon[23] = "Protron";
$NameToInv["Protron"] = "Protron";
$InvWeapon[24] = "RAXX Flamer";
$NameToInv["RAXX Flamer"] = "RAXX";
$InvWeapon[25] = "Shocklance";
$InvWeapon[26] = "Spinfusor";
$InvWeapon[27] = "Starburst Cannon";
$NameToInv["Starburst Cannon"] = "Starburst";

$InvWeapon[28] = "Placeholder 1";
$InvWeapon[29] = "Placeholder 2";
$InvWeapon[30] = "Placeholder 3";

$InvWeapon[31] = "Mitzi Blaster Rifle";
$NameToInv["Mitzi Blaster Rifle"] = "MitziDeathRay";
$InvWeapon[32] = "Soul Comet Cannon";
$NameToInv["Soul Comet Cannon"] = "SoulComet";
$InvWeapon[33] = "Soul Hammer Cannon";
$NameToInv["Soul Hammer Cannon"] = "SoulHammer";
$InvWeapon[34] = "Soul Missile Launcher";
$NameToInv["Soul Missile Launcher"] = "MagicMissile";
$InvWeapon[35] = "Soul Shocker";
$NameToInv["Soul Shocker"] = "SoulShocker";
$InvWeapon[36] = "Soul Strike";			// $InvWeapon[37] = "Soul Swarmer"; -soph
$NameToInv["Soul Strike"] = "SoulStrike";	// $NameToInv["Soul Swarmer"] = "SoulStrike"; -soph


//==============================================================================
// Packs

$InvPack[0] = "Ammunition Pack";
$InvPack[1] = "Cloak Pack";
$InvPack[2] = "Energy Pack";
$InvPack[3] = "Turbocharger";
$NameToInv["Turbocharger"] = "HeatShieldPack";
$InvPack[4] = "Portal Generator";
$NameToInv["Portal Generator"] = "SlipstreamPack";
$InvPack[5] = "Repair Pack";
$InvPack[6] = "Sensor Jammer Pack";
$InvPack[7] = "Shield Pack";
$InvPack[8] = "Synomium Device";
$NameToInv["Synomium Device"] = "SynomiumPack";
$InvPack[9] = "VectorPort";
$NameToInv["VectorPort"] = "VectorPortPack";

$InvPack[10] = "AutoCannon Burst";
$NameToInv["AutoCannon Burst"] = "AutoCCannonPack";
$InvPack[11] = "Comet Cannon";
$NameToInv["Comet Cannon"] = "CometCannonPack";
$InvPack[12] = "Disc Cannon";
$NameToInv["Disc Cannon"] = "DiscCannonPack";
$InvPack[13] = "Dual Grenade Cannons";
$NameToInv["Dual Grenade Cannons"] = "DualGrenadeCannonPack";
$InvPack[14] = "Dual Pulse Cannons";
$NameToInv["Dual Pulse Cannons"] = "PulseCannonPack";
$InvPack[15] = "Grenade Cannon";
$NameToInv["Grenade Cannon"] = "GrenadeCannonPack";
$InvPack[16] = "Mitzi Light Cannon";
$NameToInv["Mitzi Light Cannon"] = "BlasterCannonPack";
$InvPack[17] = "Satchel Charge";
$InvPack[18] = "Phaser Blast Cannon";
$NameToInv["Phaser Blast Cannon"] = "ShockBlasterPack";
$InvPack[19] = "Shotgun Cannon";
$NameToInv["Shotgun Cannon"] = "ShotgunCannonPack";
$InvPack[20] = "Star Hammer";
$NameToInv["Star Hammer"] = "StarHammerPack";
$InvPack[21] = "Streak SRM4 Mount";
$NameToInv["Streak SRM4 Mount"] = "SRM4Pack";
$InvPack[22] = "Streak SRM2 Mount";		// i'm stealing the fuck out of the shield beacons
$NameToInv["Streak SRM2 Mount"] = "SRM2Pack";	// +soph
$InvPack[23] = "Streak SRM1 Mount";		// also nukes
$NameToInv["Streak SRM1 Mount"] = "SRM1Pack";	// +soph

$InvPack[24] = "Base Turret";
$NameToInv["Base Turret"] = "TurretBasePack";
$InvPack[25] = "Blast Door";
$NameToInv["Blast Door"] = "BlastDoorPack";
$InvPack[26] = "Blast Wall";
$NameToInv["Blast Wall"] = "BlastWallPack";
$InvPack[27] = "Defense+ Device";
$NameToInv["Defense+ Device"] = "DefEnhancePack";
$InvPack[28] = "Forcefield Array";
$NameToInv["Forcefield Array"] = "ShieldGeneratorPack";
$InvPack[29] = "Inventory Station";
$InvPack[30] = "Jammer Beacon";
$NameToInv["Jammer Beacon"] = "JammerBeaconPack";
$InvPack[31] = "Landspike Turret";
$InvPack[32] = "Large Inv Station";
$NameToInv["Large Inv Station"] = "DeployableInvHumanPack";
$InvPack[33] = "MANTA Ion Battery";
$NameToInv["MANTA Ion Battery"] = "MANTACapacitorPack";
$InvPack[34] = "MANTA Satellite";
$NameToInv["MANTA Satellite"] = "MantaSatPack";
$InvPack[35] = "Motion Sensor Pack";
$InvPack[36] = "RMS Silo";
$NameToInv["RMS Silo"] = "RMSSpack";
$InvPack[37] = "Pulse Sensor Pack";
$InvPack[38] = "Remote Blastech Bunker";
$NameToInv["Remote Blastech Bunker"] = "BorgCubePack";
$InvPack[39] = "Remote Defense Base";
$NameToInv["Remote Defense Base"] = "DefenseBasePack";
$InvPack[40] = "Remote Vehicle Pad";
$NameToInv["Remote Vehicle Pad"] = "VehiclePadPack";
$InvPack[41] = "Spider Clamp Turret";
$InvPack[42] = "Teleporter Pad";
$NameToInv["Teleporter Pad"] = "TelePack";
$InvPack[43] = "Weapon Repulsor";
$NameToInv["Weapon Repulsor"] = "RepulsorBeaconPack";
$InvPack[44] = "Sentry Turret";
$NameToInv["Sentry Turret"] = "DeployableSentryPack";
$InvPack[45] = "MANTA/RMS Tag Pack" ;				// = "Ion Tag Pack"; -soph
$NameToInv[ "MANTA/RMS Tag Pack" ] = "IonTAGPack" ;		// ["Ion Tag Pack"] = "IonTAGPack"; -soph


// Grenades
//

$InvGrenade[0] = "Std Frag";
$NameToInv["Std Frag"] = "Grenade";
$InvGrenade[1] = "Mortar";
$NameToInv["Mortar"] = "MortarGrenade";
$InvGrenade[2] = "Poison Gas";
$NameToInv["Poison Gas"] = "PoisonGrenade";
$InvGrenade[3] = "ElectroMag";
$NameToInv["ElectroMag"] = "EMPGrenade";
$InvGrenade[4] = "Whiteout";
$NameToInv["Whiteout"] = "FlashGrenade";
$InvGrenade[5] = "Concussion";
$NameToInv["Concussion"] = "ConcussionGrenade";
$InvGrenade[6] = "Incendiary";
$NameToInv["Incendiary"] = "FireGrenade";
$InvGrenade[7] = "Camera";
$NameToInv["Camera"] = "CameraGrenade";
$InvGrenade[8] = "Flare";
$NameToInv["Flare"] = "FlareGrenade";

// Mines
//

$InvMine[0] = "Mine";
$NameToInv["Mine"] = "Mine";
$InvMine[1] = "Large Mine";
$NameToInv["Large Mine"] = "LargeMine";
$InvMine[2] = "Cloak Mine";
$NameToInv["Cloak Mine"] = "CloakMine";
$InvMine[3] = "EMP Mine";
$NameToInv["EMP Mine"] = "EMPMine";
$InvMine[4] = "Incendiary Mine";
$NameToInv["Incendiary Mine"] = "IncendiaryMine";
$InvMine[5] = "Anti-Blastech Mine";
$NameToInv["Anti-Blastech Mine"] = "BlastechMine";

// Armor modules
//
$InvArmorMod[0] = "Dual Mortar Fitting";
$NameToInv["Dual Mortar Fitting"] = "MortarAdaptorMod";
$InvArmorMod[1] = "D+D Power Link Module";
$NameToInv["D+D Power Link Module"] = "DaishiPowerMod";
$InvArmorMod[2] = "Stabilizer Module" ;					// +soph
$NameToInv[ "Stabilizer Module" ] = "MagHandClampMod";			// +soph
$InvArmorMod[3] = "Power Recirculator Module";
$NameToInv["Power Recirculator Module"] = "PowerRecircMod";
$InvArmorMod[4] = "Shocklance Extender Module";
$NameToInv["Shocklance Extender Module"] = "ShockExtendMod";
$InvArmorMod[5] = "Light Armor Plating";
$NameToInv["Light Armor Plating"] = "GravArmorPlatePack";
$InvArmorMod[6] = "Heavy Armor Plating";
$NameToInv["Heavy Armor Plating"] = "PolarArmorPlatePack";
$InvArmorMod[ 7 ] = "Hardened Armor Plating" ;				// +[soph]
$NameToInv[ "Hardened Armor Plating" ] = "ReactiveArmorPlating" ;	// +
$InvArmorMod[ 8 ] = "Refractive Armor Plating" ;			// +
$NameToInv[ "Refractive Armor Plating" ] = "ElectronFieldPlating" ;	// +
$InvArmorMod[ 9 ] = "Deflective Armor Plating" ;			// +
$NameToInv[ "Deflective Armor Plating" ] = "InertialDampenerPlating" ;	// +
$InvArmorMod[ 10 ] = "Insulated Armor Plating" ;			// +
$NameToInv[ "Insulated Armor Plating" ] = "ThermalArmorPlating" ;	// +[/soph]
$InvArmorMod[11] = "Damage Amplifier Module";
$NameToInv["Damage Amplifier Module"] = "AmpMod";
$InvArmorMod[12] = "Soul Shield";
$NameToInv["Soul Shield"] = "SoulShieldMod";
$InvArmorMod[13] = "Subspace Regenerator";
$NameToInv["Subspace Regenerator"] = "SubspaceRegenMod";
$InvArmorMod[14] = "[A] D+D Overdrive Mod";
$NameToInv["[A] D+D Overdrive Mod"] = "BAOverdriveMod";
$InvArmorMod[15] = "Shield Hardener";
$NameToInv["Shield Hardener"] = "ShieldHardenerMod";
$InvArmorMod[16] = "D+D BASS Shield Extender";
$NameToInv["D+D BASS Shield Extender"] = "BASSMod";
$InvArmorMod[17] = "[A] Jetfire Transformer";
$NameToInv["[A] Jetfire Transformer"] = "JetfireMod";
$InvArmorMod[18] = "D+D Final Stand";
$NameToInv["D+D Final Stand"] = "FinalStandMod";
$InvArmorMod[19] = "[A] Shield Pulse Activator";
$NameToInv["[A] Shield Pulse Activator"] = "ShieldPulseMod";

// Mage Ion armor modules
//
$InvArmorMod[20] = "Efficiency";
$NameToInv["Efficiency"] = "EfficiencyMod";
$InvArmorMod[21] = "Power";
$NameToInv["Power"] = "PowerMod";
$InvArmorMod[22] = "Guardian";
$NameToInv["Guardian"] = "GuardianMod";
$InvArmorMod[23] = "Archmage";
$NameToInv["Archmage"] = "ArchMageMod";

$MDInvToName["Light"] = "Scout Armor";
$MDInvToName["Medium"] = "Assault Armor";
$MDInvToName["Heavy"] = "Juggernaut Armor";
$MDInvToName["MagIon"] = "Magnetic Ion Armor";
$MDInvToName["Engineer"] = "Field Engineer Armor";
$MDInvToName["BattleAngel"] = "Battle Angel Armor";
$MDInvToName["Blastech"] = "Blastech Armor";

$ArmorNote["Light"] = "The Scout can use the Sniper Rifle and a number of exotic backpack devices.";	// "The Scout has exclusive access to Cloaking, Portal, and VectorPort packs."; -soph
$ArmorNote["Medium"] = "The all-arounder; firepower, mobility, and a built-in personal repulsor device.";	// "All around good, the Assault and it's repulsor are deadly in professional hands."; -soph
$ArmorNote["Heavy"] = "Capable of surprising feats of speed, bringing the hurt wherever it is needed.";	// "The lightest heavy-frame armor, the Jugg travels fast with firepower to match."; -soph
$ArmorNote["MagIon"] = "A unique fusion of Psionics and Technology that can tap into abilities unknown.";
$ArmorNote["Engineer"] = "The Engineer carries all deployables in the game, and comes with a repair gun.";
$ArmorNote["BattleAngel"] = "Slow and armed to the teeth, this angel leaves destruction in its wake."; 	// can swoop down with retribution."; -soph
$ArmorNote["Blastech"] = "Shielded against explosives, powered by a heavy battery, but no generator.";	// "100% explosive resistant with a capacitor instead of a generator."; -soph

function displayArmorStatistics(%client, %data)
{
     %urf = %client.player.universalResistFactor;
     
     if(%urf > 1)
          %colorFlag = "<color:FF0000>";
     else if(%urf < 1)
          %colorFlag = "<color:00FF00>";
     else
          %colorFlag = "<color:FFFFFF>";

     bottomPrint(%client, $MDInvToName[%client.armor]@"\nArmor:"@%colorFlag SPC mFloor((%data.maxDamage / %urf) * 100) SPC "<color:42dbea>HP | Energy:<color:FFFFFF>" SPC %data.maxEnergy SPC "<color:42dbea>KW | Run Speed:" SPC mFloor(%data.maxForwardSpeed * 3.6) SPC "KPH\n"@$ArmorNote[%client.armor], 30, 3);
}

function buyFavorites( %client , %healBool )
{
   if(%client.player.overdriveMode)
       return;
       
   %client.player.battleangel = false;
   %client.player.nextOverdriveTime = 0;
   %client.player.invPackBlockTime = getSimTime() + 2000;
   
   if(%client.player.desigThread)
   {
       cancel(%client.player.desigThread);
       %client.player.desigTag = false;
   }

   if(%client.player.DESetMember)
   {
          %client.player.DESetSet.remove(%client.player);
          %client.player.inDEDField = false;
          %client.player.DESetSet = "";
          %client.player.DESetMember = "";
   }

   // don't forget -- for many functions, anything done here also needs to be done
   // below in buyDeployableFavorites !!!
   %client.player.clearInventory();
   %client.setWeaponsHudClearAll();
   %cmt = $CurrentMissionType;

   %curArmor = %client.player.getDatablock();
   %curDmgPct = getDamagePercent(%curArmor.maxDamage, %client.player.getDamageLevel());	// uncommented -soph

   // armor
   %client.armor = $NameToInv[%client.favorites[0]];
   %client.player.setArmor(%client.armor);

   addStatTrack(0, %client.armor@"InventoryUsage", 1);
   addStatTrack(%client.guid, %client.armor@"TimesBought", 1);
   
   %newArmor = %client.player.getDataBlock();

   toggleMageIon(%client, %client.player, %newArmor);

   if(%newArmor.armorMask & $ArmorMask::MagIon)
     calculateSoulBonuses(%client.player, %client.player.soulCount);
   
   schedule(32, %client.player, displayArmorStatistics, %client, %newArmor);

   if(%newArmor.armorMask & $ArmorMask::BattleAngel | $ArmorMask::Heavy)
   {
      %client.player.battleangel = true;
      ejectFlag(%client.player);
   }

   %client.player.universalResistFactor = 1.0;
   
   if(%newArmor.armorMask & $ArmorMask::Blastech)
   {
        schedule(32, %client.player, BlastechRecharge, %client.player); 
        %client.player.dynamicResistance[$DamageGroupMask::Misc] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Energy] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Explosive] = 0.0;
        %client.player.dynamicResistance[$DamageGroupMask::Kinetic] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Plasma] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Mitzi] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Poison] = 1.0;   
   }
   else
   {
        %client.player.dynamicResistance[$DamageGroupMask::Misc] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Energy] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Explosive] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Kinetic] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Plasma] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Mitzi] = 1.0;
        %client.player.dynamicResistance[$DamageGroupMask::Poison] = 1.0;   
   }

   if( !%healBool )
      %client.player.setDamageLevel(%curDmgPct * %newArmor.maxDamage);	// uncommented -soph
   else
      %client.player.setDamageLevel(0);
   %client.player.setRechargeRate(%newArmor.rechargeRate);

   if(!%client.player.charging)
   {
        %client.player.charging = true; //case MB
        ChargeMBC(%client.player, %client.player.getDatablock());
   }

   if(!%client.player.chargingProt)
   {
        %client.player.chargingProt = true; //case Protron/blaster
        ChargeProtron(%client.player, %client.player.getDatablock());
   }

   if(!%client.player.chargingPCR)
   {
        %client.player.chargingPCR = true; //case Phaser
        ChargePhaser(%client.player, %client.player.getDatablock());
   }

   if(!%client.player.chargingPCC)
   {
        %client.player.chargingPCC = true; //case Phaser
        ChargePhaserC(%client.player, %client.player.getDatablock());
   }
   
   if(!%client.player.chargingRFL)
   {
        %client.player.chargingRFL = true; //case RFL
        ChargeRFL(%client.player);
   }

   schedule(0, 0, useEnergy, %client.player, -%newArmor.maxEnergy); // why I had to do this? T2 is w3rd... 8(

   // weapons
   %data = %client.player.getDatablock();	// +soph
   for(%i = 0; %i < getFieldCount( %client.weaponIndex ); %i++)
   {
      %inv = $NameToInv[%client.favorites[getField( %client.weaponIndex, %i )]];

      if( %inv !$= "" )
      {
         %weaponCount++;
         %client.player.setInventory( %inv, 1 );
      }
      // z0dd - ZOD, 9/13/02. Streamlining.
      if ( %inv.image.ammo !$= "" )
         if( %data.isEngineer )									// +[soph]
         {										  	// + engineer exception
            %packmax = AmmoPack.max[%inv.image.ammo];						// +
            if( %packmax < 20 )									// +
               %packmax = 2;									// +
            else if( %packmax < 50 )								// +
               %packmax = 10;									// +
            else if( %packmax < 100 )								// +
               %packmax = 25;									// +
            else										// +
               %packmax = 50;									// +
            %client.player.setInventory( %inv.image.ammo, %data.max[%inv.image.ammo] + %packmax );
         }											// +
         else											// +[/soph]
            %client.player.setInventory( %inv.image.ammo, 5000 );

      if(%inv $= "MBCannon") // hack - would give 2000 ammo to MB :o lol
         %client.player.setInventory( MBCannonCapacitor, 50 );

      if(%inv $= "Protron") // hack -
//         %client.player.setInventory( ProtronAmmo, mFloor(%client.player.getDatablock().max[ProtronAmmo]*0.5) ); 	// -soph //-Nite- Something different - MrKeen: changed to multiplication to prevent divide by 0
         %client.player.setInventory( ProtronAmmo , mFloor( %client.player.getInventory( ProtronAmmo ) / 2 ) ) ;	// +soph
      if(%inv $= "PCR") // hack -
//         %client.player.setInventory( PCRAmmo, mFloor(%client.player.getDatablock().max[PCRAmmo]*0.5) );		// -soph
         %client.player.setInventory( PCRAmmo , mFloor( %client.player.getInventory( PCRAmmo ) / 2 ) ) ;		// +soph

      if(%inv $= "PCC") // hack -
//         %client.player.setInventory( PCRAmmo, mFloor(%client.player.getDatablock().max[PCCAmmo]*0.5) );		// -soph
         %client.player.setInventory( PCCAmmo , mFloor( %client.player.getInventory( PCRAmmo ) / 2 ) ) ;		// +soph

      if(%inv $= "RFL") // hack -
//         %client.player.setInventory( RFLAmmo, mFloor(%client.player.getDatablock().max[RFLAmmo]*0.5) );		// -soph
         %client.player.setInventory( RFLAmmo , mFloor( %client.player.getInventory( RFLAmmo ) / 2 ) ) ;		// +soph
         
   }
   %client.player.weaponCount = %weaponCount;

   // pack
   %pCh = $NameToInv[%client.favorites[%client.packIndex]];
   if ( %pCh $= "" )
      %client.clearBackpackIcon();
   else
      %client.player.setInventory( %pCh, 1 );

//   addStatTrack(%client.guid, "InventoryUsage", 1);
//   addStatTrack(0, "InventoryUsage", 1);

   // keen: support for pack ammo
   if(%pCh.image.ammo !$= "")
         %client.player.setInventory(%pCh.image.ammo, 5000);
         
   // if this pack is a deployable that has a team limit, warn the purchaser
	// if it's a deployable turret, the limit depends on the number of players (deployables.cs)
	if(%pCh $= "TurretIndoorDeployable" || %pCh $= "TurretOutdoorDeployable")
		%maxDep = countTurretsAllowed(%pCh);
	else
	   %maxDep = $TeamDeployableMax[%pCh];

   if(%maxDep !$= "")
   {
      %depSoFar = $TeamDeployedCount[%client.player.team, %pCh];
      %packName = %client.favorites[%client.packIndex];

      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep SPC %packName@"s deployed.";
      else
         %msTxt = "You have deployed "@%depSoFar@" of "@%maxDep SPC %packName@"s.";

      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   // module
   %amCh = $NameToInv[%client.favorites[%client.armorModIndex]];

   if(%amCh !$= "")
   {
      %client.player.setInventory(%amCh, 1);
      %client.player.currentArmorMod = %amCh;
   }

   // grenades
   for ( %i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++ )
   {
      %grenName = $NameToInv[%client.favorites[getField( %client.grenadeIndex,%i )]];
      %client.player.setInventory( %grenName, 30 );
   }

   // if player is buying cameras, show how many are already deployed
   if(%client.favorites[%client.grenadeIndex] $= "Deployable Camera")
   {
      %maxDep = $TeamDeployableMax[DeployedCamera];
      %depSoFar = $TeamDeployedCount[%client.player.team, DeployedCamera];
      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep@" Deployable Cameras placed.";
      else
         %msTxt = "You have placed "@%depSoFar@" of "@%maxDep@" Deployable Cameras.";
      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   // mines
   // -----------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/8/02. Old code did not check to see if mines are banned, fixed.
   //for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   //   %client.player.setInventory( $NameToInv[%client.favorites[getField( %client.mineIndex,%i )]], 30 );

   for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   {
      %mineName = $NameToInv[%client.favorites[getField( %client.mineIndex, %i )]];
      if ( !($InvBanList[%cmt, %mineName]) )
      {
        %client.player.setInventory( %mineName , 30 );
      }
   }
   // End z0dd - ZOD
   // -----------------------------------------------------------------------------------------------------

   // miscellaneous stuff -- Repair Kit, Beacons, Targeting Laser
   if ( !($InvBanList[%cmt, RepairKit]) )
      %client.player.setInventory( RepairKit, 10 );
   if ( !($InvBanList[%cmt, Beacon]) )
      %client.player.setInventory( Beacon, 1000 );
   if ( !($InvBanList[%cmt, TargetingLaser]) )
      %client.player.setInventory( TargetingLaser, 1 );

   // ammo pack pass -- hack! hack!
   if( %pCh $= "AmmoPack" )
      invAmmoPackPass(%client);
}

function buyDeployableFavorites(%client)
{
   if(%client.player.overdriveMode)
       return;
       
   %player = %client.player;
	%prevPack = %player.getMountedImage($BackpackSlot);
   %player.clearInventory();
   %client.setWeaponsHudClearAll();
   %cmt = $CurrentMissionType;

   if(!%client.player.charging)
   {
        %client.player.charging = true; //case MB
        ChargeMBC(%client.player, %client.player.getDatablock());
   }

   if(!%client.player.chargingProt)
   {
        %client.player.chargingProt = true; //case Protron
        ChargeProtron(%client.player, %client.player.getDatablock());
   }

   if(!%client.player.chargingPCR)
   {
        %client.player.chargingPCR = true; //case Phaser
        ChargePhaser(%client.player);
   }

   if(!%client.player.chargingPCC)
   {
        %client.player.chargingPCC = true; //case Phaser
        ChargePhaserC(%client.player);
   }

   if(!%client.player.chargingRFL)
   {
        %client.player.chargingRFL = true; //case Phaser
        ChargeRFL(%client.player);
   }
   
   schedule(0, 0, useEnergy, %client.player, -%client.player.getDatablock().maxEnergy);

  // players cannot buy armor from deployable inventory stations
   %weapCount = 0;
   %data = %client.player.getDatablock();	// +soph
   for ( %i = 0; %i < getFieldCount( %client.weaponIndex ); %i++ )
   {
      %inv = $NameToInv[%client.favorites[getField( %client.weaponIndex, %i )]];
      if ( !($InvBanList[DeployInv, %inv]) )
      {
         %player.setInventory( %inv, 1 );
         // increment weapon count if current armor can hold this weapon
         if(%player.getDatablock().max[%inv] > 0)
            %weapCount++;

         // z0dd - ZOD, 9/13/02. Streamlining
         if ( %inv.image.ammo !$= "" )	// -soph
            if( %data.isEngineer )								// +[soph]
            {										  	// + engineer exception
               %packmax = AmmoPack.max[%inv.image.ammo];				  	// +
               if( %packmax < 20 )							 	// +
                  %packmax = 2;								  	// +
               else if( %packmax < 50 )							 	// +
                  %packmax = 10;							 	// +
               else if( %packmax < 100 )						  	// +
                  %packmax = 25;								// +
               else								  		// +
                  %packmax = 50;								// +
               %player.setInventory( %inv.image.ammo, %data.max[%inv.image.ammo] + %packmax );	// +
            }								  			// +
            else										// +[/soph]
               %player.setInventory( %inv.image.ammo, 2500 );

         if(%inv $= "MBCannon") // hack - would give 2000 ammo to MB :o lol
            %client.player.setInventory( MBCannonCapacitor, 50 );

         if(%inv $= "Protron")
//            %client.player.setInventory( ProtronAmmo, mFloor(%client.player.getDatablock().max[ProtronAmmo] * 0.5) );	// -soph  //-Nite- Something different
            %client.player.setInventory( ProtronAmmo , mFloor( %client.player.getInventory( ProtronAmmo ) / 2 ) ) ;	// +soph

         if(%inv $= "PCR")
//            %client.player.setInventory( PCRAmmo, mFloor(%client.player.getDatablock().max[PCRAmmo] * 0.5) );	// -soph  //-Nite- Something different
            %client.player.setInventory( PCRAmmo , mFloor( %client.player.getInventory( PCRAmmo ) / 2 ) ) ;	// +soph

         if(%inv $= "PCC")
//            %client.player.setInventory( PCRAmmo, mFloor(%client.player.getDatablock().max[PCCAmmo] * 0.5) );	// -soph  //-Nite- Something different
            %client.player.setInventory( PCCAmmo , mFloor( %client.player.getInventory( PCCAmmo ) / 2 ) ) ;	// +soph

         if(%inv $= "RFL")
            %client.player.setInventory( RFLAmmo, mFloor(%client.player.getDatablock().max[RFLAmmo] * 0.5) );	// -soph  //-Nite- Something different
            %client.player.setInventory( RFLAmmo , mFloor( %client.player.getInventory( RFLAmmo ) / 2 ) ) ;	// +soph

         if(%weapCount >= %player.getDatablock().maxWeapons)
            break;
      }
   }
   %player.weaponCount = %weapCount;

   // give player the grenades and mines they chose, beacons, and a repair kit
   for ( %i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++)
   {
      %GInv = $NameToInv[%client.favorites[getField( %client.grenadeIndex, %i )]];
      if ( !($InvBanList[DeployInv, %GInv]) )
         %player.setInventory( %GInv, 30 );
   }

   // if player is buying cameras, show how many are already deployed
   if(%client.favorites[%client.grenadeIndex] $= "Deployable Camera")
   {
      %maxDep = $TeamDeployableMax[DeployedCamera];
      %depSoFar = $TeamDeployedCount[%client.player.team, DeployedCamera];
      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep@" Deployable Cameras placed.";
      else
         %msTxt = "You have placed "@%depSoFar@" of "@%maxDep@" Deployable Cameras.";
      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   {
      %MInv = $NameToInv[%client.favorites[getField( %client.mineIndex, %i )]];
      if ( !($InvBanList[DeployInv, %MInv]) )
         %player.setInventory( %MInv, 30 );
   }
   if ( !($InvBanList[DeployInv, Beacon]) && !($InvBanList[%cmt, Beacon]) )
      %player.setInventory( Beacon, 1000 );
   if ( !($InvBanList[DeployInv, RepairKit]) && !($InvBanList[%cmt, RepairKit]) )
      %player.setInventory( RepairKit, 10 );
   if ( !($InvBanList[DeployInv, TargetingLaser]) && !($InvBanList[%cmt, TargetingLaser]) )
      %player.setInventory( TargetingLaser, 1 );

   // players cannot buy deployable station packs from a deployable inventory station
   %packChoice = $NameToInv[%client.favorites[%client.packIndex]];
   if ( !($InvBanList[DeployInv, %packChoice]) )
      %player.setInventory( %packChoice, 1 );

   if(%packChoice.image.ammo !$= "")
         %client.player.setInventory(%packChoice.image.ammo, 5000);

   // if this pack is a deployable that has a team limit, warn the purchaser
	// if it's a deployable turret, the limit depends on the number of players (deployables.cs)
	if(%packChoice $= "TurretIndoorDeployable" || %packChoice $= "TurretOutdoorDeployable")
		%maxDep = countTurretsAllowed(%packChoice);
	else
	   %maxDep = $TeamDeployableMax[%packChoice];
   if((%maxDep !$= "") && (%packChoice !$= "InventoryDeployable"))
   {
      %depSoFar = $TeamDeployedCount[%client.player.team, %packChoice];
      %packName = %client.favorites[%client.packIndex];

      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep SPC %packName@"s deployed.";
      else
         %msTxt = "You have deployed "@%depSoFar@" of "@%maxDep SPC %packName@"s.";

      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

	if(%prevPack > 0)
	{
		// if player had a "forbidden" pack (such as a deployable inventory station)
		// BEFORE visiting a deployed inventory station AND still has that pack chosen
		// as a favorite, give it back
		if((%packChoice $= %prevPack.item) && ($InvBanList[DeployInv, %packChoice]))
	      %player.setInventory( %prevPack.item, 1 );
	}

   if(%client.player.currentArmorMod !$= "")
      %client.player.setInventory(%client.player.currentArmorMod, 1);

   if(%packChoice $= "AmmoPack")
      invAmmoPackPass(%client);
}

$WeaponsHudCount = 32;

function weaponsHudMakeVisible()
{
     for(%idx = 0; %idx < $WeaponsHudCount; %idx++)
        $WeaponsHudData[%idx, visible] = true;
}

weaponsHudMakeVisible();

$WeaponsHudData[0, reticle] = "gui/ret_blaster";
$WeaponsHudData[1, reticle] = "gui/ret_plasma";
$WeaponsHudData[2, reticle] = "gui/ret_chaingun";
$WeaponsHudData[3, reticle] = "gui/ret_disc";
$WeaponsHudData[4, reticle] = "gui/ret_grenade";

$WeaponsHudData[5, ammoDataName] = "Sniper3006Ammo";

$WeaponsHudData[6, reticle] = "gui/ret_elf";
$WeaponsHudData[7, reticle] = "gui/ret_mortor";
$WeaponsHudData[8, reticle] = "gui/ret_missile";

$WeaponsHudData[9, bitmapName]   = "gui/hud_blaster";
$WeaponsHudData[9, itemDataName] = "LaserCannonGun";
$WeaponsHudData[9, ammoDataName] = "";
$WeaponsHudData[9, reticle] = "gui/ret_blaster";

$WeaponsHudData[10, reticle] = "gui/hud_ret_shocklance";

$WeaponsHudData[11, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[11, itemDataName] = "EnergyRifle";
$WeaponsHudData[11, ammoDataName] = "EnergyRifleAmmo";
$WeaponsHudData[11, reticle] = "gui/ret_plasma";

$WeaponsHudData[12, bitmapName]   = "gui/hud_mortor";
$WeaponsHudData[12, itemDataName] = "MegaBlasterCannon";
$WeaponsHudData[12, ammoDataName] = "";
$WeaponsHudData[12, reticle] = "gui/ret_mortor";

$WeaponsHudData[13, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[13, itemDataName] = "Protron";
$WeaponsHudData[13, ammoDataName] = "ProtronAmmo";
$WeaponsHudData[13, reticle] = "gui/ret_blaster";

$WeaponsHudData[14, bitmapName]   = "gui/hud_elfgun";
$WeaponsHudData[14, itemDataName] = "MBCannon";
$WeaponsHudData[14, ammoDataName] = "MBCannonCapacitor";
$WeaponsHudData[14, reticle] = "gui/ret_mortor";

$WeaponsHudData[15, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[15, itemDataName] = "PCR";
$WeaponsHudData[15, ammoDataName] = "PCRAmmo";
$WeaponsHudData[15, reticle] = "gui/ret_elf";

$WeaponsHudData[16, bitmapName]   = "gui/hud_plasma";
$WeaponsHudData[16, itemDataName] = "SpikeRifle";
$WeaponsHudData[16, ammoDataName] = "SpikeAmmo";
$WeaponsHudData[16, reticle] = "gui/ret_grenade";

$WeaponsHudData[17, bitmapName]   = "gui/hud_sniper";
$WeaponsHudData[17, itemDataName] = "PBW";
$WeaponsHudData[17, ammoDataName] = "PBWAmmo";
$WeaponsHudData[17, reticle] = "gui/ret_grenade";

$WeaponsHudData[18, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[18, itemDataName] = "ChainBarrelGun";
$WeaponsHudData[18, ammoDataName] = "MechMinigunAmmo";
$WeaponsHudData[18, reticle] = "gui/ret_chaingun";

$WeaponsHudData[19, bitmapName]   = "gui/hud_plasma";
$WeaponsHudData[19, itemDataName] = "PlasmaCannon";
$WeaponsHudData[19, ammoDataName] = "";
$WeaponsHudData[19, reticle] = "gui/ret_blaster"; //hud_ret_sniper";

$WeaponsHudData[20, bitmapName]   = "gui/hud_new_packenergy";
$WeaponsHudData[20, itemDataName] = "PCC";
$WeaponsHudData[20, ammoDataName] = "PCCAmmo";
$WeaponsHudData[20, reticle] = "gui/ret_missile";

$WeaponsHudData[21, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[21, itemDataName] = "RFL";
$WeaponsHudData[21, ammoDataName] = "RFLAmmo";
$WeaponsHudData[21, reticle] = "gui/ret_blaster";

$WeaponsHudData[22, bitmapName]   = "gui/hud_missiles";
$WeaponsHudData[22, itemDataName] = "MagMissileLauncher";
$WeaponsHudData[22, reticle] = "gui/ret_missile";

$WeaponsHudData[23, bitmapName]   = "gui/hud_disc";
$WeaponsHudData[23, itemDataName] = "Multifusor";
$WeaponsHudData[23, ammoDataName] = "DiscAmmo";
$WeaponsHudData[23, reticle] = "gui/ret_disc";

$WeaponsHudData[24, bitmapName]   = "gui/hud_plasma";
$WeaponsHudData[24, itemDataName] = "AutoCannon";
$WeaponsHudData[24, ammoDataName] = "AutoCannonAmmo";
$WeaponsHudData[24, reticle] = "gui/ret_blaster";

$WeaponsHudData[25, bitmapName]   = "gui/hud_mortor";
$WeaponsHudData[25, itemDataName] = "Starburst";
$WeaponsHudData[25, ammoDataName] = "StarburstAmmo";
$WeaponsHudData[25, reticle] = "gui/ret_mortor";

$WeaponsHudData[26, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[26, itemDataName] = "RAXX";
$WeaponsHudData[26, ammoDataName] = "RAXXAmmo";
$WeaponsHudData[26, reticle] = "gui/ret_missile";

$WeaponsHudData[27, bitmapName]   = "gui/hud_new_packturretout";
$WeaponsHudData[27, itemDataName] = "EcstacyCannon";
$WeaponsHudData[27, ammoDataName] = "EcstacyCapacitor";
$WeaponsHudData[27, reticle] = "gui/ret_chaingun";

$WeaponsHudData[28, bitmapName]   = "gui/hud_chaingun";
$WeaponsHudData[28, itemDataName] = "MechMinigun";
$WeaponsHudData[28, ammoDataName] = "MechMinigunAmmo";
$WeaponsHudData[28, reticle] = "gui/ret_chaingun";

$WeaponsHudData[29, bitmapName]   = "gui/hud_missiles";
$WeaponsHudData[29, itemDataName] = "MechRocketGun";
$WeaponsHudData[29, ammoDataName] = "MechRocketGunAmmo";
$WeaponsHudData[29, reticle] = "gui/ret_missile";

$WeaponsHudData[30, bitmapName]   = "gui/hud_missiles";
$WeaponsHudData[30, itemDataName] = "MissileBarrelGun";
$WeaponsHudData[30, ammoDataName] = "";
$WeaponsHudData[30, reticle] = "gui/ret_missile";

$WeaponsHudData[31, bitmapName]   = "gui/hud_shocklance";
$WeaponsHudData[31, itemDataName] = "PhaserBarrelGun";
$WeaponsHudData[31, ammoDataName] = "PCRAmmo";
$WeaponsHudData[31, reticle] = "gui/ret_elf";

//$WeaponsHudData[32, bitmapName]   = "gui/hud_elfgun";
//$WeaponsHudData[32, itemDataName] = "ELFBarrelGun";
//$WeaponsHudData[32, reticle] = "gui/ret_elf";

//$WeaponsHudData[33, bitmapName]   = "gui/hud_blaster"; // hud_targetlaser
//$WeaponsHudData[33, itemDataName] = "BlasterBarrelGun";
//$WeaponsHudData[33, reticle] = "gui/ret_blaster";


$AmmoIncrement[EnergyRifleAmmo]  = 25;
$AmmoIncrement[PCRAmmo] = 1;
$AmmoIncrement[Sniper3006Ammo]   = 5;
$AmmoIncrement[MechMinigunAmmo] = 100;
$AmmoIncrement[MechRocketGunAmmo]   = 4;
$AmmoIncrement[StarburstAmmo]   = 2;
$AmmoIncrement[AutoCannonAmmo]   = 50;
$AmmoIncrement[RAXXAmmo] = 25;
$AmmoIncrement[StarHammerAmmo] = 4;
$AmmoIncrement[SRM4Ammo] = 8;
$AmmoIncrement[ProtronAmmo] = 1; //in the hopes of fixing this bug
$AmmoIncrement[ACPackAmmo] = 5;
$AmmoIncrement[DiscPackAmmo] = 3;
$AmmoIncrement[SpikeAmmo] = 5;

$ammoType[0] = "PlasmaAmmo";
$ammoType[1] = "DiscAmmo";
$ammoType[2] = "GrenadeLauncherAmmo";
$ammoType[3] = "MortarAmmo";
$ammoType[4] = "MissileLauncherAmmo";
$ammoType[5] = "ChaingunAmmo";
$ammoType[6] = "EnergyRifleAmmo";
$ammoType[7] = "StarburstAmmo";
$ammoType[8] = "StarHammerAmmo";
$ammoType[9] = "MechRocketGunAmmo";
$ammoType[10] = "Sniper3006Ammo";
$ammoType[11] = "MechMinigunAmmo";
$ammoType[12] = "AutoCannonAmmo";
$ammoType[13] = "RAXXAmmo";
$ammoType[14] = "ProtronAmmo";
$ammoType[15] = "PBWAmmo";
$ammoType[16] = "SRM4Ammo";
$ammoType[17] = "ACPackAmmo";
$ammoType[18] = "DiscPackAmmo";
$ammoType[19] = "SpikeAmmo";
$ammoType[20] = "RFLAmmo";
$ammoType[21] = "PCCAmmo";

/// vehicles

function VehicleHud::updateHud( %obj, %client, %tag)
{
   %station = %client.player.station;
   %obj.vstag = %tag;
   %menu = %client.vsmenu;
// Not sure if we need to clear the huds... They'll always have the same num rows...
   //if ( %station.lastCount !$= "" )
   //   VehicleHud::clearHud( %client, %tag, %station.lastCount );

   if(!%station.overrideMasterList)
   {
      %station.vehicle[scoutVehicle] = true;
      %station.vehicle[scoutFlyer] = true;
      %station.vehicle[AssaultVehicle] = true;
      %station.vehicle[bomberFlyer] = true;
      %station.vehicle[LOSDownFlyer] = true;
      %station.vehicle[hapcFlyer] = true;
      %station.vehicle[mobileBaseVehicle] = true;
      %station.vehicle[Emancipator] = true;
   }

   %station.vehicle[Advanced] = true;

   %team = %client.getSensorGroup();
   %count = 0;

   if(%menu == 1)
   {
      if (%station.vehicle[scoutVehicle])
      {
         if( %client.vModPriWeapon[ "ScoutVehicle" ] == $VehicleWeapon::CatRocket )
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Missilecat", "", ScoutVehicle, $VehicleMax[ScoutVehicle] - $VehicleTotalCount[%team, ScoutVehicle] );
         else
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Wildcat", "", ScoutVehicle, $VehicleMax[ScoutVehicle] - $VehicleTotalCount[%team, ScoutVehicle] );
         %count++;
      }
      if (%station.vehicle[scoutFlyer])
      {
         if( %client.vModShield[ "ScoutFlyer" ] == $VehicleShield::None )
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Shrike Interceptor", "", ScoutFlyer, $VehicleMax[ScoutFlyer] - $VehicleTotalCount[%team, ScoutFlyer] );
         else
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Shrike Fighter", "", ScoutFlyer, $VehicleMax[ScoutFlyer] - $VehicleTotalCount[%team, ScoutFlyer] );
         %count++;
      }
      if (%station.vehicle[mobileBaseVehicle])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Mobile Point Base", "", MobileBaseVehicle, $VehicleMax[MobileBaseVehicle] - $VehicleTotalCount[%team, MobileBaseVehicle] );
         %count++;
      }
      if(%station.vehicle[Emancipator])
      {
												// -[soph]
        // messageClient( %client, 'SetLineHud', "", %tag, %count, "Emancipator Heavy EAX Mech", "", Emancipator, $VehicleMax[Emancipator] - $VehicleTotalCount[%team, Emancipator] );
												// -[/soph]
         %val = %station.mechTons > 0 ? mFloor( %station.mechTons / %client.mechWeight ) : 0 ;	// +[soph]
         %color = %val > 0 ? "<color:00FF00>" : "<color:FF0000>" ;				// +
         messageClient( %client , 'SetLineHud' , "" , %tag , %count , $MechName[ %client.currentMechSlot ] , "" , Emancipator , %val ) ;
         bottomPrint( %client, "This station has" SPC %color @ %station.mechTons SPC "<color:42dbea>mech tonnage available.\nYour currently selected mech weighs <color:FFFF00>" @ %client.mechWeight SPC "<color:42dbea>tons." , 3 , 2 ) ;
												// +[/soph]
         %count++;
      }
      if (%station.vehicle[Advanced])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", Back, 1 );
         %count++;
      }
   }
   else if(%menu == 2)
   {
      if (%station.vehicle[AssaultVehicle])
      {
         if( %client.vModSecWeapon[ "AssaultVehicle" ] == $VehicleWeapon::TankFlamethrower && %client.vModule[ "AssaultVehicle" ] == $VehicleModule::BeoPilotRAXX )
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Dragon Tank", "", AssaultVehicle, $VehicleMax[AssaultVehicle] - $VehicleTotalCount[%team, AssaultVehicle] );
         else
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Beowulf Tank", "", AssaultVehicle, $VehicleMax[AssaultVehicle] - $VehicleTotalCount[%team, AssaultVehicle] );
         %count++;
      }
      if (%station.vehicle[bomberFlyer])
      {
         if( %client.vModPriWeapon[ "BomberFlyer" ] $= "" || %client.vModPriWeapon[ "BomberFlyer" ] $= $VehicleWeapon::BomberBomb )
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Thundersword Bomber", "", BomberFlyer, $VehicleMax[BomberFlyer] - $VehicleTotalCount[%team, BomberFlyer] );
         else if( %client.vModPriWeapon[ "BomberFlyer" ] $= $VehicleWeapon::TSwordPilotCannon  )
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Thundersword Cruiser", "", BomberFlyer, $VehicleMax[BomberFlyer] - $VehicleTotalCount[%team, BomberFlyer] );
         else
            messageClient( %client, 'SetLineHud', "", %tag, %count, "Thundersword Gunship", "", BomberFlyer, $VehicleMax[BomberFlyer] - $VehicleTotalCount[%team, BomberFlyer] );
         %count++;
      }
      if (%station.vehicle[Retaliator])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Retaliator Assault Battleship", "", Retaliator, $VehicleMax[Retaliator] - $VehicleTotalCount[%team, Retaliator] );
         %count++;
      }
      if (%station.vehicle[LOSDownFlyer])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Nightshade Support Carrier", "", LOSDownFlyer, $VehicleMax[LOSDownFlyer] - $VehicleTotalCount[%team, LOSDownFlyer] );
         %count++;
      }
      if (%station.vehicle[hapcFlyer])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Havoc Troop Carrier", "", HAPCFlyer, $VehicleMax[HAPCFlyer] - $VehicleTotalCount[%team, HAPCFlyer] );
         %count++;
      }
      if (%station.vehicle[Advanced])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", Back, 1 );
         %count++;
      }
   }
   else if(%menu == 3)
   {
      if (%station.vehicle[Advanced])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", Back, 1 );
         %count++;
      }
   }
   else if(%menu == 4)
   {
      if (%station.vehicle[Advanced])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Back", "", Back, 1 );
         %count++;
      }
   }
   else
   {
      if (%station.vehicle[Advanced])
      {
        messageClient( %client, 'SetLineHud', "", %tag, %count, "Single Pilot Vehicles", "", Terragravs, 1 );
         %count++;
      }
      if (%station.vehicle[Advanced])
      {
         messageClient( %client, 'SetLineHud', "", %tag, %count, "Team Vehicles", "", Turbogravs, 1 );
         %count++;
      }
//      if (%station.vehicle[Advanced])
//      {
//         messageClient( %client, 'SetLineHud', "", %tag, %count, "Transport Vehicles", "", Heavy, 1 );
//         %count++;
//      }
//      if (%station.vehicle[Advanced])
//      {
//         messageClient( %client, 'SetLineHud', "", %tag, %count, "Ground Vehicles", "", Ground, 1 );
//         %count++;
//      }
   }
   %station.lastCount = %count;
}

$VehicleMax[ScoutVehicle]      = 3;
$VehicleMax[AssaultVehicle]    = 2;
$VehicleMax[MobileBaseVehicle] = 1;
$VehicleMax[ScoutFlyer]        = 3;
$VehicleMax[BomberFlyer]       = 2;
$VehicleMax[HAPCFlyer]         = 3;

$VehicleMax[LOSDownFlyer]      = 1;
$VehicleMax[Emancipator]       = 0 ;	// = 2;	-soph
$VehicleMax[Advanced]          = 1;

function clearVehicleCount(%team)
{
   $VehicleTotalCount[%team, ScoutVehicle]      = 0;
   $VehicleTotalCount[%team, AssaultVehicle]    = 0;
   $VehicleTotalCount[%team, MobileBaseVehicle] = 0;
   $VehicleTotalCount[%team, ScoutFlyer]        = 0;
   $VehicleTotalCount[%team, BomberFlyer]       = 0;
   $VehicleTotalCount[%team, HAPCFlyer]         = 0;

   $VehicleTotalCount[%team, Advanced]          = 0;
   $VehicleTotalCount[%team, LOSDownFlyer]      = 0;
   $VehicleTotalCount[%team, Emancipator]       = 0;
}

function refreshVehicleCounts()
{
     if($VehicleMax[ScoutVehicle] > 3)
        $VehicleMax[ScoutVehicle] = 3;

     if($VehicleTotalCount[1, ScoutVehicle] < 0)
        $VehicleTotalCount[1, ScoutVehicle] = 0;

     if($VehicleTotalCount[2, ScoutVehicle] < 0)
        $VehicleTotalCount[2, ScoutVehicle] = 0;

     if($VehicleMax[AssaultVehicle] > 2)
        $VehicleMax[AssaultVehicle] = 2;

     if($VehicleTotalCount[1, AssaultVehicle] < 0)
        $VehicleTotalCount[1, AssaultVehicle] = 0;

     if($VehicleTotalCount[2, AssaultVehicle] < 0)
        $VehicleTotalCount[2, AssaultVehicle] = 0;

     if($VehicleMax[MobileBaseVehicle] > 1)
        $VehicleMax[MobileBaseVehicle] = 1;

     if($VehicleTotalCount[1, MobileBaseVehicle] < 0)
        $VehicleTotalCount[1, MobileBaseVehicle] = 0;

     if($VehicleTotalCount[2, MobileBaseVehicle] < 0)
        $VehicleTotalCount[2, MobileBaseVehicle] = 0;

     if($VehicleMax[ScoutFlyer] > 3)
        $VehicleMax[ScoutFlyer] = 3;

     if($VehicleTotalCount[1, ScoutFlyer] < 0)
        $VehicleTotalCount[1, ScoutFlyer] = 0;

     if($VehicleTotalCount[2, ScoutFlyer] < 0)
        $VehicleTotalCount[2, ScoutFlyer] = 0;

     if($VehicleMax[BomberFlyer] > 2)
        $VehicleMax[BomberFlyer] = 2;

     if($VehicleTotalCount[1, BomberFlyer] < 0)
        $VehicleTotalCount[1, BomberFlyer] = 0;

     if($VehicleTotalCount[2, BomberFlyer] < 0)
        $VehicleTotalCount[2, BomberFlyer] = 0;

     if($VehicleMax[HAPCFlyer] > 3)
        $VehicleMax[HAPCFlyer] = 3;

     if($VehicleTotalCount[1, HAPCFlyer] < 0)
        $VehicleTotalCount[1, HAPCFlyer] = 0;

     if($VehicleTotalCount[2, HAPCFlyer] < 0)
        $VehicleTotalCount[2, HAPCFlyer] = 0;
}

// Weapon Data - for weapons without modes
$WeaponData[MagMissileLauncher] = "Damage: 60 HP | DPS: 30 | Type: Energy | Speed: Fast";
$WeaponData[MechMinigun] = "Damage: 4x 10 HP | DPS: 250 | Type: Kinetic | Speed: Very Fast" ;	// "Damage: 15 HP | DPS: 300 | Type: Kinetic | Speed: Very Fast"; -soph
$WeaponDescription[MechMinigun] = "Quad-barrel chaingun for close-quarters and anti-air use.";	// +soph
$WeaponData[RAXX] = "Damage: 10 HP | DPS: 300 | Type: Plasma | Range: 40m";			// "Damage: 10 HP (+22b) | DPS: 260 (832b) | Type: Plasma | Range: 40m"; -soph
$WeaponDescription[RAXX] = "Plasma jet flamethrower, melts through most anything in reach.";	// +soph
$WeaponData[PBW] = "Damage: 330 HP | Range: 300m | Type: Kinetic | Speed: Continuous";		// "Damage: 225 HP | Range: 300m | Type: Kinetic | Speed: Instant"; -soph
$WeaponDescription[PBW] = "Charged particle shotgun with a hell of a kick. Devastating up close.";	// +soph
$WeaponData[PlasmaCannon] = "Damage: 75 HP | DPS: 75 | Type: Plasma | Speed: Fast";
$WeaponDescription[PlasmaCannon] = "Man-portable heavy plasma cannon. ";			// +soph
$WeaponData[LaserCannonGun] = "Damage: 19 HP | DPS: 152 | Type: Energy | Speed: Very Fast";
$WeaponDescription[LaserCannonGun] = "Rotary anti-air energy cannon.";				// +soph
$WeaponData[Shocklance] = "Damage: 50 HP | Range: 15m | Type: Energy | Speed: Instant";		// "Damage: 150 HP | Range: 15m | Type: Energy | Speed: Instant"; -soph
$WeaponDescription[Shocklance] = "Short-range reactor-cracker.";				// +soph
$WeaponData[MegaBlasterCannon] = "Damage: 90 HP | DPS: 45 | Type: Energy | Speed: Fast";	// = "Damage: 50 (90d) HP | DPS: 25 (45d) | Type: Energy | Speed: Fast"; -soph
$WeaponDescription[MegaBlasterCannon] = "High-yield ball of explosive shield-phasing energy.";	// +soph
$WeaponData[Plasma] = "Damage: 45 HP | DPS: 90 | Type: Plasma | Speed: Fast";
$WeaponDescription[Plasma] = "The tried-and-true go-to in coherent-plasma weaponry.";		// +soph
$WeaponData[SpikeRifle] = "Damage: 45 HP | DPS: 90 | Type: Plasma | Speed: Fast";
$WeaponDescription[SpikeRifle] = "High velocity rifle derived from the turret weapon.";		// +soph
$WeaponData[PCR] = "Damage: 21 HP | DPS: 60 | Type: Kinetic | Speed: Fast";			// "Damage: 45 HP | DPS: 90 | Type: Plasma | Speed: Fast"; -soph
$WeaponDescription[PCR] = "Exotic kinetic-spalsh weapon. Lightweight and self-recharging.";	// +soph

// Beacon Modes

//MrKeen - $DenyMode[armor, weapon, %mode] = true;

$WeaponModeCount[Blaster] = 2;
$WeaponModeName[Blaster] = "Blaster";
$WeaponModeSound[Blaster] = "SniperRifleDryFireSound";
$WeaponMode[Blaster, 0] = "Red Blaster";
$WeaponMode[Blaster, 1] = "Blue Blaster";
$WeaponMode[Blaster, 2] = "Fission Pistol";
$WeaponModeDescription[Blaster, 0] = "Red Blaster Bolt. Bypasses shielding on any target.";		// "Red Blaster Bolt. Ignores shielding on any object."; -soph
$WeaponModeDescription[Blaster, 1] = "Blue Blaster Bolt. Twice the damage, useless against shields.";	// "Blue Blaster Bolt. No shield damage, but 200% armor damage."; -soph
$WeaponModeDescription[Blaster, 2] = "Weak but highly-accurate sidearm.";				// "Sentry fission bolt adapted to the Blaster."; -soph
$WeaponModeData[Blaster, 0] = "Damage: 8x3 (24) HP | DPS: 120 | Type: Energy | Speed: Moderate";
$WeaponModeData[Blaster, 1] = "Damage: 16x3 (48) HP | DPS: 240 | Type: Energy | Speed: Moderate";	// "Damage: 8x3 (24) HP | DPS: 120 | Type: Energy | Speed: Moderate"; -soph
$WeaponModeData[Blaster, 2] = "Damage: 10 HP | DPS: 50 | Type: Energy | Speed: Very Fast";

$WeaponModeCount[Chaingun] = 1;
$WeaponMode[Chaingun, 0] = "Standard";
$WeaponMode[Chaingun, 1] = "Poison Dart";
$WeaponModeName[Chaingun] = "Chaingun";
$WeaponModeSound[Chaingun] = "SniperRifleDryFireSound";
$WeaponModeDescription[Chaingun, 0] = "Standard Chaingun bullets.";
$WeaponModeDescription[Chaingun, 1] = "Poisons victims. Uses triple ammo.";				// "25% chance to cause Poison status on damage. Uses 3 ammo."; -soph
$WeaponModeData[Chaingun, 0] = "Damage: 8 HP | DPS: 165 | Type: Kinetic | Speed: Very Fast";
$WeaponModeData[Chaingun, 1] = "Damage: 8 HP | DPS: 165 | Type: Kinetic | Speed: Very Fast";
$DenyMode[Juggernaut, Chaingun, 1] = true;

$WeaponModeCount[AutoCannon] = 2;
$WeaponMode[AutoCannon, 0] = "Standard HE";
$WeaponMode[AutoCannon, 1] = "Incendiary";
$WeaponMode[AutoCannon, 2] = "Proximity Explosive";
$WeaponModeName[AutoCannon] = "AutoCannon";
$WeaponModeSound[AutoCannon] = "SniperRifleDryFireSound";
$WeaponModeDescription[AutoCannon, 0] = "Explosive-core shells, accurate and damaging.";			// "High explosive tipped shells, good damage and accuracy."; -soph
$WeaponModeDescription[AutoCannon, 1] = "Flaming rounds which may immolate targets, but consumes double ammo.";	// "Can destroy seeking missiles, 1% chance to set target on fire."; -soph
$WeaponModeDescription[AutoCannon, 2] = "Explodes near heat sources such as jetting players, vehicles, and flares.";
$WeaponModeData[AutoCannon, 0] = "Damage: 15 HP | DPS: 150 | Type: Explosive | Speed: Fast";			// "Damage: 15 HP | DPS: 150 | Type: Explosive | Speed: Fast"; -soph
$WeaponModeData[AutoCannon, 1] = "Damage: 20 HP | DPS: 200 | Type: Plasma | Speed: Moderate";			// "Damage: 20 HP | DPS: 200 | Type: Plasma | Speed: Fast"; -soph
$WeaponModeData[AutoCannon, 2] = "Damage: 20 HP | DPS: 200 | Type: Explosive | Speed: Moderate" ;		// "Damage: 25 HP | DPS: 250 | Type: Explosive | Speed: Fast"; -soph

$WeaponModeCount[MechMinigun] = 0;									// +[soph]	
$WeaponMode[MechMinigun, 0] = "Standard";								// + probably not going to go with this
$WeaponMode[MechMinigun, 1] = "Overspin";								// +
$WeaponModeName[MechMinigun] = "Mech Minigun";								// +
$WeaponModeSound[MechMinigun] = "SniperRifleDryFireSound";						// +
$WeaponModeDescription[MechMinigun, 0] = "Quad-chaingun designed for anti-air, also deadly up close.";	// +
$WeaponModeData[MechMinigun, 0] = "Damage: 4x 10 HP | DPS: 250 | Type: Kinetic | Speed: Very Fast";	// +[/soph]

$WeaponModeCount[Shocklance] = 2;
$WeaponMode[Shocklance, 0] = "Standard";
$WeaponMode[Shocklance, 1] = "Incendiary";
$WeaponMode[Shocklance, 2] = "Poison";
$WeaponModeName[Shocklance] = "Shocklance";
$WeaponModeSound[Shocklance] = "ShockLanceDryFireSound";
$WeaponModeDescription[Shocklance, 0] = "Standard electro-lance.";
$WeaponModeDescription[Shocklance, 1] = "Thermal lance, sets your target on fire!";	// "Sets your target on fire!"; -soph
$WeaponModeDescription[Shocklance, 2] = "Poisons your target. Give them a nasty rash!";
$WeaponModeData[Shocklance, 0] = "Damage: 50 HP | Range: 15m | Type: Energy | Speed: Instant";	// "Damage: 150 HP | Range: 15m | Type: Energy | Speed: Instant"; -soph
$WeaponModeData[Shocklance, 1] = "Damage: 45 HP | Range: 15m | Type: Thermal | Speed: Instant";	// "Damage: 10 HP | Range: 15m | Type: Energy | Speed: Instant"; -soph
$WeaponModeData[Shocklance, 2] = "Damage: 30 HP | Range: 15m | Type: Poison | Speed: Instant";	// "Damage: 40 HP | Range: 15m | Type: Energy | Speed: Instant"; -soph

$WeaponModeCount[EnergyRifle] = 1;
$WeaponMode[EnergyRifle, 0] = "Standard";
$WeaponMode[EnergyRifle, 1] = "Spreadfire";
$WeaponModeName[EnergyRifle] = "Energy Rifle";
$WeaponModeSound[EnergyRifle] = "ShockLanceDryFireSound";
$WeaponModeDescription[EnergyRifle, 0] = "Standard Energy blast.";
$WeaponModeDescription[EnergyRifle, 1] = "Fires a triple bolt spread.";	// "Fires 3 high powered energy blasts, uses 3 ammo."; -soph
$WeaponModeData[EnergyRifle, 0] = "Damage: 36 HP | DPS: 36 | Type: Energy | Speed: Fast";
$WeaponModeData[EnergyRifle, 1] = "Damage: 3x 36 HP | DPS: 108 | Type: Energy | Speed: Fast";	// "Damage: 36x3 (108) HP | DPS: 108 | Type: Energy | Speed: Fast"; -soph

$WeaponModeCount[RFL] = 1;
$WeaponMode[RFL, 0] = "High Frequency";
$WeaponMode[RFL, 1] = "Low Frequency";
$WeaponModeName[RFL] = "Laser Assault Rifle";
$WeaponModeSound[RFL] = "ShockLanceDryFireSound";
$WeaponModeDescription[RFL, 0] = "High frequency laser, does more damage with less energy but shorter range."; 
$WeaponModeDescription[RFL, 1] = "Low frequency laser, not as much damage, uses more energy for longer range."; 
$WeaponModeData[RFL, 0] = "Damage: 15-45 HP | DPS: 60-180 | Type: Energy | Range: 75m" ;		// "Damage: 20-60 HP | DPS: 80-240 | Type: Energy | Range: 75m"; -soph
$WeaponModeData[RFL, 1] = "Damage: 12-38 HP | DPS: 50-150 | Type: Energy | Range: 150m";		// "Damage: 15-45 HP | DPS: 60-180 | Type: Energy | Range: 150m"; -soph

$WeaponModeCount[Protron] = 2;
$WeaponMode[Protron, 0] = "Standard";
$WeaponMode[Protron, 1] = "Energetic Pulse";								// "Pulse";
$WeaponMode[Protron, 2] = "Thermal Pulse";								// = "Blue Blaster"; -soph
//$WeaponMode[Protron, 3] = "Fission Gattling";
$WeaponModeName[Protron] = "Protron";
$WeaponModeSound[Protron] = "SniperRifleDryFireSound";
$WeaponModeDescription[Protron, 0] = "Energy chaingun blast with a small splash.";
$WeaponModeDescription[Protron, 1] = "Disruptive pulse bolts. Saps target's energy as well as armor.";	// "Shield-modulated Protron bolt. Does more damage to shields than armor."; -soph
$WeaponModeDescription[Protron, 2] = "Deadly short-range plasma spray, but rapidly burns ammo.";	// = "Blue Blaster bolt, does 200% to unshielded targets, uses 2 ammo.";
//$WeaponModeDescription[Protron, 3] = "The power of a rapid-fire sentry turret in your hands!";
$WeaponModeData[Protron, 0] = "Damage: 8 HP | DPS: 80 | Type: Energy | Speed: Fast";
$WeaponModeData[Protron, 1] = "Damage: 8 HP + 4 *EN | DPS: 80 | Type: Energy + EMP | Speed: Moderate";	// = "Damage: 10 HP | DPS: 100 | Type: Energy | Speed: Moderate"; -soph
$WeaponModeData[Protron, 2] = "Damage: 6 HP | DPS: 240 | Type: Plasma | Speed: Very Fast";			// = "Damage: 8 HP | DPS: 80 | Type: Energy | Speed: Moderate";

$WeaponModeCount[Mortar] = 3;
$WeaponMode[Mortar, 0] = "Standard";
$WeaponMode[Mortar, 1] = "Impact";
$WeaponMode[Mortar, 2] = "EMP";
$WeaponMode[Mortar, 3] = "Artillery";
$WeaponModeName[Mortar] = "Mortar";
$WeaponModeSound[Mortar] = "MortarReloadSound";
$WeaponModeDescription[Mortar, 0] = "Standard big green boomie.";
$WeaponModeDescription[Mortar, 1] = "Impact explosive charge. Not as powerful as normal mortar.";
$WeaponModeDescription[Mortar, 2] = "Industrial-strength EMP bomb, fry someone today! Uses 2 ammo.";
$WeaponModeDescription[Mortar, 3] = "Turret-strength heavy Mortar launched at high speeds. Uses 2 ammo.";
$WeaponModeData[Mortar, 0] = "Damage: 200 HP | Fuse Time: 3s | Type: Explosive | Speed: Slow";
$WeaponModeData[Mortar, 1] = "Damage: 100 HP | Fuse Time: 0.25s | Type: Explosive | Speed: Slow";
$WeaponModeData[Mortar, 2] = "Damage: 400 *EN | Fuse Time: 4s | Type: EMP | Speed: Slow";
$WeaponModeData[Mortar, 3] = "Damage: 300 HP | Fuse Time: 6s | Type: Explosive | Speed: Fast";	// "Damage: 375 HP | Fuse Time: 6s | Type: Explosive | Speed: Fast"; -soph
$DenyMode[Heavy, Mortar, 2] = true;
$DenyMode[Heavy, Mortar, 3] = true;

$WeaponModeCount[GrenadeLauncher] = 2; 
$WeaponMode[GrenadeLauncher, 0] = "Standard";
$WeaponMode[GrenadeLauncher, 1] = "EMP";
$WeaponMode[GrenadeLauncher, 2] = "RPG";
$WeaponModeName[GrenadeLauncher] = "Grenade Launcher";
$WeaponModeSound[GrenadeLauncher] = "GrenadeReloadSound";
$WeaponModeDescription[GrenadeLauncher, 0] = "Standard explosive grenade.";
$WeaponModeDescription[GrenadeLauncher, 1] = "Normal strength EMP bomb. 15m effect radius.";
$WeaponModeDescription[GrenadeLauncher, 2] = "Rocket Propelled Grenade. Death at 360 KPH.";
$WeaponModeData[GrenadeLauncher, 0] = "Damage: 40 HP | DPS: 44 | Type: Explosive | Speed: Moderate";
$WeaponModeData[GrenadeLauncher, 1] = "Damage: 125 *EN | DPS: 27 | Type: EMP | Speed: Moderate";
$WeaponModeData[GrenadeLauncher, 2] = "Damage: 40 HP | DPS: 44 | Type: Explosive | Speed: Moderate";
$DenyMode[MagIon, GrenadeLauncher, 2] = true;

$WeaponModeCount[Disc] = 2;
$WeaponMode[Disc, 0] = "Standard";
$WeaponMode[Disc, 1] = "Turbo Disc";
$WeaponMode[Disc, 2] = "Power Disc";
$WeaponMode[Disc, 3] = "Ripper Disc";
$WeaponModeName[Disc] = "Spinfusor";
$WeaponModeSound[Disc] = "DiscReloadSound";
$WeaponModeDescription[Disc, 0] = "Traditional explosive Tribal weapon.";					// "Standard destructive Tribal weapon.";
$WeaponModeDescription[Disc, 1] = "Higher velocity, lower power. Preferred for intercepting aerial targets.";	// "Trades power for speed. Useful against lights.";
$WeaponModeDescription[Disc, 2] = "High explosive disc, requires double ammo and some energy.";			// "High explosive power disc, uses 2 ammo for 2x the pain.";
$WeaponModeDescription[Disc, 3] = "Rip them apart with this fast bouncing explosive disc! Uses 2 ammo.";
$WeaponModeData[Disc, 0] = "Damage: 75 HP | DPS: 75 | Type: Explosive | Speed: Moderate";			// "Damage: 50 (+25d) HP | DPS: 63 (94d) | Type: Explosive | Speed: Fast"; -soph
$WeaponModeData[Disc, 1] = "Damage: 38 HP | DPS: 35 | Type: Explosive | Speed: Fast";				// "Damage: 25 (13d) HP | DPS: 32 (42) | Type: Explosive | Speed: Very Fast"; -soph
$WeaponModeData[Disc, 2] = "Damage: 125 HP | DPS: 83 | Type: Explosive | Speed: Slow";				// "Damage: 100 HP | DPS: 125 | Type: Explosive | Speed: Slow"; -soph
$WeaponModeData[Disc, 3] = "Damage: 50 HP | DPS: 63 | Type: Kinetic | Speed: Fast";
$DenyMode[MagIon, Disc, 2] = true;
//$DenyMode[Blastech, Disc, 2] = true;

$WeaponModeCount[MultiFusor] = 1;
$WeaponMode[MultiFusor, 0] = "Standard";
$WeaponMode[MultiFusor, 1] = "Ripper Disc";
$WeaponModeName[MultiFusor] = "Multifusor";
$WeaponModeSound[MultiFusor] = "DiscReloadSound";
$WeaponModeDescription[MultiFusor, 0] = "Rapid, powerful discs, but only minimally explosive.";	// "High RoF disc launcher with direct damage explosive shells."; -soph
$WeaponModeDescription[MultiFusor, 1] = "Rip them apart with this fast bouncing kinetic disc!";
$WeaponModeData[MultiFusor, 0] = "Damage: 70 HP | DPS: 160 | Type: Explosive | Speed: Fast";	// "Damage: 80 HP | DPS: 212 | Type: Explosive | Speed: Fast"; -soph
$WeaponModeData[MultiFusor, 1] = "Damage: 50 HP | DPS: 114 | Type: Kinetic | Speed: Fast";

$WeaponModeCount[Plasma] = 1;
$WeaponMode[Plasma, 0] = "Standard Plasma Blast";
$WeaponMode[Plasma, 1] = "Meteor Plasma Blast";
$WeaponModeName[Plasma] = "Plasma";
$WeaponModeSound[Plasma] = "PlasmaReloadSound";
$WeaponModeDescription[Plasma, 0] = "A good hit can set enemies on fire.";			// "Plasma does well against shields, 10% chance to catch target on fire."; -soph
$WeaponModeDescription[Plasma, 1] = "Destructive, but short-ranged and burns double ammo.";	// "Increased plasma damage, most useful for aerial bombardment, uses 2 ammo."; -soph
$WeaponModeData[Plasma, 0] = "Damage: 45 HP | DPS: 90 | Type: Plasma | Speed: Moderate";
$WeaponModeData[Plasma, 1] = "Damage: 52 HP | DPS: 104 | Type: Plasma | Speed: Fast";		// "Damage: 60 HP | DPS: 120 | Type: Plasma | Speed: Fast";
$DenyMode[Light, Plasma, 1] = true;
$DenyMode[MagIon, Plasma, 1] = true;
$DenyMode[Engineer, Plasma, 1] = true;
$DenyMode[Blastech, Plasma, 1] = true;

$WeaponModeCount[MBCannon] = 5;
$WeaponMode[MBCannon, 0] = "Standard Mitzi Blast (10)";
$WeaponMode[MBCannon, 1] = "Light Mitzi Blast (5)";
$WeaponMode[MBCannon, 2] = "Dense Mitzi Blast (20)";
$WeaponMode[MBCannon, 3] = "Mitzi Booster (25)";
$WeaponMode[MBCannon, 4] = "Mitzi Annihalator (100)";
$WeaponMode[MBCannon, 5] = "Modulated Shield Transparency Blast (125)";	// (150)"; -soph
$WeaponModeName[MBCannon] = "Mitzi Blast Cannon";
$WeaponModeSound[MBCannon] = "MortarDryFireSound";
$WeaponModeDescription[MBCannon, 0] = "Standard Mitzi blast. Mitzi will damage anything.";
$WeaponModeDescription[MBCannon, 1] = "Half damage, double speed. Useful for tagging fast moving targets.";
$WeaponModeDescription[MBCannon, 2] = "Dense core Mitzi travels slower, but is radioactive, damaging as it passes.";
$WeaponModeDescription[MBCannon, 3] = "Boosts your target away from you, or vice versa.";
$WeaponModeDescription[MBCannon, 4] = "Extremely powerful blast of Mitzi energy, able to bend time and space.";
$WeaponModeDescription[MBCannon, 5] = "Fast moving ball of Mitzi energy that phases through most shielding.";
$WeaponModeData[MBCannon, 0] = "Damage: 105 HP | DPS: 52 | Type: Mitzi | Speed: Moderate";
$WeaponModeData[MBCannon, 1] = "Damage: 60 HP | DPS: 30 | Type: Mitzi | Speed: Fast";
$WeaponModeData[MBCannon, 2] = "Damage: 135 HP | DPS: 67 (98r) | Type: Mitzi | Speed: Slow";	// = "Damage: 135 HP (+60r) | DPS: 67 (98r) | Type: Mitzi | Speed: Slow"; -soph
$WeaponModeData[MBCannon, 3] = "Damage: 0 HP | DPS: N/A | Type: N/A | Speed: Moderate";
$WeaponModeData[MBCannon, 4] = "Damage: 750 HP | Type: Explosive | Speed: Very Slow";
$WeaponModeData[MBCannon, 5] = "Damage: 500 HP | DPS: N/A | Type: Mitzi | Speed: Fast";		// "Damage: 500 HP | DPS: N/A | Type: Mitzi | Speed: Very Fast"; -soph
$DenyMode[MagIon, MBCannon, 4] = true;
$DenyMode[MagIon, MBCannon, 5] = true;
$DenyMode[Blastech, MBCannon, 4] = true;
$DenyMode[Blastech, MBCannon, 5] = true;
$DenyMode[Light, MBCannon, 4] = true;
$DenyMode[Light, MBCannon, 5] = true;
$DenyMode[Medium, MBCannon, 4] = true;
$DenyMode[Medium, MBCannon, 5] = true;
$DenyMode[Engineer, MBCannon, 4] = true;
$DenyMode[Engineer, MBCannon, 5] = true;
$DenyMode[Heavy, MBCannon, 5] = true;

$WeaponModeCount[MechRocketGun] = 1;
$WeaponMode[MechRocketGun, 0] = "Explosive Rockets";
$WeaponMode[MechRocketGun, 1] = "Howitzer Shells";
$WeaponModeName[MechRocketGun] = "Mech Rocket Launcher";
$WeaponModeSound[MechRocketGun] = "MissileReloadSound";
$WeaponModeDescription[MechRocketGun, 0] = "High explosive rockets, great against light armors.";	// "High explosive rockets, great vs. light armors."; -soph
$WeaponModeDescription[MechRocketGun, 1] = "Accelerating kinetic rockets, devastating at long range.";	// "Anti-personnel slugs that sacrifice explosive power for raw kinetic power."; -soph
$WeaponModeData[MechRocketGun, 0] = "Damage: 2x 45 HP | DPS: 45 | Type: Explosive | Speed: Fast" ;	// "Damage: 90 HP | DPS: 45 | Type: Explosive | Speed: Fast"; -soph
$WeaponModeData[MechRocketGun, 1] = "Damage: Variable | DPS: Variable | Type: Kinetic | Speed: Accelerating" ;	// "Damage: 270 HP | DPS: 180 | Type: Kinetic | Speed: Very Fast"; -soph

$WeaponModeCount[ElfGun] = 3;
$WeaponMode[ElfGun, 0] = "Standard";
$WeaponMode[ElfGun, 1] = "Tractor Beam";
$WeaponMode[ElfGun, 2] = "Super Tractor";
$WeaponMode[ElfGun, 3] = "Pull Beam"; //-Nite-
$WeaponMode[ElfGun, 4] = "Power Transfer Beam";
$WeaponModeName[ElfGun] = "Electron Flux";
$WeaponModeSound[ElfGun] = "ShocklanceDryFireSound";
$WeaponModeDescription[ElfGun, 0] = "Standard Energy draining beam.";
$WeaponModeDescription[ElfGun, 1] = "Allows you to hold onto players and vehicles.";
$WeaponModeDescription[ElfGun, 2] = "Pulls you toward your target.";
$WeaponModeDescription[ElfGun, 3] = "Pulls objects towards you.";
$WeaponModeDescription[ElfGun, 4] = "Diverts power from your armor to target object, player, or vehicle.";
$WeaponModeData[ElfGun, 0] = "DPS: 32 HP/s | Energy DPS: 24 KW/s | Type: Energy | Speed: Instant";
$WeaponModeData[ElfGun, 1] = "";
$WeaponModeData[ElfGun, 2] = "";
$WeaponModeData[ElfGun, 3] = "";
$WeaponModeData[ElfGun, 4] = "Power transfer rate based on 50% of your armor's recharge rate.";
$DenyMode[Heavy, ElfGun, 1] = true;
$DenyMode[BattleAngel, ElfGun, 1] = true;
$DenyMode[BattleAngel, ElfGun, 2] = true;

$WeaponModeCount[MissileLauncher] = 6;	// 5; -soph
$WeaponMode[MissileLauncher, 0] = "Standard";
$WeaponMode[MissileLauncher, 1] = "Speed";
$WeaponMode[MissileLauncher, 2] = "EMP";
$WeaponMode[MissileLauncher, 3] = "Incendiary";
$WeaponMode[MissileLauncher, 4] = "Mega";
$WeaponMode[MissileLauncher, 5] = "Laser Guided";
$WeaponMode[MissileLauncher, 6] = "Cluster";
$WeaponModeName[MissileLauncher] = "Missile Launcher";
$WeaponModeSound[MissileLauncher] = "MissileReloadSound";
$WeaponModeDescription[MissileLauncher, 0] = "Standard heatseeking missile.";
$WeaponModeDescription[MissileLauncher, 1] = "Trades power for speed and kick.";					//= "Trades power for speed."; -soph
$WeaponModeDescription[MissileLauncher, 2] = "Cripple your target's shields and engines.";				// "Stops energy generation on vehicles or locked players."; -soph
$WeaponModeDescription[MissileLauncher, 3] = "Light people on fire!";							// "Sets the player on fire, only players."; -soph
$WeaponModeDescription[MissileLauncher, 4] = "Slow moving Rocket Propelled Mortar. Best for adding injury to insult.";	// "Slow moving Rocket Propelled Mortar. Best for adding insult to injury."; -soph
$WeaponModeDescription[MissileLauncher, 5] = "Standard missile that follows the attached laser.";			// "Normal power missile that follows the attached laser."; -soph
$WeaponModeDescription[MissileLauncher, 6] = "Cluster missile launch for overwhelming flares.";
$WeaponModeData[MissileLauncher, 0] = "Damage: 100 HP | DPS: 34 | Type: Explosive | Speed: Fast";			// "Damage: 90 HP | DPS: 30 | Type: Explosive | Speed: Fast"; -soph
$WeaponModeData[MissileLauncher, 1] = "Damage: 70 HP | DPS: 24 | Type: Explosive | Speed: Very Fast";			// "Damage: 60 HP | DPS: 20 | Type: Explosive | Speed: Very Fast"; -soph
$WeaponModeData[MissileLauncher, 2] = "Damage: 150 *EN | DPS: N/A | Type: EMP | Speed: Fast";
$WeaponModeData[MissileLauncher, 3] = "Damage: 20 HP | DPS: 7 | Type: Plasma | Speed: Fast";
$WeaponModeData[MissileLauncher, 4] = "Damage: 150 HP | DPS: 52 | Type: Explosive | Speed: Sloooow";
$WeaponModeData[MissileLauncher, 5] = "Damage: 80 HP | DPS: 28 | Type: Explosive | Speed: Fast";			// "Damage: 90 HP | DPS: 30 | Type: Explosive | Speed: Fast"; -soph
$WeaponModeData[MissileLauncher, 6] = "Damage: 20x4 HP | DPS: 28 | Type: Explosive | Speed: Fast";			// +soph
$DenyMode[Blastech, MissileLauncher, 4] = true;

$WeaponModeCount[SniperRifle] = 4;
$WeaponMode[SniperRifle, 0] = "Standard Laser";
$WeaponMode[SniperRifle, 1] = "Pulse Laser";
$WeaponMode[SniperRifle, 2] = "Standard 30.06";
$WeaponMode[SniperRifle, 3] = "Poison Dart";
$WeaponMode[SniperRifle, 4] = "Mass Driver";
$WeaponModeName[SniperRifle] = "Laser/Sniper";
$WeaponModeSound[SniperRifle] = "SniperRifleDryFireSound";
$WeaponModeDescription[SniperRifle, 0] = "150 Megajoule laser, burn a hole in someone today!";
$WeaponModeDescription[SniperRifle, 1] = "Rapid fire laser, does not require energy pack. 20% damage, 20% energy use.";
$WeaponModeDescription[SniperRifle, 2] = "Standard sniper shot. For when you need to be covert.";
$WeaponModeDescription[SniperRifle, 3] = "Poison a player today! No, you can't poison Mechs.  Stop trying." ;		// "Poison a player today! Does not work on Mechs."; -soph
$WeaponModeDescription[SniperRifle, 4] = "Fires an explosive tipped bolt. Uses 2 ammo, cannot headshot.";
$WeaponModeData[SniperRifle, 0] = "Damage: 1-55 HP | Type: Energy | Speed: Instant" ;					// "Damage: 1-75 HP (150 HS) | Type: Energy | Speed: Instant"; -soph
$WeaponModeData[SniperRifle, 1] = "Damage: 11 HP | DPS: 90 | Type: Energy | Speed: Instant" ;				// "Damage: 15 HP | DPS: 90 | Type: Energy | Speed: Instant"; -soph
$WeaponModeData[SniperRifle, 2] = "Damage: 45 HP (135 HS) | Type: Kinetic | Speed: Near Instant";
$WeaponModeData[SniperRifle, 3] = "Damage: 1 HP | DPS: - | Type: Poison | Speed: Near Instant" ;			// "Damage: 1 HP | DPS: 1 | Type: Poison | Speed: Near Instant"; -soph
$WeaponModeData[SniperRifle, 4] = "Damage: 80 HP | DPS: 33.3 | Type: Explosive | Speed: Very Fast " ;			// "Damage: 100 HP | DPS: 33.3 | Type: Explosive | Speed: Fast"; -soph

$WeaponModeCount[Starburst] = 3;
$WeaponMode[Starburst, 0] = "Frag Bomb";
$WeaponMode[Starburst, 1] = "Plasma Burst";
$WeaponMode[Starburst, 2] = "Disc Storm";
$WeaponMode[Starburst, 3] = "MIRV Grenades";
$WeaponModeName[Starburst] = "Starburst Cannon";
$WeaponModeSound[Starburst] = "SniperRifleDryFireSound";
$WeaponModeDescription[Starburst, 0] = "Manual-detonation mortar shreds hot targets with a shower of lead.";		// "Fires a sphere of chaingun bullets."; -soph
$WeaponModeDescription[Starburst, 1] = "Manual-detonation mortar bathes hot targets in even more heat.";		// "Fires streams of RAXX plasma in all directions."; -soph
$WeaponModeDescription[Starburst, 2] = "Manual-detonation mortar nails hot targets with a disc barrage. Uses 2 ammo.";	// "Fires several discs in a random direction. Uses 2 ammo."; -soph
$WeaponModeDescription[Starburst, 3] = "Manual-detonation mortar sprays the battlefield with grenades. Uses 3 ammo.";	// "Launches a cluster of grenades in a MIRV pattern. Uses 3 ammo."; -soph
$WeaponModeData[Starburst, 0] = "Maximum Damage: 120-360 HP | Type: Kinetic";
$WeaponModeData[Starburst, 1] = "Maximum Damage: 150-300 HP | Type: Plasma";						// "Maximum Damage: 150-300 (+110-220 burn/sec) HP | Type: Plasma"; -soph
$WeaponModeData[Starburst, 2] = "Maximum Damage: 350-700 HP | Type: Explosive";
$WeaponModeData[Starburst, 3] = "Maximum Damage: 1200-1800 HP | Type: Explosive";
$DenyMode[Medium, Starburst, 2] = true;
$DenyMode[Medium, Starburst, 3] = true;

$WeaponModeCount[EcstacyCannon] = 1;
$WeaponMode[EcstacyCannon, 0] = "Ecstacy Shotgun Blast";
$WeaponMode[EcstacyCannon, 1] = "Particle Accelerator Ball";
$WeaponMode[EcstacyCannon, 2] = "Laser Punch Beam";
$WeaponModeName[EcstacyCannon] = "Energy Projector Cannon";
$WeaponModeSound[EcstacyCannon] = "SniperRifleDryFireSound";
$WeaponModeDescription[EcstacyCannon, 0] = "Keep in-hand to charge a deadly spray of energy." ;				// "Hold down to charge for a powerful shotgun blast of energy."; -soph
$WeaponModeDescription[EcstacyCannon, 1] = "Keep in-hand to charge a powerful energy bomb." ;				// "Hold down to charge for a powerful ball of dense particles."; -soph
$WeaponModeDescription[EcstacyCannon, 2] = "Hold down to charge for a devistating laser blast.";
$WeaponModeData[EcstacyCannon, 0] = "Damage: 140% of charge | Type: Energy | Speed: Fast";	// "Maximum Damage: 1-704 HP | Type: Energy | Speed: Based on charge"; -soph
$WeaponModeData[EcstacyCannon, 1] = "Damage: 120% of charge | Type: Energy | Speed: Slows with charge";	// "Maximum Damage: 1-601 HP | Type: Energy | Speed: Fast"; -soph
$WeaponModeData[EcstacyCannon, 2] = "Maximum Damage: 1-425 HP | Type: Energy | Speed: Instant";

$WeaponModeCount[BarrelSwapper] = 8;
$WeaponMode[BarrelSwapper, 0] = "AA";
$WeaponMode[BarrelSwapper, 1] = "Blaster Shotgun";
$WeaponMode[BarrelSwapper, 2] = "Missile";
$WeaponMode[BarrelSwapper, 3] = "Mortar";
$WeaponMode[BarrelSwapper, 4] = "Plasma";
$WeaponMode[BarrelSwapper, 5] = "Energy Concussion";
$WeaponMode[BarrelSwapper, 6] = "Turbo Chaingun"; 
$WeaponMode[BarrelSwapper, 7] = "Fireball Launcher";
$WeaponMode[BarrelSwapper, 8] = "Phaser";
$WeaponModeName[BarrelSwapper] = "Turret Barrel Swapper";
$WeaponModeSound[BarrelSwapper] = "";
$WeaponModeDescription[BarrelSwapper, 0] = "Damage: 34 | Type: Energy | RoF: 3/sec | Detect Range: 400m";	// "Damage: 25 | Type: Energy | RoF: 4/sec | Detect Range: 300m"; -soph
$WeaponModeDescription[BarrelSwapper, 1] = "Damage: 120 | Type: Energy | RoF: 1/sec | Detect Range: 75m";	// "Damage: 100 | Type: Energy | RoF: 1/sec | Detect Range: 75m"; -soph
$WeaponModeDescription[BarrelSwapper, 2] = "Damage: 200 | Type: Explosive | RoF: 0.26/sec | Detect Range: 250m";
$WeaponModeDescription[BarrelSwapper, 3] = "Damage: 250 | Type: Explosive | RoF: 0.58/sec | Detect Range: 200m";
$WeaponModeDescription[BarrelSwapper, 4] = "Damage: 60 | Type: Plasma | RoF: 0.9/sec | Detect Range: 225m";
$WeaponModeDescription[BarrelSwapper, 5] = "Damage: 10 | Type: Energy | RoF: 0.55/sec | Detect Range: 190m";
$WeaponModeDescription[BarrelSwapper, 6] = "Damage: 10 | Type: Kinetic | RoF: 15/sec | Detect Range: 140m";
$WeaponModeDescription[BarrelSwapper, 7] = "Damage: 30 | Type: Plasma | RoF: 2/sec | Detect Range: 100m";
$WeaponModeDescription[BarrelSwapper, 8] = "Damage: 63 | Type: Kinetic | RoF: 0.75/sec | Detect Range: 125m";

$WeaponModeCount[SlipstreamPack] = 1;
$WeaponMode[SlipstreamPack, 0] = "Entry Portal";
$WeaponMode[SlipstreamPack, 1] = "Exit Portal";
$WeaponModeName[SlipstreamPack] = "Portal Gun";
$WeaponModeSound[SlipstreamPack] = "SniperRifleDryFireSound";
$WeaponModeDescription[SlipstreamPack, 0] = "Creates an entrance portal.";
$WeaponModeDescription[SlipstreamPack, 1] = "Creates an exit portal.";
$WeaponModeData[SlipstreamPack, 0] = "No damage done.";
$WeaponModeData[SlipstreamPack, 1] = "No damage done";

$WeaponModeCount[LaserBeamerGun] = 1;
$WeaponMode[LaserBeamerGun, 0] = "Charge";
$WeaponMode[LaserBeamerGun, 1] = "Beam";
$WeaponModeName[LaserBeamerGun] = "Laser Charge Cannon";
$WeaponModeSound[LaserBeamerGun] = "SniperRifleDryFireSound";
$WeaponModeDescription[LaserBeamerGun, 0] = "Use this mode to get power for the gun.";
$WeaponModeDescription[LaserBeamerGun, 1] = "Fires a continuous laser beam that can literally cut objects.";
$WeaponModeData[LaserBeamerGun, 0] = "No damage done.";
$WeaponModeData[LaserBeamerGun, 1] = "Damage: 12.5 HP | DPS: 125 | Type: Energy | Speed: Instant";

$WeaponModeCount[ IonTAGGun ] = 1;										// +[soph]
$WeaponMode[ IonTAGGun , 0 ] = "MANTA Tagger" ;
$WeaponMode[ IonTAGGun , 1 ] = "RMS Tagger" ;
$WeaponModeName[ IonTAGGun ] = "Superweapon Tag Pack";
$WeaponModeSound[ IonTAGGun ] = "SniperRifleDryFireSound";
$WeaponModeDescription[ IonTAGGun , 0 ] = "Tags a target for MANTA strike." ;
$WeaponModeDescription[ IonTAGGun , 1 ] = "Tags a target area for RMS barrage." ;
$WeaponModeData[ IonTAGGun , 0 ] = "Must be held on target for 8 seconds." ;
$WeaponModeData[ IonTAGGun , 1 ] = "Must be held on target for 12 seconds." ;					// +[/soph]

//------------------------------------------------------------------------------
//

function TLPlaceBeacon(%obj, %image)
{
        %mVec = %obj.getMuzzleVector(0);
        %mPos = %obj.getMuzzlePoint(0);
        %nmVec = VectorNormalize(%mVec);
        %scmVec = VectorScale(%nmVec, %image.projectile.maxRifleRange);
        %mEnd = VectorAdd(%mPos, %scmVec);
        %searchResult = containerRayCast(%mPos, %mEnd, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType, %obj);
        %raycastPt = posFrRaycast(%searchResult);

         if(%searchresult)
         {
               %targetObject = firstWord(%searchResult);

               if(%targetObject.isMounted())
                  return;
                  
               if(isObject(%obj.targetbeacon))
               {
                  %obj.targetbeacon.delete();
                  %obj.targetbeacon = "";
               }

               %beacon = new BeaconObject()
               {
                  dataBlock = "DeployedBeacon";
                  beaconType = "vehicle"; // vehicle
                  position = %raycastPt;
               };

               %beacon.playThread($AmbientThread, "ambient");
               %beacon.team = %obj.team;
               %beacon.sourceObject = %obj;

               // give it a team target
               %beacon.setTarget(%obj.team);
               MissionCleanup.add(%beacon);
               %beacon.schedule(35000, delete);	// 60000 -soph
               %obj.targetbeacon = %beacon;
               %obj.temporary = true;
               
               if(%targetObject.getType() &  $TypeMasks::VehicleObjectType)
                  %targetObject.mountObject(%beacon, 11);
         }
}

function changeWeaponMode(%obj)
{
   if(!isObject(%obj))
     return;	

   %cl = %obj.client;     
   
   if(%obj.getMountedImage(0) != 0)
   {
      %image = %obj.getMountedImage(0);
      %weapon = %image.item;
      
      if(%image.isTL)
      {
         // TLPlaceBeacon(%obj, %image);	// now placed through aiming steady -soph
          return;
      }
      
      if($WeaponModeCount[%weapon])
      {
         %armor = %obj.getArmorSize();

         %obj.play3D($WeaponModeSound[%weapon]);
         
         if(%cl.mode[%weapon] >= $WeaponModeCount[%weapon])
            %cl.mode[%weapon] = -1;

         %cl.mode[%weapon]++;

         while($DenyMode[%armor, %weapon, %cl.mode[%weapon]]) // MrKeen - still dynamic ;)
         {
            if(%cl.mode[%weapon] >= $WeaponModeCount[%weapon])
               %cl.mode[%weapon] = -1;

            %cl.mode[%weapon]++;
         }

//         if(%cl.hasMD2Client)
//              commandToClient(%cl, 'SetMode',  %cl.mode[%weapon], %weapon);
//         else
//         {
             if($WeaponModeData[%weapon, %cl.mode[%weapon]] !$= "")
                 bottomPrint(%cl, "<color:ffffff>[Mode "@%cl.mode[%weapon]+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %cl.mode[%weapon]] NL $WeaponModeDescription[%weapon, %cl.mode[%weapon]] NL $WeaponModeData[%weapon, %cl.mode[%weapon]], $ChangeWepTime+2, 3);
             else
                 bottomPrint(%cl, "<color:ffffff>[Mode "@%cl.mode[%weapon]+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %cl.mode[%weapon]] NL $WeaponModeDescription[%weapon, %cl.mode[%weapon]], $ChangeWepTime, 2);
//         }
         
         %weapon.onCycleMode(%obj, %cl.mode[%weapon]);
      }
      else
         bottomPrint(%cl, "This weapon does not have any fire modes.", $ChangeWepTime, 1);
   }
}
//-Nite- & SouthTown This so far works pretty well
//All thats really needed is a new bottom print for when they mount the
//weapon.(which mode they dont have access too) Stuff.
// Place in functions changeWeaponmode and weaponONmount
//function findArmorMode(%obj)
//{
//   %armor = %obj.getArmorSize();    //get armor size
//   %weapon = %obj.getMountedImage(0).item;  //get weapon
//   %mode = %obj.client.mode[%weapon];      //get mode of weapon
//
//   if(%armor $= "MagIon")      // if i am ion and
//   {
//   if(%weapon $= "Disc" && %mode == 2)   //if i am using Disc
//         %obj.client.mode[%weapon] = 0; // skip mode 2
//   else if(%weapon $= "Chaingun" && %mode == 1) // just an example
//         %obj.client.mode[%weapon]++; // skip mode 1
//   else if(%weapon $= "MBCannon" && %mode == 4)
//         %obj.client.mode[%weapon] = 0;  // goto 1
//   else if(%weapon $= "MBCannon" && %mode == 5)
//         %obj.client.mode[%weapon] = 0;  //goto 1
//   }
//    bottomPrint(%obj.client, "<color:ffffff>[Mode "@%obj.client.mode[%weapon]+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %obj.client.mode[%weapon]] NL $WeaponModeDescription[%weapon, %obj.client.mode[%weapon]], $ChangeWepTime, 2);
//   else if(%armor $= "BattleAngel") // just an example
//   {
//      if(%weapon $= "whatever" && %mode == 0)
//         %client.mode[%weapon]++; // skip mode 0
//   }
//}
//}
// MrKeen - Sorry guys, but this method is directly integrated into the mode change code, and your code didn't account for mode keys (directModifyMode())

function directModifyWeaponMode(%cl, %mode)
{
   %obj = %cl.player;

   if(!isObject(%obj))
        return;

   if(%obj.getMountedImage(0) != 0)
   {
      %weapon = %obj.getMountedImage(0).item;
      %armor = %obj.getArmorSize();

      if($WeaponModeCount[%weapon])
      {
         if($DenyMode[%armor, %weapon, %mode]) // MrKeen - still dynamic ;)
         {
            bottomPrint(%cl, "The mode you selected for this weapon on your current armor is not avaliable.", $ChangeWepTime, 1);
            return;
         }
         else
         {
            if(%mode <= $WeaponModeCount[%weapon])
            {
               %obj.play3D($WeaponModeSound[%weapon]);
               %cl.mode[%weapon] = %mode;

//               if(%cl.hasMD2Client)
//                    commandToClient(%cl, 'SetMode',  %cl.mode[%weapon], %weapon);
//               else
//               {
                   if($WeaponModeData[%weapon, %cl.mode[%weapon]] !$= "")
                        bottomPrint(%obj.client, "<color:ffffff>[Mode "@%cl.mode[%weapon]+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %cl.mode[%weapon]] NL $WeaponModeDescription[%weapon, %cl.mode[%weapon]] NL $WeaponModeData[%weapon, %cl.mode[%weapon]], $ChangeWepTime+2, 3);
                   else
                        bottomPrint(%obj.client, "<color:ffffff>[Mode "@%cl.mode[%weapon]+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %cl.mode[%weapon]] NL $WeaponModeDescription[%weapon, %cl.mode[%weapon]], $ChangeWepTime, 2);
//               }
                    
               %weapon.onCycleMode(%obj, %cl.mode[%weapon]);
            }
            else
               bottomPrint(%cl, "This weapon only has "@$WeaponModeCount[%weapon]+1@" fire modes.", $ChangeWepTime, 1);
         }
      }
      else
         bottomPrint(%cl, "This weapon does not have any fire modes.", $ChangeWepTime, 1);
   }
}

function serverCmdSelectWeaponSlot(%client, %data)
{
   if(%client.modekeys)
      directModifyWeaponMode(%client, %data);
   else
      %client.getControlObject().selectWeaponSlot( %data );
}

function ItemData::onCycleMode(%weapon, %obj, %mode)
{
   // dummy to eliminate console spam
}

 //-Nite-
$maxThrowStr = 2.5; // New throw str features

function serverCmdEndThrowCount(%client, %data)
{
   if(%client.player.throwStart == 0)
      return;

   // ---------------------------------------------------------------
   // z0dd - ZOD, 8/6/02. New throw str features
   %throwStrength = (getSimTime() - %client.player.throwStart) / 150;
   if(%throwStrength > $maxThrowStr)
      %throwStrength = $maxThrowStr;
   else if(%throwStrength < 0.5)
      %throwStrength = 0.5;
   // ---------------------------------------------------------------

   %throwScale = %throwStrength / 2;
   %client.player.throwStrength = %throwScale;

   %client.player.throwStart = 0;
}

function serverCmdthrowMaxEnd(%client, %data)		// +[soph]		
{							// + long overdue compatability
   %client.player.throwStrength = $maxThrowStr / 2 ;	// +
}							// +[/soph]