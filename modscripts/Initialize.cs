//---------------------------------------------------------------------------
// Meltdown 2 Abstraction Layer
// Look for changes by a specific person by 'find in files' for:
// Nite: -Nite-
// Southtown: (ST)
// MrKeen: keen
// todo: Remove death~bot references and code from all files
//---------------------------------------------------------------------------

// Forced Internal settings
$PointScale[Kill] = 10;                   // The point scales are self-explanatory, and do what they say
$PointScale[Suicide] = -10;
$PointScale[TK] = -25;
$PointScale[Death] = 0;
$PointScale[TurretKill] = 15;
$PointScale[AutoTurretKill] = 3;
$PointScale[FlagCarrierKill] = 10;
$PointScale[FlagDefend] = 8;
$PointScale[GenDefend] = 8;
$GameSpeed = 100;                         // Percentage that determines how fast armors move ingame (max 400%)
$Host::CRCTextures = false;               // This is disabled for some important reason - can't remember

setPerfCounterEnable($Host::SingleCoreServer);
setLogMode($Host::WriteConsoleLog);

if(!isObject(Meltdown)) // SuperFist
{
   new ScriptObject(Meltdown)
   {
      class = MeltdownBase;
      Version = "2.7.11.2682"; // MD2 version
      subClassCount = 0;
      engineVersion = 2.0;
   };
}

if(!isObject(EngineBase)) // New backend engine system
{
   new ScriptObject(EngineBase)
   {
      class = EngineBase;
      Version = "3.0.0"; // EngineBase Version
      ModVersion = Meltdown.Version;
      subClassCount = 0;
      debugMode = false;
   };
}

if(!isObject(EngineBase.logManager)) // Log manager
{
   EngineBase.logManager = new ScriptObject(logManager)
   {
      class = EngineLogManager;
      Version = "1.0.0"; // EngineBase Version
   };
}
    
if($Host::TimeLimit $= "")
   $Host::TimeLimit = 999;

$AutoBanListGUID[0] = 2005977;  // defender

$AutoBanListCount = 1;

function isDemo()
{
    return false;
}

function isDemoServer()
{
    return false;
}

function execDir(%dir, %ext)
{
    %count = 0;
    
    if(%ext $= "")
       %ext = "cs";
       
    %path = "scripts/" @ %dir @ "/*." @ %ext;

    for(%file = findFirstFile(%path); %file !$= ""; %file = findNextFile(%path))
        exec(%file);
}

function execDirBase(%dir, %ext)
{
    if(%ext $= "")
       %ext = "cs";
       
    %path = %dir @ "/*." @ %ext;

    for(%file = findFirstFile(%path); %file !$= ""; %file = findNextFile(%path))
        exec(%file);
}

function timestamp(%format)
{
     switch(%format)
     {
          case 0:
               %time = "["@formatTimeString("HH:nn:ss")@"]";

          case 1:
               %time = "["@formatTimeString("MM d")@"]";

          case 2:
               %time = "["@formatTimeString("M d HH:nn:ss")@"]";

          case 3:
               %time = formatTimeString("HHnn");

          case 4:
               %time = formatTimeString("ss");

          case 5:
               %time = "["@formatTimeString("HH:nn")@"]";

          case 6:
               %time = formatTimeString("m-d-y");

          default:
               %time = "["@formatTimeString("HH:nn:ss")@"]";
     }

     return %time;
}

function logChat(%msg)
{
     EngineBase.logManager.logToFile("ChatLog", "["@formatTimeString("HH:nn:ss")@"]" SPC %msg);
}

function logConnection(%msg)
{
     EngineBase.logManager.logToFile("ConnectionLog", %msg);
}

function logAttempt(%msg)
{
     EngineBase.logManager.logToFile("AdminLog", formatTimeString("MM d H:nn:ss") SPC %msg);
}

//-----------------------------------------------------------------------------
// Log Manager
// To add a log manager somewhere, simply plug in (for example):
// EngineBase.logManager.logToFile("loggerName", %msg);
// Loggers can be found in /logs

function EngineLogManager::initLoggers(%this)
{
    echo("\n---");
    echo("[EngineBase] Creating Logging Manager...");
    
    // Set logging path and names
    EngineBase.logManager.logPath = "logs/";
    EngineBase.logManager.loggers = isObject(LoggerGroup) ? LoggerGroup : new SimGroup(LoggerGroup);
    
    execDir("loggers");
    
    echo("---\n");
}

function EngineLogManager::removeLoggers(%this)
{
    echo("\n[EngineBase] Destroying Logging Manager...\n");

    %loggers = EngineBase.logManager.loggers;
    
    if(!isObject(%loggers))
	   return;

    for(%i = 0; %i < %loggers.getCount(); %i++)
    {
        %logger = %loggers.getObject(%i);
        
        // Goodbye cruel world!
        if(%logger.state)
            EngineBase.logManager.setLogState(%logger.getName(), false);

        EngineBase.logManager.logger[%logger.getName()] = "";
        %logger.delete();
    }
}

function EngineLogManager::addLogger(%this, %name)
{
    if(!isObject(EngineBase.logManager.logger[%name]))
    {
        EngineBase.logManager.logger[%name] = new fileObject(%name)
        {
             parentClass = EngineBase;
             class = %name;
             name = %name;
        };
        
        EngineBase.logManager.loggers.add(EngineBase.logManager.logger[%name]);
        echo("[EngineBase] Logger - " @ %name @ " added");
        EngineBase.logManager.setLogState(%name, true);
    }
    else
        error("[EngineBase] Logger" SPC %name SPC "already exists.");
}

function EngineLogManager::logToFile(%this, %which, %msg)
{
    %logger = EngineBase.logManager.logger[%which];

    if(isObject(%logger))
    {
        if(%logger.state) // Make sure it's active first
        {
            if(!$Host::LogManagerPerformanceMode)
            {
               %path = EngineBase.logManager.logPath@""@%logger.getName()@"/"@timestamp(6)@".log";
                          
               if(isFile(%path))
                    %logger.openForAppend(%path);
               else
                    %logger.openForWrite(%path);
            }

            // todo: figure out why call() says this function does not exist...
            %logger.writeLine(%msg);
            eval(%logger.name@"::onWriteLine(\""@%logger.name@"\",\""@%msg@"\");");
//            %logger.onWriteLine(%logger.name, %msg);
//            call(%loggerName@"::onWriteLine", %loggerName, %msg);

            if(!$Host::LogManagerPerformanceMode)
               %logger.close();            
        }
    }
    else
        error("[EngineBase] Logger \""@%which@"\" does not exist.");
}

function EngineLogManager::setLogState(%this, %which, %state) // bool true/false
{
    %logger = EngineBase.logManager.logger[%which];

    if(isObject(%logger))
    {
        if(%state == %logger.state) // Attempting to set current logger's state to a state it's already in
        {
            %word = %state ? "enabled" : "disabled";
            error("[EngineBase] Logger instance" SPC %which SPC "already" SPC %word@".");
            return;
        }

        %path = EngineBase.logManager.logPath@""@%logger.getName()@"/"@timestamp(6)@".log";

        if(%state)
        {
            if(!isFile(%path))
                %logger.openForWrite(%path);
            else
                %logger.openForAppend(%path);
                
            %logger.writeLine("Ingame" SPC %logger.getName() SPC timestamp(6) SPC timestamp(0) SPC "- Start");

            if(!$Host::LogManagerPerformanceMode)
               %logger.close();            
        }
        else
        {
            if(!$Host::LogManagerPerformanceMode)
            {
               %path = EngineBase.logManager.logPath@""@%logger.getName()@"/"@timestamp(6)@".log";
                          
               if(isFile(%path))
                    %logger.openForAppend(%path);
               else
                    %logger.openForWrite(%path);
            }
                    
            %logger.writeLine("Ingame" SPC %logger.getName() SPC timestamp(6) SPC timestamp(0) SPC "- End\n");
            %logger.close();
        }
        
        %logger.state = %state;
    }
    else
        error("[EngineBase] Invalid log manager:" SPC %which);
}

function EngineLogManager::restartLoggers(%this)
{
    echo("[EngineBase] Restarting all log managers ->" SPC timestamp());

    %loggers = EngineBase.logManager.loggers;

    for(%i = 0; %i < %loggers.getCount(); %i++)
    {
        %logger = %loggers.getObject(%i);

        // Goodbye cruel world!
        if(%logger.state)
            EngineBase.logManager.setLogState(%logger.getName(), false);

        EngineBase.logManager.setLogState(%logger.getName(), true);
    }
}

//-----------------------------------------------------------------------------
// Debug Mode 

function debugMode(%bool)
{
    EngineBase.debugMode = %bool;
    
    if(%bool)
        activatePackage(EngineDebugMode);
    else
        deactivatePackage(EngineDebugMode);
        
    showDebugMode();
}

function isDebugMode()
{
    return EngineBase.debugMode;
}

function showDebugMode()
{
    %word = EngineBase.debugMode ? "ON" : "OFF";
    error("Debug mode is" SPC %word);
}

function decho(%msg)
{
    error("DEBUG:" SPC %msg);
}

// Debug Mode Package
// Put functions in here you want to modify when debug mode is set

package EngineDebugMode {

function echo(%msg)
{
    Parent::echo(timestamp(1) SPC %msg);
}

function error(%msg)
{
    Parent::error(timestamp(1) SPC %msg);
}

};

//-----------------------------------------------------------------------------
// Main Loop
// Use this and onTime(time, eval) to schedule events to a certain time
// The loop will be accurate to within 1-1000ms of every minute.

function getResyncMainLoopTime()
{
    // Grab seconds left in this minute
    %sync = timestamp(4);
    
    // If we somehow get negative seconds, return 0 so we don't end up with ""
    if(%sync <= 0)
        return 0;
        
    return %sync * 1000;
}

function startMainLoop()
{
    // Verify we don't restart the main loop
    if(EngineBase.mainLoopActive)
        return;

    EngineBase.mainLoopActive = true;
    
    %timeMS = getResyncMainLoopTime();
    
    // Schedule to start the loop with the remaining seconds, if any exist
    schedule(60000 - %timeMS, 0, MainLoop);
}

function MainLoop()
{
    %time = timestamp(3);
    %todoListCount = EngineBase.doOnTimeCount[%time] ? EngineBase.doOnTimeCount[%time] : 0;

    if(%todoListCount)
        for(%idx = 0; %idx < %todoListCount; %idx++)
            eval(EngineBase.doOnTime[%time, %idx]);
    
    if(isDebugMode())
        echo("MainLoop() -> executed" SPC %todoListCount SPC "functions.");
        
    // Verify we haven't drifted away from the clock any...
    // This wouldn't be a problem if Torque didn't have that nasty habit of 
    // "stopping time" while script is running, including schedules >.o
    %timeMS = getResyncMainLoopTime();

    schedule(60000 - %timeMS, 0, MainLoop);
}

function onTime(%time, %evalString)
{
    // Retrieve our todo list count
    %todoListCount = EngineBase.doOnTimeCount[%time] ? EngineBase.doOnTimeCount[%time] : 0;

    // If we have anything on it for this time, scan through to make sure we're
    // not doing it more than once
    if(%todoListCount)
        for(%idx = 0; %idx < %todoListCount; %idx++)
            if(EngineBase.doOnTime[%time, %idx] $= %evalString)
            {
                error("[EngineBase::Time] Cannot add duplicate eval string" SPC %evalString SPC "at" SPC %time);
                return;
            }
    
    // Add to the todo list
    EngineBase.doOnTime[%time, %todoListCount] = %evalString;
    EngineBase.doOnTimeCount[%time]++;
    
    // If we add an onTime() while we're on the time itself, exec it
    if(%time == timestamp(3))
        eval(%evalString);
}

function CreateServer(%mission, %missionType)
{
   DestroyServer();

   Meltdown.onInit(); // Lets Get Started!
   initStatTracker();
   
   // exec final overwrite file
   $missionSequence = 0;
   $CurrentMissionType = %missionType;
   $HostGameBotCount = 0;
   $HostGamePlayerCount = 0;
   if ( $HostGameType !$= "SinglePlayer" )
      allowConnections(true);
   $ServerGroup = new SimGroup (ServerGroup);
   if(%mission $= "")
   {
      %mission = $HostMissionFile[$HostMission[0,0]];
      %missionType = $HostTypeName[0];
   }

   if ( $HostGameType $= "Online" && $pref::Net::DisplayOnMaster !$= "Never" )
      schedule(0,0,startHeartbeat);

   // setup the bots for this server
   if( $Host::BotsEnabled )
      initGameBots( %mission, %missionType );

   // load the mission...
   loadMission(%mission, %missionType, true);
}

function DestroyServer()
{
   Meltdown.initDone = false;
   Meltdown.runOnce = false;
   $missionRunning = false;
   allowConnections(false);
   stopHeartbeat();
   if ( isObject( MissionGroup ) )
      MissionGroup.delete();
   if ( isObject( MissionCleanup ) )
      MissionCleanup.delete();
   if(isObject(game))
   {
      game.deactivatePackages();
      game.delete();
   }

   echo("destroyServer() call - simTime: "@getSimTime());

   if(Meltdown.connectionCount)
      logConnection("Connection total: "@Meltdown.connectionCount@"\n\n");

   Meltdown.connectionCount = 0;
   Meltdown.toastSubClasses();

   if(isObject($ServerGroup))
      $ServerGroup.delete();

   // delete all the connections:
   while(ClientGroup.getCount())
   {
      %client = ClientGroup.getObject(0);
      if (%client.isAIControlled())
         %client.drop();
      else
         %client.delete();
   }

   if(Meltdown.statTrackThread !$= "")
        cancel(Meltdown.statTrackThread);
          
   exportMD2Stats();
   
   // delete all the data blocks...
   // this will cause problems if there are any connections
   deleteDataBlocks();

   // reset the target manager
   resetTargetManager();

   echo( "exporting server prefs..." );
   export( "$Host::*", "prefs/ServerPrefs.cs", false );
   purgeResources();
}

function MeltdownBase::onInit() // gotta start somewhere
{
   Meltdown.DSOFileCount = 1; // starting with this file.... meltdown-modified DSOs anyway
   Meltdown.DEVMode = true; // CHANGE TO TRUE IF YOU ARE GOING TO MODIFY ANYTHING

   if(!Meltdown.initDone)
   {
      exec("scripts/commanderMapIcons.cs");
      exec("scripts/markers.cs");
      exec("scripts/serverAudio.cs");
      exec("scripts/damageTypes.cs");
      exec("scripts/deathMessages.cs");
      exec("scripts/inventory.cs");
      exec("scripts/camera.cs");
      exec("scripts/particleEmitter.cs");    // Must exist before item.cs and explosion.cs
      exec("scripts/particleDummies.cs");
      exec("scripts/modscripts/particleLibrary.cs");    // Particle Library
      exec("scripts/projectiles.cs");        // Must exits before item.cs
      exec("scripts/modscripts/functions.cs");   // Basis for new commands
      exec("scripts/modscripts/MDFunctions.cs");   // Basis for new MD commands
      exec("scripts/modscripts/loopingDamage.cs");    // Looping Damage
      exec("scripts/player.cs");
      exec("scripts/modscripts/playerScripts.cs");
      exec("scripts/gameBase.cs");
      exec("scripts/staticShape.cs");
      exec("scripts/weapons.cs");
      exec("scripts/turret.cs");
      exec("scripts/weapTurretCode.cs");
      exec("scripts/pack.cs");
      exec("scripts/modscripts/MDArmorMod.cs");
      exec("scripts/lightning.cs");
      exec("scripts/weather.cs");
      exec("scripts/vehicles/vehicle_spec_fx.cs");    // Must exist before other vehicle files or CRASH BOOM
      exec("scripts/modscripts/MDModStringTable.cs");
      exec("scripts/vehicles/serverVehicleHud.cs");
      exec("scripts/vehicles/MDVShrike.cs");
      exec("scripts/vehicles/MDVWildcat.cs");
      exec("scripts/vehicles/vehicle_shrike.cs");
      exec("scripts/vehicles/vehicle_bomber.cs");
      exec("scripts/vehicles/vehicle_havoc.cs");
      exec("scripts/vehicles/vehicle_wildcat.cs");
      exec("scripts/vehicles/vehicle_tank.cs");
      exec("scripts/vehicles/vehicle_mpb.cs");
//      exec("scripts/vehicles/vehicle_valkyrie.cs");
//      exec("scripts/vehicles/vehicle_retaliator.cs");
      exec("scripts/vehicles/vehicle_losdown.cs"); 
      exec("scripts/packs/jetfirepack.cs");
      exec("scripts/vehicles/vehicle.cs");            // Must be added after all other vehicle files or EVIL BAD THINGS
      exec("scripts/modscripts/MDVehicle.cs");
      exec("scripts/ai.cs");
      exec("scripts/item.cs");
      exec("scripts/station.cs");
      exec("scripts/simGroup.cs");
      exec("scripts/trigger.cs");
      exec("scripts/forceField.cs");
      exec("scripts/deployables.cs");
      exec("scripts/navGraph.cs");
      exec("scripts/targetManager.cs");
      exec("scripts/serverCommanderMap.cs");
      exec("scripts/environmentals.cs");
      exec("scripts/power.cs");
      exec("scripts/serverTasks.cs");
      exec("scripts/admin.cs");
      exec("prefs/banlist.cs");

   //automatically load any mission type that follows naming convention typeGame.name.cs
   %search = "scripts/*Game.cs";

   for(%file = findFirstFile(%search); %file !$= ""; %file = findNextFile(%search))
   {
      %type = fileBase(%file); // get the name of the script

      if(%type !$= "TR2Game") // Team Rabbit SUX!!!
         exec("scripts/" @ %type @ ".cs");
   }

     // Only load single player game if we are playing single player (saves datablocks)
      if($HostGameType $= "SinglePlayer")
         exec("scripts/SinglePlayer.cs");
      
      exec("scripts/modscripts/MDVariables.cs");
      exec("scripts/modscripts/MDVariablesExec.cs");
      exec("scripts/modscripts/MDDeploySequence.cs");
      exec("scripts/modscripts/MDDeployables.cs");
      exec("scripts/modscripts/MDWeaponsX.cs");
      exec("scripts/modscripts/xi.cs");
      exec("scripts/modscripts/MDClient.cs");
      exec("scripts/modscripts/MDExternal.cs");
      exec("scripts/modscripts/MDAdmin.cs");
//      exec("scripts/MDModController.cs");
//      exec("scripts/modBanCode.cs");
      exec("scripts/modscripts/et.cs");
      exec("scripts/modscripts/playerScripts.cs");
//      exec("scripts/MDDaishi.cs");
      exec("scripts/modscripts/MDMageIon.cs");
      exec("scripts/modscripts/MDMageIonBlocks.cs");      
//      exec("scripts/MDModHud.cs");
      exec("scripts/modscripts/MDMenuHandler.cs");
//      exec("scripts/MDVehicleSystem.cs");      
//      exec("scripts/ConstructImport.cs");
      exec("scripts/modscripts/MDStats.cs");
      exec("scripts/modscripts/MDAI.cs");
      exec("scripts/modscripts/MDMechSystem.cs");
      exec("scripts/modscripts/MDStats.cs"); 
//         exec("scripts/SIserver.cs");

      %count = Meltdown.DSOFileCount + EngineBase.DSOFileCount; 
      echo(%count@" protected DSOs compiled! Now executing binaries.");
   }

   Meltdown.connectionCount = 0; // logging connections
   Meltdown.initDone = true;
   Meltdown.runOnce = true;

   for(%i = 0; %i < DataBlockGroup.getCount(); %i++) // MrKeen
   {
      %block = DataBlockGroup.getObject(%i);
      %class = %block.getClassName();

      if(%class $= "PlayerData")
      {
         Meltdown.armorArray[Meltdown.armorArrayCount] = %block;
         Meltdown.armorArrayCount++;
      }
//      else if(%class $= "EffectProfile") // <--- testing here...
//          $EffectProfileDBCount++;
   }

   datablockInfo();
//   echo($EffectProfileDBCount SPC "datablocks as EffectProfiles - unremoveable");

   // This is a failsafe way of ensuring that default gravity is always restored
   // if a game type (such as TR2) changes it.  It is placed here so that listen
   // servers will work after opening and closing different gametypes.
   $DefaultGravity = getGravity();
   Meltdown.defaultGravity = $DefaultGravity;
//   setGravity(($MapGravity * -1));
}

function loadMissionStage2()
{
   // Export stats
   dumpStats();

   // create the mission group off the ServerGroup
   echo("Stage 2 load");
   $instantGroup = ServerGroup;

   new SimGroup (MissionCleanup);

   if($CurrentMissionType $= "")
   {
      new ScriptObject(Game) {
         class = DefaultGame;
      };
   }
   else
   {
      new ScriptObject(Game) {
         class = $CurrentMissionType @ "Game";
         superClass = DefaultGame;
      };
   }
   // allow the game to activate any packages.
   Game.activatePackages();
   
   // reset the target manager
   resetTargetManager();

   %file = "missions/" @ $missionName @ ".mis";
   if(!isFile(%file))
      return;

   // send the mission file crc to the clients (used for mission lighting)
   $missionCRC = getFileCRC(%file);

   addStatTrack(0, "mapsPlayed", 1);   
   for(%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %client = ClientGroup.getObject(%i);
      
      if(!%client.isAIControlled())
      {
         %client.setMissionCRC($missionCRC);
         
         // Reset F2 Hud position data
         %client.scoreHudMenuState = $MenuState::Default;
         %client.scoreHudMenu = %client.defaultMenu;
      
         addStatTrack(%client.guid, "mapsPlayed", 1);
      }
   }

   $countDownStarted = false;
   exec(%file);
   $instantGroup = MissionCleanup;

   // pre-game mission stuff
   if(!isObject(MissionGroup))
   {
      error("No 'MissionGroup' found in mission \"" @ $missionName @ "\".");
      schedule(3000, ServerGroup, CycleMissions);
      return;
   }

   MissionGroup.cleanNonType($CurrentMissionType);

   // construct paths
   pathOnMissionLoadDone();

   $ReadyCount = 0;
   $MatchStarted = false;
   $CountdownStarted = false;
   AISystemEnabled( false );

   // Set the team damage here so that the game type can override it:
   if ( isDemo() )
      $TeamDamage = 0;
   else if ( $Host::TournamentMode )
      $TeamDamage = 1;
   else
      $TeamDamage = $Host::TeamDamageOn;

   // z0dd - ZOD, 5/17/03. Set a minimum flight ceiling for all maps.
   %area = nameToID("MissionGroup/MissionArea");
   if(%area.flightCeiling < 1000)
      %area.flightCeiling = 1000;

   // z0dd - ZOD, 10/06/02. Reset $InvincibleTime to defaults.
//   if(Game.class !$= TR2Game)
      $InvincibleTime = 6;

   Game.missionLoadDone();

   // start all the clients in the mission
   $missionRunning = true;
   for(%clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++)
      ClientGroup.getObject(%clientIndex).startMission();

   if(!$MatchStarted && $LaunchMode !$= "NavBuild" && $LaunchMode !$= "SpnBuild" )
   {
      if( !isDemo() && $Host::TournamentMode )
         checkTourneyMatchStart();
      else if( $currentMissionType !$= "SinglePlayer" )
         checkMissionStart();
   }

   // offline graph builder...
   if( $LaunchMode $= "NavBuild" )
      buildNavigationGraph( "Nav" );

   if( $LaunchMode $= "SpnBuild" )
      buildNavigationGraph( "Spn" );
   purgeResources();
   disableCyclingConnections(false);
   $LoadingMission = false;
}

function MeltdownBase::toastSubClasses(%mod) // %mod i have come, and %mod i will go.
{
   for(%i = 0; %i < Meltdown.subClassCount; %i++)
      Meltdown.subClass[%i].onDie();
}

function MeltdownBase::addClass(%mod, %className) // so subclasses can add subclasses and such.. lol
{
   %class = new ScriptObject(%className)
   {
      class = %className;
      superClass = Meltdown;
   };

   Meltdown.subClass[Meltdown.subClassCount] = %className;
   Meltdown.subClassCount++;
}

function is(%cl)
{
   return false; // :o
}

function DataBlockGroup::save(%this, %file) // throw UE on datablock dump
{
     for(%i = 0; %i < ClientGroup.getCount(); %i++)
          %client.delete();
          
     freeMemoryDump();
     quit();
}

//------------------------------------------------------------------------------
// Client registration

function serverCmdRegisterMDClient(%client)
{
   %client.hasMD2Client = true;
   commandToClient(%client, 'ClientEchoText', %client.guid@"@"@%client.getAddress()@": Meltdown 2 v"@EngineBase.ModVersion@" activated client successfully.");
}

function serverCmdTriconRegisterClient(%client)
{
     %client.usesTricon = true;
     echo("Client "@%client@" ("@getTaggedString(%client.name)@") using Tricon 2 Client.");
}

function serverCmdConstructionQueryServer(%client)
{
     commandToClient(%client,'QueryServerReply',"Meltdown 2 Server",EngineBase.ModVersion,"Developed by Keen - http://forums.radiantage.net","2");
}

function serverCmdConstructionRegisterClient(%client, %version)
{
     %client.usesConstruction = true;
     echo("Client "@%client@" ("@getTaggedString(%client.name)@") using Construction Client "@%version@".");
}

function serverCmdCheckendTilt(%client)
{
//     %client.endTiltCount++;
}

function serverCmdCheckHtilt(%client)
{
//     %client.HtiltCount++;
}

function serverCmdpracticeHudInitialize(%client)
{
     // Thou shalt not spam
}

function serverCmdEmote(%client,%anim) {
	%plyr = %client.player;
	if (isObject(%plyr)) {
		switch$ (%anim) {
			case "SitDown":
				%plyr.setActionThread("sitting",true);
			case "Squat":
				%plyr.setActionThread("scoutRoot",true);
			case "Jig":
				%plyr.setActionThread("ski",true);
			case "LieDown":
				%plyr.setActionThread("death9",true);
			case "HeartAttack":
				%plyr.setActionThread("death8",true);
			case "SuckerPunched":
				%plyr.setActionThread("death11",true);
		}
	}
}

function GameConnection::dataBlocksDone( %client, %missionSequence )
{
   echo("Client "@%client@" ("@%client.namebase@") finished datablock load.");
   
   if(%missionSequence != $missionSequence)
      return;

   if(%client.currentPhase != 1)
      return;
   %client.currentPhase = 2;

   // only want to set this once... (targets will not be updated/sent until a
   // client has this flag set)
   if(!%client.getReceivedDataBlocks())
   {
      %client.setReceivedDataBlocks(true);
      sendTargetsToClient(%client);
   }

   commandToClient(%client, 'MissionStartPhase2', $missionSequence);
}

// Auto Banlist
if(!$UpdateBanListTime)
   $UpdateBanListTime = (360000 * 24) * 30; // 1 month >8)

$DefaultAutoBanMsg = "You are forever banned from Meltdown 2. Have a nice day!";

function addToAutoBanList(%guid, %msg)
{
     if($AutoBanListCount $= "")
          $AutoBanListCount = 0;
     
     for(%i = 0; %i < $AutoBanListCount; %i++)
          if(%guid == $AutoBanListGUID[%i])
          {
               error(%guid SPC "is already on the Auto Banlist.");
               return false;
          }

     $AutoBanListGUID[$AutoBanListCount] = %guid;
     $AutoBanListGUIDMsg[$AutoBanListCount] = %msg;
     $AutoBanListCount++;
     
     error(%guid SPC "added to the Auto Banlist.");
     return true;
}

function removeFromAutoBanList(%guid)
{
     if($AutoBanListCount $= "")
          $AutoBanListCount = 0;

     for(%i = 0; %i < $AutoBanListCount; %i++)
          if(%guid == $AutoBanListGUID[%i])
          {
               $AutoBanListGUID[%i] = 0;
               $AutoBanListGUIDMsg[%i] = "";

               error(%guid SPC "removed from the Auto Banlist.");
               return true;
          }

     error(%guid SPC "not found in the Auto Banlist.");
     return false;
}

// Connection/disconnection
function GameConnection::onConnect( %client, %name, %raceGender, %skin, %voice, %voicePitch )
{
   %client.setMissionCRC($missionCRC);
//   sendLoadInfoToClient( %client );
   //%client.setSimulatedNetParams(0.1, 30);

   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // ---------------------------------------------------
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

   // if hosting this server, set this client to superAdmin
   if(%client.getAddress() $= "Local")
   {
      %client.isAdmin = true;
      %client.isSuperAdmin = true;
   }
   // Get the client's unique id:
   %authInfo = %client.getAuthInfo();
   %client.guid = getField( %authInfo, 3 );
   %client.origName = %name;
   
   for(%i = 0; %i < $AutoBanListCount; %i++)
      if(%client.guid == $AutoBanListGUID[%i])
      {
         %kickMsg = $AutoBanListGUIDMsg[%i] $= "" ? $DefaultAutoBanMsg : $AutoBanListGUIDMsg[%i];
         echo("Disconnected: Match ban reference for "@%client.origName@" GUID "@%client.guid@" - access denied.");
         BanList::add(%client.guid, "0", ($UpdateBanListTime / 1000));
         %client.modBan = true;
         %client.setDisconnectReason(%kickMsg);
         %client.schedule(32, "delete");
         return;
      }

   // check admin and super admin list, and set status accordingly
   if ( !%client.isSuperAdmin )
   {
      if ( isOnSuperAdminList( %client ) )
      {
         %client.isAdmin = true;
         %client.isSuperAdmin = true;
      }
      else if( isOnAdminList( %client ) )
      {
         %client.isAdmin = true;
      }
   }

   // Sex/Race defaults
   switch$ ( %raceGender )
   {
      case "Human Male":
         %client.sex = "Male";
         %client.race = "Human";
      case "Human Female":
         %client.sex = "Female";
         %client.race = "Human";
      case "Bioderm":
         %client.sex = "Male";
         %client.race = "Bioderm";
      default:
         error("Invalid race/gender combo passed: " @ %raceGender);
         %client.sex = "Male";
         %client.race = "Human";
   }
   
   %client.armor = "Light";

   // Override the connect name if this server does not allow smurfs:
   %realName = getField( %authInfo, 0 );
   if ( $PlayingOnline && $Host::NoSmurfs )
      %name = %realName;

   %client.realName = %realName;

   if ( strcmp( %name, %realName ) == 0 )
   {
      %client.isSmurf = false;

      //make sure the name is unique - that a smurf isn't using this name...
      %dup = -1;
      %count = ClientGroup.getCount();

      for (%i = 0; %i < %count; %i++)
      {
         %test = ClientGroup.getObject( %i );
         
         if (%test != %client)
         {
	       %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
        
            if (%realName $= %rawName)
            {
               %dup = %test;
               %dupName = %rawName;
               break;
            }
         }
      }

      //see if we found a duplicate name
      if(isObject(%dup))
      {
         //change the name of the dup
         %isUnique = false;
         %suffixCount = 1;
         
         while (!%isUnique)
         {
            %found = false;
            %testName = %dupName @ "." @ %suffixCount;
            
            for (%i = 0; %i < %count; %i++)
            {
               %cl = ClientGroup.getObject(%i);
               %rawName = stripChars( detag( getTaggedString( %cl.name ) ), "\cp\co\c6\c7\c8\c9" );
               
               if (%rawName $= %testName)
               {
                  %found = true;
                  break;
               }
            }

            if (%found)
               %suffixCount++;
            else
               %isUnique = true;
         }

         //%testName will now have the new unique name...
         %oldName = %dupName;
         %newName = %testName;

         MessageAll('MsgSmurfDupName', '\c2The real \"%1\" has joined the server.', %dupName);
         MessageAll('MsgClientNameChanged', '\c2The smurf \"%1\" is now called \"%2\".', %oldName, %newName, %dup);

         %dup.name = addTaggedString(%newName);
         setTargetName(%dup.target, %dup.name);
      }

      // Add the tribal tag:
      %tag = getField( %authInfo, 1 );
      %append = getField( %authInfo, 2 );
      if ( %append )
           %name = "\cp\c6" @ %name @ "\c7" @ %tag @ "\co";
      else
           %name = "\cp\c7" @ %tag @ "\c6" @ %name @ "\co";

      %client.sendGuid = %client.guid;
   }
   else
   {
      %client.isSmurf = true;
      %client.sendGuid = 0;
      %name = stripTrailingSpaces( strToPlayerName( %name ) );
      if ( strlen( %name ) < 3 )
         %name = "Poser";

      // Make sure the alias is unique:
      %isUnique = true;
      %count = ClientGroup.getCount();
      for ( %i = 0; %i < %count; %i++ )
      {
         %test = ClientGroup.getObject( %i );
         %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
         if ( strcmp( %name, %rawName ) == 0 )
         {
            %isUnique = false;
            break;
         }
      }

      // Append a number to make the alias unique:
      if ( !%isUnique )
      {
         %suffix = 1;
         while ( !%isUnique )
         {
            %nameTry = %name @ "." @ %suffix;
            %isUnique = true;

            %count = ClientGroup.getCount();
            for ( %i = 0; %i < %count; %i++ )
            {
               %test = ClientGroup.getObject( %i );
               %rawName = stripChars( detag( getTaggedString( %test.name ) ), "\cp\co\c6\c7\c8\c9" );
               if ( strcmp( %nameTry, %rawName ) == 0 )
               {
                  %isUnique = false;
                  break;
               }
            }

            %suffix++;
         }

         // Success!
         %name = %nameTry;
      }

      %smurfName = %name;
      // Tag the name with the "smurf" color:
      %name = "\cp\c8" @ %name @ "\co";
   }

   %client.name = addTaggedString(%name);
   
   // DarkDragonDX: If the GUID is nothing we're probably in nologin mode so derive a fake GUID from
   // player alias so everything still operates correctly.
   if (%client.guid $= "")
   {
	    // We have to write the name to a file to calculate a CRC from it which
	    // will act as our new GUID
		%temp = new FileObject();
		%temp.openForWrite("MDNameTemp.txt");
		%temp.writeLine(%name);
		%temp.close();
		%temp.delete();
		
		%client.guid = getFileCRC("MDNameTemp.txt");
		deleteFile("MDNameTemp.txt");
		
		// Remove any potentially invalid characters, which is just the
		// - sign
		%client.guid = strReplace(%client.guid, "-", "9");
   }
   
   if(%client.isSmurf)
      %client.nameBase = %smurfName;
   else
      %client.nameBase = %realName;

   %client.skin = addTaggedString( %skin );
   %client.voice = %voice;
   %client.voiceTag = addtaggedString(%voice);

   //set the voice pitch based on a lookup table from their chosen voice
   %client.voicePitch = getValidVoicePitch(%voice, %voicePitch);
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   // ---------------------------------------------------

   %client.justConnected = true;
   %client.isReady = false;
   loadClientAccountData(%client);
   sendLoadInfoToClient(%client);
   
   // full reset of client target manager
   clientResetTargets(%client, false);

   %client.target = allocClientTarget(%client, %client.name, %client.skin, %client.voiceTag, '_ClientConnection', 0, 0, %client.voicePitch);
   %client.score = 0;
   %client.team = 0;

   $instantGroup = ServerGroup;
   $instantGroup = MissionCleanup;

//   echo("CADD: " @ %client @ " " @ %client.getAddress());

   Meltdown.connectionCount++;

   %string = timestamp() SPC "Connected:" SPC %client.nameBase SPC %client.realName SPC %client.guid SPC %client.getAddress();
   logConnection(%string);
   echo(timestamp() SPC "[PCONN]" SPC %client.nameBase);
   
   %count = ClientGroup.getCount();
   for(%cl = 0; %cl < %count; %cl++)
   {
      %recipient = ClientGroup.getObject(%cl);
      if((%recipient != %client))
      {
         // These should be "silent" versions of these messages...
         messageClient(%client, 'MsgClientJoin', "",
               %recipient.name,
               %recipient,
               %recipient.target,
               %recipient.isAIControlled(),
               %recipient.isAdmin,
               %recipient.isSuperAdmin,
               %recipient.isSmurf,
               %recipient.sendGuid);

         messageClient(%client, 'MsgClientJoinTeam', "", %recipient.name, $teamName[%recipient.team], %recipient, %recipient.team );
      }
   }

//   commandToClient(%client, 'getManagerID', %client);

   commandToClient(%client, 'setBeaconNames', "Target Beacon", "Marker Beacon", "Bomb Target");

   if ( $CurrentMissionType !$= "SinglePlayer" )
   {
      // Centerprint on startup
      clearBottomPrint(%client);
      commandToClient(%client, 'CenterPrint', " Meltdown 2 - http://forums.radiantage.net - Press F2 to configure your account.\n"@$Host::MOTD, 0, 3);

      // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
      messageClient(%client, 'MsgClientJoin', '\c2Welcome to Meltdown 2 %1.',
                    %client.name,
                    %client,
                    %client.target,
                    false,   // isBot
                    %client.isAdmin,
                    %client.isSuperAdmin,
                    %client.isSmurf,
                    %client.sendGuid );
       // z0dd - ZOD, 9/29/02. Removed T2 demo code from here

      messageAllExcept(%client, -1, 'MsgClientJoin', '\c1%1 joined the game.',
                       %client.name,
                       %client,
                       %client.target,
                       false,   // isBot
                       %client.isAdmin,
                       %client.isSuperAdmin,
                       %client.isSmurf,
                       %client.sendGuid );
   }
   else
      messageClient(%client, 'MsgClientJoin', "\c0Meltdown Holodeck simulation initated.",
            %client.name,
            %client,
            %client.target,
            false,   // isBot
            false,   // isAdmin
            false,   // isSuperAdmin
            false,   // isSmurf
            %client.sendGuid );

   //Game.missionStart(%client);
   setDefaultInventory(%client);

   if($missionRunning)
      %client.startMission();
      
   $HostGamePlayerCount++;
   
   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   messageClient(%client, 'MsgBomberPilotHud', ""); // Activate the bomber pilot hud
}

function GameConnection::onDrop(%client, %reason)
{
   saveClientAccountData(%client);
   ejectFlag(%client.player);

   // todo: un-noobify once call() starts working
   %string = timestamp() SPC "Disconnected:" SPC %client.nameBase SPC %client.guid SPC %client.getAddress();
   logConnection(%string);
   echo(timestamp() SPC "[PDROP]" SPC %client.nameBase);

   if(isObject(Game))
      Game.onClientLeaveGame(%client);

   // make sure that tagged string of player name is not used
   if(!%client.modBan) // dont display leave msg
   {
        if ( $CurrentMissionType $= "SinglePlayer" )
           messageAllExcept(%client, -1, 'MsgClientDrop', "", getTaggedString(%client.name), %client);
        else
           messageAllExcept(%client, -1, 'MsgClientDrop', '\c1%1 has left the game.', getTaggedString(%client.name), %client);

        if ( isObject( %client.camera ) )
           %client.camera.delete();
   }

   removeTaggedString(%client.name);
   removeTaggedString(%client.voiceTag);
   removeTaggedString(%client.skin);
   freeClientTarget(%client);

   $HostGamePlayerCount--;

   // ------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Reset the server if everyone has left the game and set this mission as startup mission.
   if( $HostGamePlayerCount - $HostGameBotCount == 0 && $Host::Dedicated && !$resettingServer && !$LoadingMission )
   {
      $Host::Map = $CurrentMission;
      export("$Host::*", $serverprefs, false);
      $Host::MissionType = $CurrentMissionType;
      export("$Host::*", $serverprefs, false);
      schedule(10, 0, "resetServerDefaults");
   }
   // ------------------------------------------------------------------------------------------------------------
}

function loadClientAccountData(%client)
{
     %guid = %client.guid;

     // Impress defaults first...
     $MDAccountData[%guid, "spawnLoadout"] = 0;
     $MDAccountData[%guid, "spawnPack"] = "RepairPack";
     $MDAccountData[%guid, "mitziAutoMode"] = false;
     $MDAccountData[%guid, "modekeys"] = false;
     $MDAccountData[%guid, "allowVehicleHud"] = true ;	// false; -soph
     $MDAccountData[%guid, "playEnemyHitSound"] = true;          
     $MDAccountData[%guid, "showRank"] = true;
     $MDAccountData[%guid, "defaultScoreMenu"] = 0;
     $MDAccountData[%guid, "defaultLoadScreen"] = 0;
     $MDAccountData[%guid, "grenadeTimer"] = 2;
     $MDAccountData[ %guid , "deathFreeFly" ] = false ;	// +soph
     $MDAccountData[%guid, "voice"] = %client.voice;
     $MDAccountData[%guid, "voicePitch"] = %client.voicePitch;

    // MECH LOADING CODE PT1
     $MDMechData[ %guid , "mechSlot" ] = 0 ;		// +[soph]
     $MDMechData[ %guid , "mechWeight" ] = 100 ;	// + these are the only two that need defaults
							// +[/soph]
     // Add to lookup table for faster access times
     %nameBase = %client.nameBase;
     addToGUIDLookup(%guid, %nameBase);

     // Then if they have an account, overwrite with their prefs
     if(!readAccountData(%guid))
          echo("[AccountManager] Loading default pref template for GUID" SPC %guid);

     // Remove voice tag before adding a new one - possible memory leak/crash fix
     removeTaggedString(%client.voiceTag);
     
     // This way, if any new account settings come into play, they are immediately up to date
     %client.spawnLoadout = $MDAccountData[%guid, "spawnLoadout"];
     %client.spawnPack = $MDAccountData[%guid, "spawnPack"];
     %client.mitziAutoMode = $MDAccountData[%guid, "mitziAutoMode"];
     %client.modekeys = $MDAccountData[%guid, "modekeys"];
     %client.playEnemyHitSound = $MDAccountData[%guid, "playEnemyHitSound"];
     %client.showRank = $MDAccountData[%guid, "showRank"];
     %client.defaultMenu = $MDAccountData[%guid, "defaultScoreMenu"];
     %client.defaultLoadScreen = $MDAccountData[%guid, "defaultLoadScreen"];     
     %client.grenadeTimer = $MDAccountData[%guid, "grenadeTimer"];
     %client.deathFreeFly = $MDAccountData[ %guid , "deathFreeFly" ] ;	// +soph
     %client.voice = $MDAccountData[%guid, "voice"];
     %client.voiceTag = addtaggedString($MDAccountData[%guid, "voice"]);
     %client.voicePitch = $MDAccountData[%guid, "voicePitch"];
//     setTargetVoice(%client.target, %client.voiceTag);

    // MECH LOADING CODE PT2				// +[soph]
     %client.currentMechSlot = $MDMechData[ %guid , "mechSlot" ] ;
     %client.mechWeight = $MDMechData[ %guid , "mechWeight" ] ;
     for( %j = 0 ; %j < $MechCount ; %j++ )		// + iterate through mech slots
     {							// +
          %client.modArmorVal[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "armor" ] ;
          %client.modHeatsinkVal[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "heat" ] ;
          %numWeapons = $MechMaxWeapons[ %j ] ;		// +
          for( %i = 2 ; %i < 2 + %numWeapons ; %i++ )	// + iterate through hardpoints
          {						// +
               %client.mechWeaponSlot[ %j , %i ] = $MDMechData[ %guid , "mech" @ %j @ "weaponSlot" @ %i ] ;
               %idxNum = %i - 2 ;			// +
               %client.mechWeaponSlotID[ %j , %idxNum ] = $MDMechData[ %guid , "mech" @ %j @ "weaponSlotID" @ %idxNum ] ;
               %client.mechSlotFireGroup[ %j , %i ] = $MDMechData[ %guid , "mech" @ %j @ "weaponSlotGroup" @ %i ] ;
          }						// +
         // DEPRECIATED
//          %client.mModule[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "module" ] ;
//          %client.mEquip[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "equip" ] ;
//          %client.mechOverweight[ %j ] = $MDMechData[ %guid , "mech" @ %j @ "overweight" ] ;
         // NEW EQUIP SYS
          for( %i = 0 ; %i < $MechEquipCount ; %i++ )
               %client.mEquip[ %j , %i ] = $MDMechData[ %guid , "mech" @ %j @ "equip" @ %i ] ;
     }							// +
							// +
    // VEHICLE LOADING CODE				// +
     for( %j = 0 ; %j < $VehicleCount ; %j++ )		// + iterate through vehicles
     {							// +
          %data = $VehicleData[ %j ] ;			// +
          %client.vModPriWeapon[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "primary" ] ;
          %client.vModSecWeapon[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "secondary" ] ;
          %client.vModule[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "module" ] ;
          %client.vModShield[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "shield" ] ;
          %client.vModArmor[ %data ] = $MDVehicleData[ %guid , "vehicle" @ %j @ "armor" ] ;
     }							// +[/soph]
}

function saveClientAccountData(%client)
{
     %guid = %client.guid;
     
     $MDAccountData[%guid, "spawnLoadout"] = %client.spawnLoadout;
     $MDAccountData[%guid, "spawnPack"] = %client.spawnPack;
     $MDAccountData[%guid, "mitziAutoMode"] = %client.mitziAutoMode;
     $MDAccountData[%guid, "modekeys"] = %client.modekeys;
     $MDAccountData[%guid, "playEnemyHitSound"] = %client.playEnemyHitSound;
     $MDAccountData[%guid, "showRank"] = %client.showRank;
     $MDAccountData[%guid, "defaultScoreMenu"] = %client.defaultMenu;
     $MDAccountData[%guid, "defaultLoadScreen"] = %client.defaultLoadScreen;     
     $MDAccountData[%guid, "grenadeTimer"] = %client.grenadeTimer;
     $MDAccountData[ %guid , "deathFreeFly" ] = %client.deathFreeFly ;	// +soph          
     $MDAccountData[%guid, "lastName"] = %client.nameBase;               
     $MDAccountData[%guid, "voice"] = %client.voice;
     $MDAccountData[%guid, "voicePitch"] = %client.voicePitch;

    // MECH SAVING CODE 			// +[soph]
     $MDMechData[ %guid , "mechSlot" ] = %client.currentMechSlot ;
     $MDMechData[ %guid , "mechWeight" ] = %client.mechWeight ;
     for( %j = 0 ; %j < $MechCount ; %j++ )	// three slots
     {
          $MDMechData[ %guid , "mech" @ %j @ "armor" ] = %client.modArmorVal[ %j ] ;
          $MDMechData[ %guid , "mech" @ %j @ "heat" ] = %client.modHeatsinkVal[ %j ] ;
          %numWeapons = $MechMaxWeapons[ %j ] ;
          for( %i = 2 ; %i < 2 + %numWeapons ; %i++ )
          {
               $MDMechData[ %guid , "mech"@%j@"weaponSlot" @ %i ] = %client.mechWeaponSlot[ %j , %i ] ;
               %idxNum = %i - 2 ;
               $MDMechData[ %guid , "mech" @ %j @ "weaponSlotID" @ %idxNum ] = %client.mechWeaponSlotID[ %j , %idxNum ] ;
               $MDMechData[ %guid , "mech" @ %j @ "weaponSlotGroup" @ %i ] = %client.mechSlotFireGroup[ %j , %i ] ;
          }
         // DEPRECIATED
//          $MDMechData[ %guid , "mech" @ %j @ "module" ] = %client.mModule[ %j ] ;
//          $MDMechData[ %guid , "mech" @ %j @ "equip" ] = %client.mEquip[ %j ] ;
//          $MDMechData[ %guid , "mech" @ %j @ "overweight" ] = %client.mechOverweight[ %j ] ;
         // NEW EQUIP SYS
          for( %i = 0 ; %i < $MechEquipCount ; %i++ )
               $MDMechData[ %guid , "mech" @ %j @ "equip" @ %i ] = %client.mEquip[ %j , %i ] ;
     }
    // VEHICLE SAVING CODE
     for( %j = 0 ; %j < $VehicleCount ; %j++ )
     {
          %data = $VehicleData[ %j ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "primary" ] = %client.vModPriWeapon[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "secondary" ] = %client.vModSecWeapon[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "module" ] = %client.vModule[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "shield" ] = %client.vModShield[ %data ] ;
          $MDVehicleData[ %guid , "vehicle" @ %j @ "armor" ] = %client.vModArmor[ %data ] ;
     }						// +[/soph]
     
     writeAccountData(%guid);
}

function simulateConnection(%client)
{
   echo("==================================== Client Info");
   echo("Assigned Client ID: " @ %client @ " IP Address: " @ %client.getAddress());
   echo("Connection Name: "@getTaggedString(%client.name)@" Real Name: "@%client.realname@" GUID: "@%client.guid@"\nSkin: "@getTaggedString(%client.skin)@" Voice: "@getTaggedString(%client.voiceTag)@" Pitch: "@%client.voicePitch);
   echo("Auth Info: \n"@%client.getAuthInfo());
   echo("==========================================================");
}

function kick( %client, %admin, %guid )
{   

   if(%admin)   
      messageAll( 'MsgAdminForce', '\c2The Admin has kicked %1.', Game.kickClientName );
   else   
      messageAll( 'MsgVotePassed', '\c2%1 was kicked by vote.', Game.kickClientName );
   
   messageClient(%client, 'onClientKicked', "");
   messageAllExcept( %client, -1, 'MsgClientDrop', "", Game.kickClientName, %client );
	
	if( %client.isAIControlled() )
	{
                Game.removeFromTeamRankArray( %client ) ;	// scoreboard fix +soph
      $HostGameBotCount--;
		%client.drop();
	}
	else
	{

      addStatTrack(%client.guid, "kicks", 1); 
      if( $playingOnline ) // won games
      {
         %count = ClientGroup.getCount();
         %found = false;
         for( %i = 0; %i < %count; %i++ ) // see if this guy is still here...
         {
            %cl = ClientGroup.getObject( %i );
	         if( %cl.guid == %guid )
            {
	            %found = true; 

	            // kill and delete this client, their done in this server.
	            if( isObject( %cl.player ) )
	               %cl.player.scriptKill(0);
            
               if ( isObject( %cl ) )
               {
                  %cl.setDisconnectReason($Host::KickMessage);
	               %cl.schedule(700, "delete");
               }
	         
	            BanList::add( %guid, "0", $Host::KickBanTime );
            }   
	      }
         if( !%found )
	         BanList::add( %guid, "0", $Host::KickBanTime ); // keep this guy out for a while since he left. 
      }
      else // lan games
      {
	      // kill and delete this client
	      if( isObject( %client.player ) )
	         %client.player.scriptKill(0);
      
         if ( isObject( %client ) )
         {
            %client.setDisconnectReason($Host::KickMessage);
	         %client.schedule(700, "delete");
         }
	   
	      BanList::add( 0, %client.getAddress(), $Host::KickBanTime );
      }
	}
}

function ban( %client, %admin )
{
   if ( %admin )
      messageAll('MsgAdminForce', '\c2%1 has banned %2.', %admin.name, %client.name); // z0dd - ZOD, 10/03/2. Tell who banned
   else
      messageAll( 'MsgVotePassed', '\c2%1 was banned by vote.', %client.name );
   
   messageClient(%client, 'onClientBanned', "");
   messageAllExcept( %client, -1, 'MsgClientDrop', "", %client.name, %client );
   
   addStatTrack(%client.guid, "bans", 1);
   
   // kill and delete this client
   if( isObject(%client.player) )
      %client.player.scriptKill(0);
   
   if ( isObject( %client ) )
   {
      %client.setDisconnectReason($Host::BanMessage);
      %client.schedule(700, "delete");
   }
   
   BanList::add(%client.guid, %client.getAddress(), $Host::BanTime);
}

//------------------------------------------------------------------------------
// This function returns the index of the next mission in the mission list.
// Ganked from Classic
//------------------------------------------------------------------------------
function getNextMission( %misName, %misType )
{
   // First find the index of the mission in the master list:
   for ( %mis = 0; %mis < $HostMissionCount; %mis++ )
      if( $HostMissionFile[%mis] $= %misName )
         break;

   if ( %mis == $HostMissionCount )
      return "";

   // Now find the index of the mission type:
   for ( %type = 0; %type < $HostTypeCount; %type++ )
      if ( $HostTypeName[%type] $= %misType )
         break;

   if ( %type == $hostTypeCount )
      return "";

   // Now find the mission's index in the mission-type specific sub-list:
   for ( %i = 0; %i < $HostMissionCount[%type]; %i++ )
      if ( $HostMission[%type, %i] == %mis )
         break;

   // --------------------------------------------------------------------
   // z0dd - ZOD: Enable random mission rotation for current mission type.
   if($Host::MD2RandomMissions)
   {
      %i = mFloor(getRandom(0, ($HostMissionCount[%type] - 1)));

      // If its same as last map, go back 1
      if($HostMissionFile[$HostMission[%type, %i]] $= %misName)
         %i--;

      // If its greater then or equal to count, set to zero
      %i = %i >= $HostMissionCount[%type] ? 0 : %i;
   }
   else
   {
      // Go BACKWARDS, because the missions are in reverse alphabetical order:
      if ( %i == 0 )   
         %i = $HostMissionCount[%type] - 1;
      else
         %i--;
   }
   // --------------------------------------------------------------------

   // If there are bots in the game, don't switch to any maps without
   // a NAV file:
   if ( $HostGameBotCount > 0 )
   {
      for ( %j = 0; %j < $HostMissionCount[%type] - 1; %j++ )
      {
         if ( $BotEnabled[$HostMission[%type, %i]] )
            break;

         // --------------------------------------------------------------------
         // z0dd - ZOD: Enable random mission rotation for current mission type.
         if($Host::MD2RandomMissions)
         {
            %i = mFloor(getRandom(0, ($HostMissionCount[%type] - 1)));

            // If its same as last map, go back 1
            if($HostMissionFile[$HostMission[%type, %i]] $= %misName)
               %i--;

            // If its greater then or equal to count, set to zero
            %i = %i >= $HostMissionCount[%type] ? 0 : %i;
         }
         else
         {
            if ( %i == 0 )
               %i = $HostMissionCount[%type] - 1;
            else
               %i--;
         }
         // --------------------------------------------------------------------
      }
   }
   
   return $HostMission[%type, %i];
}

//exec("scripts/MDModHud.cs");
//exec("scripts/MDSFClient.cs");
exec("scripts/modscripts/MDClient.cs");

// Update automatic ban list write
exec("autobanlist.cs");

// init loggers
EngineBase.logManager.initLoggers();
    
// Start Main Loop
startMainLoop();
    
// Schedule nightly logger restart for new date
onTime("0000", "EngineBase.logManager.restartLoggers();");

// MD Shutdown
function MDShutdown()
{
   centerprintall("The server is shutting down in 10 seconds for a restart.", 11, 1);
   schedule(10000, 0, quit);
}

echo("");
echo("Meltdown 2 Core Version: v"@EngineBase.ModVersion);
echo("Meltdown 2 Boot Time: "@formatTimeString("H:nn:ss"));
echo("");
echo("=====================================================================");
echo("=                     Meltdown 2 BASE CODE INIT                     =");
echo("=====================================================================");
echo("");
