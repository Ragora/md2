if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock LinearFlareProjectileData(WarpBall)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0;
   damageRadius        = 1.0;
   kickBackStrength    = 7500;
   radiusDamageType    = $DamageType::Default;

   explosion           = "SatchelMainExplosion";
   underwaterExplosion = "UnderwaterSatchelMainExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 1000.0;
   wetVelocity       = 1000.0;       // faster uw
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 21000;
   lifetimeMS        = 21000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 21000;

   activateDelayMS = -1;
   numFlares         = 0;
   flareColor        = "1 0.6 0.7";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0 0 0";
};

datablock StaticShapeData(DeployableWarpBeacon) : StaticShapeDamageProfile
{
   shapeFile = "deploy_sensor_motion.dts";   // teamlogo_projector
   maxDamage = 5;	// = 6; -soph
   destroyedLevel = 5;	// = 6; -soph
   disabledLevel = 5;	// = 6; -soph
   explosion = ConcMortarExplosion;
   underwaterExplosion = UnderwaterSatchelMainExplosion;

   targetNameTag = 'Deployable';
   targetTypeTag = 'Warp Beacon';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

function DeployableWarpBeacon::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);

   if(%obj.cancelled)
   {
      if(isObject(%obj.sourceClient) && !%obj.success)
         commandToClient(%obj.sourceClient, 'CenterPrint', "Teleport portal for "@%obj.deplname@" has been destroyed.", 3, 1);
   }

   if(isObject(%obj.projectile))
      %obj.projectile.delete();
      
   %obj.setPosition("0 0 10000");
   %obj.schedule(1250, "delete");
}

function setDamagePercentOfObject(%obj, %src)
{
   %dmg = getDamagePercent(%src.getDatablock().maxDamage, %src.getDamageLevel());
   %obj.setDamageLevel(%dmg * %obj.getDatablock().maxDamage);
}

function initSpecialDeploySequenceItemT(%name, %time, %which, %item, %plyr, %slot, %rot, %genPower)
{
   %owner = %plyr.client;
   
   %deplbecn = addObjectO(%owner, "DeployableWarpBeacon", %item.surfacePt);
   %deplbecn.setDamageLevel(%deplbecn.getDatablock().maxDamage - 0.01);
   %deplbecn.setScale("10 10 10");
   %deplbecn.playThread(0, "ambient");
   %deplbecn.surfacept = %item.surfacept;	// +soph	

   commandToClient(%owner, 'CenterPrint', "["@%name@"]\nScanning for Defense+ construction signal...", 3, 2);
   schedule(1000, %deplbecn, checkHealthDeployLoop, %name, %time, %which, %item, %owner, %deplbecn, %rot, %genPower);
}

function checkHealthDeployLoop(%name, %time, %which, %item, %owner, %slot, %rot, %genPower)
{
   if(%slot.getDamageLevel() > 0 && !%slot.inDEDField)
   {
      if(!%slot.messageSent)
      {
          %slot.messageSent = true;
          commandToClient(%owner, 'CenterPrint', "["@%name@"]\n Defense+ construction signal not detected, require manual building.", 3, 2);
      }
      
      schedule(250, %slot, checkHealthDeployLoop, %name, %time, %which, %item, %owner, %slot, %rot, %genPower);
   }
   else
   {
      if(%slot.inDEDField)
      {
         %slot.setDamageLevel(0);
         commandToClient(%owner, 'CenterPrint', "["@%name@"]\nDefense+ construction field active, building...", 3, 2);
      }
      else
         commandToClient(%owner, 'CenterPrint', "["@%name@"]\nBuilding complete, unpackaging...", 3, 2);
         

      initSpecialDeploySequenceItemO(%name, 1000, %which, %item, %owner, %slot, %rot, %genPower);
   }
}

function initSpecialDeploySequenceItem(%name, %time, %which, %item, %plyr, %slot, %rot, %genPower)
{
   if(%time < 1 || %time > 20000)
      return;

   commandToClient(%plyr.client, 'CenterPrint', %name@" is being teleported in... wait "@mFloor(%time/1000)@" seconds.", 3, 1);

   %deplbecn = addObject(%plyr, "DeployableWarpBeacon", %slot.surfacePt);	// %item.surfacePt); -soph
   %deplbecn.setScale("10 10 10");
   %deplbecn.playThread(0, "ambient");
   %deplbecn.schedule(%time+100, setDamageState, Destroyed);
   %deplbecn.schedule(%time+150, setPosition, "0 0 10000");
   %deplbecn.schedule(%time+1350, delete);


   %pos = vectorAdd(%slot.surfacePt, "0 0 "@%time);	// (%item.surfacePt, "0 0 "@%time); deploy bug -soph
   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%pos);
   %FFRObject.schedule(%time+1000, delete);

   %p = new LinearFlareProjectile()
   {
      dataBlock        = WarpBall;
      initialDirection = "0 0 -1"; //"0 0 -1";
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %plyr;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %deplbecn.projectile = %p;
   %deplbecn.sourceClient = %plyr.client;
   %deplbecn.deplname = %name;
//   %deplbecn.surfacept = %item.surfacept;		// this might be a problem -soph
   %deplbecn.item = %item;
   %deplbecn.deploythread = schedule(%time, 0, "specialDeploySequenceItem", %which, %item, %plyr, %slot, %rot, %deplbecn, %genPower);
   specialDeploySequenceValidCheck(%deplbecn, %time, %plyr);
}

function initSpecialDeploySequenceItemO(%name, %time, %which, %item, %owner, %slot, %rot, %genPower)
{
//   %slot.schedule(%time+100, setDamageState, Destroyed);
   %slot.schedule(%time+150, setPosition, "0 0 10000");
   %slot.schedule(%time+1350, delete);

   %pos = vectorAdd(%slot.surfacePt, "0 0 "@%time);	// vectorAdd(%item.surfacePt, "0 0 "@%time); deploy bug -soph
   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%pos);
   %FFRObject.schedule(%time+1000, delete);

   %p = new LinearFlareProjectile()
   {
      dataBlock        = WarpBall;
      initialDirection = "0 0 -1";
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %slot.projectile = %p;
   %slot.sourceClient = %owner;
   %slot.deplname = %name;
//   %slot.surfacept = %item.surfacept;			// this might be a problem -soph
   %slot.item = %item;
   %slot.deploythread = schedule(%time, 0, "specialDeploySequenceItemO", %which, %item, %owner, %slot, %rot, %slot, %genPower);
   specialDeploySequenceValidCheck(%slot, %time, %owner);
}

function specialDeploySequenceValidCheck(%obj, %time, %owner)	// use %owner instead of %plyr  -soph
{											// replace inst of %plyr with %owner.player 
   if(isObject(%obj))
   {
      if(%obj.item.testObjectTooClose() || %obj.item.extendedDeployChecks( %owner.player ))
      {
        // commandToClient(%owner, 'CenterPrint', "["@%obj.deplname@"]\nERROR: Unexpected obstruction, deploy sequence aborted.", 3, 2);
         cancelSpecialDeploySequence(%obj, %owner.player);
      }

      if(!%obj.cancelled)
         schedule(mFloor(%time / 500), %obj, "specialDeploySequenceValidCheck", %obj, %time, %owner);	// must be %owner
   }
}

function cancelSpecialDeploySequence(%obj, %plyr)
{
echo("cancelSpecialDeploySequence("@%obj@", "@%plyr@")" SPC %obj.getDataBlock().getName());
   if(isObject(%obj) && !%obj.cancelled)
   {
//      commandToClient(%plyr.client, 'CenterPrint', %obj.deplname@" deploy sequence has been cancelled due to obstruction.", 3, 1);

      %obj.startFade(0, 100, true);
//      %obj.schedule(100, setDamageState, Destroyed);
      %obj.schedule(350, delete);
      %obj.cancelled = true;
      cancel(%obj.deploythread);

      if(isObject(%obj.projectile))
         %obj.projectile.delete();
   }
}

function updateMultiObjectHealth(%obj, %target , %team )			// (%obj, %target) -soph
{
     if(isObject(%obj) && isObject(%target))
     {
          if( %team )								// +[soph]
          {									// + multiobject teamswitch bugfix
             %obj.team = %team ;						// +
             %target.team = %team ;						// +
          }									// +
          else									// +
             %team = %obj.team ; 						// +[/soph]
          %obj1d = %obj.getDamageLevel();
          %obj2d = %target.getDamageLevel();
          if( %obj.damageTimeMS == %target.damageTimeMS )			// +[soph]
               if( %obj1d > %obj2d )						// +
                    %obj.setDamageLevel( %obj2d ) ;				// +
               else								// +
                    %target.setDamageLevel( %obj1d ) ;				// +
          else if( %obj.damageTimeMS > %target.damageTimeMS )			// +
          {									// +
               %target.damageTimeMS = %obj.damageTimeMS ;			// +
               if( %obj.lastDamagedBy !$= "" )					// +
                    %targetObject.lastDamagedBy = %obj.lastDamagedBy ;		// +
               %target.setDamageLevel( %obj1d ) ;				// +
          }									// +
          else									// +
          {									// +
               %obj.damageTimeMS = %target.damageTimeMS ;			// +
               if( %targetObject.lastDamagedBy !$= "" )				// +
                    %obj.lastDamagedBy = %targetObject.lastDamagedBy ;		// +
               %obj.setDamageLevel( %obj2d ) ;					// +
          }									// +
										// +[/soph]
//          %avg = 0;								// -[soph]
          									// -
//          if(%obj1d > 0 && %obj2d > 0)          				// -
//               %avg = (%obj1d+%obj2d) / 2;          				// -
//          else if(%obj1d < %obj2d)          					// -
//               %avg = %obj2d;          					// -
//          else if(%obj2d < %obj1d)          					// -
//               %avg = %obj1d;          					// -
//          else          							// -
//               %avg = %obj2d;          					// -
                    								// -
//          %obj.setDamageLevel(%avg);						// -[/soph]

          schedule(250, %obj, updateMultiObjectHealth, %obj, %target, %team ) ;	// schedule(250, %obj, updateMultiObjectHealth, %obj, %target); -soph
     }
     else if(isObject(%target))	
          %obj.setDamageState(Destroyed);
     else
          %target.setDamageState(Destroyed);
}

// Previous code used with the construction mod bases, not sure if it's working properly but worth keeping around
function baseIntegrityCheck(%parent, %set)
{
     if(isObject(%parent))
     {
          %count = %set.getCount();
          %total = 0;
          %totalDmg = 0;

          for(%i = 0; %i < %count; %i++)
          {
               %obj = %set.getObject(%i);

               // We lost an object, base is being destroyed
               if(!isObject(%obj))
               {
                    %parent.setDamageState("Destroyed");
                    %set.schedule(0, delete);
                    return;
               }

               %dmg = %obj.getDamageLevel();

//               if(!%dmg)
//                    continue;
//               else
//               {
//                    %total++;
                    %totalDmg += %dmg;
//               }
          }

          %avg = %totalDmg / %count;

          for(%i = 0; %i < %count; %i++)
               %set.getObject(%i).setDamageLevel(%avg);

          schedule(250, %parent, baseIntegrityCheck, %parent, %set);
     }
     else
          if(isObject(%set))
               %set.delete();
}

function specialDeploySequenceItem(%which, %item, %plyr, %slot, %rot, %this, %genPower)
{
   if(isObject(%this))
   {
      %this.success = true;

      if(%which == 0)
      {
         $TeamDeployedCount[%plyr.team, BorgCubePack]++;						// +[soph]
         if( $TeamDeployedCount[%plyr.team, "BorgCubePack"] > $TeamDeployableMax["BorgCubePack"] )	// fix for multi-beacon deploying
            return $TeamDeployedCount[%plyr.team, "BorgCubePack"]--;					// +[/soph]

         %trans[0] = vectorAdd(%this.surfacePt, "0 0 0.5") SPC "0 1 0 0";
         %deplObj = addMultiObjectManager(%plyr, %trans[0]);
         %borgpad = addObject(%plyr, "BorgCubePad", %trans[0], %deplObj, "12 12 0.5");
         newBaseItemAppear(%borgpad, %trans[0]);

         %trans[1] = vectorAdd(%this.surfacePt, "0 0 8.5") SPC "0 1 0 0";
         %deplObjTop = addObject(%plyr, "BorgCubeTop", %trans[1], %deplObj, "12 12 0.5");
         newBaseItemAppear(%deplObjTop, %trans[1]);

         %trans[2] = vectorAdd(%this.surfacePt, "-4 -4 1.5") SPC "0 1 0 0";
         %deplObjGen = addObject(%plyr, "BaseGenerator", %trans[2], %deplObj);
         %deplObjGen.blastechCanRecharge = true;
         %deplObjGen.baseGenerator = true;
         %deplObjGen.playThread(0, "power");
         %deplObjGen.protectedByFFBase = true;
         newBaseItemAppear(%deplObjGen, %trans[2]);

         schedule(500, %borgpad, updateMultiObjectHealth, %borgpad, %deplObjTop);
//         schedule(650, %deplObjTop, updateMultiObjectHealth, %deplObjTop, %borgpad);			// -[soph]

         %deploySet = createRandomSimSet();
         
         %deploySet.add(%deplObjTop);
         %deploySet.add(%borgpad);
         
//         baseIntegrityCheck(%deplObj, %deploySet);

         %deplObj.play3d(%item.deploySound);

      //   addObject(%plyr, "BombBaseMarker", %this.surfacePt SPC "0 1 0 0", %deplObj);

         %trans[3] = vectorAdd(%this.surfacePt, "4 4 1.5") SPC "0 1 0 0";
         %stationObj = addStation(%plyr, %trans[3], %deplObj);
         %stationObj.protectedByFFBase = true;
         %stationObj.setSelfPowered();
         newBaseItemAppear(%stationObj, %trans[3]);

         %deplObj.station = %stationObj;
         %deplObj.gen = %deplObjGen;
         %deplObj.top = %deplObjTop;

      //   addAttachment(%deplObj, %deplObjTop);
      //   addAttachment(%deplObj, %deplObjGen);
      //   addAttachment(%deplObj, %stationObj);

      //   addAttachment(%deplObjTop, %deplObjGen);
      //   addAttachment(%deplObjTop, %stationObj);

         %thickness = ".2"; // hehe

         %pta = vectorAdd(%this.surfacePt, "8 8 0.5");
         %ptb = vectorAdd(%this.surfacePt, "-8 8 0.5");
         %ptc = vectorAdd(%this.surfacePt, "8 -8 0.5");
         %ptd = vectorAdd(%this.surfacePt, "-8 -8 0.5");

         %vec1 = getVectorFromPoints(%pta, %ptb);
         %vec2 = getVectorFromPoints(%ptb, %ptd);
         %vec3 = getVectorFromPoints(%ptd, %ptc);
         %vec4 = getVectorFromPoints(%ptc, %pta);

         %rota = vectorToRotZ(%vec1);
         %rotb = vectorToRotZ(%vec2);
         %rotc = vectorToRotZ(%vec3);
         %rotd = vectorToRotZ(%vec4);

         %lena = getDistance3D(%pta, %ptb);
         %lenb = getDistance3D(%ptb, %ptd);
         %lenc = getDistance3D(%ptd, %ptc);
         %lend = getDistance3D(%ptc, %pta);

         %scalea = %lena@" "@%thickness@" 8.5";
         %scaleb = %lenb@" "@%thickness@" 8.5";
         %scalec = %lenc@" "@%thickness@" 8.5";
         %scaled = %lend@" "@%thickness@" 8.5";

         %deplObj.shield[0] = addForcefield(%plyr, "BorgBaseField", %pta, %rota, %scalea, %deplObj);
         %deplObj.shield[1] = addForcefield(%plyr, "BorgBaseField", %ptb, %rotb, %scaleb, %deplObj);
         %deplObj.shield[2] = addForcefield(%plyr, "BorgBaseField", %ptc, %rotc, %scalec, %deplObj);
         %deplObj.shield[3] = addForcefield(%plyr, "BorgBaseField", %ptc, %rotd, %scaled, %deplObj);

         newBaseItemAppear(%deplObj.shield[0], %pta);
         newBaseItemAppear(%deplObj.shield[1], %ptb);
         newBaseItemAppear(%deplObj.shield[2], %ptc);
         newBaseItemAppear(%deplObj.shield[3], %ptc);

      //   for(%d = 0; %d < 4; %d++)
      //      %deplObj.shield[%d].deployBase = %deplObjGen;

         %deplObj.unbeaconable = true;
         %deplObjTop.unbeaconable = true;
         %deplObjGen.unbeaconable = true;
         %stationObj.unbeaconable = true;

        // $TeamDeployedCount[%plyr.team, BorgCubePack]++;	// moved up -soph
      }
      else if(%which == 1)
      {
         $TeamDeployedCount[%plyr.team, "DefenseBasePack"]++;							// +[soph]
         if( $TeamDeployedCount[%plyr.team, "DefenseBasePack"] > $TeamDeployableMax[DefenseBasePack] )		// fix for multi-beacon deploying
            return $TeamDeployedCount[%plyr.team, "DefenseBasePack"]--;						// +[/soph]

         %this.surfacePtO = %this.surfacePt;
         %this.surfacePt = vectorAdd("0 0 5", %this.surfacePt);

         %trans[0] = vectorAdd(%this.surfacePt, "0 0 0.5") SPC "0 1 0 0";
         %deplObj = addMultiObjectManager(%plyr, %trans[0]);
         %defpad = addObject(%plyr, "DefensePad", %trans[0], %deplObj, "24 24 0.5");
         newBaseItemAppear(%defpad, %trans[0]);
      //   addObject(%plyr, "DefBaseMarker", %this.surfacePt SPC "0 1 0 0", %deplObj);

         %trans[1] = vectorAdd(%this.surfacePt, "0 0 17") SPC "0 1 0 0";
         %deplObjTop = addObject(%plyr, "DefenseTop", %trans[1], %deplObj, "24 24 0.5");
         newBaseItemAppear(%deplObjTop, %trans[1]);

         %trans[2] = vectorAdd(%this.surfacePt, "-8 -8 1.5") SPC "0 1 0 0";
         %deplObjGen = addObject(%plyr, "BaseGenerator", %trans[2], %deplObj, "2 2 2");
         %deplObjGen.blastechCanRecharge = true;
         %deplObjGen.playThread(0, "power");
         %deplObjGen.protectedByFFBase = true;
         newBaseItemAppear(%deplObjGen, %trans[2]);

         %deplObj.play3d(%item.deploySound);

         %trans[4] = vectorAdd(%this.surfacePt, "-12.5 -12.5 1.5") SPC "0 1 0 0";
         %telepadB = addObject(%plyr, "DefenseTelePad", %trans[4], %deplObj);
         %telepadB.playThread(0, "power");
         %telepadB.isMANTATele = true;
         newBaseItemAppear(%telepadB, %trans[4]);

         %trans[5] = vectorAdd(%this.surfacePt, "12 12 8.500011") SPC "0 1 0 0";
         %deplObjBlock1 = addObject(%plyr, "DefenseTop", %trans[5], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock1, %trans[5]);

         %trans[6] = vectorAdd(%this.surfacePt, "-12 -12 8.500022") SPC "0 1 0 0";
         %deplObjBlock2 = addObject(%plyr, "DefenseTop", %trans[6], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock2, %trans[6]);

         %trans[7] = vectorAdd(%this.surfacePt, "-12 12 8.500000") SPC "0 1 0 0";
         %deplObjBlock3 = addObject(%plyr, "DefenseTop", %trans[7], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock3, %trans[7]);

         %trans[8] = vectorAdd(%this.surfacePt, "12 -12 8.500033") SPC "0 1 0 0";
         %deplObjBlock4 = addObject(%plyr, "DefenseTop", %trans[8], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock4, %trans[8]);

         %trans[9] = vectorAdd(%this.surfacePt, "12 0 8.500044") SPC "0 1 0 0";
         %deplObjBlock5 = addObject(%plyr, "DefenseTop", %trans[9], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock5, %trans[9]);

         %trans[10] = vectorAdd(%this.surfacePt, "-12 0 8.500055") SPC "0 1 0 0";
         %deplObjBlock6 = addObject(%plyr, "DefenseTop", %trans[10], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock6, %trans[10]);

         %trans[11] = vectorAdd(%this.surfacePt, "0 12 8.500066") SPC "0 1 0 0";
         %deplObjBlock7 = addObject(%plyr, "DefenseTop", %trans[11], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock7, %trans[11]);

         %trans[12] = vectorAdd(%this.surfacePt, "0 -12 8.500077") SPC "0 1 0 0";
         %deplObjBlock8 = addObject(%plyr, "DefenseTop", %trans[12], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock8, %trans[12]);

         schedule(500, %deplObjTop, updateMultiObjectHealth, %deplObjTop, %defpad);
         schedule(800, %deplObjBlock1, updateMultiObjectHealth, %deplObjBlock1, %defpad);
         schedule(950, %deplObjBlock2, updateMultiObjectHealth, %deplObjBlock2, %defpad);
         schedule(1100, %deplObjBlock3, updateMultiObjectHealth, %deplObjBlock3, %defpad);
         schedule(1250, %deplObjBlock4, updateMultiObjectHealth, %deplObjBlock4, %defpad);
         schedule(1400, %deplObjBlock5, updateMultiObjectHealth, %deplObjBlock5, %defpad);
         schedule(1550, %deplObjBlock6, updateMultiObjectHealth, %deplObjBlock6, %defpad);
         schedule(1700, %deplObjBlock7, updateMultiObjectHealth, %deplObjBlock7, %defpad);
         schedule(1850, %deplObjBlock8, updateMultiObjectHealth, %deplObjBlock8, %defpad);

         %deploySet = createRandomSimSet();

         %deploySet.add(%deplObjTop);
         %deploySet.add(%defpad);
         %deploySet.add(%deplObjBlock1);
         %deploySet.add(%deplObjBlock2);
         %deploySet.add(%deplObjBlock3);
         %deploySet.add(%deplObjBlock4);
         %deploySet.add(%deplObjBlock5);
         %deploySet.add(%deplObjBlock6);
         %deploySet.add(%deplObjBlock7);
         %deploySet.add(%deplObjBlock8);

//         baseIntegrityCheck(%deplObj, %deploySet);

         %trans[13] = vectorAdd(%this.surfacePt, "12.5 12.5 1.5") SPC "0 1 0 0";
         %stationObj = addStation(%plyr, %trans[13], %deplObj);
         %stationObj.protectedByFFBase = true;
         %stationObj.setSelfPowered();
         newBaseItemAppear(%stationObj, %trans[13]);

         %trans[14] = vectorAdd(%this.surfacePt, "-12.5 12.5 9.5") SPC "0 1 0 0";
         %stationObj2 = addStation(%plyr, %trans[14], %deplObj);
         %stationObj2.protectedByFFBase = true;
         %stationObj2.setSelfPowered();
         newBaseItemAppear(%stationObj2, %trans[14]);

      //   %deplObj.attachment[12] = %telepadA;
      //   %deplObj.attachment[13] = %telepadB;

         %thickness = ".2"; // 0.5

         %pta = vectorAdd(%this.surfacePt, "16.4 16.4 0.75");	// vectorAdd(%this.surfacePt, "16.4 16.4 0.5"); -soph
         %ptb = vectorAdd(%this.surfacePt, "-16.4 16.4 0.75");	// vectorAdd(%this.surfacePt, "-16.4 16.4 0.5"); -soph
         %ptc = vectorAdd(%this.surfacePt, "16.4 -16.4 0.75");	// vectorAdd(%this.surfacePt, "16.4 -16.4 0.5"); -soph
         %ptd = vectorAdd(%this.surfacePt, "-16.4 -16.4 0.75");	// vectorAdd(%this.surfacePt, "-16.4 -16.4 0.5"); -soph

         %vec1 = getVectorFromPoints(%pta, %ptb);
         %vec2 = getVectorFromPoints(%ptb, %ptd);
         %vec3 = getVectorFromPoints(%ptd, %ptc);
         %vec4 = getVectorFromPoints(%ptc, %pta);

         %rota = vectorToRotZ(%vec1);
         %rotb = vectorToRotZ(%vec2);
         %rotc = vectorToRotZ(%vec3);
         %rotd = vectorToRotZ(%vec4);

         %lena = getDistance3D(%pta, %ptb);
         %lenb = getDistance3D(%ptb, %ptd);
         %lenc = getDistance3D(%ptd, %ptc);
         %lend = getDistance3D(%ptc, %pta);

         %scalea = %lena@" "@%thickness@" 17.25";
         %scaleb = %lenb@" "@%thickness@" 17.25";
         %scalec = %lenc@" "@%thickness@" 17.25";
         %scaled = %lend@" "@%thickness@" 17.25";

         %deplObj.shield[0] = addForcefield(%plyr, "DefenseBaseField", %pta, %rota, %scalea, %deplObj);
         %deplObj.shield[1] = addForcefield(%plyr, "DefenseBaseField", %ptb, %rotb, %scaleb, %deplObj);
         %deplObj.shield[2] = addForcefield(%plyr, "DefenseBaseField", %ptc, %rotc, %scalec, %deplObj);
         %deplObj.shield[3] = addForcefield(%plyr, "DefenseBaseField", %ptc, %rotd, %scaled, %deplObj);
         newBaseItemAppear(%deplObj.shield[0], %pta);
         newBaseItemAppear(%deplObj.shield[1], %ptb);
         newBaseItemAppear(%deplObj.shield[2], %ptc);
         newBaseItemAppear(%deplObj.shield[3], %ptc);

         %deplObj.unbeaconable = true;
         %deplObjTop.unbeaconable = true;
         %deplObjGen.unbeaconable = true;
         %stationObj.unbeaconable = true;
         %stationObj2.unbeaconable = true;
         %defpad.unbeaconable = true;
         %deplObjBlock1.unbeaconable = true;
         %deplObjBlock2.unbeaconable = true;
         %deplObjBlock3.unbeaconable = true;
         %deplObjBlock4.unbeaconable = true;
         %deplObjBlock5.unbeaconable = true;
         %deplObjBlock6.unbeaconable = true;
         %deplObjBlock7.unbeaconable = true;
         %deplObjBlock8.unbeaconable = true;

        // $TeamDeployedCount[%plyr.team, "DefenseBasePack"]++;	// moved up -soph     // just fine
      }
      else if(%which == 2)
      {
         $TeamDeployedCount[%plyr.team, "HoverBasePack"]++;						// +[soph]
         if( $TeamDeployedCount[%plyr.team, "HoverBasePack"] > $TeamDeployableMax["HoverBasePack"] )	// fix for multi-beacon deploying
            return $TeamDeployedCount[%plyr.team, "HoverBasePack"]--;					// +[/soph]

         %trans[0] = vectorAdd(%this.surfacePt, "0 0 23.5") SPC "0 1 0 0";
         %deplObj = addMultiObjectManager(%plyr, %trans[0]);
      //   addObject(%plyr, "HoverBaseMarker", %this.surfacePt SPC "0 1 0 0", %deplObj);

         %hoverpad = addObject(%plyr, "HoverPad", %trans[0], %deplObj, "7.5 7.5 5.5");
         %hoverpad.playThread(0, "ambient");
         newBaseItemAppear(%hoverpad, %trans[0]);

         %trans[1] = vectorAdd(%this.surfacePt, "0 0 24.75") SPC "0 1 0 0";
         %deplObjPad = addObject(%plyr, "HoverSupport", %trans[1], %deplObj, "10 10 0.5");
         newBaseItemAppear(%deplObjPad, %trans[1]);

         %trans[2] = vectorAdd(%this.surfacePt, "6.5 6.5 25.5") SPC "0 0 1 0.785";
         %turret = addTurret(%plyr, %trans[2], %deplObj, "AABarrelLarge");
         newBaseItemAppear(%turret, %trans[2]);

         %trans[3] = vectorAdd(%this.surfacePt, "-6.5 6.5 25.5") SPC "0 0 1 -0.785";
         %turret2 = addTurret(%plyr, %trans[3], %deplObj, "MissileBarrelLarge");
         newBaseItemAppear(%turret2, %trans[3]);

         %trans[4] = vectorAdd(%this.surfacePt, "0 0 26.175") SPC "0 1 0 0"; // 1 rot = 2PI; halfRot = 3.14

         %trans[5] = %this.surfacePt SPC "0 1 0 0";
         %telepadA = addObject(%plyr, "HoverTelePad", %trans[5], %deplObj);
         %telepadA.playThread(0, "power");
         newBaseItemAppear(%telepadA, %trans[5]);

         %trans[6] = vectorAdd(%this.surfacePt, "-6.5 -6.5 25.75") SPC "0 1 0 0";
         %telepadB = addObject(%plyr, "HoverTelePad", %trans[6], %deplObj);
         %telepadB.playThread(0, "power");
         newBaseItemAppear(%telepadB, %trans[6]);

         %telepadA.teletgt = %telepadB;
         %telepadB.teletgt = %telepadA;

         %deplObj.play3d(%item.deploySound);
        // $TeamDeployedCount[%plyr.team, "HoverBasePack"]++;	// moved up -soph     // just fine
      }
      else if(%which == 3)
      {
         $TeamDeployedCount[%plyr.team, "TurretBasePack"]++;						// +[soph]
         if( $TeamDeployedCount[%plyr.team, "TurretBasePack"] > $TeamDeployableMax["TurretBasePack"] )	// fix for multi-beacon deploying
            return $TeamDeployedCount[%plyr.team, "TurretBasePack"]--;					// +[/soph]

         %trans = %this.surfacePt SPC %rot;
         %turret = addTurret(%plyr, %trans); //, 0, "", %genPower);
//         newBaseItemAppear(%turret, %trans);
          findFriendlyGenInArea(%turret, 250);
         %turret.play3D(%item.deploySound);
        // $TeamDeployedCount[%plyr.team, "TurretBasePack"]++;	// moved up -soph     // just fine
         $TurretDeployedTime[%plyr.client.team] = getSimTime();
         detectDeployingOn(%turret, %this.surfacePt);
      }
   }
}

function specialDeploySequenceItemO(%which, %item, %owner, %slot, %rot, %this, %genPower)
{
   if(isObject(%this))
   {
      %this.success = true;

      if(%which == 0)
      {
         if( $TeamDeployedCount[%owner.team, "BorgCubePack"] < $TeamDeployableMax[BorgCubePack] )	// +[soph]
            $TeamDeployedCount[%owner.team, "BorgCubePack"]++;
         else         											// fix for multi-beacon deploying
            return;											// +[/soph]

         %trans[0] = vectorAdd(%this.surfacePt, "0 0 0.5") SPC "0 1 0 0";
         %deplObj = addMultiObjectManagerO(%owner, %trans[0]);
         %borgpad = addObjectO(%owner, "BorgCubePad", %trans[0], %deplObj, "12 12 0.5");
         newBaseItemAppear(%borgpad, %trans[0]);

         %trans[1] = vectorAdd(%this.surfacePt, "0 0 8.5") SPC "0 1 0 0";
         %deplObjTop = addObjectO(%owner, "BorgCubeTop", %trans[1], %deplObj, "12 12 0.5");
         newBaseItemAppear(%deplObjTop, %trans[1]);

         %trans[2] = vectorAdd(%this.surfacePt, "-4 -4 1.5") SPC "0 1 0 0";
         %deplObjGen = addObjectO(%owner, "BaseGenerator", %trans[2], %deplObj);
         %deplObjGen.blastechCanRecharge = true;
         %deplObjGen.baseGenerator = true;
         %deplObjGen.playThread(0, "power");
         %deplObjGen.protectedByFFBase = true;
         newBaseItemAppear(%deplObjGen, %trans[2]);

         schedule(500, %borgpad, updateMultiObjectHealth, %borgpad, %deplObjTop);
//         schedule(650, %deplObjTop, updateMultiObjectHealth, %deplObjTop, %borgpad);			// -soph

         %deploySet = createRandomSimSet();

         %deploySet.add(%deplObjTop);
         %deploySet.add(%borgpad);

//         baseIntegrityCheck(%deplObj, %deploySet);

         %deplObj.play3d(%item.deploySound);

      //   addObjectO(%owner, "BombBaseMarker", %this.surfacePt SPC "0 1 0 0", %deplObj);

         %trans[3] = vectorAdd(%this.surfacePt, "4 4 1.5") SPC "0 1 0 0";
         %stationObj = addStationO(%owner, %trans[3], %deplObj);
         %stationObj.protectedByFFBase = true;
         %stationObj.setSelfPowered();
         newBaseItemAppear(%stationObj, %trans[3]);

         %deplObj.station = %stationObj;
         %deplObj.gen = %deplObjGen;
         %deplObj.top = %deplObjTop;

      //   addAttachment(%deplObj, %deplObjTop);
      //   addAttachment(%deplObj, %deplObjGen);
      //   addAttachment(%deplObj, %stationObj);

      //   addAttachment(%deplObjTop, %deplObjGen);
      //   addAttachment(%deplObjTop, %stationObj);

         %thickness = ".2"; // hehe

         %pta = vectorAdd(%this.surfacePt, "8 8 0.5");
         %ptb = vectorAdd(%this.surfacePt, "-8 8 0.5");
         %ptc = vectorAdd(%this.surfacePt, "8 -8 0.5");
         %ptd = vectorAdd(%this.surfacePt, "-8 -8 0.5");

         %vec1 = getVectorFromPoints(%pta, %ptb);
         %vec2 = getVectorFromPoints(%ptb, %ptd);
         %vec3 = getVectorFromPoints(%ptd, %ptc);
         %vec4 = getVectorFromPoints(%ptc, %pta);

         %rota = vectorToRotZ(%vec1);
         %rotb = vectorToRotZ(%vec2);
         %rotc = vectorToRotZ(%vec3);
         %rotd = vectorToRotZ(%vec4);

         %lena = getDistance3D(%pta, %ptb);
         %lenb = getDistance3D(%ptb, %ptd);
         %lenc = getDistance3D(%ptd, %ptc);
         %lend = getDistance3D(%ptc, %pta);

         %scalea = %lena@" "@%thickness@" 8.5";
         %scaleb = %lenb@" "@%thickness@" 8.5";
         %scalec = %lenc@" "@%thickness@" 8.5";
         %scaled = %lend@" "@%thickness@" 8.5";

         %deplObj.shield[0] = addForcefieldO(%owner, "BorgBaseField", %pta, %rota, %scalea, %deplObj);
         %deplObj.shield[1] = addForcefieldO(%owner, "BorgBaseField", %ptb, %rotb, %scaleb, %deplObj);
         %deplObj.shield[2] = addForcefieldO(%owner, "BorgBaseField", %ptc, %rotc, %scalec, %deplObj);
         %deplObj.shield[3] = addForcefieldO(%owner, "BorgBaseField", %ptc, %rotd, %scaled, %deplObj);

         newBaseItemAppear(%deplObj.shield[0], %pta);
         newBaseItemAppear(%deplObj.shield[1], %ptb);
         newBaseItemAppear(%deplObj.shield[2], %ptc);
         newBaseItemAppear(%deplObj.shield[3], %ptc);

      //   for(%d = 0; %d < 4; %d++)
      //      %deplObj.shield[%d].deployBase = %deplObjGen;

         %deplObj.unbeaconable = true;
         %deplObjTop.unbeaconable = true;
         %deplObjGen.unbeaconable = true;
         %stationObj.unbeaconable = true;

        // $TeamDeployedCount[%owner.team, BorgCubePack]++;	// moved up -soph
      }
      else if(%which == 1)
      {

         if( $TeamDeployedCount[%owner.team, "DefenseBasePack"] < $TeamDeployableMax[DefenseBasePack] )	// +[soph]
            $TeamDeployedCount[%owner.team, "DefenseBasePack"]++;
         else													// fix for multi-beacon deploying
            return;												// +[/soph]

         %this.surfacePtO = %this.surfacePt;
         %this.surfacePt = vectorAdd("0 0 5", %this.surfacePt);

         %trans[0] = vectorAdd(%this.surfacePt, "0 0 0.5") SPC "0 1 0 0";
         %deplObj = addMultiObjectManagerO(%owner, %trans[0]);
         %defpad = addObjectO(%owner, "DefensePad", %trans[0], %deplObj, "24 24 0.5");
         newBaseItemAppear(%defpad, %trans[0]);
      //   addObjectO(%owner, "DefBaseMarker", %this.surfacePt SPC "0 1 0 0", %deplObj);

         %trans[1] = vectorAdd(%this.surfacePt, "0 0 17") SPC "0 1 0 0";
         %deplObjTop = addObjectO(%owner, "DefenseTop", %trans[1], %deplObj, "24 24 0.5");
         newBaseItemAppear(%deplObjTop, %trans[1]);

         %trans[2] = vectorAdd(%this.surfacePt, "-8 -8 1.5") SPC "0 1 0 0";
         %deplObjGen = addObjectO(%owner, "BaseGenerator", %trans[2], %deplObj, "2 2 2");
         %deplObjGen.blastechCanRecharge = true;
         %deplObjGen.playThread(0, "power");
         %deplObjGen.protectedByFFBase = true;
         newBaseItemAppear(%deplObjGen, %trans[2]);

         %deplObj.play3d(%item.deploySound);

         %trans[4] = vectorAdd(%this.surfacePt, "-12.5 -12.5 1.5") SPC "0 1 0 0";
         %telepadB = addObjectO(%owner, "DefenseTelePad", %trans[4], %deplObj);
         %telepadB.playThread(0, "power");
         %telepadB.isMANTATele = true;
         %telepadB.protectedByFFBase = true;									// +soph
         newBaseItemAppear(%telepadB, %trans[4]);

         %trans[5] = vectorAdd(%this.surfacePt, "12 12 8.500011") SPC "0 1 0 0";
         %deplObjBlock1 = addObjectO(%owner, "DefenseTop", %trans[5], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock1, %trans[5]);

         %trans[6] = vectorAdd(%this.surfacePt, "-12 -12 8.500022") SPC "0 1 0 0";
         %deplObjBlock2 = addObjectO(%owner, "DefenseTop", %trans[6], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock2, %trans[6]);

         %trans[7] = vectorAdd(%this.surfacePt, "-12 12 8.500000") SPC "0 1 0 0";
         %deplObjBlock3 = addObjectO(%owner, "DefenseTop", %trans[7], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock3, %trans[7]);

         %trans[8] = vectorAdd(%this.surfacePt, "12 -12 8.500033") SPC "0 1 0 0";
         %deplObjBlock4 = addObjectO(%owner, "DefenseTop", %trans[8], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock4, %trans[8]);

         %trans[9] = vectorAdd(%this.surfacePt, "12 0 8.500044") SPC "0 1 0 0";
         %deplObjBlock5 = addObjectO(%owner, "DefenseTop", %trans[9], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock5, %trans[9]);

         %trans[10] = vectorAdd(%this.surfacePt, "-12 0 8.500055") SPC "0 1 0 0";
         %deplObjBlock6 = addObjectO(%owner, "DefenseTop", %trans[10], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock6, %trans[10]);

         %trans[11] = vectorAdd(%this.surfacePt, "0 12 8.500066") SPC "0 1 0 0";
         %deplObjBlock7 = addObjectO(%owner, "DefenseTop", %trans[11], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock7, %trans[11]);

         %trans[12] = vectorAdd(%this.surfacePt, "0 -12 8.500077") SPC "0 1 0 0";
         %deplObjBlock8 = addObjectO(%owner, "DefenseTop", %trans[12], %deplObj, "8 8 0.5");
         newBaseItemAppear(%deplObjBlock8, %trans[12]);

         schedule(500, %deplObjTop, updateMultiObjectHealth, %deplObjTop, %defpad);
//         schedule(650, %defpad, updateMultiObjectHealth, %defpad, %deplObjTop);				// -soph
         schedule(800, %deplObjBlock1, updateMultiObjectHealth, %deplObjBlock1, %defpad);
         schedule(950, %deplObjBlock2, updateMultiObjectHealth, %deplObjBlock2, %defpad);
         schedule(1100, %deplObjBlock3, updateMultiObjectHealth, %deplObjBlock3, %defpad);
         schedule(1250, %deplObjBlock4, updateMultiObjectHealth, %deplObjBlock4, %defpad);
         schedule(1400, %deplObjBlock5, updateMultiObjectHealth, %deplObjBlock5, %defpad);
         schedule(1550, %deplObjBlock6, updateMultiObjectHealth, %deplObjBlock6, %defpad);
         schedule(1700, %deplObjBlock7, updateMultiObjectHealth, %deplObjBlock7, %defpad);
         schedule(1850, %deplObjBlock8, updateMultiObjectHealth, %deplObjBlock8, %defpad);

         %deploySet = createRandomSimSet();

         %deploySet.add(%deplObjTop);
         %deploySet.add(%defpad);
         %deploySet.add(%deplObjBlock1);
         %deploySet.add(%deplObjBlock2);
         %deploySet.add(%deplObjBlock3);
         %deploySet.add(%deplObjBlock4);
         %deploySet.add(%deplObjBlock5);
         %deploySet.add(%deplObjBlock6);
         %deploySet.add(%deplObjBlock7);
         %deploySet.add(%deplObjBlock8);

//         baseIntegrityCheck(%deplObj, %deploySet);

         %trans[13] = vectorAdd(%this.surfacePt, "12.5 12.5 1.5") SPC "0 1 0 0";
         %stationObj = addStationO(%owner, %trans[13], %deplObj);
         %stationObj.protectedByFFBase = true;
         %stationObj.setSelfPowered();
         newBaseItemAppear(%stationObj, %trans[13]);

         %trans[14] = vectorAdd(%this.surfacePt, "-12.5 12.5 9.5") SPC "0 1 0 0";
         %stationObj2 = addStationO(%owner, %trans[14], %deplObj);
         %stationObj2.protectedByFFBase = true;
         %stationObj2.setSelfPowered();
         newBaseItemAppear(%stationObj2, %trans[14]);

      //   %deplObj.attachment[12] = %telepadA;
      //   %deplObj.attachment[13] = %telepadB;

         %thickness = ".2"; // 0.5

         %pta = vectorAdd(%this.surfacePt, "16.4 16.4 0.5");
         %ptb = vectorAdd(%this.surfacePt, "-16.4 16.4 0.5");
         %ptc = vectorAdd(%this.surfacePt, "16.4 -16.4 0.5");
         %ptd = vectorAdd(%this.surfacePt, "-16.4 -16.4 0.5");

         %vec1 = getVectorFromPoints(%pta, %ptb);
         %vec2 = getVectorFromPoints(%ptb, %ptd);
         %vec3 = getVectorFromPoints(%ptd, %ptc);
         %vec4 = getVectorFromPoints(%ptc, %pta);

         %rota = vectorToRotZ(%vec1);
         %rotb = vectorToRotZ(%vec2);
         %rotc = vectorToRotZ(%vec3);
         %rotd = vectorToRotZ(%vec4);

         %lena = getDistance3D(%pta, %ptb);
         %lenb = getDistance3D(%ptb, %ptd);
         %lenc = getDistance3D(%ptd, %ptc);
         %lend = getDistance3D(%ptc, %pta);

         %scalea = %lena@" "@%thickness@" 17.25";
         %scaleb = %lenb@" "@%thickness@" 17.25";
         %scalec = %lenc@" "@%thickness@" 17.25";
         %scaled = %lend@" "@%thickness@" 17.25";

         %deplObj.shield[0] = addForcefieldO(%owner, "DefenseBaseField", %pta, %rota, %scalea, %deplObj);
         %deplObj.shield[1] = addForcefieldO(%owner, "DefenseBaseField", %ptb, %rotb, %scaleb, %deplObj);
         %deplObj.shield[2] = addForcefieldO(%owner, "DefenseBaseField", %ptc, %rotc, %scalec, %deplObj);
         %deplObj.shield[3] = addForcefieldO(%owner, "DefenseBaseField", %ptc, %rotd, %scaled, %deplObj);
         newBaseItemAppear(%deplObj.shield[0], %pta);
         newBaseItemAppear(%deplObj.shield[1], %ptb);
         newBaseItemAppear(%deplObj.shield[2], %ptc);
         newBaseItemAppear(%deplObj.shield[3], %ptc);

         %deplObj.unbeaconable = true;
         %deplObjTop.unbeaconable = true;
         %deplObjGen.unbeaconable = true;
         %stationObj.unbeaconable = true;
         %stationObj2.unbeaconable = true;
         %defpad.unbeaconable = true;
         %deplObjBlock1.unbeaconable = true;
         %deplObjBlock2.unbeaconable = true;
         %deplObjBlock3.unbeaconable = true;
         %deplObjBlock4.unbeaconable = true;
         %deplObjBlock5.unbeaconable = true;
         %deplObjBlock6.unbeaconable = true;
         %deplObjBlock7.unbeaconable = true;
         %deplObjBlock8.unbeaconable = true;
echo("End dbase construction");
        // $TeamDeployedCount[%owner.team, "DefenseBasePack"]++;	// moved up -soph     // just fine
      }
      else if(%which == 2)
      {
         if( $TeamDeployedCount[%owner.team, "HoverBasePack"] < $TeamDeployableMax[HoverBasePack] )	// +[soph]
            $TeamDeployedCount[%owner.team, "HoverBasePack"]++;
         else												// fix for multi-beacon deploying
            return;											// +[/soph]

         %trans[0] = vectorAdd(%this.surfacePt, "0 0 23.5") SPC "0 1 0 0";
         %deplObj = addMultiObjectManagerO(%owner, %trans[0]);
      //   addObjectO(%owner, "HoverBaseMarker", %this.surfacePt SPC "0 1 0 0", %deplObj);

         %hoverpad = addObjectO(%owner, "HoverPad", %trans[0], %deplObj, "7.5 7.5 5.5");
         %hoverpad.playThread(0, "ambient");
         newBaseItemAppear(%hoverpad, %trans[0]);

         %trans[1] = vectorAdd(%this.surfacePt, "0 0 24.75") SPC "0 1 0 0";
         %deplObjPad = addObjectO(%owner, "HoverSupport", %trans[1], %deplObj, "10 10 0.5");
         newBaseItemAppear(%deplObjPad, %trans[1]);

         %trans[2] = vectorAdd(%this.surfacePt, "6.5 6.5 25.5") SPC "0 0 1 0.785";
         %turret = addTurretO(%owner, %trans[2], %deplObj, "AABarrelLarge");
         newBaseItemAppear(%turret, %trans[2]);

         %trans[3] = vectorAdd(%this.surfacePt, "-6.5 6.5 25.5") SPC "0 0 1 -0.785";
         %turret2 = addTurretO(%owner, %trans[3], %deplObj, "MissileBarrelLarge");
         newBaseItemAppear(%turret2, %trans[3]);

         %trans[4] = vectorAdd(%this.surfacePt, "0 0 26.175") SPC "0 1 0 0"; // 1 rot = 2PI; halfRot = 3.14

         %trans[5] = %this.surfacePt SPC "0 1 0 0";
         %telepadA = addObjectO(%owner, "HoverTelePad", %trans[5], %deplObj);
         %telepadA.playThread(0, "power");
         newBaseItemAppear(%telepadA, %trans[5]);

         %trans[6] = vectorAdd(%this.surfacePt, "-6.5 -6.5 25.75") SPC "0 1 0 0";
         %telepadB = addObjectO(%owner, "HoverTelePad", %trans[6], %deplObj);
         %telepadB.playThread(0, "power");
         newBaseItemAppear(%telepadB, %trans[6]);

         %telepadA.teletgt = %telepadB;
         %telepadB.teletgt = %telepadA;

         %deplObj.play3d(%item.deploySound);
        // $TeamDeployedCount[%owner.team, "HoverBasePack"]++;	// moved up -soph     // just fine
      }
      else if(%which == 3)
      {
         if( $TeamDeployedCount[%owner.team, "TurretBasePack"] < $TeamDeployableMax[TurretBasePack] )	// +[soph]
            $TeamDeployedCount[%owner.team, "TurretBasePack"]++;
         else         											// fix for multi-beacon deploying
            return;											// +[/soph]

         %trans = %this.surfacePt SPC %rot;
         %turret = addTurretO(%owner, %trans); //, 0, "", %genPower);
//         newBaseItemAppear(%turret, %trans);
          findFriendlyGenInArea(%turret, 250);
         %turret.play3D(%item.deploySound);
        // $TeamDeployedCount[%owner.team, "TurretBasePack"]++;	// moved up -soph     // just fine
         $TurretDeployedTime[%owner.team] = getSimTime();
         detectDeployingOn(%turret, %this.surfacePt);
      }
   }
}
