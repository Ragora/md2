if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ET Explosion efects.. 101!!
// nuke done in 2 hours.

datablock ShockwaveData(ETRingShockwave)
{
   width = 36.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 90;
   acceleration = -30;
   lifetimeMS = 3000;
   height = 4.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.9 0.8 0.1 0.5";
   colors[1] = "0.9 0.8 0.1 0.8";
   colors[2] = "1.0 1.0 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ShockwaveData(ETMainRingShockwave)
{
   width = 64.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 180;
   acceleration = -20;
   lifetimeMS = 5000;
   height = 3.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1 1 1 0.5";
   colors[1] = "1 1 1 0.8";
   colors[2] = "1 1 1 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(ETMortarExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.1;
   faceViewer = true;

   sizes[0] = "7.5 7.5 7.5";
   sizes[1] = "7.5 7.5 7.5";
   times[0] = 0.0;
   times[1] = 1.0;

   soundProfile   = HugeExplosionSound;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = ImpactCrescentEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(ETMainRingExplosion)
{

   soundProfile   = HugeExplosionSound;

   shockwave = ETMainRingShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   shakeCamera = true;
   camShakeFreq = "15.0 15.0 15.0";
   camShakeAmp = "50.0 50.0 50.0";
   camShakeDuration = 3.5;
   camShakeRadius = 375.0;
};

datablock ExplosionData(ETRingExplosion)
{
   soundProfile   = MortarExplosionSound;

   shockwave = ETRingShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = ImpactCrescentEmitter;
   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 12.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.0;
   camShakeRadius = 35.0;
};

datablock ExplosionData(TacMechRingExplosion)
{
   soundProfile   = MortarExplosionSound;

   shockwave = TacMechShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = ImpactCrescentEmitter;
   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 12.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.0;
   camShakeRadius = 35.0;
};

datablock ExplosionData(ETMissileExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.1;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

   sizes[0] = "7 7 7";
   sizes[1] = "7 7 7";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = LargeMissileSubExplosion;
   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MissileCrescentEmitter;
   emitter[2] = MissileSparkEmitter;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

//datablock LinearFlareProjectileData(TacMechArmorBlowout)
//{
//   projectileShapeName = "turret_muzzlePoint.dts";
//   faceViewer          = false;
//   directDamage        = 0.0;
//   hasDamageRadius     = true;
//   indirectDamage      = 50.0;
//   damageRadius        = 30.0;
//   radiusDamageType    = $DamageType::Mortar;
//   kickBackStrength    = 10000;
//
//   explosion           = "TacMechRingExplosion";
//   underwaterExplosion = "TacMechRingExplosion";
//
//   splash              = MissileSplash;
//   velInheritFactor    = 0;
//
//   dryVelocity       = 0.1;
//   wetVelocity       = 0.1;
//   fizzleTimeMS      = 35;
//   lifetimeMS        = 35;
//   explodeOnDeath    = true;
//   reflectOnWaterImpactAngle = 90;
//   explodeOnWaterImpact      = false;
//   deflectionOnWaterImpact   = 0.0;
//   fizzleUnderwaterMS        = 35;
//
//   activateDelayMS = -1;
//
//   numFlares         = 0;
//   flareColor        = "0 0 0";
//   flareModTexture   = "flaremod";
//   flareBaseTexture  = "flarebase";
//};

datablock LinearFlareProjectileData(ETD)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.5 ;				// 500.0; -soph
   damageRadius        = 225.0 ;			// 500.0; -soph
   radiusDamageType    = $DamageType::Nuke;
   kickBackStrength    = 15000 ;			// 50000; -soph

   explosion           = "ETMainRingExplosion";
   underwaterExplosion = "ETMainRingExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(ETMortar) : ETD
{
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   radiusDamageType    = $DamageType::Nuke;
   kickBackStrength    = 0;

   explosion           = "ETMortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
};

datablock LinearFlareProjectileData(ETMissile) : ETD
{
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   radiusDamageType    = $DamageType::Nuke;
   kickBackStrength    = 0;

   explosion           = "ETMissileExplosion";
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
};

datablock LinearFlareProjectileData(ETSmallWave) : ETD
{
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   radiusDamageType    = $DamageType::Nuke;
   kickBackStrength    = 0;

   explosion           = "ETRingExplosion";
   underwaterExplosion = "ETRingExplosion";
};

datablock LinearFlareProjectileData(ETMainWave) : ETD
{
   indirectDamage      = 0;
   damageRadius        = 0.0;
   radiusDamageType    = $DamageType::Nuke;
   kickBackStrength    = 0;

   explosion           = "ETMainRingExplosion";
   underwaterExplosion = "ETMainRingExplosion";
};


datablock ShockwaveData(IonCannonBlastShockwave)
{
   width = 50.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 500;
   acceleration = -500.0;
   lifetimeMS = 500;
   height = 2.0;
   verticalCurve = 0.5;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.85 0.8 0.0 0.5";
   colors[1] = "0.85 0.8 0.0 0.25";
   colors[2] = "0.85 0.8 0.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ShockwaveData(IonCannonBlastShockwaveN) : IonCannonBlastShockwave
{
   is2D = false;
};

datablock ShockwaveData(IonCannonTrailShockwave)
{
   width = 15.0;
   numSegments = 12;
   numVertSegments = 6;
   velocity = 1;
   acceleration = -0.5;
   lifetimeMS = 3500;
   height = 1.0;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.1 0.3 1.0 1.0";
   colors[1] = "0.1 0.3 1.0 0.25";
   colors[2] = "0.1 0.3 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(IonCannonTrailExplosion)
{
//   soundProfile   = plasmaExpSound;
   shockwave = IonCannonTrailShockwave;
   faceViewer = true;
};

datablock LinearFlareProjectileData(IonCannonTrailCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::IonCannon;
   kickBackStrength    = 1;

   explosion           = "IonCannonTrailExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ExplosionData(IonCannonUpExplosion)
{
//   soundProfile   = plasmaExpSound;
   shockwave = IonCannonBlastShockwaveN;
   faceViewer = true;
};

datablock LinearFlareProjectileData(IonCannonUpCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::IonCannon;
   kickBackStrength    = 1;

   explosion           = "IonCannonUpExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ExplosionData(IonCannonWaveExplosion)
{
   soundProfile = BombExplosionSound;
   shockwave = IonCannonBlastShockwave;
   faceViewer = true;
};

datablock LinearFlareProjectileData(IonCannonWaveCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::IonCannon;
   kickBackStrength    = 1;

   explosion           = "IonCannonWaveExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ParticleData( IonCannonBlastCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.8 0.8 0.1 1.0";
   colors[1]     = "0.8 0.8 0.1 0.5";
   colors[2]     = "0.8 0.8 0.1 0.0";
   sizes[0]      = 4.0*6;
   sizes[1]      = 8.0*6;
   sizes[2]      = 9.0*6;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( IonCannonBlastCrescentEmitter )
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;
   ejectionVelocity = 75;
   velocityVariance = 10.0;
   ejectionOffset   = 30.0;
   thetaMin         = 10;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 750;
   particles = "IonCannonBlastCrescentParticle";
};

datablock ParticleData(IonCannonBlastExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.30;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 5000;
   lifetimeVarianceMS   = 1000;

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.5";
   colors[3]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 5.0*8;
   sizes[1]      = 6.0*8;
   sizes[2]      = 10.0*8;
   sizes[3]      = 12.0*8;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(IonCannonBlastExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 40.0;


   ejectionVelocity = 2.25;
   velocityVariance = 2.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "IonCannonBlastExplosionSmoke";
};

datablock ExplosionData(IonCannonBlastSubExplosion1)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 100;
   offset = 6.0*5;
   playSpeed = 0.25;

   sizes[0] = "25 25 25";
   sizes[1] = "25 25 25";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(IonCannonBlastSubExplosion2)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 50;
   offset = 5.0*6;
   playSpeed = 0.375;

   sizes[0] = "25 25 25";
   sizes[1] = "25 25 25";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(IonCannonBlastSubExplosion3)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.5;

   sizes[0] = "25 25 25";
   sizes[1] = "25 25 25";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(IonCannonBlastExplosion)
{
   soundProfile   = HugeExplosionSound;

//   shockwave = IonCannonBlastShockwave;
//   shockwaveOnTerrain = true;

   subExplosion[0] = IonCannonBlastSubExplosion1;
   subExplosion[1] = IonCannonBlastSubExplosion2;
   subExplosion[2] = IonCannonBlastSubExplosion3;

   emitter[0] = IonCannonBlastExplosionSmokeEmitter;
   emitter[1] = IonCannonBlastCrescentEmitter;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 32;
   debrisVelocity = 50.0;
   debrisVelocityVariance = 12.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

$IonSpeed = 500;
$IonLife = 16000;

datablock EnergyProjectileData(IONBeam)
{
   emitterDelay        = -1;
   directDamage        = 0.15;
   directDamageType    = $DamageType::IonCannon;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 1.0;

   explosion           = "IonCannonBlastExplosion";
   splash              = BlasterSplash;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 250;//200; --- 250 minimum? (ST)

   muzzleVelocity    = $IonSpeed;

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = $IonSpeed;
   wetVelocity       = $IonSpeed;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = $IonLife;

   hasLight    = true;
   lightRadius = 20.0;//30.0; --- Range is 1-20 (ST)
   lightColor  = "0.1 1.0 0.2";

   scale = "1 1 1";
   crossViewAng = 0.8;
   crossSize = 1.0;

   ignoreReflection = true;

   lifetimeMS     = $IonLife;
   blurLifetime   = 0.05;
   blurWidth      = 0.05;
   blurColor = "0.1 1.0 0.2 1.0";

   texture[0] = "special/expflare";//"special/blasterBolt";
   texture[1] = "special/expflare";//"special/blasterBoltCross";
};

datablock ParticleData(IonContrailParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = -0.05;
   lifetimeMS           = 10000;
   lifetimeVarianceMS   = 0;
   textureName          = "special/lightFallOffMono"; //"particleTest";
   colors[0]     = "0.3 1.0 0.2 1.0";
   colors[1]     = "0.6 0.6 0.6 0.75";
   colors[2]     = "1.0 1.0 1.0 0";
   sizes[0]      = 9;
   sizes[1]      = 6;
   sizes[2]      = 3;
   times[0]      = 0;
   times[1]      = 0.75;
   times[2]      = 1;
};

datablock ParticleEmitterData(IonContrailEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 1;
   ejectionVelocity = 0;
   velocityVariance = 0.0;
   ejectionOffset   = 2.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "IonContrailParticle";
};

//datablock ParticleData(SmallIonContrailParticle)	// obsolete, removed -soph
//{
//   dragCoefficient      = 2;
//   gravityCoefficient   = 0;
//   inheritedVelFactor   = 0.0;
//   constantAcceleration = -0.05;
//   lifetimeMS           = 4000;
//   lifetimeVarianceMS   = 0;
//   textureName          = "special/lightFallOffMono"; //"particleTest";
//   colors[0]     = "0.3 0.2 1.0 1.0";
//   colors[1]     = "0.6 0.6 0.6 0.75";
//   colors[2]     = "1.0 1.0 1.0 0";
//   sizes[0]      = 4;
//   sizes[1]      = 2;
//   sizes[2]      = 1;
//   times[0]      = 0;
//   times[1]      = 0.75;
//   times[2]      = 1;
//};

//datablock ParticleEmitterData(SmallIonContrailEmitter)	// obsolete, removed -soph
//{
//   ejectionPeriodMS = 5;
//   periodVarianceMS = 1;
//   ejectionVelocity = 0;
//   velocityVariance = 0.0;
//   ejectionOffset   = 2.0;
//   thetaMin         = 0;
//   thetaMax         = 10;
//   phiReferenceVel  = 0;
//   phiVariance      = 360;
//   overrideAdvances = false;
//   particles = "SmallIonContrailParticle";
//};

datablock LinearFlareProjectileData(IONBeamHead)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "20 20 20";
   faceViewer          = true;
   directDamageType    = $DamageType::IonCannon;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 1000;
   damageRadius        = 32 ;		// 20; -soph
   kickBackStrength    = 37500;
   radiusDamageType    = $DamageType::IonCannon;

   explosion           = "FireballAtmosphereBoltExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = IonContrailEmitter;

   dryVelocity       = $IonSpeed;
   wetVelocity       = $IonSpeed;       // faster uw
   velInheritFactor  = 1.0;
   fizzleTimeMS      = $IonLife;
   lifetimeMS        = $IonLife;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = $IonLife;

   ignoreReflection = true;

   activateDelayMS = -1;
   numFlares         = 10;
   flareColor        = "0.1 1.0 0.4";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/blasterboltcross";

   sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 20.0;//32.0; --- Range is 1-20 (ST)
   lightColor  = "0.9 0.5 0.1";

   size[0] = 1.0;
   size[1] = 7.0;
   size[2] = 5.0;
};

function IONBeamHead::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{								// +
	PARENT::onExplode( %data , %proj , %pos , %mod ) ;	// +
	schedule( 50 , 0 , RadiusExplosion , %proj , %pos , %data.damageRadius / 2 , mSqRt( %data.indirectDamage ), %data.kickBackStrength / 2 , %proj.sourceObject , %data.radiusDamageType ) ;
}								// +[/soph]


//datablock LinearFlareProjectileData(SmallIONBeamHead)	// obsolete, removed -soph
//{
  // projectileShapeName = "grenade_flare.dts";
  // scale               = "2.0 2.0 2.0";
//   scale               = "12 12 12";
//   faceViewer          = true;
//   directDamageType    = $DamageType::IonCannon;
//   directDamage        = 0;
//   hasDamageRadius     = true;
//   indirectDamage      = 20.0;
//   damageRadius        = 20;
//   kickBackStrength    = 12000;
//   radiusDamageType    = $DamageType::IonCannon;

//   explosion           = "FireballAtmosphereBoltExplosion";
//   splash              = PlasmaSplash;
//   baseEmitter         = SmallIonContrailEmitter;

//   dryVelocity       = $IonSpeed;
//   wetVelocity       = $IonSpeed;       // faster uw
//   velInheritFactor  = 1.0;
//   fizzleTimeMS      = $IonLife;
//   lifetimeMS        = $IonLife;
//   explodeOnDeath    = true;
//   reflectOnWaterImpactAngle = 0.0;
//   explodeOnWaterImpact      = false;
//   deflectionOnWaterImpact   = 0.0;
//   fizzleUnderwaterMS        = $IonLife;

//   ignoreReflection = true;

//   activateDelayMS = -1;
//   numFlares         = 10;
//   flareColor        = "0.1 1.0 0.4";
//   flareModTexture   = "flaremod";
//   flareBaseTexture  = "special/shrikeBoltCross";

//   sound      = PlasmaProjectileSound;
//   fireSound    = PlasmaFireSound;
//   wetFireSound = PlasmaFireWetSound;

//   hasLight    = true;
//   lightRadius = 20.0;//32.0; --- Range is 1-20 (ST)
//   lightColor  = "0.1 0.3 1.0";

//   size[0] = 1.0;
//   size[1] = 7.0;
//   size[2] = 5.0;
//};

datablock ShockwaveData(IonCannonBlastShockwave)
{
   width = 50.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 500;
   acceleration = -500.0;
   lifetimeMS = 500;
   height = 2.0;
   verticalCurve = 0.5;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.85 0.8 0.0 0.5";
   colors[1] = "0.85 0.8 0.0 0.25";
   colors[2] = "0.85 0.8 0.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ShockwaveData(IonCannonBlastShockwaveN) : IonCannonBlastShockwave
{
   is2D = false;
};

datablock ShockwaveData(IonCannonTrailShockwave)
{
   width = 15.0;
   numSegments = 12;
   numVertSegments = 6;
   velocity = 1;
   acceleration = -0.5;
   lifetimeMS = 3500;
   height = 1.0;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.1 0.3 1.0 1.0";
   colors[1] = "0.1 0.3 1.0 0.25";
   colors[2] = "0.1 0.3 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

function IONBeamHead::onExplode(%data, %proj, %pos, %mod)
{
   %player = %proj.client;

   if(%proj.ionnuke)
   {
      beginNuke(%proj.bkSourceObject, %pos);
      return;
   }

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%pos);
   %FFRObject.schedule(32, delete);

   %h = new LinearFlareProjectile()
   {
      dataBlock        = IonCannonUpCharge;
      initialDirection = "0 0 0";
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %player;
   };
   MissionCleanup.add(%h);

   %i = new LinearFlareProjectile()
   {
      dataBlock        = IonCannonWaveCharge;
      initialDirection = "0 0 1";
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %player;
   };
   MissionCleanup.add(%i);


   Parent::onExplode(%data, %proj, %pos, %mod);
}

function etExplode(%src, %option, %pos, %mod)
{
   if(isObject(%src))
   {
      %final = vectorAdd(%pos, %mod);
      %p = new LinearFlareProjectile()
      {
         dataBlock        = %option;
         initialDirection = "0 0 0";
         initialPosition  = %final;
         sourceObject     = %src.bks;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);

//      %p.sourceObject = %src;
   }
}

function etShock(%src, %option, %pos, %mod)
{
   if(isObject(%src))
   {
      %final = vectorAdd(%pos, %mod);
      %p = new LinearFlareProjectile()
      {
         dataBlock        = %option;
         initialDirection = "0 0 1";
         initialPosition  = %final;
         sourceObject     = %src.bks;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);

//      %p.sourceObject = %src;
   }
}

function beginNuke(%obj, %pos, %bool) // 90 + random generated from loopFX() explosions total
{
   %obj.bks = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%obj.bks);
   %obj.bks.setPosition(%pos);
   %obj.bks.schedule(15000, delete);

   loopDamageFX2(%obj, vectorAdd(%pos, "0 0 150"), 30, 100, 20, LinearFlareProjectile, ETMortar); // random explosion effect

   etExplode(%obj, ETMissile, %pos, "10 0 5"); // base
   etExplode(%obj, ETMissile, %pos, "-10 0 5");
   etExplode(%obj, ETMissile, %pos, "0 10 5");
   etExplode(%obj, ETMissile, %pos, "0 -10 5");
   etExplode(%obj, ETMissile, %pos, "10 10 5");
   etExplode(%obj, ETMissile, %pos, "-10 -10 5");
   etExplode(%obj, ETMissile, %pos, "-10 10 5");
   etExplode(%obj, ETMissile, %pos, "10 -10 5");

//   etExplode(%obj, ETMissile, %pos, "0 0 5");		// -soph // stem (recursive loop rendering)
//   etExplode(%obj, ETMissile, %pos, "0 0 10");	// -soph

   for(%i = 1; %i < 20; %i++)				// (%i = 0; %i < 17; %i++) -soph
   {
//      if(%z < 10)					// -[soph]
//         %z = 10;
//      else
//         %z += 5;

//      %mod = "0 0 "@%z;

//      etExplode(%obj, ETMortar, %pos, %mod);		// -[/soph]

      %mod = "0 0 " @ ( 5 * %i ) ;			// +[soph]
      schedule( %i * 10 , 0 ,  etExplode , %obj , ETMortar , %pos , %mod ) ;
   }
   etExplode( %obj , ETMainWave , %pos , "0 0 5" ) ;	// +[/soph]

   if(!%bool)
      schedule(250, 0, nukStage2, %obj, %pos);
}

function nukStage2(%obj, %pos)
{
   etExplode(%obj, ETSmallWave, %pos, "0 0 20"); // 4 mini-shockwaves
   etExplode(%obj, ETSmallWave, %pos, "0 0 45");
   etExplode(%obj, ETSmallWave, %pos, "0 0 62.5");
   etExplode(%obj, ETSmallWave, %pos, "0 0 80");

   etExplode(%obj, ETMainWave, %pos, "0 0 45"); // huge shockwave
   etExplode(%obj, ETD, %pos, "0 0 44"); // Nuke Damage Blast

   %posUp = getWord( %pos , 0 ) SPC getWord( %pos , 0 ) SPC ( getWord( %pos , 0 ) + 5 ) ;	// +soph
   RadiusExplosion( %obj , %posUp , 100 , 2.5 , 6000 , %obj , $DamageType::Nuke );		// +soph

   etExplode(%obj, ETMissile, %pos, "10 0 100"); // Top of nuke, slice 1
   etExplode(%obj, ETMissile, %pos, "-10 0 100");
   etExplode(%obj, ETMissile, %pos, "0 10 100");
   etExplode(%obj, ETMissile, %pos, "0 -10 100");
   etExplode(%obj, ETMissile, %pos, "10 10 100");
   etExplode(%obj, ETMissile, %pos, "-10 -10 100");
   etExplode(%obj, ETMissile, %pos, "-10 10 100");
   etExplode(%obj, ETMissile, %pos, "10 -10 100");

   etExplode(%obj, ETMissile, %pos, "20 0 110"); // Top of nuke, slice 2
   etExplode(%obj, ETMissile, %pos, "-20 0 110");	// 15 -> 20 -soph
   etExplode(%obj, ETMissile, %pos, "0 20 110");
   etExplode(%obj, ETMissile, %pos, "0 -20 110");
   etExplode(%obj, ETMissile, %pos, "15 15 110");
   etExplode(%obj, ETMissile, %pos, "-15 -15 110");
   etExplode(%obj, ETMissile, %pos, "-15 15 110");
   etExplode(%obj, ETMissile, %pos, "15 -15 110");

   etExplode(%obj, ETMissile, %pos, "35 0 120"); // Top of nuke, slice 3
   etExplode(%obj, ETMissile, %pos, "-35 0 120");	// 20 -> 35 -soph
   etExplode(%obj, ETMissile, %pos, "0 35 120");
   etExplode(%obj, ETMissile, %pos, "0 -35 120");
   etExplode(%obj, ETMissile, %pos, "30 30 120");	// 20 -> 30 -soph
   etExplode(%obj, ETMissile, %pos, "-30 -30 120");
   etExplode(%obj, ETMissile, %pos, "-30 30 120");
   etExplode(%obj, ETMissile, %pos, "30 -30 120");

   etExplode(%obj, ETMissile, %pos, "45 0 130"); // Top of nuke, slice 4
   etExplode(%obj, ETMissile, %pos, "-45 0 130");	// 25 -> 45 -soph
   etExplode(%obj, ETMissile, %pos, "0 45 130");
   etExplode(%obj, ETMissile, %pos, "0 -45 130");
   etExplode(%obj, ETMissile, %pos, "40 40 130");	// 25 -> 40 -soph
   etExplode(%obj, ETMissile, %pos, "-40 -40 130");
   etExplode(%obj, ETMissile, %pos, "-40 40 130");
   etExplode(%obj, ETMissile, %pos, "40 -40 130");

   etExplode(%obj, ETMissile, %pos, "35 0 140"); // Top of nuke, slice 5
   etExplode(%obj, ETMissile, %pos, "-35 0 140");	// 20 -> 35 -soph
   etExplode(%obj, ETMissile, %pos, "0 35 140");
   etExplode(%obj, ETMissile, %pos, "0 -35 140");
   etExplode(%obj, ETMissile, %pos, "30 30 140");	// 20 -> 30 -soph
   etExplode(%obj, ETMissile, %pos, "-30 -30 140");
   etExplode(%obj, ETMissile, %pos, "-30 30 140");
   etExplode(%obj, ETMissile, %pos, "30 -30 140");

   etExplode(%obj, ETMissile, %pos, "20 0 150"); // Top of nuke, slice 6
   etExplode(%obj, ETMissile, %pos, "-20 0 150");	// 15 -> 20 -soph
   etExplode(%obj, ETMissile, %pos, "0 20 150");
   etExplode(%obj, ETMissile, %pos, "0 -20 150");
   etExplode(%obj, ETMissile, %pos, "15 15 150");
   etExplode(%obj, ETMissile, %pos, "-15 -15 150");
   etExplode(%obj, ETMissile, %pos, "-15 15 150");
   etExplode(%obj, ETMissile, %pos, "15 -15 150");

   etExplode(%obj, ETMissile, %pos, "10 0 160"); // Top of nuke, slice 7
   etExplode(%obj, ETMissile, %pos, "-10 0 160");
   etExplode(%obj, ETMissile, %pos, "0 10 160");
   etExplode(%obj, ETMissile, %pos, "0 -10 160");
   etExplode(%obj, ETMissile, %pos, "10 10 160");
   etExplode(%obj, ETMissile, %pos, "-10 -10 160");
   etExplode(%obj, ETMissile, %pos, "-10 10 160");
   etExplode(%obj, ETMissile, %pos, "10 -10 160");

   etExplode(%obj, ETMissile, %pos, "0 0 170"); // Cap of nuke

   schedule(750, 0, beginNuke, %obj, %pos, true);
}

function beginMiniNuke(%cl, %pos)
{
   %obj = %cl.player;
   loopDamageFX2(%obj, vectorAdd(%pos, "0 0 20"), 5, 50, 20, LinearFlareProjectile, ReaverCharge); // random explosion effect

   etExplode(%obj, ReaverCharge, %pos, "1 0 0.5"); // base
   etExplode(%obj, ReaverCharge, %pos, "-1 0 0.5");
   etExplode(%obj, ReaverCharge, %pos, "0 1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "0 -1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "1 1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "-1 -1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "-1 1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "1 -1 0.5");

   etExplode(%obj, ReaverCharge, %pos, "0 0 0.5"); // stem (recursive loop rendering)
   etShock(%obj, BAArmorBlowout, %pos, "0 0 8"); // shockwave

   for(%i = 0; %i < 15; %i++)
   {
      %mod = "0 0 "@%i;

      etExplode(%obj, ReaverCharge, %pos, %mod);
   }

   schedule(1500, 0, continueMiniNuke, %cl, %pos);
}

function continueMiniNuke(%cl, %pos)
{
   %obj = %cl.player;

   etExplode(%obj, ReaverCharge, %pos, "1 0 0.5"); // base
   etExplode(%obj, ReaverCharge, %pos, "-1 0 0.5");
   etExplode(%obj, ReaverCharge, %pos, "0 1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "0 -1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "1 1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "-1 -1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "-1 1 0.5");
   etExplode(%obj, ReaverCharge, %pos, "1 -1 0.5");

   etExplode(%obj, ReaverCharge, %pos, "0 0 0.5"); // stem (recursive loop rendering)

   for(%i = 0; %i < 15; %i++)
   {
      %mod = "0 0 "@%i;

      etExplode(%obj, ReaverCharge, %pos, %mod);
   }
}

function MANTAFireIonBlast(%obj, %FFRObject, %pointdir, %pos2)
{
   %p = new EnergyProjectile()
   {
      dataBlock        = IONBeam;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %h = new LinearFlareProjectile()
   {
      dataBlock        = IONBeamHead;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%h);

   schedule(750, 0, "projectileTrail", %h, 50, LinearFlareProjectile, IonCannonTrailCharge);
}

function MANTAFireIonRain(%obj, %FFRObject, %pointdir, %pos2)
{
   %p = new EnergyProjectile()
   {
      dataBlock        = IONRainBlast;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %h = new LinearFlareProjectile()
   {
      dataBlock        = IONRainBlastHead;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%h);
}

function MANTAFireIonNuke(%obj, %FFRObject, %pointdir, %pos2)
{
   %p = new EnergyProjectile()
   {
      dataBlock        = IONBeam;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %h = new LinearFlareProjectile()
   {
      dataBlock        = IONBeamHead;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
      ionNuke = true;
   };
   MissionCleanup.add(%h);
}
