function NoneShields::applyMod(%vData, %obj, %moduleID)
{
     // NoSpam
}

function StandardVehicleShields::applyMod(%vData, %obj, %moduleID)
{
     // NoSpam
}

function HeatShields::applyMod(%vData, %obj, %moduleID)
{
     %obj.heatshielded = true;

     heatShieldLoop(%obj);
}

function heatShieldLoop(%obj)
{
     %obj.setHeat(0);

     schedule(250, %obj, heatShieldLoop, %obj);
}

function RepulsorFieldShields::applyMod(%vData, %obj, %moduleID)
{
    %type = %vData.hullType;
    %radius = 5;
    
    if(%type & $VehicleHull::Shrike)
    {
         %scale = "4 4 4";
         %radius = 20;
    }
    else if(%type & $VehicleHull::Thundersword)
    {
         %scale = "6 6 6";
         %radius = 24;    
    }
    else if(%type & $VehicleHull::Havoc)
    {
         %scale = "8 8 8";
         %radius = 30;    
    }
    else    
    {
         %scale = "9 9 9";
         %radius = 20;    
    }
    
//    echo("size" SPC %scale SPC %radius);
    %field = new StaticShape()
    {
        scale = %scale;
        dataBlock = "WarpBubble";
    };
    
    if(%vData.vehicleType & $VehicleMask::Retaliator)
    {
         %field.mountSlot = 10;
         %obj.mountObject(%field, 10);
    }
    else
    {
         %field.mountSlot = 6;
         %obj.mountObject(%field, 6);
    }
         
    %obj.repulsorField = %field;
    %field.ownerF = %obj;
    %field.startFade(1, 0, true);
    %field.playThread(0, "deploy");
    %field.schedule(5000, startFade, 250, 0, false);

    fieldParentCheck(%field);
    RepulsorShieldBubbleReflect(%obj, %radius);
}

function fieldParentCheck(%field)
{
     if(isObject(%field.ownerF))
     {
          %field.ownerF.mountObject(%field, %field.mountSlot);
          schedule(2000, %field, fieldParentCheck, %field);
     }
     else
          %field.delete();
}

function RepulsorShieldBubbleReflect(%obj, %radius)
{
   if(isObject(%obj) && isObject(%obj.repulsorField) && isObject(%obj.shieldCap))
   {
        %pos = %obj.getPosition();
        InitContainerRadiusSearch(%pos, %radius, $TypeMasks::ProjectileObjectType);
     
        while((%int = ContainerSearchNext()) != 0)
        {
           if( %obj.shieldCap.getCapacitorLevel() < %obj.getRechargeRate() * 60 )
               break;

           if(%obj.siegeDeployed)
               break;

           if(%int.reflected)
              continue;

           if(%int.sourceObject.team == %obj.team)
               continue;

           if(isReflectableProjectile(%int, ""))
           {
                if ( %intData.hasDamageRadius && %intData.indirectDamage > ( 2 * %intData.directDamage ) )	// +[soph]
                     %damage = %intData.indirectDamage;								// +
                else												// +
                     %damage = %intData.directDamage;								// +
                if( %int.damageMod > 0 )									// +
                    %damage *= %int.damageMod ;									// +[/soph]

                if( %FFRObject )
                {
                     %FFRObject = new StaticShape()
                     {
                        dataBlock = mReflector;
                     };
                     MissionCleanup.add(%FFRObject);
                     %obj.ffrobject = %FFRObject;
                     %FFRObject.schedule( 32 , delete ) ;
                }
                %FFRObject.setPosition(%int.getPosition());

                %pVec = VectorNormalize( %int.initialDirection ) ;						// +[soph]
                %ctrPos = VectorAdd( %obj.getPosition() , "0 0 3" ) ;						// + from deployable repulsor logic
                %vecFromCtr = VectorNormalize( VectorSub( %int.getPosition() , %ctrPos ) ) ;			// +
														// +
                %flip = VectorDot( %vecFromCtr , %pVec ) ;							// +
                %flip = VectorScale( %vecFromCtr , %flip ) ;							// +
                %newVec = VectorAdd( %pVec , VectorScale( %flip , -2 ) ) ;					// +
                %newPos = VectorAdd( %int.getPosition() , %newVec ) ;						// +[/soph]

                if(%int.getClassName() $= "BombProjectile")
                     %newvec = vectorScale( %vector , 350 ) ;							// +soph
     
                %intData = %int.getDatablock();
                %objData = %obj.getDatablock();
                %damageType = %intData.directDamageType ? %intData.directDamageType : %intData.radiusDamageType;

                if(!isObject(%int.sourceObject))
                    %src = %int.bkSourceObject;
                else
                    %src = %int.sourceObject;
                          
                if( %int.originTime )										// +[soph]
                {												// +
                   %airTimeMod = getSimTime() - %int.originTime ; 						// +
                   %data = %int.getDatablock().getName() ;							// +
                   if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
                      %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
                   else												// +
                      %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;				// +
                   %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;					// +
                }												// +[/soph]  
   
                %p = new(%int.getClassName())()
                {
                   dataBlock         = %intData.getName();
                   initialDirection  = %newvec ;								// %vector; -soph
                   initialPosition   = %int.getPosition();
                   sourceObject      = %obj.ffrobject;
                   sourceSlot        = %int.sourceSlot;
                   reflected         = true;
                   bkSourceObject    = %src;
                   starburstMode     = %int.starburstMode;
                   vehicleMod        = %int.vehicleMod;
                };
                MissionCleanup.add(%p);

                if( isObject( %src ) )										// +soph
                {
                     %int.sourceObject.lastProjectile = %p ;
                     if( %src.lastMortar == %int )								// if(%src.lastMortar !$= "") -soph
                         %src.lastMortar = %p;
                }

                %p.originTime = %int.originTime ;								// +soph
             
                if(%int.trailThread)
                   projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);
     
                if(%int.proxyThread)
                   AABeginProxy(%p);
                   
                if(%int.lanceThread)
                {        
                    SBLanceRandomThread(%p);
                    %p.lanceThread = true;
                }

                createLifeEmitter(%int.getPosition(), "WadBlueTrailEmitter", 300);
                %obj.getDatablock().shieldCapCheck( %obj , %int.getPosition() , %damage , %damageType ) ;
                %int.delete();
           }
           else continue;
        }

        schedule($Host::RepulsorTickRate, %obj, RepulsorShieldBubbleReflect, %obj, %radius);
   }
}

datablock SensorData(VehicleJammerSensor)
{
   detects = true;
   detectsUsingLOS = false;
   detectsPassiveJammed = false;
   detectsActiveJammed = false;
   detectsCloaked = false;
   detectionPings = false;
   detectRadius = 45;
   
   jams = true;
   jamsOnlyGroup = false;
   jamsUsingLOS = false;
   jamRadius = 10;
};

function JammerFieldShields::applyMod(%vData, %obj, %moduleID)
{
     %obj.isJammed = true;

     setTargetSensorData(%obj.target, VehicleJammerSensor);
}
//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
