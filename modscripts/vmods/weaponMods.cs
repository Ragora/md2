function NoneWeapon::applyMod(%vData, %obj, %moduleID, %vType)
{
     // NoSpam
}

function CatLance::applyMod( %vData , %obj , %moduleID , %vType )
{
	%obj.mountImage( TurbocatShocklance , 2 ) ;

	%obj.weapon[ 1 , Display ] = true ;
	%obj.weapon[ 1 , Name ] = "Shocklance" ;
	%obj.weapon[ 1 , Description ] = "Eletrify your enemies with a 2x range shocklance." ;
}

function CatMissile::applyMod( %vData , %obj , %moduleID , %vType )
{
	%obj.mountImage( ViperMissileParam , 0 , false ) ;
	%obj.mountImage( ViperImage , 2 ) ;
	%obj.mountImage( ViperImagePair , 3 ) ;

	%obj.weapon[ 1 , Display ] = true ;
	%obj.weapon[ 1 , Name ] = "Viper Missiles" ;
	%obj.weapon[ 1 , Description ] = "Ground-launched anti-air missiles." ;
}

//----------------------------------------------------------------------------

datablock TracerProjectileData(ReaverChaingunBullet)
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.11;
   directDamageType    = $DamageType::Bullet;
   hasDamageRadius     = true;	// = false; -soph

   indirectDamage      = 0.001;	// +[soph]
   damageRadius        = 0.5;	// +
   radiusDamageType    = $DamageType::Bullet;
   kickBackStrength    = 100;	// +[/soph]

   splash			   = ChaingunSplash;

   kickbackstrength    = 0.0;
//   sound          	   = TankChaingunProjectile;

   dryVelocity       = 425.0;
   wetVelocity       = 100.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.10;
   crossSize       = 0.20;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;

   activateDelayMS   = 100;

   explosion = ChaingunExplosion;
};

function ReaverChaingunBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
  if(detectIsWet(%position))
     serverPlay3d(getRandomChaingunWetSound(), %position);
   else
      serverPlay3d(getRandomChaingunSound(), %position);

  Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

datablock ShapeBaseImageData(ReaverWeaponPairImage)
{
   className = WeaponImage;
   shapeFile = "turret_tank_barrelchain.dts";
   projectile = ReaverChaingunBullet;
   projectileType = TracerProjectile;
   ammo = ChaingunAmmo;
   mountPoint = 10;
   offset = "2.0 -1.0 -0.25";
//   rotation = "0 1 0 0";
//   offset = "1.15 1 0.7";
//   rotation = "0 1 0 32";
//   ammo 	 = ChaingunAmmo;

   usesEnergy = false;
   useMountEnergy = true;
   minEnergy = 3;
   fireEnergy = 1;

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 4.0;

   projectileSpread = 5.0 / 1000.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AssaultChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.075;	// = 0.1; -soph
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(ReaverWeaponImage) : ReaverWeaponPairImage
{
//   minEnergy = 3;
//   fireEnergy = 1;

   offset = "-2.0 -1.0 -0.25";
//   offset = "-1.15 1 0.7";
//   rotation = "0 1 0 -32";
};

function ReaverWeaponImage::onFire(%data, %obj, %slot)
{
     Parent::onFire(%data, %obj, %slot);
     %obj.secondaryAmmo = %data.ammo;									// screw this, just gonna hack it -soph
     %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %obj.secondaryAmmo);	// %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo); -soph
}

function ReaverWeaponPairImage::onFire(%data, %obj, %slot)
{
     Parent::onFire(%data, %obj, %slot);
     %obj.secondaryAmmo = %data.ammo;									// screw this, just gonna hack it -soph
     %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %obj.secondaryAmmo);	// %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo); -soph
}

function ReaverWeaponImage::onDryFire(%data, %obj, %slot)
{
     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);
}

function ReaverWeaponPairImage::onDryFire(%data, %obj, %slot)
{
     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);
}

function TurboChaingun::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.mountImage(ReaverWeaponImage, 4);
     %obj.mountImage(ReaverWeaponPairImage, 5  );
}

function TurboChaingun::onTrigger(%vData, %obj, %state)
{
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(4, false);
               %obj.setImageTrigger(5, false);
          case 1:
               %obj.fireWeapon = true;

               %obj.setImageTrigger(4, true);
               %obj.setImageTrigger(5, true);

               %obj.setImageTrigger(2, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(3, false);	// +[/soph]
     }
}

//----------------------------------------------------------------------------

datablock ShapeBaseImageData(BlasterShotgunPairImage)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";	// "TR2weapon_grenade_launcher.dts"; -soph
   item      = Chaingun;
//   ammo   = ChaingunAmmo;
   projectile = ScoutChaingunBullet;
   projectileType = TracerProjectile;
   mountPoint = 10;
//**original**   offset = ".73 0 0";
//   offset = "1.93 -0.52 0.044";

   offset = "1 1 0.7";
   rotation = "0 1 0 32";

   projectileSpread = 16.0 / 1000.0;	//was 8.0 / 1000.0;  -soph

   usesEnergy = true;
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 75;	// 100; -soph
   fireEnergy = -1;
   fireTimeout = 1500;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
//   stateSound[4]            = ;
   stateSequence[4]         = "Fire";
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 0.5;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(BlasterShotgunImage) : BlasterShotgunPairImage
{
   minEnergy = 150;	// 200; -soph
   fireEnergy = -1;
   offset = "-1 1 0.7";
   rotation = "0 1 0 -32";
};

function BlasterShotgunImage::onFire(%data,%obj,%slot)
{
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   %obj.play3D(AAFireSound);
   %obj.useEnergy( 75 ) ;		// %obj.useEnergy(100); -soph
}

function BlasterShotgunPairImage::onFire(%data,%obj,%slot)
{
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   %obj.play3D(AAFireSound);
   %obj.useEnergy( 75 ) ;		// %obj.useEnergy(100); -soph
}

function BlasterShotgun::applyMod(%vData, %obj, %moduleID, %vType)
{
   %obj.mountImage(BlasterShotgunImage, 2);
   %obj.mountImage(BlasterShotgunPairImage, 3);
}

function BlasterShotgun::onTrigger(%vData, %obj, %state)
{
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(2, false);
               %obj.setImageTrigger(3, false);
          case 1:
               %obj.fireWeapon = true;

               %obj.setImageTrigger(2, true);
               %obj.setImageTrigger(3, true);

               %obj.setImageTrigger(4, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(5, false);	// +[/soph]
     }
}

//----------------------------------------------------------------------------

function ScoutChaingunImage::onFire(%data,%obj,%slot)
{
   // obj = ScoutFlyer object number
   // slot = 2

  // %time = getSimTime();						// let's try removing this -[soph]

  // if(%time < %obj.lastFirePrimary)
  //     return;
  // else
  //     %obj.lastFirePrimary = %time + ( %data.fireTimeout / 2 );	// %obj.lastFirePrimary = %time + %data.fireTimeout - 5;
       									// -[/soph]
   Parent::onFire(%data,%obj,%slot);
   %obj.nextWeaponFire = 3;
   schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function ScoutChaingunPairImage::onFire(%data,%obj,%slot)
{
   // obj = ScoutFlyer object number
   // slot = 3

  // %time = getSimTime();						// let's try removing this -[soph]

  // if(%time < %obj.lastFirePrimary)
  //     return;
  // else
  //     %obj.lastFirePrimary = %time + ( %data.fireTimeout / 2 );	// %obj.lastFirePrimary = %time + %data.fireTimeout - 5;
              								// -[/soph]
   Parent::onFire(%data,%obj,%slot);
   %obj.nextWeaponFire = 2;
   schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function ShrikeBlaster::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.mountImage(ScoutChaingunImage, 2);
     %obj.mountImage(ScoutChaingunPairImage, 3);     
}

function ShrikeBlaster::onTrigger(%vData, %obj, %state)
{
//     echo("SBFire" SPC %obj.nextWeaponFire SPC %state);
     
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(2, false);               
               %obj.setImageTrigger(3, false);
          case 1:
               %obj.fireWeapon = true;

               if(%obj.nextWeaponFire == 2)
               {
                    %obj.setImageTrigger(2, true);
                    %obj.setImageTrigger(3, false);
               }
               else
               {
                    %obj.setImageTrigger(2, false);
                    %obj.setImageTrigger(3, true);
               }

               %obj.setImageTrigger(4, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(5, false);	// +[/soph]
     }
}

//----------------------------------------------------------------------------

datablock LinearFlareProjectileData(SpikeRifleBoltF)	// +[soph]
{							// + duplicated here from weapons/spike.cs
//   projectileShapeName = "plasmabolt.dts";		// + needs to exist for SpikeCannonPairImage
   scale               = "1.0 7.0 1.0";			// +
   faceViewer          = true;				// +
   directDamage        = 0.05;				// +
   directDamageType    = $DamageType::OutdoorDepTurret;	// +
   hasDamageRadius     = true;				// +
   indirectDamage      = 0.35;				// +
   damageRadius        = 4.5;				// +
   kickBackStrength    = 1000.0;			// +
   radiusDamageType    = $DamageType::OutdoorDepTurret;	// +
   sound          	   = BlasterProjectileSound;	// +
   explosion           = GaussExplosion;		// +
							// +
   splash              = PlasmaSplash;			// +
							// +
   dryVelocity       = 500.0;				// +
   wetVelocity       = 275.0;				// +
   velInheritFactor  = 1.0;				// +
   fizzleTimeMS      = 2000;				// +
   lifetimeMS        = 2500;				// +
   reflectOnWaterImpactAngle = 0.0;			// +
   explodeOnWaterImpact      = true;			// +
   deflectionOnWaterImpact   = 0.0;			// +
   fizzleUnderwaterMS        = -1;			// +
							// +
   activateDelayMS = -1;				// +
							// +
   size[0]           = 0.2;				// +
   size[1]           = 0.5;				// +
   size[2]           = 0.1;				// +
							// +
   numFlares         = 35;				// +
   flareColor        = "1 0.75 0.25";			// +
   flareModTexture   = "flaremod";			// +
   flareBaseTexture  = "special/landSpikeBoltCross";	// +
							// +
   hasLight    = true;					// +
   lightRadius = 3.0;					// +
   lightColor  = "1 0.75 0.25";				// +
};							// +[/soph]

datablock ShapeBaseImageData(SpikeCannonPairImage)
{
   className = WeaponImage;
   shapeFile = "turret_missile_large.dts";

   ammo   = PlasmaAmmo;
   
   projectile = SpikeRifleBoltF ;		// VehicleSpikeBolt; -soph	
   projectileType = LinearFlareProjectile ;	// TracerProjectile; -soph
   mountPoint = 10;
//**original**   offset = ".73 0 0";
//   offset = "1.93 -0.52 0.044";

   offset = "3.25 -1.5 -1.0";
   rotation = "0 1 0 -125";

   projectileSpread = 1.0 / 1000.0 ;		// readded +soph

   usesEnergy = false;
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 15;
   fireEnergy = 15;
   fireTimeout = 375;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
//   stateSound[4]            = ShrikeBlasterFire;
   stateSound[4]            = OBLFireSound;
   stateSequence[4]         = "Fire";
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 0.375;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateScript[7]              = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(SpikeCannonImage) : SpikeCannonPairImage
{
   offset = "-3.25 -1.5 -1.0";
   rotation = "0 1 0 125";
};

function SpikeCannonImage::onFire(%data, %obj, %slot)
{
//     Parent::onFire(%data, %obj, %slot);	// -soph

     %vector = %obj.getForwardVector() ;	// +[soph]
     %x = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
     %y = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
     %z = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
     %matrix = MatrixCreateFromEuler( %x SPC %y SPC %z ) ;
     %vector = MatrixMulVector( %matrix , %vector ) ;
						// +
     %p = new ( %data.projectileType )() {	// +
          dataBlock        = %data.projectile ;	// +
          initialDirection = %vector ;		// +
          initialPosition  = %obj.getMuzzlePoint( %slot ) ;
          sourceObject     = %obj ;		// +
          sourceSlot       = %slot ;		// +
     } ;					// +
     %obj.secondaryAmmo = %data.ammo;		// + screw this, just gonna hack it
     %obj.decInventory( %obj.secondaryAmmo , 1 ) ;
						// +[/soph]

     %obj.secondaryAmmo = %data.ammo;		// screw this, just gonna hack it -soph
//     %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo); // -soph

}

function SpikeCannonPairImage::onFire(%data, %obj, %slot)
{
//     Parent::onFire(%data, %obj, %slot);	// -soph

     %vector = %obj.getForwardVector() ;	// +[soph]
     %x = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
     %y = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
     %z = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
     %matrix = MatrixCreateFromEuler( %x SPC %y SPC %z ) ;
     %vector = MatrixMulVector( %matrix , %vector ) ;
						// +
     %p = new ( %data.projectileType )() {	// +
          dataBlock        = %data.projectile ;	// +
          initialDirection = %vector ;		// +
          initialPosition  = %obj.getMuzzlePoint( %slot ) ;
          sourceObject     = %obj ;		// +
          sourceSlot       = %slot ;		// +
     } ;					// +
     MissionCleanup.add( %p ) ;			// +
     %obj.decInventory( %obj.secondaryAmmo , 1 ) ;
						// +[/soph]

     %obj.getDatablock().updateAmmoCount( %obj , %obj.getMountNodeObject(0).client , %obj.secondaryAmmo ) ;	// %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo); -soph
}

function SpikeCannonImage::onDryFire(%data, %obj, %slot)
{
     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);
}

function SpikeCannonPairImage::onDryFire(%data, %obj, %slot)
{
     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);
}

function SpikeArray::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.mountImage(SpikeCannonImage, 4);
     %obj.mountImage(SpikeCannonPairImage, 5);
}

function SpikeArray::onTrigger(%vData, %obj, %state)
{
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(4, false);               
               %obj.setImageTrigger(5, false);

          case 1:
               %obj.fireWeapon = true;

               %obj.setImageTrigger(4, true);
               %obj.setImageTrigger(5, true);

               %obj.setImageTrigger(2, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(3, false);	// +[/soph]
     }
}

//----------------------------------------------------------------------------

datablock ShapeBaseImageData(SeekingMissileParam)
{
   mountPoint = 4;
   offset = "0 0.1 -0.3";
   rotation = "0 1 0 180";
   shapeFile = "turret_muzzlepoint.dts";

   isSeeker     = true;
   seekRadius   = 300;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 5;
};

datablock ShapeBaseImageData(PumaMissileA) : EngineAPEImage
{
   shapeFile = "vehicle_grav_scout.dts";
   mountPoint = 1;
   offset = "-4.5 0.25 -2.75";
   rotation = "0 1 0 -52.5";
};

datablock ShapeBaseImageData(PumaMissileB) : PumaMissileA
{
   mountPoint = 1;
   offset = "4.5 0.25 -2.75";
   rotation = "0 1 0 52.5";
};

datablock SeekerProjectileData(PumaMissile)
{
   scale = "10.0 10.0 10.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_grav_scout.dts";
   directDamage        = 0.75;			// = 0.5; -soph
   directDamageType    = $DamageType::EMP;	// = $DamageType::EnergyBlast; -soph
   hasDamageRadius     = true;
   indirectDamage      = 0.625 ;		// = 1.0; -soph
   damageRadius        = 9.5;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 3750;

   explosion           = "LargeMissileExplosion";
   underwaterExplosion = UnderwaterHandGrenadeExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 100;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 7000;
   muzzleVelocity      = 40.0;
   maxVelocity         = 200.0;
   turningSpeed        = 80.0;
   acceleration        = 50.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   flechetteDelayMs = 50;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

function PumaMissile::onExplode(%data, %proj, %pos, %mod)
{
   %proj.exploded = true ;	// +soph
   if(%proj.bkSourceObject)
      if(isObject(%proj.bkSourceObject))
         %proj.sourceObject = %proj.bkSourceObject;

     %modifier = 1;

   if(%data.hasDamageRadius) // I lost a bomb. Do you have it?
   {
      if(%proj.vehicleMod)
           %modifier *= %proj.vehicleMod;

      if(%data.passengerDamageMod && %proj.passengerCount)
          %modifier *= 1 + (%proj.passengerCount * %data.passengerDamageMod);

      if(%proj.damageMod)
          %modifier *= %proj.damageMod;

     // RadiusExplosion(%proj, %pos, %data.damageRadius, %data.directDamage * %modifier, 0, %proj.sourceObject, %data.directDamageType);	// -soph

      if( %proj.RMSTarget )															// +[soph]
         RadiusExplosion( %proj , %pos , RMSSCruiseMissile.damageRadius / 2 , RMSSCruiseMissile.indirectDamage / 2 , RMSSCruiseMissile.kickBackStrength / 1.5 , %proj.sourceObject , RMSSCruiseMissile.radiusDamageType ) ;
      else
         EMPBurstExplosion(%proj, %pos, %data.damageRadius, %data.directDamage * %modifier, 0, %proj.sourceObject, %data.directDamageType);	// +[/soph]
      RadiusExplosion(%proj, %pos, %data.damageRadius, %data.indirectDamage * %modifier, %data.kickBackStrength, %proj.sourceObject, %data.radiusDamageType);
   }
}

function handlePumaLaunch(%data, %obj)
{
     %obj.setImageTrigger(6, false);
     %obj.setImageTrigger(7, false);

     if(%obj.pumaLaunchTimeout)
          return;

     if(%obj.pumaReady[6])
          launchPumaMissile(%obj, 6);
     else if(%obj.pumaReady[7])
          launchPumaMissile(%obj, 7);
     else
          %obj.play3d(BomberTurretDryFireSound);
}

function launchPumaMissile(%obj, %slot)
{
     %player = %obj.getMountNodeObject(0);
     %vehicle = %obj.getObjectMount();

     %fwdVec = %obj.getForwardVector();
//     %fwdVec = %obj.getMuzzleVector(%slot);
     %point = %obj.getMuzzlePoint(%slot);
     %point = vectorAdd(vectorScale(vectorNormalize(%fwdVec), 6.0), %point);
//     %FFRObject = new StaticShape()
//     {
//          dataBlock        = mReflector;
//     };
//     %FFRObject.setPosition(%point);
//     %FFRObject.schedule(32, delete);

     %p = new SeekerProjectile()
     {
         dataBlock        = "PumaMissile";
         initialDirection = %fwdVec;
         initialPosition  = %point;
//         sourceObject     = %FFRObject;
         sourceObject     = %player;
         sourceSlot       = %slot;
         vehicleObject    = %obj;
         vehicleMod       = %vehicle.damageMod;
//         bkSourceObject   = %player;
         accuFire         = true;
      };

//   Item::setCollisionTimeout(%p, %obj);
//   %p.getDatablock().schedule(1000, onExplode, %p, %p.getPosition(), 1);
   MissionCleanup.add(%p);
   MissileSet.add(%p);

   %target = %obj.getLockedTarget();
   %vehicle.setCollisionTimeout( %p ) ; 	// +soph
   if(%target)
//      %p.setObjectTarget(%target);		// -soph
      if( %target.getDatablock().jetfire )	// +[soph]
         assignTrackerTo( %target , %p ) ;	// + from hand launcher code
      else					// +
         %p.setObjectTarget( %target ) ;	// +[/soph]
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget();

//     if(%target)
//          commandToClient(%player.client, 'BottomPrint', "Puma Missile launched at" SPC detag(%target.getName(), 5, 1);
//     else
//          commandToClient(%player.client, 'BottomPrint', "Puma Missile launched!", 5, 1);

     %obj.pumaReady[%slot] = false;
     %obj.play3D(MechRocketFireSound);
     %obj.play3D(MechFire2Sound);
     %obj.unmountImage(%slot);
     %obj.pumaLaunchTimeout = true;
     %obj.numPumas--;

     commandToClient(%player.client, 'setAmmoHudCount', "Missiles:" SPC %obj.numPumas);
     schedule(30000, %obj, "resetPumaMissile", %obj, %slot);
     schedule( 2000, %obj, "resetPumaTimeout", %obj);	// (6000, %obj, "resetPumaTimeout", %obj); -soph
}

function resetPumaTimeout(%obj)
{
     %obj.pumaLaunchTimeout = false;
}

function resetPumaMissile(%obj, %slot)
{
     %obj.pumaReady[%slot] = true;
     %obj.numPumas++;

     if(isObject(%obj.getMountNodeObject(0)))
          commandToClient(%obj.getMountNodeObject(0).client, 'setAmmoHudCount', "Missiles:" SPC %obj.numPumas);

     if(%slot == 6)
     {
           %obj.mountImage(PumaMissileA, 6);
           %obj.play3D(MissileReloadSound);
     }
     else if(%slot == 7)
     {
           %obj.mountImage(PumaMissileB, 7);
           %obj.play3D(MissileReloadSound);
     }
}

function PumaEngineLoop(%obj)
{
   if(isObject(%obj))
   {
      %speed = vectorLen(%obj.getVelocity());

      if(%speed > 12)
      {
         %obj.setImageTrigger(6, true);
         %obj.setImageTrigger(7, true);
      }
      else
      {
         %obj.setImageTrigger(6, false);
         %obj.setImageTrigger(7, false);
      }

      schedule(50, %obj, PumaEngineLoop, %obj);
   }
}

function Pumas::applyMod(%vData, %obj, %moduleID, %vType)
{
   %obj.mountImage(SeekingMissileParam, 0, false);
   %obj.mountImage(PumaMissileA, 6);
   %obj.mountImage(PumaMissileB, 7);

   %obj.pumaReady[6] = true;
   %obj.pumaReady[7] = true;
   %obj.numPumas = 2;
   
   PumaEngineLoop(%obj);
}

function Pumas::onTrigger(%vData, %obj, %state)
{
     %obj.setImageTrigger(2, false);
     %obj.setImageTrigger(3, false);
     %obj.setImageTrigger(4, false);
     %obj.setImageTrigger(5, false);

     if(%state)
          handlePumaLaunch(%vData, %obj);
}

//----------------------------------------------------------------------------

// Todo: adjust to front
datablock ShapeBaseImageData(ScoutMitziPairImage)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_grenade_launcher.dts";
   item      = Chaingun;
   projectile = KiloBlasterBall;
   projectileType = LinearFlareProjectile;
   mountPoint = 10;
   offset = "1 1 0.7";	// offset = "2.0 -1.0 -0.25"; -soph
//   rotation = "0 1 0 305";
   usesEnergy = true;
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 30;
   fireEnergy = 30;
   fireTimeout = 750;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSequence[4]         = "recoil";
   stateSound[4]            = MBLFireSound;
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 0.75;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(ScoutMitziImage) : ScoutMitziPairImage
{
   offset = "-1 1 0.7";	// offset = "-2.0 -1.0 -0.25"; -soph
//   rotation = "0 1 0 -305";
   stateSequence[2]         = "Deploy";
   stateScript[3]           = "onTriggerDown";
   stateScript[5]           = "onTriggerUp";
   stateScript[6]           = "onTriggerUp";
};

function ScoutMitziImage::onFire(%data,%obj,%slot)
{
   if(%data.projectile $= "")
     %data.projectile = KiloBlasterBall;

   Parent::onFire(%data,%obj,%slot);
//   %obj.nextWeaponFire = 5;
//   schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function ScoutMitziPairImage::onFire(%data,%obj,%slot)
{
   if(%data.projectile $= "")
     %data.projectile = KiloBlasterBall;

   Parent::onFire(%data,%obj,%slot);
//   %obj.nextWeaponFire = 4;
//   schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function ShrikeMSTB::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.mountImage(ScoutMitziImage, 2);
     %obj.mountImage(ScoutMitziPairImage, 3);
}

function ShrikeMSTB::onTrigger(%vData, %obj, %state)
{
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(2, false);
               %obj.setImageTrigger(3, false);

          case 1:
               %obj.fireWeapon = true;

               %obj.setImageTrigger(2, true);
               %obj.setImageTrigger(3, true);

               %obj.setImageTrigger(4, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(5, false);	// +[/soph]
     }
}

//----------------------------------------------------------------------------
datablock ShapeBaseImageData(ScoutPhaserPairImage)
{
   className = WeaponImage;
   shapeFile = "turret_mortar_large.dts";
   item      = Chaingun;
//   ammo = PCRAmmo;	// primary uses energy -soph
   emap = true;

   projectile = PCRBolt;
   projectileType = TracerProjectile;
   projectileSpread = 6 / 998;

   mountPoint = 10;
   offset = "1 1 0.7";	// offset = "-4.25 -2.25 -1.75"; -soph //".95 0.6 -0.1"; 
   rotation = "0 1 0 125";

   usesEnergy = true;	// false; -soph
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 15;	// 30; -soph
   fireEnergy = 15;	// 30; -soph
   fireTimeout = 500;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = PCRFireSound;
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 0.5;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateScript[7]              = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(ScoutPhaserImage) : ScoutPhaserPairImage
{
   minEnergy = 30;	// 60; -soph
   fireEnergy = 15;	// 30; -soph
   fireTimeout = 3000;
   
   offset = "-1 1 0.7";	// offset = "4.25 -2.25 -1.75"; -soph //"-.95 0.6 -0.1"; 
   rotation = "0 1 0 -135";
   stateScript[3]           = "onTriggerDown";
   stateScript[5]           = "onTriggerUp";
   stateScript[6]           = "onTriggerUp";
};

function ScoutPhaserImage::onFire(%data,%obj,%slot)
{
   if(%data.projectile $= "")
     %data.projectile = PCRBolt;
     
   Parent::onFire(%data,%obj,%slot);
   
//   %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);	// no longer uses ammo -soph
}

function ScoutPhaserPairImage::onFire(%data,%obj,%slot)
{
   if(%data.projectile $= "")
     %data.projectile = PCRBolt;
     
   Parent::onFire(%data,%obj,%slot);

//   %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);	// no longer uses ammo -soph
}

function ScoutPhaserImage::onDryFire(%data, %obj, %slot)
{
//     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);	// no longer uses ammo -soph
}

function ScoutPhaserPairImage::onDryFire(%data, %obj, %slot)
{
//     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);	// no longer uses ammo -soph
}

function ShrikeHeavyPhaser::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.mountImage(ScoutPhaserImage, 2);	// %obj.mountImage(ScoutPhaserImage, 4); -soph
     %obj.mountImage(ScoutPhaserPairImage, 3);
}

function ShrikeHeavyPhaser::onTrigger(%vData, %obj, %state)
{
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(2, false);	// %obj.setImageTrigger(4, false); -soph               
               %obj.setImageTrigger(3, false);	// %obj.setImageTrigger(5, false); -soph     

          case 1:
               %obj.fireWeapon = true;

               %obj.setImageTrigger(2, true);	// %obj.setImageTrigger(4, true); -soph     
               %obj.setImageTrigger(3, true);	// %obj.setImageTrigger(5, true); -soph  

               %obj.setImageTrigger(4, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(5, false);	// +[/soph]   
     }
}

//----------------------------------------------------------------------------
datablock ShapeBaseImageData(ShrikePlasmaPairImage)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_grenade_launcher.dts";	// "TR2weapon_disc.dts"; -soph
   item      = Chaingun;
   projectile = BasicGrenade;				// MFDiscProjectile; -soph
   projectileType = GrenadeProjectile;			// LinearProjectile; -soph
   projectileSpread = 25 / 1000;			// +soph

   mountPoint = 10;
   offset = "-4.25 -2.5 -1.9";
   rotation = "0 1 0 125";
   
   ammo = DiscAmmo;

   usesEnergy = false;
   useMountEnergy = true;
   // DAVEG -- balancing numbers below!
   minEnergy = 45;
   fireEnergy = 45;
   fireTimeout = 1500;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Deploy";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateSound[4]            = MBLFireSound;		// DiscFireSound; -soph
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 1.5;
   stateTransitionOnTimeout[4]   = "checkState";
   stateSequence[4]         = "Fire";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateScript[7]              = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

datablock ShapeBaseImageData(ShrikePlasmaImage) : ShrikePlasmaPairImage
{
//   minEnergy = 90;
//   fireEnergy = 45;
//   fireTimeout = 1500;
   
   offset = "4.25 -2.5 -1.6";
   rotation = "0 1 0 -125";
   stateScript[3]           = "onTriggerDown";
   stateScript[5]           = "onTriggerUp";
   stateScript[6]           = "onTriggerUp";
};

function ShrikePlasmaImage::onFire(%data,%obj,%slot)
{
//   if(%data.projectile $= "")			// -[soph]
//     %data.projectile = MFDiscProjectile;	// -
     						// -
//   Parent::onFire(%data,%obj,%slot);		// -[/soph]

   for( %i = 2 ; %i < 5 ; %i++ )		// +[soph]
   {						// +
      if( %obj.getInventory( %data.ammo ) < 1 )	// +
         break ;				// +
      						// +
      %vector = %obj.getMuzzleVector( %slot ) ;	// +
      %x = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
      %y = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
      %z = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
      %mat = MatrixCreateFromEuler( %x @ " " @ %y @ " " @ %z ) ;
      %vector = MatrixMulVector( %mat , %vector ) ;
						// +
      %p = new (%data.projectileType)() {	// +
         dataBlock        = %data.projectile ;	// +
         initialDirection = vectorScale( %vector , %i / 5 ) ;
         initialPosition  = %obj.getMuzzlePoint( %slot ) ;
         sourceObject     = %obj ;		// +
         sourceSlot       = %slot ;		// +
         bkSourceObject   = %obj ;		// +
      };					// +
      MissionCleanup.add( %p ) ;		// +
      %p.damageMod = 1.0 ;			// +
      %obj.decInventory( %data.ammo , 1 ) ;	// +
   }						// +[/soph]


   %obj.secondaryAmmo = %data.ammo;	// screw this, just gonna hack it -soph
   %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %obj.secondaryAmmo);	// %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo); -soph
}

function ShrikePlasmaPairImage::onFire(%data,%obj,%slot)
{
//   if(%data.projectile $= "")			// -[soph]
//     %data.projectile = MFDiscProjectile;	// -
     						// -
//   Parent::onFire(%data,%obj,%slot);		// -[/soph]

   for( %i = 2 ; %i < 5 ; %i++ )		// +[soph]
   {						// +
      if( %obj.getInventory( %data.ammo ) < 1 )	// +
         break ;				// +
      						// +
      %vector = %obj.getMuzzleVector( %slot ) ;	// +
      %x = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
      %y = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
      %z = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %data.projectileSpread ;
      %mat = MatrixCreateFromEuler( %x @ " " @ %y @ " " @ %z ) ;
      %vector = MatrixMulVector( %mat , %vector ) ;
						// +
      %p = new (%data.projectileType)() {	// +
         dataBlock        = %data.projectile ;	// +
         initialDirection = vectorScale( %vector , %i / 5 ) ;
         initialPosition  = %obj.getMuzzlePoint( %slot ) ;
         sourceObject     = %obj ;		// +
         sourceSlot       = %slot ;		// +
         bkSourceObject   = %obj ;		// +
      };					// +
      MissionCleanup.add( %p ) ;		// +
      %p.damageMod = 1.0 ;			// +
      %obj.decInventory( %data.ammo , 1 ) ;	// +
   }						// +[/soph]

   %obj.secondaryAmmo = %data.ammo;	// screw this, just gonna hack it -soph
   %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %obj.secondaryAmmo);	// %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo); -soph
}

function ShrikePlasmaImage::onDryFire(%data, %obj, %slot)
{
     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);
}

function ShrikePlasmaPairImage::onDryFire(%data, %obj, %slot)
{
     %obj.getDatablock().reloadAmmo(%obj, %obj.getMountNodeObject(0).client, %data.ammo, 8, 0);
}

function ShrikeHeavyPlasma::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.mountImage(ShrikePlasmaImage, 4);
     %obj.mountImage(ShrikePlasmaPairImage, 5);
}

function ShrikeHeavyPlasma::onTrigger(%vData, %obj, %state)
{
     switch(%state)
     {
          case 0:
               %obj.fireWeapon = false;

               %obj.setImageTrigger(4, false);               
               %obj.setImageTrigger(5, false);

          case 1:
               %obj.fireWeapon = true;

               %obj.setImageTrigger(4, true);
               %obj.setImageTrigger(5, true);

               %obj.setImageTrigger(2, false);	// for lack of a more elegant fix... +[soph]
               %obj.setImageTrigger(3, false);	// +[/soph]
     }
}

//----------------------------------------------------------------------------

function TankAABarrel::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.mountImage( AABarrelLarge , 2 ) ;	// +soph
    // %obj.turretObject.mountImage(TankAABarrel1, 2);		// -soph
    // %obj.turretObject.mountImage(TankAABarrel2, 3);		// -soph
}

//----------------------------------------------------------------------------

function TankCometCannon::applyMod(%vData, %obj, %moduleID, %vType)
{
     if(%vData.vehicleType == $VehicleMask::Thundersword)
          %obj.turretObject.mountImage(TSMortarTurretBarrel, 4);
     else
          %obj.turretObject.mountImage(AssaultMortarTurretBarrel, 4);
}

//----------------------------------------------------------------------------

function TankStarHammer::applyMod(%vData, %obj, %moduleID, %vType)
{
     if(%vData.vehicleType == $VehicleMask::Thundersword)
          %obj.turretObject.mountImage(TSStarHammerBarrel, 4);
     else
          %obj.turretObject.mountImage(TankStarHammerBarrel, 4);
}

//----------------------------------------------------------------------------

function TankPBW::applyMod(%vData, %obj, %moduleID, %vType)
{
     if(%vData.vehicleType == $VehicleMask::Thundersword)
          %obj.turretObject.mountImage(TSPBWBarrel, 4);	 
     else
          %obj.turretObject.mountImage(TankPBWBarrel, 4);
}

//----------------------------------------------------------------------------

function TankStarburst::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.flakEnabled = true;

     if(%vData.vehicleType == $VehicleMask::Thundersword)
          %obj.turretObject.mountImage(TSPumaLauncherBarrel, 4);     
     else     
          %obj.turretObject.mountImage(TankPumaLauncherBarrel, 4); 
}

//----------------------------------------------------------------------------

function TankFlakMortar::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.flakEnabled = true;

     if(%vData.vehicleType == $VehicleMask::Thundersword)
          %obj.turretObject.mountImage(TSFlakMortarBarrel, 4);     
     else     
          %obj.turretObject.mountImage(TankFlakMortarBarrel, 4); 
}

//----------------------------------------------------------------------------

function TankTurboChaingun::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.mountImage(ChaingunBarrelLarge, 2);
}

//----------------------------------------------------------------------------

function TankFlamethrower::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.mountImage(FlameBarrelLarge, 2);
}

//----------------------------------------------------------------------------

function TankMissileTurret::applyMod(%vData, %obj, %moduleID, %vType)
{
//     %obj.turretObject.mountImage(MechSeekingParam, 0);
     %obj.turretObject.mountImage(MissileBarrelLarge, 0); 
//     %obj.turretObject.mountImage(MissileBarrelLarge, 2);
}

//----------------------------------------------------------------------------

function TankEnergyTurret::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.mountImage(PhaserBarrelLarge, 2);
}

//----------------------------------------------------------------------------

function BomberBomb::applyMod(%vData, %obj, %moduleID, %vType)
{
     %obj.turretObject.mountImage(BomberBombImage, 4);		// moved here from onadd +soph
     %obj.turretObject.mountImage(BomberBombPairImage, 5);	// +soph
     %obj.applyBombReticle = true;
}

//----------------------------------------------------------------------------

function TSwordBellyKM::applyMod(%vData, %obj, %moduleID, %vType)	// +[soph]
{									// nothing doing here...
}									// +[/soph]

//----------------------------------------------------------------------------

function TSwordBellyATC::applyMod(%vData, %obj, %moduleID, %vType)	// +[soph]
{
     %obj.turretObject.unMountImage( 2 );
    // %obj.turretObject.unMountImage( 3 );
     %obj.turretObject.mountImage(BomberAutoCannon, 2);
    // %obj.turretObject.mountImage(BomberAutoCannonPair, 3);
   %obj.turretObject.weapon[1, Display] = true;
   %turret.turretObject.weapon[1, Name] = "Bombardment Autocannons";
   %turret.turretObject.weapon[1, Description] = "High rate-of-fire saturation weapons";
}									// +[/soph]

//----------------------------------------------------------------------------
function TSwordREmpty::applyMod(%vData, %obj, %moduleID, %vType)
{

}

//----------------------------------------------------------------------------

function TSwordPilotCannon::applyMod( %vData, %obj, %moduleID, %vType )	// +[soph]
{									// +
   %obj.turretObject.unMountImage( 4 ) ;				// +
   %obj.turretObject.unMountImage( 5 ) ;				// +
   %obj.mountImage( BomberPilotCannon , 4 ) ;				// +
   %obj.mountImage( BomberPilotCannonPair , 5 ) ;			// +
   %obj.turretObject.weapon[2, Name] = "[No Primary Mounted]" ;		// +
   %obj.turretObject.weapon[2, Description] = " " ;			// +
}									// +
									// +
function TSwordBellyATC::applyMod( %vData, %obj, %moduleID, %vType )	// +
{									// +
   %obj.turretObject.unMountImage( 2 );					// +
   %obj.turretObject.unMountImage( 3 );					// +
   %obj.turretObject.mountImage( BomberAutoCannon, 2);			// +
   %obj.turretObject.mountImage( BomberAutoCannonPair, 3);		// +
   %obj.turretObject.weapon[1, Name] = "Dual Autocannon";		// +
   %obj.turretObject.weapon[1, Description] = "Explosive hail on demand.";
}									// +[/soph]

//----------------------------------------------------------------------------
function TSwordRTurret::applyMod(%vData, %obj, %moduleID, %vType)
{
   %turret = TurretData::create(BomberFlyerTailTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 2);
   %turret.mountImage(ELFBarrelLarge, 0);	// %turret.mountImage(AABarrelLarge, 0); -soph
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
  // %turret.setAutoFire(false);		// -soph
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.schedule( 34 , "setHeat" , 1.0 );	// %turret.setHeat(0.0); -soph
   
   %obj.moduleState = true;			// +soph
   %obj.rearTurret = %turret;
}

function TSwordRTurret::onActivate( %vehicleData, %vehicle, %obj )
{
   // shush
}

function TSwordRTurret::onDeactivate( %vehicleData, %vehicle, %obj )
{
   // shush
}

//----------------------------------------------------------------------------
function HavocTGTurret::applyMod(%vData, %obj, %moduleID, %vType)	// +[soph]
{
   %turret = TurretData::create( BomberFlyerTailTurret ) ;
   %turret.selectedWeapon = 1 ;
   MissionCleanup.add( %turret ) ;
   %turret.team = %obj.teamBought ;
   %turret.setSelfPowered() ;
   %obj.mountObject( %turret , 1 ) ;
   %turret.mountImage( MortarBarrelLarge , 0 ) ;
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate ) ;
   setTargetSensorGroup( %turret.getTarget() , %turret.team) ;
   setTargetNeverVisMask( %turret.getTarget() , 0xffffffff ) ;
   %turret.setCloaked( true ) ;
   %turret.schedule( 5500 , "setCloaked" , false ) ;
   %turret.schedule( 34 , "setHeat" , 1.0 ) ;	
   
   %obj.moduleState = true ;			
   %obj.rearTurret = %turret ;
}

function HavocTGTurret::onActivate( %vehicleData, %vehicle, %obj )
{
   // shush
}

function HavocTGTurret::onDeactivate( %vehicleData, %vehicle, %obj )
{
   // shush
}									// +[/soph]

//----------------------------------------------------------------------------
function TSwordRTurretM::applyMod(%vData, %obj, %moduleID, %vType)	// no longer used -soph
{
   %turret = TurretData::create(BomberFlyerTailTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 2);
   %turret.mountImage(MissileBarrelLarge, 0);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.setHeat(0.0);

   %obj.rearTurret = true;
}

//----------------------------------------------------------------------------
function TSwordRTurretC::applyMod( %vData , %obj , %moduleID , %vType )	// no longer used -soph
{
   %turret = TurretData::create(BomberFlyerTailTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 2);
   %turret.mountImage(MortarBarrelLarge, 0);
   %turret.setCapacitorRechargeRate(%turret.getDataBlock().capacitorRechargeRate) ;
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.setHeat(0.0);

   %obj.rearTurret = true;
}

//----------------------------------------------------------------------------
function TSwordRTurretP::applyMod(%vData, %obj, %moduleID, %vType)	// no longer used -soph
{
   %turret = TurretData::create(BomberFlyerTailTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 2);
   %turret.mountImage(PhaserBarrelLarge, 0);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.setHeat(0.0);

   %obj.rearTurret = true;
}
//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }