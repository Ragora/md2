// Projectiles
datablock SeekerProjectileData(SRM4Missile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 0.45 ;		// 0.4; -soph
   damageRadius        = 8.0 ;		// 10; -soph
   radiusDamageType    = $DamageType::MechMissile;
   kickBackStrength    = 500;

   explosion           = "HandGrenadeExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = MagMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 3000;
   muzzleVelocity      = 20.0;
   maxVelocity         = 200.0;
   turningSpeed        = 150.0;
   acceleration        = 100.0;

   proximityRadius     = 2;

   terrainAvoidanceSpeed         = 360;
   terrainScanAhead              = 50;
   terrainHeightFail             = 24;
   terrainAvoidanceRadius        = 200;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 50;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock SeekerProjectileData(LRM5Missile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 0.325 ;	// 0.2; -soph
   damageRadius        = 4.5 ;		// 5.0; -soph
   radiusDamageType    = $DamageType::MechMissile;
   kickBackStrength    = 250;

   flareDistance = 200;
   flareAngle    = 30;
   minSeekHeat   = 0.6;

   explosion           = "MissileExplosion";
   velInheritFactor    = 0.2;

   splash              = MissileSplash;
   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter; 
   puffEmitter         = MissilePuffEmitter;

   lifetimeMS          = 20000;
   muzzleVelocity      = 20.0;
   turningSpeed        = 850.0;
   acceleration        = 75.0;
   maxVelocity         = 150.0;
   
   proximityRadius     = 4;

   terrainAvoidanceSpeed = 180;
   terrainScanAhead      = 60 ;		// = 25; -soph
   terrainHeightFail     = 24 ;		// = 12; -soph
   terrainAvoidanceRadius = 100;

   useFlechette = true;
   flechetteDelayMs = 50;
   casingDeb = FlechetteDebris;
};

datablock SniperProjectileData(MechPunchBeam)
{
   directDamage        = 0.6;	// = 0.4; -soph
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::MechLaser;

   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.29;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

function MechPunchBeam::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
  // %damageAmount = %projectile.damageFactor;					// -soph
   %damageAmount = %data.directDamage;						// +soph

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   %dist = vectorDist(%projectile.sourceObject.getWorldBoxCenter(), %position);	// +[soph]
   if( %dist > 150 )
      %modifier = 0.6 + 0.4 * ( ( 300 - %dist ) / 150 );			// dropoff from 60 to 36
   else										// +[/soph]
   %modifier = 1;


   if(%targetObject.getType() & $TypeMasks::PlayerObjectType)
        %targetObject.getOwnerClient().headShot = 0;   
//   {
//      BAOverdrivePulseUpdate(%targetObject);
//      %targetObject.startFade(500, 0, false);
//   }

   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
}

datablock SniperProjectileData(MechPulseBeam)
{
   directDamage        = 0.4;	// -soph
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::MechLaser;

   maxRifleRange       = 200;	// = 150; -soph
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.29;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

function MechPulseBeam::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
  // %damageAmount = %projectile.damageFactor;					// -soph
   %damageAmount = %data.directDamage;						// +soph

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   %dist = vectorDist(%projectile.sourceObject.getWorldBoxCenter(), %position);	// +[soph]
   if( %dist > 100 )
      %modifier = 0.6 + 0.4 * ( ( 200 - %dist ) / 100 );			// dropoff from 40 to 24
   else										// +[/soph]
   %modifier = 1;


   if(%targetObject.getType() & $TypeMasks::PlayerObjectType)
        %targetObject.getOwnerClient().headShot = 0;
//   {
//      BAOverdrivePulseUpdate(%targetObject);
//      %targetObject.startFade(500, 0, false);
//   }

   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
}

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
