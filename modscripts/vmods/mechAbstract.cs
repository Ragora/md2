// PatriotMechBase
//-----------------------------------------------------------------------------

function PatriotMechBase::checkShields(%data, %targetObject, %position, %amount, %damageType)
{
     return MechCalculateShields(%data, %targetObject, %position, %amount, %damageType);
}

function PatriotMechBase::processUse(%data, %obj, %idata)
{
     MechProcessUse(%data, %obj, %idata);
}

function PatriotMechBase::weaponTriggerToggle(%data, %obj, %state)
{
     MechProcessFireWeapons(%data, %obj, %state);
}

function PatriotMechBase::cycleWeapon(%data, %obj, %dir)
{
     MechCycleWeapon(%data, %obj, %dir);
}

function PatriotMechBase::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
     MechDamageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC);
}

function PatriotMechBase::onCollision(%this,%obj,%col,%forceVehicleNode)
{
     MechOnCollision(%this,%obj,%col,%forceVehicleNode);
}

function PatriotMechBase::onEnterLiquid(%data, %obj, %coverage, %type)
{
     MechEnterLiquid(%data, %obj, %coverage, %type);
}

function PatriotMechBase::onLeaveLiquid(%data, %obj, %type)
{
     MechExitLiquid(%data, %obj, %type);
}

function PatriotMechBase::tickLoop(%data, %obj)
{
     MechTick(%data, %obj);
}

function PatriotMechBase::onTrigger(%data, %player, %triggerNum, %val)
{
     MechOnTrigger(%data, %player, %triggerNum, %val);
}

function PatriotStatic::onCollision(%data, %obj, %col)
{
     MechHandleCollision(%data, %obj, %col);
}

function PatriotStatic::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
}

// PatriotMechJet
//-----------------------------------------------------------------------------

function PatriotMechJet::checkShields(%data, %targetObject, %position, %amount, %damageType)
{
     return MechCalculateShields(%data, %targetObject, %position, %amount, %damageType);
}

function PatriotMechJet::processUse(%data, %obj, %idata)
{
     MechProcessUse(%data, %obj, %idata);
}

function PatriotMechJet::weaponTriggerToggle(%data, %obj, %state)
{
     MechProcessFireWeapons(%data, %obj, %state);
}

function PatriotMechJet::cycleWeapon(%data, %obj, %dir)
{
     MechCycleWeapon(%data, %obj, %dir);
}

function PatriotMechJet::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
     MechDamageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC);
}

function PatriotMechJet::onCollision(%this,%obj,%col,%forceVehicleNode)
{
     MechOnCollision(%this,%obj,%col,%forceVehicleNode);
}

function PatriotMechJet::onEnterLiquid(%data, %obj, %coverage, %type)
{
     MechEnterLiquid(%data, %obj, %coverage, %type);
}

function PatriotMechJet::onLeaveLiquid(%data, %obj, %type)
{
     MechExitLiquid(%data, %obj, %type);
}

function PatriotMechJet::tickLoop(%data, %obj)
{
     MechTick(%data, %obj);
}

function PatriotMechJet::onTrigger(%data, %player, %triggerNum, %val)
{
     MechOnTrigger(%data, %player, %triggerNum, %val);
}

// EmancipatorMechBase
//-----------------------------------------------------------------------------

function EmancipatorMechBase::checkShields(%data, %targetObject, %position, %amount, %damageType)
{
     return MechCalculateShields(%data, %targetObject, %position, %amount, %damageType);
}

function EmancipatorMechBase::processUse(%data, %obj, %idata)
{
     MechProcessUse(%data, %obj, %idata);
}

function EmancipatorMechBase::weaponTriggerToggle(%data, %obj, %state)
{
     MechProcessFireWeapons(%data, %obj, %state);
}

function EmancipatorMechBase::cycleWeapon(%data, %obj, %dir)
{
     MechCycleWeapon(%data, %obj, %dir);
}

function EmancipatorMechBase::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
     MechDamageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC);
}

function EmancipatorMechBase::onCollision(%this,%obj,%col,%forceVehicleNode)
{
     MechOnCollision(%this,%obj,%col,%forceVehicleNode);
}

function EmancipatorMechBase::onEnterLiquid(%data, %obj, %coverage, %type)
{
     MechEnterLiquid(%data, %obj, %coverage, %type);
}

function EmancipatorMechBase::onLeaveLiquid(%data, %obj, %type)
{
     MechExitLiquid(%data, %obj, %type);
}

function EmancipatorMechBase::tickLoop(%data, %obj)
{
     MechTick(%data, %obj);
}

function EmancipatorMechBase::onTrigger(%data, %player, %triggerNum, %val)
{
     MechOnTrigger(%data, %player, %triggerNum, %val);
}

function EmancipatorStatic::onCollision(%data, %obj, %col)
{
     MechHandleCollision(%data, %obj, %col);
}

function EmancipatorStatic::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
}

// EmancipatorMechJet
//-----------------------------------------------------------------------------

function EmancipatorMechJet::checkShields(%data, %targetObject, %position, %amount, %damageType)
{
     return MechCalculateShields(%data, %targetObject, %position, %amount, %damageType);
}

function EmancipatorMechJet::processUse(%data, %obj, %idata)
{
     MechProcessUse(%data, %obj, %idata);
}

function EmancipatorMechJet::weaponTriggerToggle(%data, %obj, %state)
{
     MechProcessFireWeapons(%data, %obj, %state);
}

function EmancipatorMechJet::cycleWeapon(%data, %obj, %dir)
{
     MechCycleWeapon(%data, %obj, %dir);
}

function EmancipatorMechJet::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
     MechDamageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC);
}

function EmancipatorMechJet::onCollision(%this,%obj,%col,%forceVehicleNode)
{
     MechOnCollision(%this,%obj,%col,%forceVehicleNode);
}

function EmancipatorMechJet::onEnterLiquid(%data, %obj, %coverage, %type)
{
     MechEnterLiquid(%data, %obj, %coverage, %type);
}

function EmancipatorMechJet::onLeaveLiquid(%data, %obj, %type)
{
     MechExitLiquid(%data, %obj, %type);
}

function EmancipatorMechJet::tickLoop(%data, %obj)
{
     MechTick(%data, %obj);
}

function EmancipatorMechJet::onTrigger(%data, %player, %triggerNum, %val)
{
     MechOnTrigger(%data, %player, %triggerNum, %val);
}

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
