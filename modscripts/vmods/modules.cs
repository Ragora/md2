function SSToggleTargetingComputer(%vehicle, %turret)
{
//     echo("SSTurret call");
     %vehicle.getDatablock().swapTurretControl(%vehicle, %turret.getControllingClient().player);
}

// Depreciated - kept for reference
function xSSToggleTargetingComputer(%vehicle, %turret)
{
     %client = %turret.getControllingClient();

     if(!%turret.tc_status)
     {
          %turret.targetingComputer = true;
          %turret.tc_status = true;
          %turret.play3D(PBWSwitchSound);
//          %turret.mountImage(AIAimingSeekingBarrel, 0, true);
          commandToClient(%client, 'CenterPrint', "Targeting Computer online.", 3, 1);	// commandToClient(%client, 'BottomPrint', "Targeting Computer online.", 3, 1); -soph
     }
     else
     {
          %turret.targetingComputer = false;
          %turret.tc_status = false;
          %turret.play3D(SynomiumOff);
//          %turret.mountImage(AIAimingTurretBarrel, 0, true);
          commandToClient(%client, 'CenterPrint', "Targeting Computer offline.", 3, 1);	// commandToClient(%client, 'BottomPrint', "Targeting Computer offline.", 3, 1); -soph
     }
}

// Tank Weapons
exec("scripts/modscripts/vehaugs/TankStarHammer.cs");
exec("scripts/modscripts/vehaugs/TankCometCannon.cs");
exec("scripts/modscripts/vehaugs/TankFlakMortar.cs");
exec("scripts/modscripts/vehaugs/TankPBW.cs");
exec("scripts/modscripts/vehaugs/TankPumaLauncher.cs");

datablock ShockLanceProjectileData(BlueShift)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 0.4;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shieldenvmap";
   texture[1] = "special/shieldmap";
   texture[2] = "special/shieldenvmap";
   texture[3] = "special/shieldmap";
};

//------------------------------------------------------------------------------

function NoneModule::applyMod(%vData, %obj, %moduleID)
{
     %obj.moduleInstalled = false;
}

function SynMod::applyMod(%vData, %obj, %moduleID)
{
     %obj.isSyn = true;
}

function SubspaceMod::applyMod(%vData, %obj, %moduleID)
{
    // %obj.rechargeBonus -= 0.25;	// -soph
     vSubspaceModTick(%obj.getDatablock(), %obj);
}

function vSubspaceModTick(%data, %obj)
{
     if( isObject( %obj ) && !%obj.ignoreRepairThis )					// if(isObject(%obj)) -soph
     {
          if( %obj.getEnergyLevel() >= %data.maxEnergy && %obj.getDamageLevel() > 0 )	// +[soph]
          {										// +
               %obj.subspaceHealTick++;							// +
               if( %obj.subspaceHealTick >= 20 )					// +
               {									// +
                    zapVehicle( %obj , "HealGreen" ) ;					// +
                    if( %obj.turret )							// +
                         zapVehicle( %obj.turret , "HealGreen" ) ;			// +
                    %obj.subspaceHealTick = 10 ;					// +[/soph]
                    if( !%obj.ignoreRepairThis )
                       %obj.takeDamage(%data.maxDamage * -0.005 - 0.01 ) ;			// was * -0.008 - 0.006 for 1% per sec on shrike
                  // %obj.takeDamage(%data.maxDamage * -0.01); 				// -soph
               }
          }
          else										// +soph
               %obj.subspaceHealTick = 0;						// +soph
          schedule( 100, %obj, vSubspaceModTick, %data, %obj);				// schedule(5000, %obj, vSubspaceModTick, %data, %obj); -soph
     }
}

function DampenerMod::applyMod(%vData, %obj, %moduleID)
{
     %obj.inertialDampener = true;
}

function FlareMod::applyMod(%vData, %obj, %moduleID)
{
     %obj.numFlares = 5;				// failsafe =soph
     	switch$ (%vData.vehicleType)			// +[soph]
	{						// +
		case $VehicleMask::Wildcat :		// +
			%obj.numFlares = 4 ;		// + original default for all vehicles
		case $VehicleMask::Shrike :		// +
			%obj.numFlares = 5 ;		// + original default for all vehicles
		case $VehicleMask::Thundersword :	// +
			%obj.numFlares = 6 ;		// + increased capacity for the thundersword 
		case $VehicleMask::Nightshade :		// +
			%obj.numFlares = 5 ;		// + nightshade gets less, it only needs one pass
		case $VehicleMask::Havoc :		// +
			%obj.numFlares = 9 ;		// + even more for the havoc
	}						// +[/soph]
}


function FlareMod::onActivate(%vData, %obj, %player)
{
     %client = %player.client;
     
     if(%obj.numFlares < 1)
     {
          centerPrint(%client, "Out of flares.", 5, 1);				//bottomPrint(%client, "Out of flares.", 5, 1); -soph
          %obj.moduleState = false;          
          return;
     }
	%j = 4;									// +[soph]
	%z = -1 ;								// + failsafes
	%s = 1 ;								// +[/soph]
	switch$ (%vData.vehicleType)
	{
		case $VehicleMask::Wildcat :					// +[soph]
			%j = 2 ;						// +
			%z = 1.5 ;						// +
			%s = 1.5 ;						// +[/soph]
		case $VehicleMask::Shrike :
			%j = 4 ;						// + ye originale flaree counte
			%z = -2 ;						// +
			%s = 2 ;						// +
		case $VehicleMask::ThunderSword :	 
			%j = 6 ;						// more for this guy
			%z = -1.25 ;						// +
			%s = 2.5 ;						// +
		case $VehicleMask::Nightshade :
			%j = 5 ;						// nuker gets five, can't overdo it
			%z = -1.5 ;						// +
			%s = 2.25 ;						// +
		case $VehicleMask::Havoc :
			%j = 8 ;						// fatass gets eight, fireworks!
			%z = -1 ;						// +
			%s = 3 ;						// +
	}
     %position = %obj.getWorldBoxCenter() ;					// +soph
     for( %i = 0 ; %i < %j ; %i++ )						// for(%i = 0; %i < 4; %i++) -soph
     {
         %vector = vectorScale( vectorNormalize( getRandomT() SPC getRandomT() SPC %z ) , %s ) ;	// +soph
         %f = new FlareProjectile()
         {
                dataBlock        = FlareGrenadeProj;
                initialDirection = %vector ;					// getRandomT() SPC getRandomT() SPC "-2"; -soph
                initialPosition  = %position ;					// = %obj.getWorldBoxCenter(); -soph
                sourceObject     = %obj;
                sourceSlot       = 0;
         };
         FlareSet.add(%f);
         MissionCleanup.add(%f);
    //     %f.setVelocity(vectorScale(vectorNeg(%obj.getForwardVector()), 5));
         %f.schedule(10000, "delete");
    }
    
     %obj.numFlares--;
     
//     bottomPrint(%client, %obj.numFlares@" flares left", 5, 1);	// -soph
     %j = 0 ;
     %mounts = %obj.getDatablock().numMountPoints ;			// +[soph]
     for( %i = 0 ; %i < %mounts ; %i++ )				// +
     {									// + notify all passangers
          %sitter = %obj.getMountNodeObject( %i ) ;
          if( %sitter > 0 && isObject( %sitter ) )
               if( %sitter != %player )
               {
                    %found[ %j ] = %sitter ;
                    %j++ ;
               }
               else
                    %nodeName = findNodeName( %obj , %i ) ; 
     }
     messageClient( %client , 'MsgVehFlares', '\c2Flares deployed. %1 flare charges reamining.' , %obj.numFlares ) ;
     for( %i = 0 ; %i < %j ; %i++ )
          messageClient( %sitter.client, 'MsgVehFlares', '\c2%1 %2 deployed vehicle flares. %1 flare charges reamining.' , %nodeName , %client.name , %obj.numFlares ) ;
									// +[/soph]
     %obj.moduleState = false;
}

function FlareMod::onDeactivate(%vData, %obj, %player)
{

}

function BoosterModule::applyMod(%vData, %obj, %moduleID)
{
     %obj.boosterCharges = 12;
}

function BoosterModule::onActivate(%vData, %obj, %player)
{
     %client = %player.client;
     
     if(%obj.boosterCharges)
     {
          %nvel = %obj.getVelocity();
          %xvel = vectorLen(%nVel);
          %vel = mFloor(msToKPH(%xvel));

          if(%vel < 200)
          {
               %vec = vectorScale(%obj.getMuzzleVector(0), 35); // Adding to the current velocity..
               %force = %obj.getMass();
               %newVec = VectorScale(%vec, %force);
               %obj.applyImpulse(%obj.getTransform(), %newVec);
               %obj.boosterCharges--;
               %obj.play3D("MechRocketFireSound");
               centerPrint(%client, "Booster rocket fired! "@%obj.boosterCharges@" remaining.", 5, 1);	//bottomPrint(%client, "Booster rocket fired! "@%obj.boosterCharges@" remaining.", 5, 1); -soph
          }
          else
               centerPrint(%client, "Travelling too fast to engage booster rocket.", 5, 1);	//bottomPrint(%client, "Travelling too fast to engage booster rocket.", 5, 1); -soph         
     }
     else
          centerPrint(%client, "No more booster rocket charges.", 5, 1);	//bottomPrint(%client, "No more booster rocket charges.", 5, 1); -soph
     
     %obj.moduleState = false;     
}

function BoosterModule::onDeactivate(%vData, %obj, %player)
{

}

function InventoryModule::applyMod(%vData, %obj, %moduleID)
{
     %obj.mineInventory = true;
}

function vInvEffectStart(%obj)
{
     %obj.zap = new ShockLanceProjectile()
     {
          dataBlock        = "BlueShift"; 
          initialDirection = "0 0 0";
          initialPosition  = %obj.getWorldBoxCenter();
          sourceObject     = %obj;
          sourceSlot       = 0;
          targetId         = %obj;
     };
     MissionCleanup.add(%obj.zap);

     %obj.client.inInv = false;
     %obj.inStation = false;
     commandToClient(%obj.client,'setStationKeys', false);
        
     if(%obj.inv[%obj.lastWeapon])
          %obj.use(%obj.lastWeapon);
     else
          %obj.selectWeaponSlot(0);     
}

function TankPilotMitzi::applyMod( %vData , %obj , %moduleID )	// +[soph]
{
     %obj.pilotCannonMode = 1 ;
}

function TankPilotEMP::applyMod( %vData , %obj , %moduleID )
{
     %obj.pilotCannonMode = 2 ;
}

function TankPilotRAXX::applyMod( %vData , %obj , %moduleID )
{
     %obj.pilotCannonMode = 0 ;
}								// +[/soph]

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
