// Weapon Datablocks

datablock ShapeBaseImageData(MechIronLaser2Image)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts" ;	// "TR2weapon_shocklance.dts"; -soph
   offset = $MechHardPointPos["Iron", 2];
   rotation = "0 1 0 0";
//   emap = true;
   mountPoint = 0;

   usesEnergy = true;
   minEnergy = -1;
   fireEnergy = -1;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Deploy";
//   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
//   stateSpinThread[2]       = FullSpeed;
//   stateEjectShell[2]       = true;
//   stateSequenceRandomFlash[2] = true;
   stateSequence[2]         = "Fire";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";   
   stateTransitionOnTimeout[2] = "Fire";
};


datablock ShapeBaseImageData(MechIronAC2Image) : MechIronLaser2Image
{
   shapeFile = "turret_tank_barrelchain.dts";
   offset = $MechHardPointPos["Iron", 2];
   rotation = "0 1 0 0";

   casing              = AutoShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 3.0;
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Deploy";
   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSpinThread[2]       = FullSpeed;
   stateEjectShell[2]       = true;
   stateSequenceRandomFlash[2] = true;
   stateSequence[2]         = "Fire";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";   
   stateTransitionOnTimeout[2] = "Fire";
};

//------------------------------------------------------------------------------

datablock ShapeBaseImageData(MechIronLaser3Image) : MechIronLaser2Image
{
   offset = $MechHardPointPos["Iron", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronLaser4Image) : MechIronLaser2Image
{
   offset = $MechHardPointPos["Iron", 4];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronLaser5Image) : MechIronLaser2Image
{
   offset = $MechHardPointPos["Iron", 5];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronLaser6Image) : MechIronLaser2Image
{
   offset = $MechHardPointPos["Iron", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronLaser7Image) : MechIronLaser2Image
{
   offset = $MechHardPointPos["Iron", 7];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronCannon2Image) : MechIronLaser2Image
{
   shapeFile = "TR2weapon_mortar.dts";						// shapeFile = "turret_tank_barrelmortar.dts"; -soph
   offset = $MechHardPointPos["Iron", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronCannon3Image) : MechIronCannon2Image
{
   shapeFile = "TR2weapon_mortar.dts";						// +soph
   offset = $MechHardPointPos["Iron", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronCannon4Image) : MechIronCannon2Image
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $MechHardPointPos["Iron", 4];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronCannon5Image) : MechIronCannon2Image
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $MechHardPointPos["Iron", 5];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronCannon6Image) : MechIronCannon2Image
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $MechHardPointPos["Iron", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronCannon7Image) : MechIronCannon2Image
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $MechHardPointPos["Iron", 7];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronAC3Image) : MechIronAC2Image
{
   offset = $MechHardPointPos["Iron", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronAC4Image) : MechIronAC2Image
{
   offset = $MechHardPointPos["Iron", 4];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronAC5Image) : MechIronAC2Image
{
   offset = $MechHardPointPos["Iron", 5];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronAC6Image) : MechIronAC2Image
{
   offset = $MechHardPointPos["Iron", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronAC7Image) : MechIronAC2Image
{
   offset = $MechHardPointPos["Iron", 7];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronPlasma2Image) : MechIronLaser2Image
{
   shapeFile = "turret_fusion_large.dts";
   offset = $MechHardPointPos["Iron", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronPlasma3Image) : MechIronPlasma2Image
{
   offset = $MechHardPointPos["Iron", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronPlasma4Image) : MechIronPlasma2Image
{
   offset = $MechHardPointPos["Iron", 4];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronPlasma5Image) : MechIronPlasma2Image
{
   offset = $MechHardPointPos["Iron", 5];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronPlasma6Image) : MechIronPlasma2Image
{
   offset = $MechHardPointPos["Iron", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronPlasma7Image) : MechIronPlasma2Image
{
   offset = $MechHardPointPos["Iron", 7];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(MechIronMissile2Image) : MechIronLaser2Image
{
   shapeFile = "stackable1m.dts";
   offset = $MechHardPointPos["Iron", 2];
   rotation = "0 0 1 90";
};

datablock ShapeBaseImageData(MechIronMissile3Image) : MechIronMissile2Image
{
   shapeFile = "stackable1m.dts";
   offset = $MechHardPointPos["Iron", 3];
   rotation = "0 0 1 90";
};

datablock ShapeBaseImageData(MechIronMissile6Image) : MechIronMissile2Image
{
   shapeFile = "stackable1m.dts";
   offset = $MechHardPointPos["Iron", 6];
   rotation = "0 0 1 90";
};

datablock ShapeBaseImageData(MechIronMissile7Image) : MechIronMissile2Image
{
   shapeFile = "stackable1m.dts";
   offset = $MechHardPointPos["Iron", 7];
   rotation = "0 0 1 90";
};

datablock ShapeBaseImageData(MechIronHeavy2Image) : MechIronLaser2Image		// +[soph]
{
   shapeFile = "turret_tank_barrelmortar.dts" ;
   offset = $MechHardPointPos[ "Iron" , 2 ] ;
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(MechIronHeavy3Image) : MechIronHeavy2Image
{
   offset = $MechHardPointPos[ "Iron" , 3 ] ;
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(MechIronHeavy6Image) : MechIronHeavy2Image
{
   offset = $MechHardPointPos[ "Iron" , 6 ] ;
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(MechIronHeavy7Image) : MechIronHeavy2Image
{
   offset = $MechHardPointPos[ "Iron" , 7 ] ;
   rotation = "0 1 0 0" ;
};

datablock ShapeBaseImageData(MechIronLtMissile2Image) : MechIronLaser2Image
{
   shapeFile = "stackable2m.dts" ;
   offset = $MechHardPointPos[ "Iron" , 2 ] ;
   rotation = "1 0 0 90" ;
};

datablock ShapeBaseImageData(MechIronLtMissile3Image) : MechIronLtMissile2Image
{
   offset = $MechHardPointPos[ "Iron" , 3 ] ;
   rotation = "1 0 0 90" ;
};

datablock ShapeBaseImageData(MechIronLtMissile6Image) : MechIronLtMissile2Image
{
   offset = $MechHardPointPos[ "Iron" , 6 ] ;
   rotation = "1 0 0 90" ;
};

datablock ShapeBaseImageData(MechIronLtMissile7Image) : MechIronLtMissile2Image
{
   offset = $MechHardPointPos[ "Iron" , 7 ] ;
   rotation = "1 0 0 90" ;
};										// +[/soph]

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
