datablock TSShapeConstructor(TR2HeavyMaleDts)
{
   baseShape = "TR2heavy_male.dts";
   sequence0 = "TR2heavy_male_root.dsq root";
   sequence1 = "TR2heavy_male_forward.dsq run";
   sequence2 = "TR2heavy_male_back.dsq back";
   sequence3 = "TR2heavy_male_side.dsq side";
   sequence4 = "heavy_male_lookde.dsq look";
   sequence5 = "heavy_male_head.dsq head";
   sequence6 = "TR2heavy_male_fall.dsq fall";
   sequence7 = "TR2heavy_male_jet.dsq jet";
   sequence8 = "TR2heavy_male_land.dsq land";
   sequence9 = "TR2heavy_male_jump.dsq jump";
   sequence10 = "heavy_male_recoilde.dsq light_recoil";
   sequence11 = "heavy_male_idlepda.dsq pda";
   sequence12 = "heavy_male_headside.dsq headside";
   sequence13 = "heavy_male_lookms.dsq lookms";
   sequence14 = "TR2heavy_male_diehead.dsq death1";
   sequence15 = "TR2heavy_male_diechest.dsq death2";
   sequence16 = "TR2heavy_male_dieback.dsq death3";
   sequence17 = "TR2heavy_male_diesidelf.dsq death4";
   sequence18 = "TR2heavy_male_diesidert.dsq death5";
   sequence19 = "TR2heavy_male_dieforward.dsq death6";      // heavy_male_dieleglf
   sequence20 = "TR2heavy_male_diechest.dsq death7";        // heavy_male_dielegrt
   sequence21 = "TR2heavy_male_dieslump.dsq death8";
   sequence22 = "TR2heavy_male_dieforward.dsq death9";      // heavy_male_dieknees
   sequence23 = "TR2heavy_male_dieforward.dsq death10";
   sequence24 = "TR2heavy_male_diespin.dsq death11";
   sequence25 = "TR2heavy_male_celsalute.dsq cel1";
   sequence26 = "TR2heavy_male_celwave.dsq cel2";
   sequence27 = "TR2heavy_male_tauntbest.dsq cel3";
   sequence28 = "TR2heavy_male_tauntimp.dsq cel4";
   sequence29 = "TR2heavy_male_celdance.dsq cel5";
   sequence30 = "TR2heavy_male_celflex.dsq cel6";
   sequence31 = "TR2heavy_male_celtaunt.dsq cel7";
   sequence32 = "TR2heavy_male_celjump.dsq cel8";
   sequence33 = "TR2heavy_male_ski.dsq ski";
   sequence34 = "TR2heavy_male_standjump.dsq standjump";
   sequence35 = "heavy_male_looknw.dsq looknw";
};

datablock SimDataBlock(MechDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Blaster] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Blaster] = $UDP::UniversalDamage;
   damageScale[$DamageType::Bullet] = 1.0;
   shieldDamageScale[$DamageType::Bullet] = 1.0;
   damageScale[$DamageType::ShockLance] = 1.0;
   shieldDamageScale[$DamageType::ShockLance] = 1.0;
   damageScale[$DamageType::OutdoorDepTurret] = 1;
   shieldDamageScale[$DamageType::OutdoorDepTurret] = 1.0;
   damageScale[$DamageType::SentryTurret] = 1;
   shieldDamageScale[$DamageType::SentryTurret] = 1.0;
   damageScale[$DamageType::Plasma] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Plasma] = $UDP::UniversalDamage;
   damageScale[$DamageType::BomberBombs] = 1.0;
   shieldDamageScale[$DamageType::BomberBombs] = 1.0;
   damageScale[$DamageType::TankChaingun] = 1.0;
   shieldDamageScale[$DamageType::TankChaingun] = 1.0;
   damageScale[$DamageType::TankMortar] = 1;
   shieldDamageScale[$DamageType::TankMortar] = 1;
   damageScale[$DamageType::MissileTurret] = 1;
   shieldDamageScale[$DamageType::MissileTurret] = 1.0;
   damageScale[$DamageType::MortarTurret] = 1;
   shieldDamageScale[$DamageType::MortarTurret] = 1;
   damageScale[$DamageType::PlasmaTurret] = 1;
   shieldDamageScale[$DamageType::PlasmaTurret] = 1.0;
   damageScale[$DamageType::Impact] = 1;
   shieldDamageScale[$DamageType::Impact] = 1;
   damageScale[$DamageType::Ground] = 1.0;
   shieldDamageScale[$DamageType::Ground] = 1.0;
   damageScale[$DamageType::Lightning] = 8 ;		// = 10; -soph
   shieldDamageScale[$DamageType::Lightning] = 20 ;	// = 10; -soph

   damageScale[$DamageType::BurnLoop] = 1.0;		// = 0 ; -soph
   shieldDamageScale[$DamageType::BurnLoop] = 1.0;	// = 0 ; -soph
   damageScale[$DamageType::PoisonLoop] = 0;
   shieldDamageScale[$DamageType::PoisonLoop] = 0;
   damageScale[$DamageType::AutoCannon] = 1.0;
   shieldDamageScale[$DamageType::AutoCannon] = 1.0;
   damageScale[$DamageType::Poison] = 0;
   shieldDamageScale[$DamageType::Poison] = 0;
   damageScale[$DamageType::Burn] = 1.0;		// = 0 ; -soph
   shieldDamageScale[$DamageType::Burn] = 1.0;		// = 0 ; -soph
   damageScale[$DamageType::Annihalator] = 1;
   shieldDamageScale[$DamageType::Annihalator] = 1.0;
   damageScale[$DamageType::PlasmaCannon] = 1;
   shieldDamageScale[$DamageType::PlasmaCannon] = 1.0;
   damageScale[$DamageType::Sniper] = 1.0;
   shieldDamageScale[$DamageType::Sniper] = 1;
   damageScale[$DamageType::Phaser] = 1;
   shieldDamageScale[$DamageType::Phaser] = 1;
   damageScale[$DamageType::PBW] = 1;
   shieldDamageScale[$DamageType::PBW] = 1;
   damageScale[$DamageType::MB] = 1;
   shieldDamageScale[$DamageType::MB] = 1;
   damageScale[$DamageType::RAXX] = 1;
   shieldDamageScale[$DamageType::RAXX] = 1;
   damageScale[$DamageType::Pulse] = $UDP::UniversalDamage;
   shieldDamageScale[$DamageType::Pulse] = $UDP::UniversalDamage;
   damageScale[$DamageType::Ripper] = 1;
   shieldDamageScale[$DamageType::Ripper] = 1;
};

datablock StaticShapeData(EmancipatorStatic) : MechDamageProfile
{
   armorMask = $MechMask::Emancipator;
   shapeFile = "TR2heavy_male.dts";   // TR2HeavyMale
   maxDamage = 5.5;
   destroyedLevel = 5.5;
   disabledLevel = 5.5;

   isTacticalMech = true;
   deployedObject = true;
   emap = true;
   targetNameTag = 'Emancipator';
   targetNameTag = 'EAX Mech';
   explosion = AnExplosion;
   heatSignature = 0;
};

datablock PlayerData(EmancipatorMechBase) : MechDamageProfile
{
   emap = true;

   isTacticalMech = true;
   armorMask = $MechMask::Emancipator;
   
   className = Armor;
   shapeFile = "TR2heavy_male.dts";
   cameraMaxDist = 12;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = BADebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_playertriangle.png";
   hudImageNameEnemy[0] = "gui/hud_playertriangle_enemy.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 19110 ;									// 1911; -soph
   drag = 0.5;
   maxdrag = 0.6;
   density = 10;
   maxDamage = 44.0;
   maxEnergy = $MechReactorSize[ $MechMask::Emancipator ] ;				// 150; -soph
   repairRate = 0.0011;
   energyPerDamagePoint = 57.5 ;							// 25.0; // shield energy required to block one point of damage -soph

   rechargeRate = $MechRechargeRate[$MechMask::Emancipator];
   jetForce = (21.4 * 182) * 11.1; 
   underwaterJetForce = (21.4 * 182) * 13;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = $MechRechargeRate[ $MechMask::Emancipator ] * 2 ;			//  1.359; -soph
   underwaterJetEnergyDrain =  $MechRechargeRate[ $MechMask::Emancipator ] * 1.75 ;	// 1.25; -soph
   minJetEnergy = $MechReactorSize[ $MechMask::Emancipator ] + 1 ;			// 751; -soph
   maxJetHorizontalPercentage = 0.8;

   runForce = ( 35 * 210 ) * 50 ;							// (35 * 210) * 5; -soph
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 6;
   maxBackwardSpeed = 2;
   maxSideSpeed = 2;

   maxUnderwaterForwardSpeed = 2;
   maxUnderwaterBackwardSpeed = 1;
   maxUnderwaterSideSpeed = 1;

   recoverDelay = 6;
   recoverRunForceScale = 1.2;

   jumpForce = ( 9.25 * 215 ) * 100 ;
   jumpEnergyDrain = 0;
   minJumpEnergy = 65535;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 0.01; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 10; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 75;
   jumpSurfaceAngle = 75;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   horizMaxSpeed = 1000;
   horizResistSpeed = 20;
   horizResistFactor = 0.3;
   maxJetForwardSpeed = 8;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.15;

   minImpactSpeed = 60;
   speedDamageScale = 0.006;

   jetSound = HAPCFlyerThrustSound;
   wetJetSound = AssaultVehicleThrustSound;
   jetEmitter = TurboJetEmitter;

   boundingBox = "6.2 6.2 9.0";
   pickupRadius = 1.5; //0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = HeavyMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 20.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 6;           // Max number of different weapons the mech can have
   maxGrenades = 0;
   maxMines = 0;

   // Inventory restrictions
//   max[RepairKit]          = 1000;	// -[soph]
//   max[Beacon]             = 1000;	// -
//   max[Mine]               = 1000;	// -
//   max[Grenade]            = 1000;	// -[/soph]

//   max[TargetingLaser]     = 1;

   observeParameters = "1.0 12.0 12.0";

   shieldEffectScale = "1.0 1.0 1.0";
};

datablock PlayerData(EmancipatorMechJet) : EmancipatorMechBase
{
   jetForce = ( 21.4 * 182 ) * 151 ;							// (21.4 * 182) * 15.1; -soph
   underwaterJetForce = ( 21.4 * 182 ) * 250 ;						// (21.4 * 182) * 25; -soph
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = $MechRechargeRate[$MechMask::Emancipator] * 6;
   underwaterJetEnergyDrain = $MechRechargeRate[$MechMask::Emancipator] * 3;
   minJetEnergy = $MechReactorSize[ $MechMask::Emancipator ] / 15 ;			// 15; -soph
   maxJetHorizontalPercentage = 0.4;

   horizMaxSpeed = 1000;
   horizResistSpeed = 20;
   horizResistFactor = 0.3;
   maxJetForwardSpeed = 8;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.15;
};

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
