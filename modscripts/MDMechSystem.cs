// Proper Mech system v2.0

// Datablocks
exec("scripts/modscripts/vmods/mechChassis.cs");
exec("scripts/modscripts/vmods/mechWeapons.cs");
exec("scripts/modscripts/vmods/mechProjectiles.cs");
exec("scripts/modscripts/vmods/mechBlocks.cs");
exec("scripts/modscripts/vmods/mechAbstract.cs");

// String Table
$MechKeys = "Jump: This help screen, HealthKit: Activates module (if active type), Pack: Alpha Strike, Beacon: Flush coolant, Mine: Turns Targeting Computer on/off, Switch Weapon: Switches firegroups, Fire: Fires selected firegroup.";
$MechStationTonnageDefault = 50 ;
$MechStationTonnageSchedule = 7500 ;
$MechStationTonnageMax = 200 ;

// Function Prototypes
// Assault EAX Main Datablock


// Functions

function MechTonnageLoop( %obj ) 
{
   if( !isObject( %obj ) )
      return ;

   if( %obj.mechTonnageLoop )
      cancel( %obj.mechTonnageLoop ) ;

   %obj.mechPad = true ;

   if( $matchStarted && %obj.getDamageState() $= "Enabled" )
   {
      if( %obj.mechTons < $MechStationTonnageMax )
      {
         %obj.mechTons++ ;
         %obj.mechTonnageLoop = schedule( $MechStationTonnageSchedule , 0 , MechTonnageLoop , %obj ) ;
      }
      else
         %obj.mechTonnageLoop = schedule( 250 , 0 , MechTonnageLoop , %obj ) ;
   }
   else
      %obj.mechTonnageLoop = schedule( 250 , 0 , MechTonnageLoop , %obj ) ;
}

function MechHandleCollision(%data, %obj, %col)
{
   if(%col.isMounted() || %col.getState() $= "Dead")
   {
     %obj.schedule(1001, delete);
     %obj.startFade(1000, 0, true);
//     $TeamDeployedCount[%obj.team, StaticTacMech]--;
    // $VehicleTotalCount[%obj.team, %obj.dataName] -= 1;
     return;
   }

   if( isObject( %col.mechStation ) )			// +[soph]
   {							// +
      %client = %col.client ;				// +
      if( %col.mechStation.mechTons >= %client.mechWeight ) 
         %col.mechStation.mechTons -= %client.mechWeight ;
      else						// +
      {							// +
         %obj.schedule( 1001 , delete ) ;		// +
         %obj.startFade( 1000 , 0 , true ) ;		// +
         return ;					// +
      }							// +
   }							// +
   else							// +
   {							// +
      %obj.schedule( 1001 , delete ) ;			// +
      %obj.startFade( 1000 , 0 , true ) ;		// +
      return ;						// +
   }							// +
   messageTeam( %obj.team , 'MsgMechBought' , '\c5%1 has activated one of your team\'s %2 Mechs. The station has %3 mech tons remaining.' , %col.client.name , %obj.dataName , %col.mechStation.mechTons ) ;
   %obj.originTeam = %col.team ;			// +
   %obj.pilot = %col.client ;				// +[/soph]
      ejectFlag(%col);
      %col.setVelocity("0 0 0");
      %col.clearInventory();
      %col.client.setWeaponsHudClearAll();
      %col.setImageTrigger(0, false);
      %col.setImageTrigger(1, false);
      %col.setImageTrigger(2, false);

      MechInit(%col, %obj);
}

function MechInit(%mech, %static)
{
   %trans = %static.getTransform();
   %vData = $MechData[ %mech.client.currentMechSlot ] ;	// +soph

   %static.delete();
   %mech.setTransform(%trans);
   %mech.damageMod = 1.0;

   %mech.deployedMode = false;
   %mech.inDEDField = false;
   %mech.daishiOnline = false;
   %mech.overdriveMode = false;
   %mech.isMageIon = false;
   
   // Now to put humpty dumpty together...
   %client = %mech.client;
   %mechDatablock = %vData@"MechBase";
   %mechMask = %mechDatablock.armorMask;
   %mechID = %mech.client.currentMechSlot ;		// $MechID[%mechMask]; -soph
   %baseWeight = $MechWeightLimit[%mechID];
   %name = $MechName[%mechID];
   %subName = $MechSubName[%mechID];
//   %vData = %chassis//$MechData[%mechID];
   %armorTon = $MechDefaultArmorWeight[%mechID];
   %modArmorVal = %client.modArmorVal[ %mech.client.currentMechSlot ] !$= "" ? %client.modArmorVal[ %mech.client.currentMechSlot ] : 0;
   %hp = %mechDatablock.maxDamage ;			// %hp = %armorTon * $MechHPPerTon; -soph
   %healthMod = ((%modArmorVal + %armorTon) * $MechHPPerTon / %hp);
   %health = %healthMod * %hp;
   %displayHP = mFloor(%health * 100);
   %heatsinkTon = $MechDefaultHeatSinkWeight[%mechID];
   %modHeatsinkVal = %client.modHeatsinkVal[ %mech.client.currentMechSlot ] !$= "" ? %client.modHeatsinkVal[ %mech.client.currentMechSlot ] : 0;
   %equipWeight = %heatsinkTon + %modHeatsinkVal;
   %numWeapons = $MechMaxWeapons[%mechID];

   // Object copyovers
   %mech.mechID = %mech.client.currentMechSlot ;	// %mechID; -soph
   %mech.mechName = %name;
   %mech.vData = %vData;
   %mech.damageMod = 1.0;
   %mech.resistMod = %healthMod; // resist = damage / resistMod
   %mech.heatSinkCount = %equipWeight;
   %mech.numWeapons = %numWeapons;
   %mech.isTacticalMech = true;
   %mech.isMech = true;   
   %mech.selectedFireGroup = 0;
   %mech.heatGainMod = 1.0;
   %mech.isShielded = false;
   %mech.nextShutdownTime = 0;
   %mech.mechShutdownState = false;
   %mech.targetingComputerStatus = false;
   %mech.mechHeatLevel = 0;
   %mech.heatReduceTick = 0;
   %mech.isJetting = false;
   %mech.ammoMod = 1.0;
   %mech.totalWeight = %client.mechWeight ;		// +[soph]
   %mech.baseWeight = %baseWeight ;			// +
   %mech.armorWeight = %armorTon + %modArmorVal ;	// +
   clearLoopingDamages( %mech ) ;			// +[/soph]
   
   %mech.coolantCharges = %client.mEquip[ %client.currentMechSlot , $MechEquip::Coolant ] ? 6 : 3 ;	// %mech.coolantCharges = %mech.moduleID == $MechModule::AmmoCache ? 6 : 3; -soph
   if( %client.mEquip[ %client.currentMechSlot , $MechEquip::Synomium ] )				// %mech.isSyn = %mech.moduleID == $MechModule::SynomiumDevice ? true : false; -soph
   {													// +[soph]
      %mech.isSyn = true ;										// +
      %rechargeBonus = 4 / 32 ;										// +
   }													// +
   else													// +
      %rechargeBonus = 0 ;										// +
   if( %client.mEquip[ %client.currentMechSlot , $MechEquip::Shields ] )				// +
   {													// +
      %mech.isShielded = true ;										// +
      %rechargeBonus -= 4 / 32 ;									// +
   }													// +
   if( %client.mEquip[ %client.currentMechSlot , $MechEquip::JumpJets ] )				// +
   {													// +
      %word = "Jet" ;											// +
      %mech.hasJets = true ;										// +
   }													// +
   else													// +
      %word = "Base" ;											// +[/soph]

   // Assign weaponry
   for(%i = 2; %i < 2+%numWeapons; %i++)
   {
       %idxNum = %i - 2;
       %wep = $MechHardPointSlot[%mechID, %i] & $MechHardpointType::None ? 0 : (%client.mechWeaponSlot[%mech.client.currentMechSlot , %i] $= "" ? $MechHardPointSlot[%mechID, %i] : %client.mechWeaponSlot[%mech.client.currentMechSlot , %i]);

       if(!%wep)
            continue;
       else
       {
          %weaponID = %client.mechWeaponSlotID[%mech.client.currentMechSlot , %idxNum] $= "" ? $MechDefaultWeapon[%mechID, %idxNum] : %client.mechWeaponSlotID[%mech.client.currentMechSlot , %idxNum];
          %mech.slotType[%i] = %weaponID;
          %mech.slotFireGroup[%i] = %client.mechSlotFireGroup[%mech.client.currentMechSlot , %i] $= "" ? $MechWeaponDefaultFiregroup[%weaponID] : %client.mechSlotFireGroup[%mech.client.currentMechSlot , %i];
          %mech.slotAmmo[%i] = $MechWeaponUsesAmmo[%mechID] ? $MechWeaponAmmoCount[%weaponID] * %mech.ammoMod : 0;
          %shapeName = $MechWeaponShape[%weaponID];
          %imageName = "Mech"@%subName@%shapeName@%i@"Image";
          
          %mech.mountImage(%imageName, %i);
          commandToClient(%client, 'setWeaponsHudItem', %i, 5, 1);          
          commandToClient(%client, 'setWeaponsHudAmmo', %i, %mech.slotAmmo[%i]);          
       }
   }

   %data = %vData@"Mech"@%word;
   %mech.setDatablock(%data);
   %mech.setEnergyLevel(%mechDatablock.maxEnergy);
   %mech.setRechargeRate( %mechDatablock.rechargeRate + %rechargeBonus ) ;				// %mech.setRechargeRate(%mechDatablock.rechargeRate); -soph
   %mech.client.setWeaponsHudActive("Blaster");
   %mech.mountImage(MechNormalParam, 0);
   %mech.setInventory("Beacon", %mech.coolantCharges);

   commandToClient(%mech.client, 'setAmmoHudCount', "Alpha");
   MechTick(%data, %mech);
   MechUpdateHeat(%mech);
}

function MechRestock(%mech)
{
   %vData = %mech.vData;
   %client = %mech.client;
   %mechDatablock = %vData@"MechBase";
   %mechMask = %mechDatablock.armorMask;
   %mechID = $MechID[%mechMask];
   %numWeapons = $MechMaxWeapons[%mechID];

   %mech.coolantCharges = %mech.moduleID == $MechModule::AmmoCache ? 6 : 3;

   // Reassign ammo
   for(%i = 2; %i < 2+%numWeapons; %i++)
   {
       %idxNum = %i - 2;
       %wep = $MechHardPointSlot[%mechID, %i] & $MechHardpointType::None ? 0 : (%client.mechWeaponSlot[%mech.client.currentMechSlot , %i] $= "" ? $MechHardPointSlot[%mechID, %i] : %client.mechWeaponSlot[%mech.client.currentMechSlot , %i]);

       if(!%wep)
            continue;
       else
       {
          %weaponID = %client.mechWeaponSlotID[%mech.client.currentMechSlot , %idxNum] $= "" ? $MechDefaultWeapon[%mechID, %idxNum] : %client.mechWeaponSlotID[%mech.client.currentMechSlot , %idxNum];
          %mech.slotAmmo[%i] = $MechWeaponUsesAmmo[%mechID] ? $MechWeaponAmmoCount[%weaponID] * %mech.ammoMod : 0;
          commandToClient(%client, 'setWeaponsHudAmmo', %i, %mech.slotAmmo[%i]);
       }
   }
}

$MechUpdateTimeMS = 333;

function MechTick(%data, %obj)
{
     if(!isObject(%obj))
          return;

     if(%obj.mechDead)
          return;

     if(%obj.isWet)
          %obj.setHeat(0.0);
     else
          %obj.setHeat(%obj.mechHeatLevel * 0.01);

     if(%obj.isLava)
          MechAddHeat(%obj, 4.6 );						// getRandom(10, 20)); -soph

     if( %obj.hasJets && %obj.isJetting && %obj.getEnergyLevel() > 50 && !%obj.mechShutdownState )
          MechAddHeat(%obj, 2.3);

     if(%obj.heatReduceTick <= 0) // paranoia check
     {
          if( %obj.mechHeatLevel > ( $MechHeatLimit[%obj.mechID] + 50 ) )	// +[soph]
               MechAmmoExplosion( %data , %obj );				// +
          else									// +
               %obj.LD_Brn_SrcObj = 0 ;						// +[/soph]

          if(%obj.mechHeatLevel > 0)
               if(%obj.mechShutdownState)
                    %obj.mechHeatLevel -= (1 + %obj.heatSinkCount) / 3;
               else
                    %obj.mechHeatLevel -= (1 + %obj.heatSinkCount) / 7;
                    
          if(%obj.mechHeatLevel < 0)
               %obj.mechHeatLevel = 0;
                    
          if( %obj.isWet )							// if(%obj.isWet || %obj.isLava || %obj.mechShutdownState) -soph
               %obj.heatReduceTick = 2;				
          else
               %obj.heatReduceTick = 5;
               
//          if(!%obj.mechShutdownState && getSimTime() > %obj.nextShutdownTime)
//               MechUpdateHeat(%obj);
     }
     else
          %obj.heatReduceTick--;
     
     %obj.mech_lastPosition = %obj.getPosition() ;
     %obj.mech_lastVelocity = %obj.getVelocity() ;
     
     schedule(50, %obj, MechTick, %data, %obj);
}

function MechUpdateHeat(%obj)
{
     if(!isObject(%obj))
          return;

     if(%obj.mechDead)
          return;

     if(!%obj.mechShutdownState)
     {
          %graphHeat = %obj.mechHeatLevel;
          %maxHeat = $MechHeatLimit[%obj.mechID];
          %heatPct = %graphHeat / %maxHeat;

          %color = %heatPct > 0.6 ? (%heatPct > 0.8 ? "<color:FF0000>" : "<color:FFFF00>") : "<color:00FF00>";					// +soph

          if(%heatPct > 1)
               %heatPct = 1;

//          %status = %obj.targetingComputerStatus ? "<color:FF0000>On<color:42dbea>" : "<color:00FF00>Off<color:42dbea>";			// -soph
          %status = %obj.targetingComputerStatus ? "<color:FF0000>Long-Range<color:42dbea>" : "<color:00FF00>Short-Range<color:42dbea>";	// +soph
          %word = $MechFiregroupName[%obj.selectedFireGroup];

          if(%obj.client.hasMD2Client)
               commandToClient(%obj.client, 'GraphMechHUD', %heatPct, %graphHeat, %status, %word, %obj.coolantCharges);
          else
               bottomPrint(%obj.client, "Heat:"@%color SPC mCeil(%graphHeat) SPC "<color:42dbea>K | Targeting Comp:" SPC %status SPC "| Firegroup:" SPC %word SPC "| Coolant:" SPC %obj.coolantCharges, 1, 1);
     }
     
     %updateTime = %obj.client.hasMD2Client ? 250 : 333;
     schedule(%updateTime, %obj, MechUpdateHeat, %obj);
}

function MechAddHeat(%obj, %heat)
{
     %mechHeat = %obj.mechHeatLevel;
     %modifier = %obj.mechShutdownState ? %obj.heatGainMod * 2 : %obj.heatGainMod;
     %newMechHeat = %mechHeat + (%heat * %modifier);
     
     if(%newMechHeat < 0)
          %newMechHeat = 0;
     
     %obj.mechHeatLevel = %newMechHeat;
     
     %explodeHeat = $MechHeatLimit[%obj.mechID];
     
     if(%newMechHeat > %explodeHeat)
     {
          // Bad stuff here
          if(!%obj.mechShutdownState)
          {
               schedule(32, %obj, MechGrenadeTriggerToggle, %obj.getDatablock(), %obj, true);
               bottomPrint( %obj.client , "[ WARNING ]  HEAT CRITICAL  [ WARNING ]\nREACTOR MELTDOWN - AMMO EXPLOSION IMMINENT" , 5 , 2 ) ;	// +soph
          }
          else
          {
              // MechDamageObject(%obj.getDatablock(), %obj, %obj, %obj.position, 10000, $DamageType::Overheat, "0 0 0", 0);			// -soph
              // bottomPrint(%obj.client, "Mech reactor critical... overload!", 5, 1);								// -soph
               
          }
     }
}

function MechAmmoExplosion( %data , %obj )	// +[soph]
{
   if( %obj.LD_Brn_SrcObj )
      %source = %obj.LD_Brn_SrcObj ;
   else
      if( %obj.player )
         %source = %obj ;
      else
         %source = %obj.pilot.player ;

   %center = %obj.getEyeTransform() ;
   %center = getWords( %center , 0 , 2 ) ;

   %checks = 1 + ( ( %obj.mechHeatLevel - ( $MechHeatLimit[ %obj.mechID ] + 50 ) ) / 50 ) ;
   for( %i = 0 ; %i < %checks ; %i++ )
   {
      %slot = getRandom( %obj.numWeapons ) + 2 ;

      %type = %obj.slotType[ %slot ] ;

      if( !%type )
         continue ;
     
      %pos = vectorSub( %obj.getMuzzlePoint( %slot ) , vectorscale( vectorNormalize( %obj.getMuzzleVector( %slot ) ) , 0.5 ) ) ;
      %rand = mSqRt( %checks ) ;
      %x = getWord( %pos, 0 ) + getRandomT( %rand ) ;
      %y = getWord( %pos, 1 ) + getRandomT( %rand ) ;
      %z = getWord( %pos, 2 ) + getRandomT( %rand ) ;
      %vec = getVectorFromPoints( %center , %pos ) ;

      if( $MechWeaponUsesAmmo[ %type ] && MechHasAmmo( %obj , %slot ) )
      {
         if( !%obj.fireTimeout[ %slot ] )
            MechSetSlotCooldown( %obj , %slot , $MechWeaponFireTime[ %type ] ) ;
         else
         {
            if( getRandom() * ( %obj.mechHeatLevel + 4 * $MechHeatLimit[ %obj.mechID ] ) < 4 * $MechHeatLimit[ %obj.mechID ] )
            {
               throwSparks( %obj , %slot , %pos , %vec ) ;
               continue ;
            }
         }
         createLifeEmitter( %pos , PlasmaExplosionEmitter , 300 ) ;
         for( %j = 0 ; %j < getRandom( 1 , 4) ; %j++ )
            schedule( getRandom( 4 ) * 50 , 0 , throwSparks , %obj , %slot , %pos , %vec ) ;
         %pos = %x SPC %y SPC %z ;
         %radius = getRandom( 1 , ( 3 + %rand ) ) ;
         %damage = 0.05 + ( $MechWeaponHeat[ %type ] / 40 ) ;
         if( !%obj.invincible )
         {
            %obj.invincible = true;
            RadiusExplosion( %obj , %pos , %radius , %damage , $MechWeaponHeat[ %type ] * 10 , %obj , $DamageType::DaishiReactor ) ;
            IncendiaryExplosion( %pos , %radius , %damage , %obj );
            %obj.invincible = false;
            MechDamageObject( %data , %obj , %source , %pos , %damage , $DamageType::BurnLoop ) ;
         }
         else
         {
            RadiusExplosion( %obj , %pos , %radius , %damage , $MechWeaponHeat[ %type ] * 10 , %obj , $DamageType::DaishiReactor ) ;
            IncendiaryExplosion( %pos , %radius , %damage , %obj );
         }

         MechUseAmmo( %obj , %slot , 1 ) ;
//         MechAddHeat( %obj , ( $MechWeaponHeat[ %type ] / 10 ) ) ;
      }
      else
      {
         if( getRandom() * ( %obj.mechHeatLevel + 6 * $MechHeatLimit[ %obj.mechID ] ) < 6 * $MechHeatLimit[ %obj.mechID ] )
         {
            throwSparks( %obj , %slot , %pos , %vec ) ;
            continue ;
         }
      }
      %obj.setImageTrigger( %slot , false ) ;
   }
   MechDamageObject( %data , %obj , %source , %obj.getWorldBoxCenter() , 0.02 * %checks + 0.1 , $DamageType::BurnLoop ) ;
}

function throwSparks( %obj , %slot , %pos , %vec )
{
   %p = new GrenadeProjectile()
   {
      dataBlock        = ProtronFireSpray ;
      initialDirection = vectorScale( calcSpreadVector( %vec , 650 ) , 1 + getRandom( 9 ) ) ;
      initialPosition  = %pos ;
      sourceObject     = %obj ;
      sourceSlot       = %slot ;
   };
   MissionCleanup.add( %p ) ;
   %p.schedule( getRandom( 5 , 8 ) * 50 , delete ) ;
}						// +[/soph]

function MechCalculateShields(%data, %targetObject, %position, %amount, %damageType)
{
   if(!isObject(%targetObject))
      return 0;

//   if(%damageType == $DamageType::MitziTransparent || %damageType == $DamageType::Blaster)
//      return %amount;

   if($DamageGroup[%damageType] & $DamageGroupMask::IgnoreShield)
      return %amount;

//   if(%obj.isSyn)				// -soph
//     %amount *= 2.0;				// -soph
      
//   %energy  = %targetObject.turretObject.getCapacitorLevel();
   %energy = %targetObject.getEnergyLevel();
   %strength = %energy / %data.energyPerDamagePoint;
   %shieldScale = $InheritDamageProfile[%damageType] > 0 ? %data.shieldDamageScale[$InheritDamageProfile[%damageType]] : %data.shieldDamageScale[%damageType];   
//   %shieldScale = %data.shieldDamageScale[%damageType];
   if(%shieldScale $= "")
      %shieldScale = 1;

   if (%amount * %shieldScale <= %strength)
   {
      // Shield absorbs all
      %lost = %amount * %shieldScale * %data.energyPerDamagePoint;
      %energy -= %lost;
      %targetObject.setEnergyLevel(%energy);
//      %targetObject.turretObject.setCapacitorLevel(%energy);

      %normal = "0.0 0.0 1.0";
      %targetObject.playShieldEffect( %normal );

      return 0;
   }

   // Shield exhausted
//   %targetObject.turretObject.setCapacitorLevel(0);
   %targetObject.setEnergyLevel(0);
   return %amount - %strength / %shieldScale;
}

function MechProcessUse(%data, %obj, %idata)
{
     switch$(%idata)
     {
          case "BackPack":
               MechPackTriggerToggle(%data, %obj);

          case "Beacon":
               MechBeaconTriggerToggle(%data, %obj);

          case "RepairKit":
               MechHealthKitTriggerToggle(%data, %obj);

          default:
               return;
     }
}

function MechOnTrigger(%data, %player, %triggerNum, %val)
{
     switch(%triggerNum)
     {
          case 0:
               MechProcessFireWeapons(%data, %player, %val);

          case 2:
               MechJumpTriggerToggle(%data, %player, %val);
               
          case 3:
               MechJetTriggerToggle(%data, %player, %val);

          case 4:
               MechGrenadeTriggerToggle(%data, %player, %val);
               
          case 5:
               MechMineTriggerToggle(%data, %player, %val);
               
          default:
               return;
     }
}

// Triggerless
function MechPackTriggerToggle(%data, %obj)
{
     if(%obj.mechDead)
          return;
     if( %obj.isFiring )	// +[soph]
          %firing = true;	// alpha strike should not interrupt normal chain
     else			// +[/soph]
          %obj.isFiring = true;

     %maxSlot = 2 + %obj.numWeapons;

     for(%i = 2; %i < %maxSlot; %i++)
          fireSlotWeapon(%data, %obj, %i, %obj.slotType[%i]);
     
     if( !%firing )		// +soph
          %obj.isFiring = false;
//     bottomPrint(%obj.client, ">>> ALPHA STRIKE <<<\nAll weapons fired, trigger automatically released.", 5, 2);               
}

function MechBeaconTriggerToggle(%data, %obj)
{
     if(%obj.coolantCharges > 0)
     {
          MechAddHeat(%obj, $MechHeatLimit[%obj.mechID] * -0.75);
     
          %obj.coolantCharges--;
          %obj.setInventory("Beacon", %obj.coolantCharges);
          %obj.play3D(VehicleExitWaterSoftSound);  
        
          %center = %obj.getWorldBoxCenter() ;					// +[soph]
          for( %i = 0 ; %i < 8 ; %i++ )						// +
          {									// +
               %pos = ( getWord( %center , 0 ) + getRandom( 0 , 6 ) - 3 ) SPC	// +
                      ( getWord( %center , 1 ) + getRandom( 0 , 6 ) - 3 ) SPC	// +
                      ( getWord( %center , 2 ) + getRandom( 1 , 9 ) - 5 ) ;	// +
               %time = getRandom( 2 , 5 ) * 100 ;				// +
               createLifeEmitter( %pos , "MortarBubbleEmitter" , %time ) ;	// +
          }									// +[/soph]

//          bottomPrint(%obj.client, "Coolant used! "@%obj.coolantCharges@" remaining.", 5, 1);          
     }
     else
//     {
          %obj.play3d(BomberTurretDryFireSound);
//          bottomPrint(%obj.client, "Out of coolant!", 3, 1);
//     }
     
//     if(%obj.slotFireGroup[%obj.selectedFireGroup] & (1 << %obj.selectedSlot))
//     {
//          %obj.slotFireGroup[%obj.selectedFireGroup] ^= (1 << %obj.selectedSlot);
//          
//          %word = getFiregroupName(%obj.selectedFireGroup);
//          bottomPrint(%obj.client, $MechWeaponName[%obj.slotType[%obj.selectedSlot]] SPC "removed from Firegroup" SPC %word, 4, 1);
//     }
//     else
//     {
//          %obj.slotFireGroup[%obj.selectedFireGroup] |= (1 << %obj.selectedSlot);
//
//          %word = getFiregroupName(%obj.selectedFireGroup);
//          bottomPrint(%obj.client, $MechWeaponName[%obj.slotType[%obj.selectedSlot]] SPC "added to Firegroup" SPC %word, 4, 1);
//     }
}

function MechHealthKitTriggerToggle(%data, %obj)
{
     %state = %obj.mechModuleTrigger ? false : true;
     
    // MechUseModule(%obj, %state);	// no point with no active modules -soph
//     %obj.selectedSlot++;
//
//     if(%obj.chassis $= "Heavy")
//          %maxSlot = 7;
//     else if(%obj.chassis $= "Medium")
//          %maxSlot = 5;
//     else if(%obj.chassis $= "Light")
//          %maxSlot = 4;
//
//     if(%obj.selectedSlot > %maxSlot)
//          %obj.selectedSlot = 2;
//
//     commandToClient(%obj.client, 'setWeaponsHudActive', %obj.selectedSlot, "Blaster", true);
//     bottomPrint(%obj.client, "Weapon" SPC %obj.selectedSlot-1 SPC "["@$MechWeaponName[%obj.slotType[%obj.selectedSlot]]@"] selected.", 3, 1);
}

// Triggered
function MechGrenadeTriggerToggle(%data, %obj, %state)
{
     if(!%state)
          return;
          
     %time = getSimTime();
     
     if(%time < %obj.nextShutdownTime)
          return;
     
//     %obj.mechShutdownState =  ? false : true;
     
     if(%obj.mechShutdownState)
     {
          %obj.mechShutdownState = false;

          %obj.setPassiveJammed( false ) ;										// +soph
//          setTargetSensorData(%obj.client.target, PlayerSensor);          						// -soph
        //  bottomPrint(%obj.client, %obj.mechName SPC "starting up\nWeapons system coming online...", 5, 2);		// -soph
	  
          %recharge = %obj.getRechargeRate() + ( 1.5 * %data.rechargeRate ) ;						// +[soph]
          messageClient( %obj.client , 'MsgMechShutdown' , '\c2%1 powering up, five seconds to weapons hot.' , %obj.mechName ) ;
          %obj.setRechargeRate( %recharge  ) ;										// +[/soph]   
       
          %obj.nextShutdownTime = %time + 5000;
          %obj.client.setControlObject(%obj);
//          %obj.isShielded = false;											// -soph
          %obj.play3D(SynomiumEngage);
          // Play gen startup sound
     }
     else
     {
          %obj.mechShutdownState = true;     
          %obj.isFiring = false;          
          %obj.nextShutdownTime = %time + 5000;

         // bottomPrint(%obj.client, %obj.mechName SPC "shutting down...\nPress FIRE to restart your mech.", 30, 2);	// -soph
          messageClient( %obj.client , 'MsgMechShutdown' , '\c2%1 shutting down...' , %obj.mechName ) ;			// +[soph]
          %recharge = %obj.getRechargeRate() - ( 1.5 * %data.rechargeRate ) ;						// +
          %obj.setRechargeRate( %recharge  ) ;										// +
          schedule( 5000 , 0 , mechShutDownMessage , %obj , %obj.client ) ;						// +[/soph]
         
//          setTargetSensorData(%obj.client.target, JammerSensorObjectActive);						// -soph
          %obj.setPassiveJammed( true ) ;										// +soph
          
          %obj.client.camera.setTransform(%obj.getTransform());
          %obj.client.camera.getDataBlock().setMode( %obj.client.camera, "mechOrbit", %obj);
          %obj.client.camera.setOrbitMode(%obj, %obj.getTransform(), 0.75, 9.5, 9.5);
          %obj.client.setControlObject(%obj.client.camera);
//          %obj.isShielded = false;											// -soph
          %obj.play3D(JetfireTransform);
     }
}

function mechShutDownMessage( %obj , %client )
{
	if( isObject( %obj ) && !%obj.mechDead )
		messageClient( %client , 'MsgMechShutdown' , '\c2%1 shutdown complete.\n Press FIRE to power up, or JUMP to eject.' , %obj.mechName ) ;
}

function MechMineTriggerToggle(%data, %obj, %state)
{
     if(%state)
     {
          %obj.targetingComputerStatus = %obj.targetingComputerStatus ? false : true;
//          %obj.heatGainMod = %obj.targetingComputerStatus ? 1.25 : 1.0;						// -soph
          %image = %obj.targetingComputerStatus ? MechSeekingParam : MechNormalParam;
          %sound = %obj.targetingComputerStatus ? "PBWSwitchSound" : "SynomiumOff";
          %status = %obj.targetingComputerStatus ? "online" : "offline";
          %ret = %obj.targetingComputerStatus ? "MissileLauncher" : "Blaster";
          %obj.unmountImage(0);
          %obj.mountImage(%image, 0);
          
          %obj.play3D(%sound);
//          commandToClient(%obj.client, 'setWeaponsHudActive', 0, "Blaster", true);
          %obj.client.setWeaponsHudActive(%ret);          
//          bottomPrint(%obj.client, "Targeting Computer is" SPC %status, 3, 1);
     }
}

function MechJumpTriggerToggle(%data, %obj, %state)
{
//     bottomPrint(%obj.client, "Key Help - Keys:" SPC $MechKeys, 10, 3);
	%time = getSimTime() ;					// +[soph]
	if( %obj.mechshutdownstate && %time > %obj.nextShutdownTime )
	{							// +
		if( %obj.mechLastEjectAttempt + 600 > %time )	// +
			MechEjectPilot( %obj ) ;		// +
		else						// +
			messageClient( %obj.client , 'MsgEjectAttempt' , '\c2Escape system primed, press JUMP key again to eject.' ) ;
		%obj.mechLastEjectAttempt = %time ;		// +
	}							// +[/soph]
}

function MechEjectPilot( %obj )					// +[soph]
{								// +
	%client = %obj.client ;					// +
	%mechTransform = %obj.getTransform ;			// +
	%pilotTransform = vectorAdd( %obj.getWorldBoxCenter() , "0 0 9" ) SPC getWords( %pilotTransform , 3 , 6 ) ;
	if( %client.race $= "Bioderm" )				// +
		%armor = $DefaultPlayerArmor @ "Male" @ %client.race @ Armor ;
	else							// +
		%armor = $DefaultPlayerArmor @ %client.sex @ %client.race @ Armor ;
	%client.armor = $DefaultPlayerArmor;			// +
	%client.player = 0 ;					// +
	for( %i =0 ; %i < $InventoryHudCount ; %i++ )		// +
		%client.setInventoryHudItem( $InventoryHudData[ %i , itemDataName ] , 0 , 1 ) ;
	%client.clearBackpackIcon() ;				// +
								// +
	%obj.client = 0 ;					// +
	%obj.team = 0 ;						// +
	%obj.setTarget( 0 ) ;					// +
								// +
	%player = new Player() {				// +
		dataBlock = %armor ;				// +
	} ;							// +
	%player.setTransform( %pilotTransform ) ;		// +
	MissionCleanup.add( %player ) ;				// +
	%player.client = %client ;				// +
	%client.player = %player ;				// +
	%player.team = %client.team ;				// +
	%player.setEnergyLevel( 60 ) ;				// +
	%client.setWeaponsHudClearAll() ;			// +
	%player.setInventory( Blaster , 1 ) ;			// +
	%player.setInventory( TargetingLaser , 1 ) ;		// +
	%player.use( "Blaster" ) ;				// +
	%player.applyImpulse( getWords( %pilotTransform , 0 , 2 ) , "0 0 1" ) ;
	%player.weaponCount = 1 ;				// +
								// +
	%client.setControlObject( %player ) ;			// +
	%player.setTarget( %client.target ) ;			// +
	setTargetDataBlock( %client.target , %player.getDatablock() ) ;
	setTargetSensorData( %client.target , PlayerSensor ) ;	// +
	setTargetSensorGroup( %client.target , %player.team ) ;	// +
	%client.setSensorGroup( %client.team ) ;		// +
	clearCenterPrint( %client ) ;				// +
	clearBottomPrint( %client ) ;				// +
								// +
//	%count = ClientGroup.getCount() ;			// +
//	for( %i = 0 ; %i < %count ; %i++ )			// +
//	{							// +
//		%cl = ClientGroup.getObject( %i ) ;		// +
//		if( %cl.camera.mode $= "observerFollow" && %cl.observeClient == %player.client )
//		{						// +
//			%transform = %player.getTransform() ;	// +
//			%cl.camera.setOrbitMode( %player , %transform , 0.5 , 4.5 , 4.5 ) ;
//			%cl.camera.targetObj = %player ;	// +
//		}						// +
//	}							// +
	if( %client.observeCount > 0 )				// +
		resetObserveFollow( %client , true ) ;		// +
								// +
	return %player ;					// +
}								// +[/soph]

function MechJetTriggerToggle(%data, %obj, %state)
{
     if(%obj.equipID == $MechEquip::JumpJets)
          %obj.isJetting = %state;
}

function MechCycleWeapon(%data, %obj, %dir)
{
     if(%dir $= "prev")
     {
          %obj.selectedFireGroup--;

          if(%obj.selectedFireGroup < 0)
               %obj.selectedFireGroup = $MechMaxFireGroups - 1;
     }
     else
     {
          %obj.selectedFireGroup++;

          if(%obj.selectedFireGroup >= $MechMaxFireGroups)
               %obj.selectedFireGroup = 0;
     }

     %word = $MechFiregroupName[%obj.selectedFireGroup];
     commandToClient(%obj.client, 'setAmmoHudCount', %word);
//     bottomPrint(%obj.client, "Switched to firegroup" SPC %word, 3, 1);
//     %obj.setInventory("RepairKit", %obj.selectedFireGroup);     
//     %obj.isFiring = false;					// -soph
}

function MechProcessFireWeapons(%data, %obj, %state)
{
     %obj.isFiring = %state;
     
     if(%state)
          MechFireCheckLoop(%data, %obj);
}

function MechFireCheckLoop(%data, %obj)
{
     if(%obj.mechDead)
          return;

     if(%obj.mechShutdownState)
     {
          MechGrenadeTriggerToggle(%data, %obj, true);
          return;
     }

     if(%obj.isFiring)
         schedule(100, %obj, MechFireCheckLoop, %data, %obj);
     else
         return;

     %maxSlot = 2 + %obj.numWeapons;

     for(%i = 2; %i < %maxSlot; %i++)
          if(%obj.slotFireGroup[%i] & (1 << %obj.selectedFireGroup))
               fireSlotWeapon(%data, %obj, %i, %obj.slotType[%i]);
}

function EAXOnPlayerMounted(%data, %obj, %client)
{
   %sr = 0;

   if(%data.isShielded)
      %sr = %data.maxEnergy / %data.energyPerDamagePoint;

   %w1 = %obj.weapon[1, Name] $= "" ? "N/A" : %obj.weapon[1, Name];
   %w2 = %obj.weapon[2, Name] $= "" ? "N/A" : %obj.weapon[2, Name];

//   %obj.resumeVBarTime = getSimTime() + 5000;

   commandToClient(%client, 'BottomPrint', "Hit Points (HP) "@(%data.maxDamage * 100)@" | Shield Strength: "@(%sr * 100)@" GeV | Power Core: "@%data.maxEnergy@" MW\nWeapon 1: "@%w1@" | Weapon 2: "@%w2@"\n"@%obj.veh_description, 10, 3);
}

function MechUseModule(%mech, %state)
{
     if(%state)
     {
          if(!%mech.moduleID)
          {
               bottomPrint(%mech.client, "No module installed.", 5, 1);
               return;
          }

//          switch(%mech.moduleID)
//          {
//               default:
                    bottomPrint(%mech.client, "Module is passive.", 5, 1);
                    return;
//          }
     }
}

function MechHasAmmo(%mech, %slot)
{
     return $MechWeaponUsesAmmo[%obj.slotType[%slot]] ? (%mech.slotAmmo[%slot] ? true : false) : true;
}

function MechUseAmmo(%mech, %slot, %amount)
{
     %client = %mech.client;

     if(%mech.slotAmmo[%slot])
          %mech.slotAmmo[%slot] -= %amount;

     if(%mech.slotAmmo[%slot] < 0)
          %mech.slotAmmo[%slot] = 0;

     commandToClient(%client, 'setWeaponsHudAmmo', %slot, %mech.slotAmmo[%slot]);
}

// Weapon Triggers

function MechSlotCooldown(%obj, %slot)
{
   %obj.fireTimeout[%slot] = 0;
//   %obj.plal3D(%obj.slotReloadSound[%slot]);
//   %obj.play3d(StarHammerReloadSound);
}

function MechSetSlotCooldown(%obj, %slot, %time)
{
     %obj.fireTimeout[%slot] = %time;
     schedule(%time, %obj, MechSlotCooldown, %obj, %slot);
}

function fireSlotWeapon(%data, %obj, %slot, %type)
{
   if(%obj.fireTimeout[%slot])
      return;

     %time = getSimTime();
     
     if(%time < %obj.nextShutdownTime)
          return;

     if(!MechHasAmmo(%obj, %slot))
     {
          %obj.play3D(MortarDryFireSound);
          MechSetSlotCooldown(%obj, %slot, $MechWeaponReloadTime[%type]);
//          %obj.setImageTrigger(%slot, false);		// -soph
          return;
     }

     %energy = %obj.getEnergyLevel() ;			// +[soph]
     %cost = $MechWeaponEnergyCost[ %type ] / 1.75 ;	// +
     if( %energy < %cost )				// +
     {							// +
        %obj.play3D( ShockLanceMissSound ) ;		// +
        return;						// +
     }							// +
     %obj.setEnergyLevel( %energy - %cost ) ;		// +[/soph]

     eval("MechW"@$MechWeaponClass[%type]@"::fire("@%data@", "@%obj@", "@%slot@");");

     %obj.setImageTrigger(%slot, true);
     MechUseAmmo(%obj, %slot, 1);
     MechSetSlotCooldown(%obj, %slot, $MechWeaponFireTime[%type]);
     MechAddHeat(%obj, $MechWeaponHeat[%type]);
     %obj.setImageTrigger(%slot, false);
}

function MechStaggerFire(%obj, %slot, %time, %count, %type)
{
     for(%i = 0; %i < %count; %i++)
          schedule(%i*%time, %obj, MechFireStaggered, %obj, %slot, %time, %i , %type ) ;	// %time, %count, %type); -soph
}

function MechFireStaggered(%obj, %slot, %time, %count, %type)
{
     switch(%type)
     {
          case $MechWeapon::None:
               return;

          case $MechWeapon::SRM4:
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 32);
          
               %p = new SeekerProjectile()
               {
                   dataBlock        = SRM4Missile;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
          
               %target = %obj.getLockedTarget();
          
               if(%target)
               {
                    if(%target.getDatablock().jetfire || %target.getDatablock().MegaDisc)
                         assignTrackerTo(%target, %p);
                    else
                         %p.schedule(500, setObjectTarget, %target);
               }
               else if(%obj.isLocked())
                    %p.setPositionTarget(%obj.getLockedPosition());
               else
                    %p.setNoTarget();
          //     %p.proxyThread = true;
          //     schedule(1000, %p, MMBeginProxy, %p);
               MechUseAmmo(%obj, %slot, 1);
               %obj.play3d(MILFireSound);
               MechAddHeat( %obj , $MechWeaponHeat[ %type ] );				// +soph

          case $MechWeapon::SRM6:
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 32);
          
               %p = new SeekerProjectile()
               {
                   dataBlock        = SRM4Missile;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
          
               %target = %obj.getLockedTarget();
          
               if(%target)
               {
                    if(%target.getDatablock().jetfire || %target.getDatablock().MegaDisc)
                         assignTrackerTo(%target, %p);
                    else
                         %p.schedule(500, setObjectTarget, %target);
               }
               else if(%obj.isLocked())
                    %p.setPositionTarget(%obj.getLockedPosition());
               else
                    %p.setNoTarget();
          
               MechUseAmmo(%obj, %slot, 1);
               %obj.play3d(MILFireSound);
               MechAddHeat( %obj , $MechWeaponHeat[ %type ] );				// +soph
          
          case $MechWeapon::LRM5:
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 40);
          
               %p = new SeekerProjectile()
               {
                   dataBlock        = LRM5Missile;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
          
               %target = %obj.getLockedTarget();
          
               if(%target)
               {
                    if(%target.getDatablock().jetfire || %target.getDatablock().MegaDisc)
                         assignTrackerTo(%target, %p);
                    else
                         %p.schedule(500, setObjectTarget, %target);
               }
               else if(%obj.isLocked())
                    %p.setPositionTarget(%obj.getLockedPosition());
               else
                    %p.setNoTarget();
          
               MechUseAmmo(%obj, %slot, 1);
               %obj.play3d(MechRocketFireSound);
               MechAddHeat( %obj , $MechWeaponHeat[ %type ] );				// +soph
          
          case $MechWeapon::LRM10:
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 40);
          
               %p = new SeekerProjectile()
               {
                   dataBlock        = LRM5Missile;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
          
               %target = %obj.getLockedTarget();
          
               if(%target)
               {
                    if(%target.getDatablock().jetfire || %target.getDatablock().MegaDisc)
                         assignTrackerTo(%target, %p);
                    else
                         %p.schedule(500, setObjectTarget, %target);
               }
               else if(%obj.isLocked())
                    %p.setPositionTarget(%obj.getLockedPosition());
               else
                    %p.setNoTarget();
          
               MechUseAmmo(%obj, %slot, 1);
               %obj.play3d(MechRocketFireSound);
               MechAddHeat( %obj , $MechWeaponHeat[ %type ] ) ;				// +soph

          case $MechWeapon::UltraAC10:
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 7);

               %p = new TracerProjectile()
               {
                   dataBlock        = AutoCannonBullet;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               schedule( 864 + getRandom( 6 ) * 32 , 0 , AtCCleanup , %p ) ;		// +soph

          case $MechWeapon::LPLAS:
               %p = new SniperProjectile()
               {
                   dataBlock        = MechPulseBeam;
                   initialDirection = %obj.getMuzzleVector(%slot);
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   damageFactor     = 0.5;
                   sourceSlot       = %slot;
                   noHeadshot       = true;                   
               };
               %p.setEnergyPercentage(0.0);
               MissionCleanup.add(%p);
          
               %obj.play3d(SniperRifleProjectileSound);
               MechAddHeat( %obj , $MechWeaponHeat[ %type ] ) ;				// +soph

          case $MechWeapon::PhaserCannon :						// +[soph]
               %vector = calcSpreadVector( %obj.getMuzzleVector( %slot ) , 7 ) ;	// +
               %p = new TracerProjectile()						// +
               {									// +
                   dataBlock        = PCRBolt ;						// +
                   initialDirection = %vector ;						// +
                   initialPosition  = %obj.getMuzzlePoint( %slot ) ;			// +
                   sourceObject     = %obj ;						// +
                   sourceSlot       = %slot ;           				// +
               };									// +
               MissionCleanup.add( %p ) ;						// +
               %obj.play3d( PCRFireSound ) ;						// +[/soph]

          default:
               return;
     }
}

function MMBeginProxy(%obj)
{
   if(isObject(%obj) && %obj.proxyThread)
   {
      InitContainerRadiusSearch(%obj.position, 50, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType);
      
      while((%int = ContainerSearchNext()) != 0)
      {
        if((%int.team || %int.client.team) != %obj.sourceObject.team)
           if(%int.getHeat() > 0.35)
           {
               %obj.setObjectTarget(%int);
               %proxyThread = false;
               break;
           }
      }

      schedule(100, %obj, MMBeginProxy, %obj);
   }
}

//function SRM4Missile::onExplode(%data, %proj, %pos, %mod)
//{
//     %proj.setNoTarget();
//     Parent::onExplode(%data, %proj, %pos, %mod);
//}

// Other Code

function MechDamageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %mineSC)
{
   %targetObject.lastDamageTime = getSimTime(); // observer damage hack

   if(%targetObject.invincible || %targetObject.getState() $= "Dead")
      return;

   //----------------------------------------------------------------
   // z0dd - ZOD, 6/09/02. Check to see if this vehcile is destroyed,
   // if it is do no damage. Fixes vehicle ghosting bug. We do not
   // check for isObject here, destroyed objects fail it even though
   // they exist as objects, go figure.
   if(%damageType == $DamageType::Impact)
      if(%sourceObject.getDamageState() $= "Destroyed")
         return;

   %targetClient = %targetObject.getOwnerClient();
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else
      %sourceClient = isObject(%sourceObject) ? %sourceObject.getOwnerClient() : 0;

   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if (%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if( %damageType == $DamageType::Suicide || ( %damageType == 0 && %amount > 1 ) )	// if(%damageType == $DamageType::Suicide) -soph
   {
      %sourceTeam = 0;
      %pilot = MechEjectPilot( %targetObject ) ;				// +[soph]
      %targetObject.mechLastEjectAttempt = getSimTime() + 5000 ;		// +
      %team = %pilot.team ;							// +
      %mechPosition = %targetObject.getPosition() ;				// +
      InitContainerRadiusSearch( %targetObject.getPosition() , 80 , $TypeMasks::StaticObjectType |
                                                                    $TypeMasks::StationObjectType ) ;
      while( ( %int = ContainerSearchNext() ) != 0 )				// +
      {										// +
         if( isObject( %int ) )							// +
         {									// +
            if( %int.mechPad )							// +
               %pad = %int ;							// +
            else if( %int.pad.mechPad )						// +
               %pad = %int.pad ;						// +
            else								// +
               continue ;							// +
										// +
            %padTeam = %pad.station.team ;					// +
            %position = %pad.getWorldBoxCenter() ;				// +
            %distance = getDistance3D( %mechPosition , %position ) ;		// +
            if( ( %closestPad && %closestDistance < %distance ) || %distance > 30 || ( %padTeam != 0 && %team != %padTeam ) )
               continue ;							// +
            %closestPad = %pad ;						// +
            %closestDistance = %distance ;					// +
         }									// +
      }										// +
      if( %closestPad )								// +
      {										// +
         %targetObject.startFade( 5000 , 0 , true ) ;				// +
         schedule( 5000 , 0 , refundMech , %targetObject , %closestPad , %pilot.client ) ;
         return ;								// +
      }										// +
      %pilot.applyImpulse( %pilot.getPosition , "0 0 5000" ) ;			// +
      if( %damageType != $DamageType::Suicide )					// +
      {										// +
         %pilot.scriptKill( 0 ) ;						// +
         schedule( 32 , %targetObject , MechGrenadeTriggerToggle , %targetObject.getDatablock() , %targetObject , true ) ;
         return ;								// +
      }										// +
      else									// +
         centerPrint( %pilot.client , "EJECTING!" , 3 , 1 ) ;			// +
   }										// +[/soph]
   //--------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/8/02. Check to see if this turret has a valid owner, if not clear the owner varible.
   //else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   //   %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      if(%sourceObject.owner !$="" && (%sourceObject.owner.team != %sourceObject.team || !isObject(%sourceObject.owner)))
         %sourceObject.owner = "";
   }
   // End z0dd - ZOD
   //--------------------------------------------------------------------------------------------------------------------
   else if( isObject(%sourceObject) &&
   	( %sourceObject.getClassName() $= "FlyingVehicle" || %sourceObject.getClassName() $= "WheeledVehicle" || %sourceObject.getClassName() $= "HoverVehicle"))
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

//      if(%damageType == $DamageType::EMP && !%targetObject.isSyn)		// -soph
//           %targetObject.EMPObject();						// -soph
//      else if(%targetObject.isSyn && %damageType == $DamageType::EMP)
//         %targetObject.zapObject();

//   if(%targetObject.isShielded) // && (%damageType != $DamageType::MitziTransparent || %damageType != $DamageType::Blaster))
//      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);
   if( %targetObject.isShielded )									// +soph
      %amount = MechCalculateShields( %data , %targetObject , %position , %amount , %damageType ) ;	// +soph

   if(%amount == 0)
      return;
										// -[soph]
  // if(%damageType == $DamageType::Plasma) // || %damageType == $DamageType::FlameTurret || %damageType == $DamageType::Burn || %damageType == $DamageType::RAXX || %damageType == $DamageType::BurnLoop)
  //   if(%sourceObject != %targetObject)					// - removing this, replacement below
  //     MechAddHeat(%targetObject, 9);						// -
										// -
  // if(%damageType == $DamageType::RAXX || %damageType == $DamageType::FlameTurret)
  //   if(%sourceObject != %targetObject)					// -
  //     MechAddHeat(%targetObject, 3);						// -[/soph]

   if( getDamageGroup( %damageType ) & $DamageGroupMask::Plasma )		// +[soph]
   {										// +
      if( %damageType == $DamageType::Burn )					// +
         MechAddHeat(%targetObject, %amount * 50 ) ;				// +
      else if( %damageType == $DamageType::FlameTurret )			// +
         MechAddHeat(%targetObject, %amount * 25 ) ;				// +
      else if( %damageType == $DamageType::Plasma )				// +
         MechAddHeat(%targetObject, %amount * 12.5 ) ;				// +
      else if( %damageType == $DamageType::PlasmaCannon || $DamageType::RAXX )	// +
         MechAddHeat(%targetObject, %amount * 6.25 ) ;				// +
      else if( %damageType != $DamageType::BurnLoop )				// +
         MechAddHeat(%targetObject, %amount * 3.125 ) ;				// +
      if( %targetObject != %sourceObject )					// +
         %targetObject.LD_Brn_SrcObj = %sourceObject ;				// +
   }										// +
   if( %damageType != $DamageType::BurnLoop )					// +
      %amount -= %targetObject.armorWeight / 1000 ;				// + shrug code
   if( %amount <= 0 )								// +
      return ;									// +[/soph]

//   %damageScale = %data.damageScale[%damageType];
   %damageScale = $InheritDamageProfile[%damageType] > 0 ? %data.damageScale[$InheritDamageProfile[%damageType]] : %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

   if(%sourceObject.isMageIon)							// _here_ +soph
       MageRegisterHit(%sourceObject, %targetObject, %amount);			// +soph

   %amount /= %targetObject.resistMod;
   
//   if(%amount > 0)

   %flash = %targetObject.getDamageFlash() ;
   if( %flash < 0.27 )
   {
      %new = ( %amount / 5 ) + 0.05 ;
      if( %new < %flash )
         %new = %flash + 0.04 ;
      else if( %new > 0.33 )
         %new = 0.33 ;
      %targetObject.setDamageFlash( %new ) ;
   }

//   %flash = %targetObject.getDamageFlash() + (%amount * 2);			// -[soph]
//   if (%flash > 0.35)								// -
//      %flash = 0.35;								// -[/soph]

   %previousDamage = %targetObject.getDamagePercent();
//   %targetObject.setDamageFlash(%flash);					// -soph
   %targetObject.applyDamage(%amount);
   Game.onClientDamaged(%targetClient, %sourceClient, %damageType, %sourceObject);

   if( %targetObject != %sourceObject && %sourceObject > 0 )			// +soph
      %targetClient.lastDamagedBy = %sourceClient ;				// %damagingClient; -soph
   %targetClient.lastDamaged = getSimTime();

//   if(%sourceObject.isMageIon)						// ^up -soph
//       MageRegisterHit(%sourceObject, %targetObject, %amount);		// -soph

   //now call the "onKilled" function if the client was... you know...
   if( %targetObject.getState() $= "Dead" && !%targetObject.mechDead )		// (%targetObject.getState() $= "Dead") -soph
   {
      if(isObject(%sourceObject))
      {
           if( %sourceObject != %targetObject )					// +soph
           {
               addStatTrack(%sourceObject.client.guid, "mechkills", 1);
           
               if(%sourceObject.getDatablock().isBlastech && %sourceObject.getState() !$= "Dead")
               {
                   blastechRegisterKill(%sourceObject);
                   blastechRegisterKill(%sourceObject);               
               
                   if(!%sourceObject.client.isAIControlled())
                   {
                        %energyPct = %sourceObject.getEnergyPct();
                        %graph = createColorGraph("|", 35, %energyPct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mCeil(%sourceObject.getEnergyLevel()) SPC "KW"; // SPC %pct@"%";
                 
                        bottomPrint(%sourceObject.client, "BlasTech armor detected unit destruction, harvesting energy:" NL %graph, 3, 2);
                   }
               }

               if(%sourceObject.isMageIon && %sourceObject.getState() !$= "Dead" && %targetObject.player )
               {
                   MageRegisterKill(%sourceObject);
                   MageRegisterKill(%sourceObject);
               }               
           }
                      
          // if(%damageType == $DamageType::BellyTurret && %sourceObject.getType() & $TypeMasks::PlayerObjectType)	// -soph
          //     %sourceObject.useEnergy(%sourceObject.getDatablock().maxEnergy * -0.1);				// -soph
      }          
              
//      %targetObject.client.setWeaponsHudClearAll();
      %targetObject.setImageTrigger(0, false);
      %targetObject.mechDead = true;

//      if(%targetObject.fake)
//         %targetObject.sourceClient.setControlObject(%targetObject.sourceClient.player);

         %targetObject.ignoreDED = true;

         %targetObject.setMomentumVector(%momVec);
         loopDamageFX(%targetObject, 8, 56, 8, LinearFlareProjectile, ReaverCharge); //BAMiniExplodeCharge);
         %lastTime = 6000 ;												// +[soph]
         for( %i = 0 ; %i < getRandom( 3 , 5 ) ; %i++ )
         {
            %time = getRandom( 6000 , 8500 ) ;										// +[/soph]
            schedule( %time , 0, "TMSelfDestruct", %targetObject , false );						// schedule(getRandom(6000, 7000), 0, "TMSelfDestruct", %targetObject); -soph
            if( %time > %lastTime )											// +[soph]
               %lastTime = %time ;											// +
         }														// +
         %lastTime += getRandom( 0 , 500 ) ;										// +
         if( %targetObject.mechHeatLevel > $MechHeatLimit[%targetObject.mechID] )					// +
         {														// +
            for( %i = 0 ; %i < getRandom( 4 , 5 ) ; %i++ )								// +
            {														// +
               %time = getRandom( 2500 , %lastTime - 1000 ) ;								// +
               schedule( %time , 0, "TMSelfDestruct", %targetObject , false ) ;						// +
            }														// +
            schedule( %lastTime , 0 , "nukeMech" , %targetObject ) ;							// +
         }														// +
         else														// +
            schedule( %lastTime , 0 , MechShockwave , %targetObject , ETSmallWave , %targetObject.position , "0 0 2" ) ;
         schedule( %lastTime + 500 , 0 , "TMSelfDestruct" , %targetObject , true ) ;					// +
         schedule( 500 , 0 , trackerUpdateCheck , %targetObject ) ;							// +[/soph]
         MechShockwave(%targetObject, ETSmallWave, %targetObject.position, "0 0 2");
         %targetObject.mechDead = true;
         %targetObject.deathFiring = false;

      // where did this guy get it?
      %damLoc = %targetObject.getDamageLocation(%position);

            %targetObject.setMomentumVector(%momVec);
//            %targetObject.blowup();
//            %targetObject.blowedUp = true;

      // If we were killed, max out the flash
      %targetObject.setDamageFlash(0.75);
      %team = %targetObject.team;
      %tData = %targetObject.vData;
//      schedule(7.5 * 60 * 1000, 0, MechIncMaxCount, %team, %tData);							// -soph

      %points = 20 * %targetObject.totalWeight / %targetObject.baseWeight ;						// +[soph]
      %rem = ( %points * 10 ) % 10 ;											// +
      if( %rem )													// +
         if( %rem < 5 ) 												// +
            %points = mFloor( %points ) ;										// +
         else														// +
            %points = mCeil( %points ) ;										// +
															// +
      if( %team == 0 )													// +
         if( %pilot )													// +
         {														// +
            %name = %pilot.client.name ;										// +
            %sex = %pilot.client.sex $= "Male" ? 'his' : 'her' ;							// +
            messageAllExcept( %pilot.client, -1, 'msgMechKill', '\c5%1 destructed %2 own mech.' , %name , %sex ) ;	// +
         }														// +
         else if( %targetObject.originTeam == %sourceObject.team )							// +
         {														// +
            messageClient( %sourceClient , 'msgMechKill' , '\c0You recieved no bonus for destroying your team\'s abandoned mech.' , %points , %targetObject.vdata ) ;
            messageAllExcept( %sourceClient , -1 , 'msgMechTeamKill' , '\c5%1 destroyed an abandoned mech.' , %sourceClient.name ) ;
         }														// +
         else														// +
         {														// +
            %points = mCeil( %points * 3 / 4 ) ;									// +
            messageClient( %sourceClient , 'msgMechKill' , '\c0You received a %1 point bonus for destroying an abandoned enemy mech.' , %points , %targetObject.vdata ) ;            
            messageTeamExcept( %sourceClient , 'msgTeammateMechKill' , '\c5%1 destroyed an abandoned enemy mech.', %sourceClient.name , %targetObject.vdata ) ;
            messageTeam( %targetObject.pilot.team , 'MsgMechLost' , '\c5Enemy %1 destroyed your team\'s abandoned mech.' , %sourceClient.name , %targetObject.vdata ) ;
         }														// +
      else														// +
      {															// +
         if( %sourceObject == %targetObject )										// +
         {														// +
            %points = 0 ;												// +
            messageAllExcept( %sourceClient , -1 , 'msgMechTeamKill' , '\c5%1 selfdestructed.' , %sourceClient.name  ) ;
         }														// +
         else if( %team == %sourceClient.team )										// +
         {														// +
            %points = -1 * %points ;											// +
            messageClient( %sourceClient , 'msgMechKill' , '\c0You recieved a %1 point penalty for TEAMKILLING a mech.' , %points , %targetObject.vdata ) ;
            messageAllExcept( %sourceClient , -1 , 'msgMechTeamKill' , '\c5%1 TEAMKILLED a mech.' , %sourceClient.name  ) ;
         }														// +
         else 														// +
         {														// +
            messageClient( %sourceClient , 'msgMechKill' , '\c0You received a %1 point bonus for destroying an enemy mech.' , %points , %targetObject.vdata ) ;            
            messageTeamExcept( %sourceClient , 'msgTeammateMechKill' , '\c5%1 destroyed an enemy mech.', %sourceClient.name , %targetObject.vdata ) ;
            messageTeamExcept( %targetObject.client , 'MsgMechLost' , '\c5Enemy %1 destroyed your team\'s mech.' , %sourceClient.name , %targetObject.vdata ) ;
         }														// +
      }															// +
      %sourceClient.score += %points ;											// +
															// +
      if( %targetObject.client )											// +[/soph]
         Game.onClientKilled(%targetClient, %sourceClient, %damageType, %sourceObject, %damLoc);
      else														// +[soph]
      {															// +
         %targetObject.setRepairRate( 0 ) ;										// +
         playDeathAnimation( %targetObject , %damLoc , %damageType ) ;          					// +
         														// +
         if( isObject( %sourceClient.player ) && %sourceClient.player.getObjectMount() && %points > 0 )			// +
         {														// +
            for(%i = 0; %i < 8; %i++)											// +
            {														// +
               %passenger = %sourceClient.player.getObjectMount().getMountNodeObject( %i ) ;				// +
															// +
               if( isObject( %passenger.client ) && ( %passenger.client != %sourceClient ) )				// +
               {													// +
                  %passenger.client.score += %points ;									// +
                  %game.updateKillScores( 0 , %passenger.client , %damageType , %sourceObject ) ;			// +
               }													// +
            }														// +
         }														// +
         if( isObject( %sourceClient ) && %sourceClient.isAIControlled() )						// +
            Game.onAIKilledClient( 0 , %sourceClient , %damageType , %implement ) ;					// +
      }															// +
   }															// +[/soph]
}

function refundMech( %mech , %pad , %client )										// +[soph]
{															// +
	%armorPercent = %mech.getDamagePercent() ;									// +
	%weight = %mech.armorWeight * ( 1 - %armorPercent ) ;								// +
	%weight += ( %mech.totalWeight - %mech.armorWeight ) * ( 1 - ( %armorPercent * %armorPercent ) ) ;		// +
	%weight = mFloor( %weight ) ;											// +
	%pad.mechTons += %weight ;											// +
	messageClient( %client , 'MsgMechRecycle' , '\c2Mech recycled, %1 tons reclaimed.' , %weight ) ;		// +
	%mech.delete() ;												// +
}															// +[/soph]

function nukeMech( %targetObject )											// +[soph]
{															// + this is needed to get up-to-date position
   %position = %targetObject.getPosition() ;										// +
   beginNuke( %targetObject , %position ) ;										// +
   RadiusExplosion( %targetObject , %position , 150 , 6 , 25000 , %targetObject , $DamageType::Nuke ) ;			// +
   RadiusExplosion( %targetObject , %position , 75 , 24 , 35000 , %targetObject , $DamageType::Nuke ) ;			// +
}															// +[/soph]

function MechIncMaxCount(%team, %data)
{
     $VehicleTotalCount[%team, %data] -= 1;
     %plural = $VehicleTotalCount[%team, %data] == 1 ? "mech" : "mechs" ;
     messageTeam( %team , 'MsgMechRespawn', '\c5Your team now has %1 %2 %3 available.' , $VehicleMax[ %data ] - $VehicleTotalCount[ %team , %data ] , %data , %plural ) ;
}

function MechShockwave(%src, %option, %pos, %mod)
{
   if(isObject(%src))
   {
      %final = vectorAdd(%pos, %mod);
      
      %p = new LinearFlareProjectile()
      {
         dataBlock        = %option;
         initialDirection = "0 0 0";
         initialPosition  = %final;
         sourceObject     = %src;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
   }
}

function MechEnterLiquid(%data, %obj, %coverage, %type)
{
   switch(%type)
   {
      case 0:
          %obj.isWet = true;
      case 1:
          %obj.isWet = true;
      case 2:
          %obj.isWet = true;
      case 3:
          %obj.isWet = true;
      case 4:
         //Lava
          %obj.isLava = true;
      case 5:
         //Hot Lava
          %obj.isLava = true;
      case 6:
         //Crusty Lava
          %obj.isLava = true;
      case 7:
         //Quick Sand
   }
}

function MechExitLiquid(%data, %obj, %type)
{
   %obj.isWet = false;
   %obj.isLava = false;

   switch(%type)
   {
      case 0:
         //Water
      case 1:
         //Ocean Water
      case 2:
         //River Water
      case 3:
         //Stagnant Water
      case 4:
         //Lava
      case 5:
         //Hot Lava
      case 6:
         //Crusty Lava
      case 7:
         //Quick Sand
   }
}

function MechOnCollision(%this,%obj,%col,%forceVehicleNode)
{
   if (%obj.getState() $= "Dead" || (%obj.dead && $votePracticeModeOn))
      return;

   %dataBlock = %col.getDataBlock();
   %className = %dataBlock.className;
//   %client = %obj.client;					// -soph

   if( %col.isMounted() )					// +[soph]
      %vehicle = %col.getObjectMount() ;			// +
   else								// +
      %vehicle = %col;						// +
								// +   
   if( isObject( %vehicle ) && %vehicle.isVehicle() )		// +
   {								// +
      %mechCenter = %obj.getWorldBoxCenter() ;			// +
      %vehicleCenter = %vehicle.getWorldBoxCenter() ;		// +
      %vehicleBlock = %vehicle.getDataBlock() ;			// +
      %velocity = vectorLen( %col.getVelocity() ) ;		// +
      %vector = vectorNormalize( vectorSub( %vehicleCenter , %mechCenter ) ) ;
     // halt mech						// +
//      %impulse = vectorScale( %velocity , %this.mass - %vehicleBlock.mass ) ;
//      %obj.applyImpulse( %mechCenter , %impulse ) ;		// +
     // bounce vehicle 						// +
//      %impulse = vectorNormalize( vectorSub( %vehicleCenter , %mechCenter ) ) ;
//      %impulse = vectorNormalize( vectorAdd( %impulse , vectorNormalize( %velocity ) ) ) ;
//      %velocity = vectorLen( %velocity ) + 1 ;		// +
//      %impulse = vectorScale( %impulse , %velocity * %vehicleBlock.mass ) ;
//      %vehicle.applyImpulse( %vehicleCenter , %impulse ) ;	// +
//      %dataBlock.damageObject( %col , %obj , %mechFoot , %velocity * 0.05 , $damageType::Impact ) ;
								// +
     // halt mech						// +
      %impulse = vectorScale( %vector , %velocity * ( %this.mass - %vehicleBlock.mass ) ) ;
      %obj.applyImpulse( %mechCenter , %impulse ) ;		// +
     // bounce vehicle						// +
      %velocity++ ;						// +
      if( !%vehicle.inertialDampener )				// +
      {								// +
         %impulse = vectorScale( %vector , %velocity * %vehicleBlock.mass ) ;
         %vehicle.applyImpulse( %vehicleCenter , %impulse ) ;	// +
      }								// +
      if( %col.team == %obj.team )				// +
         %dataBlock.damageObject( %col , %col , %mechFoot , %velocity * 0.05 , $damageType::Impact ) ;
      else							// +
         %dataBlock.damageObject( %col , %obj , %mechFoot , %velocity * 0.05 , $damageType::MechStomp ) ;
   }								// +
   else								// +[/soph]
   if (%className $= "Armor")
   {
     // do something here, like kill the squishies!
      if( %col.isTacticalMech)					// +[soph]
         return ;						// +
      if( %col.getState() $= "Dead" )				// +
      {								// +
         if( !%col.blowedUp )					// +
         {							// +
            %vector = vectorAdd( vectorNormalize( %obj.getVelocity() ) , "0 0 -1" ) ;
            %col.setMomentumVector( %vector ) ;			// +
            %col.blowup() ;					// +
            %col.blowedUp = true ;				// +
         }							// +
         return ;						// +
      }								// +
      %playerCenter = getWord( %col.getWorldBoxCenter() , 2 ) ;	// +
      %playerFoot = getWord( %col.getPosition() , 2 ) ;		// +
      %playerHead = ( 2 * %playerCenter ) - %playerFoot ;	// +
      %mechCenter = getWord( %obj.getWorldBoxCenter() , 2 ) ;	// +
      %mechFoot = getWord( %obj.getPosition() , 2 ) ;		// +
      if( %playerFoot < %mechFoot + 1 )				// +
      {								// +
         %direction = vectorNormalize( vectorSub( %playerCenter , %mechFoot ) ) ;
         %damage = vectorLen( %obj.mech_lastVelocity ) ;	// +
         %impulse = vectorScale( %direction , ( %damage + 1 ) * %dataBlock.mass ) ;
         if( getWord( %direction , 2 ) < 0 )			// + stomp the squishie
            %damage = 0.125 + ( 0.5 * %damage ) ;		// +
         else							// + kick the squishie
            %damage *= 0.125 ;					// + 

         // DarkDragonDX: If its a practice mode ghost, scale up the actual impulse a little.
 	 if (%col.dead && $votePracticeModeOn)
	 	%impulse *= 2;

         %col.applyImpulse( %playerCenter , %impulse) ;		// +
         %dataBlock.damageObject( %col , %obj , %mechFoot , %damage , $damageType::MechStomp ) ;
         %obj.mech_lastVelocity = %obj.getVelocity() ;		// +
      }								// +
      else if( %obj.team == 0 && %playerCenter > %cockpitCenter )
      {								// +
         %time = getSimTime() ;					// +
         if( %obj.mechLastEjectAttempt + 2500 > %time )		// +
            return ;						// +
         %client = %col.client ;				// +
         if( %client.isAIControlled() )				// +
            return ;						// +
         if( %col.holdingFlag )					// +
         {							// +
            %flag = %col.holdingFlag ;				// +
            ejectFlag( %col ) ;					// +
            %flag.applyImpulse( %obj.getWorldBoxCenter() , "0 0 1500" ) ;
         }							// +
         %col.clearInventory() ;				// +
         %client.SetWeaponsHudClearAll() ;			// +
         %client.setAmmoHudCount( -1 ) ;			// +
         %col.setImageTrigger( 0 , false ) ;			// +
         %col.setImageTrigger( 1 , false ) ;			// +
         %col.setImageTrigger( 2 , false ) ;			// +
								// +
         %client.player = %obj ;				// +
         %obj.client = %client ;				// +
         %obj.team = %client.team ;				// +
         %obj.pilot = %client ;					// +
         %client.setControlObject( %client.player ) ;		// +
         %obj.setTarget( %client.target ) ;			// +
         							// +
         %col.setTarget( 0 ) ;					// +
         %col.client = 0 ;					// +
         %col.schedule( 32 , delete ) ;				// + 
   								// +
         %obj.setPassiveJammed( true ) ;			// + 
         for( %i = 2 ; %i < 2 + %obj.numWeapons ; %i++ )	// +
         {							// +
            commandToClient( %client , 'setWeaponsHudItem' , %i , 5 , 1 ) ;          
            commandToClient( %client , 'setWeaponsHudAmmo' , %i , %obj.slotAmmo[ %i ] ) ;     
            %client.setWeaponsHudActive( "Blaster" ) ;		// +
            %obj.setInventory( "Beacon" , %obj.getInventory( "Beacon" ) ) ;
            MechCycleWeapon( %this , %obj , "prev ") ;		// +
            MechCycleWeapon( %this , %obj , "adv ") ;		// +
         }							// +
         %client.camera.setTransform( %obj.getTransform() ) ;	// +
         %client.camera.getDataBlock().setMode( %client.camera , "mechOrbit" , %obj ) ;
         %client.camera.setOrbitMode( %obj , %obj.getTransform() , 0.75 , 9.5 , 9.5 ) ;
         %client.setControlObject( %client.camera ) ;		// +  
								// +
         if( %client.observeCount > 0 )				// +
            resetObserveFollow( %client , true ) ;		// +
      }								// +
      else							// +
      {								// +
         %direction = vectorNormalize( vectorSub( %playerCenter , %mechCenter ) ) ;
         %impulse = vectorScale( %direction , %dataBlock.mass ) ;
         %col.applyImpulse( %playerCenter , %impulse) ;		// +
      }								// +
   }								// +[/soph]
}

function verifyMechCount()
{
     if($VehicleMax[Patriot] > 2)
        $VehicleMax[Patriot] = 2;

     if($VehicleTotalCount[1, Patriot] < 0)
        $VehicleTotalCount[1, Patriot] = 0;

     if($VehicleTotalCount[2, Patriot] < 0)
        $VehicleTotalCount[2, Patriot] = 0;

     if($VehicleMax[Emancipator] > 2)
        $VehicleMax[Emancipator] = 2;

     if($VehicleTotalCount[1, Emancipator] < 0)
        $VehicleTotalCount[1, Emancipator] = 0;

     if($VehicleTotalCount[2, Emancipator] < 0)
        $VehicleTotalCount[2, Emancipator] = 0;
}

exec("scripts/modscripts/MDMechWeapons.cs");

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
