if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Tank Gun
//--------------------------------------------------------------------------

datablock GrenadeProjectileData(TFlakShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.5;
   damageRadius        = 30.0;
   radiusDamageType    = $DamageType::MortarTurret;
   kickBackStrength    = 5000;

   explosion           = "MortarExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;

   baseEmitter         = MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.25;
   grenadeFriction   = 0.4;
   armingDelayMS     = 3000;
   muzzleVelocity    = 86.42;
   drag              = 0.1;

   sound			 = MortarTurretProjectileSound;

   hasLight    = true;
   lightRadius = 3;
   lightColor  = "0.05 0.2 0.05";
};

datablock LinearFlareProjectileData(TFlakCharge)
{
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;	// 1.25; -soph
   damageRadius        = 22.5;	// 25; -soph
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 3500;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function TFlakCharge::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{								// + everything here from the starburst handler
   %source = %proj.bkSourceObject ;				// + tweaked search radius, seed count, etc
   %team = %source.team ;					// +
								// +
   %count = 0 ;							// +
   InitContainerRadiusSearch( %pos , 45 , $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::ProjectileObjectType ) ;
   while( ( %int = ContainerSearchNext() ) != 0 )		// +
   {								// +
      if( %int.team != %team && ( %int.getType() & $TypeMasks::VehicleObjectType || %int.getType() & $TypeMasks::PlayerObjectType ) && %int.getHeat() > 0.5 )
      {								// +
         %playerArray[ %count ] = %int;				// +
         %count++ ;						// +
      }								// +
      else if( %int.getClassName() $= "FlareProjectile" )	// +
      {								// +
         %playerArray[ %count ] = %int ;			// +
         %count++ ;						// +
      }								// +
   }								// +
								// +
   %seed = 24 ;							// + 34 ;
   if(!%count )							// +
   {								// +
      %spread = 3.1415926 ;					// +
      %vector = "0 0 1" ;					// +
      %FFRObject = new StaticShape()				// +
      {								// +
         dataBlock        = mReflector ;			// +
      } ;							// +
      MissionCleanup.add( %FFRObject ) ;			// +
      %FFRObject.setPosition( %pos ) ;				// +
      %FFRObject.schedule( 32 , delete ) ;			// +
								// +
      for( %i = 0 ; %i < %seed ; %i++ )				// +
      {								// +
         %x = (getRandom() - 0.5) * 2 * 3.1415926 * %spread ;	// +
         %y = (getRandom() - 0.5) * 2 * 3.1415926 * %spread ;	// +
         %z = (getRandom() - 0.5) * 2 * 3.1415926 * %spread ;	// +
         %mat = MatrixCreateFromEuler( %x @ " " @ %y @ " " @ %z ) ;
         %vector = MatrixMulVector( %mat , %vector ) ;		// +
         %p = new TracerProjectile()				// +
         {							// +
            dataBlock        = "ChaingunBullet" ;		// +
            initialDirection = %vector ;			// +
            initialPosition  = %pos ;				// +
            sourceObject     = %FFRObject ;			// +
            sourceSlot       = 0 ;				// +
            vehicleObject    = 0 ;				// +
            bkSourceObject   = %source ;			// +
            starburstSpawned = true ;				// +
         } ;							// +
         MissionCleanup.add( %p ) ;				// +
         %p.vehicleMod = %projectile.vehicleMod ;		// +
         %p.damageMod = %projectile.damageMod ;			// +
      }								// +
   }								// +
   else								// +
   {								// +
      %remainder = %seed % ( %count + 1 ) ;			// +
      %seed = mFloor( %seed / ( %count + 1 ) ) ;		// +
      %remainder += %seed ;					// +
      for( %idx = 0 ; %idx < %count ; %idx++ )			// +
      {								// +
         %FFRObject = new StaticShape()				// +
         {							// +
            dataBlock        = mReflector ;			// +
         } ;							// +
         MissionCleanup.add( %FFRObject ) ;			// +
         %FFRObject.setPosition( %pos ) ;			// +
         %FFRObject.schedule( 32 , delete ) ;			// +
								// +
         for( %i = 0 ; %i < %seed ; %i++ )			// +
         {							// +
            %vect = getVectorFromPoints( %pos , %playerArray[ %idx ].getWorldBoxCenter() ) ;
            %vector = vectorScale( calcSpreadVector( %vect , 72 ) , 10 ) ;
            %p = new TracerProjectile()				// +
            {							// +
               dataBlock        = "ChaingunBullet" ;		// +
               initialDirection = %vector ;			// +
               initialPosition  = %pos ;			// +
               sourceObject     = %FFRObject ;			// +
               sourceSlot       = 0 ;				// +
               vehicleObject    = 0 ;				// +
               bkSourceObject   = %source ;			// +
               starburstSpawned = true ;			// +
            } ;							// +
            MissionCleanup.add( %p ) ;				// +
            %p.vehicleMod = %projectile.vehicleMod ;		// +
            %p.damageMod = %projectile.damageMod ;		// +
         }							// +
      }								// +
      for( %i = 0 ; %i < %remainder ; %i++ )			// + random burnoff
      {								// +
         %vector = vectorScale( calcSpreadVector( "0 0 1" , 1000 ) , 10 ) ;
         %p = new TracerProjectile() 				// +
         {							// +
            dataBlock        = "ChaingunBullet" ;		// +
            initialDirection = %vector ;			// +
            initialPosition  = %pos ;				// +
            sourceObject     = %FFRObject ;			// +
            sourceSlot       = 0 ;				// +
            vehicleObject    = 0 ;				// +
            bkSourceObject   = %source ;			// +
            starburstSpawned = true ;				// +
         } ;							// +
         MissionCleanup.add( %p ) ;				// +
         %p.vehicleMod = %projectile.vehicleMod ;		// +
         %p.damageMod = %projectile.damageMod ;			// +
      }								// +
   }								// +
   %seed = ( 24 + 6 ) * 32 ;					// + 14 ;
   for( %i = 6 * 32 ; %i < %seed ; %i += 32 )			// +
   {								// +
      %spread = 1000 / 1000 ;					// +
      %vector = "0 0 1" ;					// +
      %x = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %spread ;	// +
      %y = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %spread ;	// +
      %z = ( getRandom() - 0.5 ) * 2 * 3.1415926 * %spread ;	// +
      %mat = MatrixCreateFromEuler( %x @ " " @ %y @ " " @ %z ) ;
      %vector = MatrixMulVector( %mat , %vector ) ;		// +
      %p = new LinearProjectile()				// +
      {								// +
         dataBlock        = "ReaverRocket" ;			// +
         initialDirection = %vector ;				// +
         initialPosition  = %pos ;				// +
         sourceObject     = %FFRObject ;			// +
         sourceSlot       = 0 ;					// +
         vehicleObject    = 0 ;					// +
         bkSourceObject   = %source ;				// +
         starburstSpawned = true ;				// +
      } ;							// +
      MissionCleanup.add( %p ) ;				// +
      %p.vehicleMod = %projectile.vehicleMod ;			// +
      %p.damageMod = %projectile.damageMod ;			// +
      schedule( %i , 0 , rBeginProxy , %p ) ;			// +
   }								// +
   Parent::onExplode( %data , %proj , %pos , %mod ) ;		// +
}								// +[/soph]

function TFlakTrigger(%obj)
{
   if(isObject(%obj))
   {
         if( %obj.exploded )					// +soph
            return;						// +soph
         %pos = %obj.getWorldBoxCenter();
         %FFRObject = new StaticShape()
         {
            dataBlock = mReflector;
         };
         MissionCleanup.add(%FFRObject);
         %FFRObject.setPosition(%pos);
         %FFRObject.schedule(32, delete);
         
      %p = new LinearFlareProjectile()
      {
         dataBlock        = TFlakCharge;
         initialDirection = "0 0 0";
         initialPosition  = %pos;
         sourceObject     = %FFRObject;
         sourceSlot       = 0;
         vehicleMod       = %obj.vehicleMod;
         bkSourceObject   = %obj.sourceObject;
         damageMod        = %obj.damageMod;
      };
      %p.client = %obj.client;	// +soph
      MissionCleanup.add(%p);

      %obj.delete();
   }
}

function tEnableFlak(%obj)
{
   if(!%obj.flakEnabled)
   {
//      TFlakTrigger(%obj.lastMortar);
      %obj.play3d(MortarReloadSound);
      %obj.flakEnabled = true;
   }
}

datablock TurretImageData(TankFlakMortarBarrel)
{
   shapeFile = "turret_tank_barrelmortar.dts"; 
   mountPoint = 0;

   usesEnergy = true;
   useMountEnergy = true;
   fireEnergy = 1;
   minEnergy = 50;
   offset = "-1 0 0" ;	// +soph
   useCapacitor = true;

   projectile = TFlakShot;
   projectileType = GrenadeProjectile;

   // Turret parameters
   activationMS                  = 1000;
   deactivateDelayMS             = 500;
   thinkTimeMS                   = 140;
   degPerSecTheta                = 600;
   degPerSecPhi                  = 1080;
   attackRadius                  = 75;

   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activate";
   stateSound[1]                 = MBLSwitchSound;
   stateTimeoutValue[1]          = 2;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.3;
   stateFire[3]                = true;
//   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
//   stateSequence[3]            = "Fire";
//   stateSound[3]               = AssaultMortarFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.05;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 2;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

function TankFlakMortarBarrel::onFire(%data, %obj, %slot)
{
   if(isObject(%obj.lastMortar))
   {
      TFlakTrigger(%obj.lastMortar);
      return;
   }

   if(!%obj.flakEnabled)
       return;

   %p = new GrenadeProjectile()
   {
      dataBlock        = "TFlakShot";
      initialDirection = %obj.getMuzzleVector(%slot);
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
      vehicleObject    = %obj;
   };

   %obj.lastMortar = %p;
   MissionCleanup.add(%p);

     %vehicle = %obj.getObjectMount();
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if(%obj.client)
   {					// +[soph]
      %p.client = %obj.client;		// + trace back to source for team info
      %obj.client.projectile = %p;
   }					// +[/soph]

    %obj.play3d(AssaultMortarFireSound);
    %obj.flakEnabled = false;
    %vehicle.turretObject.setCapacitorLevel(%vehicle.turretObject.getCapacitorLevel() - 50);
    schedule(2800, 0, "tEnableFlak", %obj);
}

datablock TurretImageData(TSFlakMortarBarrel) : TankFlakMortarBarrel
{
  // shapeFile                        = "weapon_plasma.dts";	// -soph
   shapeFile                        = "turret_mortar_large.dts";
   offset                           = "-0.1 -1 -0.45";			// "0 0 1"; +soph

   mountPoint = 0;
};

function TSFlakMortarBarrel::onFire(%data, %obj, %slot)
{
   if(isObject(%obj.lastMortar))
   {
      TFlakTrigger(%obj.lastMortar);
      return;
   }

   if(!%obj.flakEnabled)
       return;

   %p = new GrenadeProjectile()
   {
      dataBlock        = "TFlakShot";
      initialDirection = %obj.getMuzzleVector(%slot);
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
      vehicleObject    = %obj;
   };

   %obj.lastMortar = %p;
   MissionCleanup.add(%p);

     %vehicle = %obj.getObjectMount();
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if(%obj.client)
   {
      %p.client = %obj.client;	// +soph
      %obj.client.projectile = %p;
   }

    %obj.play3d(AssaultMortarFireSound);
    %obj.flakEnabled = false;
    %vehicle.turretObject.setCapacitorLevel(%vehicle.turretObject.getCapacitorLevel() - 50);
    schedule(2800, 0, "tEnableFlak", %obj);
}

function TankInstallFlakMortar(%data, %obj, %slot)
{
   // search for a vehicle in player's LOS
   %eyeVec = VectorNormalize(%obj.getEyeVector());
   %srchRange = VectorScale(%eyeVec, 5.0); // look 5m for a vehicle base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potVehicle = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType);

   if(%potVehicle != 0)    // positon of the vehicle
   {
      if(%potVehicle.getDatablock().getName() $= "AssaultVehicle") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugTFM', "\c2Changed Weapon barrel -> Tank -> Flak Mortar");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             TankMountFlakMortar(%data, %obj, %potVehicle);
      }
      else if(%potVehicle.getDatablock().getName() $= "WolfSideTurret") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugWFM', "\c2Changed Weapon barrel -> Retaliator -> Flak Mortar");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             WolfhoundMountFlakMortar(%data, %obj, %potVehicle);
      }
      else
      {
         messageClient(%obj.client, 'MsgAugCantAug', "\c2Error! You cannot change this vehicle's weapon using this module.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any vehicle
      messageClient(%obj.client, 'MsgAugORange', "\c2No vehicle within the 5m range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function TankMountFlakMortar(%datablock, %player, %vehicle)
{
     %turret = %vehicle.turretObject;

     %turret.unmountImage(0);
     %turret.unmountImage(4);

     %turret.flakEnabled = true;

     %turret.mountImage(AssaultTurretParam, 0);
     %turret.mountImage(TankFlakMortarBarrel, 4);

     %turret.weapon[2, Display] = true;
     %turret.weapon[2, Name] = "Flak Mortar";
     %turret.weapon[2, Description] = "Press 'Fire' again while the mortar is in flight to detonate.";
}

function WolfhoundMountFlakMortar(%datablock, %player, %vehicle)
{
     %turret = %vehicle;

     %turret.unmountImage(0);
     %turret.flakEnabled = true;
     %turret.mountImage(TankFlakMortarBarrel, 0, false);
}
