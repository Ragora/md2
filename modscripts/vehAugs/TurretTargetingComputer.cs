function SSToggleTargetingComputer(%vehicle, %turret)
{
     %client = %turret.getControllingClient();

     if(!%turret.tc_status)
     {
          %turret.targetingComputer = true;
          %turret.tc_status = true;
          %turret.play3D(PBWSwitchSound);
//          %turret.mountImage(AIAimingSeekingBarrel, 0, true);
          commandToClient(%client, 'BottomPrint', "Targeting Computer online.", 3, 1);
     }
     else
     {
          %turret.targetingComputer = false;
          %turret.tc_status = false;
          %turret.play3D(SynomiumOff);
//          %turret.mountImage(AIAimingTurretBarrel, 0, true);
          commandToClient(%client, 'BottomPrint', "Targeting Computer offline.", 3, 1);
     }
}
