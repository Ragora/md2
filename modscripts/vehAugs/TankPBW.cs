if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Tank Gun
//--------------------------------------------------------------------------

datablock TurretImageData(TankPBWBarrel)
{
   shapeFile = "turret_elf_large.dts" ;		// = "turret_tank_barrelmortar.dts"; -soph
   mountPoint = 0;

   usesEnergy = true;
   useMountEnergy = true;
   fireEnergy = 1;
   minEnergy = 250 ;				// = 150; -soph
   offset = "-0.5 0 0.5" ;			// +soph
   useCapacitor = true;

   // Turret parameters
   activationMS                  = 1000;
   deactivateDelayMS             = 500;
   thinkTimeMS                   = 140;
   degPerSecTheta                = 600;
   degPerSecPhi                  = 1080;
   attackRadius                  = 75;

   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activate";
   stateSound[1]                 = MBLSwitchSound;
   stateTimeoutValue[1]          = 2;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 6.0 ;		// = 3.0; -soph
   stateFire[3]                = true;
//   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
//   stateSequence[3]            = "Fire";
//   stateSound[3]               = AssaultMortarFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.05;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 2;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

function TankPBWBarrel::onFire(%data, %obj, %slot)
{
     %dmgMod = 1;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %dmgMod = 1 * %vehicle.damageMod ;					// = 1 + %vehicle.damageMod; -soph


       if(%obj.getCapacitorLevel() < 250 )				// < 150) -soph
       {
         serverPlay3D(ShockLanceDryFireSound, %obj.getTransform());
         return;
       }

     %t = new TargetProjectile()
      {
             dataBlock        = "TPBWLaserProj";			// "PBWLaserProj"; -soph
             initialDirection = %obj.getMuzzleVector(%slot);
             initialPosition  = %obj.getMuzzlePoint(%slot);
             sourceObject     = %obj;
             sourceSlot       = %slot;
             vehicleObject    = 0;
      };
      MissionCleanup.add(%t);

     // %p = new SniperProjectile() {					// -[soph]
     //    dataBlock        = "ParticleBeam";				// - now uses TPBWFragment loop
     //    initialDirection = %obj.getMuzzleVector(%slot);		// -
     //    initialPosition  = %obj.getMuzzlePoint(%slot);		// -
     //    sourceObject     = %obj;					// -
     //    damageFactor     = 3.0*%dmgMod;				// -
     //    damageForce      = 8400*%dmgMod;				// -
     //    sourceSlot       = %slot;					// -
     // };								// -
      //%t was here -Nite-						// -
     // %obj.lastProjectile = %p;					// -
     // MissionCleanup.add(%p);						// -
     // serverPlay3D(underwaterDiscExpSound, %obj.getTransform());	// -[/soph]

     %obj.setCapacitorLevel( %obj.getCapacitorLevel() - 250 ) ;		// (%obj.getCapacitorLevel() - 150); -soph
      TPBWFragment( %data , %obj , %slot , 22 , %dmgmod , %t ) ;	// 16 up from 10 +soph
//      schedule(286, 0, "killit", %t);					// -soph
}

function TPBWFragment( %data , %obj , %slot , %fragments , %dmgmod , %t )	// +[soph]
{										// +
   %p = new SniperProjectile() {						// +
      dataBlock        = "TParticleBeam" ;					// +
      initialDirection = %obj.getMuzzleVector( %slot ) ;			// +
      initialPosition  = %obj.getMuzzlePoint( %slot ) ;				// +
      sourceObject     = %obj ;							// +
      damageFactor     = 0.25 * %dmgMod ;					// + 27.5 back down from 40 // less than standard pbw but lasts longer
      damageForce      = 950 * %dmgMod ;					// + 1000 down from 1500 // same force and greater range
      sourceSlot       = %slot ;						// +
   } ;										// +
										// +
   %obj.getObjectMount().getMountNodeObject( 0 ).setCollisionTimeout( %p ) ;	// +
										// +
   %obj.lastProjectile = %p ;							// +
   MissionCleanup.add( %p ) ;							// +
   serverPlay3D( underwaterDiscExpSound , %obj.getTransform() ) ;		// +
										// +
   // AI hook									// +
   if( %obj.client )								// +
      %obj.client.projectile = %p ;						// +
   if( %fragments-- > 0 )							// +
      schedule( 30 , 0 , TPBWFragment , %data , %obj , %slot , %fragments , %dmgmod , %t ) ;
   else										// +
      %t.delete() ;								// +
}										// +[/soph]

datablock TurretImageData(TSPBWBarrel) : TankPBWBarrel
{
   shapeFile                        = "turret_elf_large.dts" ;	// "weapon_plasma.dts"; -soph
   offset                           = "-0.1 -1 -0.45" ;		// "0 0 0"; +soph
   rotation                         = "0 1 0 180" ;		// +soph

   mountPoint = 0;
};

function TSPBWBarrel::onFire(%data, %obj, %slot)
{
     %dmgMod = 1;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %dmgMod = 1 + %vehicle.damageMod;


       if(%obj.getCapacitorLevel() < 150)
       {
         serverPlay3D(ShockLanceDryFireSound, %obj.getTransform());
         return;
       }

     %t = new TargetProjectile()
      {
             dataBlock        = "TPBWLaserProj";
             initialDirection = %obj.getMuzzleVector(%slot);
             initialPosition  = %obj.getMuzzlePoint(%slot);
             sourceObject     = %obj;
             sourceSlot       = %slot;
             vehicleObject    = 0;
      };
      MissionCleanup.add(%t);

     // %p = new SniperProjectile() {					// -[soph]
     //    dataBlock        = "TParticleBeam";				// now uses TPBWFragment loop
     //    initialDirection = %obj.getMuzzleVector(%slot);
     //    initialPosition  = %obj.getMuzzlePoint(%slot);
     //    sourceObject     = %obj;
     //    damageFactor     = 3.0*%dmgMod;
     //    damageForce      = 8400*%dmgMod;
     //    sourceSlot       = %slot;
     // };
      //%t was here -Nite-
     // %obj.lastProjectile = %p;
     // MissionCleanup.add(%p);
     // serverPlay3D(underwaterDiscExpSound, %obj.getTransform());	// -[/soph]

     %obj.setCapacitorLevel(%obj.getCapacitorLevel() - 150);
      TPBWFragment( %data , %obj , %slot , 17 , %dmgmod );		// +soph
      schedule(350, 0, "killit", %t);
}

function TankInstallPBW(%data, %obj, %slot)
{
   // search for a vehicle in player's LOS
   %eyeVec = VectorNormalize(%obj.getEyeVector());
   %srchRange = VectorScale(%eyeVec, 5.0); // look 5m for a vehicle base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potVehicle = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType);

   if(%potVehicle != 0)    // positon of the vehicle
   {
      if(%potVehicle.getDatablock().getName() $= "AssaultVehicle") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugTPBW', "\c2Changed Weapon barrel -> Tank -> PBW");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             TankMountPBW(%data, %obj, %potVehicle);
      }
      else if(%potVehicle.getDatablock().getName() $= "WolfSideTurret") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugTPBW', "\c2Changed Weapon barrel -> Retaliator -> PBW");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             WolfMountPBW(%data, %obj, %potVehicle);
      }
      else
      {
         messageClient(%obj.client, 'MsgAugCantAug', "\c2Error! You cannot change this vehicle's weapon using this module.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any vehicle
      messageClient(%obj.client, 'MsgAugORange', "\c2No vehicle within the 5m range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function TankMountPBW(%datablock, %player, %vehicle)
{
     %turret = %vehicle.turretObject;

     %turret.unmountImage(0);
     %turret.unmountImage(4);

     %turret.mountImage(AssaultTurretParam, 0);
     %turret.mountImage(TankPBWBarrel, 4);

     %turret.weapon[2, Display] = true;
     %turret.weapon[2, Name] = "Particle Beam Weapon (PBW)";
     %turret.weapon[2, Description] = "Starsiege technology at it's finest.";
}

function WolfMountPBW(%datablock, %player, %vehicle)
{
     %turret = %vehicle;

     %turret.unmountImage(0);

     %turret.mountImage(TankPBWBarrel, 0, false);
}
