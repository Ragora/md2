if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

function TankInstallCometCannon(%data, %obj, %slot)
{
   // search for a vehicle in player's LOS
   %eyeVec = VectorNormalize(%obj.getEyeVector());
   %srchRange = VectorScale(%eyeVec, 5.0); // look 5m for a vehicle base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potVehicle = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType);

   if(%potVehicle != 0)    // positon of the vehicle
   {
      if(%potVehicle.getDatablock().getName() $= "AssaultVehicle") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugTCC', "\c2Changed Weapon barrel -> Tank -> Comet Cannon");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             TankMountCometCannon(%data, %obj, %potVehicle);
      }
      else if(%potVehicle.getDatablock().getName() $= "WolfSideTurret") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugWCC', "\c2Changed Weapon barrel -> Retaliator -> Comet Cannon");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             WolfhoundMountCometCannon(%data, %obj, %potVehicle);
      }
      else
      {
         messageClient(%obj.client, 'MsgAugCantAug', "\c2Error! You cannot change this vehicle's weapon using this module.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any vehicle
      messageClient(%obj.client, 'MsgAugORange', "\c2No vehicle within the 5m range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function TankMountCometCannon(%datablock, %player, %vehicle)
{
     %turret = %vehicle.turretObject;

     %turret.unmountImage(0);
     %turret.unmountImage(4);

     %turret.mountImage(AssaultTurretParam, 0);
     %turret.mountImage(AssaultMortarTurretBarrel, 4);

     %turret.weapon[2, Display] = true;
     %turret.weapon[2, Name] = "Comet Cannon";
     %turret.weapon[2, Description] = "Fearsome explosive blast of energy.";
}

function WolfhoundMountCometCannon(%datablock, %player, %vehicle)
{
     %turret = %vehicle;

     %turret.unmountImage(0);

     %turret.mountImage(AssaultMortarTurretBarrel, 0, false);
}
