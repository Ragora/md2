if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Tank Gun
//--------------------------------------------------------------------------

//datablock TurretImageData(TankPumaLauncherBarrel)	// removed, datablock reclaimed -soph
//{
//   shapeFile = "turret_tank_barrelmortar.dts";
//   mountPoint = 0;
//
//   usesEnergy = true;
//   useMountEnergy = true;
//   fireEnergy = 1;
//   minEnergy = 75;
//   offset = "-0.5 0 0";	// +soph
//   useCapacitor = true;
//   
//   // Turret parameters
//   activationMS                  = 1000;
//   deactivateDelayMS             = 500;
//   thinkTimeMS                   = 140;
//   degPerSecTheta                = 600;
//   degPerSecPhi                  = 1080;
//   attackRadius                  = 75;
//
//   stateName[0]                  = "Activate";
//   stateTransitionOnNotLoaded[0] = "Dead";
//   stateTransitionOnLoaded[0]    = "ActivateReady";
//
//   stateName[1]                  = "ActivateReady";
//   stateSequence[1]              = "Activate";
//   stateSound[1]                 = MBLSwitchSound;
//   stateTimeoutValue[1]          = 2;
//   stateTransitionOnTimeout[1]   = "Ready";
//   stateTransitionOnNotLoaded[1] = "Deactivate";
//   stateTransitionOnNoAmmo[1]    = "NoAmmo";
//
//   stateName[2]                    = "Ready";
//   stateTransitionOnNotLoaded[2]   = "Deactivate";
//   stateTransitionOnTriggerDown[2] = "Fire";
//   stateTransitionOnNoAmmo[2]      = "NoAmmo";
//
//   stateName[3]                = "Fire";
//   stateTransitionOnTimeout[3] = "Reload";
//   stateTimeoutValue[3]        = 0.3;
//   stateFire[3]                = true;
////   stateRecoil[3]              = LightRecoil;
//   stateAllowImageChange[3]    = false;
////   stateSequence[3]            = "Fire";
////   stateSound[3]               = AssaultMortarFireSound;
//   stateScript[3]              = "onFire";
//
//   stateName[4]                  = "Reload";
//   stateTimeoutValue[4]          = 0.05;
//   stateAllowImageChange[4]      = false;
//   stateSequence[4]              = "Reload";
//   stateTransitionOnTimeout[4]   = "Ready";
//   stateTransitionOnNotLoaded[4] = "Deactivate";
//   stateTransitionOnNoAmmo[4]    = "NoAmmo";
//
//   stateName[5]                = "Deactivate";
//   stateSequence[5]            = "Activate";
//   stateDirection[5]           = false;
//   stateTimeoutValue[5]        = 2;
//   stateTransitionOnLoaded[5]  = "ActivateReady";
//   stateTransitionOnTimeout[5] = "Dead";
//
//   stateName[6]               = "Dead";
//   stateTransitionOnLoaded[6] = "ActivateReady";
//
//   stateName[7]             = "NoAmmo";
//   stateTransitionOnAmmo[7] = "Reload";
//   stateSequence[7]         = "NoAmmo";
//};

function TankPumaLauncherBarrel::onFire(%data, %obj, %slot)
{
   %vehicle = %obj.getObjectMount();

   if(%vehicle.turretObject.getCapacitorLevel() < 75)
   {
     %obj.play3d(MortarDryFireSound);
     return;
   }

   if(isObject(%obj.lastMortar))
   {
      SBFlakTrigger(%obj.lastMortar);
      return;
   }

   if(!%obj.flakEnabled)
       return;

   %vehicle = 0;

      %p = new GrenadeProjectile() {
         dataBlock        = "StarburstShell";
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

     %p.client = %obj.client;
//     %p.client.player = %obj.client.player;
//     %p.client.player.slot = %slot;
     %p.StarburstMode = 3;
//     %proj = %p.getDatablock();
     %obj.lastMortar = %p;

     %obj.play3d(StarburstFireSound);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.flakEnabled = false;
    schedule(2800, 0, "SBEnableFlak", %obj);
}

//datablock TurretImageData(TSPumaLauncherBarrel) : TankPumaLauncherBarrel
//{
//   shapeFile                        = "weapon_plasma.dts";
//   offset                           = "0 0 0";
//
//   mountPoint = 0;
//};

function TSPumaLauncherBarrel::onFire(%data, %obj, %slot)
{
   %vehicle = %obj.getObjectMount();

   if(%vehicle.turretObject.getCapacitorLevel() < 75)
   {
     %obj.play3d(MortarDryFireSound);
     return;
   }

   if(isObject(%obj.lastMortar))
   {
      SBFlakTrigger(%obj.lastMortar);
      return;
   }

   if(!%obj.flakEnabled)
       return;

   %vehicle = 0;

      %p = new GrenadeProjectile() {
         dataBlock        = "StarburstShell";
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

     %p.client = %obj.client;
//     %p.client.player = %obj.client.player;
//     %p.client.player.slot = %slot;
     %p.StarburstMode = 3;
//     %proj = %p.getDatablock();
     %obj.lastMortar = %p;

     %obj.play3d(StarburstFireSound);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.flakEnabled = false;
    schedule(2800, 0, "SBEnableFlak", %obj);
}

function TankInstallPumaLauncher(%data, %obj, %slot)
{
   // search for a vehicle in player's LOS
   %eyeVec = VectorNormalize(%obj.getEyeVector());
   %srchRange = VectorScale(%eyeVec, 5.0); // look 5m for a vehicle base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potVehicle = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType);

   if(%potVehicle != 0)    // positon of the vehicle
   {
      if(%potVehicle.getDatablock().getName() $= "AssaultVehicle") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugTPL', "\c2Changed Weapon barrel -> Tank -> Starburst Cannon");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             TankMountPumaLauncher(%data, %obj, %potVehicle);
      }
      else if(%potVehicle.getDatablock().getName() $= "WolfSideTurret") // Shrike Filter
      {
		   messageClient(%obj.client, 'MsgAugTPL', "\c2Changed Weapon barrel -> Retaliator -> Starburst Cannon");
		   serverPlay3D(AugmentVehicle, %potVehicle.getTransform());
             WolfMountPumaLauncher(%data, %obj, %potVehicle);
      }
      else
      {
         messageClient(%obj.client, 'MsgAugCantAug', "\c2Error! You cannot change this vehicle's weapon using this module.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any vehicle
      messageClient(%obj.client, 'MsgAugORange', "\c2No vehicle within the 5m range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function TankMountPumaLauncher(%datablock, %player, %vehicle)
{
     %turret = %vehicle.turretObject;

     %turret.unmountImage(0);
     %turret.unmountImage(4);

     %turret.mountImage(AssaultTurretParam, 0);
     %turret.mountImage(TankPumaLauncherBarrel, 4);

     %turret.flakEnabled = true;
     
     %turret.weapon[2, Display] = true;
     %turret.weapon[2, Name] = "Starburst Cannon";
     %turret.weapon[2, Description] = "A beautiful fireworks show... of death.";
}

function WolfMountPumaLauncher(%datablock, %player, %vehicle)
{
     %turret = %vehicle;

     %turret.unmountImage(0);

     %turret.mountImage(TankPumaLauncherBarrel, 0, false);
     
     %turret.flakEnabled = true;
}
