if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// MD2 External Datafile

// String Table
$ChangeWepTime = 3;

$InvBanList[DM, "ShieldGeneratorPack"] = 1;
$InvBanList[DM, "VehiclePadPack"] = 1;
$InvBanList[DM, "DefenderDesignator"] = 1;
$InvBanList[DM, "SlipstreamPack"] = 1;
$InvBanList[DM, "DetPack"] = 1;
$InvBanList[DM, "DeployableGeneratorPack"] = 1;
$InvBanList[DM, "DeployableInvHumanPack"] = 1;
$InvBanList[DM, "TurretBasePack"] = 1;
$InvBanList[DM, "ShieldBeaconPack"] = 1;
$InvBanList[DM, "TelePack"] = 1;
$InvBanList[DM, "BorgCubePack"] = 1;
$InvBanList[DM, "DefenseBasePack"] = 1;
$InvBanList[DM, "RepulsorBeaconPack"] = 1;
$InvBanList[DM, "TeamchatBeaconPack"] = 1;
$InvBanList[DM, "HoverBasePack"] = 1;
$InvBanList[DM, "LargeMine"] = 1;
$InvBanList[DM, "CloakMine"] = 1;
$InvBanList[DM, "EMPMine"] = 1;
$InvBanList[DM, "IncendiaryMine"] = 1;
$InvBanList[DM, "BlastWallPack"] = 1;
$InvBanList[DM, "BlastDoorPack"] = 1;
$InvBanList[DM, "DeployableSentryPack"] = 1;
$InvBanList[DM, "LaserSentryPack"] = 1;
$InvBanList[DM, "DeployableSentryPack"] = 1;

$InvBanList[Hunters, "ShieldGeneratorPack"] = 1;
$InvBanList[Hunters, "VehiclePadPack"] = 1;
$InvBanList[Hunters, "DefenderDesignator"] = 1;
$InvBanList[Hunters, "SlipstreamPack"] = 1;
$InvBanList[Hunters, "DetPack"] = 1;
$InvBanList[Hunters, "DeployableGeneratorPack"] = 1;
$InvBanList[Hunters, "DeployableInvHumanPack"] = 1;
$InvBanList[Hunters, "TurretBasePack"] = 1;
$InvBanList[Hunters, "DeployableSentryPack"] = 1;
$InvBanList[Hunters, "ShieldBeaconPack"] = 1;
$InvBanList[Hunters, "TelePack"] = 1;
$InvBanList[Hunters, "BorgCubePack"] = 1;
$InvBanList[Hunters, "DefenseBasePack"] = 1;
$InvBanList[Hunters, "RepulsorBeaconPack"] = 1;
$InvBanList[Hunters, "TeamchatBeaconPack"] = 1;
$InvBanList[Hunters, "HoverBasePack"] = 1;
$InvBanList[Hunters, "LargeMine"] = 1;
$InvBanList[Hunters, "CloakMine"] = 1;
$InvBanList[Hunters, "EMPMine"] = 1;
$InvBanList[Hunters, "IncendiaryMine"] = 1;
$InvBanList[Hunters, "BlastWallPack"] = 1;
$InvBanList[Hunters, "BlastDoorPack"] = 1;
$InvBanList[Hunters, "DeployableSentryPack"] = 1;
$InvBanList[Hunters, "LaserSentryPack"] = 1;

$InvBanList[Siege, "VectorPortPack"] = 1;
$InvBanList[Siege, "BlastWallPack"] = 1;
$InvBanList[Siege, "BlastDoorPack"] = 1;
$InvBanList[Siege, "EnergyPack"] = 0;
$InvBanList[Siege, "RepairPack"] = 0;
$InvBanList[Siege, "AmmoPack"] = 0;
$InvBanList[Siege, "CloakingPack"] = 0;
$InvBanList[Siege, "SensorJammerPack"] = 0;


$InvBanList[$CurrentMissionType, "Mortar"] = 0;

//--------------------------------------------------------------------------
// Flag definition
//--------------------------------------

datablock ShapeBaseImageData(FlagImage)
{
   shapeFile = "flag.dts";
   item = Flag;
   mountPoint = 2;
   offset = "0 0 0";

   mass = 0;
   noSpecialRegen = true;
   
   lightType = "PulsingLight";
   lightColor = "0.5 0.5 0.5 1.0";
   lightTime = "1000";
   lightRadius = "3";
   cloakable = false;
};

//--------------------------------------------------------------------------
// Loading GUI
//--------------------------------------

$MD2LoadScreenTip[0] = "Blastech armor is immune to explosives. Keep this in mind.";
$MD2LoadScreenTip[1] = "Magnetic Ion armor can fly high and is powered by souls.";
$MD2LoadScreenTip[2] = "Use your targeting laser! All armors have a special targeting laser.";
$MD2LoadScreenTip[3] = "Check the forums for guides, tips, updates and more!";
$MD2LoadScreenTip[4] = "Don't be afraid to ask for help, most of the experienced players will be glad to help you.";
$MD2LoadScreenTip[5] = "The VectorPort pack allows you to travel through common obstacles, such as Blast Walls.";
$MD2LoadScreenTip[6] = "The Sensor Jammer pack can uncloak nearby cloaked players.";
$MD2LoadScreenTip[7] = "Concussion Grenades can knock your current weapon and pack off as well as inhibit your movement.";
$MD2LoadScreenTip[8] = "The Battle Angel CSL depends on the currently equipped pack.";
$MD2LoadScreenTip[9] = "If you're poisoned or on fire, use a Health Kit. It will remove the effects.";
$MD2LoadScreenTip[10] = "Pressing your Beacon key will change the firing mode of your current weapon.";
$MD2LoadScreenTip[11] = "Mitzi boosters are great ways to get around quickly in almost any armor.";
$MD2LoadScreenTip[12] = "Be sure to use the boost on your Energy pack. It can save you from a sticky situation.";

$MD2LoadScreenTipCount = 12;

//------------------------------------------------------------------------------
function sendLoadInfoToClient(%client)
{
     if(%client.loadScreened)
          return;
          
     %client.loadScreened = true;
     
     // Switch screens if the option is selected
     if(%client.defaultLoadScreen)
          sendChatInfoToClient(%client);
     else
          sendModInfoToClient(%client);
     
//          schedule(1000, %client, messageClient, %client, 'MsgGameOver', "");     
}

function sendModInfoToClient(%client)
{
   %snd[0] = '~wfx/misc/hunters_horde.wav';
   %snd[1] = '~wfx/misc/nexus_cap.wav';
   %snd[2] = '~wfx/misc/switch_taken.wav';
   %snd[3] = '~wfx/misc/bounty_bonus.wav';
   %snd[4] = '~wfx/misc/diagnostic_on.wav';
   %snd[5] = '~wgui/objective_notification.wav';
   %snd[6] = '~wvoice/Training/Any/ANY.tip_now01.wav';
                     
   %sndCnt = 6;
   
   %launchSnd = %snd[getRandom(0, %sndCnt)];
   %tip = $MD2LoadScreenTip[getRandom(0, $MD2LoadScreenTipCount)];
   
   %singlePlayer = $CurrentMissionType $= "SinglePlayer";
   messageClient(%client, 'MsgLoadInfo', "", $CurrentMission, "", "");

   %nmis = "<font:verdana bold:12><color:33CCCC>* Mission: <color:FFFFFF>" SPC $MissionDisplayName SPC "("@$MissionTypeDisplayName@")";

   messageClient(%client, 'MsgLoadQuoteLine', %launchSnd, "<spush><font:sui generis:22><color:EEEE33><just:center>Meltdown <color:FFFFFF>2<spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<spush><font:times new roman:20><color:fe2322><just:center>31st Century Tactical Combat<spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");   
   messageClient(%client, 'MsgLoadQuoteLine', "", "<spush><font:verdana bold:16><color:33CCCC>Version: <color:FFFFFF>"@Meltdown.Version@" <color:33CCCC>Developer: <color:FFFFFF><a:PLAYER\tKeen>Keen</a><spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<spush><font:verdana bold:16><color:33CCCC>Code Contributors: <color:FFFFFF>-Nite-, SouthTown, SoLoFiRe, Sophisanimus<spop>");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<font:verdana bold:16><color:33CCCC>For more information, visit <color:ffffff><a:wwwlink	forums.radiantage.net>http://forums.radiantage.net</a>.");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<font:verdana bold:16><color:33CCCC>Press F2 to configure your account options, like spawn loadout.");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "<font:verdana bold:16><color:33CCCC>Tip: <color:FFFFFF>"@%tip );   
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");
   messageClient(%client, 'MsgLoadQuoteLine', "", "");

   // Send server info:
   messageClient( %client, 'MsgLoadRulesLine', "", "<font:verdana:16>" @ $Host::GameName, false );
   messageClient( %client, 'MsgLoadRulesLine', "", $Host::Info, false );
   messageClient( %client, 'MsgLoadRulesLine', "", %nmis );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Time limit: <color:FFFFFF>" @ $Host::TimeLimit, false );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Team damage: <color:FFFFFF>" @ ($TeamDamage ? "On" : "Off") );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Anti-baserape player count: <color:FFFFFF>" @ $Host::MD2::AntiBaseRapeCount );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:33CCCC>* Smurfs: <color:FFFFFF>" @ ($Host::NoSmurfs ? "No" : "Yes") );
   messageClient( %client, 'MsgLoadRulesLine', "", "<color:FFFFFF>" @ $Host::LoadScreenMessage, false );
   
   messageClient(%client, 'MsgLoadInfoDone');
}

function sendChatInfoToClient(%client)
{
   messageClient(%client, 'MsgGameOver', "");
   messageClient(%client, 'MsgClearDebrief', "");
   
   %snd[0] = '~wfx/misc/hunters_horde.wav';
   %snd[1] = '~wfx/misc/nexus_cap.wav';
   %snd[2] = '~wfx/misc/switch_taken.wav';
   %snd[3] = '~wfx/misc/bounty_bonus.wav';
   %snd[4] = '~wfx/misc/diagnostic_on.wav';
   %snd[5] = '~wgui/objective_notification.wav';
   %snd[6] = '~wvoice/Training/Any/ANY.tip_now01.wav';
                     
   %sndCnt = 6;
   
   %launchSnd = %snd[getRandom(0, %sndCnt)];
   %tip = $MD2LoadScreenTip[getRandom(0, $MD2LoadScreenTipCount)];
   
   %singlePlayer = $CurrentMissionType $= "SinglePlayer";
   messageClient(%client, 'MsgLoadInfo', "", $CurrentMission, "", "");

   %nmis = "<font:verdana bold:12><color:33CCCC>* Mission: <color:FFFFFF>" SPC $MissionDisplayName SPC "("@$MissionTypeDisplayName@")";

   // Server name
   messageClient(%client, 'MsgDebriefResult', "", '<just:center>%1', $Host::GameName);
   
   messageClient(%client, 'MsgLoadQuoteLine', %launchSnd, "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:sui generis:22><color:EEEE33><just:center>Meltdown <color:FFFFFF>2<spop>");   
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:times new roman:20><color:fe2322><just:center>31st Century Tactical Combat<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "");   
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:verdana bold:16><color:33CCCC>Version: <color:FFFFFF>"@Meltdown.Version@" <color:33CCCC>Developer: <color:FFFFFF><a:PLAYER\tKeen>Keen</a><spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<spush><font:verdana bold:16><color:33CCCC>Code Contributors: <color:FFFFFF>-Nite-, SouthTown, SoLoFiRe, Sophisanimus<spop>");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>For more information, visit <color:ffffff><a:wwwlink	forums.radiantage.net>http://forums.radiantage.net</a>.");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Press F2 to configure your account options, like spawn loadout.");
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "<font:verdana bold:16><color:33CCCC>Tip: <color:FFFFFF>"@%tip );   
   messageClient(%client, 'MsgDebriefAddLine', "", "");
   messageClient(%client, 'MsgDebriefAddLine', "", "");

   // Send server info:
   messageClient(%client, 'MsgDebriefAddLine', "", %nmis);
   messageClient(%client, 'MsgDebriefAddLine', "", $Host::Info);
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:FFFFFF>" @ $Host::LoadScreenMessage);
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Time limit: <color:FFFFFF>" @ $Host::TimeLimit, false );
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Team damage: <color:FFFFFF>" @ ($TeamDamage ? "On" : "Off"));
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Anti-baserape player count: <color:FFFFFF>" @ $Host::MD2::AntiBaseRapeCount );
   messageClient(%client, 'MsgDebriefAddLine', "", "<color:33CCCC>* Smurfs: <color:FFFFFF>" @ ($Host::NoSmurfs ? "No" : "Yes"));
}

//--------------------------------------------------------------------------
// Automatic onMount Weapon Info
//--------------------------------------

function showWeaponInfo(%this, %weapon)
{
   %this.setInventory("EcstacyCapacitor", 0); // Ecstacy bug.
   %this.ecstacy = false;
   %this.stopEcstacy = true;
//   %this.setInventory("PACCapacitor", 0); // PAC bug.
   %this.CL = false;
//   %this.RFLActive = false;
   %this.MultiFusorActive = false;

   %armor = %this.getArmorSize(); // MrKeen
   %mode = %this.client.mode[%weapon];

   if($DenyMode[%armor, %weapon, %mode])
   {
      directModifyWeaponMode(%this.client, 0);
      showWeaponInfo(%this, %weapon);
      return;
   }

   %weaponName = %weapon.pickupName;

   if(!%mode)
      %mode = 0;

   %line3 = $Host::MD2::NoFireModeText;
   %weaponData = $WeaponData[%weapon] !$= "" ? true : false;
   
   if($WeaponModeCount[%weapon]) // does it have modes?
      %line2 = "<color:42dbea>Total <color:99FC99>"@$WeaponModeCount[%weapon]+1@"<color:42dbea> modes. Current mode: "@%mode+1@" [<color:FC9999>"@$WeaponMode[%weapon, %mode]@"<color:42dbea>]";
   else if(%weaponData)
      %line2 = $WeaponData[%weapon];

   if( $WeaponDescription[%weapon] )		// +soph
      %line3 = $WeaponDescription[%weapon] ;	// +soph

   if(%weapon $= "EcstacyCannon")
     // commandToClient( %this.client, 'BottomPrint', "<font:times new roman:18>Now using "@%weaponName@"\n"@%line2@"\nThis weapon is a charging weapon. To charge, hold down the fire key", 5, 3);				// -soph
      %line3 = "This weapon is a charging weapon. To charge, hold down the fire key";	// +soph
   else if(%weapon $= "MBCannon")
     // commandToClient( %this.client, 'BottomPrint', "<font:times new roman:18>Now using "@%weaponName@"\n"@%line2@"\nThis weapon auto-charges. Ammo represents charge.", 5, 3);						// -soph
      %line3 = "This weapon auto-charges. Ammo represents charge.";			// +soph
   else if(%weapon $= "SniperRifle")
     // commandToClient( %this.client, 'BottomPrint', "<font:times new roman:18>Now using "@%weaponName@"\nThis weapon is a hybrid laser/sniper weapon.\nSimply change modes to select between laser and projectile.", 5, 3);	// -soph
      %line3 = "This weapon can fire both energy beams and solid shells.";		// +soph
//   else if(%this.client.hasMD2Client)
//        commandToClient(%this.client, 'DisplayWeapon', %weapon, %weaponName, %mode, %line3);
  // else										// -soph
  // {											// -soph
       if($WeaponModeCount[%weapon])
          commandToClient( %this.client, 'BottomPrint', "<font:times new roman:18>Now using "@%weaponName@"\n"@%line2@"\nTo change fire modes, press the beacon key.", 5, 3);
       else if(%weaponData)
          commandToClient( %this.client, 'BottomPrint', "<font:times new roman:18>Now using "@%weaponName@"\n"@%line2 NL %line3, 5, 3); // This should be interesting.....
       else // last... and most definitely least. Yeah and what huh?!
          commandToClient( %this.client, 'BottomPrint', "<font:times new roman:18>Now using "@%weaponName@"\n"@%line3, 3, 2); // This should be interesting.....
  // }											// -soph
}

function WeaponImage::onMount(%this,%obj,%slot)
{
   //MES -- is call below useful at all?
   //Parent::onMount(%this, %obj, %slot);
//   if(%obj.getClassName() !$= "Player")
//      return;
   if(%obj.getType() & $TypeMasks::PlayerObjectType)
   {
        //messageClient(%obj.client, 'MsgWeaponMount', "", %this, %obj, %slot);
        // Looks arm position
        if (%this.armthread $= "")
        {
           %obj.setArmThread(look);
        }
        else
        {
           %obj.setArmThread(%this.armThread);
        }

        // Initial ammo state
        %thisobject = %obj.getMountedImage($WeaponSlot);
// echo( %thisobject.getName() SPC isObject(%thisobject) SPC %thisobject.getName().tool );	// todo this might need to go =soph
        if(isObject(%thisobject) && !%thisobject.getName().tool)
           %obj.keychain[%obj.getArmorSize()] = 0;

        if(%thisobject.ammo !$= "")
           if (%obj.getInventory(%this.ammo))
              %obj.setImageAmmo(%slot,true);

        %obj.client.setWeaponsHudActive(%this.item);
        if(%obj.getMountedImage($WeaponSlot).ammo !$= "")
           %obj.client.setAmmoHudCount(%obj.getInventory(%this.ammo));
        else
           %obj.client.setAmmoHudCount(-1);
        /// findArmorMode(%obj);
        showWeaponInfo(%obj, %this.item);
     }
}

function WeaponImage::onUnmount(%this,%obj,%slot)
{
   %type = $TypeMasks::TurretObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType;

   if(%obj.getType() & %type)
     return;

   if( %obj.client )	// console spam fix -soph
   {
      %obj.client.setWeaponsHudActive(%this.item, 1);
      %obj.client.setAmmoHudCount(-1);
      commandToClient(%obj.client,'removeReticle');
   }
      // try to avoid running around with sniper/missile arm thread and no weapon
      %obj.setArmThread(look);

   Parent::onUnmount(%this, %obj, %slot);
}

function ShapeBase::cycleWeapon( %this, %data )
{
   if(!isObject(%this))
       return;
     
   if(%this.isTacticalMech)
      %this.getDatablock().cycleWeapon(%this, %data);

   if(%this.weaponSlotCount == 0)
      return;

   %slot = -1;
   if ( %this.getMountedImage($WeaponSlot) != 0 )
   {
      %wep = %this.getMountedImage($WeaponSlot);
//      echo(%wep SPC %wep.getName() SPC isObject(%wep) SPC isObject(%wep.item));
      
      // keen: temp test with cycleWeapon()
      if(!isObject(%wep)) // || !isObject(%wep.item))
          return;

      %curWeapon = !isObject(%wep.item) ? "" : %wep.item.getName();

      for ( %i = 0; %i < %this.weaponSlotCount; %i++ )
      {
         //error("curWeaponName == " @ %curWeaponName);
         if ( %curWeapon $= %this.weaponSlot[%i] )
         {
            %slot = %i;
            break;
         }
      }
   }

   if ( %data $= "prev" )
   {
      // Previous weapon...
      if ( %slot == 0 || %slot == -1 )
      {
         %i = %this.weaponSlotCount - 1;
         %slot = 0;
      }
      else
         %i = %slot - 1;
   }
   else
   {
      // Next weapon...
      if ( %slot == ( %this.weaponSlotCount - 1 ) || %slot == -1 )
      {
         %i = 0;
         %slot = ( %this.weaponSlotCount - 1 );
      }
      else
         %i = %slot + 1;
   }

   %newSlot = -1;
   while ( %i != %slot )
   {
      if ( %this.weaponSlot[%i] !$= ""
        && %this.hasInventory( %this.weaponSlot[%i] )
        && %this.hasAmmo( %this.weaponSlot[%i] ) )
      {
         // player has this weapon and it has ammo or doesn't need ammo
         %newSlot = %i;
         break;
      }

      if ( %data $= "prev" )
      {
         if ( %i == 0 )
            %i = %this.weaponSlotCount - 1;
         else
            %i--;
      }
      else
      {
         if ( %i == ( %this.weaponSlotCount - 1 ) )
            %i = 0;
         else
            %i++;
      }
   }

   if ( %newSlot != -1 )
      %this.use( %this.weaponSlot[%newSlot] );
}

function ShapeBase::selectWeaponSlot( %this, %data )
{
   if(%this.isTacticalMech)
      %this.getDatablock().cycleWeapon(%this);
      
   if ( %data < 0 || %data > %this.weaponSlotCount
     || %this.weaponSlot[%data] $= "" || %this.weaponSlot[%data] $= "TargetingLaser" )
      return;

   %this.use( %this.weaponSlot[%data] );
}

function BeaconObject::getTargetPoint(%obj)
{
     return %obj.position;
}

//--------------------------------------------------------------------------
// Vehicle Handling Code
//--------------------------------------
datablock AudioProfile(EngineAlertSound)
{
   filename    = "gui/vote_nopass.WAV";
   description = AudioExplosion3d;
   preload = true;
};

function VDUndo(%obj)
{
   %obj.vdOverride = false;
   %obj.dmgApplyImp = false;
}

function vDmgApplyImpulse(%obj)
{
   if(%obj.getDatablock().forceSensitive)
      return;

    %obj.dmgApplyImp = true;
    %lastPct = %obj.lastDmgPct;
    %pct = %obj.getDamagePct();

    if(%lastPct > %pct)
    {
       %obj.vdOverride = true;
       schedule(1000, 0, "VDUndo", %obj);
    }

    %obj.tp_sndCnt++;

    if(((%pct > 0.749 && %pct < 1) && (%obj.vdOverride == false || %obj.vdOverride $= "")))
    {
        %force = (%pct * 500) * getRandom(1);
        if(%force)
        {
            %nVec = vectorScale(vectorRandom(), %force);

            %seed = getRandom(100);
            if(%seed > 60 && %seed < 90)
                %obj.playThread(0, "maintainback");
            else if(%seed > 90)
                %obj.playThread(0, "maintainbot");
            else
                %obj.playThread(0, "activateback");

            %obj.applyImpulse(%obj.getTransform(), %nVec);
        }
        %obj.lastDmgPct = %pct;
        schedule(250, %obj, "vDmgApplyImpulse", %obj);

        if(%obj.tp_sndCnt >= 4)
        {
            %obj.tp_sndCnt = 0;
            %obj.play3D(EngineAlertSound);
        }
    }
    else
        %obj.stopThread(0);
}

function VehicleData::onDamage(%this,%obj) // Apples are delicious
{
   %damage = %obj.getDamageLevel();
   if (%damage >= %this.destroyedLevel)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         if(%obj.respawnTime !$= "")
            %obj.marker.schedule = %obj.marker.data.schedule(%obj.respawnTime, "respawn", %obj.marker);
         %obj.setDamageState(Destroyed);
      }
   }
   else
   {
      if(%obj.getDamageState() !$= "Enabled")
         %obj.setDamageState(Enabled);

        %pct = %obj.getDamagePct();

        if(%pct >= 0.6 && !%obj.dmgApplyImp)
            vDmgApplyImpulse(%obj);
   }
}

//******************************************************************************
//*   Static Shape  -  Functions                                               *
//******************************************************************************

function StaticShapeData::create(%block)
{
   %obj = new StaticShape()
   {
      dataBlock = %block;
   };
   return(%obj);
}

function ShapeBase::damage(%this, %sourceObject, %position, %amount, %damageType)
{
   %this.getDataBlock().damageObject(%this, %sourceObject, %position, %amount, %damageType);
}

function ShapeBaseData::damageObject(%data, %targetObject, %position, %sourceObject, %amount, %damageType)
{
}

function ShapeBaseData::onDestroyed(%data, %obj, %prevState)
{
//   if(hasAttachments(%obj))
//      destroyAllAttachments(%obj);
}

function StaticShapeData::onEnabled(%data, %obj, %prevState)
{
   if(%obj.isPowered())
   {
//      if(isObject(%obj.deadSmokeEmitter))
//         %obj.deadSmokeEmitter.delete();

      %data.onGainPowerEnabled(%obj);
   }

   Parent::onEnabled(%data, %obj, %prevState);
}

function StaticShapeData::onDisabled(%data, %obj, %prevState)
{
   if(%obj.isPowered() || (%data.className $= "Generator"))
   {
//      if(isObject(%obj.deadSmokeEmitter))
//         %obj.deadSmokeEmitter.delete();

//      %obj.deadSmokeEmitter = createEmitter(%obj.getWorldBoxCenter(), "LightDamageSmoke");
      %data.onLosePowerDisabled(%obj);
   }

   Parent::onDisabled(%data, %obj, %prevState);
}

function statusSmoke(%obj)
{
   if(isObject(%obj))
   {
      createLifeEmitter(%obj.getWorldBoxCenter(), "LightDamageSmoke", 1100);
      schedule(1000, 0, "statusSmoke", %obj);
   }
}

function StaticShapeData::onDamage(%this,%obj)
{
   // Set damage state based on current damage level
   %damage = %obj.getDamageLevel();
   if(%damage >= %this.destroyedLevel)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         %obj.setDamageState(Destroyed);
         // if object has an explosion damage radius associated with it, apply explosion damage

         if(isObject(%obj.deadSmokeEmitter))
            %obj.deadSmokeEmitter.delete();

         if(%this.expDmgRadius)
            RadiusExplosion(%obj, %obj.getWorldBoxCenter(), %this.expDmgRadius, %this.expDamage, %this.expImpulse, %obj, $DamageType::Explosion);
         %obj.setDamageLevel(%this.maxDamage);

         if(%obj.attachedToObject) //limit attachment explosive
            destroyAllAttachments(%obj);

         if(isObject(%obj.parent))
         {
            if( %obj.disassembled )
               %obj.parent.disassembled = true;
            %obj.parent.setDamageState(Destroyed);
         }

         if(%obj.relaySet)
            %obj.relaySet.remove(%obj);

         if(%obj.DESetSet)						// alterations to get the D+D working right - soph
	 {
            %obj.DESetSet.remove(%obj);
	    %obj.DESetMember = false;
	 }

         %obj.clearLoopingDamages();
	 %obj.ignoreDED = true;
         %obj.ignoreRepairThis = true;
	 %obj.inDEDField = false;					// don't think this matters for shapes, but better safe than sorry -soph
	 if ( %obj.getDatablock( ).getName( ) $= "generatorLarge" )
            schedule(4000, %obj, "resetRegen", %obj);			// supplanted by disabled/enabled below for all objects except gens -soph	// schedule(5000, %obj, "resetRegen", %obj); -soph
         
//          if(isObject(%obj.ownerShieldBeacon))			// obsolete -soph
//              %obj.ownerShieldBeacon.setDamageState(Destroyed);	// -soph
      }

//      %this.onStateDestroyed(%obj);
   }
   else
   {
      if(%damage >= %this.disabledLevel)
      {
         if(%obj.getDamageState() !$= "Disabled")
            %obj.setDamageState(Disabled);
      }
      else
      {
         if(%obj.getDamageState() !$= "Enabled")
	 {
            %obj.setDamageState(Enabled);
	    %obj.ignoreRepairThis = false;	// -soph
	    %obj.ignoreDED = false;	
	 }
      }
   }
}

function StaticShapeData::onStateDestroyed(%data, %obj)
{
}

function resetRegen(%obj)	// only used by gens -soph
{
   if(isObject(%obj))
   {
      %obj.ignoreRepairThis = false;
      %obj.ignoreDED = false;	// -soph
   }
}

function ShapeBaseData::checkShields(%data, %targetObject, %position, %amount, %damageType)
{
   if(!isObject(%targetObject))
      return 0;

//   %veh = %targetObject.getType() & $TypeMasks::VehicleObjectType ? true : false;

   if(%targetObject.shieldStrengthFactor $= "")
      %targetObject.shieldStrengthFactor = 1;

   if(%targetObject.shieldStrengthFactor == 0)
        return %amount;

   if(isObject(%targetObject.ownerShieldBeacon))
   {
      if(%targetObject.ownerShieldBeacon.isEMP)
         return %amount;

      if(!isObject(%targetObject.ownerShieldBeacon.linkedShieldSource))
          return %amount;

      if(!%targetObject.ownerShieldBeacon.linkedShieldSource.isPowered()) // holy crap lol
         return %amount;


      %normal = "0.0 0.0 1.0";
      %targetObject.playShieldEffect( %normal );
//     zapObject(%targetObject);
//      %targetObject.ownerShieldBeacon.playShieldEffect( %normal );
      // Apply damage directly to D+D
//      %targetObject.ownerShieldBeacon.linkedShieldSource.applyDamage(%amount * 0.2);
      %targetObject.ownerShieldBeacon.applyDamage(%amount);
      return 0;
   }

   if( %targetObject.basePowered && !%targetObject.basePoweredNow )	// for blue blasters, etc +[soph]
      return %amount;							// +[/soph]

   if(!%targetObject.disableIgnoreShield && $DamageGroup[%damageType] & $DamageGroupMask::IgnoreShield)
      return %amount;

   %amount *= %targetObject.shieldStrengthFactor;
   %energy = %targetObject.getEnergyLevel();
   %strength = %targetObject.usingHardener ? (%energy / %data.hardenedEnergyPerDamagePoint) : (%energy / %data.energyPerDamagePoint);
   %shieldScale = $InheritDamageProfile[%damageType] > 0 ? %data.shieldDamageScale[$InheritDamageProfile[%damageType]] : %data.shieldDamageScale[%damageType];

   if(%shieldScale $= "")
      %shieldScale = 1;

   if (%amount * %shieldScale <= %strength)
   {
      // Shield absorbs all
      %lost = %amount * %shieldScale * (%targetObject.usingHardener ? %data.hardenedEnergyPerDamagePoint : %data.energyPerDamagePoint);
      %energy -= %lost;
      %targetObject.setEnergyLevel(%energy);

      %normal = "0.0 0.0 1.0";
      %targetObject.playShieldEffect( %normal );

      return 0;
   }

   // Shield exhausted
   %targetObject.setEnergyLevel(0);
   return %amount - %strength / %shieldScale;
}

function antiBaseRapeInEffect()
{
//     if(getSimTime() < (5 * 60000)) 				// -[soph]
//          return true; 					// -
//     else if(getPlayerCount(true) < $Host::MD2::AntiBaseRapeCount)
//          return true; 					// -[/soph]

     if( Game.class $= "SiegeGame" || Game.class $= "DnDGame" )	// +[soph]
          return false ;					// +
     if( $voteBaseRapeOn )					// +
          return false ;					// +
     if( getPlayerCount( 1 ) < $Host::MD2::AntiBaseRapeCount )	// +
          return 1 ;						// +
     if( ( getSimTime() - $missionStartTime ) < ( 5 * 60000 ) )	// +
          return 2 ;						// +[/soph]
     else
          return false;
}

function StaticShapeData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %force, %parentCall)
{
//   error("StaticShapeData::damageObject( "@%data@", "@%targetObject.getClassName()@", "@%sourceObject@", "@%position@", "@%amount@", "@%damageType@", "@%momVec@" )");
   if(%data.ignoreAllDamage)
     return;

   // if this is a non-team mission type and the object is "protected", don't damage it. At least not today.
   if(%data.noIndividualDamage && Game.allowsProtectedStatics())
      return;

   // if this is a Siege mission and this object shouldn't take damage (e.g. vehicle stations) then damage it anyway.
   if(%data.noDamageInSiege && Game.class $= "SiegeGame")
      return;

   if(%targetObject.protectedByFFBase || %targetObject.isShieldBeacon)
      return;

   if(%data.unrapeable && !%targetObject.fake )	// (%data.unrapeable && antiBaseRapeInEffect()) -soph
   {
     %isRape = antiBaseRapeInEffect();		// +[soph]
     if( %isRape )
     {
        if( %isRape == 2 )
        {
           %time = 300 - mCeil( ( getSimTime() - $missionStartTime ) / 1000 ) ;
           if( %time > 60 )
           {
              %time = mFloor( %time / 60 ) ;
              if( %time == 1 )
                 %units = "minute." ;
              else
                 %units = "minutes." ;
           }
           else
              if( %time == 1 )
                 %units = "second." ;
              else
                 %units = "seconds." ;
           centerPrint( %sourceObject.client , "Anti-Baserape will expire in" SPC %time SPC "more" SPC %units , 5 , 1 );
        }
        else					// +[/soph]
           centerPrint(%sourceObject.client, "Anti-Baserape in effect until there are more than" SPC $Host::MD2::AntiBaseRapeCount SPC "non-bot players.", 5, 1);
        return;
     }
   }
//   if(isObject(%targetObject.parent) && !%parentCall)
//   {
//     echo("Damaged child -> parent called" SPC %targetObject SPC %targetObject.getDatablock().getName());
//      %targetObject.parent.getDatablock().damageObject(%targetObject.parent, %sourceObject, %position, %amount, %damageType, %force, true);
//      return;
//   }

   if(%sourceObject && %targetObject.isEnabled())
   {
      if(%sourceObject.client)
      {
        %targetObject.lastDamagedBy = %sourceObject.client;
        %targetObject.lastDamagedByTeam = %sourceObject.client.team;
//        %targetObject.damageTimeMS = GetSimTime();		// -soph
      }
      else
      {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamagedByTeam = %sourceObject.team;
//        %targetObject.damageTimeMS = GetSimTime();		// -soph
      }
      %targetObject.damageTimeMS = GetSimTime();		// +soph
   }

//   if(%targetObject.isExtension) // dont damage APE extensions
//      return;
//   else
//   {
//      if(%damageType == $DamageType::Burn) //$DamageType::Burn == 49
//         if(!isObject(%targetObject.ownerShieldBeacon))
//            %targetObject.BurnObject(%sourceObject);

//      if(%damageType == $DamageType::EMP && %amount > 0.05)	// -[soph]
//         if(!isObject(%targetObject.ownerShieldBeacon))	// emp is handled differently now
//            %targetObject.EMPObject();			// -[/soph]

//   }

   if(!isObject(%targetObject.ownerShieldBeacon))		// +[soph]
      if( %damageType == $DamageType::EMP )
         if( ( %amount * 100 ) > %targetObject.getEnergyLevel() )
            %targetObject.EMPObject();				// even though this shouldn't be used
         else							// might as well provide the logic in case it ever is
            %targetObject.setEnergyLevel( %targetObject.getEnergyLevel() - ( %amount * 100 ) );
								// +[/soph]

   // D+D enhancement damage reduction
   if( %targetObject.inDEDField )				// +soph
      %amount /= 1.5 ;						// +soph

   // Scale damage type & include shield calculations...
   if(%data.isShielded && !%targetObject.isEMP )		// || %targetObject.ownerShieldBeacon) -soph
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);

   if( %targetObject.inDEDField )				// +soph
      %amount /= 4 / 3 ;					// +soph

//   %damageScale = %data.damageScale[%damageType];
   %damageScale = $InheritDamageProfile[%damageType] > 0 ? %data.damageScale[$InheritDamageProfile[%damageType]] : %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

    if(%targetObject.universalResistFactor $= "")
         %targetObject.universalResistFactor = 1.0;
    
    %amount *= %targetObject.universalResistFactor;

    //if team damage is off, cap the amount of damage so as not to disable the object...
    // edit: stupid rule, go away before i do evil things
    if(!$TeamDamage)
    {
      //see if the object is being shot by a friendly
      // and make sure it exists not to cause console spam :( jeez Dynamix
      if(isObject(%sourceObject))
      {
         if(%sourceObject.getDataBlock().catagory $= "Vehicles")
             %attackerTeam = getVehicleAttackerTeam(%sourceObject);
         else %attackerTeam = %sourceObject.team;
      }
      else %attackerTeam = %sourceObject.team;

      if ((%targetObject.getTarget() != -1) && isTargetFriendly(%targetObject.getTarget(), %attackerTeam))
         if( %targetObject.getClassName() $= "BeaconObject" && %amount > 0.1 )	// targeting beacons are now teamkillable +soph
            %amount = 0.1;
         else
            %amount = 0;
    }

   // if there's still damage to apply
   if (%amount > 0)
      %targetObject.applyDamage(%amount);
   
   // Damage children
   %dmgLevel = %targetObject.getDamageLevel();
      
   if(!isObject(%targetObject.parent) && isObject(%targetObject.attachment[0]))
   {
      while(%targetObject.attachment[%idx] !$= "") // get the last used index
      {
         if(isObject(%targetObject.attachment[%idx]))
         {
              if(%targetObject.attachment[%idx].getType() & ($TypeMasks::GeneratorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::StationObjectType))
              {
                   %idx++;
                   continue;
              }

//              %obj.attachment[%idx].getDatablock().damageObject(%int, %sourceObject, %position, %amount, %damageType, %force);
              %targetObject.attachment[%idx].setDamageLevel(%dmgLevel);
         }

         %idx++;
      }   
   }
}

// Clearing up errors
function Lightning::getControllingClient()
{
     return 0;
}

function DefaultGame::onClientKilled(%game, %clVictim, %clKiller, %damageType, %implement, %damageLocation)
{
   %plVictim = %clVictim.player;
   %plKiller = %clKiller.player;
   %clVictim.plyrPointOfDeath = %plVictim.position;
   %clVictim.plyrDiedHoldingFlag = %plVictim.holdingFlag;
   %clVictim.waitRespawn = 1;
   %wasInVehicle = false;
   
   cancel( %plVictim.reCloak );
   cancel(%clVictim.respawnTimer);
    // DarkDragonDX: Don't want the Respawn Timer going off when you're "dead"
   if (!%clVictim.isAIControlled() && !$votePracticeModeOn && %damageType != 0)
  	 %clVictim.respawnTimer = %game.schedule(($Host::PlayerRespawnTimeout * 1000), "forceObserver", %clVictim, "spawnTimeout" );

   // reset the alarm for out of bounds
   if(%clVictim.outOfBounds)
      messageClient(%clVictim, 'EnterMissionArea', "");

   if (%damageType == $DamageType::Suicide)
      %respawnDelay = $Host::MD2::SuicideRespawnTime;
   else
      %respawnDelay = $Host::MD2::RespawnTime; //2;

   // DarkDragonDX: Same as above.
   if (!%clVictim.isAIControlled() && !$votePracticeModeOn && %damageType != 0)
   	%game.schedule(%respawnDelay*1000, "clearWaitRespawn", %clVictim);

   // if victim had an undetonated satchel charge pack, get rid of it
   if(%plVictim.thrownChargeId != 0)
      if(!%plVictim.thrownChargeId.kaboom)
      {
         %plVictim.thrownChargeId.startFade( 3000 , 0 , true );	// +soph
         %plVictim.thrownChargeId.schedule( 3000 , delete );	// .delete(); -soph
      }

   if(%plVictim.lastVehicle !$= "")
   {
      schedule(15000, %plVictim.lastVehicle,"vehicleAbandonTimeOut", %plVictim.lastVehicle);
      %plVictim.lastVehicle.lastPilot = "";
   }

   if(isObject(%clKiller.player) && %clKiller.player.isMounted())
         %wasInVehicle = true;   

   // unmount pilot or remove sight from bomber
   if(%plVictim.isMounted())
   {
      if(%plVictim.vehicleTurret)
         %plVictim.vehicleTurret.getDataBlock().playerDismount(%plVictim.vehicleTurret);
//      else							// -soph
//      {
         %plVictim.getDataBlock().doDismount(%plVictim, true);
         %plVictim.mountVehicle = false;
//      }							// -soph
   }

   if(%plVictim.inStation)
      commandToClient(%plVictim.client,'setStationKeys', false);
   %clVictim.camera.mode = "playerDeath";

   // reset who triggered this station and cancel outstanding armor switch thread
   if(%plVictim.station)
   {
      %plVictim.station.triggeredBy = "";
      %plVictim.station.getDataBlock().stationTriggered(%plVictim.station,0);
      if(%plVictim.armorSwitchSchedule)
         cancel(%plVictim.armorSwitchSchedule);
   }

   //Close huds if player dies...
   messageClient(%clVictim, 'CloseHud', "", 'inventoryScreen');
   messageClient(%clVictim, 'CloseHud', "", 'vehicleHud');
   commandToClient(%clVictim, 'setHudMode', 'Standard', "", 0);

   // $weaponslot from item.cs
   %plVictim.setRepairRate(0);

   if(%plVictim.deathFiring)
//      %plVictim.schedule(5000, "setImageTrigger", $WeaponSlot, false);	// -soph
      deathFiringLoop( %plVictim , 0 ) ;
   else
      %plVictim.setImageTrigger($WeaponSlot, false);

   playDeathAnimation(%plVictim, %damageLocation, %damageType);
   playDeathCry(%plVictim);

   %victimName = %clVictim.name;

   %game.displayDeathMessages(%clVictim, %clKiller, %damageType, %implement);
   %game.updateKillScores(%clVictim, %clKiller, %damageType, %implement);

   if(isObject(%implement))
      %vcl = %implement.getControllingClient();

//   echo(%vcl SPC %vcl.namebase);

//   if(isObject
   if(isObject(%vcl.player) && %vcl.player.getObjectMount())
   {
      for(%i = 0; %i < 8; %i++)
      {
         %passenger = %vcl.player.getObjectMount().getMountNodeObject(%i);

         if(isObject(%passenger.client) && (%passenger.client != %vcl))
         {
//            echo("slot "@%i@" client "@%passenger.client@"  name "@%passenger.client.namebase@" allocate points");
            %passenger.client.score += 10;
            %passenger.client.kills++;
            %game.updateKillScores(%clVictim, %passenger.client, %damageType, %implement);
         }
      }
   }

   // toss whatever is being carried, '$flagslot' from item.cs
   // MES - had to move this to after death message display because of Rabbit game type
   %idxStart = %plVictim.deathFiring ? 1 : 0;

   for(%index = %idxStart; %index < 8; %index++)
   {
      %image = %plVictim.getMountedImage(%index);
      if(%image)
      {
         if(%index == $FlagSlot)
            %plVictim.throwObject(%plVictim.holdingFlag);
         else
            %plVictim.throw(%image.item);
      }
   }

   // target manager update
   setTargetDataBlock(%clVictim.target, 0);
   setTargetSensorData(%clVictim.target, 0);

   // clear the hud
   %clVictim.SetWeaponsHudClearAll();
   %clVictim.SetInventoryHudClearAll();
   %clVictim.setAmmoHudCount(-1);

   // clear out weapons, inventory and pack huds
   messageClient(%clVictim, 'msgDeploySensorOff', "");  //make sure the deploy hud gets shut off
   messageClient(%clVictim, 'msgPackIconOff', "");  // clear the pack icon

   //clear the deployable HUD
   %plVictim.client.deployPack = false;
   cancel(%plVictim.deployCheckThread);
   deactivateDeploySensor(%plVictim);

   //if the killer was an AI...
   if (isObject(%clKiller) && %clKiller.isAIControlled())
      %game.onAIKilledClient(%clVictim, %clKiller, %damageType, %implement);

   // reset control object on this player: also sets 'playgui' as content
   serverCmdResetControlObject(%clVictim);

   // set control object to the camera
   %clVictim.player = 0;
//   %transform = %plVictim.getTransform();	// -soph

   //note, AI's don't have a camera...
   if (isObject(%clVictim.camera))
   {
      %transform = %plVictim.getEyeTransform() ;	// +[soph]
      if( %clVictim.deathFreeFly )			// +
         %clVictim.camera.setFlyMode() ;		// +
      else						// +[/soph]
//      %clVictim.camera.setTransform(%transform);	// -soph
         %clVictim.camera.setOrbitMode(%plVictim, %plVictim.getTransform(), 0.5, 4.5, 4.5);
   }

   //if the death was a suicide, prevent respawning for (defined) seconds...
   %clVictim.lastDeathSuicide = false;
   if (%damageType == $DamageType::Suicide)
   {
      addStatTrack(%clVictim.guid, "suicides", 1);      
      %clVictim.lastDeathSuicide = true;
      %clVictim.suicideRespawnTime = getSimTime() + ($Host::MD2::SuicideRespawnTime * 1000);
   }

   //used to track corpses so the AI can get ammo, etc...
   AICorpseAdded(%plVictim);

  // if(!%clKiller.isAIControlled())					// console spam report -soph
  //     addStatTrack(%clVictim.guid, "deaths", 1);			// let's put this in the next section -soph

   if(!%clVictim.isAIControlled())
   {
        if(%clVictim == %clKiller)
             addStatTrack(%clVictim.guid, "suicides", 1);
        else if( isObject( %clKiller ) )				// +[soph]
             if(!%clKiller.isAIControlled())				// + spams console without above check
             {								// + both would have to be players to track either stat
                  if( %clVictim.team == %clKiller.team &&$teamdamage )	// +
                  {							// +
                       addStatTrack( %clKiller.guid , "teamkills" , 1 ) ;
                       addStatTrack( %clVictim.guid , "teamkilled" , 1 ) ;
                  }							// +
                  else							// +
                  {							// +
                       addStatTrack( %clVictim.guid , "deaths" , 1 ) ;	// +[/soph]
                       addStatTrack(%clKiller.guid, "kills", 1);
                  }
             }
   }
   else
        addStatTrack(%clKiller.guid, "botkills", 1);
        
//   if(%wasInVehicle)
//        addStatTrack(%clKiller.guid, "vehiclekills", 1);

   //hook in the AI specific code for when a client dies
   if (%clVictim.isAIControlled())
   {
      aiReleaseHumanControl(%clVictim.controlByHuman, %clVictim);
      %game.onAIKilled(%clVictim, %clKiller, %damageType, %implement);
   }
   else
      aiReleaseHumanControl(%clVictim, %clVictim.controlAI);

   if(%clVictim.die1stPerson && $Host::MD2::Die1stPerson)
      schedule($Host::MD2::SuicideRespawnTime - 32, %clVictim, continueClientKilled, %game, %clVictim, %clKiller, %damageType, %implement, %damageLocation);
   else
      continueClientKilled(%game, %clVictim, %clKiller, %damageType, %implement, %damageLocation);

	// DarkDragonDX: Add Practice Mode Logic
	if ((($votePracticeModeOn && !%clVictim.isAIControlled()) || (%plVictim.client !$= "" && %plVictim.isTacticalMech && $votePracticeModeOn)) && %damageType != 0)
	{
		%player = new Player() { 
					team = %clVictim.team;
					target = %plVictim.target;
					datablock = "Light" @ %clVictim.sex @ %clVictim.race @ "Armor"; 
					client = %clVictim; 
					pdead = true;
					dead = false; };

		for(%j = 0; $ammoType[%j] !$= ""; %j++)
            		%player.inv[$ammoType[%j]] = 999;

		//%player.startFade(1,0,1);
		%player.setInventory("EnergyPack", 1, true);
		%player.setInventory("MBCannon", 1, true);
		%player.setInventory("MBCannonCapacitor", 25, true); // Force a fully charged mitzi to start
		%player.selectWeaponSlot(0);

		%player.setInvincible(true);
		%player.setWhiteout(1);

		if (%damageType == $DamageType::Nuke)
			%player.schedule(600, "setVelocity", "0 0 0");
		else
			%player.setVelocity(%plVictim.getVelocity());

		%player.setTransform(%plVictim.getTransform());

		%clVictim.setControlObject(%player);
		%clVictim.player = %player;

		bottomPrint(%clVictim, "You have been knocked out of the game. Return to home base and rearm.", 10, 1);

		// Add the ammo Box...
		if (!%plVictim.isTacticalMech)
		{
			%box = new Item() { position = %plVictim.getPosition(); datablock = AmmoPack; rotate = true; player_drop = true; };
			for(%j = 0; $ammoType[%j] !$= ""; %j++)
		    		%box.inv[$ammoType[%j]] = %plVictim.inv[$ammoType[%j]];
			%box.setVelocity(%plVictim.getVelocity());
			%box.schedule($CorpseTimeoutValue, "delete");

			%plVictim.schedule(32, "delete");
		}
	}
}

function continueClientKilled(%game, %clVictim, %clKiller, %damageType, %implement, %damageLocation)
{
   if (isObject(%clVictim.camera))
      %clVictim.setControlObject(%clVictim.camera);
}

function Armor::doDismount(%this, %obj, %forced)
{
   // This function is called by player.cc when the jump trigger
   // is true while mounted
   if (!%obj.isMounted())
      return;

   if(isObject(%obj.getObjectMount().shield))
      %obj.getObjectMount().shield.delete();

   commandToClient(%obj.client,'SetDefaultVehicleKeys', false);

   // Position above dismount point

   %pos    = getWords(%obj.getTransform(), 0, 2);
   %oldPos = %pos;
   %vec[0] = " 0  0  1";
   %vec[1] = " 0  0  1";
   %vec[2] = " 0  0 -1";
   %vec[3] = " 1  0  0";
   %vec[4] = "-1  0  0";
   %numAttempts = 5;
   %success     = -1;
   %impulseVec  = "0 0 0";
   if (%obj.getObjectMount().getDatablock().hasDismountOverrides() == true)
   {
      %vec[0] = %obj.getObjectMount().getDatablock().getDismountOverride(%obj.getObjectMount(), %obj);
      %vec[0] = MatrixMulVector(%obj.getObjectMount().getTransform(), %vec[0]);
   }
   else
   {
      %vec[0] = MatrixMulVector( %obj.getTransform(), %vec[0]);
   }

   %pos = "0 0 0";
   for (%i = 0; %i < %numAttempts; %i++)
   {
      %pos = VectorAdd(%oldPos, VectorScale(%vec[%i], 3));
      if (%obj.checkDismountPoint(%oldPos, %pos))
      {
         %success = %i;
         %impulseVec = %vec[%i];
         break;
      }
   }
   if (%forced && %success == -1)
   {
      %pos = %oldPos;
   }

   // hide the dashboard HUD and delete elements based on node
   commandToClient(%obj.client, 'setHudMode', 'Standard', "", 0);
   // Unmount and control body
   if(%obj.vehicleTurret)
      %obj.vehicleTurret.getDataBlock().playerDismount(%obj.vehicleTurret);
   %obj.unmount();
   if(%obj.mVehicle)
      %obj.mVehicle.getDataBlock().playerDismounted(%obj.mVehicle, %obj);

   // bots don't change their control objects when in vehicles
   if(!%obj.client.isAIControlled())
   {
      %vehicle = %obj.getControlObject();
      %obj.setControlObject(0);
   }

   %obj.mountVehicle = false;
   %obj.schedule(2000, "setMountVehicle", true);

   // Position above dismount point
   %obj.setTransform(%pos);
   %obj.playAudio(0, UnmountVehicleSound);
   %obj.applyImpulse(%pos, VectorScale(%impulseVec, %obj.getDataBlock().mass * 3));
   %obj.setPilot(false);
   %obj.vehicleTurret = "";
}

function ProjectileData::onExplode(%data, %proj, %pos, %mod)
{
   %proj.exploded = true;	// putting this everywhere it could possibly need to be +soph
   if(%proj.bkSourceObject)
      if(isObject(%proj.bkSourceObject))
         %proj.sourceObject = %proj.bkSourceObject;

     %modifier = 1;

   if(%data.hasDamageRadius) // I lost a bomb. Do you have it?
   {
      if(%proj.vehicleMod)
           %modifier *= %proj.vehicleMod;

      if(%data.passengerDamageMod && %proj.passengerCount)
          %modifier *= 1 + (%proj.passengerCount * %data.passengerDamageMod);

      if( %proj.damageMod > 0 )	// if(%proj.damageMod) -soph
          %modifier *= %proj.damageMod;
          
//      if(%reportDmg)
//          echo("R" SPC %modifier SPC "|" SPC (%data.indirectDamage * %modifier));
          
      RadiusExplosion(%proj, %pos, %data.damageRadius, %data.indirectDamage * %modifier, %data.kickBackStrength, %proj.sourceObject, %data.radiusDamageType);
   }
}

function fireNextWeapon(%obj, %slot1, %slot2)
{
   if(%obj.fireWeapon)
   {
      if(%obj.nextWeaponFire == %slot1)
      {
         %obj.setImageTrigger(%slot1, true);
         %obj.setImageTrigger(%slot2, false);
      }
      else
      {
         %obj.setImageTrigger(%slot1, false);
         %obj.setImageTrigger(%slot2, true);
      }
   }
   else
   {
      %obj.setImageTrigger(%slot1, false);
      %obj.setImageTrigger(%slot2, false);
   }
}

function fireNextGun(%obj)
{
   if(%obj.fireWeapon)
   {
      if(%obj.nextWeaponFire == 2)
      {
         %obj.setImageTrigger(2, true);
         %obj.setImageTrigger(3, false);
      }
      else if(%obj.nextWeaponFire == 3)
      {
         %obj.setImageTrigger(2, false);
         %obj.setImageTrigger(3, true);
      }
      else if(%obj.nextWeaponFire == 5)
      {
         %obj.setImageTrigger(5, true);
         %obj.setImageTrigger(6, false);
      }
      else if(%obj.nextWeaponFire == 6)
      {
         %obj.setImageTrigger(5, false);
         %obj.setImageTrigger(6, true);
      }
   }
   else
   {
      %obj.setImageTrigger(2, false);
      %obj.setImageTrigger(3, false);
      %obj.setImageTrigger(5, false);
      %obj.setImageTrigger(6, false);
   }
}

function fireNextWarp(%obj)
{
   if(%obj.fireWeapon)
   {
      if(%obj.nextWeaponFire == 2)
      {
         %obj.setImageTrigger(2, true);
         %obj.setImageTrigger(3, false);
         %obj.setImageTrigger(4, false);
      }
      else if(%obj.nextWeaponFire == 3)
      {
         %obj.setImageTrigger(2, false);
         %obj.setImageTrigger(3, true);
         %obj.setImageTrigger(4, false);
      }
      else if(%obj.nextWeaponFire == 4)
      {
         %obj.setImageTrigger(2, false);
         %obj.setImageTrigger(3, false);
         %obj.setImageTrigger(4, true);
      }
   }
   else
   {
      %obj.setImageTrigger(2, false);
      %obj.setImageTrigger(3, false);
      %obj.setImageTrigger(4, false);
   }
}

//--------------------------------------------------------------------------
//
//--------------------------------------

function shutdownServer(%time, %msg, %lines) // overloaded from functions.cs
{
     %left = %time*1000;
     flashWindowDynamicAll(%msg, 100, %lines, %left, true, "<color:0000FF>");
     schedule(%left, 0, "quit");
}

function DefaultGame::createPlayer(%game, %client, %spawnLoc, %respawn)
{
   // do not allow a new player if there is one (not destroyed) on this client
   if(isObject(%client.player) && (%client.player.getState() !$= "Dead"))
         return;

   // clients and cameras can exist in team 0, but players should not
   if(%client.team == 0)
   {
      error("Attempted to exploit team0 bug: "@%client.guid);
      return;
   }

   // defaultplayerarmor is in 'players.cs'
   if(%spawnLoc == -1)
      %spawnLoc = "0 0 300 1 0 0 0";
   //else
   //  echo("Spawning player at " @ %spawnLoc);

   // copied from player.cs
   if (%client.race $= "Bioderm")
      // Only have male bioderms.
      %armor = $DefaultPlayerArmor @ "Male" @ %client.race @ Armor;
   else
      %armor = $DefaultPlayerArmor @ %client.sex @ %client.race @ Armor;
   %client.armor = $DefaultPlayerArmor;

   // Delete clones on spawn
   if(isObject(%client.player))
     %client.player.delete();

   %player = new Player() {
      //dataBlock = $DefaultPlayerArmor;
      dataBlock = %armor;
   };


   if(%respawn)
   {
      %player.setInvincible(true);
      %player.setCloaked(true);
      %player.setInvincibleMode($InvincibleTime,0.02);
      %player.respawnCloakThread = %player.schedule($InvincibleTime * 1000, "setRespawnCloakOff");
      %player.schedule($InvincibleTime * 1000, "setInvincible", false);
   }

   %player.setTransform( %spawnLoc );
   MissionCleanup.add(%player);

   // setup some info
   %player.setOwnerClient(%client);
   %player.team = %client.team;
   %client.outOfBounds = false;
   %player.setEnergyLevel(60);
   %client.player = %player;

   // updates client's target info for this player
   %player.setTarget(%client.target);
   setTargetDataBlock(%client.target, %player.getDatablock());
   setTargetSensorData(%client.target, PlayerSensor);
   setTargetSensorGroup(%client.target, %client.team);
   %client.setSensorGroup(%client.team);
   clearCenterPrint(%client);
   clearBottomPrint(%client);

   //make sure the player has been added to the team rank array...
   %game.populateTeamRankArray(%client);

   %game.playerSpawned(%client.player);
}

datablock StaticShapeData(mReflector)
{
   shapeFile = "turret_muzzlepoint.dts";
   ignoreAllDamage = true;
};

$ProjectileTypeToIgnore[0] = "ELFProjectile";
$ProjectileTypeToIgnore[1] = "TargetProjectile";
$ProjectileTypeToIgnore[2] = "SeekerProjectile";
$ProjectileTypeToIgnore[3] = "RepairProjectile";
$ProjectileTypeToIgnore[4] = "SniperProjectile";

function isFFReflectableProjectile(%obj)
{
   if(%obj.ignoreReflect)
      return false;
     
   for(%i = 0; $ProjectileTypeToIgnore[%i] !$= ""; %i++)
   {
      if(%obj.getClassName() $= $ProjectileTypeToIgnore[%i])
         return false;
   }

   return true;
}

//FFReflectProjectile(%projectile, %targetObject, %normal, %source);

function FFReflectProjectile(%int, %ff, %normal, %src)
{
   if(isFFReflectableProjectile(%int))
   {
      if(%int.lastReflectedFrom == %ff)// --- (ST)
         return;

      if(%int.reflectCount > 8) //  seems to be the magic number.
         return;
         
      if(%int.ignoreReflections || %int.ignoreReflectionsFF)
         return;

      %FFRObject = new StaticShape()
      {
         dataBlock        = mReflector;
      };
      MissionCleanup.add(%FFRObject);
      %FFRObject.setPosition(%int.getPosition());

// --- Find reflection angle (ST)
      //%vector = vectorNeg(%int.initialDirection); // Cooking it off wont allow the enemy to pick it up and throw it right back at ya
      //%x = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
      //%y = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
      //%z = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
      //%mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      //%vector = MatrixMulVector(%mat, %vector);

      %pVec = VectorNormalize(%int.initialDirection);

      // --- Thanks to Mostlikely
//      %start = VectorSub(%int.getPosition(), %pVec);
//      %end = VectorAdd(%int.getPosition(), %pVec);
//      %surface = containerRayCast(%start, %end, $TypeMasks::ForceFieldObjectType, 0);
//      %normal =  normalFromRaycast(%surface);
      %flip = VectorDot(%normal, %pVec);
      %flip = VectorScale(%normal, %flip);
      %newVec = VectorAdd(%pVec, VectorScale(%flip, -2));
      // --- ...again... LOL (ST)
      %newPos = VectorAdd(%int.getPosition(), %newVec);
// --- End reflection angle (ST)

      if(%int.getClassName() $= "BombProjectile")
          %newVec = vectorScale(%newVec, 175);
               
//      if(!isObject(%int.sourceObject))
//          %src = %int.bkSourceObject;
//      else
//          %src = %int.sourceObject;
               
      if( %int.originTime )						// +[soph]
      {									// +
         %airTimeMod = getSimTime() - %int.originTime ; 		// +
         %data = %int.getDatablock().getName() ;			// +
         if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
            %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
         else								// +
            %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
         %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;	// +
      }									// +[/soph]

      %p = new(%int.getClassName())()
      {
         dataBlock        = %int.getDatablock().getName();
         initialDirection = %newVec;// --- (ST) %vector; //vectorNeg(%int.initialDirection);
         initialPosition  = %newPos;// --- (ST) VectorAdd(%int.getPosition(), %vector);
         sourceObject     = %FFRObject;
         sourceSlot       = %int.sourceSlot;
         reflectCount     = %int.reflectCount++;
         lastReflectedFrom = %ff; // --- (ST)
         bkSourceObject   = %int.sourceObject;
         starburstMode    = %int.starburstMode;
         damageMod        = %int.damageMod;
         vehicleMod       = %int.vehicleMod;
      };
      MissionCleanup.add(%p);

      %p.sourceObject = %int.sourceObject;

      if( %src.lastMortar == %int )					// if(%src.lastMortar !$= "") -soph
           %src.lastMortar = %p;

      %p.originTime = %int.originTime ;					// +soph

      if(%int.trailThread)
         projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);

      if(%int.proxyThread)
         rBeginProxy(%p);

       if(%int.blackholeThread)
          blackHoleLoop(%p);
          
      if(%int.lanceThread)
      {        
          SBLanceRandomThread(%p);
          %p.lanceThread = true;
      }
           
      %int.delete();
      %FFRObject.schedule(32, delete);
   }
}

function WaterBlock::Damage(){}

function checkMAHit(%projectile, %targetObject, %position)
{
   if( %projectile.starburstSpawned )	// +soph
      return;				// +soph

   %data = %projectile.getDatablock();
   %ddt = %data.directDamage + %data.indirectDamage;
//   %ddt = %data.directDamage ? %data.directDamage : %data.indirectDamage;
   %ddt = %projectile.damageFactor !$= "" ? %projectile.damageFactor : %ddt;

   %source = %projectile.sourceObject;
   %time = getSimTime();

   %height = -15 ;			// +[soph]
   if( %targetObject.isTacticalMech )
      return ;
   if( %source.isTacticalMech )
      %ddt /= 2 ;			// +[/soph]
   
   if(!isObject(%source))
       return;
         
   if(%time < %source.lastMATime)
       return;
   
   if(%ddt > 0.29 && %targetObject.getType() & $TypeMasks::PlayerObjectType && %source.getType() & $TypeMasks::PlayerObjectType && !%source.client.isAIControlled())
   {
//     %searchResult = containerRayCast(%targetObject.getPosition(), VectorAdd(%position, "0 0 -15"), $TypeMasks::PlayerObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType, %targetObject);
     %searchResult = containerRayCast(%targetObject.getPosition(), VectorAdd(%position, "0 0" SPC %height ), $TypeMasks::PlayerObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType, %targetObject);
     %word = firstWord(%searchResult);
     
     if(!isObject(%word))
     {
          // Prevent tracking if teams are the same
          if(%source.client.team == %targetObject.client.team)
               return;
               
          %dist = mFloor(getDistance3D(%source.getWorldBoxCenter(), %position));
          %source.lastMATime = %time + 1500;          
          
          commandToClient(%source.client, 'BottomPrint', ">>> MID AIR HIT <<<\nDistance:" SPC %dist@"m", 5, 2);
          chatMessageAll(%source.client, '\c4%1 has scored a mid-air hit on %2! (distance: %3m)', %source.client.namebase, %targetObject.client.namebase, %dist);                         
          addStatTrack(%source.client.guid, "MAs", 1);
     }
   }
}

function ProjectileData::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %projectile.exploded = true;	// putting this everywhere it could possibly need to be +soph
   if(!isObject(%targetObject))
      return;

   if(%targetObject.reflectGun)
      return;

//   	%source = isObject(%projectile.sourceObject) ? %projectile.sourceObject : %projectile.bkSourceObject;	// starburst bug -soph
   %source = isObject(%projectile.bkSourceObject) ? %projectile.bkSourceObject : %projectile.sourceObject;	// fix +soph

   if(%projectile.lockedMissileThread) // radar-seeking projectiles
      %projectile.lockedMissileThread = false;

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   checkMAHit(%projectile, %targetObject, %position);

   if(!%modifier)
     %modifier = 1;

   if(%projectile.pDmgMod)
      %modifier *= %projectile.pDmgMod;

   if(%projectile.vehicleMod)
      %modifier *= %projectile.vehicleMod;
   
   if(%data.passengerDamageMod && %projectile.passengerCount)
     %modifier *= 1 + (%projectile.passengerCount * %data.passengerDamageMod);

   if(%projectile.damageMod)
     %modifier *= %projectile.damageMod;
          
//   echo("Proj" SPC %projectile SPC %projectile.getClassName() SPC %data.indirectDamage SPC %modifier SPC %data.indirectDamage * %modifier);
   
   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);

   if(%targetObject.getClassName() $= "ForceFieldBare")
   {
      if(isObject(%targetObject.deployBase))
      {
         if(%targetObject.deployBase.baseGenerator)
         {
            if( %data.hasDamageRadius && %data.indirectDamage > ( %data.directDamage * 2 ) )	// if(%data.hasDamageRadius) -soph
               %targetObject.deployBase.damage(%source, %position, %data.indirectDamage * %modifier * 0.1, %data.indirectDamageType);
            else
               %targetObject.deployBase.damage(%source, %position, %data.directDamage * %modifier * 0.05, %data.directDamageType);
         }
         else
         {
            if( %data.hasDamageRadius && %data.indirectDamage > ( %data.directDamage * 2 ) )	// if(%data.hasDamageRadius) -soph
               %targetObject.deployBase.damage(%source, %position, %data.indirectDamage * %modifier, %data.indirectDamageType);
            else
               %targetObject.deployBase.damage(%source, %position, %data.directDamage * %modifier, %data.directDamageType);
         }
      }
      else if( %targetObject.deployBase )
         %targetObject.delete() ;
      
      FFReflectProjectile(%projectile, %targetObject, %normal, %source);
   }
   else
   {
     // ApplyDam(%targetObject,%projectile.sourceObject);
//     if(%reportdmg)
//          echo(%modifier SPC "|" SPC (%data.directDamage * %modifier));

      %ddt = %data.directDamageType $= "" ? %data.radiusDamageType : %data.directDamageType;

      %targetObject.damage(%source, %position, %data.directDamage * %modifier, %ddt);   //console error when using raxx -Nite- (not fixed yet)
   }
}

function SniperProjectileData::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %damageAmount = %projectile.doNormalDamage == true ? %projectile.normalDmg : (%data.directDamage * %projectile.damageFactor);

   checkMAHit(%projectile, %targetObject, %position);

   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);
      
   if(!%dmodifier)
     %dmodifier = 1;

   if(%projectile.vehicleMod)
      %dmodifier *= 1 + %projectile.vehicleMod;

   if(%data.passengerDamageMod && %projectile.passengerCount)
     %dmodifier *= 1 + (%projectile.passengerCount * %data.passengerDamageMod);

     %damageAmount *= %dmodifier;

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   if(%targetObject.getType() & %TypeMasks::PlayerObjectType && !%projectile.noHeadshot)
   {
      %damLoc = firstWord(%targetObject.getDamageLocation(%position));
      if(%damLoc $= "head")
      {
         %targetObject.getOwnerClient().headShot = 1; // OMG They killed kenny!
         %modifier = %data.rifleHeadMultiplier;
      }
      else
      {
         %modifier = 1;
         %targetObject.getOwnerClient().headShot = 0;
      }
   }
   else
      %targetObject.getOwnerClient().headShot = 0;

   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
}

function ShapeBaseImageData::onFire(%data, %obj, %slot)
{
//   %data.lightStart = getSimTime();

//   if( %obj.station $= "" && %obj.isCloaked() )
//   {
//      if( %obj.respawnCloakThread !$= "" )
//      {
//         Cancel(%obj.respawnCloakThread);
//         %obj.setCloaked( false );
//         %obj.respawnCloakThread = "";
//      }
//      else
//      {
//         if( %obj.getEnergyLevel() > 20 )
//         {
//            %obj.setCloaked( false );
//            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
//         }
//      }
//   }
 
   %enUse = %data.fireEnergy;
   
   %mountDamageMod = 0;
   
   %useEnergyObj = %obj.getObjectMount();
   
   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   if(%data.usesEnergy)
   {
      if( %data.useMountEnergy || %obj.getDatablock().getName() $= "BomberFlyerTailTurret" )
         %energy = %useEnergyObj.getEnergyLevel();
      else
         %energy = %obj.getEnergyLevel();

      if(%data.useCapacitor && %data.usesEnergy)
         if(%useEnergyObj.turretObject.getCapacitorLevel() < %data.minEnergy)
            return;

      if(%obj.weaponAmpEngaged)
          %enUse *= 2.0;

      if(%obj.powerRecirculator)
         %enUse *= 0.75;

      else if(%energy < %enUse)
         return;
   }

   %vector = %obj.getMuzzleVector(%slot);

   if(%obj.targetingComputer)
   {
        %target = %obj.getLockedTarget();

        if(%target)
        {
           %tCenter = %target.getWorldBoxCenter();
           
           if(%target.getType() & $TypeMasks::VehicleObjectType)
           {
               %tFvec = vectorScale(%target.getForwardVector(), ((vectorLen(%target.getVelocity())+1) / getRandom(3, 8)));
               %tPredictPos = vectorAdd(%tCenter, %tFvec);
           }
           else
               %tPredictPos = %tCenter;
               
           %vector = getVectorFromPoints(%obj.getMuzzlePoint(%slot), %tPredictPos);
        }
        else
           %vector = %obj.getMuzzleVector(%slot);
   }
   
   if(%data.projectileSpread)
   {
      %x = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread;
      %y = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread;
      %z = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread;
      %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      %vector = MatrixMulVector(%mat, %vector);

      %p = new (%data.projectileType)() {
         dataBlock        = %data.projectile;
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };
   }
   else
   {
      %p = new (%data.projectileType)() {
         dataBlock        = %obj.targetingComputer ? "KMPlusBullet" : %data.projectile;	// ? "AARotaryBullet" : %data.projectile; -soph
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };
   }

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %p.damageMod = 1.0;

   // Vehicle Damage Modifier
   if( %vehicle && %vehicle != %obj )	// because sometimes they're the same and then we get exponential damage +soph
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if((%data.turboPlasmaEnabled && %obj.heatShielded) || (%data.enPackDamageBonus && %obj.hasEnergyPack))
     %p.damageMod += 0.25;
     
   if(%obj.weaponAmpEngaged)
     %p.damageMod += 0.3;
     
   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;

   if(%data.isCapWeapon && %obj.synMod)
     %p.damageMod += %obj.synMod * %obj.isSyn;

   if(%obj.targetingComputer)
   {
//      rBeginFlare(%p);
      rBeginProxy(%p);
      %enUse += 5;
   }
   
   if(%data.usesEnergy)
   {
      if(%data.useMountEnergy)
      {
         if( %data.useCapacitor )
            %vehicle.turretObject.setCapacitorLevel( %vehicle.turretObject.getCapacitorLevel() - %data.fireEnergy );
         else
            %useEnergyObj.setEnergyLevel(%energy - %enUse);
      }
      else
         %obj.setEnergyLevel(%energy - %enUse);
   }
   else
      %obj.decInventory(%data.ammo,1);

   return %p;
}

function ShapeBase::throwWeapon(%this)
{
//   if(%this.magneticHands)
//        return;
        
   if(Game.shapeThrowWeapon(%this))
   {
      %image = %this.getMountedImage($WeaponSlot);
      %this.throw(%image.item);
      %this.client.setWeaponsHudItem(%image.item, 0, 0);
   }
}

function ShapeBase::throwPack(%this)
{
   if(isObject(%this)) // z0dd - ZOD, 5/18/03. Console spam fix
   {
//      if(%this.armorPlate || %this.magneticPack)
//          return;

      if(%this.BAPackID)
          %this.unmountImage(0);
      
      %image = %this.getMountedImage($BackpackSlot);
      %this.throw(%image.item);
      %this.client.setBackpackHudItem(%image.item, 0);
   }
}

function ForcefieldBare::isMounted(){   return false;   }
function ForcefieldBare::shouldApplyImpulse(){   return false;   }
function ForcefieldBare::damage(){}

function ItemData::create(%block)
{
//   echo("created: "@ %block);
   if(%block $= "flag")
      %obj = new Item() {
         className = FlagObj;
         dataBlock = %block;
         static = false;
         rotate = false;
      };
   else
      %obj = new Item() {
         dataBlock = %block;
         static = false;
         //rotate = true;
         // don't make "placed items" rotate
         rotate = false;
      };
   return(%obj);
}

function ShapeBase::throwItem(%this,%data)
{
   %item = new Item() {
      dataBlock = %data;
      rotation = "0 0 1 " @ (getRandom() * 360);
      static = false;
   };

   %item.ammoStore = %data.ammoStore;
   MissionCleanup.add(%item);
   %this.throwObject(%item);
}

function ShapeBase::throwObject(%this,%obj) //make sure all objects are moveable
{
     if(!isObject(%this) || !isObject(%obj))
          return;
          
     if(%this.isTacticalMech)
          return;

   //-------------------------------------------
   // z0dd - ZOD, 5/27/02. Fixes flags hovering
   // over friendly player when collision occurs
//   if(%obj.getDataBlock().getName() $= "Flag")
      %obj.static = false;
//      echo("threw "@ %obj.getDatablock().getName() SPC %obj.getClassName());
   //-------------------------------------------

   //if the object is being thrown by a corpse, use a random vector
   if (%this.getState() $= "Dead")
   {
      %vec = (-1.0 + getRandom() * 2.0) SPC (-1.0 + getRandom() * 2.0) SPC getRandom();
      %vec = vectorScale(%vec, 10);
   }

   // else Initial vel based on the dir the player is looking
   else
   {
      %eye = %this.getEyeVector();
      %vec = vectorScale(%eye, 20);
   }

   // Add a vertical component to give the item a better arc
   %dot = vectorDot("0 0 1",%eye);
   if (%dot < 0)
      %dot = -%dot;
   %vec = vectorAdd(%vec,vectorScale("0 0 8",1 - %dot));

   // Add player's velocity
   %vec = vectorAdd(%vec,%this.getVelocity());
   %pos = getBoxCenter(%this.getWorldBox());

   //since flags have a huge mass (so when you shoot them, they don't bounce too far)
   //we need to up the %vec so that you can still throw them...
   if (%obj.getDataBlock().getName() $= "Flag")
      %vec = vectorScale(%vec, 40);

   //
   %obj.setTransform(%pos);
   %obj.applyImpulse(%pos,%vec);
   %obj.setCollisionTimeout(%this);
   %data = %obj.getDatablock();
   %data.onThrow(%obj, %this);

   //call the AI hook
   AIThrowObject(%obj);
}

function RadiusExplosion(%explosionSource, %position, %radius, %damage, %impulse, %sourceObject, %damageType)
{
//   error("RadiusExplosion("@%explosionSource@", "@%position@", "@%radius@", "@%damage@", "@%impulse@", "@%sourceObject@", "@%damageType@" )");

   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::TurretObjectType      |
                                                 $TypeMasks::ForceFieldObjectType  |
                                                 $TypeMasks::CorpseObjectType      |
                                                 $TypeMasks::ItemObjectType);

   %numTargets = 0;
   %bkCvgCount = 0;				// +soph
   while ((%targetObject = containerSearchNext()) != 0)
   {
      if(!isObject(%targetObject)) // bug!
         return;

      if(%targetObject.devShield)
         return;

      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;
							// +[soph]
							// + bwalls (and possibly other objects) tend to obstruct 
							// + each other in odd ways; using thrown rays to 
        						// + catch some occurrences is relatively fast and 
         %endPos = %targetObject.getPosition() ;	// + simple on top of the normal explosion routine
         %lineVec = getVectorFromPoints( %position , %endpos ) ;
         %endPos = vectorAdd( %position , vectorScale( %lineVec , %dist ) ) ;
         %startPos = vectorAdd( %position , vectorScale( %lineVec , -0.1) ) ;
         %hit = ContainerRayCast( %position , %endpos , $TypeMasks::InteriorObjectType    |
                                                        $TypeMasks::TerrainObjectType     |
                                                        $TypeMasks::PlayerObjectType      |
                                                        $TypeMasks::VehicleObjectType     |
                                                        $TypeMasks::StaticShapeObjectType |
                                                        $TypeMasks::TurretObjectType      |
                                                        $TypeMasks::ForceFieldObjectType  |
                                                        $TypeMasks::CorpseObjectType      |
                                                        $TypeMasks::ItemObjectType );
         if( isObject( %hit ) )				// +
         {						// +
            %hit.auxCoverageCheck = true ;		// +
            %bkCvgList[%bkCvgCount] = %hit ;		// +
            %bkCvgCount++ ;				// +
         }						// +[/soph]

      if(%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if(%mount.getDataBlock().isProtectedMountPoint[%found] && %mount.getEnergyLevel() < 5)
               continue;
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      if(!isObject(%targetObject))
         continue;

      %coverage = %damageType == $DamageType::Nuke && Game.class !$= "SiegeGame" ? calcExplosionCoverage(%position, %targetObject, $TypeMasks::TerrainObjectType) : calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::StaticShapeObjectType |
                                         $TypeMasks::VehicleObjectType));
      if( %coverage == 0 )			// aux coverage check +[soph]
         if( %targetObject.auxCoverageCheck )	// only if primary check failed
            %coverage = 1;			// +[/soph]

      if (%coverage == 0)
         continue;

      //if ( $splashTest )
         %amount = (1.0 - ((%dist / %radius) * 0.75)) * %coverage * %damage;
      //else
         //%amount = (1.0 - (%dist / %radius)) * %coverage * %damage;

      //error( "damage: " @ %amount @ " at distance: " @ %dist @ " radius: " @ %radius @ " maxDamage: " @ %damage );

      %data = %targetObject.getDataBlock();
      %className = %data.className;
      %type = %targetObject.getType();

      if(%impulse && %data.shouldApplyImpulse(%targetObject))
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);
         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));
         %doImpulse = true;
      }
      else if(%type & $TypeMasks::VehicleObjectType)
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);
         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));


         if( getWord( %momVec, 2 ) < -0.5 )
            %momVec = "0 0 1";

         // Add obj's velocity into the momentum vector
//         %velocity = %targetObject.getVelocity();
         //%momVec = VectorNormalize( vectorAdd( %momVec, %velocity) );
         %doImpulse = true;
					// no longer used -[soph]
//         if(%targetObject.usesCPS && %targetObject.getEnergyPct() >= 0.2) // needs at least 20% shields
//            %doImpulse = false;	// -[/soph]

         if(%targetObject.usesCPS )	// now scales to shield pct +[soph]
            if( isObject( %targetObject.shieldCap ) && %targetObject.shieldCap.getDatablock().maxCapacitorEnergy )
               if( %targetObject.shieldCap.getCapacitorLevel() )
               {			// better hope they're not emped
                  %factor = 1 - ( %targetObject.shieldCap.getCapacitorLevel() / %targetObject.shieldCap.getDatablock().maxCapacitorEnergy );
                  %impulseVec *= %factor / mSqrt( %targetObject.getDataBlock().maxDamage );
               }			// +[/soph]

         if(%targetObject.devshield)
            %doImpulse = false;
      }
      else if(%targetObject.pickupName) // for objects like packs and weapons
      {
         echo(%targetObject.pickupName);
//            %forcepct = %targetObject.getDatablock().mass / 50; // comparing damages based on flag
            %doImpulse = true;
      }
      else
      {
         %momVec = "0 0 1";
         %doImpulse = false;
      }

      if( %doImpulse && !%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive)	// +soph
         %targetObject.applyImpulse(%position, %impulseVec);							// +soph

      if(isObject(%targetObject))
      {
         if(%explosionSource.pDmgMod)
            %amount *= %explosionSource.pDmgMod;

         if(%amount > 0)
         {
            if( %damageType == $DamageType::Poison )								// +[soph]
               %targetObject.poisonObject( %sourceObject , %amount ) ;						// +
            else												// +[/soph]
            if(isObject(%targetObject.deployBase))
            {
               if(%targetObject.isFF)
                  %targetObject.deployBase.damage(%sourceObject, %position, %amount, %damageType);
               else
                  %targetObject.damage(%sourceObject, %position, %amount, %damageType);
            }
            else
               %targetObject.damage(%sourceObject, %position, %amount, %damageType);
         }
         else if( (%explosionSource.triggerConcussion || %explosionSource.getDataBlock().getName() $= "ConcussionGrenadeThrown") && %data.getClassName() $= "PlayerData" )
   	   {
             %data.applyConcussion( %dist, %radius, %sourceObject, %targetObject );

             if(!$teamDamage && %sourceObject != %targetObject && %sourceObject.client.team == %targetObject.client.team)
                messageClient(%targetObject.client, 'msgTeamConcussionGrenade', '\c1You were hit by %1\'s concussion grenade.', getTaggedString(%sourceObject.client.name));
         }
      }

//      if( %doImpulse && !%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive)	// ^up^ -soph
//         %targetObject.applyImpulse(%position, %impulseVec);							// +soph
   }
   for( %i = 0 ; %i < %bkCvgCount ; %i++ )									// aux coverage cleanup +soph
      %bkCvgList[%i].auxCoverageCheck = "";									// +soph
}

function EMPBurstExplosion(%explosionSource, %position, %radius, %damage, %impulse, %sourceObject )	// %damageType) -soph
{
   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::TurretObjectType);

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      if(!isObject(%targetObject)) // bug!
         return;

      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

//     if(%targetObject.isSyn || %targetObject.ownerShieldBeacon || %targetObject.devShield)		// -soph
      if( %targetObject.devShield )									// +soph
          continue;

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      if(!isObject(%targetObject))
         continue;

      %coverage = calcExplosionCoverage( %position , %targetObject ,
                                         ( $TypeMasks::InteriorObjectType	|
                                           $TypeMasks::TerrainObjectType	|
                                           $TypeMasks::ForceFieldObjectType	|
                                           $TypeMasks::StaticShapeObjectType	|
                                           $TypeMasks::VehicleObjectType ) ) ;
      if (%coverage == 0)
         continue;

      %amount = ( 1.0 - ( %dist / %radius ) ) * %coverage * %damage * 100 ; 
      EMPObject( %targetObject , %sourceObject , %amount ) ;
   }
}

function IncendiaryExplosion( %position , %radius , %damage , %sourceObject , %nonlinear )
{
   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType );

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      if(!isObject(%targetObject)) // bug!
         return;

      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

      if(%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if(%mount.getDataBlock().isProtectedMountPoint[%found] && %mount.getEnergyLevel() < 5)
               continue;
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      if(!isObject(%targetObject))
         continue;

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::StaticShapeObjectType |
                                         $TypeMasks::VehicleObjectType));
      if (%coverage == 0)
         continue;

      %amount = 1.0 - ( %dist / %radius ) ; 
      if( %nonlinear )
         %amount *= %amount * %coverage * %damage ; 
      else
         %amount *= %coverage * %damage ; 

      if( isObject( %targetObject ) && %amount > 0 )
         BurnObject( %targetObject , %sourceObject , %amount );
   }
}

function HealingExplosion( %position , %radius , %damage , %sourceObject )	// +[soph]
{
   %p = new LinearFlareProjectile()
   {
      dataBlock        = MendPulseProjectile ;
      initialDirection = "0 0 -1" ;
      initialPosition  = %position ;
      sourceObject     = %sourceObject ;
      sourceSlot       = 0 ;
   };
   MissionCleanup.add( %p ) ;

   InitContainerRadiusSearch( %position , %radius , $TypeMasks::PlayerObjectType |
                                                    $TypeMasks::VehicleObjectType ) ;

   %numTargets = 0 ;
   while ( ( %targetObject = containerSearchNext() ) != 0 )
   {
      if( !isObject( %targetObject ) ) // bug!
         return ;

      %dist = containerSearchCurrRadDamageDist() ;

      if ( %dist > %radius )
         continue ;

      if( %targetObject.isMounted() )
      {
         %mount = %targetObject.getObjectMount() ;
         %found = -1 ;
         for ( %i = 0 ; %i < %mount.getDataBlock().numMountPoints ; %i++ )
         {
            if ( %mount.getMountNodeObject( %i ) == %targetObject )
            {
               %found = %i ;
               break ;
            }
         }

         if ( %found != -1 )
         {
            if( %mount.getDataBlock().isProtectedMountPoint[ %found ] && %mount.getEnergyLevel() < 5 )
               continue ;
         }
      }

      %targets[ %numTargets ]     = %targetObject ;
      %targetDists[ %numTargets ] = %dist ;
      %numTargets++ ;
   }

   for ( %i = 0 ; %i < %numTargets ; %i++ )
   {
      %targetObject = %targets[ %i ] ;
      %dist = %targetDists[ %i ] ;

      if( !isObject( %targetObject ) )
         continue ;

      %coverage = calcExplosionCoverage( %position , %targetObject ,
                                       ( $TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::StaticShapeObjectType |
                                         $TypeMasks::VehicleObjectType ) ) ;
      if ( %coverage == 0 )
         continue;

      %amount = mSqRt( 1.0 - ( %dist / %radius ) ) * %coverage * %damage ; 

      if( isObject( %targetObject ) && %amount > 0 )
      {
         if( %targetObject.getType() & $TypeMasks::PlayerObjectType ) 
         {
            %targetObject.applyRepair( %amount * 0.9 ) ;
            %var = 10 ;
         }
         else
            %var = 5 ;
         zapVehicle( %targetObject , "HealGreen" ) ;
            
         %damageLevel = %targetObject.getDamageLevel() ;
         if( %damageLevel > 0 )
             %targetObject.setDamageLevel( %damageLevel - ( %amount / %var ) ) ;

         %targetObject.play3D( RepairPatchSound ) ;
      }
   }
}										// +[/soph]

function serverCmdPlayAnim(%client, %anim)
{
   if( %anim $= "Death1" || %anim $= "Death2" || %anim $= "Death3" || %anim $= "Death4" || %anim $= "Death5" ||
       %anim $= "Death6" || %anim $= "Death7" || %anim $= "Death8" || %anim $= "Death9" || %anim $= "Death10" || %anim $= "Death11" )
      return;

   %player = %client.player;
   // don't play animations if player is in a vehicle
   // ------------------------------------------------------------------
   // z0dd - ZOD, 5/8/02. Console spam fix, check for player object too.
   //if (%player.isMounted())
   //   return;
   if(isObject(%player) && (%player.isMounted() || !isObject(%player) || isNearForcefield(%player, 1)))
      return;

   %weapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).getName().item;
   if(%weapon $= "MissileLauncher" || %weapon $= "SniperRifle")
   {
      %player.animResetWeapon = true;
      %player.lastWeapon = %weapon;
      %player.unmountImage($WeaponSlot);
      // ----------------------------------------------
      // z0dd - ZOD, 5/8/02. %obj is the wrong varible.
      //%obj.setArmThread(look);
      %player.setArmThread(look);
   }
   %player.setActionThread(%anim);
}

function isNearForcefield(%obj, %dist)
{
  	InitContainerRadiusSearch(%obj.getPosition(), %dist, $TypeMasks::ForceFieldObjectType);

	if(containerSearchNext())
      return true;
   else
      return false;
}

//--------------------------------------------------------------------------
// Grenade/Mine code
//--------------------------------------

function MortarGrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %gren.setScale("3 3 3");
   %time = %player.client.isAIControlled() ? 2000 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "detonateGrenade", %gren);
   
   addStatTrack(%player.client.guid, "throwngrenades", 1);
}

function EMPGrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %time = %player.client.isAIControlled() ? 2000 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "empDetonateGrenade", %gren);
   
   addStatTrack(%player.client.guid, "throwngrenades", 1);   
}

function empDetonateGrenade(%obj)
{
   %obj.setDamageState(Destroyed);
   %data = %obj.getDataBlock();
   EMPBurstExplosion( %obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,
                   %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);
   %obj.schedule(500,"delete");
}

function FireGrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %time = %player.client.isAIControlled() ? 2000 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "incendiaryDetonateGrenade", %gren);	// "detonateGrenade", %gren); -soph
   
   addStatTrack(%player.client.guid, "throwngrenades", 1);   
}

function incendiaryDetonateGrenade(%obj)	// +[soph]
{
   %obj.setDamageState(Destroyed);
   %data = %obj.getDataBlock();
   RadiusExplosion( %obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage, %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);
   IncendiaryExplosion( %obj.getPosition() , %data.damageradius , 5 , %obj.sourceObject );
   %obj.schedule(500,"delete");
}						// +[/soph]

function PoisonGrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %time = %player.client.isAIControlled() ? 2000 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "detonateGrenade", %gren);

   addStatTrack(%player.client.guid, "throwngrenades", 1);   
}

// Perhaps this will get used now? O.o 
function BoosterGrenadeThrown::onThrow(%this, %gren, %player)
{
   %gren.setVelocity("0 0 0");
   %gren.detThread = schedule(32, %gren, "detonateGrenade", %gren); // AHHAHAHHAHA

//   %gren.delete();
//   applyKick(%this, 2500); // %this is not the thrower
}

function GrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %time = %player.client.isAIControlled() ? 1500 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "detonateGrenade", %gren);

   addStatTrack(%player.client.guid, "throwngrenades", 1);   
}

function ConcussionGrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %time = %player.client.isAIControlled() ? 2000 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "detonateGrenade", %gren);

   addStatTrack(%player.client.guid, "throwngrenades", 1);   
}

function FlashGrenadeThrown::onThrow(%this, %gren, %player)
{
   AIGrenadeThrown(%gren);
   %time = %player.client.isAIControlled() ? 2000 : %player.client.grenadeTimer * 1000;
   %gren.detThread = schedule(%time, %gren, "detonateFlashGrenade", %gren);   

   addStatTrack(%player.client.guid, "throwngrenades", 1);   
}

function detonateGrenade(%obj)
{
   %obj.setDamageState(Destroyed);
   %data = %obj.getDataBlock();
   RadiusExplosion( %obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage, 
                   %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);
   %obj.schedule(500,"delete");
}

function detonateFlashGrenade(%hg)
{
   %maxWhiteout = 2.0; //%hg.getDataBlock().maxWhiteout;
   %thrower = %hg.sourceObject.client;
   %hg.setDamageState(Destroyed);   
   %hgt = %hg.getTransform();
   %plX = firstword(%hgt);
   %plY = getWord(%hgt, 1);
   %plZ = getWord(%hgt, 2);
   %pos = %plX @ " " @ %plY @ " " @ %plZ;
   //all this stuff below ripped from projectiles.cs

   InitContainerRadiusSearch(%pos, 100.0, $TypeMasks::PlayerObjectType |
                                          $TypeMasks::TurretObjectType);

   while ((%damage = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrDist();

      %eyeXF = %damage.getEyeTransform();
      %epX   = firstword(%eyeXF);
      %epY   = getWord(%eyeXF, 1);
      %epZ   = getWord(%eyeXF, 2);
      %eyePos = %epX @ " " @ %epY @ " " @ %epZ;
      %eyeVec = %damage.getEyeVector();

      // Make sure we can see the thing...
      if (ContainerRayCast(%eyePos, %pos, $TypeMasks::TerrainObjectType |
                                          $TypeMasks::InteriorObjectType |
                                          $TypeMasks::StaticObjectType, %damage) !$= "0")
      {
         continue;
      }

      %distFactor = 1.0;
      if (%dist >= 100)
         %distFactor = 0.0;
      else if (%dist >= 35) {
         %distFactor = 1.0 - ((%dist - 35.0) / 80.0);
      }

      %dif = VectorNormalize(VectorSub(%pos, %eyePos));
      %dot = VectorDot(%eyeVec, %dif);

      %difAcos = mRadToDeg(mAcos(%dot));
      %dotFactor = 1.0;
      if (%difAcos > 60)
         %dotFactor = ((1.0 - ((%difAcos - 60.0) / 120.0)) * 0.2) + 0.5;
      else if (%difAcos > 45)
         %dotFactor = ((1.0 - ((%difAcos - 45.0) / 15.0)) * 0.5) + 0.8;

      %totalFactor = %dotFactor * %distFactor;
              
	  %prevWhiteOut = %damage.getWhiteOut();

		if(!%prevWhiteOut)
			if(!$teamDamage)
			{
				if(%damage.client != %thrower && %damage.client.team == %thrower.team)
					messageClient(%damage.client, 'teamWhiteOut', '\c1You were hit by %1\'s whiteout grenade.', getTaggedString(%thrower.name)); 
			}
		
      %whiteoutVal = %prevWhiteOut + %totalFactor;
      if(%whiteoutVal > %maxWhiteout)
      {
        //error("whitout at max");
        %whiteoutVal = %maxWhiteout;
      }
      
      %damage.setWhiteOut( %whiteoutVal );
   }
   %hg.schedule( 500, "delete" );
}

function MineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.team = %thrower.client.team;
   %mine.theClient = %thrower.client;
   if( %thrower.inv[ BlastechMine ] )	// +soph
{
      %mine.split = true ;		// +soph
error( "Splitter mine deployed:" SPC $TeamDeployedCount[%mineTeam, MineDeployed] SPC "mines alive");
}
   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);

   addStatTrack(%thrower.client.guid, "thrownmines", 1);
}

function LargeMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   %mine.setScale("4 4 4");
   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
   
   addStatTrack(%thrower.client.guid, "thrownmines", 1);   
}

function CloakMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   %mine.schedule( 250 , setCloaked , true );	// %mine.setCloaked(true); -soph
   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
   
   addStatTrack(%thrower.client.guid, "thrownmines", 1);   
}

function EMPMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
   
   addStatTrack(%thrower.client.guid, "thrownmines", 1);   
}

function IncendiaryMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
   
   addStatTrack(%thrower.client.guid, "thrownmines", 1);   
}

function BlastechMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
   
   addStatTrack(%thrower.client.guid, "thrownmines", 1);   
}

function deployMineCheck(%mineObj, %player)
{
   if(%mineObj.depCount > %mineObj.getDatablock().maxDepCount)
   {							// +[soph]
      if( %mineObj.split )				// +
         splitMine( %mineObj , %player ) ;		// +
      else 						// +[/soph]
         explodeMine(%mineObj, true);
      return ;						// +
   }							// +soph

   // wait until the mine comes to rest
   if(%mineObj.getVelocity() $= "0 0 0")
   {
      // 2-second delay before mine is armed -- let deploy thread play out etc.
      schedule(%mineObj.getDatablock().armTime, %mineObj, "armDeployedMine", %mineObj);


      // check for other deployed mines in the vicinity
      InitContainerRadiusSearch( %mineObj.getWorldBoxCenter() , LargeMineDeployed.spacing , $TypeMasks::ItemObjectType ) ;
      while((%itemObj = containerSearchNext()) != 0)
      {
         if(%itemObj == %mineObj)
            continue;
         %itemObjDatablock = %itemObj.getDatablock() ;	// +[soph]
         if( %itemObjDatablock.spacing > 0 && !%itemObj.noDamage )
         {						// +
            %distance = ContainerSearchCurrDist() ;	// +
            %mineObjDatablock = %mineObj.getDatablock() ;
            if( %distance < %itemObjDatablock.spacing && %distance < %mineObjDatablock.spacing )
            {						// +
               explodeMine( %mineObj , true ) ;		// +
               break ;					// +
            }						// +
         }						// +[/soph]
      }
      // play "deploy" thread
      if( %mineObj.split )				// +[soph]
      {							// +
         splitMine( %mineObj , %player ) ;		// +
         return ;					// +
      }							// +[/soph]
      %mineObj.playThread(0, "deploy");
      serverPlay3D(MineDeploySound, %mineObj.getTransform());
      %mineTeam = %mineObj.sourceObject.team;
      if( %mineObj.getDatablock().getName() $= "BlastechMineDeployed" )	// +[soph]
         $TeamDeployedCount[ %mineTeam , MineDeployed ] += 0.5 ;	// +
      else								// +[/soph]
         $TeamDeployedCount[%mineTeam, MineDeployed]++;
      if($TeamDeployedCount[%mineTeam, MineDeployed] > $TeamDeployableMax[MineDeployed])
      {
         messageClient( %player.client, 'MsgMaxMines', 'Maximum allowable mines deployed.' );
         schedule(100, %mineObj, "explodeMine", %mineObj, true);
      }
      else
      {
         //start the thread that keeps checking for objects near the mine...
         mineCheckVicinity(%mineObj);

         //let the AI know *after* it's come to rest...
         AIDeployMine(%mineObj);

         //let the game know there's a deployed mine
         Game.notifyMineDeployed(%mineObj);
      }
   }
   else
   {
      //schedule this deploy check again a little later
      %mineObj.depCount++;
      schedule(500, %mineObj, "deployMineCheck", %mineObj, %player);
   }
}

function splitMine( %mine , %player ) 			// +[soph]
{							// +
	%position = %mine.getPosition() ;		// +
	%p = new LinearProjectile()			// +
	{						// +
		dataBlock        = "Blowthrough" ;	// +
		initialDirection = "0 0 -1" ;		// +
		initialPosition  = %position ;		// +
		sourceObject     = %mine ;		// +
	};						// +
	%p.ignoreReflections = true ;			// +
	MissionCleanup.add( %p ) ;			// +
							// +
	%vector = vectorNormalize( getRandom() SPC getRandom() SPC "0" ) ;
	%x = getWord( %vector , 0 ) * 3 ;		// +
	%y = getWord( %vector , 1 ) * 3 ;		// +
	for( %i = 0 ; %i < 4 ; %i++ )			// +
	{						// +
		%newMine = new Item() {			// +
			dataBlock = BlastechMineDeployed ;
			position = %position ;		// +
			rotation = "0 0 1" SPC ( getRandom() * 360 ) ;
			sourceObject = %player ;	// +
		} ;					// +
		MissionCleanup.add( %newMine ) ;	// +
							// +
		%newMine.team = %mine.team ;		// +
		%newMine.armed = false ;		// +
		%newMine.damaged = 0 ;			// +
		%newMine.detonated = false ;		// +
		%newMine.depCount = 0 ;			// +
		%newMine.theClient = %player.client ;	// +
		%newMine.applyImpulse( %position , %x SPC %y SPC ( 6 + 6 * getRandom ) ) ;
		schedule( 1500 , %newMine , "deployMineCheck" , %newMine , %player ) ;
		%z = %x * -1 ;				// +
		%x = %y ;				// +
		%y = %z ;				// +
	}						// +
	%mine.schedule( 100 , delete ) ;		// +
}							// +[/soph]

function armDeployedMine(%mine)
{
   %mine.armed = true;
}

function mineCheckVicinity(%mine)
{
   // this function is called after the mine has been deployed. It will check the
   // immediate area around the mine (2.5 meters at present) for players or vehicles
   // passing by, and detonate if any are found. This is to extend the range of the
   // mine so players don't have to collide with them to set them off.

   // don't bother to check if mine isn't armed yet
   if(%mine.armed)
      // don't keep checking if mine is already detonating
      if(!%mine.boom)
      {
         // the actual check for objects in the area
         %mineLoc = %mine.getWorldBoxCenter();
         %masks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
         %detonateRange = %mine.getDatablock().proximity;

         InitContainerRadiusSearch(%mineLoc, %detonateRange, %masks);
         while((%tgt = containerSearchNext()) != 0)
         {
            if(%mine.team != %tgt.client.team)
            {
                 %mine.detonated = true;
                 schedule(50, %mine, "explodeMine", %mine, false);
                 break;
            }
         }
      }
   // if nothing set off the mine, schedule another check
   if(!%mine.detonated)
      schedule(300, %mine, "mineCheckVicinity", %mine);
}

function MineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(%col.client)
      {
          if(%col.client.team != %obj.team)
               explodeMine(%obj, false);
      }
      else
           explodeMine(%obj, false);
   }
}

function LargeMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
      explodeMine(%obj, false);
}

function CloakMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(%col.client)
      {
          if(%col.client.team != %obj.team)
               explodeMine(%obj, false);
      }
      else
           explodeMine(%obj, false);
   }
}

function EMPMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(%col.client)
      {
          if(%col.client.team != %obj.team)
               explodeMine(%obj, false);
      }
      else
           explodeMine(%obj, false);
   }
}

function IncendiaryMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(%col.client)
      {
          if(%col.client.team != %obj.team)
               explodeMine(%obj, false);
      }
      else
           explodeMine(%obj, false);
   }
}

function BlastechMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(%col.client)
      {
          if(%col.client.team != %obj.team)
               explodeMine(%obj, false);
      }
      else
           explodeMine(%obj, false);
   }
}

function explodeMine(%mo, %noDamage)
{
   %mo.noDamage = %noDamage;
   %mo.setDamageState(Destroyed);
}

function MineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(!%targetObject.armed)
      return;

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
      %targetObject.setDamageState(Destroyed);
}

function LargeMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(!%targetObject.armed)
      return;

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function CloakMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(!%targetObject.armed)
      return;

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function EMPMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(!%targetObject.armed)
      return;

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function IncendiaryMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(!%targetObject.armed)
      return;

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function BlastechMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if(!%targetObject.armed)
      return;

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function MineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
      RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,
                      %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);

   %obj.schedule(600, "delete");
error( "Mine detonated:" SPC $TeamDeployedCount[%mineTeam, MineDeployed] SPC "mines alive");
}

function LargeMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
      RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,
                      %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);

   %obj.schedule(600, "delete");
}

function CloakMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
      RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,
                      %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);

   %obj.schedule(600, "delete");
}

function EMPMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
      EMPBurstExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,	// was radiusexplosion -soph
                      %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);

   %obj.schedule(600, "delete");
}

function IncendiaryMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
   {
      RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,
                      %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);
      IncendiaryExplosion( %obj.getPosition() , %data.damageradius , 5 , %obj.sourceObject );	// +soph
   }

   %obj.schedule(600, "delete");
}

function BlastechMineDeployed::onDestroyed(%data, %obj, %lastState)
{
	%p = new LinearProjectile()			// +[soph]
	{						// +
		dataBlock        = "Blowthrough" ;	// +
		initialDirection = "0 0 -1" ;		// +
		initialPosition  = %obj.getPosition() ;	// +
		sourceObject     = %obj ;		// +
	};						// +
	%p.ignoreReflections = true ;			// +
	MissionCleanup.add( %p ) ;			// +[/soph]

   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[ %mineTeam , MineDeployed ] -= 0.5 ;
   // %noDamage is a boolean flag -- don't want to set off all other mines in
   // vicinity if there's a "mine overload", so apply no damage/impulse if true
   if(!%obj.noDamage)
      RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage,
                      %data.kickBackStrength, %obj.sourceObject, %data.radiusDamageType);

   %obj.schedule(600, "delete");
}

//--------------------------------------------------------------------------
// Initial ::onChange function (beacon)
//--------------------------------------

function Beacon::onUse(%data, %obj)
{
   // look for 3 meters along player's viewpoint for interior or terrain
   %searchRange = 3.0;
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType;
   // get the eye vector and eye transform of the player
   %eyeVec = %obj.getEyeVector();
   %eyeTrans = %obj.getEyeTransform();
   // extract the position of the player's camera from the eye transform (first 3 words)
   %eyePos = posFromTransform(%eyeTrans);
   // normalize the eye vector
   %nEyeVec = VectorNormalize(%eyeVec);
   // scale (lengthen) the normalized eye vector according to the search range
   %scEyeVec = VectorScale(%nEyeVec, %searchRange);
   // add the scaled & normalized eye vector to the position of the camera
   %eyeEnd = VectorAdd(%eyePos, %scEyeVec);
   // see if anything gets hit
   %searchResult = containerRayCast(%eyePos, %eyeEnd, %mask, 0);
   if(!%searchResult )
   {

     changeWeaponMode(%obj);
     return 0;
   }
   else
   {
      %searchObj = GetWord(%searchResult, 0);
      if(%searchObj.getType() & ($TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType) )
      {
         // if there's already a beacon where player is aiming, switch its type
         // otherwise, player can't deploy a beacon there
         if(%searchObj.getDataBlock().getName() $= DeployedBeacon)
            switchBeaconType(%searchObj);
         else
            messageClient(%obj.client, 'MsgBeaconNoSurface', '\c2Cannot place beacon. Not a valid surface.');
         return 0;
      }
      else if(%obj.inv[%data.getName()] <= 0)
         return 0;
   }
   // newly deployed beacons default to "target" type
   if($TeamDeployedCount[%obj.team, TargetBeacon] >= $TeamDeployableMax[TargetBeacon])
   {
      messageClient(%obj.client, 'MsgDeployFailed', '\c2Your team\'s control network has reached its capacity for this item.~wfx/misc/misc.error.wav');
      return 0;
   }
   %terrPt = posFromRaycast(%searchResult);
   %terrNrm = normalFromRaycast(%searchResult);

   %intAngle = getTerrainAngle(%terrNrm);  // getTerrainAngle() function found in staticShape.cs
   %rotAxis = vectorNormalize(vectorCross(%terrNrm, "0 0 1"));
   if (getWord(%terrNrm, 2) == 1 || getWord(%terrNrm, 2) == -1)
      %rotAxis = vectorNormalize(vectorCross(%terrNrm, "0 1 0"));
   %rotation = %rotAxis @ " " @ %intAngle;

   %obj.decInventory(%data, 1);
   %depBeac = new BeaconObject() {
      dataBlock = "DeployedBeacon";
      position = VectorAdd(%terrPt, VectorScale(%terrNrm, 0.05));
      rotation = %rotation;
   };
   $TeamDeployedCount[%obj.team, TargetBeacon]++;

   %depBeac.playThread($AmbientThread, "ambient");
   %depBeac.team = %obj.team;
   %depBeac.sourceObject = %obj;

   // give it a team target
   %depBeac.setTarget(%depBeac.team);
   MissionCleanup.add(%depBeac);
   %depBeac.setBeaconType(vehicle);
}

//function Beacon::getTargetPoint() // O.o?
//{
//   return 0;
//}

function RepairKit::onUse(%data, %obj)
{
   if(%obj.repairKitFactor $= "")
      %obj.repairKitFactor = 1.0;
      
   %objectMount = %obj.getObjectMount();
   
   if(isObject(%objectMount))
   {
       if(%objectMount.getDatablock().jetFire)
       {
             if(%objectMount.getDamageLevel() != 0)
             {
                addStatTrack(%obj.client.guid, "repairKitsUsed", 1);             

//                if(%obj.isEMP || %obj.isPoisoned || %obj.isFlaming)	// -[soph]
//                {							// -
//                     %obj.statusFlags = 0;				// -[/soph]

                if( %obj.isPoisoned )					// +[soph]
                {							// +
                     %time = getSimTime() ;				// +
                     %obj.PoisonTime = %time + ( ( %obj.PoisonTime - %time ) * 0.5 ) ;
                     %obj.lDamageStack[ Poison ] *= 0.1 ;		// +
                }							// +
                if( %obj.isEMP )					// +
                {							// +[/soph]
                     %obj.isEMP = false;
                     %obj.statusFlags |= $StatusEffect::EMP ;		// +[soph]
                }							// +
                if( %obj.isFlaming )					// +
                {							// +[/soph]					
//                     %obj.isPoisoned = false;				// -soph
                     %obj.isFlaming = false;
                     %obj.statusFlags |= $StatusEffect::Burn ;		// +soph
                }							// +soph
                     %obj.statusImmunityTime = getSimTime() + 5000;
//                }							// -soph

                %obj.decInventory(%data,1);
                %objectMount.play3D(RepairPatchSound);
                messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Repair Kit Used.');
                %objectMount.applyRepair(0.2 * %obj.repairKitFactor);
                %obj.applyRepair(0.2 * %obj.repairKitFactor);	// so it transfers over when the player hops out +soph
                return;						// so it doesn't burn two at once +soph
             }
       }
   }
   

   // Don't use the kit unless we're damaged
   if( %obj.getDamageLevel() != 0 || %obj.isEMP )			// (%obj.getDamageLevel()) -soph
   {
          addStatTrack(%obj.client.guid, "repairKitsUsed", 1);

//          if(%obj.isEMP || %obj.isPoisoned || %obj.isFlaming)		// -[soph]
//          {								// -
//               %obj.statusFlags = 0;					// -[/soph]

          if( %obj.isPoisoned )						// +[soph]
          {								// +
               %time = getSimTime() ;					// +
               %obj.PoisonTime = %time + ( ( %obj.PoisonTime - %time ) * 0.5 ) ;
               %obj.lDamageStack[ Poison ] *= 0.1 ;			// +
          }								// +
          if( %obj.isEMP )						// +
          {								// +[/soph]
               %obj.isEMP = false;
               %obj.statusFlags |= $StatusEffect::EMP ;			// +[soph]
          }								// +
          if( %obj.isFlaming )						// +
          {								// +[/soph]
//               %obj.isPoisoned = false;				// -soph 
               %obj.isFlaming = false;
               %obj.statusFlags |= $StatusEffect::Burn ;		// +soph
          }								// +soph
               
               %obj.statusImmunityTime = getSimTime() + 5000;
//          }								// -soph

         %obj.decInventory(%data,1);
         %obj.play3D(RepairPatchSound);
         messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Repair Kit Used.');
         %obj.applyRepair(0.2 * %obj.repairKitFactor);
   }
}

//--------------------------------------------------------------------------
//
//--------------------------------------

function RepairPatch::onCollision(%data,%obj,%col)
{
   if(%obj.repairKitFactor $= "")
      %obj.repairKitFactor = 1.0;
      
   if ( %col.getDataBlock().className $= Armor
//     && %col.getDamageLevel() != 0									// -soph
//     && %col.getState() !$= "Dead" )									// -soph
       && %col.getState() !$= "Dead" 									// +soph
       && ( %col.getDamageLevel() != 0 || %col.isEMP ) )						// +soph
   {
      if (%col.isMounted())
         return;

//      if(%obj.statusFlags & ($StatusEffect::EMP | $StatusEffect::Burn | $StatusEffect::Poison))	// -[soph]
//      {												// -
//           %obj.statusFlags = 0;									// -
//           %obj.statusImmunityTime = getSimTime() + 5000;						// -
//      }												// -[/soph]

      if( %obj.isPoisoned )										// +[soph]
      {													// +
         %time = getSimTime() ;										// +
         %obj.PoisonTime = %time + ( ( %obj.PoisonTime - %time ) * 0.75 ) ;				// +
         %obj.lDamageStack[ Poison ] *= 0.5 ;								// +
      }													// +
      if( %obj.isEMP )											// +
      {													// +
         %obj.isEMP = false;										// +
         %obj.statusFlags |= $StatusEffect::EMP ;							// +
      }													// +
      if( %obj.isFlaming )										// +
      {													// +
         %obj.isFlaming = false;									// +
         %obj.statusFlags |= $StatusEffect::Burn ;							// +
      }													// +
      %obj.statusImmunityTime = getSimTime() + 5000;							// +[/soph]
      
      %col.playAudio(0, RepairPatchSound);
      %col.applyRepair(0.125 * %obj.repairKitFactor);
      %obj.respawn();

      if (%col.client > 0)
         messageClient(%col.client, 'MsgItemPickup', '\c0You picked up %1.', %data.pickUpName);
   }
}

function stationTrigger::onEnterTrigger(%data, %obj, %colObj)
{
	//make sure it's a player object, and that that object is still alive
   if(%colObj.getDataBlock().className !$= "Armor" || %colObj.getState() $= "Dead" || %colObj.getDataBlock().isTacticalMech || %colObj.overdriveMode)
      return;

      if(!isObject(%obj.station))
           return;
   if (%obj.station.getDatablock().getName() $= "StationVehicle" && (%colObj.pdead && $votePracticeModeOn))
	return;

   %colObj.inStation = true;
   commandToClient(%colObj.client,'setStationKeys', true);
   if(Game.stationOnEnterTrigger(%data, %obj, %colObj)) {
      //verify station.team is team associated and isn't on player's team
      if((%obj.mainObj.team != %colObj.client.team) && (%obj.mainObj.team != 0))
      {
         %obj.station.playAudio(2, StationAccessDeniedSound);
         messageClient(%colObj.client, 'msgStationDenied', '\c2Wrong team, buddy.');
      }
      else if(isObject(%obj.disableObj) && %obj.disableObj.isDisabled() && !(%colObj.pdead && $votePracticeModeOn))
      {
         messageClient(%colObj.client, 'msgStationDisabled', '\c2I require repairs.');
      }
      else if((!%obj.mainObj.isPowered() || %obj.mainObj.isEMP) && !(%colObj.pdead && $votePracticeModeOn))
      {
         messageClient(%colObj.client, 'msgStationNoPower', '\c2Hmm... no power.');
      }
      else if(%obj.station.notDeployed)
      {
         messageClient(%colObj.client, 'msgStationNotDeployed', '\c2Station is not deployed.');
      }
      else if(%obj.station.triggeredBy $= "")
      {
         if(%obj.station.getDataBlock().setPlayersPosition(%obj.station, %obj, %colObj))
         {
	   // DarkDragonDX: Fix the HUD
	   if (%colObj.pdead)
	   {
		   for(%i =0; %i<$InventoryHudCount; %i++)
		      %colObj.client.setInventoryHudItem($InventoryHudData[%i, itemDataName], 0, 1);
		   %colObj.client.clearBackpackIcon();
	   }
//            %colObj.setInvincible(true);
//            schedule(500, %colObj, "checkInvicibleTag", %colObj);
            messageClient(%colObj.client, 'CloseHud', "", 'inventoryScreen');
            commandToClient(%colObj.client, 'TogglePlayHuds', true);
            %obj.station.triggeredBy = %colObj;
            %obj.station.getDataBlock().stationTriggered(%obj.station, 1);
            %colObj.station = %obj.station;
            %colObj.lastWeapon = ( %colObj.getMountedImage($WeaponSlot) == 0 ) ? "" : %colObj.getMountedImage($WeaponSlot).getName().item;
            %colObj.unmountImage($WeaponSlot);
            addStatTrack(%obj.client.guid, "invStationUses", 1);      

	    // DarkDragonDX: Set their Dead State, undo invincibility and fix their sensordata.
	    if (%colObj.pdead)
	    {
		messageClient(%colObj.client, 'msgClient', "\c3You are now ready to re-enter the battlefield.");
		
		%colObj.pdead = false;
		%colObj.setInvincible(false);
		%colObj.startFade(0,0,0);
		%colObj.setTarget(%colObj.client.target);

		%colObj.setOwnerClient(%colObj.client);

		// updates client's target info for this player
		setTargetDataBlock(%colObj.client.target, %colObj.getDatablock());
		setTargetSensorData(%colObj.client.target, PlayerSensor);
		setTargetSensorGroup(%colObj.client.target, %colObj.client.team);
		%colObj.client.setSensorGroup(%colObj.client.team);

		// Inv shut down / destroyed?
		if (!%obj.mainObj.isPowered() || (isObject(%obj.disableObj) && %obj.disableObj.isDisabled()))
		{
			%colObj.schedule(500, "ClearInventory");
			%colObj.client.schedule(510, "SetWeaponsHudClearAll");
  			%colObj.client.schedule(520, "SetInventoryHudClearAll");
			schedule(600,0,"GameDefaultEquip",%colObj, false);
			%colObj.schedule(610, "setArmor", "Light");
		}
  	    }
      
         }
      }
   }
}

function stationTrigger::onLeaveTrigger(%data, %obj, %colObj)
{
   if(%colObj.getDataBlock().className !$= "Armor")
      return;

   %colObj.setInvincible(false);
   %colObj.inStation = false;
   commandToClient(%colObj.client,'setStationKeys', false);
   if(%obj.station)
   {
      if(%obj.station.triggeredBy == %colObj)
      {
         %obj.station.getDataBlock().stationFinished(%obj.station);
         %obj.station.getDataBlock().endRepairing(%obj.station);
         %obj.station.triggeredBy = "";
         %obj.station.getDataBlock().stationTriggered(%obj.station, 0);

         if(!%colObj.teleporting)
            %colObj.station = "";
			if(%colObj.getMountedImage($WeaponSlot) == 0 && !%colObj.teleporting)
	      {
	         if(%colObj.inv[%colObj.lastWeapon])
	            %colObj.use(%colObj.lastWeapon);

            if(%colObj.getMountedImage($WeaponSlot) == 0)
               %colObj.selectWeaponSlot( 0 );
	      }
      }
   }
}

function checkTurretMount(%data, %obj, %slot)
{
   // search for a turret base in player's LOS
   %eyeVec = VectorNormalize(%obj.getEyeVector());
   %srchRange = VectorScale(%eyeVec, 5.0); // look 5m for a turret base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potTurret = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::TurretObjectType);
   if(%potTurret != 0)
   {
      if(%potTurret.getDatablock().getName() $= "TurretBaseLarge" || %potTurret.getDatablock().getName() $= "MobileTurretBase" || %potTurret.getDatablock().getName() $= "FreeBaseTurret") // || %potTurret.getDatablock().getName() $= "AssaultFlyerMainTurret"
      {
         // found a turret base, what team is it on?
         if(%potTurret.team == %obj.client.team)
         {
				if(%potTurret.getDamageState() !$= "Enabled")
				{
					// the base is destroyed
					messageClient(%obj.client, 'MsgBaseDestroyed', "\c2Turret base is disabled, cannot mount barrel.");
					%obj.setImageTrigger($BackpackSlot, false);
				}
				else
				{
		         // it's a functional turret base on our team! stick the barrel on it
		         messageClient(%obj.client, 'MsgTurretMount', "\c2Mounting barrel pack on turret base.");
		         serverPlay3D(TurretPackActivateSound, %potTurret.getTransform());
		         %potTurret.initiateBarrelSwap(%obj);
				}
         }
         else
         {
            // whoops, wrong team
            messageClient(%obj.client, 'MsgTryEnemyTurretMount', "\c2Cannot mount a barrel on an enemy turret base!");
            %obj.setImageTrigger($BackpackSlot, false);
         }
      }
      else
      {
         // tried to mount barrel on some other turret type
         messageClient(%obj.client, 'MsgNotTurretBase', "\c2Can only mount a barrel on a turret base.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any turret
      messageClient(%obj.client, 'MsgNoTurretBase', "\c2No turret within range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

//--------------------------------------------------------------------------
//
//--------------------------------------

function checkCorrectMount(%col, %obj) //no more heavy in pilot seat
{
   if(findEmptySeat(%col, %obj, false) < 0)
   {
      %obj.getDataBlock().doDismount(%obj, true);
      %xVel = 250.0 - (getRandom() * 500.0);
      %yVel = 250.0 - (getRandom() * 500.0);
      %zVel = (getRandom() * 100.0) + 50.0;
      %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
      %obj.applyImpulse(%obj.getTransform(), %flingVel);
   }
}

// keen: wait.... why is this function here twice...
function xArmor::onCollision(%this,%obj,%col,%forceVehicleNode)
{
   if (%obj.getState() $= "Dead")
      return;

   %dataBlock = %col.getDataBlock();
   %className = %dataBlock.className;
   %client = %obj.client;

//   if(%col.isFlaming)
//      %obj.burnObject(%col);

   // player collided with a vehicle
   %node = -1;
   if (%forceVehicleNode !$= "" || (%className $= WheeledVehicleData || %className $= FlyingVehicleData || %className $= HoverVehicleData) &&
         %obj.mountVehicle && %obj.getState() $= "Move" && %col.mountable && !%obj.inStation && %col.getDamageState() !$= "Destroyed") {

      %vvel = velToSingle(%col.getVelocity());
      if(%vvel > %datablock.collDamageThresholdVel) // do the same damage you did to them, except nulled some
         %col.damage(%obj, %col.getWorldBoxCenter(), 0.5*(%col.getMass() / %obj.getMass())*(%vvel*%datablock.collDamageMultiplier), $DamageType::Impact);

      //if the player is an AI, he should snap to the mount points in node order,
      //to ensure they mount the turret before the passenger seat, regardless of where they collide...
      if (%obj.client.isAIControlled())
      {
         %transform = %col.getTransform();

         //either the AI is *required* to pilot, or they'll pick the first available passenger seat
         if (%client.pilotVehicle)
         {
            //make sure the bot is in light armor
            if (%client.player.getArmorSize() $= "Light")
            {
               //make sure the pilot seat is empty
               if (!%col.getMountNodeObject(0))
                  %node = 0;
            }
         }
         else
            %node = findAIEmptySeat(%col, %obj);
      }
      else
         %node = findEmptySeat(%col, %obj, %forceVehicleNode);

      //now mount the player in the vehicle
      if(%node >= 0)
      {
         if(%obj.inStation)
            return;

         // players can't be pilots, bombardiers or turreteers if they have
         // "large" packs -- stations, turrets, turret barrels
         if(hasLargePack(%obj)) {
            // check to see if attempting to enter a "sitting" node
            if(nodeIsSitting(%datablock, %node)) {
               // send the player a message -- can't sit here with large pack
               if(!%obj.noSitMessage)
               {
                  %obj.noSitMessage = true;
                  %obj.schedule(2000, "resetSitMessage");
                  messageClient(%obj.client, 'MsgCantSitHere', '\c2Pack too large, can\'t occupy this seat.~wfx/misc/misc.error.wav');
               }
               return;
            }
         }
         if(%col.noEnemyControl && %obj.team != %col.team)
            return;

         commandToClient(%obj.client,'SetDefaultVehicleKeys', true);
         //If pilot or passenger then bind a few extra keys
         if(%node == 0)
            commandToClient(%obj.client,'SetPilotVehicleKeys', true);
         else
            commandToClient(%obj.client,'SetPassengerVehicleKeys', true);

         if(!%obj.inStation)
            %col.lastWeapon = ( %col.getMountedImage($WeaponSlot) == 0 ) ? "" : %col.getMountedImage($WeaponSlot).getName().item;
         else
            %col.lastWeapon = %obj.lastWeapon;

         //AI Hook here to state sitting position for vehicle type to fix the AI standing is turret seat bug - Lagg... 10-17-2003
         if (%obj.client.isAIControlled() && %node < 2 && (%dataBlock.getName() $= "BomberFlyer" ||
           %dataBlock.getName() $= "AssaultVehicle"))
         {
            //%client.clearStep();
            %client.player.setActionThread(%dataBlock.mountPose[0], true, true);
         }

         %col.mountObject(%obj,%node);
         %col.playAudio(0, MountVehicleSound);
         %obj.mVehicle = %col;

			// if player is repairing something, stop it
			if(%obj.repairing)
				stopRepairing(%obj);

         //this will setup the huds as well...
         vehicleOnPlayerMounted(%dataBlock, %col, %obj.client);
         %dataBlock.playerMounted(%col,%obj, %node);
//`         schedule(1000, 0, "checkCorrectMount", %col, %obj); //MrKeen - Heavy pilot bug
      }
   }
   else if (%className $= "Armor") {
      // player has collided with another player
      if(%col.getState() $= "Dead") {
         %gotSomething = false;
//         if(%obj.isFlaming)
//            %col.burnObject(%obj);
         // it's corpse-looting time!
         // weapons -- don't pick up more than you are allowed to carry!
         for(%i = 0; ( %obj.weaponCount < %obj.getDatablock().maxWeapons ) && $InvWeapon[%i] !$= ""; %i++)
         {
            %weap = $NameToInv[$InvWeapon[%i]];
            if ( %col.hasInventory( %weap ) )
            {
               if ( %obj.incInventory(%weap, 1) > 0 )
               {
                  %col.decInventory(%weap, 1);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %weap.pickUpName);
               }
            }
         }
         // targeting laser:
         if ( %col.hasInventory( "TargetingLaser" ) )
         {
            if ( %obj.incInventory( "TargetingLaser", 1 ) > 0 )
            {
               %col.decInventory( "TargetingLaser", 1 );
               %gotSomething = true;
               messageClient( %obj.client, 'MsgItemPickup', '\c0You picked up a targeting laser.' );
            }
         }
         // ammo
         for(%j = 0; $ammoType[%j] !$= ""; %j++)
         {
            %ammoAmt = %col.inv[$ammoType[%j]];
            if(%ammoAmt)
            {
               // incInventory returns the amount of stuff successfully grabbed
               %grabAmt = %obj.incInventory($ammoType[%j], %ammoAmt);
               if(%grabAmt > 0)
               {
                  %col.decInventory($ammoType[%j], %grabAmt);
                  %gotSomething = true;
                  messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', $ammoType[%j].pickUpName);
                  %obj.client.setWeaponsHudAmmo($ammoType[%j], %obj.getInventory($ammoType[%j]));
               }
            }
         }
         // figure out what type, if any, grenades the (live) player has
         %playerGrenType = "None";
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++) {
            %gren = $NameToInv[$InvGrenade[%x]];
            %playerGrenAmt = %obj.inv[%gren];
            if(%playerGrenAmt > 0)
            {
               %playerGrenType = %gren;
               break;
            }
         }
         // grenades
         for(%k = 0; $InvGrenade[%k] !$= ""; %k++)
         {
            %gren = $NameToInv[$InvGrenade[%k]];
            %corpseGrenAmt = %col.inv[%gren];
            // does the corpse hold any of this grenade type?
            if(%corpseGrenAmt)
            {
               // can the player pick up this grenade type?
               if((%playerGrenType $= "None") || (%playerGrenType $= %gren))
               {
                  %taken = %obj.incInventory(%gren, %corpseGrenAmt);
                  if(%taken > 0)
                  {
                     %col.decInventory(%gren, %taken);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %gren.pickUpName);
                     %obj.client.setInventoryHudAmount(%gren, %obj.getInventory(%gren));
                  }
               }
               break;
            }
         }
         // figure out what type, if any, mines the (live) player has
         %playerMineType = "None";
         for(%y = 0; $InvMine[%y] !$= ""; %y++)
         {
            %mType = $NameToInv[$InvMine[%y]];
            %playerMineAmt = %obj.inv[%mType];
            if(%playerMineAmt > 0)
            {
               %playerMineType = %mType;
               break;
            }
         }
         // mines
         for(%l = 0; $InvMine[%l] !$= ""; %l++)
         {
            %mine = $NameToInv[$InvMine[%l]];
            %mineAmt = %col.inv[%mine];
            if(%mineAmt) {
               if((%playerMineType $= "None") || (%playerMineType $= %mine))
               {
                  %grabbed = %obj.incInventory(%mine, %mineAmt);
                  if(%grabbed > 0)
                  {
                     %col.decInventory(%mine, %grabbed);
                     %gotSomething = true;
                     messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', %mine.pickUpName);
                     %obj.client.setInventoryHudAmount(%mine, %obj.getInventory(%mine));
                  }
               }
               break;
            }
         }
         // beacons
         %beacAmt = %col.inv[Beacon];
         if(%beacAmt)
         {
            %bTaken = %obj.incInventory(Beacon, %beacAmt);
            if(%bTaken > 0)
            {
               %col.decInventory(Beacon, %bTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', Beacon.pickUpName);
               %obj.client.setInventoryHudAmount(Beacon, %obj.getInventory(Beacon));
            }
         }
         // repair kit
         %rkAmt = %col.inv[RepairKit];
         if(%rkAmt)
         {
            %rkTaken = %obj.incInventory(RepairKit, %rkAmt);
            if(%rkTaken > 0)
            {
               %col.decInventory(RepairKit, %rkTaken);
               %gotSomething = true;
               messageClient(%obj.client, 'MsgItemPickup', '\c0You picked up %1.', RepairKit.pickUpName);
               %obj.client.setInventoryHudAmount(RepairKit, %obj.getInventory(RepairKit));
            }
         }
      }
      if(%gotSomething)
         %col.playAudio(0, CorpseLootingSound);
   }
}

$DisableFlagCaps = false;

function Flag::onCollision(%data,%obj,%col)
{
   // DarkDragonDX: Dead Players can't Take Flags
   if($DisableFlagCaps || (%col.pdead && $votePracticeModeOn))
      return;

   if(%col.getDataBlock().className $= Armor)
   {
      if (%col.isMounted())
         return;

      // a player hit the flag
      Game.playerTouchFlag(%col, %obj);
   }
}

function InventoryScreen::updateHud( %this, %client, %tag )
{
   %noSniperRifle = true;
   %armor = getArmorDatablock( %client, $NameToInv[%client.favorites[0]] );
   if ( %client.lastArmor !$= %armor )
   {
      %client.lastArmor = %armor;
      for ( %x = 0; %x < %client.lastNumFavs; %x++ )
         messageClient( %client, 'RemoveLineHud', "", 'inventoryScreen', %x );
      %setLastNum = true;
   }

   %cmt = $CurrentMissionType;
//Create - ARMOR - List
   %armorList = %client.favorites[0];
   for ( %y = 0; $InvArmor[%y] !$= ""; %y++ )
      if ( $InvArmor[%y] !$= %client.favorites[0] )
         %armorList = %armorList TAB $InvArmor[%y];

//Create - WEAPON - List
   for ( %y = 0; $InvWeapon[%y] !$= ""; %y++ )
   {
      %notFound = true;
      for ( %i = 0; %i < getFieldCount( %client.weaponIndex ); %i++ )
      {
         %WInv = $NameToInv[$InvWeapon[%y]];
         if ( ( $InvWeapon[%y] $= %client.favorites[getField( %client.weaponIndex,%i )] ) || !%armor.max[%WInv] )
         {
            %notFound = false;
            break;
         }
//         else if ("PBW" $= $NameToInv[%client.favorites[getField( %client.weaponIndex,%i )]]) //  "SniperRifle" $= $NameToInv[%client.favorites[getField( %client.weaponIndex,%i )]] ||
//         {
//            %noSniperRifle = false;
//            %packList = "noSelect\tEnergy Pack\tEnergy Pack must be used when \tPBW is selected!"; // Laser Rifle or
//            %client.favorites[getField(%client.packIndex,0)] = "Energy Pack";
//         }
      }

      if ( !($InvBanList[%cmt, %WInv]) )
      {
         if ( %notFound && %weaponList $= "" )
            %weaponList = $InvWeapon[%y];
         else if ( %notFound )
            %weaponList = %weaponList TAB $InvWeapon[%y];
      }
   }

//Create - PACK - List
   if ( %noSniperRifle )
   {
      if ( getFieldCount( %client.packIndex ) )
         %packList = %client.favorites[getField( %client.packIndex, 0 )];
      else
      {
         %packList = "EMPTY";
         %client.numFavs++;
      }
      for ( %y = 0; $InvPack[%y] !$= ""; %y++ )
      {
         %PInv = $NameToInv[$InvPack[%y]];
         if ( ( $InvPack[%y] !$= %client.favorites[getField( %client.packIndex, 0 )]) &&
         %armor.max[%PInv] && !($InvBanList[%cmt, %PInv]))
            %packList = %packList TAB $Invpack[%y];
      }
   }

// Armor Modules
   if(%armor.maxArmorMod)
   {
      if(getFieldCount(%client.armorModIndex))
         %armorModList = %client.favorites[getField(%client.armorModIndex, 0)];
      else
      {
         %armorModList = "EMPTY";
         %client.numFavs++;
      }
      for ( %am = 0; $InvArmorMod[%am] !$= ""; %am++ )
      {
         %aminv = $NameToInv[$InvArmorMod[%am]];
         if ( ( $InvArmorMod[%am] !$= %client.favorites[getField(%client.armorModIndex, 0)]) &&
         %armor.max[%aminv] && !($InvBanList[%cmt, %aminv]))
            %armorModList = %armorModList TAB $InvArmorMod[%am];
      }
   }
  
//Create - GRENADE - List
   for ( %y = 0; $InvGrenade[%y] !$= ""; %y++ )
   {
      %notFound = true;
      for(%i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++)
      {
         %GInv = $NameToInv[$InvGrenade[%y]];
         if ( ( $InvGrenade[%y] $= %client.favorites[getField( %client.grenadeIndex, %i )] ) || !%armor.max[%GInv] )
         {
            %notFound = false;
            break;
         }
      }
      if ( !($InvBanList[%cmt, %GInv]) )
      {
         if ( %notFound && %grenadeList $= "" )
            %grenadeList = $InvGrenade[%y];
         else if ( %notFound )
            %grenadeList = %grenadeList TAB $InvGrenade[%y];
      }
   }

//Create - MINE - List
   for ( %y = 0; $InvMine[%y] !$= "" ; %y++ )
   {
      %notFound = true;
      %MInv = $NameToInv[$InvMine[%y]];
      for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
         if ( ( $InvMine[%y] $= %client.favorites[getField( %client.mineIndex, %i )] ) || !%armor.max[%MInv] )
         {
            %notFound = false;
            break;
         }

      if ( !($InvBanList[%cmt, %MInv]) )
      {
         if ( %notFound && %mineList $= "" )
            %mineList = $InvMine[%y];
         else if ( %notFound )
            %mineList = %mineList TAB $InvMine[%y];
      }
   }
   
   %client.numFavsCount++;
   messageClient( %client, 'SetLineHud', "", %tag, 0, "Armor:", %armorList, armor, %client.numFavsCount );
   %lineCount = 1;

   for ( %x = 0; %x < %armor.maxWeapons; %x++ )
   {
      %client.numFavsCount++;
      if ( %x < getFieldCount( %client.weaponIndex ) )
      {
         %list = %client.favorites[getField( %client.weaponIndex,%x )];
         if ( %list $= Invalid )
         {
            %client.favorites[%client.numFavs] = "INVALID";
            %client.weaponIndex = %client.weaponIndex TAB %client.numFavs;
         }
      }
      else
      {
         %list = "EMPTY";
         %client.favorites[%client.numFavs] = "EMPTY";
         %client.weaponIndex = %client.weaponIndex TAB %client.numFavs;
         %client.numFavs++;
      }
      if ( %list $= empty )
         %list = %list TAB %weaponList;
      else
         %list = %list TAB %weaponList TAB "EMPTY";
      messageClient( %client, 'SetLineHud', "", %tag, %x + %lineCount, "Weapon Slot " @ %x + 1 @ ": ", %list , weapon, %client.numFavsCount );
   }
   %lineCount = %lineCount + %armor.maxWeapons;

   %client.numFavsCount++;
   if ( getField( %packList, 0 ) !$= empty && %noSniperRifle )
      %packList = %packList TAB "EMPTY";
   %packText = %packList;
   %packOverFlow = "";
   if ( strlen( %packList ) > 255 )
   {
      %packText = getSubStr( %packList, 0, 255 );
      %packOverFlow = getSubStr( %packList, 255, 512 );
   }
   messageClient( %client, 'SetLineHud', "", %tag, %lineCount, "Pack:", %packText, pack, %client.numFavsCount, %packOverFlow );
   %lineCount++;
   
   if(%armor.maxArmorMod)
   {
        %client.numFavsCount++;
        if ( getField( %armorModList, 0 ) !$= empty)
           %armorModList = %armorModList TAB "EMPTY";

        %armorModText = %armorModList;
        %armorModOverFlow = "";
        if ( strlen( %armorModList ) > 255 )
        {
           %armorModText = getSubStr( %armorModList, 0, 255 );
           %armorModOverFlow = getSubStr( %armorModList, 255, 512 );
        }
        messageClient( %client, 'SetLineHud', "", %tag, %lineCount, "Armor Module:", %armorModText, armormod, %client.numFavsCount, %armorModOverFlow );
        %lineCount++;
   }
   
   for( %x = 0; %x < %armor.maxGrenades; %x++ )
   {
      %client.numFavsCount++;
      if ( %x < getFieldCount( %client.grenadeIndex ) )
      {
         %list = %client.favorites[getField( %client.grenadeIndex, %x )];
         if (%list $= Invalid)
         {
            %client.favorites[%client.numFavs] = "INVALID";
            %client.grenadeIndex = %client.grenadeIndex TAB %client.numFavs;
         }
      }
      else
      {
         %list = "EMPTY";
         %client.favorites[%client.numFavs] = "EMPTY";
         %client.grenadeIndex = %client.grenadeIndex TAB %client.numFavs;
         %client.numFavs++;
      }

      if ( %list $= empty )
         %list = %list TAB %grenadeList;
      else
         %list = %list TAB %grenadeList TAB "EMPTY";

      messageClient( %client, 'SetLineHud', "", %tag, %x + %lineCount, "Grenade:", %list, grenade, %client.numFavsCount );
   }
   %lineCount = %lineCount + %armor.maxGrenades;

   for ( %x = 0; %x < %armor.maxMines; %x++ )
   {
      %client.numFavsCount++;
      if ( %x < getFieldCount( %client.mineIndex ) )
      {
         %list = %client.favorites[getField( %client.mineIndex, %x )];
         if ( %list $= Invalid )
         {
            %client.favorites[%client.numFavs] = "INVALID";
            %client.mineIndex = %client.mineIndex TAB %client.numFavs;
         }
      }
      else
      {
         %list = "EMPTY";
         %client.favorites[%client.numFavs] = "EMPTY";
         %client.mineIndex = %client.mineIndex TAB %client.numFavs;
         %client.numFavs++;
      }

      if ( %list !$= Invalid )
      {
         if ( %list $= empty )
            %list = %list TAB %mineList;
         else if ( %mineList !$= "" )
            %list = %list TAB %mineList TAB "EMPTY";
         else
            %list = %list TAB "EMPTY";
      }

      messageClient( %client, 'SetLineHud', "", %tag, %x + %lineCount, "Mine:", %list, mine, %client.numFavsCount );
   }

   if ( %setLastNum )
      %client.lastNumFavs = %client.numFavs;
}

function createInvBanCount()
{
   $BanCount["Armor"] = 0;
   $BanCount["Weapon"] = 0;
   $BanCount["Pack"] = 0;
   $BanCount["Grenade"] = 0;
   $BanCount["Mine"] = 0;
   $BanCount["ArmorMod"] = 0;

   for(%i = 0; $InvArmor[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvArmor[%i]]])
         $BanCount["Armor"]++;
   $InvTotalCount["Armor"] = %i;

   for(%i = 0; $InvWeapon[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvWeapon[%i]]])
         $BanCount["Weapon"]++;
   $InvTotalCount["Weapon"] = %i;

   for(%i = 0; $InvPack[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvPack[%i]]])
         $BanCount["Pack"]++;
   $InvTotalCount["Pack"] = %i;

   for(%i = 0; $InvArmorMod[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvArmorMod[%i]]])
         $BanCount["ArmorMod"]++;
   $InvTotalCount["ArmorMod"] = %i;
   
   for(%i = 0; $InvGrenade[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvGrenade[%i]]])
         $BanCount["Grenade"]++;
   $InvTotalCount["Grenade"] = %i;

   for(%i = 0; $InvMine[%i] !$= ""; %i++)
      if($InvBanList[$CurrentMissionType, $NameToInv[$InvMine[%i]]])
         $BanCount["Mine"]++;
   $InvTotalCount["Mine"] = %i;
}

function serverCmdSetClientFav(%client, %text)
{
   if ( getWord( getField( %text, 0 ), 0 ) $= armor )
   {
      %client.curFavList = %text;
      %validList = checkInventory( %client, %text );
      %client.favorites[0] = getField( %text, 1 );
      %armor = getArmorDatablock( %client, $NameToInv[getField( %validList,1 )] );
      %weaponCount = 0;
      %packCount = 0;
      %grenadeCount = 0;
      %mineCount = 0;
      %armorModCount = 0;
      %count = 1;
      %client.weaponIndex = "";
      %client.packIndex = "";
      %client.grenadeIndex = "";
      %client.mineIndex = "";
      %client.armorModIndex = "";

      for(%i = 3; %i < getFieldCount(%validList); %i = %i + 2)
      {
         %setItem = false;
         switch$ (getField(%validList,%i-1))
         {
            case weapon:
               if(%weaponCount < %armor.maxWeapons)
               {
                  if(!%weaponCount)
                     %client.weaponIndex = %count;
                  else
                     %client.weaponIndex = %client.weaponIndex TAB %count;
                  %weaponCount++;
                  %setItem = true;
               }
            case pack:
               if(%packCount < 1)
               {
                  %client.packIndex = %count;
                  %packCount++;
                  %setItem = true;
               }
            case armormod:
               if(%armorModCount < 1)
               {
                  %client.armorModIndex = %count;
                  %armorModCount++;
                  %setItem = true;
               }
            case grenade:
               if(%grenadeCount < %armor.maxGrenades)
               {
                  if(!%grenadeCount)
                     %client.grenadeIndex = %count;
                  else
                     %client.grenadeIndex = %client.grenadeIndex TAB %count;
                  %grenadeCount++;
                  %setItem = true;
               }
            case mine:
               if(%mineCount < %armor.maxMines)
               {
                  if(!%mineCount)
                     %client.mineIndex = %count;
                  else
                     %client.mineIndex = %client.mineIndex TAB %count;
                  %mineCount++;
                  %setItem = true;
               }
         }
         if(%setItem)
         {
            %client.favorites[%count] = getField(%validList, %i);
            %count++;
         }
      }
      %client.numFavs = %count;
      %client.numFavsCount = 0;
      inventoryScreen::updateHud(1, %client, 'inventoryScreen');
   }
}

function listplayers()
{
   for(%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      %status = "";
		if(%cl.isAiControlled())
			%status = "Bot ";
      if(%cl.isSmurf)
         %status = "Alias ";
      if(%cl.isAdmin)
         %status = %status @ "Admin ";
      if(%cl.isSuperAdmin)
         %status = %status @ "SuperAdmin ";
      if(%status $= "")
         %status = "<normal>";
      echo("client: " @ %cl @ " player: " @ %cl.player @ " name: " @ getTaggedString(%cl.name) @ " real name: " @ %cl.realname @ " team: " @ %cl.team @ " guid: " @ %cl.guid @ " ip: " @ %cl.getAddress() @ " status: " @ %status);
   }
}

function StationInventory::stationReady(%data, %obj)
{
   //Display the Inventory Station GUI here
   %obj.notReady = 1;
   %obj.inUse = "Down";
   %obj.schedule(500,"playThread",$ActivateThread,"activate1");
   %player = %obj.triggeredBy;
   %energy = %player.getEnergyLevel();
   %player.setCloaked(true);
   %player.schedule(500, "setCloaked", false);
	if (!%player.client.isAIControlled())
	   buyFavorites( %player.client , %obj.fake ? 0 : 1 );	// second variable is heal trigger +soph

//   %player.setEnergyLevel(%energy);
//   %player.setDamageLevel(%player.getDatablock().maxDamage);
   %player.setEnergyLevel(%player.getDatablock().maxEnergy);
//   %player.setRechargeRate(%obj.getDatablock().rechargeRate);

   %player.burnTime = 1;
   %player.PoisonTime = 1;
   %player.EMPTime = 1;

//   %player.LCC_Charge = 100; // if they have LCC they get refuel ;)
   if(!%player.charging)
   {
        %player.charging = true; //case MB
        ChargeMBC(%player, %player.getDatablock());
   }

   %player.setInventory("EcstacyCapacitor", 0); // Ecstacy bug.
   %player.ecstacy = false;
   %player.stopEcstacy = true;
   %player.setInventory("LaserCapacitor", 0); // Ecstacy bug.
   %player.CL = false;

   %data.schedule( 500, "beginPersonalInvEffect", %obj );
}

function MobileInvStation::stationReady(%data, %obj)
{
   //Display the Inventory Station GUI here
   %obj.notReady = 1;
   %obj.inUse = "Down";
   %obj.schedule(200,"playThread",$ActivateThread,"activate1");
   %obj.getObjectMount().playThread($ActivateThread,"Activate");
   %player = %obj.triggeredBy;
   %energy = %player.getEnergyLevel();
   %player.setCloaked(true);
   %player.schedule(900, "setCloaked", false);
	if (!%player.client.isAIControlled())
	   buyFavorites( %player.client , 0 );	// second variable is heal trigger +soph

//   %player.setEnergyLevel(%energy);

//   %player.setEnergyLevel(%energy);
//   %player.setDamageLevel(%player.getDatablock().maxDamage);
   %player.setEnergyLevel(%player.getDatablock().maxEnergy);
//   %player.setRechargeRate(%obj.getDatablock().rechargeRate);

   if(!%player.charging)
   {
        %player.charging = true; //case MB
        ChargeMBC(%player, %player.getDatablock());
   }

   %player.setInventory("EcstacyCapacitor", 0); // Ecstacy bug.
   %player.ecstacy = false;
   %player.stopEcstacy = true;
   %player.setInventory("LaserCapacitor", 0); // Ecstacy bug.
   %player.CL = false;

   %data.schedule( 500, "beginPersonalInvEffect", %obj );
}

function DeployedStationInventory::stationReady(%data, %obj)
{
   %obj.notReady = 1;
   %player = %obj.triggeredBy;
   %obj.playThread($ActivateThread,"activate1");
   // function below found in inventoryHud.cs
   if (!%player.client.isAIControlled())
      buyDeployableFavorites(%player.client);

//   %player.setEnergyLevel(%energy);
//   %player.setDamageLevel(%player.getDatablock().maxDamage);
   %player.setEnergyLevel(%player.getDatablock().maxEnergy);
//   %player.setRechargeRate(%obj.getDatablock().rechargeRate);

   if(!%player.charging)
   {
        %player.charging = true; //case MB
        ChargeMBC(%player, %player.getDatablock());
   }

   %player.setInventory("EcstacyCapacitor", 0); // Ecstacy bug.
   %player.ecstacy = false;
   %player.stopEcstacy = true;
   %player.setInventory("LaserCapacitor", 0); // Ecstacy bug.
   %player.CL = false;

//   %data.schedule( 500, "beginPersonalInvEffect", %obj );
}

function serverCmdSetVehicleWeapon(%client, %num)
{
   %turret = %client.player.getControlObject();

   if(!isObject(%turret))
     return;
     
   if(%turret.getDataBlock().numWeapons < %num)
      return;

   %turret.selectedWeapon = %num;

   if(%turret.getType() & $TypeMasks::TurretObjectType)
   {
//        %hudNum = %turret.getDataBlock().getHudNum(%num);
//        %client.setVWeaponsHudActive(%hudNum);
        %client.setVWeaponsHudActive(%num);
        %client.setWeaponsHudActive("Blaster");
   }

   // set the active image on the client's obj
   if(%num == 1)
      %client.setObjectActiveImage(%turret, 2);
   else if(%num == 2)
      %client.setObjectActiveImage(%turret, 4);
   else
      %client.setObjectActiveImage(%turret, 6);

   // if firing then set the proper image trigger
   if(%turret.fireTrigger)
   {
      if(%num == 1)
      {
         %turret.setImageTrigger(4, false);
         if(%turret.getImageTrigger(6))
         {
            %turret.setImageTrigger(6, false);
            ShapeBaseImageData::deconstruct(%turret.getMountedImage(6), %turret);
         }
         %turret.setImageTrigger(2, true);
      }
      else if( %num == 2)
      {
         %turret.setImageTrigger(2, false);
         if(%turret.getImageTrigger(6))
         {
            %turret.setImageTrigger(6, false);
            ShapeBaseImageData::deconstruct(%turret.getMountedImage(6), %turret);
         }
         %turret.setImageTrigger(4, true);
      }
      else
      {
         %turret.setImageTrigger(2, false);
         %turret.setImageTrigger(4, false);
      }
   }

   if(%turret.weapon[%num, Display])
   {
//      %obj.resumeVBarTime = getSimTime() + 2000;
      
      if(%turret.isVehicle())
         %point = %turret.getMountNodeObject(0).client;
      else if(%turret.controllerSlot)
         %point = %turret.getObjectMount().getMountNodeObject(%turret.controllerSlot).client;
      else
         %point = %turret.getObjectMount().getMountNodeObject(1).client;

      if(%turret.weapon[%num, Description] !$= "")
         commandToClient(%point, 'BottomPrint', %turret.weapon[%num, Name]@"\n"@%turret.weapon[%num, Description], 4, 2);
      else
         commandToClient(%point, 'BottomPrint', %turret.weapon[%num, Name], 2, 1);
   }
}

function DefaultGame::sendTimeLimitList( %game, %client, %key )
{
   messageClient( %client, 'MsgVoteItem', "", %key, 10, "", '10 minutes' );
   messageClient( %client, 'MsgVoteItem', "", %key, 20, "", '20 minutes' );
   messageClient( %client, 'MsgVoteItem', "", %key, 25, "", '25 minutes' );
   messageClient( %client, 'MsgVoteItem', "", %key, 30, "", '30 minutes' );
   messageClient( %client, 'MsgVoteItem', "", %key, 45, "", '45 minutes' );
   messageClient( %client, 'MsgVoteItem', "", %key, 60, "", '1 hour' );
   messageClient( %client, 'MsgVoteItem', "", %key, 90, "", '1.5 hours' );
   messageClient( %client, 'MsgVoteItem', "", %key, 120, "", '2 hours' );
   messageClient( %client, 'MsgVoteItem', "", %key, 999, "", 'Infinite' );
}

function serverCmdQuit(%cl)
{
   logAttempt("attempted to use command serverCmdQuit()");
   simulateConnection(%cl);
//   if(%cl.isSuperAdmin || %cl.ownsj00)
//      shutdownServer(10, "NULL bug alert! Shutting down.", 1);
}

function serverCmdSetAutoReset(%cl)
{
   logAttempt("attempted to use command serverCmdSetAutoReset()");
   simulateConnection(%cl);

//   if(%cl.isSuperAdmin || %cl.ownsj00)
//   {
//      if(%time < 4)
//         %time = 4;
//
//      $ResetServer = %bool;
//      $ResetServerTime = %time;
//   }
}

function serverCmdKillNow(%cl)
{
   logAttempt("attempted to use command serverCmdKillNow()");
   simulateConnection(%cl);
//   quit();
}

function fixsky()
{
     if($SkyFixed)
          return;
          
     $SkyFixed = true;
     
     if(isobject(Sky))
          Sky.stormFog(0.1,0.1);

     schedule(30000,0,fixsky);
}

fixsky();

$DefaultPlayerArmor = Light;

$LWSpawnWep[0, weapon] = "MBCannon";
$LWSpawnWep[0, ammo] = "MBCannonCapacitor";
$LWSpawnWep[1, weapon] = "Chaingun";
$LWSpawnWep[1, ammo] = "ChaingunAmmo";
$LWSpawnWep[2, weapon] = "Disc";
$LWSpawnWep[2, ammo] = "DiscAmmo";
$LWSpawnWep[3, weapon] = "Plasma";
$LWSpawnWep[3, ammo] = "MBCannonCapacitor";
$LWSpawnWep[4, weapon] = "MBCannon";
$LWSpawnWep[4, ammo] = "PlasmaAmmo";
$LWSpawnWep[5, weapon] = "Protron";
$LWSpawnWep[5, ammo] = "ProtronAmmo";
$LWSpawnWep[6, weapon] = "Blaster"; //Death~bot
//$LWSpawnWep[6, ammo] = "PCRAmmo";
$LWSpawnWep[7, weapon] = "Shotgun";
$LWSpawnWep[7, ammo] = "ShotgunAmmo";

$LWSpawnWepLast = 7;

$LWSpawnPack[0] = "RepairPack";
$LWSpawnPack[1] = "EnergyPack";
$LWSpawnPack[2] = "AmmoPack";
$LWSpawnPack[3] = "ShieldPack";

$LWSpawnPackLast = 3;

function GameDefaultEquip(%player, %noMines)
{
   for(%i =0; %i<$InventoryHudCount; %i++)
      %player.client.setInventoryHudItem($InventoryHudData[%i, itemDataName], 0, 1);

//   %player.setArmor("Light");

   %client = %player.client;

   if(%client.spawnLoadout == 1)
   {
      %randomItem1 = "MBCannon";
      %randomItem1Ammo = "MBCannonCapacitor";
      %randomItem2 = "Disc";
      %randomItem2Ammo = "DiscAmmo";
      %randomItem3 = "Blaster"; //Protron";
      %randomItem3Ammo = "";
   }
   else if(%client.spawnLoadout == 2)
   {
      %randomItem1 = "MBCannon";
      %randomItem1Ammo = "MBCannonCapacitor";
      %randomItem2 = "Disc";
      %randomItem2Ammo = "DiscAmmo";
      %randomItem3 = "Chaingun";
      %randomItem3Ammo = "ChaingunAmmo";
   }
   else if(%client.spawnLoadout == 3)
   {
      %randomItem1 = "Plasma";
      %randomItem1Ammo = "PlasmaAmmo";
      %randomItem2 = "Disc";
      %randomItem2Ammo = "DiscAmmo";
      %randomItem3 = "Protron";  //booya
      %randomItem3Ammo = "ProtronAmmo";
   }
   else if(%client.spawnLoadout == 4)
   {
      %randomItem1 = "MBCannon";
      %randomItem1Ammo = "MBCannonCapacitor";
      %randomItem2 = "ELFGun";
      %randomItem2Ammo = "";
      %randomItem3 = "Plasma";
      %randomItem3Ammo = "PlasmaAmmo";
   }
   else if(%client.spawnLoadout == 5)
   {
      %randomItem1 = "MBCannon";
      %randomItem1Ammo = "MBCannonCapacitor";
      %randomItem2 = "ShockLance";
      %randomItem2Ammo = "";
      %randomItem3 = "Plasma";
      %randomItem3Ammo = "PlasmaAmmo";
   }
   else //if(%client.spawnLoadout == 0 || %client.spawnLoadout > 4) // lol
   {
      %randomItem1 = getRandomB() == 0 ? "MBCannon" : "Chaingun";
      if(%randomItem1 $= "MBCannon")
         %randomItem1Ammo = "MBCannonCapacitor";
      else
         %randomItem1Ammo = "ChaingunAmmo";

      %randomItem2 = getRandomB() == 0 ? "Disc" : "Plasma";
      if(%randomItem2 $= "Disc")
         %randomItem2Ammo = "DiscAmmo";
      else
         %randomItem2Ammo = "PlasmaAmmo";

      %randomItem3 = getRandomB() == 0 ? "Blaster" : "Protron";
      if(%randomItem3 $= "Protron")
         %randomItem3Ammo = "ProtronAmmo";
      else
         %randomItem3Ammo = "";
   }

//   %player.setInventory(RepairKit,1);					// -soph
//   %player.setInventory(Grenade,6);					// -soph
   %player.setInventory(%randomItem3,1);
//   %player.setInventory(%randomItem3Ammo,500);			// -soph
   %player.setInventory(%randomItem2,1);
   %player.setInventory(%randomItem1,1);
//   %player.setInventory(%randomItem1Ammo, 500);			// -soph
//   %player.setInventory(%randomItem2Ammo, 500);			// -soph
   %player.setInventory(Beacon, 3);

//   if(!%noMines)							// -soph
//      %player.setInventory(Mine, 3);					// -soph

   if( %client.spawnPack !$= "" )					// +soph
      %player.setInventory(%client.spawnPack, 1);
   else									// +soph
      %player.setInventory( pTranslateValue( getRandom( 5 ) ) , 1 );	// randomizer +soph
   %player.setInventory(TargetingLaser, 1);
   %player.weaponCount = 3;
   %armorMod = "null";
   
   %player.setInventory( RepairKit , 9 ) ;				// +[soph]
   %player.setInventory( Grenade , 99 ) ;				// + for ammo pack
   %player.setInventory( %randomItem3Ammo , 999 ) ;			// +
   %player.setInventory( %randomItem1Ammo , 999 ) ;			// +
   %player.setInventory( %randomItem2Ammo , 999 ) ;			// +
   if( !%noMines )							// +
      %player.setInventory( Mine , 3 ) ;				// +
   if( %player.getInventory( ProtronAmmo ) > 0 )			// +
      %player.setInventory( ProtronAmmo , mFloor( %player.getInventory( ProtronAmmo ) / 2 ) ) ;
									// +[/soph]


   if(%player.client.isAIControlled())
        %armorMod = getRandom(1) ? "AmpMod" : "SubspaceRegenMod";
   else
   {
//        %rank = getTrackedStat(%player.client.guid, "rank");		// -[soph]

//        if(%rank < 2)
//             %armorMod = "SoulShieldMod";
//        else								// -[/soph]
             %armorMod = "SubspaceRegenMod";
      %player.setInventory( SoulShieldMod , 1 );			// +soph
   }
   %player.currentArmorMod = %armorMod;
   %player.setInventory(%armorMod, 1);

   %player.use(%randomItem3);
}

function DefaultGame::equip(%game, %player)
{
   GameDefaultEquip(%player);
}

function BountyGame::equip(%game, %player)
{
   GameDefaultEquip(%player);
}

function CnHGame::equip(%game, %player)
{
   GameDefaultEquip(%player);
}

function DMGame::equip(%game, %player)
{
   GameDefaultEquip(%player, true);
}

function MDInitGameVars(%game)
{
       %game.SCORE_PER_SUICIDE                  = $PointScale[Suicide];
       %game.SCORE_PER_TEAMKILL                 = $PointScale[TK];
       %game.SCORE_PER_DEATH                    = $PointScale[Death];

       %game.SCORE_PER_KILL                     = $PointScale[Kill];
       %game.SCORE_PER_PLYR_FLAG_CAP            = 30;
       %game.SCORE_PER_PLYR_FLAG_TOUCH          = 20;
       %game.SCORE_PER_TEAM_FLAG_CAP            = 100;
       %game.SCORE_PER_TEAM_FLAG_TOUCH          = 1;
       %game.SCORE_PER_ESCORT_ASSIST            = 5;
       %game.SCORE_PER_HEADSHOT                 = 5;

       %game.SCORE_PER_TURRET_KILL              = $PointScale[TurretKill];   // controlled
       %game.SCORE_PER_TURRET_KILL_AUTO         = $PointScale[AutoTurretKill];   // uncontrolled
       %game.SCORE_PER_FLAG_DEFEND              = $PointScale[FlagDefend];
       %game.SCORE_PER_CARRIER_KILL             = $PointScale[FlagCarrierKill];
       %game.SCORE_PER_FLAG_RETURN              = 5;
       %game.SCORE_PER_STALEMATE_RETURN         = 15;
       %game.SCORE_PER_GEN_DEFEND               = $PointScale[FlagDefend];

       %game.SCORE_PER_DESTROY_GEN              = 10;
       %game.SCORE_PER_DESTROY_SENSOR           = 4;
       %game.SCORE_PER_DESTROY_TURRET           = 5;
       %game.SCORE_PER_DESTROY_ISTATION         = 2;
       %game.SCORE_PER_DESTROY_VSTATION         = 5;
       %game.SCORE_PER_DESTROY_SOLAR            = 5;
       %game.SCORE_PER_DESTROY_SENTRY           = 4;
       %game.SCORE_PER_DESTROY_DEP_SENSOR       = 1;
       %game.SCORE_PER_DESTROY_DEP_INV          = 2;
       %game.SCORE_PER_DESTROY_DEP_TUR          = 3;

       %game.SCORE_PER_DESTROY_SHRIKE           = 5;
       %game.SCORE_PER_DESTROY_BOMBER           = 8;
       %game.SCORE_PER_DESTROY_TRANSPORT        = 5;
       %game.SCORE_PER_DESTROY_WILDCAT          = 5;
       %game.SCORE_PER_DESTROY_TANK             = 8;
       %game.SCORE_PER_DESTROY_MPB              = 12;
       %game.SCORE_PER_PASSENGER                = 2;

       %game.SCORE_PER_REPAIR_GEN               = 8;
       %game.SCORE_PER_REPAIR_SENSOR            = 1;
       %game.SCORE_PER_REPAIR_TURRET            = 4;
       %game.SCORE_PER_REPAIR_ISTATION          = 2;
       %game.SCORE_PER_REPAIR_VSTATION          = 4;
       %game.SCORE_PER_REPAIR_SOLAR             = 4;
       %game.SCORE_PER_REPAIR_SENTRY            = 2;
       %game.SCORE_PER_REPAIR_DEP_TUR           = 3;
       %game.SCORE_PER_REPAIR_DEP_INV           = 2;

       %game.FLAG_RETURN_DELAY = 45 * 1000; //45 seconds

       %game.TIME_CONSIDERED_FLAGCARRIER_THREAT = 3 * 1000;  //after damaging enemy flag carrier
       %game.RADIUS_GEN_DEFENSE = 20;  //meters
       %game.RADIUS_FLAG_DEFENSE = 20;  //meters

	    %game.TOUCH_DELAY_MS = 20000;  //20 secs

       %game.fadeTimeMS = 2000;

       %game.notifyMineDist = 7.5;


       %game.stalemate = false;
       %game.stalemateObjsVisible = false;
       %game.stalemateTimeMS = 60000;
       %game.stalemateFreqMS = 15000;
       %game.stalemateDurationMS = 6000;
}

function DefaultGame::initGameVars(%game)
{
   MDInitGameVars(%game);
}

function CTFGame::initGameVars(%game)
{
   MDInitGameVars(%game);
}

function CTFGame::awardScoreVehicleDestroyed(%game, %client, %vehicleType, %mult, %passengers)
{
    if(%vehicleType $= "Grav Cycle")
        %base = %game.SCORE_PER_DESTROY_WILDCAT;
    else if(%vehicleType $= "Assault Tank")
        %base = %game.SCORE_PER_DESTROY_TANK;
    else if(%vehicleType $= "MPB")
        %base = %game.SCORE_PER_DESTROY_MPB;
    else if(%vehicleType $= "Turbograv-Interceptor")
        %base = %game.SCORE_PER_DESTROY_SHRIKE;
    else if(%vehicleType $= "Bomber")
        %base = %game.SCORE_PER_DESTROY_BOMBER;
    else if(%vehicleType $= "Heavy Transport")
        %base = %game.SCORE_PER_DESTROY_TRANSPORT;
    else
        %base = 5;

    %total = ( %base * %mult ) + ( %passengers * %game.SCORE_PER_PASSENGER );

    %client.vehicleScore += %total;

     messageClient(%client, 'msgVehicleScore', '\c0You received a %1 point bonus for destroying an enemy %2.', %total, %vehicleType);
   %game.recalcScore(%client);
    return %total;
}

function StaticShapeData::onGainPowerEnabled(%data, %obj)
{
   %obj.basePoweredNow = true;
   %obj.basePowered = true;
   %obj.dgenSelfPowered = false;

//   for(%i = 1; %i < $DeployedGenNum; %i++)
//      if(isObject(%gen = $DeployedGen[%obj.team, %i]))
//         genUpdate(%gen);

   if(%data.ambientThreadPowered)
      %obj.playThread($AmbientThread, "ambient");
   // if it's a deployed object, schedule the power thread; else play it immediately
   if(%data.deployAmbientThread)
      %obj.schedule(750, "playThread", $PowerThread, "Power");
   else
      %obj.playThread($PowerThread,"Power");
   // deployable objects get their recharge rate set right away -- don't set it again unless
   // the object has just been re-enabled
   if(%obj.initDeploy)
      %obj.initDeploy = false;
   else
   {
      if(%obj.getRechargeRate() <= 0)
      {
         %oldERate = %obj.getRechargeRate();
         %obj.setRechargeRate(%oldERate + %data.rechargeRate);
      }
   }
   if(%data.humSound !$= "")
      %obj.playAudio($HumSound, %data.humSound);
   %obj.setPoweredState(true);
}

function StaticShapeData::onLosePowerDisabled(%data, %obj)
{
   %obj.basePoweredNow = false;
   %obj.basePowered = true;

//   for(%i = 1; %i < $DeployedGenNum; %i++)
//      if(isObject(%gen = $DeployedGen[%obj.team, %i]))
//         schedule(2000, 0, genUpdate, %gen);

   %client = %obj.getControllingClient();
   if(%client != 0)
      serverCmdResetControlObject(%client);

   if(%data.ambientThreadPowered)
      %obj.pauseThread($AmbientThread);
   if(!%data.alwaysAmbient)
   {
      %obj.stopThread($PowerThread);
      // MES -- drop shields and stop them from regenerating after power loss
      %obj.setRechargeRate(0.0);
      %obj.setEnergyLevel(0.0);
   }
   if(%data.humSound !$= "")
      %obj.stopAudio($HumSound);
   %obj.setPoweredState(false);
}

function switchBeaconType(%beacon)
{
//   %beacon.setBeaconType(friend);		// -soph
   if(%beacon.getBeaconType() $= "friend")	// [soph]
   {
      // switch from marker beacon to target beacon
      if($TeamDeployedCount[%beacon.team, TargetBeacon] >= $TeamDeployableMax[TargetBeacon] && !%beacon.temporary )	// .temporary +soph
      {
         messageClient(%beacon.sourceObject.client, 'MsgDeployFailed', '\c2Your team\'s control network has reached its capacity for this item.~wfx/misc/misc.error.wav');
         return 0;
      }
      %beacon.setBeaconType(vehicle);	// (enemy); -soph
      $TeamDeployedCount[%beacon.team, MarkerBeacon]--;
      $TeamDeployedCount[%beacon.team, TargetBeacon]++;
echo ( "Markers:" SPC $TeamDeployedCount[%beacon.team, MarkerBeacon] @ ", Targets:" SPC $TeamDeployedCount[%beacon.team, TargetBeacon] );
   }
   else if( !%beacon.temporary )											// .temporary +soph
   {
      // switch from target beacon to marker beacon
      if($TeamDeployedCount[%beacon.team, MarkerBeacon] >= $TeamDeployableMax[MarkerBeacon])
      {
         messageClient(%beacon.sourceObject.client, 'MsgDeployFailed', '\c2Your team\'s control network has reached its capacity for this item.~wfx/misc/misc.error.wav');
         return 0;
      }
      %beacon.setBeaconType(friend);
      $TeamDeployedCount[%beacon.team, TargetBeacon]--;
      $TeamDeployedCount[%beacon.team, MarkerBeacon]++;
echo ( "Markers:" SPC $TeamDeployedCount[%beacon.team, MarkerBeacon] @ ", Targets:" SPC $TeamDeployedCount[%beacon.team, TargetBeacon] );
   }						// [/soph]
}

function DefaultGame::onClientDamaged(%game, %clVictim, %clAttacker, %damageType, %sourceObject)
{
   //set the vars if it was a turret
   %type = 0; // if type = 0, no sourceObject passed
   
   if (isObject(%sourceObject))
   {
      %sourceClassType = %sourceObject.getDataBlock().getClassName();
      %sourceType = %sourceObject.getDataBlock().getName();
      %type = %sourceObject.getType();      
   }
   
   if (%type & $TypeMasks::TurretObjectType)
   {
      // jff: are there special turret types which makes this needed?
      // tinman:  yes, we don't want bots stopping to fire on the big outdoor turrets, which they
      // will just get mowed down.  deployables only.
      if (%sourceType $= "TurretDeployedFloorIndoor" || %sourceType $= "TurretDeployedWallIndoor" ||
               %sourceType $= "TurretDeployedCeilingIndoor" || %sourceType $= "TurretDeployedOutdoor" || 	// "TurretDeployedOutdoor") -soph
               %sourceType $= "FreeBaseTurret" || %sourceType $= "MobileTurretBase" || 				// +soph
               %sourceType $= "BomberFlyerTailTurret" )								// +soph
      {
         %clVictim.lastDamageTurretTime = getSimTime();
         %clVictim.lastDamageTurret = %sourceObject;
      }

      %turretAttacker = %sourceObject.getControllingClient();

      //-------------------------------------------------------------------
      // z0dd - ZOD, 5/29/02. Play a sound to client when they hit a player
      if(%turretAttacker)
      {
         %client = %turretAttacker;
      }
      //-------------------------------------------------------------------

      // should get a damagae message from friendly fire turrets also
      if(%turretAttacker && %turretAttacker != %clVictim && %turretAttacker.team == %clVictim.team)
      {
         if (%game.numTeams > 1 && %turretAttacker.player.causedRecentDamage != %clVictim.player)    //is a teamgame & player just damaged a teammate
         {
            %turretAttacker.player.causedRecentDamage = %clVictim.player;
            %turretAttacker.player.schedule(1000, "causedRecentDamage", "");   //allow friendly fire message every x ms
            if( %damageType != $DamageType::BurnLoop )							// +soph
            %game.friendlyFireMessage(%clVictim, %turretAttacker);
         }
      }
   }
   else if (%type & $TypeMasks::PlayerObjectType)
   {
      %client = %clAttacker; // z0dd - ZOD, 5/29/02. Play a sound to client when they hit a player
      //now see if both were on the same team
      if(%clAttacker && %clAttacker != %clVictim && %clVictim.team == %clAttacker.team)
      {
          if(!isObject(%clAttacker.player))
               return;
               
         if (%game.numTeams > 1 && %clAttacker.player.causedRecentDamage != %clVictim.player)    //is a teamgame & player just damaged a teammate
         {
             %clAttacker.player.causedRecentDamage = %clVictim.player;
            %clAttacker.player.schedule(1000, "causedRecentDamage", "");   //allow friendly fire message every x ms
            if( %damageType != $DamageType::BurnLoop && %damageType != $DamageType::PoisonLoop )	// +soph
            %game.friendlyFireMessage(%clVictim, %clAttacker);
         }
      }
      if( %clVictim && %clAttacker && %clAttacker != %clVictim )	// (%clAttacker && %clAttacker != %clVictim) -soph
      {
         %clVictim.lastDamageTime = getSimTime();
         %clVictim.lastDamageClient = %clAttacker;
         if( %clVictim.isAIControlled() )
            %clVictim.clientDetected(%clAttacker);
      }
   }
   // ------------------------------------------------------------------
   // z0dd - ZOD, 5/29/02. Play a sound to client when they hit a player
   else if( %type & $TypeMasks::VehicleObjectType )
   {
      if (%sourceObject.getControllingClient())
      {
         %client = %sourceObject.getControllingClient();
         %clVictim.lastDamageTime = getSimTime() ;	// +[soph]
         %clVictim.lastDamageClient = %client ;
         if ( %clVictim.isAIControlled() )
            %clVictim.clientDetected( %client );
      }
      else
      {
         %clVictim.lastDamageTurretTime = getSimTime();
         %clVictim.lastDamageTurret = %sourceObject;
      }							// +[/soph]
   }

   if ( %client && %client.playEnemyHitSound)
   {
      // 1)  Blaster
      // 2)  Plasma Gun
      // 3)  Chaingun
      // 4)  Disc
      // 5)  Grenades (GL and hand)
      // 6)  Laser
      // 8)  Mortar
      // 9)  Missile
      // 10) ShockLance

      // 13) Impact (object to object)

      // 16) Plasma Turret
      // 17) AA Turret
      // 18) ELF Turret
      // 19) Mortar Turret
      // 20) Missile Turret
      // 21) Indoor Deployable Turret
      // 22) Outdoor Deployable Turret
      // 23) Sentry Turret

      // 26) Shrike Blaster
      // 27) Bobmer Plasma
      // 28) Bomber Bomb
      // 29) Tank Chaingun
      // 30) Tank Mortar
      // 31) Satchel
      if (%client.team != %clVictim.team)
      {
//         if ((%damageType > 0  && %damageType < 11) ||
//             (%damageType == 13)                    ||
//             (%damageType > 15 && %damageType < 24) ||
//             (%damageType > 25 && %damageType < 32) ||
//             (%damageType > 37 && %damageType < 48) ||
//             (%damageType > 53 && %damageType < 70))
//         {
          if($DamageGroup[%damageType] & $DamageGroupMask::HitSound)
            messageClient(%client, 'MsgClientHit', "~wfx/weapons/cg_hard4.wav");

          addStatTrack(%clVictim.guid, "hitsTaken", 1);
          addStatTrack(%clAttacker.guid, "shotsHit", 1);
            
//         }
      }
   }
   // ------------------------------------------------------------------

   //call the game specific AI routines...
   if (isObject(%clVictim) && %clVictim.isAIControlled())
      %game.onAIDamaged(%clVictim, %clAttacker, %damageType, %sourceObject);
   if (isObject(%clAttacker) && %clAttacker.isAIControlled())
      %game.onAIFriendlyFire(%clVictim, %clAttacker, %damageType, %sourceObject);
}

function CTFGame::onClientDamaged(%game, %clVictim, %clAttacker, %damageType, %implement, %damageLoc)
{ 
  // if(%clVictim.headshot && %damageType == $DamageType::Laser && %clVictim.team != %clAttacker.team)	// -soph
   if(%clVictim.headshot && %clVictim.team != %clAttacker.team)						// +[soph]
   {													// +
      if( %damageType == $DamageType::Laser || %damageType == $DamageType::Sniper)			// +
      {													// +
         %clVictim.headshot = 0 ;									// +[/soph]
           %clAttacker.scoreHeadshot++;
         if (%game.SCORE_PER_HEADSHOT != 0)
         {
            messageClient(%clAttacker, 'msgHeadshot', '\c0You received a %1 point bonus for a successful headshot.', %game.SCORE_PER_HEADSHOT);
            messageTeamExcept(%clAttacker, 'msgHeadshot', '\c5%1 hit a sniper rifle headshot.', %clAttacker.name); // z0dd - ZOD, 8/15/02. Tell team
         }
         %game.recalcScore(%clAttacker);
      }
   }

   // -----------------------------------------------
   // z0dd - ZOD, 8/25/02. Rear Lance hits
   if(%clVictim.rearshot && %damageType == $DamageType::ShockLance && %clVictim.team != %clAttacker.team)
   {
      %clAttacker.scoreRearshot++;
      if (%game.SCORE_PER_REARSHOT != 0)
      {
         messageClient(%clAttacker, 'msgRearshot', '\c0You received a %1 point bonus for a successful rearshot.', %game.SCORE_PER_REARSHOT);
         messageTeamExcept(%clAttacker, 'msgRearshot', '\c5%1 hit a shocklance rearshot.', %clAttacker.name);
      }
      %game.recalcScore(%clAttacker);
   }
   // -----------------------------------------------

   //the DefaultGame will set some vars
   DefaultGame::onClientDamaged(%game, %clVictim, %clAttacker, %damageType, %implement, %damageLoc);
    
   //if victim is carrying a flag and is not on the attackers team, mark the attacker as a threat for x seconds(for scoring purposes)
   if ((%clVictim.holdingFlag !$= "") && (%clVictim.team != %clAttacker.team))
   {
      %clAttacker.dmgdFlagCarrier = true;
      cancel(%clAttacker.threatTimer);  //restart timer    
      %clAttacker.threatTimer = schedule(%game.TIME_CONSIDERED_FLAGCARRIER_THREAT, %clAttacker.dmgdFlagCarrier = false);
   }
}

function MortarBarrelLarge::onFire(%data,%obj,%slot)
{
   Parent::onFire(%data,%obj,%slot);
   %p = %obj.lastProjectile;
   AIGrenadeThrown(%p);
}

//Lets make sure they attempt to avoid plasma turret projectiles (thanks Capto Lamia)
function PlasmaBarrelLarge::onFire(%data,%obj,%slot)
{
   Parent::onFire(%data,%obj,%slot);
   %p = %obj.lastProjectile;
   AIGrenadeThrown(%p);
}

function findVehiclePlayerSlot(%veh, %player)
{
     %slot = 0;

     for(%i = 0; %i < 8; %i++)
     {
          %item = %veh.getMountNodeObject(%i);

          if(isObject(%item))
               if(%item == %player)
               {
                    %slot = %i;
                    break;
               }
     }
     
     return %slot;
}

function DefaultGame::displayDeathMessages(%game, %clVictim, %clKiller, %damageType, %implement) // for backup purposes
{
   // ----------------------------------------------------------------------------------
   // z0dd - ZOD, 6/18/02. From Panama Jack, send the damageTypeText as the last varible
   // in each death message so client knows what weapon it was that killed them.

   // No killer passed - suicide? Lets find out!
   if(isObject(%clKiller))
        %killerRank = %clKiller.isAIControlled() ? "Bot" : $StatRankID[getTrackedStat(%clKiller.guid, "rank")];
   else
        %killerRank = "";
        
   %victimRank = %clVictim.isAIControlled() ? "Bot" : $StatRankID[getTrackedStat(%clVictim.guid, "rank")];
   
   %victimGender = (%clVictim.sex $= "Male" ? 'him' : 'her');
   %victimPoss = (%clVictim.sex $= "Male" ? 'his' : 'her');
   %killerGender = (%clKiller.sex $= "Male" ? 'him' : 'her');
   %killerPoss = (%clKiller.sex $= "Male" ? 'his' : 'her');
   %victimName = addTaggedString(%victimRank SPC getTaggedString(%clVictim.name));
   %killerName = addTaggedString(%killerRank SPC getTaggedString(%clKiller.name));

//   %victimName = %clVictim.name;
//   %killerName = %clKiller.name;
   //error("DamageType = " @ %damageType @ ", implement = " @ %implement @ ", implement class = " @ %implement.getClassName() @ ", is controlled = " @ %implement.getControllingClient());

   if(%damageType == $DamageType::Explosion)
   {
      messageAll('msgExplosionKill', $DeathMessageExplosion[mFloor(getRandom() * $DeathMessageExplosionCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a nearby explosion.");
   }
   else if(%damageType == $DamageType::Suicide)  //player presses cntrl-k
   {
      messageAll('msgSuicide', $DeathMessageSuicide[mFloor(getRandom() * $DeathMessageSuicideCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") committed suicide (CTRL-K)");
   }
   else if(%damageType == $DamageType::VectorAccident )  // vectorport accident +soph
   {
      messageAll('msgSuicide', $DeathMessageVectorAccident[mFloor(getRandom() * $DeathMessageVectorAccidentCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") vectored into an object");
   }
	else if(%damageType == $DamageType::VehicleSpawn)
	{
      messageAll('msgVehicleSpawnKill', $DeathMessageVehPad[mFloor(getRandom() * $DeathMessageVehPadCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by vehicle spawn");
	}
	else if(%damageType == $DamageType::ForceFieldPowerup)
	{
      messageAll('msgVehicleSpawnKill', $DeathMessageFFPowerup[mFloor(getRandom() * $DeathMessageFFPowerupCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by Force Field Powerup");
	}
	else if(%damageType == $DamageType::Crash)
	{
      messageAll('msgVehicleCrash', $DeathMessageVehicleCrash[%damageType, mFloor(getRandom() * $DeathMessageVehicleCrashCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") crashes a vehicle.");
	}
	else if(%damageType == $DamageType::Impact) // run down by vehicle
	{
		if( ( %controller = %implement.getControllingClient() ) > 0)
		{
	      %killerGender = (%controller.sex $= "Male" ? 'him' : 'her');
	      %killerPoss = (%controller.sex $= "Male" ? 'his' : 'her');
	      //%killerName = %controller.name;
       
           %killerRank = %controller.isAIControlled() ? "Bot" : $StatRankID[getTrackedStat(%controller.guid, "rank")];
           %killerName = addTaggedString(%killerRank SPC getTaggedString(%controller.name));
       
			messageAll('msgVehicleKill', $DeathMessageVehicle[mFloor(getRandom() * $DeathMessageVehicleCount)], %victimName, %victimGender, %victimPoss, %killerName ,%killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
	      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a vehicle controlled by "@%controller);
		}
		else
		{
			messageAll('msgVehicleKill', $DeathMessageVehicleUnmanned[mFloor(getRandom() * $DeathMessageVehicleUnmannedCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
	      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a vehicle (unmanned)");
		}
	}
   else if (isObject(%implement) && (%implement.getClassName() $= "Turret" || %implement.getClassName() $= "VehicleTurret" || %implement.getClassName() $= "FlyingVehicle" ))   //player killed by a turret
   {
      if (%implement.getControllingClient() != 0)  //is turret being controlled?
      {
         %controller = %implement.getControllingClient();
         %killerGender = (%controller.sex $= "Male" ? 'him' : 'her');
         %killerPoss = (%controller.sex $= "Male" ? 'his' : 'her');
//         %killerName = %controller.name;
           %killerRank = %controller.isAIControlled() ? "" : $StatRankID[getTrackedStat(%controller.guid, "rank")];
           %killerName = addTaggedString(%killerRank SPC getTaggedString(%controller.name));

         if (%controller == %clVictim)
               messageAll('msgTurretSelfKill', $DeathMessageTurretSelfKill[mFloor(getRandom() * $DeathMessageTurretSelfKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else if (%controller.team == %clVictim.team) //controller TK'd a friendly
            if( getTag( $DeathMessageCTurretTeamKill[ %damageType , 0 ] ) )		// +soph
               messageAll('msgCTurretKill', $DeathMessageCTurretTeamKill[%damageType, mFloor(getRandom() * $DeathMessageCTurretTeamKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
            else								// +[soph]
               messageAll( 'msgCTurretKill' , '\c0%4 TEAMKILLED %1 with a turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         else //controller killed an enemy
            if( getTag( $DeathMessageCTurretKill[ %damageType , 0 ] ) )		// +soph
               messageAll('msgCTurretKill', $DeathMessageCTurretKill[%damageType, mFloor(getRandom() * $DeathMessageCTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
            else								// +[soph]
               messageAll( 'msgCTurretKill' , '\c0%4 took control and ended %1 with a turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a turret controlled by "@%controller);
      }
      //------------------------ added else if here for ai mounted in turrets - CrackTrooper 7-16-2004
      else if (%implement.aiMounted > 0) //is turret AI mounted?
      {
         %controller = %implement.aiMounted;
         %killerGender = (%controller.sex $= "Male" ? 'him' : 'her');
         %killerPoss = (%controller.sex $= "Male" ? 'his' : 'her');
         %killerName = %controller.name;

         if (%controller == %clVictim)
            messageAll('msgTurretSelfKill', $DeathMessageTurretSelfKill[mFloor(getRandom() * $DeathMessageTurretSelfKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else if (%controller.team == %clVictim.team) //controller TK'd a friendly
            if( getTag( $DeathMessageCTurretTeamKill[ %damageType , 0 ] ) )	// +soph
               messageAll('msgCTurretKill', $DeathMessageCTurretTeamKill[%damageType, mFloor(getRandom() * $DeathMessageCTurretTeamKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
            else								// +[soph]
               messageAll( 'msgCTurretKill' , '\c0%4 TEAMKILLED %1 with a turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         else //controller killed an enemy
            if( getTag( $DeathMessageCTurretKill[ %damageType , 0 ] ) )		// +soph
               messageAll('msgCTurretKill', $DeathMessageCTurretKill[%damageType, mFloor(getRandom() * $DeathMessageCTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
            else								// +[soph]
               messageAll( 'msgCTurretKill' , '\c0%4 took control and ended %1 with a turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by a turret controlled by "@%controller);
      }
      //---------------------------------------------------------------------------- finish CrackTrooper 7-16-2004
      // use the handle associated with the deployed object to verify valid owner
      else if (isObject(%implement.owner))
      {
         %owner = %implement.owner;
         //error("Owner is " @ %owner @ "   Handle is " @ %implement.ownerHandle);
         //error("Turret is still owned");
         //turret is uncontrolled, but is owned - treat the same as controlled.
         %killerGender = (%owner.sex $= "Male" ? 'him' : 'her');
         %killerPoss = (%owner.sex $= "Male" ? 'his' : 'her');
//         %killerName = %owner.name;

         %killerRank = %owner.isAIControlled() ? "" : $StatRankID[getTrackedStat(%owner.guid, "rank")];
         %killerName = addTaggedString(%killerRank SPC getTaggedString(%owner.name));

         if (%owner.team == %clVictim.team)  //player got in the way of a teammates deployed but uncontrolled turret.
            if( getTag( $DeathMessageCTurretAccdtlKill[ %damageType , 0 ] ) )	// +soph
               messageAll('msgCTurretKill', $DeathMessageCTurretAccdtlKill[%damageType,mFloor(getRandom() * $DeathMessageCTurretAccdtlKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
            else								// +[soph]
               messageAll( 'msgCTurretKill' , '\c0%1 got in the way of %4\'s \'friendly\' turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         else  //deployed, uncontrolled turret killed an enemy
            if( getTag( $DeathMessageCTurretKill[ %damageType , 0 ] ) )		// +soph
               messageAll('msgCTurretKill', $DeathMessageCTurretKill[%damageType,mFloor(getRandom() * $DeathMessageCTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
            else								// +[soph]
               messageAll( 'msgCTurretKill' , '\c0%1 took fatal fire from a turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") was killed by turret (automated)");
      }
      else  //turret is not a placed (owned) turret (or owner is no longer on it's team), and is not being controlled
      {
         if( getTag( $DeathMessageTurretKill[ %damageType , 0 ] ) )		// +soph
            messageAll('msgTurretKill', $DeathMessageTurretKill[%damageType,mFloor(getRandom() * $DeathMessageTurretKillCount)],%victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else									// +[soph]
            messageAll( 'msgCTurretKill' , '\c0%1 took fatal fire from a turret.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
         logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by turret");
      }
   }
   else if((%clKiller == %clVictim) || (%damageType == $DamageType::Ground)) //player killed himself or fell to death
   {
      if( getTag( $DeathMessageSelfKill[ %damageType , 0 ] ) )			// +soph
         messageAll('msgSelfKill', $DeathMessageSelfKill[%damageType,mFloor(getRandom() * $DeathMessageSelfKillCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      else									// +[soph]
         messageAll( 'msgCTurretKill' , '\c0%1 took %3 own life.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed self ("@getTaggedString($DamageTypeText[%damageType])@")");
   }

   else if (%damageType == $DamageType::OutOfBounds) //killer died due to Out-of-Bounds damage
   {
      messageAll('msgOOBKill', $DeathMessageOOB[mFloor(getRandom() * $DeathMessageOOBCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by out-of-bounds damage");
   }

   else if (%damageType == $DamageType::NexusCamping) //Victim died from camping near the nexus...
   {
      messageAll('msgCampKill', $DeathMessageCamping[mFloor(getRandom() * $DeathMessageCampingCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed for nexus camping");
   }

   else if(%clKiller.team == %clVictim.team) //was a TK
   {
      if( getTag( $DeathMessageTeamKill[ %damageType , 0 ] ) )			// +soph
         messageAll('msgTeamKill', $DeathMessageTeamKill[%damageType, mFloor(getRandom() * $DeathMessageTeamKillCount)],  %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      else									// +[soph]
         messageAll( 'msgCTurretKill' , '\c0%4 TEAMKILLED %1.' , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") teamkilled by "@%clKiller.nameBase@" (pl "@%clKiller.player@"/cl "@%clKiller@")");
   }

   else if (%damageType == $DamageType::Lava)   //player died by falling in lava
   {
      messageAll('msgLavaKill',  $DeathMessageLava[mFloor(getRandom() * $DeathMessageLavaCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by lava");
   }
   else if ( %damageType == $DamageType::Lightning )  // player was struck by lightning
   {
      messageAll('msgLightningKill',  $DeathMessageLightning[mFloor(getRandom() * $DeathMessageLightningCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by lightning");
   }
   else if( ( %damageType == $DamageType::BTMine || %damageType == $DamageType::Mine ) && !isObject( %clKiller ) )	// ( %damageType == $DamageType::Mine && !isObject(%clKiller) ) -soph
   {
         error("Mine kill w/o source");
         messageAll('MsgRogueMineKill', $DeathMessageRogueMine[%damageType, mFloor(getRandom() * $DeathMessageRogueMineCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
   }
   else  //was a legitimate enemy kill
   {
//      if(%damageType == 6 && (%clVictim.headShot))				// -soph
      if( %clVictim.headShot && getTag( $DeathMessageHeadshot[ %damageType, 0 ] ) )	// +soph
      {
         // laser headshot just occurred
         messageAll('MsgHeadshotKill', $DeathMessageHeadshot[%damageType, mFloor(getRandom() * $DeathMessageHeadshotCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
      }
      else
         if( getTag( $DeathMessage[ %damageType , 0 ] ) )			// +soph
            messageAll('MsgLegitKill', $DeathMessage[%damageType, mFloor(getRandom() * $DeathMessageCount)], %victimName, %victimGender, %victimPoss, %killerName, %killerGender, %killerPoss, %damageType, $DamageTypeText[%damageType]);
         else if( %damageType != $DamageType::Default )				// +[soph]
            messageAll( 'MsgLegitKill' , $DeathMessage[ $DamageType::Nosferatu , mFloor( getRandom() * $DeathMessageCount ) ] , %victimName , %victimGender , %victimPoss , %killerName , %killerGender , %killerPoss , %damageType , $DamageTypeText[ %damageType ] ) ;
										// +[/soph]
      logEcho(%clVictim.nameBase@" (pl "@%clVictim.player@"/cl "@%clVictim@") killed by "@%clKiller.nameBase@" (pl "@%clKiller.player@"/cl "@%clKiller@") using "@getTaggedString($DamageTypeText[%damageType]));
   }
}

// Make this optional in case I change it in the future
$UseT1Sounds = true;

function CTFGame::playerTouchEnemyFlag(%game, %player, %flag)
{
   // ---------------------------------------------------------------
   // z0dd, ZOD - 9/27/02. Player must wait to grab after throwing it
   if((%player.flagTossWait !$= "") && %player.flagTossWait)
      return false;
   // ---------------------------------------------------------------

   if(%player.getDatablock().armorMask & ($ArmorMask::Heavy | $ArmorMask::MagIon | $ArmorMask::Blastech | $ArmorMask::BattleAngel) || %player.getDatablock().isTacticalMech)
//   if(%player.getDatablock().isBattleAngel || %player.getDatablock().isJugg || %player.isTacticalMech || $DevServer)
   {											// +[soph]
      %time = getSimTime() ;								// +
      if( %player.wrongArmorWarningFlag < %time )					// +
         if( %player.getDatablock().isTacticalMech )					// +
            messageClient( %player.client , 'MsgCannotGrabFlag' , '\c2Mechs cannot carry the enemy flag.\n\tOnly Scout, Engineer, and Assault armor classes can pick up enemy flags.' ) ;
         else										// +
            messageClient( %player.client , 'MsgCannotGrabFlag' , '\c2You cannot carry the enemy flag in this armor class.\n\tOnly Scout, Engineer, and Assault armor classes can pick up enemy flags.' ) ;
      %player.wrongArmorWarningFlag = %time + 5000 ;					// +[/soph]
      return false;
   }

   if( %player.holdingFlag && %player.holdingFlag != %flag )				// +soph
      return ; 										// +soph

   if(%player.preventFlagCaps)
      return false;

   addStatTrack(%player.client.guid, "flagTouches", 1);

   cancel(%flag.searchSchedule); // z0dd - ZOD, 9/28/02. Hack for flag collision bug.  SquirrelOfDeath, 10/02/02: Moved from PlayerTouchFlag

   cancel(%game.updateFlagThread[%flag]); // z0dd - ZOD, 8/4/02. Cancel this flag's thread to KineticPoet's flag updater
   %game.flagHeldTime[%flag] = getSimTime(); // z0dd - ZOD, 8/15/02. Store time player grabbed flag.

   %client = %player.client;
   %player.holdingFlag = %flag;  //%player has this flag
   %flag.carrier = %player;  //this %flag is carried by %player

   %player.mountImage(FlagImage, $FlagSlot, true, %game.getTeamSkin(%flag.team));

   %game.playerGotFlagTarget(%player);
   //only cancel the return timer if the player is in bounds...
   if (!%client.outOfBounds)
   {
      cancel($FlagReturnTimer[%flag]);
      $FlagReturnTimer[%flag] = "";
   }

   //if this flag was "at home", see if both flags have now been taken
   if (%flag.isHome)
   {
      // tiebreaker score
      game.awardScoreFlagTouch( %client, %flag );

      %startStalemate = false;
      if ($TeamFlag[1] == %flag)
         %startStalemate = !$TeamFlag[2].isHome;
      else
         %startStalemate = !$TeamFlag[1].isHome;

      if (%startStalemate)
         %game.stalemateSchedule = %game.schedule(%game.stalemateTimeMS, beginStalemate);

   }
   %flag.hide(true);
   %flag.startFade(0, 0, false);
   %flag.isHome = false;
   if(%flag.stand)
      %flag.stand.getDataBlock().onFlagTaken(%flag.stand);//animate, if exterior stand

   $flagStatus[%flag.team] = %client.nameBase;
   %teamName = %game.getTeamName(%flag.team);

   if($UseT1Sounds)
   {
        messageTeamExcept(%client, 'MsgCTFFlagTaken', '\c2Teammate %1 took the %2 flag.~wfx/misc/Flag1.wav', %client.name, %teamName, %flag.team, %client.nameBase);
        messageTeam(%flag.team, 'MsgCTFFlagTaken', '\c2Your flag has been taken by %1!~wfx/misc/Flag1.wav',%client.name, 0, %flag.team, %client.nameBase);
        messageTeam(0, 'MsgCTFFlagTaken', '\c2%1 took the %2 flag.~wfx/misc/Flag1.wav', %client.name, %teamName, %flag.team, %client.nameBase);
        messageClient(%client, 'MsgCTFFlagTaken', '\c2You took the %2 flag.~wfx/misc/Flag1.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   }
   else
   {
        messageTeamExcept(%client, 'MsgCTFFlagTaken', '\c2Teammate %1 took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
        messageTeam(%flag.team, 'MsgCTFFlagTaken', '\c2Your flag has been taken by %1!~wfx/misc/flag_taken.wav',%client.name, 0, %flag.team, %client.nameBase);
        messageTeam(0, 'MsgCTFFlagTaken', '\c2%1 took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
        messageClient(%client, 'MsgCTFFlagTaken', '\c2You took the %2 flag.~wfx/misc/flag_snatch.wav', %client.name, %teamName, %flag.team, %client.nameBase);
   }

   logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") took team "@%flag.team@" flag");

   //call the AI function
   %game.AIplayerTouchEnemyFlag(%player, %flag);

   //if the player is out of bounds, then in 3 seconds, it should be thrown back towards the in bounds area...
   if (%client.outOfBounds)
      %game.schedule(3000, "boundaryLoseFlag", %player);
}

function CTFGame::playerDroppedFlag(%game, %player)
{
   %client = %player.client;
   %flag = %player.holdingFlag;
   %game.updateFlagTransform(%flag); // z0dd - ZOD, 8/4/02, Call to KineticPoet's flag updater
   %held = %game.formatTime(getSimTime() - %game.flagHeldTime[%flag], false); // z0dd - ZOD, 8/15/02. How long did player hold flag?

   %game.playerLostFlagTarget(%player);

   %player.holdingFlag = ""; //player isn't holding a flag anymore
   %flag.carrier = "";  //flag isn't held anymore
   $flagStatus[%flag.team] = "<In the Field>";

   %player.unMountImage($FlagSlot);
   %flag.hide(false); //Does the throwItem function handle this?

   %teamName = %game.getTeamName(%flag.team);

   if($UseT1Sounds)
   {
        messageTeamExcept(%client, 'MsgCTFFlagDropped', '\c2Teammate %1 dropped the %2 flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, %teamName, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
        messageTeam(%flag.team, 'MsgCTFFlagDropped', '\c2Your flag has been dropped by %1! (Held: %4)~wfx/misc/flag_drop.wav', %client.name, 0, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
        messageTeam(0, 'MsgCTFFlagDropped', '\c2%1 dropped the %2 flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, %teamName, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
        if(!%player.client.outOfBounds)
           messageClient(%client, 'MsgCTFFlagDropped', '\c2You dropped the %2 flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, %teamName, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
   }                                                                                                                                            // Yogi, 8/18/02. 3rd param changed 0 -> %client.name
   else
   {
        messageTeamExcept(%client, 'MsgCTFFlagDropped', '\c2Teammate %1 dropped the %2 flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, %teamName, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
        messageTeam(%flag.team, 'MsgCTFFlagDropped', '\c2Your flag has been dropped by %1! (Held: %4)~wfx/misc/flag_drop.wav', %client.name, 0, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
        messageTeam(0, 'MsgCTFFlagDropped', '\c2%1 dropped the %2 flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, %teamName, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
        if(!%player.client.outOfBounds)
           messageClient(%client, 'MsgCTFFlagDropped', '\c2You dropped the %2 flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, %teamName, %flag.team, %held); // z0dd - ZOD, 8/15/02. How long flag was held
   }                                                                                                                                            // Yogi, 8/18/02. 3rd param changed 0 -> %client.name
   logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") dropped team "@%flag.team@" flag"@" (Held: "@%held@")");

   //don't duplicate the schedule if there's already one in progress...
   if ($FlagReturnTimer[%flag] <= 0)
      $FlagReturnTimer[%flag] = %game.schedule(%game.FLAG_RETURN_DELAY - %game.fadeTimeMS, "flagReturnFade", %flag);

   //call the AI function
   %game.AIplayerDroppedFlag(%player, %flag);
}

function CTFGame::flagCap(%game, %player)
{
   %client = %player.client;
   %flag = %player.holdingFlag;
   %flag.carrier = "";

   %held = %game.formatTime(getSimTime() - %game.flagHeldTime[%flag], false); // z0dd - ZOD, 8/15/02. How long did player hold flag?

   %game.playerLostFlagTarget(%player);
   //award points to player and team
   %teamName = %game.getTeamName(%flag.team);

   if($UseT1Sounds)
   {
        messageTeamExcept(%client, 'MsgCTFFlagCapped', '\c2%1 captured the %2 flag! (Held: %5)~wfx/misc/flagcapture.wav', %client.name, %teamName, %flag.team, %client.team, %held);
        messageTeam(%flag.team, 'MsgCTFFlagCapped', '\c2Your flag was captured by %1. (Held: %5)~wfx/misc/flagcapture.wav', %client.name, 0, %flag.team, %client.team, %held);
        messageTeam(0, 'MsgCTFFlagCapped', '\c2%1 captured the %2 flag! (Held: %5)~wfx/misc/flagcapture.wav', %client.name, %teamName, %flag.team, %client.team, %held);
        messageClient(%client, 'MsgCTFFlagCapped', '\c2You captured the %2 flag! (Held: %5)~wfx/misc/flagcapture.wav', %client.name, %teamName, %flag.team, %client.team, %held); // Yogi, 8/18/02.  3rd param changed 0 -> %client.name
   }
   else
   {
        messageTeamExcept(%client, 'MsgCTFFlagCapped', '\c2%1 captured the %2 flag! (Held: %5)~wfx/misc/flag_capture.wav', %client.name, %teamName, %flag.team, %client.team, %held);
        messageTeam(%flag.team, 'MsgCTFFlagCapped', '\c2Your flag was captured by %1. (Held: %5)~wfx/misc/flag_lost.wav', %client.name, 0, %flag.team, %client.team, %held);
        messageTeam(0, 'MsgCTFFlagCapped', '\c2%1 captured the %2 flag! (Held: %5)~wfx/misc/flag_capture.wav', %client.name, %teamName, %flag.team, %client.team, %held);
        messageClient(%client, 'MsgCTFFlagCapped', '\c2You captured the %2 flag! (Held: %5)~wfx/misc/flag_capture.wav', %client.name, %teamName, %flag.team, %client.team, %held); // Yogi, 8/18/02.  3rd param changed 0 -> %client.name
   }

   logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") capped team "@%client.team@" flag"@" (Held: "@%held@")");
   %player.holdingFlag = ""; //no longer holding it.
   %player.unMountImage($FlagSlot);
   %game.awardScoreFlagCap(%client, %flag);
   %game.flagReset(%flag);

   addStatTrack(%client.guid, "flagcaps", 1);
   addStatTrack(0, "flagcaps", 1);   
    
   //call the AI function
   %game.AIflagCap(%player, %flag);

   //if this cap didn't end the game, play the announcer...
   if ($missionRunning)
   {
      if (%game.getTeamName(%client.team) $= 'Inferno')
         messageAll("", '~wvoice/announcer/ann.infscores.wav');
      else if (%game.getTeamName(%client.team) $= 'Storm')
         messageAll("", '~wvoice/announcer/ann.stoscores.wav');
      else if (%game.getTeamName(%client.team) $= 'Phoenix')
         messageAll("", '~wvoice/announcer/ann.pxscore.wav');
      else if (%game.getTeamName(%client.team) $= 'Blood Eagle')
         messageAll("", '~wvoice/announcer/ann.bescore.wav');
      else if (%game.getTeamName(%client.team) $= 'Diamond Sword')
         messageAll("", '~wvoice/announcer/ann.dsscore.wav');
      else if (%game.getTeamName(%client.team) $= 'Starwolf')
         messageAll("", '~wvoice/announcer/ann.swscore.wav');
   }
}

function CTFGame::flagReturn(%game, %flag, %player)
{
   cancel($FlagReturnTimer[%flag]);
   $FlagReturnTimer[%flag] = "";

   if(%flag.team == 1)
      %otherTeam = 2;
   else
      %otherTeam = 1;
   %teamName = %game.getTeamName(%flag.team);
   if (%player !$= "")
   {
      //a player returned it
      %client = %player.client;

      if($UseT1Sounds)
      {
           messageTeamExcept(%client, 'MsgCTFFlagReturned', '\c2Teammate %1 returned your flag to base.~wfx/misc/flagreturn.wav', %client.name, 0, %flag.team);
           messageTeam(%otherTeam, 'MsgCTFFlagReturned', '\c2Enemy %1 returned the %2 flag.~wfx/misc/flagreturn.wav', %client.name, %teamName, %flag.team);
           messageTeam(0, 'MsgCTFFlagReturned', '\c2%1 returned the %2 flag.~wfx/misc/flagreturn.wav', %client.name, %teamName, %flag.team);
           messageClient(%client, 'MsgCTFFlagReturned', '\c2You returned your flag.~wfx/misc/flagreturn.wav', %client.name, %teamName, %flag.team); // Yogi, 8/18/02. 3rd param changed 0 -> %client.name
      }
      else
      {
           messageTeamExcept(%client, 'MsgCTFFlagReturned', '\c2Teammate %1 returned your flag to base.~wfx/misc/flag_return.wav', %client.name, 0, %flag.team);
           messageTeam(%otherTeam, 'MsgCTFFlagReturned', '\c2Enemy %1 returned the %2 flag.~wfx/misc/flag_return.wav', %client.name, %teamName, %flag.team);
           messageTeam(0, 'MsgCTFFlagReturned', '\c2%1 returned the %2 flag.~wfx/misc/flag_return.wav', %client.name, %teamName, %flag.team);
           messageClient(%client, 'MsgCTFFlagReturned', '\c2You returned your flag.~wfx/misc/flag_return.wav', %client.name, %teamName, %flag.team); // Yogi, 8/18/02. 3rd param changed 0 -> %client.name
      }
      logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") returned team "@%flag.team@" flag");

      // find out what type of return it is
      // stalemate return?

      // ---------------------------------------------------
      // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
      if(%game.stalemate)
      {
        //error("Stalemate return!!!");
        %game.awardScoreStalemateReturn(%player.client);
      }
      // regular return
      else
      {
        %enemyFlagDist = vectorDist($flagPos[%flag.team], $flagPos[%otherTeam]);
        %dist = vectorDist(%flag.position, %flag.originalPosition);

        %rawRatio = %dist/%enemyFlagDist;
        %ratio = %rawRatio < 1 ? %rawRatio : 1;
        %percentage = mFloor( (%ratio) * 10 ) * 10;
        %game.awardScoreFlagReturn(%player.client, %percentage);
      }
      // ---------------------------------------------------
   }
   else
   {
      //returned due to timer
      if($UseT1Sounds)
      {
           messageTeam(%otherTeam, 'MsgCTFFlagReturned', '\c2The %2 flag was returned to base.~wfx/misc/flagreturn.wav', 0, %teamName, %flag.team);  //because it was dropped for too long
           messageTeam(%flag.team, 'MsgCTFFlagReturned', '\c2Your flag was returned.~wfx/misc/flagreturn.wav', 0, 0, %flag.team);
           messageTeam(0, 'MsgCTFFlagReturned', '\c2The %2 flag was returned to base.~wfx/misc/flagreturn.wav', 0, %teamName, %flag.team);
      }
      else
      {
           messageTeam(%otherTeam, 'MsgCTFFlagReturned', '\c2The %2 flag was returned to base.~wfx/misc/flag_return.wav', 0, %teamName, %flag.team);  //because it was dropped for too long
           messageTeam(%flag.team, 'MsgCTFFlagReturned', '\c2Your flag was returned.~wfx/misc/flag_return.wav', 0, 0, %flag.team);
           messageTeam(0, 'MsgCTFFlagReturned', '\c2The %2 flag was returned to base.~wfx/misc/flag_return.wav', 0, %teamName, %flag.team);
      }

      logEcho("team "@%flag.team@" flag returned (timeout)");
   }
   %game.flagReset(%flag);
}

function CTFGame::boundaryLoseFlag(%game, %player)
{
   // this is called when a player goes out of the mission area while holding
   // the enemy flag. - make sure the player is still out of bounds
   if (!%player.client.outOfBounds || !isObject(%player.holdingFlag))
      return;

   // ------------------------------------------------------------------------------
   // z0dd - ZOD - SquirrelOfDeath, 9/27/02. Delay on grabbing flag after tossing it
   %player.flagTossWait = true;
  // %player.schedule(1000, resetFlagTossWait);		// the reset function is absent so this doesn't work -soph
   game.schedule( 1000 , resetFlagTossWait , %player );	// +soph
   // ------------------------------------------------------------------------------

   %client = %player.client;
   %flag = %player.holdingFlag;
   %flag.setVelocity("0 0 0");
   %flag.setTransform(%player.getWorldBoxCenter());
   %flag.setCollisionTimeout(%player);

   %held = %game.formatTime(getSimTime() - %game.flagHeldTime[%flag], false); // z0dd - ZOD, 8/15/02. How long did player hold flag?

   %game.playerDroppedFlag(%player);

   // now for the tricky part -- throwing the flag back into the mission area
   // let's try throwing it back towards its "home"
   %home = %flag.originalPosition;
   %vecx =  firstWord(%home) - firstWord(%player.getWorldBoxCenter());
   %vecy = getWord(%home, 1) - getWord(%player.getWorldBoxCenter(), 1);
   %vecz = getWord(%home, 2) - getWord(%player.getWorldBoxCenter(), 2);
   %vec = %vecx SPC %vecy SPC %vecz;

   // normalize the vector, scale it, and add an extra "upwards" component
   %vecNorm = VectorNormalize(%vec);
   %vec = VectorScale(%vecNorm, 1500);
   %vec = vectorAdd(%vec, "0 0 500");

   // z0dd - ZOD, 6/09/02. Remove anti-hover so flag can be thrown properly
   %flag.static = false;

   // z0dd - ZOD, 10/02/02. Hack for flag collision bug.
   %flag.searchSchedule = %game.schedule(10, "startFlagCollisionSearch", %flag);

   // apply the impulse to the flag object
   %flag.applyImpulse(%player.getWorldBoxCenter(), %vec);

   //don't forget to send the message
   //messageClient(%player.client, 'MsgCTFFlagDropped', '\c1You have left the mission area and lost the flag.~wfx/misc/flag_drop.wav', 0, 0, %player.holdingFlag.team);

   // z0dd - ZOD 3/30/02. Above message was sending the wrong varible to objective hud.
   messageClient(%player.client, 'MsgCTFFlagDropped', '\c1You have left the mission area and lost the flag. (Held: %4)~wfx/misc/flag_drop.wav', %client.name, 0, %flag.team, %held); // Yogi, 8/18/02. 3rd param changed 0 -> %client.name
   logEcho(%player.client.nameBase@" (pl "@%player@"/cl "@%player.client@") lost flag (out of bounds)"@" (Held: "@%held@")");
}

function CTFGame::resetFlagTossWait( %game , %player )	// +[soph]
{
   %player.flagTossWait = false;
}							// +[/soph]

function CTFGame::notifyMineDeployed(%game, %mine)
{
   //see if the mine is within 5 meters of the flag stand...
   %mineTeam = %mine.sourceObject.team;
   %homeFlag = $TeamFlag[%mineTeam];
   if (isObject(%homeFlag))
   {
      %dist = VectorDist(%homeFlag.originalPosition, %mine.position);
      if (%dist <= %game.notifyMineDist)
      {
          if(getRandomB())
              messageTeam(%mineTeam, 'MsgCTFFlagMined', "The flag has been mined.~wvoice/announcer/flag_minedFem.wav" );
          else
              messageTeam(%mineTeam, 'MsgCTFFlagMinedM', "The flag has been mined.~wvoice/announcer/flag_minedMale.wav" );
      }
   }
}

function DefaultGame::leaveMissionArea(%game, %playerData, %player)
{
   if(%player.getState() $= "Dead")
      return;

   %player.client.outOfBounds = true;
   messageClient(%player.client, 'LeaveMissionArea', '\c1You left the mission area.~wvoice/announcer/ANN.oob.wav');
}

function DefaultGame::enterMissionArea(%game, %playerData, %player)
{
   if(%player.getState() $= "Dead")
      return;

   %player.client.outOfBounds = false;
   messageClient(%player.client, 'EnterMissionArea', '\c1You are back in the mission area.~wvoice/announcer/ANN.ib.wav');
}

function CTFGame::enterMissionArea(%game, %playerData, %player)
{
    if(%player.getState() $= "Dead")
    return;
    %player.client.outOfBounds = false;
   messageClient(%player.client, 'EnterMissionArea', '\c1You are back in the mission area.~wvoice/announcer/ANN.ib.wav');
   logEcho(%player.client.nameBase@" (pl "@%player@"/cl "@%player.client@") entered mission area");

   //the instant a player leaves the mission boundary, the flag is dropped, and the return is scheduled...
   if (%player.holdingFlag > 0)
   {
      cancel($FlagReturnTimer[%player.holdingFlag]);
      $FlagReturnTimer[%player.holdingFlag] = "";
   }
}

function CTFGame::leaveMissionArea(%game, %playerData, %player)
{
    if(%player.getState() $= "Dead")
    return;
   // maybe we'll do this just in case
   %player.client.outOfBounds = true;
   // if the player is holding a flag, strip it and throw it back into the mission area
   // otherwise, just print a message
   if(%player.holdingFlag > 0)
      %game.boundaryLoseFlag(%player);
   else
      messageClient(%player.client, 'MsgLeaveMissionArea', '\c1You have left the mission area.~wvoice/announcer/ANN.oob.wav');
   logEcho(%player.client.nameBase@" (pl "@%player@"/cl "@%player.client@") left mission area");
}

// -----------------------------------------------------------------------------------------------------------
// z0dd - ZOD, 8/20/02. Added this team destroy message function
function teamDestroyMessage(%client, %msgType, %msgString, %a1, %a2, %a3, %a4, %a5, %a6)
{
   %team = %client.team;

   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
   {
      %recipient = ClientGroup.getObject(%i);
      if((%recipient.team == %team) && (%recipient != %client))
      {
         commandToClient(%recipient, 'TeamDestroyMessage', %msgType, %msgString, %a1, %a2, %a3, %a4, %a5, %a6);
      }
   }
}
// -----------------------------------------------------------------------------------------------------------

function DefaultGame::objectRepaired(%game, %obj, %objName)
{
   %item = %obj.getDataBlock().getName();
   //echo("Item repaired is a " @ %item);
   switch$ (%item)
   {
      case generatorLarge :
         %game.genOnRepaired(%obj, %objName);
      case stationInventory :
         %game.stationOnRepaired(%obj, %objName);
      case sensorMediumPulse :
         %game.sensorOnRepaired(%obj, %objName);
      case sensorLargePulse :
         %game.sensorOnRepaired(%obj, %objName);
      case turretBaseLarge :
         %game.turretOnRepaired(%obj, %objName);
      case stationVehicle :
          %game.vStationOnRepaired(%obj, %objName);
      case FreeBaseTurret:
         %game.turretOnRepaired(%obj, %objName);
      case DeployableStationVehicle:
          %game.vStationOnRepaired(%obj, %objName);
      case JammerBeacon:
         %game.sensorOnRepaired(%obj, %objName);

      default:  //unused by current gametypes.  Add more checks here if desired
   }
}

function Observer::onTrigger(%data,%obj,%trigger,%state)
{
   // state = 0 means that a trigger key was released
   if (%state == 0)
      return;

   //first, give the game the opportunity to prevent the observer action
   if (!Game.ObserverOnTrigger(%data, %obj, %trigger, %state))
      return;

   //now observer functions if you press the "throw"
   if (%trigger >= 4)
      return;

   //trigger types:   0:fire 1:altTrigger 2:jump 3:jet 4:throw
   %client = %obj.getControllingClient();
   if (%client == 0)
      return;

   switch$ (%obj.mode)
   {
      case "justJoined":
         if (isDemo())
            clearCenterPrint(%client);

         //press FIRE
         if (%trigger == 0)
         {
            // clear intro message
            clearBottomPrint( %client );
            
            //spawn the player
			   commandToClient(%client, 'setHudMode', 'Standard');
            Game.assignClientTeam(%client);
            Game.spawnPlayer( %client, $MatchStarted );
            
            if( $MatchStarted )
            {
               %client.camera.setFlyMode();
               %client.setControlObject( %client.player );
            }
            else
            {   
               %client.camera.getDataBlock().setMode( %client.camera, "pre-game", %client.player );
               %client.setControlObject( %client.camera );
            }
         }
         
         //press JET
         else if (%trigger == 3)
         {
            //cycle throw the static observer spawn points
            %markerObj = Game.pickObserverSpawn(%client, true);
            %transform = %markerObj.getTransform();
            %obj.setTransform(%transform);
            %obj.setFlyMode();
         }

         //press JUMP
         else if (%trigger == 2)
         {
            //switch the observer mode to observing clients
            if (isObject(%client.observeFlyClient))
               serverCmdObserveClient(%client, %client.observeFlyClient);
            else
               serverCmdObserveClient(%client, -1);

            displayObserverHud(%client, %client.observeClient);
            messageClient(%client.observeClient, 'Observer', '\c1%1 is now observing you.', %client.name);  
         }
      
      case "playerDeath":
         // Attached to a dead player - spawn regardless of trigger type
         if(!%client.waitRespawn && getSimTime() > %client.suicideRespawnTime)
         {
			   commandToClient(%client, 'setHudMode', 'Standard');
            Game.spawnPlayer( %client, true );
            %client.camera.setFlyMode();
            %client.setControlObject(%client.player);
         }
      
      case "PreviewMode":
         if (%trigger == 0)
         {
			   commandToClient(%client, 'setHudMode', 'Standard');
            if( %client.lastTeam )
               Game.clientJoinTeam( %client, %client.lastTeam );
            else
            {   
               Game.assignClientTeam( %client, true );
               
               // Spawn the player:
               Game.spawnPlayer( %client, false );
            }
            
            %client.camera.setFlyMode();
            %client.setControlObject( %client.player );
         }

      case "toggleCameraFly":
      // this is the default camera mode

      case "observerFly":
         // Free-flying observer camera
         
         if (%trigger == 0)
         {
            if( !$Host::TournamentMode && $MatchStarted )
            {
               // reset observer params
               clearBottomPrint(%client);
				   commandToClient(%client, 'setHudMode', 'Standard');
               
               if( %client.lastTeam !$= "" && %client.lastTeam != 0 && Game.numTeams > 1)
               {   
                  Game.clientJoinTeam( %client, %client.lastTeam, $MatchStarted ); 
                  %client.camera.setFlyMode();
                  %client.setControlObject( %client.player );
               }
               else
               {   
                  
                  Game.assignClientTeam( %client );
                  
                  // Spawn the player:
                  Game.spawnPlayer( %client, true );
                  %client.camera.setFlyMode();
                  %client.setControlObject( %client.player );
                  ClearBottomPrint( %client );
               }
            }
            else if( !$Host::TournamentMode )
            {
               
               clearBottomPrint(%client);
               Game.assignClientTeam( %client );
                  
               // Spawn the player:
               Game.spawnPlayer( %client, false );
               %client.camera.getDataBlock().setMode( %client.camera, "pre-game", %client.player );
               %client.setControlObject( %client.camera );
            }
         }   
         //press JET
         else if (%trigger == 3)
         {
            %markerObj = Game.pickObserverSpawn(%client, true);
            %transform = %markerObj.getTransform();
            %obj.setTransform(%transform);
            %obj.setFlyMode();
         }

         //press JUMP
         else if (%trigger == 2)
         {
            //switch the observer mode to observing clients
            if (isObject(%client.observeFlyClient))
               serverCmdObserveClient(%client, %client.observeFlyClient);
            else
               serverCmdObserveClient(%client, -1);
            
            observerFollowUpdate( %client, %client.observeClient, false );
            displayObserverHud(%client, %client.observeClient);
            messageClient(%client.observeClient, 'Observer', '\c1%1 is now observing you.', %client.name);  
         }
      
      case "observerStatic":
         // Non-moving observer camera
         %next = (%trigger == 3 ? true : false);
         %markerObj = Game.pickObserverSpawn(%client, %next);
         %transform = %markerObj.getTransform();
         %obj.setTransform(%transform);
         %obj.setFlyMode();
         
      case "observerStaticNoNext":
         // Non-moving, non-cycling observer camera
      
      case "observerTimeout":
         // Player didn't respawn quickly enough
         if (%trigger == 0)
         {
            clearBottomPrint(%client);
			   commandToClient(%client, 'setHudMode', 'Standard');
            if( %client.lastTeam )
               Game.clientJoinTeam( %client, %client.lastTeam, true ); 
            else
            {   
               Game.assignClientTeam( %client );
               
               // Spawn the player:
               Game.spawnPlayer( %client, true );
            }
            
            %client.camera.setFlyMode();
            %client.setControlObject( %client.player );
         }   

         //press JET
         else if (%trigger == 3)
         {
            %markerObj = Game.pickObserverSpawn(%client, true);
            %transform = %markerObj.getTransform();
            %obj.setTransform(%transform);
            %obj.setFlyMode();
         }

         //press JUMP
         else if (%trigger == 2)
         {
            //switch the observer mode to observing clients
            if (isObject(%client.observeFlyClient))
               serverCmdObserveClient(%client, %client.observeFlyClient);
            else
               serverCmdObserveClient(%client, -1);
            
            // update the observer list for this client
            observerFollowUpdate( %client, %client.observeClient, false );
            
            displayObserverHud(%client, %client.observeClient);
            messageClient(%client.observeClient, 'Observer', '\c1%1 is now observing you.', %client.name);  
         }

      case "observerFollow":
         // Observer attached to a moving object (assume player for now...)
         //press FIRE - cycle to next client
         if (%trigger == 0)
         {
            %nextClient = findNextObserveClient(%client);
            %prevObsClient = %client.observeClient;
            if (%nextClient > 0 && %nextClient != %client.observeClient)
            {
               // update the observer list for this client
               observerFollowUpdate( %client, %nextClient, true );
               
               //set the new object
               %transform = %nextClient.player.getTransform();
               if( !%nextClient.player.isMounted() )
               {
                  %obj.setOrbitMode(%nextClient.player, %transform, 0.5, 4.5, 4.5);
                  %client.observeClient = %nextClient;
               }
               else
               {
                  %mount = %nextClient.player.getObjectMount();
                  if( %mount.getDataBlock().observeParameters $= "" )
                     %params = %transform;
                  else
                     %params = %mount.getDataBlock().observeParameters;
            
                  %obj.setOrbitMode(%mount, %mount.getTransform(), getWord( %params, 0 ), getWord( %params, 1 ), getWord( %params, 2 ));
                  %client.observeClient = %nextClient;
               }
               
               //send the message(s)
               displayObserverHud(%client, %nextClient);
               messageClient(%nextClient, 'Observer', '\c1%1 is now observing you.', %client.name);  
               messageClient(%prevObsClient, 'ObserverEnd', '\c1%1 is no longer observing you.', %client.name);  
            }
         }

         //press JET - cycle to prev client
         else if (%trigger == 3)
         {
            %prevClient = findPrevObserveClient(%client);
            %prevObsClient = %client.observeClient;
            if (%prevClient > 0 && %prevClient != %client.observeClient)
            {
               // update the observer list for this client
               observerFollowUpdate( %client, %prevClient, true );
               
               if(!isObject(%prevClient.player))
                    return;
               
               //set the new object
               %transform = %prevClient.player.getTransform();
               if( !%prevClient.isMounted() )
               {
                  %obj.setOrbitMode(%prevClient.player, %transform, 0.5, 4.5, 4.5);
                  %client.observeClient = %prevClient;
               }
               else
               {
                  %mount = %prevClient.player.getObjectMount();
                  if( %mount.getDataBlock().observeParameters $= "" )
                     %params = %transform;
                  else
                     %params = %mount.getDataBlock().observeParameters;
            
                  %obj.setOrbitMode(%mount, %mount.getTransform(), getWord( %params, 0 ), getWord( %params, 1 ), getWord( %params, 2 ));
                  %client.observeClient = %prevClient;
               }

               //send the message(s)
               displayObserverHud(%client, %prevClient);
               messageClient(%prevClient, 'Observer', '\c1%1 is now observing you.', %client.name);  
               messageClient(%prevObsClient, 'ObserverEnd', '\c1%1 is no longer observing you.', %client.name);  
            }
         }

         //press JUMP
         else if (%trigger == 2)
         {
            // update the observer list for this client
            observerFollowUpdate( %client, -1, false );
            
            //toggle back to observer fly mode
            %obj.mode = "observerFly";
            %obj.setFlyMode();
            updateObserverFlyHud(%client);
            messageClient(%client.observeClient, 'ObserverEnd', '\c1%1 is no longer observing you.', %client.name);  
         } 
      
      case "pre-game":
         if(!$Host::TournamentMode || $CountdownStarted)
            return;

         if(%client.notReady)
         {
            %client.notReady = "";
            MessageAll( 0, '\c1%1 is READY.', %client.name );
            if(%client.notReadyCount < 3)
               centerprint( %client, "\nWaiting for match start (FIRE if not ready)", 0, 3);
            else 
               centerprint( %client, "\nWaiting for match start", 0, 3);
         }
         else
         {
            %client.notReadyCount++;
            if(%client.notReadyCount < 4)
            {
               %client.notReady = true;
               MessageAll( 0, '\c1%1 is not READY.', %client.name );
               centerprint( %client, "\nPress FIRE when ready.", 0, 3 );
            }
            return;
         }
         CheckTourneyMatchStart();
         
      case "mechOrbit":
         if(%trigger == 0)
              MechGrenadeTriggerToggle(%client.player.getDatablock(), %client.player, true);
         else if( %trigger == 2 )									// +soph
               MechJumpTriggerToggle( %client.player.getDatablock() , %client.player , %state ) ;	// +soph
   }
}

function Observer::setMode(%data, %obj, %mode, %targetObj)
{
   if(%mode $= "")
      return;   
   %client = %obj.getControllingClient();
   switch$ (%mode) {
      case "justJoined":
		   commandToClient(%client, 'setHudMode', 'Observer');
         %markerObj = Game.pickObserverSpawn(%client, true);
         %transform = %markerObj.getTransform();
         %obj.setTransform(%transform);
         %obj.setFlyMode();
      
      case "pre-game":
		   commandToClient(%client, 'setHudMode', 'Observer');
         %obj.setOrbitMode( %targetObj, %targetObj.getTransform(), 0.5, 4.5, 4.5);

      case "mechOrbit":
		   commandToClient(%client, 'setHudMode', 'Observer');
         %obj.setOrbitMode( %targetObj, %targetObj.getTransform(), 0.5, 4.5, 4.5);
               
      case "observerFly":
         // Free-flying observer camera
		   commandToClient(%client, 'setHudMode', 'Observer');
         %markerObj = Game.pickObserverSpawn(%client, true);
         %transform = %markerObj.getTransform();
         %obj.setTransform(%transform);
         %obj.setFlyMode();
      
      case "observerStatic" or "observerStaticNoNext":
         // Non-moving observer camera
         %markerObj = Game.pickObserverSpawn(%client, true);
         %transform = %markerObj.getTransform();
         %obj.setTransform(%transform);
      
      case "observerFollow":
         // Observer attached to a moving object (assume player for now...)
         %transform = %targetObj.getTransform();
         
         if( !%targetObj.isMounted() )
            %obj.setOrbitMode(%targetObj, %transform, 0.5, 4.5, 4.5);
         else
         {
            %mount = %targetObj.getObjectMount();
            if( %mount.getDataBlock().observeParameters $= "" )
               %params = %transform;
            else
               %params = %mount.getDataBlock().observeParameters;
            
            %obj.setOrbitMode(%mount, %mount.getTransform(), getWord( %params, 0 ), getWord( %params, 1 ), getWord( %params, 2 ));
         }
      
      case "observerTimeout":
		   commandToClient(%client, 'setHudMode', 'Observer');
         %markerObj = Game.pickObserverSpawn(%client, true);
         %transform = %markerObj.getTransform();
         %obj.setTransform(%transform);
         %obj.setFlyMode();
   }
   %obj.mode = %mode;   
}

function HuntersGame::playerTouchFlag(%game, %player, %flag)
{
   //make sure the player is still alive
   %client = %player.client;
   if (%player.getState() !$= "Dead")
   {
      //increase the count bye the flag value
      %flagValue = %flag.value;
      %client.flagCount += %flagValue;
      
      //delete the flag
      %flag.delete();

      %mask = %player.getDatablock().armorMask;

      //schedule an update message
      cancel(%client.flagMsgPending);
      %client.flagMsgPending = %game.schedule(%game.flagMsgDelayMS, "sendFlagCountMessage", %client);
      messageClient(%client, 'MsgHuntYouHaveFlags', "", %client.flagCount - 1);

      //update the log...
      logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") has "@%client.flagCount-1@" flags");

      //play the sound pickup in 3D
      %player.playAudio(0, HuntersFlagPickupSound);

      //see if the client could set the record
      if (!%game.teamMode && !%client.couldSetRecord)
      {
         %numFlags = %client.flagCount - 1;
         if (%numFlags > 10 && %numFlags > $Host::HuntersRecords::Count[$currentMission])
         {
            //see if we have at least 4 non-AI players
            %humanCount = 0;
            %count = ClientGroup.getCount();
            for (%i = 0; %i < %count; %i++)
            {
               %cl = ClientGroup.getObject(%i);
               if (!%cl.isAIControlled())
                  %humanCount++;
               if (%humanCount >= %game.numHumansForRecord)
                  break;
            }

            if (%humanCount >= %game.numHumansForRecord)
            {
               %client.couldSetRecord = true;

               //send a message right away...
               if (isEventPending(%client.flagMsgPending))
               {
                  cancel(%client.flagMsgPending);
                  %game.sendFlagCountMessage(%client);
               }

               //send a message to everyone
               messageAllExcept(%client, -1, 'MsgHuntPlayerCouldSetRecord', '\c2%1 has enough flags to set the record for this mission!~wfx/misc/flag_return.wav');
               messageClient(%client, 'MsgHuntYouCouldSetRecord', '\c2You have enough flags to set the record for this mission!~wfx/misc/flag_return.wav');
            }
         }
      }

      //new tracking - *everyone* automatically tracks the "flag hoarder" if they have at least 15 flags
      %game.updateFlagHoarder(%client);
   }
   
      if(%mask & ($ArmorMask::Heavy | $ArmorMask::BattleAngel | $ArmorMask::Daishi))
          return;
             
      if(%client.flagCount >= 5)
         %player.mountImage(HuntersFlagImage, $FlagSlot);
}

function Nexus::onCollision(%data, %obj, %colObj)  
{
   //make sure it was a player that entered the trigger
   if((%colObj.getDataBlock().className $= Armor) && (%colObj.getState() !$= "Dead"))
   {
      %player = %colObj;
      %client = %player.client;

      // if player has been to the nexus within last 5 seconds, don't keep spamming messages
      if((getSimTime() - %player.lastNexusTime) < 5000)
         return;

      %player.lastNexusTime = getSimTime();

      if(Game.hoardModeActive())
      {
         messageClient(%client, 'MsgHuntHoardNoCap', '\c2Hoard mode is in effect! You cannot return any flags right now.~wfx/powered/nexus_deny.wav');

         //apply a "bounce" impulse to the player
         %velocity = %player.getVelocity();
         if (VectorDist("0 0 0", %velocity) < 2)
            %velocityNorm = "0 20 0";
         else
            %velocityNorm = VectorScale(VectorNormalize(%velocity), 20);
         %vector = VectorScale(%velocityNorm, -200);
         %player.applyImpulse(%player.position, %vector);

         Game.NexusSparkEmitter(%client, false);
         return;
      }
      
      // you can't cap your own flag
      %numToScore = %client.flagCount - 1;
      if (Game.greedMode && (%numToScore < Game.GreedMinFlags) && (%numToScore >= 1))
      {
         messageClient(%client, 'MsgHuntNeedGreed', '\c2Greed mode is ON! You must have %1 flags before you can return them.~wfx/powered/nexus_deny.wav', Game.GreedMinFlags);

         //transition the Nexus to the "off" animation and back again
         Game.flashNexus();

         //apply a "bounce" impuse to the player
         %velocity = %player.getVelocity();
         if (VectorDist("0 0 0", %velocity) < 2)
            %velocityNorm = "0 20 0";
         else
            %velocityNorm = VectorScale(VectorNormalize(%velocity), 20);
         %vector = VectorScale(%velocityNorm, -200);
         %player.applyImpulse(%player.position, %vector);

         Game.NexusSparkEmitter(%client, false);
         return;
      }

      //send the flags message right away...
      if (isEventPending(%client.flagMsgPending))
      {
         cancel(%client.flagMsgPending);
         %game.sendFlagCountMessage(%client);
      }

      //unless the nexus is very near the mission boundary, there should be no oobSched to cancel, but...
      cancel(%client.oobSched);

      //score the flags
      %totalScore = (%numToScore * (%numToScore + 1)) / 2;
      if (Game.teamMode)
      {
         $teamScore[%client.team] += %totalScore;
         logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") score "@%numToScore@" flags worth "@%totalScore@" pts for team "@%client.team);
         messageAll('MsgTeamScoreIs', "", %client.team, $teamScore[%client.team]);
         Game.recalcTeamRanks(%client.team);
      }
      else
      {
         %client.flagPoints += %totalScore;
         logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") score "@%numToScore@" flags worth "@%totalScore@" pts");
         Game.recalcScore(%client);

         //see if we should set the highest for the mission here
         if (%numToScore > Game.highestFlagReturnCount)
         {
            Game.highestFlagReturnCount = %numToScore;
            Game.highestFlagReturnName = getTaggedString(%client.name);
         }
      }
      
      //reset the flags
      %client.flagCount = 1;
      %client.couldSetRecord = false;

      %mask = %player.getDatablock().armorMask;
   
      if(%mask & $ArmorMask::Heavy | $ArmorMask::BattleAngel | $ArmorMask::Daishi)
      {
          // No unmount
      }
      else
           %player.unMountImage($FlagSlot);   
           
      messageClient(%client, 'MsgHuntYouHaveFlags', "", 0);
      
      //see if it's the top score
      if (!Game.teamMode && (%client.score > $TopClientScore))
         %topScore = true;
      else
         %topScore = false;
         
      //send the messages
      if (%numToScore <= 0)
      {
         messageClient(%client, 'MsgHuntYouNeedHelp', '\c2Pick up flags and bring them here to score points.~wfx/misc/nexus_idle.wav');
      }
      else if (%numToScore == 1)
      {
         if(Game.teamMode)
            messageAllExcept(%client, -1, 'MsgHuntPlayerScored', '\c2%1 returned 1 flag (1 point) for %2.~wfx/misc/nexus_cap.wav', %client.name, $TeamName[%client.team], 1);
         else
         {
            messageAllExcept(%client, -1, 'MsgHuntPlayerScored', '\c2%1 returned 1 flag for 1 point.~wfx/misc/nexus_cap.wav', %client.name, 1);

            //new tracking - *everyone* automatically tracks the "flag hoarder" if they have at least 15 flags
            Game.updateFlagHoarder(%client);
         }

         //add the nexus effect
         Game.NexusSparkEmitter(%client, true, %numToScore);

         messageClient(%client, 'MsgHuntYouScored', '\c2You returned 1 flag for 1 point.~wfx/misc/nexus_cap.wav', 1);
      }
      else if (%numToScore < 5)
      {
         if(Game.teamMode)
            messageAllExcept(%client, -1, 'MsgHuntPlayerScored', '\c2%1 returned %2 flags (%3 points) for %4.~wfx/misc/nexus_cap.wav', %client.name, %numToScore, %totalScore, $TeamName[%client.team]);
         else
         {
            messageAllExcept(%client, -1, 'MsgHuntPlayerScored', '\c2%1 returned %2 flags for %3 points.~wfx/misc/nexus_cap.wav', %client.name, %numToScore, %totalScore);

            //new tracking - *everyone* automatically tracks the "flag hoarder" if they have at least 15 flags
            Game.updateFlagHoarder(%client);
         }

         //add the nexus effect
         Game.NexusSparkEmitter(%client, true, %numToScore);

         messageClient(%client, 'MsgHuntYouScored', '\c2You returned %1 flags for %2 points.~wfx/misc/nexus_cap.wav', %numToScore, %totalScore);
      }
      else
      {
         if(Game.teamMode)
            messageAllExcept(%client, -1, 'MsgHuntPlayerScored', '\c2%1 returned %2 flags (%3 points) for %4.~wfx/misc/nexus_cap.wav', %client.name, %numToScore, %totalScore, $TeamName[%client.team]);
         else
         {
            messageAllExcept(%client, -1, 'MsgHuntPlayerScored', '\c2%1 returned %2 flags for %3 points.~wfx/misc/nexus_cap.wav', %client.name, %numToScore, %totalScore);

            //new tracking - *everyone* automatically tracks the "flag hoarder" if they have at least 15 flags
            Game.updateFlagHoarder(%client);
         }

         //add the nexus effect
         Game.NexusSparkEmitter(%client, true, %numToScore);

         messageClient(%client, 'MsgHuntYouScored', '\c2You returned %1 flags for %2 points.~wfx/misc/nexus_cap.wav', %numToScore, %totalScore);
      }
      
      //see if it's the top score
      if (%topScore)
      {
         $TopClientScore = %client.score;
         if (%client == $TopClient)
         {
            if (%numToScore >= 5)
               messageAll('MsgHuntTopScore', '\c0%1 is leading with a score of %2!~wfx/misc/flag_capture.wav', %client.name, %client.score);
            else
               messageAll('MsgHuntTopScore', '\c0%1 is leading with a score of %2!', %client.name, %client.score);
         }
         else
            messageAll('MsgHuntNewTopScore', '\c0%1 has taken the lead with a score of %2!~wfx/misc/flag_capture.wav', %client.name, %client.score);
         $TopClient = %client;
      }

      //see if it's a record
      if (%numToScore > 10 && %numToScore > $Host::HuntersRecords::Count[$currentMission])
      {
         //see if we have at least 4 non-AI players
         %humanCount = 0;
         %count = ClientGroup.getCount();
         for (%i = 0; %i < %count; %i++)
         {
            %cl = ClientGroup.getObject(%i);
            if (!%cl.isAIControlled())
               %humanCount++;
            if (%humanCount >= Game.numHumansForRecord)
               break;
         }

         if (%humanCount >= Game.numHumansForRecord)
         {
            $Host::HuntersRecords::Count[$currentMission] = %numToScore;
            $Host::HuntersRecords::Name[$currentMission] = getTaggedString(%client.name);

            //send a message to everyone
            messageAllExcept(%client, -1, 'MsgHuntPlayerSetRecord', '\c2%1 set the record for this mission with a return of %2 flags!~wfx/misc/flag_return.wav', %client.name, %numToScore);
            messageClient(%client, 'MsgHuntYouSetRecord', '\c2You set the record for this mission with a return of %1 flags!~wfx/misc/flag_return.wav', %numToScore);

            //update the records file...
            export( "$Host::HuntersRecords::*", "prefs/HuntersRecords.cs", false );

            //once the record has been set, reset everyone's tag
            for (%i = 0; %i < %count; %i++)
            {
               %cl = ClientGroup.getObject(%count);
               %cl.couldSetRecord = false;
            }
         }
      }

      if(Game.teamMode)
         Game.checkScoreLimit(%team);
      else
         Game.checkScoreLimit(%client);
   }
}

function HuntersGame::throwFlags(%game, %player)
{
   %client = %player.client;

   //find out how many flags to drop
   if (%client.isDead)
      %numFlags = %client.flagCount;
   else
      %numFlags = %client.flagCount - 1;

   if (%numFlags <= 0)
      return;
   //send the flags message right away...
   if (isEventPending(%client.flagMsgPending))
   {
      cancel(%client.flagMsgPending);
      %game.sendFlagCountMessage(%client);
   }

   %numFlagsToDrop = %numFlags;

   //reset the count (which doesn't matter if player is dead)
   %client.flagCount = 1;
   %client.couldSetRecord = false;
   
   //drop the flags
   %flagIncrement = 1;
   %db[1] = HuntersFlag1;
   %db[2] = HuntersFlag2;
   %db[4] = HuntersFlag4;
   %db[8] = HuntersFlag8;

   %i = 0;
   while (%i < %numFlagsToDrop)
   {
      for (%j = 0; %j < 5; %j++)
      {
         %numFlagsLeft = %numFlagsToDrop - %i;
         if (%numFlagsLeft >= %flagIncrement)
         {
            createDroppedFlag(%db[%flagIncrement], %flagIncrement, %player, %game);
            %i += %flagIncrement;
         }
         else
         {
            // cleanup
            if (%numFlagsLeft >= 8)
            {
               createDroppedFlag(%db[8], 8, %player, %game);
               %i += 8;
               %numFlagsLeft -= 8;
            }
            if (%numFlagsLeft >= 4)
            {
               createDroppedFlag(%db[4], 4, %player, %game);
               %i += 4;
               %numFlagsLeft -= 4;
            }
            if (%numFlagsLeft >= 2)
            {
               createDroppedFlag(%db[2], 2, %player, %game);
               %i += 2;
               %numFlagsLeft -= 2;
            }
            if (%numFlagsLeft >= 1)
            {
               createDroppedFlag(%db[1], 1, %player, %game);
               %i += 1;
               %numFlagsLeft -= 1;
            }

            if (%i != %numFlagsToDrop || %numFlagsLeft != 0)
            {
               error("Error, missing a flag!");
            }
            break;
         }
      }

      if (%flagIncrement < 8)
         %flagIncrement = %flagIncrement * 2;
   }

   //yard sale!
   if (%numFlags >= %game.yardSaleMin)
   {
      messageAll('MsgHuntYardSale', '\c2YARD SALE!!!~wfx/misc/yardsale.wav');

      //create a waypoint at player's location...
      %yardWaypoint = new WayPoint()
      {
         position = %player.position;
         rotation = "1 0 0 0";
         scale = "1 1 1";
         name = "YARD SALE!";
         dataBlock = "WayPointMarker";
         lockCount = "0";
         homingCount = "0";
         team = "0";
      };

      //add the waypoint to the cleanup group
      MissionCleanup.add(%yardWaypoint);
      $HuntersYardSaleSet.add(%yardWaypoint);
      schedule(30000, %yardWaypoint, "HuntersYardSaleTimeOut", %yardWaypoint);
   }
      
   //remove any mounted flag from the player
   %mask = %player.getDatablock().armorMask;
   
   if(%mask & $ArmorMask::Heavy | $ArmorMask::BattleAngel | $ArmorMask::Daishi)
   {
     // No unmount
   }
   else
        %player.unMountImage($FlagSlot);

   //update the client's hud
   messageClient(%client, 'MsgHuntYouHaveFlags', "", 0);

   //new tracking - *everyone* automatically tracks the "flag hoarder" if they have at least 15 flags
   %game.updateFlagHoarder(%client);
}

//------------------------------------------------------------------------------
// Voting stuff:
//------------------------------------------------------------------------------
function DefaultGame::sendGamePlayerPopupMenu( %game, %client, %targetClient, %key )
{
   if( !%targetClient.matchStartReady )
      return;

   %isAdmin = ( %client.isAdmin || %client.isSuperAdmin );
   %isTargetSelf = ( %client == %targetClient );
   %isTargetAdmin = ( %targetClient.isAdmin || %targetClient.isSuperAdmin );
   %isTargetBot = %targetClient.isAIControlled();
   %isTargetObserver = ( %targetClient.team == 0 );
   %outrankTarget = false;

   if ( %client.isSuperAdmin )
      %outrankTarget = !%targetClient.isSuperAdmin;
   else if ( %client.isAdmin )
      %outrankTarget = !%targetClient.isAdmin;

   if( %client.isSuperAdmin && %targetClient.guid != 0 ) // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   {
      messageClient( %client, 'MsgPlayerPopupItem', "", %key, "addAdmin", "", 'Add to Server Admin List', 10);
      messageClient( %client, 'MsgPlayerPopupItem', "", %key, "addSuperAdmin", "", 'Add to Server SuperAdmin List', 11);
   }

   //mute options
   if ( !%isTargetSelf )
   {
      if ( %client.muted[%targetClient] )
         messageClient( %client, 'MsgPlayerPopupItem', "", %key, "MutePlayer", "", 'Unmute Text Chat', 1);
      else
         messageClient( %client, 'MsgPlayerPopupItem', "", %key, "MutePlayer", "", 'Mute Text Chat', 1);

      if ( !%isTargetBot && %client.canListenTo( %targetClient ) )
      {
         if ( %client.getListenState( %targetClient ) )
            messageClient( %client, 'MsgPlayerPopupItem', "", %key, "ListenPlayer", "", 'Disable Voice Com', 9 );
         else
            messageClient( %client, 'MsgPlayerPopupItem', "", %key, "ListenPlayer", "", 'Enable Voice Com', 9 );
      }
      // ------------------------------------------
      // z0dd - ZOD 4/4/02. Observe a specific player
      if (%client.team == 0 && !%isTargetObserver)
         messageClient(%client, 'MsgPlayerPopupItem', "", %key, "ObservePlayer", "", 'Observe Player', 12);
   }
   if( !%client.canVote && !%isAdmin )
      return;

   // regular vote options on players
   if ( %game.scheduleVote $= "" && !%isAdmin && !%isTargetAdmin )
   {
      if ( $Host::allowAdminPlayerVotes && !%isTargetBot ) // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         messageClient( %client, 'MsgPlayerPopupItem', "", %key, "AdminPlayer", "", 'Vote to Make Admin', 2 );
      
      if ( !%isTargetSelf )
      {
         messageClient( %client, 'MsgPlayerPopupItem', "", %key, "KickPlayer", "", 'Vote to Kick', 3 );
      }
   }

   // Admin only options on players:
   else if ( %isAdmin ) // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   {
      if ( !%isTargetBot && !%isTargetAdmin )
         messageClient( %client, 'MsgPlayerPopupItem', "", %key, "AdminPlayer", "", 'Make Admin', 2 );
      
      if ( !%isTargetSelf && %outrankTarget )
      {
         messageClient( %client, 'MsgPlayerPopupItem', "", %key, "KickPlayer", "", 'Kick', 3 );

         if ( !%isTargetBot )
         {
            // ------------------------------------------------------------------------------------------------------
            // z0dd - ZOD 4/4/02. Warn player,  send private message and remove admin privledges 
            messageClient(%client, 'MsgPlayerPopupItem', "", %key, "Warn", "", 'Warn player', 13);
            messageClient( %client, 'MsgPlayerPopupItem', "", %key, "SendMessage", "", 'Send Private Message', 15 );
            if(%isTargetAdmin)
               messageClient( %client, 'MsgPlayerPopupItem', "", %key, "StripAdmin", "", 'Strip admin', 14 );

            if( %client.isSuperAdmin )
               messageClient( %client, 'MsgPlayerPopupItem', "", %key, "BanPlayer", "", 'Ban', 4 );

            if ( !%isTargetObserver )
            {
               messageClient( %client, 'MsgPlayerPopupItem', "", %key, "ToObserver", "", 'Force observer', 5 );
            }
         }
      }
      if ( %isTargetSelf || %outrankTarget )
      {
         if ( %game.numTeams > 1 )
         {   
            if ( %isTargetObserver )
            {
               %action = %isTargetSelf ? "Join " : "Change to ";
               %str1 = %action @ getTaggedString( %game.getTeamName(1) );      
               %str2 = %action @ getTaggedString( %game.getTeamName(2) );      

               messageClient( %client, 'MsgPlayerPopupItem', "", %key, "ChangeTeam", "", %str1, 6 );
               messageClient( %client, 'MsgPlayerPopupItem', "", %key, "ChangeTeam", "", %str2, 7 );
            }
            else
            {
               %changeTo = %targetClient.team == 1 ? 2 : 1;   
               %str = "Switch to " @ getTaggedString( %game.getTeamName(%changeTo) );
               %caseId = 5 + %changeTo;      

               messageClient( %client, 'MsgPlayerPopupItem', "", %key, "ChangeTeam", "", %str, %caseId );
            }
         }
         else if ( %isTargetObserver )
         {
            %str = %isTargetSelf ? 'Join the Game' : 'Add to Game';
            messageClient( %client, 'MsgPlayerPopupItem', "", %key, "JoinGame", "", %str, 8 );
         }
      }
   }
}

//------------------------------------------------------------------------------
function DefaultGame::sendGameVoteMenu( %game, %client, %key )
{
   %isAdmin = ( %client.isAdmin || %client.isSuperAdmin );
   %multipleTeams = %game.numTeams > 1;

   // no one is going anywhere until this thing starts
   if($MatchStarted)
   {
      // Client options:
      if ( %client.team != 0 )
      {
         if ( %multipleTeams )
            if( !$Host::TournamentMode )
               messageClient( %client, 'MsgVoteItem', "", %key, 'ChooseTeam', "", 'Change your Team' );
         messageClient( %client, 'MsgVoteItem', "", %key, 'MakeObserver', "", 'Become an Observer' );
      }
      else 
      {   
         if(!%multipleTeams && !$Host::TournamentMode)
            messageClient( %client, 'MsgVoteItem', "", %key, 'JoinGame', "", 'Join the Game' );
      }
      
      //%totalSlots = $Host::maxPlayers - ($HostGamePlayerCount + $HostGameBotCount);
     // if( $HostGameBotCount > 0 && %totalSlots > 0 && %client.isAdmin)
         //messageClient( %client, 'MsgVoteItem', "", %key, 'Addbot', "", 'Add a Bot' );
   }

   if( !%client.canVote && !%isAdmin )
      return;

   // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
   
   if ( %game.scheduleVote $= "" )
   {
      if(!%client.isAdmin)
      {
         // Actual vote options:
         messageClient( %client, 'MsgVoteItem', "", %key, 'VoteChangeMission', 'change the mission to', 'Vote to Change the Mission' );

         if( $Host::TournamentMode )
         {   
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteFFAMode', 'Change server to Free For All.', 'Vote Free For All Mode' );
            
            if(!$MatchStarted && !$CountdownStarted)
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteMatchStart', 'Start Match', 'Vote to Start the Match' );
         }
         else
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTournamentMode', 'Change server to Tournament.', 'Vote Tournament Mode' );
         
         if ( %multipleTeams )
         {
            if(!$MatchStarted && !$Host::TournamentMode)
               messageClient( %client, 'MsgVoteItem', "", %key, 'ChooseTeam', "", 'Change your Team' );

            if ( $teamDamage )
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'disable team damage', 'Vote to Disable Team Damage' );
            else
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'enable team damage', 'Vote to Enable Team Damage' );
         }
      }
      else
      {
         // Actual vote options:
         messageClient( %client, 'MsgVoteItem', "", %key, 'VoteChangeMission', 'change the mission to', 'Change the Mission' );
      
         if( $Host::TournamentMode )
         {   
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteFFAMode', 'Change server to Free For All.', 'Free For All Mode' );
            
            if(!$MatchStarted && !$CountdownStarted)
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteMatchStart', 'Start Match', 'Start Match' );
         }
         else
            messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTournamentMode', 'Change server to Tournament.', 'Tournament Mode' );
      
         if ( %multipleTeams )
         {
            if(!$MatchStarted)
               messageClient( %client, 'MsgVoteItem', "", %key, 'ChooseTeam', "", 'Choose Team' );

            if ( $teamDamage )
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'disable team damage', 'Disable Team Damage' );
            else
               messageClient( %client, 'MsgVoteItem', "", %key, 'VoteTeamDamage', 'enable team damage', 'Enable Team Damage' );
         }
      }
   }

   // Admin only options:
   if ( %client.isAdmin )
   {
      messageClient( %client, 'MsgVoteItem', "", %key, 'VoteChangeTimeLimit', 'change the time limit', 'Change the Time Limit' );
      messageClient( %client, 'MsgVoteItem', "", %key, 'VoteResetServer', 'reset server defaults', 'Reset the Server' );

      // -----------------------------------------------------------------------------
      // z0dd - ZOD, 5/12/02. Add bot menu for admins
      %totalSlots = $Host::maxPlayers - ($HostGamePlayerCount + $HostGameBotCount);
      if( $HostGameBotCount > 0 && %totalSlots > 0)
         messageClient( %client, 'MsgVoteItem', "", %key, 'Addbot', "", 'Add a Bot' );
      // -----------------------------------------------------------------------------
   }
}

function FlipFlop::playerTouch(%data, %flipflop, %player)
{
   %client = %player.client;
   %flipTeam = %flipflop.team;

   if(%flipTeam == %client.team || (%player.pdead && $votePracticeModeOn))
      return false;
   
   %teamName = game.getTeamName(%client.team);
   // Let the observers know:
   messageTeam( 0, 'MsgClaimFlipFlop', '\c2%1 claimed %2 for %3.~wfx/misc/flipflop_taken.wav', %client.name, Game.cleanWord( %flipflop.name ), %teamName );
   // Let the teammates know:
   messageTeam( %client.team, 'MsgClaimFlipFlop', '\c2%1 claimed %2 for %3.~wfx/misc/flipflop_taken.wav', %client.name, Game.cleanWord( %flipflop.name ), %teamName );
   // Let the other team know:
   %losers = %client.team == 1 ? 2 : 1;
   messageTeam( %losers, 'MsgClaimFlipFlop', '\c2%1 claimed %2 for %3.~wfx/packs/shield_hit.wav', %client.name, Game.cleanWord( %flipflop.name ), %teamName ); // z0dd - ZOD, 10/30/02. Change flipflop lost sound

   logEcho(%client.nameBase@" (pl "@%player@"/cl "@%client@") claimed flipflop "@%flipflop@" for team "@%client.team);

   //change the skin on the switch to claiming team's logo
   setTargetSkin(%flipflop.getTarget(), game.getTeamSkin(%player.team));
   setTargetSensorGroup(%flipflop.getTarget(), %player.team);

   // if there is a "projector" associated with this flipflop, put the claiming team's logo there
   if(%flipflop.projector > 0)
   {
      %projector = %flipflop.projector;
      // axe the old projected holo, if one exists
      if(%projector.holo > 0)
         %projector.holo.delete();
          
      %newHolo = getTaggedString(game.getTeamSkin(%client.team)) @ "Logo";

      %projTransform = %projector.getTransform();
      // below two functions are from deployables.cs
      %projRot = rotFromTransform(%projTransform);
      %projPos = posFromTransform(%projTransform);
      // place the holo above the projector (default 10 meters)
      %hHeight = %projector.holoHeight;
      if(%hHeight $= "")
         %hHeight = 10;
      %holoZ = getWord(%projPos, 2) + %hHeight;
      %holoPos = firstWord(%projPos) SPC getWord(%projPos,1) SPC %holoZ;

      %holo = new StaticShape() 
      {
         rotation = %projRot;
         position = %holoPos;
         dataBlock = %newHolo;
      };
      // dump the hologram into MissionCleanup
      MissionCleanup.add(%holo);
      // associate the holo with the projector
      %projector.holo = %holo;
   }

   // convert the resources associated with the flipflop
   Game.claimFlipflopResources(%flipflop, %client.team);
   
   addStatTrack(%client.guid, "objectivecaps", 1);
   addStatTrack(0, "objectivecaps", 1);   
   
   if(Game.countFlips())
      for(%i = 1; %i <= Game.numTeams; %i++)
      {
         %teamHeld = Game.countFlipsHeld(%i);
         messageAll('MsgFlipFlopsHeld', "", %i, %teamHeld);
      }
   
   //call the ai function
   Game.AIplayerCaptureFlipFlop(%player, %flipflop);
   return true;
}

function chatMessageAll( %sender, %msgString, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a10 )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;
   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
   {
      %obj = ClientGroup.getObject( %i );
      chatMessageClient( %obj, %sender, %sender.voiceTag, %sender.voicePitch, %msgString, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8, %a9, %a10 );
   }
}
