if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();
   
//---------------------------------------------------------------------------
// Meltdown 2 Statistic tracker
//---------------------------------------------------------------------------

//------------------------------------------------------------------------------
// String Table

if($Pref::MD2StatSaveTimer $= "")
     $Pref::MD2StatSaveTimer = 5;

$StatRankID[0] = "Civilian";
$StatRankKillCount[0] = 0;

$StatRankID[1] = "Initiate";
$StatRankKillCount[1] = 10;

$StatRankID[2] = "Recruit";
$StatRankKillCount[2] = 25;

$StatRankID[3] = "Soldier";
$StatRankKillCount[3] = 50;

$StatRankID[4] = "Private";
$StatRankKillCount[4] = 100;

$StatRankID[5] = "Specialist";
$StatRankKillCount[5] = 150;

$StatRankID[6] = "Corporal";
$StatRankKillCount[6] = 250;

$StatRankID[7] = "Sergeant";
$StatRankKillCount[7] = 400;

$StatRankID[8] = "Lieutenant";
$StatRankKillCount[8] = 575;

$StatRankID[9] = "Captain";
$StatRankKillCount[9] = 775;

$StatRankID[10] = "Lt. Major";
$StatRankKillCount[10] = 1000;

$StatRankID[11] = "Major";
$StatRankKillCount[11] = 1250;

$StatRankID[12] = "Lt. Colonel";
$StatRankKillCount[12] = 1600;

$StatRankID[13] = "Colonel";
$StatRankKillCount[13] = 2000;

$StatRankID[14] = "Commander";
$StatRankKillCount[14] = 2500;

$StatRankID[15] = "Lt. General";
$StatRankKillCount[15] = 3100;

$StatRankID[16] = "General";
$StatRankKillCount[16] = 3750;

$StatRankID[17] = "Warlord";
$StatRankKillCount[17] = 4800;

$StatRankID[18] = "Overlord";
$StatRankKillCount[18] = 6000;

$StatRankID[19] = "High Overlord";
$StatRankKillCount[19] = 8000;

$StatRankID[20] = "Master";
$StatRankKillCount[20] = 11500;

$StatRankID[21] = "Elite";
$StatRankKillCount[21] = 15000;

$StatRankID[22] = "Dominator";
$StatRankKillCount[22] = 31337;

$StatRankID[23] = "Lord";
$StatRankKillCount[23] = 50000;

$StatRanks = 24;

//------------------------------------------------------------------------------
// Functions

function MDStatTracker::onInit(%this)
{
     MDStatTracker.Version = 1.1;
}

function MDStatTracker::onDie(%this)
{
     // No spam in the champagne room
}

if(!isObject(MDStatTracker))
     Meltdown.addClass(MDStatTracker); // Statistics Tracking class

function initStatTracker()
{
     if(Meltdown.statTrackThread $= "")
     {
          echo("Initializing server stat tracking...");
          
          readAccountData(0);
          Meltdown.statTrackThread = schedule($Pref::MD2StatSaveTimer * 60000, 0, statTrackerUpdate);
          
          echo(formatTimeString("H:nn:ss") SPC "- Beginning statistic tracking thread");
     }
}
   
function addStatTrack(%guid, %type, %delta)
{
     // Verify we have a GUID and type
     if(%guid $= "" || %type $= "" || %delta $= "")
          return;
          
     // Initialize any empty stats
     if($MDStat[%guid, %type] $= "")
          $MDStat[%guid, %type] = 0;
          
     // This GUID has a ban on this particular stat 
     if($MDStat[%guid, %type] == -1)
          return;
     
     %val = $MDStat[%guid, %type] + %delta;
     
     // Overflow check
     if(%val < -1) 
          %val = 1 << 31;
     
     // All good here, modify this stat
     $MDStat[%guid, %type] = %val;
     
     // Send this update through the list to see if there's any dependancies
     statTrackUpdate(%guid, %type, %val, %delta); 
}

function statTrackUpdate(%guid, %type, %current, %delta)
{
     switch$(%type)
     {
          case "kills":
               if(%guid > 0) // prevent bots from getting ranks
                    statProcessKillRank(%guid, %current);
               
          default:
               return;
     }
}

function getTrackedStat(%guid, %type)
{
     // Verify we have a GUID and type
     if(%guid $= "" || %type $= "")
          return;

     %stat = $MDStat[%guid, %type];

     // Make sure the stat isn't blank     
     if(%stat $= "")
          %stat = 0;

     // Return it!
     return %stat;   
}

function exportMD2Stats()
{
//     echo(formatTimeString("H:nn:ss") SPC "Saving Meltdown 2 user database...");
     
     %count = ClientGroup.getCount();

     for(%i = 0; %i < %count; %i++)
     {
          %client = ClientGroup.getObject(%i);

          if(!%client.isAIControlled())
               saveClientAccountData(%client);
     }
     
     writeAccountData(0);

     export("$MDGUIDList*", "mdb/serverGUIDList.dat", false);
     export("$MDGUIDLookup*", "mdb/serverGUIDLookup.dat", false);
     export("$AutoBanList*", "autobanlist.cs", false);
}

function addToGUIDLookup(%guid, %name)
{
     for(%index = 0; %index < $MDGUIDListCount; %index++)
          if($MDGUIDList[%index] == %guid)
               return;
     
     $MDGUIDList[%index] = %guid;
     $MDGUIDLookup[%guid] = %name;
     $MDGUIDListCount++;
}

function findOfflineAccount(%name)
{
     for(%index = 0; %index < $MDGUIDListCount; %index++)
     {
          %guid = $MDGUIDList[%index];

          if($MDGUIDLookup[%guid] $= %name)
               return %guid;
     }

     for(%index = 0; %index < $MDGUIDListCount; %index++)
     {
          %guid = $MDGUIDList[%index];

          if(strstr(strlwr($MDGUIDLookup[%guid]), strlwr(%name)) != -1)
               return %guid;
     }

     return 0;
}

function readAccountData(%guid)
{
     if(!%guid)
     {
          exec("mdb/serverStats.dat");
          exec("mdb/serverGUIDList.dat");
          exec("mdb/serverGUIDLookup.dat");
          exec("autobanlist.cs");
          echo("Loaded server-wide statistic data.");
     }
     else
     {
          %found = isFile("mdb/"@%guid@"/stats.dat");
          
          if(%found)
          {
               exec("mdb/"@%guid@"/stats.dat");
               exec("mdb/"@%guid@"/account.dat");
               exec( "mdb/"@%guid@"/mech.dat" ) ;	// +soph
               exec( "mdb/"@%guid@"/vehicle.dat" ) ;	// +soph
               
               echo("[AccountManager] Loaded account data for:" SPC %guid SPC $MDAccountData[%guid, "account"]);
               
               $MDAccountData[%guid, "account"] = %client.realName;               
               $MDAccountData[%guid, "lastLogin"] = formatTimeString("MM d H:nn:ss");
               
               return true;                       
          }
          else
          {
               $MDAccountData[%guid, "account"] = %client.realName;               
               $MDAccountData[%guid, "lastLogin"] = formatTimeString("MM d H:nn:ss");
                         
               echo("[AccountManager] Data not found for:" SPC %guid SPC "- using blank profile.");
               
               return false;
          }
     }
}

function writeAccountData(%guid)
{
     if(!%guid)
          export("$MDStat"@%guid@"*", "mdb/serverStats.dat", false);
     else
     {
          export("$MDStat"@%guid@"*", "mdb/"@%guid@"/stats.dat", false);
          export("$MDAccountData"@%guid@"*", "mdb/"@%guid@"/account.dat", false);
          export( "$MDMechData" @ %guid @ "*" , "mdb/" @ %guid @ "/mech.dat" , false ) ;	// +soph
          export( "$MDVehicleData" @ %guid @ "*" , "mdb/" @ %guid @ "/vehicle.dat" , false ) ;	// +soph
     }
}

function dumpStats()
{
     // Export stats step - this is handled internally
     echo("Dumping stats...");
     return;
}

function statTrackerUpdate()
{
     exportMD2Stats();
     Meltdown.statTrackThread = schedule($Pref::MD2StatSaveTimer * 60000, 0, statTrackerUpdate);
}

//------------------------------------------------------------------------------
// Stat tracker update callbacks

function statProcessKillRank(%guid, %current)
{
     %rankup = false;
     %currentRank = getTrackedStat(%guid, "rank");
     %kills = getTrackedStat(%guid, "kills");
     %rankKillCount = 0;
     
     for(%i = %currentRank; %i < $StatRanks; %i++)
     {
          if(%kills >= $StatRankKillCount[%i])
          {
               %rankup = true;
               %rankKillCount = $StatRankKillCount[%i];
               %currentRank++;
               break;
          }
     }
     
     if(%rankup)
     {
          %client = GUIDToClientID(%guid);

          if(%client)
          {
               messageClient(%client, 'MsgRankup', '\c2Congratulations, you have passed the %1 kill mark and become a %2!', %rankKillCount, $StatRankID[%currentRank]);
               chatMessageAll(%client, '\c4%1 has attained the \'%2\' rank! (%3)', %client.namebase, $StatRankID[%currentRank], %rankKillCount);               
          }
          
          addStatTrack(%guid, "rank", 1);
     }
}
