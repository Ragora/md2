if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Variables EXEC

exec("scripts/modscripts/RMS.cs");

exec("scripts/weapons/energyRifle.cs");
exec("scripts/weapons/Protron.cs"); 
exec("scripts/weapons/AutoCannon.cs");
exec("scripts/weapons/MBCannon.cs");
exec("scripts/weapons/PCR.cs");
//exec("scripts/weapons/PCC.cs");
exec("scripts/weapons/PlasmaCannon.cs");
exec("scripts/weapons/PBW.cs");
//exec("scripts/weapons/MagMissileLauncher.cs");
exec("scripts/weapons/Multifusor.cs");
exec("scripts/weapons/starburst.cs");
exec("scripts/weapons/EnergyProjectorCannon.cs");
exec("scripts/weapons/MechMinigun.cs");
exec("scripts/weapons/MechRocketGun.cs");
exec("scripts/weapons/raxx.cs");
exec("scripts/weapons/EMPGrenade.cs");
exec("scripts/weapons/mortarGrenade.cs");
exec("scripts/weapons/FireGrenade.cs");
exec("scripts/weapons/PoisonGrenade.cs");
exec("scripts/packs/IonTAGPack.cs");

exec("scripts/weapons/LAR.cs");
exec("scripts/weapons/MegaBlaster.cs");
exec("scripts/weapons/spike.cs");

exec("scripts/packs/heatshield.cs");
//exec("scripts/packs/HoverPack.cs");
//exec("scripts/packs/magionpack.cs");
exec("scripts/packs/starhammerpack.cs");
exec("scripts/packs/blastercannonpack.cs");
exec("scripts/packs/PulseCannonPack.cs");
exec("scripts/packs/CometCannonPack.cs");
exec("scripts/packs/synomiumPack.cs");
exec("scripts/packs/slipstreampack.cs");
exec("scripts/packs/MASC.cs");
exec("scripts/packs/LaserCannon.cs");
//exec("scripts/packs/LaserBeamer.cs");
//exec("scripts/packs/heatIonPack.cs");
exec("scripts/packs/GravArmorPack.cs");
exec("scripts/packs/PolarArmorPack.cs");
exec("scripts/packs/StreakSRM4Pack.cs");
exec("scripts/packs/StreakSRM2Pack.cs");	// +soph
exec("scripts/packs/StreakSRM1Pack.cs");	// +soph
exec("scripts/packs/ShockBlasterPack.cs");

exec("scripts/packs/DiscCannonPack.cs");
exec("scripts/packs/GrenadeCannonPack.cs");
exec("scripts/packs/DualGrenadeCannon.cs");
exec("scripts/packs/ShotgunCannonPack.cs");
//exec("scripts/packs/PlasmaCannonPack.cs");	// obsolete -soph
exec("scripts/packs/ACCannonPack.cs");

exec("scripts/packs/FlameTurretPack.cs");
exec("scripts/packs/Phaserbarrelpack.cs");
exec("scripts/packs/ChainTurretPack.cs");
exec("scripts/packs/EnergyTurretPack.cs");
exec("scripts/packs/MantaAmmoPack.cs");
//exec("scripts/packs/MantaNukePack.cs");
exec("scripts/packs/RMSAmmoPack.cs");
exec("scripts/packs/VectorPortPack.cs");
