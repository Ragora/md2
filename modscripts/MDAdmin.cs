if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

function calcVotes(%typeName, %arg1, %arg2, %arg3, %arg4)
{
   if(%typeName $= "voteMatchStart")
      if($MatchStarted || $countdownStarted)
         return;
   
   for ( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) 
   {
      %cl = ClientGroup.getObject( %clientIndex );
      messageClient(%cl, 'closeVoteHud', "");
      
      if ( %cl.vote !$= "" ) 
      {
         if ( %cl.vote ) 
         {
            Game.votesFor[%cl.team]++;
            Game.totalVotesFor++;
         } 
         else 
         {
            Game.votesAgainst[%cl.team]++;
            Game.totalVotesAgainst++;
         }
      }
      else 
      {
         Game.votesNone[%cl.team]++;
         Game.totalVotesNone++;
      }
   }   
   //Tinman - PURE servers can't call "eval"
   //Mark - True, but neither SHOULD a normal server
   //     - thanks Ian Hardingham
   Game.evalVote(%typeName, false, %arg1, %arg2, %arg3, %arg4);
   Game.scheduleVote = "";
   Game.kickClient = "";
   clearVotes();
}

function DefaultGame::voteChangeMission(%game, %admin, %missionDisplayName, %typeDisplayName, %missionId, %missionTypeId)
{
   %mission = $HostMissionFile[%missionId];
   if ( %mission $= "" )
   {
      error( "Invalid mission index passed to DefaultGame::voteChangeMission!" );
      return;
   }

   %missionType = $HostTypeName[%missionTypeId];
   if ( %missionType $= "" )
   {
      error( "Invalid mission type id passed to DefaultGame::voteChangeMission!" );
      return;
   }

   if(%admin) 
   {
      messageAll('MsgAdminChangeMission', '\c2The Admin has changed the mission to %1 (%2).', %missionDisplayName, %typeDisplayName );   
      logEcho("mission changed to "@%missionDisplayName@"/"@%typeDisplayName@" (admin)");
      %game.gameOver();
      loadMission( %mission, %missionType, false );   
   }
   else 
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100))
      {
         messageAll('MsgVotePassed', '\c2The mission was changed to %1 (%2) by vote.', %missionDisplayName, %typeDisplayName ); 
         logEcho("mission changed to "@%missionDisplayName@"/"@%typeDisplayName@" (vote)");
         %game.gameOver();
         loadMission( %mission, %missionType, false );   
      }
      else
         messageAll('MsgVoteFailed', '\c2Change mission vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100)); 
   }
}

//------------------------------------------------------------------------------
function DefaultGame::voteTeamDamage(%game, %admin)
{
   %setto = "";
   %cause = "";
   if(%admin) 
   {
      if($teamDamage)
      {
         messageAll('MsgAdminForce', '\c2The Admin has disabled team damage.');   
         $Host::TeamDamageOn = $TeamDamage = 0;
         %setto = "disabled";
      }
      else 
      {
         messageAll('MsgAdminForce', '\c2The Admin has enabled team damage.');   
         $Host::TeamDamageOn = $TeamDamage = 1;
         %setto = "enabled";
      }
      %cause = "(admin)";
   }
   else 
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100))
      {
         if($teamDamage) 
         {
            messageAll('MsgVotePassed', '\c2Team damage was disabled by vote.'); 
            $Host::TeamDamageOn = $TeamDamage = 0;
            %setto = "disabled";
         }
         else 
         {
            messageAll('MsgVotePassed', '\c2Team damage was enabled by vote.');  
            $Host::TeamDamageOn = $TeamDamage = 1;
            %setto = "enabled";
         }
         %cause = "(vote)";
      }
      else 
      {
         if($teamDamage)
            messageAll('MsgVoteFailed', '\c2Disable team damage vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100));  
         else 
            messageAll('MsgVoteFailed', '\c2Enable team damage vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100));   
      }
   }
   if(%setto !$= "")
      logEcho("team damage "@%setto SPC %cause);
}

//------------------------------------------------------------------------------
function DefaultGame::voteTournamentMode( %game, %admin, %missionDisplayName, %typeDisplayName, %missionId, %missionTypeId )
{
   %mission = $HostMissionFile[%missionId];
   if ( %mission $= "" )
   {
      error( "Invalid mission index passed to DefaultGame::voteTournamentMode!" );
      return;
   }

   %missionType = $HostTypeName[%missionTypeId];
   if ( %missionType $= "" )
   {
      error( "Invalid mission type id passed to DefaultGame::voteTournamentMode!" );
      return;
   }

   %cause = "";
   if (%admin) 
   {
      messageAll( 'MsgAdminForce', '\c2The Admin has switched the server to Tournament mode (%1).', %missionDisplayName );
      setModeTournament( %mission, %missionType );
      %cause = "(admin)";
   }
   else 
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100)) 
      {
         messageAll('MsgVotePassed', '\c2Server switched to Tournament mode by vote (%1): %2 percent.', %missionDisplayName, mFloor(%game.totalVotesFor/%totalVotes * 100)); 
         setModeTournament( %mission, %missionType );
         %cause = "(vote)";
      }
      else
         messageAll('MsgVoteFailed', '\c2Tournament mode vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100));
   }
   if(%cause !$= "")
      logEcho("tournament mode set "@%cause);
}

//------------------------------------------------------------------------------
function DefaultGame::voteMatchStart( %game, %admin)
{
   %cause = "";
   %ready = forceTourneyMatchStart();
   if(%admin)
   {
      if(!%ready)
      {   
         messageClient( %client, 'msgClient', '\c2No players are ready yet.');
         return;
      }
      else
      {
         messageAll('msgMissionStart', '\c2The admin has forced the match to start.');
         %cause = "(admin)";
         startTourneyCountdown();
      }
   }
   else
   {
      if(!%ready)
      {
         messageAll( 'msgClient', '\c2Vote passed to start match, but no players are ready yet.');
         return;
      }
      else
      {  
         %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
         if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100)) 
         {
            messageAll('MsgVotePassed', '\c2The match has been started by vote: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100));  
            startTourneyCountdown();
         } 
         else
            messageAll('MsgVoteFailed', '\c2Start Match vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100)); 
      }
   }
   
   if(%cause !$= "")
      logEcho("start match "@%cause);
}

//------------------------------------------------------------------------------
function DefaultGame::voteFFAMode( %game, %admin, %client )
{
   %cause = "";
   %name = getTaggedString(%client.name);
   
   if (%admin) 
   {
      messageAll('MsgAdminForce', '\c2The Admin has switched the server to Free For All mode.', %client);   
      setModeFFA($CurrentMission, $CurrentMissionType); 
      %cause = "(admin)";
   }
   else 
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100)) 
      {
         messageAll('MsgVotePassed', '\c2Server switched to Free For All mode by vote.', %client); 
         setModeFFA($CurrentMission, $CurrentMissionType); 
         %cause = "(vote)";
      }
      else 
         messageAll('MsgVoteFailed', '\c2Free For All mode vote did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100)); 
   }
   if(%cause !$= "")
      logEcho("free for all set "@%cause);
}

//------------------------------------------------------------------------------
function DefaultGame::voteChangeTimeLimit( %game, %admin, %newLimit )
{
   if( %newLimit == 999 )
      %display = "unlimited";
   else
      %display = %newLimit;
      
   %cause = "";
   if ( %admin )
   {
      messageAll( 'MsgAdminForce', '\c2The Admin changed the mission time limit to %1 minutes.', %display );
      $Host::TimeLimit = %newLimit;
      %cause = "(admin)";
   }
   else
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100)) 
      {
         messageAll('MsgVotePassed', '\c2The mission time limit was set to %1 minutes by vote.', %display);   
         $Host::TimeLimit = %newLimit;
         %cause = "(vote)";
      }
      else 
         messageAll('MsgVoteFailed', '\c2The vote to change the mission time limit did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100));   
   }

   //if the time limit was actually changed...
   if(%cause !$= "")
   {
      logEcho("time limit set to "@%display SPC %cause);

      //if the match has been started, reset the end of match countdown
      if ($matchStarted)
      {
         //schedule the end of match countdown
         %elapsedTimeMS = getSimTime() - $missionStartTime;
         %curTimeLeftMS = ($Host::TimeLimit * 60 * 1000) - %elapsedTimeMS;
			error("time limit="@$Host::TimeLimit@", elapsed="@(%elapsedTimeMS / 60000)@", curtimeleftms="@%curTimeLeftMS);
         CancelEndCountdown();
         EndCountdown(%curTimeLeftMS);
         cancel(%game.timeSync);
         %game.checkTimeLimit(true);
      }
   }
}

//------------------------------------------------------------------------------
function DefaultGame::voteResetServer( %game, %admin, %client )
{
   %cause = "";
   if ( %admin )
   {
      messageAll( 'AdminResetServer', '\c2The Admin has reset the server.' );
      resetServerDefaults();
      %cause = "(admin)";
   }
   else
   {
      %totalVotes = %game.totalVotesFor + %game.totalVotesAgainst;
      if(%totalVotes > 0 && (%game.totalVotesFor / %totalVotes) > ($Host::VotePasspercent / 100)) 
      {
         messageAll('MsgVotePassed', '\c2The Server has been reset by vote.' );  
         resetServerDefaults();
         %cause = "(vote)";
      }
      else 
         messageAll('MsgVoteFailed', '\c2The vote to reset Server to defaults did not pass: %1 percent.', mFloor(%game.totalVotesFor/%totalVotes * 100));  
   }
   if(%cause !$= "")
      logEcho("server reset "@%cause);
}

//------------------------------------------------------------------------------
// all team based votes here
function DefaultGame::voteKickPlayer(%game, %admin, %client)
{
   %cause = "";
   
   if(%admin) 
   {
      kick(%client, %admin, %client.guid );
      %cause = "(admin)";
   }
   else 
   {
      %team = %client.team;
      %totalVotes = %game.votesFor[%game.kickTeam] + %game.votesAgainst[%game.kickTeam];
      if(%totalVotes > 0 && (%game.votesFor[%game.kickTeam] / %totalVotes) > ($Host::VotePasspercent / 100)) 
      {
         kick(%client, %admin, %game.kickGuid);
         %cause = "(vote)";
      }
      else
      {   
         for ( %idx = 0; %idx < ClientGroup.getCount(); %idx++ ) 
         {
            %cl = ClientGroup.getObject( %idx );

            if (%cl.team == %game.kickTeam && !%cl.isAIControlled())
               messageClient( %cl, 'MsgVoteFailed', '\c2Kick player vote did not pass' ); 
         }
      }
   }
   
   %game.kickTeam = "";
   %game.kickGuid = "";
   %game.kickClientName = "";

   if(%cause !$= "")
      logEcho(%name@" (cl " @ %game.kickClient @ ") kicked " @ %cause);
}
