//-----------------------------------------------------------------------------
// Xi Command System
// Mode Keys

function Xi::antialias(%this, %cl, %val)
{
     %name = nameToClient(%val);
     
     if(%name)
          messageClient(%cl, 'MsgXIError22', '\c3Xi[Antialias]: %1 is %2.', %name.name, %name.realname);
     else
          messageClient(%cl, 'MsgXIError24', '\c5Xi[Antialias]: This name does not exist on the server.');
}

Xi.addCommand($XI::Admin, "antialias", "Returns the real name of the player - ex. /antialias smurf");

