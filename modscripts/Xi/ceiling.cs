//-----------------------------------------------------------------------------
// Xi Command System
// Set Flight Ceiling

function Xi::ceiling(%this, %cl, %val)
{
     %val = firstWord(%val);
     
     if(%val < 100)
          %val = 100;
     
     // Prevent setting ceiling height as a word
     if(%val == 0)
          return;
     
     setFlightCeiling(%val);
     messageClient(%cl, 'MsgXIError26', '\c5Xi[Ceiling]: Flight ceiling set to %1.', %val);
}

Xi.addCommand($XI::SuperAdmin, "ceiling", "Sets the mission flight ceiling - ex. /ceiling 1000");

