//-----------------------------------------------------------------------------
// Xi Command System
// Mode Keys

function Xi::modekeys(%this, %cl, %val)
{
      if(%cl.modekeys)
      {
         %cl.modekeys = false;
         messageClient(%cl, 'MsgXIError1', '\c3Xi[ModeKeys]: 1-6 now select weapons 1-6');
      }
      else
      {
         %cl.modekeys = true;
         messageClient(%cl, 'MsgXIError2', '\c3Xi[ModeKeys]: 1-6 now select modes 1-6');
      }
}

Xi.addCommand($XI::Player, "modekeys", "Allows you to set keys 1-6 to toggle weapons 1-6 or modes 1-6 - ex. /modekeys");

