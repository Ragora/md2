//-----------------------------------------------------------------------------
// Xi Command System
// MANTA Ion Cannon hit

function Xi::ionzap(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
                 messageClient(%cl, 'MsgXICantTorture', '\c5Torture: Cannot torture %1, they are higher level than you.', %client.nameBase);
            else
            {
               if(isObject(%client.player))
               {
                    %client.player.setInvincible(false);
                    %client.player.f_hold = true;
                    holdObject(%client.player, %client.player.position, 32);
                    schedule(4000, 0, disableHoldObject, %client.player);
                    IonZapMe(%client.player);
                    messageClient(%cl, 'MsgXITorture', '\c5Torture: Applied retribution to %1.', %client.nameBase);                    
               }
               else
                    messageClient(%cl, 'MsgXITortureNoPlayer', '\c5Torture: %1 has not spawned.', %client.nameBase);               
            }
         }
      }
      else
         messageClient(%cl, 'MsgXITortureNoName', '\c5Torture: No name specified.');
}

function IonZapMe(%obj)
{
   if(isObject(%obj))
   {
      %pos = %obj.getPosition();
      %pos2 = vectorAdd(%pos, "0 0 1000");

            %FFRObject = new StaticShape()
            {
               dataBlock        = mReflector;
            };
            MissionCleanup.add(%FFRObject);
            %FFRObject.setPosition(%pos2);
            %FFRObject.schedule(32, delete);

            %p = new EnergyProjectile()
            {
               dataBlock        = IONBeam;
               initialDirection = "0 0 -1"; //"0 0 -1";
               initialPosition  = %pos2;
               sourceObject     = %FFRObject;
               sourceSlot       = 0;
               vehicleObject    = 0;
               bkSourceObject   = %obj;
               ignoreReflections = true;
            };
            MissionCleanup.add(%p);

            %h = new LinearFlareProjectile()
            {
               dataBlock        = IONBeamHead;
               initialDirection = "0 0 -1"; //"0 0 -1";
               initialPosition  = %pos2;
               sourceObject     = %FFRObject;
               sourceSlot       = 0;
               vehicleObject    = 0;
               bkSourceObject   = %obj;
               ignoreReflections = true;
            };
            MissionCleanup.add(%h);

            projectileTrail(%h, 50, LinearFlareProjectile, IonCannonTrailCharge);
   }
}

Xi.addCommand($XI::Admin, "ionzap", "Fires a MANTA Ion cannon beam at the player (partial names accepted) - ex. /ionzap noob");

