//-----------------------------------------------------------------------------
// Xi Command System
// Save Mech/Vehicle pref

$MaxPrefSaveSlots = 5;

function Xi::save(%this, %cl, %val)
{
     %mv = firstWord(%val);
     %type = 0;
     
     switch$(%mv)
     {
          case "mech" or "Mech":
               %type = 1;
               
          case "vehicle" or "Vehicle":
               %type = 2;
               
          default:
               messageClient(%cl, 'MsgSaveInvalid', '\c3Save: Invalid pref type. Accepted prefs are: mech, vehicle', %val);
               return;
     }
    
     %slot = getWord(%val, 1);
     %maxSaveSlot = $MaxPrefSaveSlots;
     
     if(%slot < 1 || %slot > %maxSaveSlot)
     {
          messageClient(%cl, 'MsgSaveOOB', '\c3Save: You can only save to slots 1-%1.', %maxSaveSlot);          
          return;
     }

     %desc = trim(getSubStr(%val, 3 + strlen(%mv), 16));

     if(%desc $= "")
          %desc = "Saved Pref" SPC %slot;

     // Shift over one for internal purposes
     %slot--;     
     %guid = %cl.guid;
     
     // Get ID from datablock/current viewed pref
     switch(%type)
     {
          case 1:
               %vID = $VehicleID[$VehicleMask::Shrike];
               $MDVehiclePref[%guid, %slot, "prefName"] = %desc;
               $MDVehiclePref[%guid, %slot, "vID"] = %vID;
               $MDVehiclePref[%guid, %slot, "vModPriWeapon"] = %client.vModPriWeapon[%vID];
               $MDVehiclePref[%guid, %slot, "vModSecWeapon"] = %client.vModSecWeapon[%vID];
               $MDVehiclePref[%guid, %slot, "vModule"] = %client.vModule[%vID];
               $MDVehiclePref[%guid, %slot, "vModShield"] = %client.vModShield[%vID];
               $MDVehiclePref[%guid, %slot, "vModArmor"] = %client.vModArmor[%vID];

          case 2:
               %mechID = $MechID[$MechMask::Emancipator];
               %vData = "Emancipator";
               $MDMechPref[%guid, %slot, "prefName"] = %desc;
               $MDMechPref[%guid, %slot, "mechID"] = %mechID;
               $MDMechPref[%guid, %slot, "vData"] = %vData;
               $MDMechPref[%guid, %slot, "modArmorVal"] = %client.modArmorVal[%vData];
               $MDMechPref[%guid, %slot, "modHeatsinkVal"] = %client.modHeatsinkVal[%vData];
               $MDMechPref[%guid, %slot, "mModule"] = %client.mModule[%vData];
               $MDMechPref[%guid, %slot, "mEquip"] = %client.mEquip[%vData];
               $MDMechPref[%guid, %slot, "mechWeaponSlotID", 0] = %client.mechWeaponSlotID[%vData, 0];
               $MDMechPref[%guid, %slot, "mechWeaponSlotID", 1] = %client.mechWeaponSlotID[%vData, 1];
               $MDMechPref[%guid, %slot, "mechWeaponSlotID", 2] = %client.mechWeaponSlotID[%vData, 2];
               $MDMechPref[%guid, %slot, "mechWeaponSlotID", 3] = %client.mechWeaponSlotID[%vData, 3];
               $MDMechPref[%guid, %slot, "mechWeaponSlotID", 4] = %client.mechWeaponSlotID[%vData, 4];
               $MDMechPref[%guid, %slot, "mechWeaponSlotID", 5] = %client.mechWeaponSlotID[%vData, 5];                                                  
               $MDMechPref[%guid, %slot, "mechSlotFireGroup", 2] = %client.mechSlotFireGroup[%vData, 2];
               $MDMechPref[%guid, %slot, "mechSlotFireGroup", 3] = %client.mechSlotFireGroup[%vData, 3];
               $MDMechPref[%guid, %slot, "mechSlotFireGroup", 4] = %client.mechSlotFireGroup[%vData, 4];
               $MDMechPref[%guid, %slot, "mechSlotFireGroup", 5] = %client.mechSlotFireGroup[%vData, 5];
               $MDMechPref[%guid, %slot, "mechSlotFireGroup", 6] = %client.mechSlotFireGroup[%vData, 6];
               $MDMechPref[%guid, %slot, "mechSlotFireGroup", 7] = %client.mechSlotFireGroup[%vData, 7];      
                    
          default:
               error("[Xi::save] This should not happen.");
     }

     messageClient(%cl, 'MsgSaveSuccess', '\c3Save: %1 pref \'%2\' saved to slot %3.', %mv, %desc, %slot);
}

// Disabled until properly working
Xi.addCommand($XI::Player, "save", "Saves your current Mech or Vehicle preference - ex. /save mech 1 Eliminator");

