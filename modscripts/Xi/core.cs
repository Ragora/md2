//-----------------------------------------------------------------------------
// Xi Command System
// Core Functions

function Xi::commands(%this, %client, %val)
{
    %cmds = Xi.listLevelCommands(%client);
    %cmdlist = getWords(%cmds, 1, firstWord(%cmds));
    
    messageClient(%client, 'MsgXiCommandList', '\c2Commands you have access to:%1', %cmdlist);
}

Xi.addCommand($XI::Player, "commands", "Shows all the commands you have access to - ex. /commands");

function Xi::help(%this, %client, %val)
{
    if(Xi.isCommand(firstWord(%val)))
        messageClient(%client, 'MsgXiHelp', '\c2%1', Xi.getCmdDesc(%val));
    else
    {
        %cmds = Xi.listLevelCommands(%client);
        %cmdlist = getWords(%cmds, 1, firstWord(%cmds));

        messageClient(%client, 'MsgXiHelpFail', '\c2This is not a valid command. Commands you have access to:%1', %cmdlist);
    }
}

Xi.addCommand($XI::Player, "help", "Displays the function of the command - ex. /help level");

// Server message
function smsg(%text)
{
    messageAll('MsgServerChat', '\c1[IRC] %1', %text);
}

function getPlayerCount(%ret)
{
	%clients = ClientGroup.getCount();
	%players = 0;
    %observers = 0;
    %teamplayers = 0;
    %playerList = "";

	for(%i = 0; %i < %clients; %i++)
	{
        %client = ClientGroup.getObject(%i);

		if(%client.isAIControlled())
			continue;

        if(%client.team > 0)
           %players++;
        else
           %observers++;

        %playerList = %playerList@" "@%client.nameBase;
	}

	%bots = %clients - (%players + %observers);
    %teamplayers = %players - %observers;

    if(%ret)
        return %players;	// return %teamplayers; -soph
    else
    {
    	echo("Players: "@%clients@" ("@%bots@" bots, "@%observers@" observers, "@%players@" humans)");

        if(%players)
        {
         	echo("List of Players:"@%playerList);
	schedule(2000, 0, echo, "bump");
        }
    }
}