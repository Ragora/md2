//-----------------------------------------------------------------------------
// Xi Command System
// Set Bot Talk

function Xi::bottalk(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %val = firstWord(%val);

         switch$(%val)
         {
              case "on":
                    %bool = false;                  

              case "off":
                    %bool = true;                  

              default:
                    %bool = -1;
         }

         if(%bool > -1)
         {
              $Host::DisableBotTalk = %bool;
              %tf = %bool ? "off" : "on";

              messageClient(%cl, 'MsgXIBotTalk', '\c5AI: Bot talk is now %1.', %tf);
              return;
         }
      }

      messageClient(%cl, 'MsgXIErrorBot', '\c5AI: Input must be "on" or "off".');
}

Xi.addCommand($XI::Admin, "bottalk", "Turns on or off bot chat - ex: /bottalk on");

