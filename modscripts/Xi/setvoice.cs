//-----------------------------------------------------------------------------
// Xi Command System
// Mode Keys

function Xi::setvoice(%this, %cl, %val)
{
     %val = firstWord(%val);
     
     if(%val !$= "")
     {
          // keen: todo - optimize
          if(%val $= "Male1" || %val $= "Male2" || %val $= "Male3" || %val $= "Male4" || %val $= "Male5" || %val $= "Fem1" || %val $= "Fem2" || %val $= "Fem3" || %val $= "Fem4" || %val $= "Fem5" || %val $= "Bot1" || %val $= "Derm1" || %val $= "Derm2" || %val $= "Derm3" || %val $= "NULL")
          {
               %cl.voice = %val;
               %cl.voiceTag = addTaggedString(%val);
               setTargetVoice(%cl.target, %cl.voiceTag);
          }
          else
          {
               messageClient(%cl, 'MsgXIError6', '\c5Xi[VoiceChanger]: %1 is not a valid voice. Valid voices are Male1-5, Fem1-5, Bioderm1-3, Bot1, and NULL.', %val);
               return;
          }

          messageClient(%cl, 'MsgXIError7', '\c3Xi[VoiceChanger]: Voice changed to %1.', %val);
          return;
     }
     else
     {
          messageClient(%cl, 'MsgXIError8', '\c5Xi[VoiceChanger-Error]: No voice specified.');
          return;
     }
}

Xi.addCommand($XI::Player, "setvoice", "Allows you to set your voice chat to a different setting - ex. /setvoice Bot1");

