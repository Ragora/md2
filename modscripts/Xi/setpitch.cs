//-----------------------------------------------------------------------------
// Xi Command System
// Set Voice Pitch

function Xi::setpitch(%this, %cl, %val)
{
     %val = firstWord(%val);
     
     if(%val !$= "" && (%val >= 0.5 && %val <= 2.0))
     {
          %cl.voicePitch = %val;
          setTargetVoicePitch(%cl.target, %cl.voicePitch);
          messageClient(%cl, 'MsgXIError20', '\c3Xi[VoicePitchCtrl]: Voice pitch set to %1', %cl.voicePitch);
          return;
     }
     else
     {
          messageClient(%cl, 'MsgXIError21', '\c5Xi[VarianceCtrl]: Out of bounds. Must be within [0.5 - 2.0].');
          return;
     }
}

Xi.addCommand($XI::Player, "setpitch", "Sets your voice pitch - ex. /setpitch 1.5");

