//-----------------------------------------------------------------------------
// Xi Command System
// Combust

function Xi::combust(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
                 messageClient(%cl, 'MsgXICantTorture', '\c5Torture: Cannot torture %1, they are higher level than you.', %client.nameBase);
            else
            {
               if(isObject(%client.player))
               {
                    %client.player.setInvincible(false);
                    BTSelfDestruct(%client.player);
                    messageClient(%cl, 'MsgXITorture', '\c5Torture: Applied retribution to %1.', %client.nameBase);                    
               }
               else
                    messageClient(%cl, 'MsgXITortureNoPlayer', '\c5Torture: %1 has not spawned.', %client.nameBase);               
            }
         }
      }
      else
         messageClient(%cl, 'MsgXITortureNoName', '\c5Torture: No name specified.');
}

Xi.addCommand($XI::Admin, "combust", "Transform a player into an explosion (partial names accepted) - ex. /combust noob");

