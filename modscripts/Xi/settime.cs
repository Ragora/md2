//-----------------------------------------------------------------------------
// Xi Command System
// Set Time Limit

function Xi::settime(%this, %cl, %val)
{
     %val = firstWord(%val);
     
     if(%val < 15)
          %val = 15;

     if(%val > 999)
          %val = 999;
     
     // Prevent setting time as a word
     if(%val == 0)
          return;
     
     if($matchStarted)
     {
          $Host::TimeLimit = %val;
          
          //schedule the end of match countdown
          %elapsedTimeMS = getSimTime() - $missionStartTime;
          %curTimeLeftMS = ($Host::TimeLimit * 60 * 1000) - %elapsedTimeMS;
          CancelEndCountdown();
          EndCountdown(%curTimeLeftMS);
          cancel(Game.timeSync);
          Game.checkTimeLimit(true);
          messageClient(%cl, 'MsgXITimeInc', '\c3Xi[Server]: Time limit set to %1 minutes.', %val);          
     }
     else
          messageClient(%cl, 'MsgXITimeError', '\c3Xi[Server]: Cannot set time inbetween missions.');
}

Xi.addCommand($XI::SuperAdmin, "settime", "Sets current match time (in minutes) - ex. /settime 300");

