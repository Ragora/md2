//-----------------------------------------------------------------------------
// Xi Command System
// Set Model

function Xi::setmodel(%this, %cl, %val)
{
     %val = firstWord(%val);
     
     if(%val !$= "")
     {
          if(%val $= "male")
          {
               %cl.race = "Human";
               %cl.sex = "Male";
          }
          else if(%val $= "female")
          {
               %cl.race = "Human";
               %cl.sex = "Female";
          }
          else if(%val $= "bioderm")
          {
               %cl.race = "Bioderm";
               %cl.sex = "Male";
          }
          else
          {
               messageClient(%cl, 'MsgXIError3', '\c5Xi[SexChanger]: %1 is not a valid type. Valid types are Male, Female, and Bioderm.', %val);
               return;
          }

          if(isObject(%cl.player) && !%cl.player.getDataBlock().isTacticalMech) // disalow "changing sex while dead" bug AND with tacmech
          {
               %dmg = %cl.player.getDamageLevel();
               %nrg = %cl.player.getEnergyLevel();
               %rate = %cl.player.getRechargeRate();
               %cl.player.setArmor(%cl.armor);
               %cl.player.setDamageLevel(%dmg);
               %cl.player.setRechargeRate(%rate);
               %cl.player.setEnergyLevel(%nrg);
          }

          messageClient(%cl, 'MsgXIError4', '\c3Xi[SexChanger] -> playerTransform(%1).', %val);
     }
     else
     {
          messageClient(%cl, 'MsgXIError5', '\c5Xi[SexChanger-Error]: No type specified.');
          return;
     }
}

Xi.addCommand($XI::Player, "setmodel", "Allows you to set your display model - ex. /setmodel Bioderm");

