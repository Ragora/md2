//-----------------------------------------------------------------------------
// Xi Command System
// Floor

function Xi::floor(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
                 messageClient(%cl, 'MsgXICantTorture', '\c5Torture: Cannot torture %1, they are higher level than you.', %client.nameBase);
            else
            {
               if(isObject(%client.player))
               {
                    %client.player.setInvincible(false);
                    %client.player.setPosition(vectorAdd(%client.player.getPosition(), "0 0 0.1"));
                    %client.player.setVelocity("0 0 -1000");
                    messageClient(%cl, 'MsgXITorture', '\c5Torture: Applied retribution to %1.', %client.nameBase);                    
               }
               else
                    messageClient(%cl, 'MsgXITortureNoPlayer', '\c5Torture: %1 has not spawned.', %client.nameBase);               
            }
         }
      }
      else
         messageClient(%cl, 'MsgXITortureNoName', '\c5Torture: No name specified.');
}

Xi.addCommand($XI::Admin, "floor", "Force player into the nearest solid surface (partial names accepted) - ex. /floor noob");

