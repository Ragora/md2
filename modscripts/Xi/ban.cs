//-----------------------------------------------------------------------------
// Xi Command System
// Ban player

function Xi::ban(%this, %cl, %val)
{
      if(%val !$= "")
      {
         %client = nameToClient(firstWord(%val));

         if(%client)
         {
            if(!canTorture(%cl, %client))
            {
                 messageClient(%cl, 'MsgXIBanTooHigh', '\c5Kick: Cannot kick %1, they are higher level than you.', %client.nameBase);
                 return 1;
            }
            else
               ban(%client, %cl);
         }
      }
      else
         messageClient(%cl, 'MsgXIBanNoName', '\c5Ban: No name specified.');
}

Xi.addCommand($XI::SuperAdmin, "ban", "Bans the player for a set amount of time (partial names accepted) - ex. /ban tard");
