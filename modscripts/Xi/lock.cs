//-----------------------------------------------------------------------------
// Xi Command System
// Whisper (Private Chat) system - Lock

function Xi::lock(%this, %cl, %val)
{
     %name = nameToClient(%val);
     
     if(%name)
     {
          %cl.lockedClient = %name;
          EngineBase.logManager.logToFile("WhisperLog", timestamp() SPC "[Whisper]" SPC %cl.namebase SPC "locked" SPC %cl.lockedClient.namebase);
        
          messageClient(%cl, 'MsgWhisperLock', '\c3Whisper: Set private chat target to %1, use /w to talk to this person and /unlock to unlock the person.', %name.name);
     }
     else
          messageClient(%cl, 'MsgWhisper404', '\c5Whisper: This name does not exist on the server.');
}

Xi.addCommand($XI::Player, "lock", "Set player name as the target of your whisper - ex. /lock dude");

