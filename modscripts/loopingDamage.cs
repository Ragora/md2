if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//================ EMP / Fire / Burn code
// Loops in 100ms chunks
// Similar (was going to do an all-in-one) but they have their own unique features.

$StatusEffect::None = 1 << 0;
$StatusEffect::EMP = 1 << 1;
$StatusEffect::Burn = 1 << 2;
$StatusEffect::Poison = 1 << 3;

$StatusEffect::Disabled = 1 << 29;
$StatusEffect::Dead = 1 << 30;
$StatusEffect::BlownUp = 1 << 31;

$StatusEffectSystem::TickMS = 100;

$SESBurnDamageTick = 0.01;
$SESPoisonCoefficient = 0.004;

datablock AudioProfile(IncenBurningSound) //for burning Death~bot
{
   filename = "fx/environment/icecrack2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock ParticleData(EMPSparksParticle)
{
    dragCoefficient = 0.6;
    gravityCoefficient = 0;
    windCoefficient = 0;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 650;
    lifetimeVarianceMS = 325;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.100000 0.100000 1.000000 1.000000";
    colors[1] = "0.050000 0.050000 1.000000 1.000000";
    colors[2] = "0.000000 0.000000 1.000000 0.000000";
    sizes[0] = 2.31452;
    sizes[1] = 0.25;
    sizes[2] = 0.25;
};

datablock ParticleEmitterData(EMPSparksEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 0;
    ejectionVelocity = 18;
    velocityVariance = 6.75;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    lifeTimeMS = 200;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "EMPSparksParticle";
};

function ShapeBase::EMPObject(%obj)
{
   EMPObject(%obj);
}

function ShapeBase::BurnObject(%obj, %attacker , %amount )	// %amount +soph
{
   BurnObject(%obj, %attacker , %amount );			// %amount +soph
}

function ShapeBase::PoisonObject(%obj, %attacker , %amount )	// %amount +soph
{
   PoisonObject(%obj, %attacker , %amount );			// %amount +soph
}

function ShapeBase::clearLoopingDamages(%obj)
{
   clearLoopingDamages(%obj);
}

function enterWaterClearIncen(%obj)
{
	if(isObject(%obj))
		if(%obj.isFlaming)
			%obj.burnTime = 1;
}

function EMPObject( %obj , %attacker , %amt )
{
	// DarkDragonDX: Don't EMP Dead People
	if( !isObject( %obj ) || ( %obj.pdead && $votePracticeModeOn ) )
		return;
	if( isObject( %attacker ) && ( %obj.team == %attacker.team ) && ( %obj != %attacker ) )
		return;
	if( %obj.isSyn )							// +[soph]
		%amt /= 2 ;							// + removing random factors and insta-shock
	if( %obj.magneticClamp || %obj.archMageMod )				// +
		%amt /= 2 ;							// +
										// +
	if( isObject( %obj ) )							// +
		if( %amt > 0 )							// +
		{								// +
			%type = %obj.getType() ;				// +
			if( %type & $TypeMasks::VehicleObjectType )		// +
			{							// +
				%amt *= 2.0 ;					// + 200% effect for vehicle targets				
				if( isObject( %obj.shieldCap ) )		// +
				{						// +
					%nrg = %obj.shieldCap.getCapacitorLevel() ;	
					if( %amt > %nrg )			// + hits shield capacitor first
					{					// +
						%obj.shieldCap.setCapacitorLevel( 0 ) ;
						%amt -= %nrg / 2 ;		// + bleedthrough gets a buff
						%nrg = %obj.getEnergyLevel() ;	// +
						if( %amt > %nrg )		// + hits main energy pool next
							if( %obj.isSyn )
								%obj.setEnergyLevel( 0 ) ;
							else
								lDamageObject( %obj , %attacker , $StatusEffect::EMP , %nrg / %amt ) ;
						else				// +
							%obj.setEnergyLevel( %nrg - %amt ) ;
					}					// +
					else					// +
						%obj.shieldCap.setCapacitorLevel( %nrg - %amt ) ;
				}						// +
				else						// +
				{						// +
					%nrg = %obj.getEnergyLevel() ;		// +
					if( %amt > %nrg )			// +
                                                if( %obj.isSyn )
							%obj.setEnergyLevel( 0 ) ;
						else
							lDamageObject( %obj , %attacker , $StatusEffect::EMP , %nrg / %amt ) ;
					else
						%obj.setEnergyLevel( %nrg - %amt ) ;
				}
			}
			else
			{
				if( %obj.getDataBlock().isBlastech )
					%amt *= 10.0;
				else 
				{
					if( %obj.isShielded || %obj.getDatablock().isShielded )
						%amt *= 1.25;

					%test = ( $TypeMasks::StationObjectType		| 
						  $TypeMasks::GeneratorObjectType	| 
						  $TypeMasks::SensorObjectType		| 
						  $TypeMasks::TurretObjectType		| 
						  $TypeMasks::StaticShapeObjectType ) ;

					if( %obj.inDEDField && %type & %test )
						%amt /= 2.25;
				}
				%nrg = %obj.getEnergyLevel();
				if( %amt > %nrg )
					if( %obj.isSyn || %obj.archMageMod )
						%obj.setEnergyLevel( 0 ) ;
					else
						lDamageObject( %obj , %attacker , $StatusEffect::EMP , %nrg / %amt ) ;
				else
					%obj.setEnergyLevel( %nrg - %amt ) ;
			}
		}
}

function BurnObject( %obj, %attacker , %amt )						// %value +soph
{
	// DarkDragonDX: Don't burn Dead People
	if( %obj.burnproof || %obj.statusImmunityTime > getSimTime() || %obj.archMageMod || %obj.isMounted() || (%obj.pdead && $votePracticeModeOn))	// removed %obj.isShielded || ... || %obj.isTacticalMech -soph
		return;

	if( %obj.isShielded && %obj.getEnergyLevel() > 0 )				// +[soph]
		return ;								// +
											// +
	if( !$teamDamage && %obj.team == %attacker.team && %obj != %attacker )		// +
		return ;								// +
											// +
	if( %obj.isTacticalMech )							// +
	{										// +
		MechAddHeat( %obj , %amt * 2.5 ) ;					// +
		if( %obj != %attacker )							// +
			%obj.LD_Brn_SrcObj = %attacker ;				// +
		return ;								// +
	}										// +[/soph]

	if(%obj.isGenuinePlayer && %amt > 0 )
	{
		if( %obj.magneticClamp )
			%amt /= 2 ;
		%time = getSimTime() ;							// +[soph]
		if( !%obj.lastBurnTime )						// +
		%obj.lastBurnAmt = 0 ;							// +
		else									// +
		{									// +
			%obj.lastBurnAmt -= %obj.getDataBlock().heatDecayPerSec * ( ( %time - %obj.lastBurnTime ) / 1000 ) ;
			if(%obj.lastBurnAmt < 0 )					// +
				%obj.lastBurnAmt = 0 ;					// +
		}									// +
		%obj.lastBurnAmt += %amt ;						// +
		%obj.lastBurnTime = %time ;						// +
		if( ( %obj.lastBurnAmt + %obj.getHeat() ) >= 2 )			// +[/soph]
			lDamageObject(%obj, %attacker, $StatusEffect::Burn);
	}
}

function PoisonObject(%obj, %attacker , %amt )					// %amt +soph
{
	// DarkDragonDX: Don't Poison Dead People
	if(%obj.isShielded || %obj.statusImmunityTime > getSimTime() || %obj.isTacticalMech || %obj.archMageMod || (%obj.pdead && $votePracticeModeOn))
		return;

	if(%obj.isGenuinePlayer)
		if( !%obj.isMounted() || %obj.getObjectMount().team != %obj.team )	// poisoned vehicle bugfix +soph
		{									// +[soph]
			if( %obj.magneticClamp )					// +
				%amt /= 2 ;						// +[/soph]	
			lDamageObject(%obj, %attacker, $StatusEffect::Poison , %amt );	// %amt +soph
		}
}

function clearLoopingDamages(%obj)
{
   %obj.statusFlags = 0 ;		// ^= $StatusEffect::EMP | $StatusEffect::Burn | $StatusEffect::Poison; -soph
   %obj.isEMP = false;
   %obj.EMPTime = 1;
   %obj.isFlaming = false;
   %obj.burnTime = 1;
   %obj.burnBuildup = 0;		// +soph
   %obj.lDamageStack[Poison] = 0 ;	// = 1; -soph
   %obj.lDamageStack[Fire] = 1;
}

function MeltdownBase::loopDamageObject(%mod, %obj)
{
   cancel( %obj.damageLoop ) ;
   if(isObject(%obj) && !%obj.blowedUp)	
   {
      %time = getSimTime();
      %break = true;

      if(%obj.isEMP)
      {
         %break = false;
         
         if(%obj.EMPTime > %time)
         {
            %obj.setEnergyLevel(0);

            if(isObject(%obj.shieldCap))
               %obj.shieldCap.setCapacitorLevel(0);            
            
            createLifeEmitter(%obj.getWorldBoxCenter(), "EMPSparksEmitter", $StatusEffectSystem::TickMS);
         }
         else
            Meltdown.finishDamageLoop(%obj, $StatusEffect::EMP);
      }
      else if(%obj.EMPTime > 0)
           Meltdown.finishDamageLoop(%obj, $StatusEffect::EMP);
           
      if( %obj.isFlaming || %obj.isTacticalMech )	// (%obj.isFlaming) -soph
      {
         if(%obj.isWet)
            Meltdown.finishDamageLoop(%obj, $StatusEffect::Burn);

         %break = false;
         
         if(%obj.burnTime > %time)
         {
            if( !isObject( %obj.LD_Brn_SrcObj ) )		// +[soph]
               %obj.LD_Brn_SrcObj = %obj ;			// +
            %obj.damage( %obj.LD_Brn_SrcObj , %obj.getWorldBoxCenter(), $SESBurnDamageTick*%obj.lDamageStack[Fire], $DamageType::BurnLoop);
            if( %obj.getDamageFlash() < 0.125 )			// +
                %obj.setDamageFlash(0.125);			// +[/soph]
            createLifeEmitter(vectorAdd(%obj.getWorldBoxCenter(), "0 0 -0.5"), "StarburstFlameEmitter", 32 ) ;
            createLifeEmitter(vectorAdd(%obj.getWorldBoxCenter(), "0 0 -0.5"), "HeavyDamageSmoke", 32 ) ;

            %obj.incenPlayCount++; // MrKeen
            if(%obj.incenPlayCount > 2)
            {
                %obj.setHeat( 1 ) ;
                %obj.play3D("IncenBurningSound");
                %obj.incenPlayCount = 0;
            }
         }
         else
            Meltdown.finishDamageLoop(%obj, $StatusEffect::Burn);
      }
      else if(%obj.burnTime > 0)
           Meltdown.finishDamageLoop(%obj, $StatusEffect::Burn);

      if( %obj.isPoisoned && !%obj.isTacticalMech )		// (%obj.isPoisoned) -soph)
      {
         %break = false;
         
         if( %obj.poisonTime > 0 )
         {
               if( !isObject( %obj.LD_Psn_SrcClnt ) )		// +[soph]
                  %obj.LD_Psn_SrcClnt = %obj.client ;		// +
               if( !isObject( %obj.LD_Psn_SrcClnt.player ) )	// +
                  %source = %obj ;				// +
               else						// +
                  %source = %obj.LD_Psn_SrcClnt.player ;	// +
               %flash = mSqRt( %obj.lDamageStack[ Poison ] ) * 0.075 ;	
               if( %obj.getDamageFlash() < %flash )		// +
                  %obj.setDamageFlash( %flash ) ;		// +
               %obj.damage( %source , %obj.getWorldBoxCenter() , $SESPoisonCoefficient * %obj.getMaxDamage() * %obj.lDamageStack[ Poison ] , $DamageType::PoisonLoop ) ;
               %obj.lDamageStack[ Poison ] -= %obj.lDamageStack[ Poison ] * $StatusEffectSystem::TickMS / %obj.poisonTime ;
               %obj.poisonTime -= $StatusEffectSystem::TickMS ;	// +
               %severity = poisonRankStepdown( %obj.lDamageStack[ Poison ] ) ;
               if( %severity < %obj.isPoisoned - 2 )		// +
                   %obj.isPoisoned = %severity + 2 ;		// +
								// +[/soph]
         }
         else
            Meltdown.finishDamageLoop(%obj, $StatusEffect::Poison);
      }
      else if(%obj.poisonTime > 0)
           Meltdown.finishDamageLoop(%obj, $StatusEffect::Poison);
      
      if(!%break)
           %obj.damageLoop = Meltdown.schedule($StatusEffectSystem::TickMS, loopDamageObject, %obj);
   }
   else
   {
      %obj.statusFlags = 0;
      %obj.isEMP = false;
      %obj.EMPTime = 1;
      %obj.isFlaming = false;
      %obj.burnTime = 1;
      %obj.isPoisoned = 0 ;		// false ;-soph
      %obj.poisonTime = 1;
      %obj.lDamageStack[ Poison ] = 0 ;	// 1; -soph
      %obj.lDamageStack[Fire] = 1;
   }
}

function MeltdownBase::finishDamageLoop(%mod, %obj, %type)
{
     switch(%type)
     {
          case $StatusEffect::EMP:
               if(%obj.isEMP)
               {
                    %obj.isEMP = false;
                    %obj.EMPTime = 0;
                    %obj.statusFlags ^= $StatusEffect::EMP;          
                    
                    if(%obj.getType() & $TypeMasks::PlayerObjectType)
                         messageClient(%obj.client, 'MsgStopEMPDamage', '\c2Armor Reactor back on-line.');
               }

          case $StatusEffect::Burn:     
               if(%obj.isFlaming)
               {
                    %obj.setHeat( 1 ) ;
                    %obj.isFlaming = false;
                    %obj.burnTime = 0;
                    %obj.lDamageStack[Fire] = 1;
                    %obj.statusFlags ^= $StatusEffect::Burn;
                              
                    messageClient(%obj.client, 'MsgStopFireDamage', '\c2You have stopped burning.');
               }

          case $StatusEffect::Poison:
               if(%obj.isPoisoned)
               {
                    %obj.isPoisoned = 0 ;										// false; -soph
                    %obj.poisonTime = 0;
                    %obj.statusFlags ^= $StatusEffect::Poison;

                    if( %obj.lDamageStack[ Poison ] > 0.2 )								// +[soph]
                         messageClient( %obj.client , 'MsgStopPoisonDamage' , '\c2The poison has worn off.' ) ;		// +
                    %obj.lDamageStack[ Poison ] = 0 ;									// +[/soph]
               }
        
          default:
               return;
     }
}

function lDamageObject(%obj, %attacker, %statusMask , %amount )		// %amount +soph
{
   // DarkDragonDX: If we're "Dead", kill the loop
   if (%obj.pdead && $votePracticeModeOn)
	return;

   if(isObject(%obj))
   {
      %player = %obj.getType() & $TypeMasks::PlayerObjectType;
      %time = getSimTime();
      
      if(%attacker $= "")
         %attacker = %obj;
      else if( %obj != %attacker && %obj.team == %attacker.team && %obj.client )	// +[soph]
      {											// +
         %type = %attacker.getType() ;							// +
         if( %type & $TypeMasks::TurretObjectType )					// +
         {										// +
            %offender = %sourceObject.getControllingClient() ;				// +
            if( isObject( %offender ) )							// +
               %name = %offender.name ;							// +
            else 									// +
               %name = "a friendly turret" ;						// +
         }										// +
         else										// +
         {										// +
            %offender = %attacker.client ;						// +
            if( isObject( %offender ) )							// +
               %name = %offender.name ;							// +
            else									// +
               %name = "friendly fire" ;						// +
         }										// +
         switch(%statusMask)								// +
         {										// +
            case $StatusEffect::EMP :							// +
               %type = "shorted out" ;							// +
            case $StatusEffect::Burn :							// +
               %type = "burned" ;							// +
            case $StatusEffect::Poison :						// +
               %type = "poisoned" ;							// +
         }										// +
         messageClient( %obj.client , 'MsgDamagedByTeam' , '\c1You were %1 by %2.' , %type , %name ) ;
         if( %offender )								// +
            messageClient( %offender , 'MsgDamagedTeam' , '\c1You just %1 teammate %2.' , %type , %obj.client.name ) ;
      }											// +[/soph]
      
      if(isObject(%attacker))
      {
         if(!$teamdamage && (%obj != %attacker) && (%attacker.client.team == %obj.client.team))
             return;
      }
      
      switch(%statusMask)
      {
          case $StatusEffect::EMP:
              if(%obj.isEMP)
                 return;
     
              if(%player)
                 messageClient(%obj.client, 'MsgGotEMPDamage', '\c2Danger! EMP detected; Armor Power Core Offine.');
     
              %obj.setEnergyLevel(0);
              %obj.isEMP = true;
              %obj.statusFlags |= $StatusEffect::EMP;
              %obj.EMPTime = %time + 7500 + ( %amount * 2500 ) ;	// -soph

          case $StatusEffect::Burn:
               
               %time += %obj.lastBurnAmt * 3000;			// +[soph]
               if ( %obj.isFlaming )					// +
               {							// +
                  if ( %time > %obj.burnTime )				// +
                     %obj.burnTime = %time;				// +
                  return;						// +
               }							// +[/soph]
               messageClient(%obj.client, 'MsgGotBurnDamage', '\c2Danger! You are on fire!');

               %obj.isFlaming = true;
               %obj.statusFlags |= $StatusEffect::Burn;
               %obj.lDamageStack[Fire] = 1;
               %obj.burnTime = %time;					// +soph
               %obj.LD_Brn_SrcObj = %attacker ;				// +soph

          case $StatusEffect::Poison:
               %factor = %amount * 2.5 ;				// +[soph]
               if( %factor > 3 )					// +
                    %factor = 3 ;					// +[/soph]
               if(%obj.isPoisoned)
               {							// +[soph]
                    %obj.PoisonTime += 40000 * %factor ; 		// +
                    %obj.lDamageStack[ Poison ] += %factor * ( 1 - %obj.lDamageStack[ Poison ] / 3 ) ;
                    %obj.LD_Psn_SrcClnt = %attacker.client ;		// +
                    %severity = poisonRankStepdown( %obj.lDamageStack[ Poison ] ) ;
                    %old = %obj.isPoisoned - 1 ;			// +
                    if( %severity > %old )				// +
                    {							// +
                         %obj.isPoisoned = %severity + 1 ;		// +
                         switch( %severity )				// +
                         {						// +
                              case 1 :					// +
                                   %severity = "lightly" ;		// + 
                                   messageClient( %obj.client , 'MsgGotPoisonDamage' , '\c2You are \c5%1 \c2poisoned!' , %severity ) ;
                                   %severity = "" ;			// +
                              case 3 :					// +
                                   %severity = "severe" ;		// +
                              case 4 :					// +
                                   %severity = "critical" ;		// +
                              case 5 :					// +
                                   %severity = "catastrophic" ;		// +
                              default :					// +
                                   %severity = "" ;			// + 
                         }						// +
                         if( %severity !$= "" )				// +
                              messageClient( %obj.client , 'MsgGotPoisonDamage' , '\c2Your poison condition has become \c5%1!' , %severity ) ;
                    }							// +
                    return ;						// +
               }							// +
               else							// +
               {							// +[/soph]
                    %obj.statusFlags |= $StatusEffect::Poison;               
                    %obj.lDamageStack[Poison] = %factor ;		// = 1; -soph
                    %obj.PoisonTime = %factor * 40000 ;			// +[soph]
                    %obj.LD_Psn_SrcClnt = %attacker.client ;		// +
                    %severity = mFloor( ( %obj.lDamageStack[Poison] + 0.2 ) * 10 / 3 ) ;
		    %obj.isPoisoned = %severity + 1 ;			// +
                    switch( %severity )					// +
                    {							// +
                         case 0 :					// +
                              %severity = "" ;				// + 0.0 - 0.2
                         case 1 :					// +
                              %severity = "lightly" ;			// + 0.2 - 0.5
                         case 2 :					// +
                              %severity = "moderately" ;		// + 0.5 - 0.8
                         case 3 :					// +
                              %severity = "severely" ;			// + 0.8 - 1.1
                         case 4 :					// +
                              %severity = "critically" ;		// + 1.1 - 1.4
                         default :					// +
                              %severity = "catastrophically" ;		// + 1.4 - 2.0
                    }							// +
                    if( %severity !$= "" )
                         messageClient( %obj.client , 'MsgGotPoisonDamage' , '\c2You have been \c5%1 \c2poisoned!' , %severity ) ;
               }							// +[/soph]

          default:
               return;
      }

      %obj.damageLoop = Meltdown.loopDamageObject(%obj);
   }
}

function poisonRankStepdown( %raw )
{
	return mFloor( ( %raw + 0.2 ) * 10 / 3 ) ;
}
