//-----------------------------------------------------------------------------
// Admin Access Attempt Logger
//-----------------------------------------------------------------------------

function AdminLog::onWriteLine(%self, %line)
{
    error(%line);
}

EngineBase.logManager.addLogger("AdminLog");
