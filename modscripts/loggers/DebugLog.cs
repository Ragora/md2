//-----------------------------------------------------------------------------
// Debugging Logger
//-----------------------------------------------------------------------------

function DebugLog::onWriteLine(%self, %line)
{
    error(timestamp(1) SPC %line);
}

EngineBase.logManager.addLogger("DebugLog");
