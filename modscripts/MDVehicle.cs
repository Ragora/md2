if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Meltdown2 Vehicles

function vSlotAutoRepair(%obj, %node)
{
   if(isObject(%obj))
   {
      if((%pl = %obj.getMountNodeObject(%node)))
      {
         if(%pl.getDamageLevel())
            %pl.takeDamage(-0.002);

         schedule(250, 0, vSlotAutoRepair, %obj, %node);
      }
   }
}

function VehicleData::mountDriver(%data, %obj, %player)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed")
   {
      %player.startFade(1000, 0, true);
      schedule(1000, 0, "testVehicleForMount", %player, %obj);
      %player.schedule(1500,"startFade",1000, 0, false);
   }
}

function VehicleData::updateAmmoCount(%data, %obj, %client, %ammo)
{
     %ammoCount = %obj.getInventory(%ammo);
     commandToClient(%client, 'setAmmoHudCount', %ammoCount);
}

function testVehicleForMount(%player, %obj)
{
   // z0dd - ZOD, 4/25/02. Do not auto mount players who are teleporting, bug fix.
   // z0dd - ZOD, 7/10/02. Added check to see if player is in inv. Prevents switching
   // to illegal armor for a vehicle by purchasing a vehicle and buying a illegal
   // armor durig the tele phase period.
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed" && !%player.teleporting && !%player.client.inInv)
      %player.getDataBlock().onCollision(%player, %obj, 0);
}

function vehicleInfoBar(%obj)
{
     if(isObject(%obj))
     {
//          %count = 0;
          
//          for(%i = 0; %i < 6; %i++) //for all 6 possible player mount points
//          {
//               %slot = %obj.getMountNodeObject(%i);
//
//               if(%slot && %slot.getType() & $TypeMasks::PlayerObjectType) // && %slot.client.allowVehicleHud)
//               {
//                    if(%slot.controllingTurret)
//                    %vehicleBarActive[%count] = %slot;
//                    %count++;
//               }
//          }
          
          %pilot = %obj.getMountNodeObject(0);
          %updateTime = 333;
          
          if(isObject(%pilot))
          {
               %vehicleData = %obj.getDatablock();
               %vehicleData.generateUsageStatistics(%obj);

               if(%pilot.client.hasMD2Client)
                    %updateTime = 250;
//               for(%i = 0; %i < %count; %i++)
//               {
//                    %client = %vehicleBarActive[%i].client;
                    %vehicleData.displayVehicleHud(%obj, %pilot.client, 1);
//               }
          }

          schedule(%updateTime, %obj, VehicleInfoBar, %obj);
     }
}

function VehicleData::generateUsageStatistics(%data, %obj)
{
     %obj.ib_hp_pct = %obj.getDamageLeftPct();
//     %obj.ib_hp_cur = mFloor(%obj.getDamageLeftPct()*%data.maxDamage*100);
    // %obj.ib_nrg_pct = isObject(%obj.shieldCap) ? (%obj.shieldCap.getCapacitorLevel() / %obj.shieldCap.getDatablock().maxCapacitorEnergy) : 0; //%obj.getEnergyPct();	// -soph

     if( isObject( %obj.shieldCap ) )				// +[soph]
        %obj.ib_nrg_pct = %obj.shieldCap.getCapacitorLevel() / %obj.shieldCap.getDatablock().maxCapacitorEnergy;
     else
        %obj.ib_nrg_pct = %obj.shieldStrengthFactor > 0 ? %obj.getEnergyPct() : 0;	// +[/soph]

//     %obj.ib_nrg_cur = mFloor(%obj.getEnergyPct()*%data.maxEnergy);
//     %obj.ib_vel_kph = msToKPH(vectorLen(%obj.getVelocity()));

//     %obj.ib_hp_graph = createColorGraph("|", 22, %obj.ib_hp_pct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mFloor(%obj.ib_hp_pct * 100); 
//     %obj.ib_sh_graph = createColorGraph("|", 22, %obj.ib_nrg_pct, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mFloor(%obj.ib_nrg_pct * 100);
     %obj.ib_hp_graph = mFloor(%obj.ib_hp_pct * 100); 
     %obj.ib_sh_graph = mFloor(%obj.ib_nrg_pct * 100);
}

function VehicleData::displayVehicleHud(%data, %obj, %client, %time)
{
//     if(%client.player.ctrlVT)
//          %weapon = %client.player.ctrlVT.weapon[%client.player.ctrlVT.selectedWeapon, Name];
//     else
     %weapon = %obj.weapon[%obj.selectedWeapon, Name];
          
     if(%weapon $= "")
          %weapon = "None";

//     %module = %obj.moduleInstalled ? %obj.moduleName : "N/A";

     if(%client.hasMD2Client)
          commandToClient(%client, 'GraphVehHUD', %obj.ib_hp_pct, %obj.ib_nrg_pct, %weapon);
     else
          commandToClient(%client, 'BottomPrint', "Hull: "@%obj.ib_hp_graph@"% | Shield: "@%obj.ib_sh_graph@"% | Current Weapon: "@%weapon, %time*2, 1);
}

//function VehicleData::getHudNum()
//{
     // thou shall not spam
//}

// ---------------------------------------------------------
// z0dd - ZOD, 6/18/02. Get the name of the vehicle node and 
// pass to Armor::onMount and Armor::onDismount in playerScripts.cs
function findNodeName(%vehicle, %node)
{
   %vName = %vehicle.getDataBlock().getName();
   if(%vName !$= "HAPCFlyer")
   {
      if(%node == 0)
         return 'pilot';
      else if(%node == 1)
         return 'gunner';
      else
         return 'tailgunner';
   }
   else
   {
      if(%node == 0)
         return 'pilot';
      else if(%node == 1)
         return 'tailgunner';
      else
         return 'passenger';
   }
}
// End z0dd - ZOD
// ---------------------------------------------------------

function VehicleData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);
   
   if((%data.sensorData !$= "") && (%obj.getTarget() != -1))
      setTargetSensorData(%obj.getTarget(), %data.sensorData);
   %obj.setRechargeRate(%data.rechargeRate);
   // set full energy
   %obj.setEnergyLevel(%data.MaxEnergy);

   if(%obj.disableMove)
      %obj.immobilized = true;
   if(%obj.deployed)
   {
      if($countDownStarted)
         %data.schedule(($Host::WarmupTime * 1000) / 2, "vehicleDeploy", %obj, 0, 1);
      else
      {
         $VehiclesDeploy[$NumVehiclesDeploy] = %obj;
         $NumVehiclesDeploy++;
      }
   }
   if(%obj.mountable || %obj.mountable $= "")
      %data.isMountable(%obj, true);
   else
      %data.isMountable(%obj, false);
   
   %obj.setSelfPowered();
//   %data.canObserve = true;

   defineVehicleConstants(%obj);
}

function VehicleData::onRemove(%this, %obj)
{
   // if there are passengers/driver, kick them out
   %this.deleteAllMounted(%obj);

   // DarkDragonDX: Fix for Practice Mode Ghosts
   %obj.clearAllVehicleHuds();

   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
      if (%obj.getMountNodeObject(%i)) {
         %passenger = %obj.getMountNodeObject(%i);
         %passenger.unmount();
      }
   vehicleListRemove(%obj.getDataBlock(), %obj);
   if(%obj.lastPilot.lastVehicle == %obj)
      %obj.lastPilot.lastVehicle = "";      

   %shieldCap = %obj.getMountNodeObject(9);
   
   if(isObject(%shieldCap))
     %shieldCap.delete();

   if(isObject(%obj.shieldCap))
     %obj.shieldCap.delete();

   Parent::onRemove(%this, %obj);
}

// OnAdd Defs

function ScoutVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
//   %obj.mountImage(TurbocatShocklance, 2);

//   %obj.weapon[1, Display] = true;
//   %obj.weapon[1, Name] = "Shocklance";
//   %obj.weapon[1, Description] = "Eletrify your enemies with a 2x range shocklance.";

   %obj.nextWeaponFire = 1;

   %obj.schedule(6000, "playShieldEffect", "0.0 0.0 1.0");
   %obj.schedule(6300, "playThread", $ActivateThread, "activate");

   %obj.veh_description = "Standard wildcat. Comes with a nose-mounted Shocklance.";
   
   vehicleInfoBar(%obj);
}

function LOSDownFlyer::deleteAllMounted(%data, %obj)
{
   %turret = %obj.getMountNodeObject(10);
   if(!%turret)
      return;

   %turret.altTrigger = 0;
   %turret.fireTrigger = 0;

   if(%client = %turret.getControllingClient())
   {
      %client.player.setControlObject(%client.player);
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
      %client.player.mountVehicle = false;

      %client.player.bomber = false;
      %client.player.isBomber = false;
   }
   %turret.delete();
}

function ScoutFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

//   %obj.mountImage(ScoutChaingunZParam, 0);
//   %obj.mountImage(SpikeCannonImage, 2);
//   %obj.mountImage(SpikeCannonPairImage, 3);
//   %obj.mountImage(ScoutChaingunParam, 4);
//   %obj.mountImage(ScoutChaingunImage, 5);
//   %obj.mountImage(ScoutChaingunPairImage, 6);

//   %obj.nextWeaponFire = 5;
//   %obj.selectedWeapon = 2;

//   %obj.weapon[1, Display] = true;
//   %obj.weapon[1, Name] = "Spike Cannons";
//   %obj.weapon[1, Description] = "Modified Landspike turret barrel for more speed and punch.";

//   %obj.weapon[2, Display] = true;
//   %obj.weapon[2, Name] = "Shrike Blasters";
//   %obj.weapon[2, Description] = "The mainstay blasters that have a reputation for pain.";

   %obj.schedule(5400, "playShieldEffect", "0.0 0.0 1.0");
   %obj.schedule(3750, "playThread", $ActivateThread, "activate");
//   $numVWeapons = 2;

   %turret = TurretData::create(ShrikeShieldCap);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 9);
   %turret.setCapacitorRechargeRate(%turret.getDataBlock().capacitorRechargeRate);
   %turret.mountImage(AssaultTurretParam, 0);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %obj.shieldCap = %turret;
   %turret.setHeat(0.0);
   
   %obj.veh_description = "Standard Interceptor. The vehicle legends are made from.";

   vehicleInfoBar(%obj);
}

function ScoutFlyer::deleteAllMounted(%data, %obj)
{
   %turret = %obj.getMountNodeObject(10);
   if(!%turret)
      return;

   %turret.altTrigger = 0;
   %turret.fireTrigger = 0;

   if(%client = %turret.getControllingClient())
   {
      %client.player.setControlObject(%client.player);
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
      %client.player.mountVehicle = false;

      %client.player.bomber = false;
      %client.player.isBomber = false;
   }
   %turret.delete();
   
   Parent::deleteAllMounted(%data, %obj);
}

function LOSDownFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %turret = TurretData::create(LOSDownTurret);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
//   %turret.mountImage(LOSDownTeleporterImage, 2);	// -soph
   %turret.mountImage( NightshadeSkyhookImage , 2) ;	// +soph
   %turret.mountImage(LOSDownRMSImage, 4);
   %turret.mountImage(LOSDownRMSImage, 6);		// +soph
//   %turret.mountImage(LOSDownMANTAImage, 6);		// -soph
   %obj.turretObject = %turret;
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %turret.vehicleMounted = %obj;
   %turret.setAutoFire(false);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setHeat(0.0);
   
   schedule(2000, %turret, LOSDownSetTurretNames, %turret);

   %turret = TurretData::create(NightshadeShieldCap);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 9);
   %turret.setCapacitorRechargeRate(%turret.getDataBlock().capacitorRechargeRate);
   %turret.mountImage(AssaultTurretParam, 0);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %obj.shieldCap = %turret;
   %turret.setHeat(0.0);
   
   %obj.usesCPS = true;
   %obj.veh_description = "The gunner controls a teleporter gun that can grab a person, even a flag carrier. +10% damage/person. (CPS)";
   vehicleInfoBar(%obj);
}

function LOSDownSetTurretNames(%turret)
{
   %turret.weapon[1, Display] = true;
   %turret.weapon[1, Name] = "Skyhook" ;								// = "Teleporter Gun"; -soph
   %turret.weapon[1, Description] = "Energetic tether, lift vehicles and mechs into the air." ;		// = "Allows you to teleport a target player into the tailgunner slot."; -soph

   %turret.weapon[2, Display] = true;
   %turret.weapon[2, Name] = "RMS Targeting Laser";
   %turret.weapon[2, Description] = "Paint a target for a cruise missile barrage.";

   %turret.weapon[3, Display] = true;
   %turret.weapon[3, Name] = "MANTA Targeting Laser";
   %turret.weapon[3, Description] = "Tag a position for MANTA Ion Cannon strike." ;			// = "Paint the location where the MANTA ION Cannon will fire."; -soph
}

function HAPCFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
   %turret = TurretData::create(AnniShieldCap);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
//   %turret.setSkillLevel(2.0);
   %obj.mountObject(%turret, 9);
   %turret.setCapacitorRechargeRate(%turret.getDataBlock().capacitorRechargeRate);
   %turret.mountImage(AssaultTurretParam, 0);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %obj.shieldCap = %turret;
   %turret.setHeat(0.0);
   
   %obj.schedule(6000, "playThread", $ActivateThread, "activate");
   %obj.usesCPS = true;
   %obj.veh_description = "Large air transport vehicle. Carries up to 5 people + pilot. 10% damage bonus per person. (CPS)";
   vehicleInfoBar(%obj);
}

function AssaultVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %turret = TurretData::create(AssaultPlasmaTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
   //Needed so we can set the turret parameters..
   %turret.mountImage(AssaultTurretParam, 0);
//   %turret.mountImage(AssaultPlasmaTurretBarrel, 2);
//   %turret.mountImage(TankAABarrel1, 2);
//   %turret.mountImage(TankAABarrel2, 3);
//   %turret.mountImage(AssaultMortarTurretBarrel, 4);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %obj.turretObject = %turret;
   %turret.setHeat(0.0);
   
//   %turret.weapon[1, Display] = true;
//   %turret.weapon[1, Name] = "Tank Anti-Air Array";
//   %turret.weapon[1, Description] = "Rail down your aerial foes with this!";

//   %turret.weapon[2, Display] = true;
//   %turret.weapon[2, Name] = "Tank Comet Cannon";
//   %turret.weapon[2, Description] = "Fearsome and explosive ball of energy.";

   //vehicle turrets should not auto fire at targets
   %turret.setAutoFire(false);

   //Needed so we can set the turret parameters..
//   %turret.mountImage(AssaultTurretParam, 0);
   %obj.schedule(6000, "playThread", $ActivateThread, "activate");

   // set the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);

   %obj.mountImage(TankPilotChaingunParam, 5);
   %obj.mountImage(TankPilotChaingunImage, 6);
   %obj.mountImage( TankPilotChaingunImagePair , 7 ) ;	// -soph
   %turret.flakEnabled = true;

   %obj.selectedWeapon = 1;
   
   %obj.weapon[1, Display] = true;
   %obj.weapon[1, Name] = "Light Mitzi Cannon";
   %obj.weapon[1, Description] = "Low power, high-speed Mitzi cannon.";

   %turret = TurretData::create(TankShieldCap);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 9);
   %turret.setCapacitorRechargeRate(%turret.getDataBlock().capacitorRechargeRate);
   %turret.mountImage(AssaultTurretParam, 0);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %obj.shieldCap = %turret;
   %turret.setHeat(0.0);
   
   %obj.veh_description = "Heavy offensive assault. +125% damage per person.";
   vehicleInfoBar(%obj);
   schedule(2000, %obj, updateTankSection, %obj);   
}

function updateTankSection(%obj)
{
   %obj.weapon[1, Display] = true;
   switch( %obj.pilotCannonMode )	// +[soph]
   {
      case 0 : 
         %obj.weapon[1, Name] = "Plasma Flamer";
         %obj.weapon[1, Description] = "Short-range streaming death.";
      case 1 :
         %obj.weapon[1, Name] = "Rapid Mitzi Cannon";
         %obj.weapon[1, Description] = "Moderate-power mitzi cannons.";
      case 2 : 
         %obj.weapon[1, Name] = "EMP Blasters";
         %obj.weapon[1, Description] = "Energy-destroying bursts.";
   }					// +[/soph]
//   %obj.weapon[1, Name] = "Mitzi Light Cannon";
//   %obj.weapon[1, Description] = "Low power, high-speed Mitzi Blast cannon.";
}

function MobileBaseVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   %obj.station = "";
   %obj.turret = "";
   %obj.beacon = "";

   %obj.schedule(5000, "playThread", $AmbientThread, "ambient");
   %obj.veh_description = "This base can go anywhere and deploy.. even underwater!";
   %obj.shutdown = true;
   initMPBFieldUpdate(%obj);
   vehicleInfoBar(%obj);
}

function MobileBaseVehicle::deleteAllMounted(%data, %obj)
{
   if(isObject(%obj.station))
   {
      %obj.station.getDataBlock().onLosePowerDisabled(%obj.station);
      %obj.unmountObject(%obj.station);
      %obj.station.trigger.delete();
      %obj.station.delete();
   }
   if(isObject(%obj.turret))
   {
      %obj.turret.getDataBlock().onLosePowerDisabled(%obj.turret);
      %obj.unmountObject(%obj.turret);
      %obj.turret.delete();
   }
   if(isObject(%obj.shield))
      %obj.shield.delete();

   if(isObject(%obj.field))
      %obj.field.delete();

//   if (isObject(%obj.teleporter))
//   {
//      %obj.teleporter.setThreadDir($ActivateThread, FALSE);
//      %obj.teleporter.playThread($ActivateThread,"activate");
//      %obj.teleporter.playAudio($ActivateSound, StationTeleportDeacitvateSound);
//   }
   if(isObject(%obj.beacon))
//   {
      %obj.beacon.delete();
//   }
}
// Player Mounting Calls

function vehicleOnPlayerMounted(%data, %obj, %client)
{
   if(%obj.universalResistFactor $= "")
      defineVehicleConstants(%obj);
      
   if(!%obj.vModded)
      %data.applyVehicleMods(%obj, %client);

   if(%obj.getMountNodeObject(0) == %client.player)
   {
        %id = %obj.selectedWeapon == 1 ? %obj.primaryWeaponID : %obj.secondaryWeaponID;
        
        if($VehicleWeaponUsesAmmo[%id])
             %data.schedule(32, updateAmmoCount, %obj, %client, $VehicleWeaponAmmo[%id]);   
   }

   // Superceded by vehicle info bar
   return;

   %normalColor = "<color:42DBEA>";
   %standardColor = "<color:FFFFFF>";
   %improveColor = "<color:23EF23>";
   %lossColor = "<color:EF2322>";

   if(%data.isShielded)
   {
      if(%obj.shieldStrengthFactor > 1)
           %sr = %improveColor@((%data.maxEnergy / (%data.energyPerDamagePoint / %obj.shieldStrengthFactor)) * 100)@%normalColor;
      else if(%obj.shieldStrengthFactor == 0)
           %sr = %lossColor@"0"@%normalColor;           
      else if(%obj.shieldStrengthFactor < 1)
           %sr = %lossColor@((%data.maxEnergy / (%data.energyPerDamagePoint / %obj.shieldStrengthFactor)) * 100)@%normalColor;            
      else
           %sr = %standardColor@((%data.maxEnergy / %data.energyPerDamagePoint) * 100)@%normalColor;      
   }
   else
      %sr = %lossColor@"0"@%normalColor;
   
   if(%obj.universalResistFactor > 1)
      %hp = %lossColor@((%data.maxDamage / %obj.universalResistFactor) * 100)@%normalColor;
   else if(%obj.universalResistFactor < 1)
      %hp = %improveColor@((%data.maxDamage / %obj.universalResistFactor) * 100)@%normalColor;
   else
      %hp = %standardColor@(%data.maxDamage * 100)@%normalColor;   
      
   %w1 = %obj.weapon[1, Name] $= "" ? "N/A" : %obj.weapon[1, Name];
   %w2 = %obj.weapon[2, Name] $= "" ? "N/A" : %obj.weapon[2, Name];

   commandToClient(%client, 'BottomPrint', "Hit Points: "@%hp@" HP | Shield Strength: "@%sr@" GeV | Power Core: "@%data.maxEnergy@" MW\nWeapon 1: "@%w1@" | Weapon 2: "@%w2@"\n"@%obj.veh_description, 10, 3);
}

function ScoutFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
       commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);
       commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
   }
   else if(%node == 1)
   {
      // bombardier position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      // DarkDragonDX: Ghosts can't use turrets.
      if(!%player.client.isAIControlled() && !(%player.pdead && $votePracticeModeOn))
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }
      %turret.bomber = %player;
      $bWeaponActive = 0;
      %obj.getMountNodeObject(10).selectedWeapon = 1;
      commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      %player.isBomber = true;
   }
   
   // scout flyer == SUV (single-user vehicle)
//   commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);
//   commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
//   $numVWeapons = 2;

   %obj.owner = %player.client;

      // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function ScoutVehicle::playerMounted(%data, %obj, %player, %node)
{
   // scout vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);
   commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
   %obj.owner = %player.client;
//   VehicleInfoBar(%obj);
//   commandToClient(%player.client, 'setRepairReticle');
   %player.client.setWeaponsHudActive("Plasma");

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function MobileBaseVehicle::checkDeploy(%data, %obj)
{
   %mask = $TypeMasks::VehicleObjectType | $TypeMasks::MoveableObjectType |
           $TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType |
           $TypeMasks::ItemObjectType | $TypeMasks::PlayerObjectType |
           $TypeMasks::TurretObjectType | //$TypeMasks::StaticTSObjectType |
           $TypeMasks::InteriorObjectType;

   //%slot 1 = turret   %slot 2 = station
   %height[1] = 0;
   %height[2] = 0;
   %radius[1] = 2.4;
   %radius[2] = 2.4;
   %stationFailed = false;
   %turretFailed = false;

   for(%x = 1; %x < 3; %x++)
   {
      %posXY = getWords(%obj.getSlotTransform(%x), 0, 1);
      %posZ = (getWord(%obj.getSlotTransform(%x), 2) + %height[%x]);
      InitContainerRadiusSearch(%posXY @ " " @ %posZ, %radius[%x], %mask);
      while ((%objFound = ContainerSearchNext()) != 0)
      {
         if(%objFound != %obj)
         {
            if(%x == 1)
               %turretFailed = true;
            else
               %stationFailed = true;
            break;
         }
      }
   }

   //If turret, station or both fail the send back the error message...
//   if(%turretFailed &&  %stationFailed)
//      return "Both Turret and Station are blocked and unable to deploy.";
//   if(%turretFailed)
//      return "Turret is blocked and unable to deploy.";
//   if(%stationFailed)
//      return "Station is blocked and unable to deploy.";

//   %turretFailed = false; // HACK!	// apparently obsolete -soph
//   %stationFailed = false; // HACK!	// -soph

   //Check the station for collision with the Terrain
   %mat = %obj.getTransform();
   for(%x = 1; %x < 7; %x+=2)
   {
      %startPos = MatrixMulPoint(%mat, %data.stationPoints[%x]);
      %endPos = MatrixMulPoint(%mat, %data.stationPoints[%x+1]);

      %rayCastObj = containerRayCast(%startPos, %endPos, $TypeMasks::TerrainObjectType, 0);
      if(%rayCastObj)
         return "Station is blocked by terrain and unable to deploy.";
   }

   // New: Verify we're within terrain range
//   %startPos = %obj.getWorldBoxCenter();			// -[soph]
//   %endPos = vectorAdd(%startPos, "0 0 -6");			// moving this concept to mpb update loop

//   %rayCastObj = containerRayCast(%startPos, %endPos, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType, 0);

//   if(!%rayCastObj)
//      return "MPB somehow not on terrain, unable to deploy.";	// -[/soph]

   return "";
}

function MobileBaseVehicle::checkTurretDistance(%data, %obj)
{
   return "" ;							// killing this all! +soph
   %pos = getWords(%obj.getTransform(), 0, 2);
   InitContainerRadiusSearch(%pos, 1, $TypeMasks::TurretObjectType | $TypeMasks::InteriorObjectType);	// (%pos, 25, $TypeMasks::TurretObjectType | $TypeMasks::InteriorObjectType); -soph
   while ((%objFound = ContainerSearchNext()) != 0)
   {
      if(%objFound.getType() & $TypeMasks::TurretObjectType)
      {
         if(%objFound.getDataBlock().ClassName $= "TurretBase")
            return "Turret Base is in the area. Unable to deploy.";
      }
      else
      {
         %subStr = getSubStr(%objFound.interiorFile, 1, 4);
         if(%subStr !$= "rock" && %subStr !$= "spir" && %subStr !$= "misc")
            return "Building is in the area. Unable to deploy.";
      }
   }
   return "";
}

function LOSDownFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
      %player.setPilot(true);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
          commandToClient(%player.client, 'endBomberSight');
   }
   else if(%node == 1)
   {
      // bombardier position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      // DarkDragonDX: Ghosts can't use turrets.
      if(!%player.client.isAIControlled() && !(%player.pdead && $votePracticeModeOn))
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 0);
      }
      %turret.bomber = %player;
      $bWeaponActive = 0;
      %obj.getMountNodeObject(10).selectedWeapon = 1;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);

	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
      %player.isBomber = true;
          commandToClient(%player.client, 'endBomberSight');
   }
   else
   {
      // tail gunner position
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "Bomber", %node);
      vSlotAutoRepair(%obj, %node);
          commandToClient(%player.client, 'endBomberSight');
   }
   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function LOSDownFlyer::onDamage(%this, %obj)
{
   if(isObject(%obj.getMountNodeObject(10)))
      (%obj.getMountNodeObject(10)).setDamagelevel(%obj.getDamageLevel());
   Parent::onDamage(%this, %obj);
}

//----------------------------
// WILDCAT GRAV CYCLE
//----------------------------

function ScoutVehicle::playerMounted(%data, %obj, %player, %node)
{
   // scout vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);
   commandToClient(%player.client, 'setRepairReticle');

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

//----------------------------
// BEOWULF ASSAULT VEHICLE
//----------------------------

function AssaultVehicle::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0) {
      // driver position
      // is there someone manning the turret?
      //%turreteer = %obj.getMountedNodeObject(1);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
      %player.client.setWeaponsHudActive("Shocklance");
   }
   else if(%node == 1)
   {
      // turreteer position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;   
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      // DarkDragonDX: Ghosts can't use turrets.
      if(!%player.client.isAIControlled() && !(%player.pdead && $votePracticeModeOn))
      {
	echo("WAT");
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }
      %turret.turreteer = %player;
//      %player.ctrlVT = %turret;
//      resetControlVT(%player);      
      // if the player is the turreteer, show vehicle's weapon icons
      //commandToClient(%player.client, 'showVehicleWeapons', %data.getName());
      //%player.client.setVWeaponsHudActive(1); // plasma turret icon (default)
      
//      $aWeaponActive = 0;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      %obj.getMountNodeObject(10).selectedWeapon = 1;
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Assault", %node);
      %player.isBomber = true;
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);
}

//----------------------------
// JERICHO FORWARD BASE
//----------------------------

function MobileBaseVehicle::playerMounted(%data, %obj, %player, %node)
{
   // MPB vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "MPB", %node);
   if(%obj.deploySchedule)
   {
      %obj.deploySchedule.clear();
      %obj.deploySchedule = "";
   }

   if(%obj.deployed !$= "" && %obj.deployed == 1)
   {
      %obj.setThreadDir($DeployThread, false);
      %obj.playThread($DeployThread,"deploy");
      %obj.playAudio($DeploySound, MobileBaseUndeploySound);
      %obj.station.setThreadDir($DeployThread, false);
      %obj.station.getDataBlock().onLosePowerDisabled(%obj.station);
      %obj.station.clearSelfPowered();
      %obj.station.goingOut=false;
      %obj.station.notDeployed = 1;
      %obj.station.playAudio($DeploySound, MobileBaseStationUndeploySound);

      if((%turretClient = %obj.turret.getControllingClient()) !$= "")
      {
         CommandToServer( 'resetControlObject', %turretClient );
      }
      
      %obj.turret.setThreadDir($DeployThread, false);
      %obj.turret.clearTarget();
      %obj.turret.setTargetObject(-1);

      %obj.turret.playAudio($DeploySound, MobileBaseTurretUndeploySound);
      %obj.shield.open();
      %obj.shield.schedule(1000,"delete");
      %obj.deploySchedule = "";

      %obj.fullyDeployed = 0;

      %obj.noEnemyControl = 0;
   }
   %obj.deployed = 0;

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function HAPCFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
      %player.setPilot(true);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
   }
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function BomberFlyer::playerDismounted(%data, %obj, %player)
{
   setTargetSensorGroup(%obj.getTarget(), %obj.team);

   if( isObject( %obj.rearTurret ) )					// (%obj.rearTurret) -soph
      setTargetSensorGroup( %obj.rearTurret.getTarget(), %obj.team);	// setTargetSensorGroup(%obj.getMountNodeObject(2).getTarget(), %obj.team); -soph
      
   setTargetSensorGroup(%obj.getMountNodeObject(10).getTarget(), %obj.team);   
   if( %obj.getMountNodeObject( 0 ) == %player ) 	// +soph
      %obj.setImageTrigger( 4 , false );		// +soph
}

function BomberFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %frame = new StaticShape()
   {
          dataBlock = BomberFlyerStaticF;
          scale = "0.4 0.4 0.4";	// scale = "0.995 0.995 0.995"; -soph
   };
   %obj.mountObject(%frame, 7);   
   MissionCleanup.add(%frame);
   
   %frame.parent = %obj;
   %obj.frame = %frame;
   %frame.team = %obj.teamBought;
   
   setTargetSensorGroup(%frame.getTarget(), %frame.team);

   %obj.frame.setCloaked(true);
   %obj.frame.schedule(5000, setCloaked, false);

   %obj.frame.mountImage(BomberFlyerDecal1, 0);
   %obj.frame.mountImage(BomberFlyerDecal2, 1);
   %obj.frame.mountImage(BomberFlyerDecal3, 2);
   %obj.frame.mountImage(BomberFlyerDecal4, 3);
   
   %obj.frame.mountImage(BomberFlyerEngine1, 4);
   %obj.frame.mountImage(BomberFlyerEngine2, 5);
   %obj.frame.mountImage(BomberFlyerEngine3, 6);
   %obj.frame.mountImage(BomberFlyerEngine4, 7);
   
//   %obj.schedule(5000, "playThread", $ActivateThread, "activate");

   // Cruise Missiles
   %obj.mountImage(BomberCruiseMissileA, 6);
   %obj.mountImage(BomberCruiseMissileB, 7);

   %obj.RaptorReady[6] = true;
   %obj.RaptorReady[7] = true;

   %turret = TurretData::create(BomberTurret);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.selectedWeapon = 1;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
   %turret.mountImage(BomberTurretBarrel,2);
   %turret.mountImage(BomberTurretBarrelPair,3);
   %turret.mountImage(BomberBombImage, 4);
   %turret.mountImage(BomberBombPairImage, 5);
   %turret.mountImage(BomberTargetingImage, 6);
   %obj.turretObject = %turret;
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %turret.vehicleMounted = %obj;
   %turret.setHeat(0.0);
   
   %turret.weapon[1, Display] = true;
   %turret.weapon[1, Name] = "Bomber KM+ AA Turret";
   %turret.weapon[1, Description] = "KM-Band Anti-Air turret madness!";

   %turret.weapon[2, Display] = true;
   %turret.weapon[2, Name] = "Bomber 100kt bombs";
   %turret.weapon[2, Description] = "Blow up a Remote Base near you today!";

   %turret.weapon[3, Display] = true;
   %turret.weapon[3, Name] = "AGM-142 'Raptor' Cruise Missile";
   %turret.weapon[3, Description] = "Combining the power of a bomb with a slow-travelling missile.";

   //vehicle turrets should not auto fire at targets
   %turret.setAutoFire(false);

   //for this particular weapon - a non-firing datablock used only so the AI can aim the turret
   //Also needed so we can set the turret parameters..
   %turret.mountImage(AIAimingSeekingBarrel, 0, false);
//   %turret.mountImage(AIAimingTurretBarrel, 0, false);

   // Tracking computer
   %vehicle.turretAugmented = true;
   %turret.augName = "TargetingComputer";

   %turret = TurretData::create(CruiserShieldCap);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 9);
   %turret.setCapacitorRechargeRate(%turret.getDataBlock().capacitorRechargeRate);
   %turret.mountImage(AssaultTurretParam, 0);
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %obj.shieldCap = %turret;
   %obj.isAvenger = true;
   %turret.setHeat(0.0);
   
   // setup the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);

   %obj.usesCPS = true;
   %obj.veh_description = "The Thundersword has an AA turret and bombs. +75% damage/person. (CPS)";
   vehicleInfoBar(%obj);
   
   %obj.applyBombReticle = false;
   %obj.rearTurret = "";		// to be sure +soph
   %obj.forceCapacitorUse = true;

  // %obj.targetingComputer = true;	// -soph
//   %obj.mountImage(BomberTargetingComputer, 0);
//   %obj.mountImage(AABarrelLarge, 1);
//   %obj.selectedWeapon = 1;
   
//   %obj.weapon[1, Display] = true;
//   %obj.weapon[1, Name] = "Light Mitzi Cannon";
//   %obj.weapon[1, Description] = "Low power, high-speed Mitzi cannon.";

   schedule(2000, %obj, updateCruiserSection, %obj);
}

function updateCruiserSection(%obj)
{
   if( %obj.weapon[1, Name] $= "Forward KM Cannons")	// +[soph]
   {
      %obj.weapon[1, Name] = "KM Cannons";
      %obj.turretObject.weapon[2, Display] = true;
      %obj.turretObject.weapon[2, Name] = "[Not Accessible]";
      %obj.turretObject.weapon[2, Description] = "This weapon system is only usable by the pilot";
   }
   else 						// +[/soph]
   {
      %obj.weapon[1, Display] = false;
      %obj.weapon[1, Name] = "None";
      %obj.weapon[1, Description] = "No weapon.";
   }
}

function BomberFlyerTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(4, false);
   if(%obj.getImageTrigger(6))
   {
      %obj.setImageTrigger(6, false);
      ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
   }
   %client = %obj.getControllingClient();
   %client.player.isBomber = false;
//   %client.player.setControlObject(%client.player);
   %client.player.mountVehicle = false;
//   %client.player.getDataBlock().doDismount(%client.player);
   if(%client.player.getState() !$= "Dead")
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
}

function BomberFlyer::deleteAllMounted(%data, %obj)
{
   %turret = %obj.getMountNodeObject(10);
   if(!%turret)
      return;

   if(%client = %turret.getControllingClient())
   {
      commandToClient(%client, 'endBomberSight');
      %client.player.setControlObject(%client.player);
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
      %client.player.mountVehicle = false;

      %client.player.bomber = false;
      %client.player.isBomber = false;
   }
   %turret.delete();

   %turret = %obj.getMountNodeObject(7);
   if(!%turret)
      return;

   if(%client = %turret.getControllingClient())
   {
      %client.player.setControlObject(%client.player);
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
      %client.player.mountVehicle = false;
   }
   %turret.delete();

   %turret = %obj.getMountNodeObject(2);
   if(!%turret)
      return;

   if(%client = %turret.getControllingClient())
   {
      commandToClient(%client, 'endBomberSight');
      %client.player.setControlObject(%client.player);
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
      %client.player.mountVehicle = false;

      %client.player.bomber = false;
      %client.player.isBomber = false;
   }
   %turret.delete();
}

function BomberFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
      %player.setPilot(true);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
      commandToClient(%player.client, 'endBomberSight');
//      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
//       VehicleInfoBar(%obj);
      %player.lastVehicle = %obj;
      %obj.lastPilot = %player;

//     commandToClient(%player.client, 'setAmmoHudCount', "Missiles:" SPC %obj.numPumas);
     %player.client.setWeaponsHudActive("Blaster");		// "Plasma); -soph

      if(%obj.applyBombReticle)					// just trying this out +soph
           commandToClient(%player.client, 'startBomberSight');	// +soph

     if( isObject( %obj.rearTurret ) )				// moved up here +[soph]
     {
          if( !%obj.moduleState )
          {
               %state = "INACTIVE" ;
               %stateMessage = "Press 'Pack' key to reactivate and engage threats." ;
          }
          else
          {
               %state = "ACTIVE" ;
               %stateMessage = "Press 'Pack' key to shut down and conserve ship power." ;
          }
          commandToClient(%player.client, 'CenterPrint', "Tail Turret" SPC %state SPC "\n" SPC %stateMessage , 8 , 2 ) ;
          messageClient( %player.client , 'MsgVehTailTurret', '\c2Vehicle tail turret is [\c3 %1 \c2].' , %state ) ;
          %obj.rearTurret.owner = %player.client;
     }								// +[/soph]
   }
   else if(%node == 1)
   {
      if(!%obj.turretSelectedNode)
          %obj.turretSelectedNode = 10;
          
      %turret = %obj.getMountNodeObject(%obj.turretSelectedNode);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      
      // DarkDragonDX: Ghosts can't use turrets.
      if(!%player.client.isAIControlled() && !(%player.pdead && $votePracticeModeOn))
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }

      if(%obj.applyBombReticle)
      {
           commandToClient(%player.client, 'startBomberSight');
           %turret.bomber = %player;
           %player.isBomber = true;
      }

//      $bWeaponActive = 0;
      %obj.getMountNodeObject(%obj.turretSelectedNode).selectedWeapon = 1;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);

    // if(%obj.rearTurret)	// moved up -[soph]
    // {
    //      commandToClient(%player.client, 'BottomPrint', "Press 'Pack' to switch between Underbelly and Rear turrets.", 8, 1);
    //      %obj.getMountNodeObject(2).owner = %player.client;
    // }
     
     %obj.getMountNodeObject(10).owner = %player.client;
   }
   else
   {
          commandToClient(%player.client, 'setHudMode', 'Passenger', "Bomber", %node);
          commandToClient(%player.client, 'endBomberSight');
          %player.lastVehicle = %obj;
   }

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function BomberFlyer::onTrigger(%data, %obj, %trigger, %state)     // there.
{
     if(%trigger == 0)
     {
          switch(%state)
          {
               case 0:
                    %obj.fireWeapon = false;

                   // %obj.setImageTrigger(3, false);
                    %obj.setImageTrigger(4, false);

               case 1:
                    %obj.fireWeapon = true;

                   // %obj.setImageTrigger(3, true);
                    %obj.setImageTrigger(4, true);
          }
     }
}

//------------------------------------------------------------------------------
function HoverVehicleData::create(%block, %team, %oldObj)
{
   if(%oldObj $= "")
   {
      %obj = new HoverVehicle() 
      {
         dataBlock  = %block;
         respawn    = "0";
         teamBought = %team;
         team = %team;
      };
   }
   else
   {
      %obj = new HoverVehicle() 
      {
         dataBlock  = %data;
         respawn    = "0";
         teamBought = %team;
         team = %team;
         mountable = %oldObj.mountable;
         disableMove = %oldObj.disableMove;
         resetPos = %oldObj.resetPos;
         respawnTime = %oldObj.respawnTime;
         marker = %oldObj;
      }; 
   }
   
   defineVehicleConstants(%obj);   
   return(%obj);
}

function WheeledVehicleData::create(%data, %team, %oldObj)
{
   if(%oldObj $= "")
   {
      %obj = new WheeledVehicle() 
      {
         dataBlock  = %data;
         respawn    = "0";
         teamBought = %team;
         team = %team;
      };
   }
   else
   {
      %obj = new WheeledVehicle() 
      {
         dataBlock  = %data;
         respawn    = "0";
         teamBought = %team;
         team = %team;
         mountable = %oldObj.mountable;
         disableMove = %oldObj.disableMove;
         resetPos = %oldObj.resetPos;
         deployed = %oldObj.deployed;
         respawnTime = %oldObj.respawnTime;
         marker = %oldObj;
      };   
   }

   defineVehicleConstants(%obj);   
   return(%obj);
}

function FlyingVehicleData::create(%data, %team, %oldObj)
{
   if(%oldObj $= "")
   {
      %obj = new FlyingVehicle() 
      {
         dataBlock  = %data;
         respawn    = "0";
         teamBought = %team;
         team = %team;
      };
      
   }      
   else
   {
      %obj = new FlyingVehicle() 
      {
         dataBlock  = %data;
         teamBought = %team;
         team = %team;
         mountable = %oldObj.mountable;
         disableMove = %oldObj.disableMove;
         resetPos = %oldObj.resetPos;
         respawnTime = %oldObj.respawnTime;
         marker = %oldObj;
      };      
   }

   defineVehicleConstants(%obj);
   return(%obj);
}

function defineVehicleConstants(%obj)
{
      %obj.universalResistFactor = 1.0;
      if( %obj.getDataBlock().damageMod )	// +soph
            %obj.damageMod = %obj.getDataBlock().damageMod;
      else
            %obj.damageMod = 1.0;
      %obj.shieldStrengthFactor = 1.0;
      %obj.selectedWeapon = 1;
      %obj.reloadCount = 4;     

      %obj.dynamicResistance[$DamageGroupMask::Misc] = 1.0;
      %obj.dynamicResistance[$DamageGroupMask::Energy] = 1.0;
      %obj.dynamicResistance[$DamageGroupMask::Explosive] = 1.0;
      %obj.dynamicResistance[$DamageGroupMask::Kinetic] = 1.0;
      %obj.dynamicResistance[$DamageGroupMask::Plasma] = 1.0;
      
      %obj.vModded = false;
}

function VehicleData::applyVehicleMods(%data, %obj, %client)
{
     // Grab all our IDs to configure the vehicle
     %type = %data.vehicleType;
     %hullType = %data.hullType;
     %vID = $VehicleID[%type];
     %priWep = %client.vModPriWeapon[%data] $= "" ? $VehicleDefaultPrimaryWep[%vID] : %client.vModPriWeapon[%data];
     %secWep = %client.vModSecWeapon[%data] $= "" ? $VehicleDefaultSecondaryWep[%vID] : %client.vModSecWeapon[%data];
     %module = %client.vModule[%data] $= "" ? $VehicleDefaultModule[%vID] : %client.vModule[%data];
     %shieldMod = %client.vModShield[%data] $= "" ? $VehicleDefaultShield[%vID] : %client.vModShield[%data];
     %armorMod = %client.vModArmor[%data] $= "" ? $VehicleDefaultArmor[%vID] : %client.vModArmor[%data];
     
     // Define constants - done on creation
//     defineVehicleConstants(%obj);
        
     // Set augmented flag
     %obj.vModded = true;
     
     // Prevent continued execution if no profile
     if(%vID $= "")
          return;
          
     // Start throwing out calls to mods
//     if(%type & $VehicleMask::Thundersword | $VehicleMask::Beowulf)
     if(%type & $VehicleMask::Beowulf)
     {
          %obj.weapon[2, Display] = $VehicleWeaponName[%secWep] $= "None" ? false : true;
          %obj.weapon[2, Name] = $VehicleWeaponName[%secWep];
          %obj.weapon[2, Description] = $VehicleWeaponDesc[%secWep];
          %obj.turretObject.weapon[1, Display] = $VehicleWeaponName[%secWep] $= "None" ? false : true;
          %obj.turretObject.weapon[1, Name] = $VehicleWeaponName[%secWep];
          %obj.turretObject.weapon[1, Description] = $VehicleWeaponDesc[%secWep];
          SubspaceMod::applyMod( %data , %obj , %module ) ;					// tank subspace standard +soph     
     }
     else if(%type & $VehicleMask::Retaliator)
     {
          %obj.turretObjectL.weapon[1, Display] = $VehicleWeaponName[%priWep] $= "None" ? false : true;
          %obj.turretObjectL.weapon[1, Name] = $VehicleWeaponName[%priWep];
          %obj.turretObjectL.weapon[1, Description] = $VehicleWeaponDesc[%priWep];          
     }     
     else
     {
          %obj.weapon[1, Display] = $VehicleWeaponName[%priWep] $= "None" ? false : true;
          %obj.weapon[1, Name] = $VehicleWeaponName[%priWep];
          %obj.weapon[1, Description] = $VehicleWeaponDesc[%priWep];     
     }
     
     %obj.primaryWeaponID = %priWep;

//     note: strangely, you can't give weapons their own ammo at the start, has to happen after the party... or the weapon will never recognize it has ammo
     if($VehicleWeaponUsesAmmo[%priWep])								
          %obj.schedule(1000, setInventory, $VehicleWeaponAmmo[%priWep], %data.max[$VehicleWeaponAmmo[%priWep]]);

     eval($VehicleWeaponClass[%priWep]@"::applyMod("@%data@", "@%obj@", "@%priWep@", "@%type@");");

     if(%type & $VehicleMask::Thundersword | $VehicleMask::Beowulf)
//     if(%type & $VehicleMask::Beowulf)
     {          
          %obj.weapon[2, Display] = $VehicleWeaponName[%secWep] $= "None" ? false : true;	// switched to priWep -soph
          %obj.weapon[2, Name] = $VehicleWeaponName[%secWep];
          %obj.weapon[2, Description] = $VehicleWeaponDesc[%secWep];
          %obj.turretObject.weapon[2, Display] = $VehicleWeaponName[%priWep] $= "None" ? false : true;
          %obj.turretObject.weapon[2, Name] = $VehicleWeaponName[%priWep];
          %obj.turretObject.weapon[2, Description] = $VehicleWeaponDesc[%priWep];          
     }
     else if(%type & $VehicleMask::Retaliator)
     {
          %obj.turretObjectR.weapon[1, Display] = $VehicleWeaponName[%secWep] $= "None" ? false : true;
          %obj.turretObjectR.weapon[1, Name] = $VehicleWeaponName[%secWep];
          %obj.turretObjectR.weapon[1, Description] = $VehicleWeaponDesc[%secWep];          
     }     
     else
     {
          %obj.weapon[2, Display] = $VehicleWeaponName[%secWep] $= "None" ? false : true;
          %obj.weapon[2, Name] = $VehicleWeaponName[%secWep];
          %obj.weapon[2, Description] = $VehicleWeaponDesc[%secWep];     
     }
     
     %obj.secondaryWeaponID = %secWep;     

     if($VehicleWeaponUsesAmmo[%secWep])
          %obj.schedule(1000, setInventory, $VehicleWeaponAmmo[%secWep], %data.max[$VehicleWeaponAmmo[%secWep]]);
                         
     eval($VehicleWeaponClass[%secWep]@"::applyMod("@%data@", "@%obj@", "@%secWep@", "@%type@");");

     %weight = $VehicleArmorMassBonus[%armorMod];
     %obj.universalResistFactor += $VehicleArmorHPBonus[%armorMod];
     eval($VehicleArmorClass[%armorMod]@"::applyMod("@%data@", "@%obj@", "@%armorMod@");");
     
     if( %type & $VehicleMask::Wildcat )							// +[soph]
     {												// +
          %obj.moduleInstalled = true ;								// +
          %obj.moduleID = %module ;								// +
          %obj.moduleName = $VehicleModuleName[ %module ] ;					// +
          %obj.moduleDesc = $VehicleModuleDesc[ %module ] ;					// +
          %obj.moduleClass = $VehicleModuleClass[ %module ] ;					// +
          eval( $VehicleModuleClass[ %module ] @ "::applyMod(" @ %data @ ", " @ %obj @ ", " @ %module @ ");" ) ;
           vehicleTickThread( %obj , %data ) ;							// +
          return ;										// +
     }												// +[/soph]
     // Don't apply the recharge bonus to vehicles which have no shielding
     %weight += $VehicleShieldMassBonus[%shieldMod];
     %obj.damageMod += $VehicleShieldWeaponBonus[%shieldMod];
     %obj.shieldStrengthFactor = $VehicleShieldStrengthBonus[%shieldMod];

     %rechargeBonus = 1.0 + (%obj.rechargeBonus > 0 ? %obj.rechargeBonus : 0);

//     if(%type & $VehicleMask::AllDefaultShielded)
     %rechargeBonus += $VehicleShieldRechargeBonus[%shieldMod];

     %obj.setRechargeRate(%data.rechargeRate * %rechargeBonus);

     if(%type & $VehicleMask::Thundersword | $VehicleMask::Beowulf)
         if(isObject(%obj.turretObject))
             %obj.turretObject.setCapacitorRechargeRate(%obj.turretObject.getDataBlock().capacitorRechargeRate * %rechargeBonus);

     // Remove the cap if we're not going to have shields
     if(%obj.shieldStrengthFactor == 0)
     {
          if(isObject(%obj.shieldCap))
          {
               %obj.shieldCap.delete();
               %obj.shieldCap = "";
          }
     }
//     else
//     {
//         %shieldBonus = 1.0 + (%obj.shieldRechargeBonus > 0 ? %obj.shieldRechargeBonus : 0);
//         %obj.shieldCap.setCapacitorRechargeRate(%obj.shieldCap.getDataBlock().capacitorRechargeRate * %shieldBonus);
//     }
     
     eval($VehicleShieldClass[%shieldMod]@"::applyMod("@%data@", "@%obj@", "@%shieldMod@");");

     // the None modules will revert this to false
     %obj.moduleInstalled = true;
     %obj.moduleID = %module;
     %obj.moduleName = $VehicleModuleName[%module];
     %obj.moduleDesc = $VehicleModuleDesc[%module];
     %obj.moduleClass = $VehicleModuleClass[%module];
     eval($VehicleModuleClass[%module]@"::applyMod("@%data@", "@%obj@", "@%module@");");
     
     // Apply weights
//     if(%weight > 100)
//          %weight = 100;
//     if(%weight <= -100)
//          %weight = -75;
                    
//     %weightImage = getWeightName(%data, %hullType, %weight);
//     %obj.mountImage(%weightImage, 1);
     
     // Start vehicle tick thread
     vehicleTickThread(%obj, %data);
}

function vehicleModuleTrigger(%obj, %turretModule)
{
     // Triggers vehicle module on/off
     %vehicle = %obj.getObjectMount();
     %vehicleData = %vehicle.getDatablock();
     
     if(%vehicle.isAvenger && %turretModule && isObject( %vehicle.rearTurret ) )	// if(%vehicle.isAvenger && !%turretModule) -soph
     {											// functionality moved to pilot
         // %vehicleData.swapTurretControl(%vehicle, %obj);				// removing manual turret -soph
          %turret = %vehicle.rearTurret;						// +[soph]
          %msg = "Tail Turret";
          if( %turret.getDamageState() $= "Destroyed" )
             %msg2 = "\nERROR: MALFUNCTION";
          else if( %vehicle.moduleState )
          {
               %vehicle.moduleState = false;
               %turret.setAutoFire( false );
               %msg2 = "INACTIVE" ;
               %msg3 = "\nPress 'pack' to reactivate" ;
          }
          else
          {
               %vehicle.moduleState = true;
               %turret.setAutoFire( true );
               %msg2 = "ACTIVE" ;
               %msg3 = "\nNow drawing power from vehicle power plant";
          }
          commandToClient( %obj.client , 'CenterPrint' , %msg SPC %msg2 SPC %msg3 , 3 , 2 );
          messageClient( %obj.client , 'MsgVehTailTurret', '\c2Vehicle %1 [\c3 %2 \c2].' , %msg , %msg2 ) ;
											// +[/soph]
          return;
     }
     else if(%vehicle.moduleInstalled)
     {
          %vehicle.moduleState = %vehicle.moduleState ? false : true;
          %state = %vehicle.moduleState ? "onActivate" : "onDeactivate";

//          echo(%vehicle.moduleClass@"::"@%state@"("@%vehicleData@", "@%vehicle@", "@%obj@");");
          eval(%vehicle.moduleClass@"::"@%state@"("@%vehicleData@", "@%vehicle@", "@%obj@");");
     }
}

function VehicleData::handleMineOption(%data, %obj, %player, %slot, %state)
{
     if( %obj.getDataBlock().vehicleType == $VehicleMask::Nightshade )		// special circumstances for nightshade
          %isNightShade = true;							// +soph
     if(%obj.moduleID == $VehicleModule::Inventory || %isNightshade )		// nightshade always has it +soph
     {
          if(isObject(%obj.getMountNodeObject(0)) && %slot != 0 )		// && %player != %obj.getMountNodeObject(0)) -soph
          {
               %time = getSimTime();

               if( %time > %player.lastVInvTime && %time > %obj.invSlotTimeout[%slot] )	// (%time > %player.lastVInvTime) -soph
               {
                    %player.lastWeapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).item;
                    %player.unmountImage($WeaponSlot);
                    %player.play3D("StationInventoryActivateSound");
                    %player.setCloaked(true);
                    %player.schedule(500, "setCloaked", false);              

                    buyFavorites( %player.client , 0 );				// second variable is heal trigger +soph
          
                   // %player.setEnergyLevel(%player.getDatablock().maxEnergy);	// -soph
                    %player.lastVInvTime = %time + 45000;			// %player.lastVInvTime = %time + 10000; -soph
                    resetInvPTimeout( %player );				// +soph
                    %obj.invSlotTimeout[%slot] = %isNightshade ? %time + 20000 : %time + 45000;
                    if( %state )
                         messageClient(%player.client, 'MsgVehInvModActivate', '\c2Inventory Module has provided your selected loadout.  System will be ready in %1 seconds.' , %isNightshade ? 20 : 45 ); // +soph
                    schedule(500, %player, vInvEffectStart, %player);
               }
               else
                    if( %state )						// +[soph]
                    {
                         if( %obj.invSlotTimeout[%slot] > %player.lastVInvTime )
                              messageClient(%player.client, 'MsgVehInvModFail', '\c2Vehicle Inventory Module not ready: please wait %1 seconds.', ( 1 + mFloor ( ( %obj.invSlotTimeout[%slot] - %time ) / 1000 ) ) ) ;
                         else
                              messageClient(%player.client, 'MsgVehInvModFail', '\c2Please wait %1 seconds before using this vehicle\'s Inventory Module.', ( 1 + mFloor ( ( %player.lastVInvTime - %time ) / 1000 ) ) ) ;
                    }								// +[/soph]
          }
     }
}

// Adjust time values here
EngineBase.vehicleTickTimeMS = 50;
EngineBase.vehicleFrameUpdateMS = 1000;

function VehicleData::processTickUpdate(%data, %obj)
{
}

function VehicleData::processFrameUpdate(%data, %obj)
{
}

function vehicleTickThread(%obj, %data)
{
     if(isObject(%obj))
     {
          %data.processTickUpdate(%obj);
          
          if(%data.updateFrames)
          {
               %time = getSimTime();
               
               if(%time > %obj.nextFrameUpdate)
               {
                    %obj.nextFrameUpdate = %time + EngineBase.vehicleFrameUpdateMS;
                    %data.processFrameUpdate(%obj);
               }
          }
          
          schedule(EngineBase.vehicleTickTimeMS, %obj, vehicleTickThread, %obj, %data);
     }
}

function VehicleData::updateAmmoCount(%data, %obj, %client, %ammo)
{
     %ammoCount = %obj.getInventory(%ammo);
     commandToClient(%client, 'setAmmoHudCount', %ammoCount);
}

function VehicleData::reloadAmmo(%data, %obj, %client, %ammo, %time, %energyUse)
{
     if(%obj.reloadThread)
          return;
     
//     if(%obj.reloadCount < 1)
//          return;
          
     if(%obj.getEnergyLevel() < %data.maxEnergy)
     {
          messageClient(%obj.getMountNodeObject(0).client, 'MsgReloadFullEnergy', '\c2Full energy required before weapons can be reloaded.');
          return;
     }
     
     %obj.setEnergyLevel(0);
     %obj.reloadThread = true;
//     %obj.reloadCount--;
     commandToClient(%client, 'setAmmoHudCount', "Reloading..." SPC %time);
     
     schedule(1000, %obj, vehicleUpdateAmmo, %obj, %ammo, %time - 1);
}

function vehicleUpdateAmmo(%obj, %ammo, %time)
{
     %pilot = %obj.getMountNodeObject(0);
     
     if(%pilot)
          if(%time > 0)
               commandToClient(%pilot.client, 'setAmmoHudCount', "Reloading..." SPC %time);

     // Time's up!
     if(!%time)
     {
          %ammoCount = %obj.getDatablock().max[%ammo];
          %obj.setInventory(%ammo, %ammoCount);
          commandToClient(%pilot.client, 'setAmmoHudCount', %ammoCount);
          %obj.play3D(MissileReloadSound);
          %obj.reloadThread = false;
          return;
     }

     schedule(1000, %obj, vehicleUpdateAmmo, %obj, %ammo, %time - 1);
}

function VehicleData::onEnterLiquid(%data, %obj, %coverage, %type)
{
   switch(%type)
   {
      case 0:
         //Water
         %obj.setHeat(0.0);
      case 1:
         //Ocean Water
         %obj.setHeat(0.0);
      case 2:
         //River Water
         %obj.setHeat(0.0);
      case 3:
         //Stagnant Water
         %obj.setHeat(0.0);
      case 4:
         //Lava
         if(!%obj.heatshielded)
              %obj.liquidDamage(%data, $VehicleDamageLava, $DamageType::Lava);
      case 5:
         //Hot Lava
         if(!%obj.heatshielded)
              %obj.liquidDamage(%data, $VehicleDamageHotLava, $DamageType::Lava);
      case 6:    
         //Crusty Lava
         if(!%obj.heatshielded)
              %obj.liquidDamage(%data, $VehicleDamageCrustyLava, $DamageType::Lava);
      case 7:
         //Quick Sand
   }
}

function VehicleData::onLeaveLiquid(%data, %obj, %type)
{
   switch(%type)
   {
      case 0:
         //Water
         %obj.setHeat(1.0);
      case 1:
         //Ocean Water
         %obj.setHeat(1.0);
      case 2:
         //River Water
         %obj.setHeat(1.0);
      case 3:
         //Stagnant Water
         %obj.setHeat(1.0);
      case 4:
         //Lava
      case 5:
         //Hot Lava
      case 6:
         //Crusty Lava
      case 7:
         //Quick Sand
   }

   if(%obj.lDamageSchedule !$= "")
   {
      cancel(%obj.lDamageSchedule);
      %obj.lDamageSchedule = "";
   }
}

function VehicleData::shieldCapCheck(%data, %targetObject, %position, %amount, %damageType)
{
   if(!isObject(%targetObject))
      return 0;

   if(!isObject(%targetObject.shieldCap))
      return %amount;

   if($DamageGroup[%damageType] & $DamageGroupMask::IgnoreShield)
      return %amount;

   %energy  = %targetObject.shieldCap.getCapacitorLevel();
   %shieldScale = $InheritDamageProfile[%damageType] > 0 ? %data.shieldDamageScale[$InheritDamageProfile[%damageType]] : %data.shieldDamageScale[%damageType];   

   if(%shieldScale $= "")
      %shieldScale = 1;

   %strength = %amount * %shieldScale * %targetObject.shieldStrengthFactor * 100;

   if(%strength <= %energy)
   {
      // Shield absorbs all
      %energy -= %strength;
      %targetObject.shieldCap.setCapacitorLevel(%energy);

      // Much trickery applied here
//      %lastEnergy = %targetObject.getEnergyLevel();
//      %targetObject.setEnergyLevel(%data.maxEnergy * (%energy / %targetObject.shieldCap.getDatablock().maxCapacitorEnergy));
      %targetObject.playShieldEffect("0.0 0.0 1.0");
//      %targetObject.setEnergyLevel(%lastEnergy);

      return 0;
   }
   
   // Shield exhausted
   %targetObject.shieldCap.setCapacitorLevel(0);
   return (%energy - %strength) / -100;
}

function VehicleData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %theClient, %proj)
{
//   error("VehicleData::damageObject( "@%data@", "@%targetObject@", "@%sourceObject@", "@%position@", "@%amount@", "@%damageType@", "@%momVec@" )");

//   if(%damageType == $DamageType::Water) //MPB Damage hack?!
//      return 0;

   if(!isObject(%targetObject))
      return 0;

   %targetObject.shieldBleed = 0 ;				// +[soph]
   if(%damageType == $DamageType::Ground && %amount == 1000 )	// +
   {								// + vehicles no longer die instantly when tapped while inverted
      %time = getSimTime();					// + tally script in place to kill them if they invert-impact for too long
      if( !%targetObject.invertedTime || %targetObject.invertedTime < ( %time - 1000 ) )
         %targetObject.invertedTick = 0;			// +
      else							// +
         %targetObject.invertedTick++;				// +
      %targetObject.invertedTime = %time;			// +
      if( %targetObject.invertedTick > 10 )			// +
         %amount = ( %targetObject.invertedTick - 10 ) / 500;	// +
      else							// +
         %amount = 0;						// +
   }								// +[/soph]

//   if(%damageType == $DamageType::Ground && velToSingle(%targetObject.getVelocity()) < 25) // Use this ONLY if you want MPBs to be able
//   to flip over and not explode
//      return 0;

   if(%proj !$= "")
   {
      if(%amount > 0 && %targetObject.lastDamageProj !$= %proj)
      {
         %targetObject.lastDamageProj = %proj;
         %targetObject.lastDamageAmount = %amount;
      }
      else if(%targetObject.lastDamageAmount < %amount)
         %amount = %amount - %targetObject.lastDamageAmount;
      else
         return;
   }
   else 						// +[soph]
   {
      if( %position !$= "" && %amount > 0 )		// hack to cover multiobject vehicle instances without %proj
      {
         if( !%targetObject.lastHitTime )
            %targetObject.lastHitTime = 1;
         %time = getSimTime();
         if( %targetObject.lastHitPosition !$= %position || %time > %targetObject.lastHitTime + 10 )
         {
            // add shield state save data here +soph
            %targetObject.lastHitPosition = %position;
            %targetObject.lastPosDamageAmount = %amount;
         }
         else if( %targetObject.lastPosDamageAmount < %amount )
            %amount = %amount - %targetObject.lastPosDamageAmount;
         else
            return;
         %targetObject.lastHitTime = %time;
      }
   }							// +[/soph]

   // check for team damage
//   %sourceClient = %sourceObject ? %sourceObject.getOwnerClient() : 0;
   if(isObject(%sourceObject))
   {
      %scType = %sourceObject.getType();
      
      if(%scType & $TypeMasks::VehicleObjectType)
          %sourceClient = %sourceObject.getMountNodeObject(0).client;
      else if(%scType & $TypeMasks::TurretObjectType)
          %sourceClient = %sourceObject.getControllingClient();  
      else if(%scType & $TypeMasks::PlayerObjectType)
          %sourceClient = %sourceObject.client;            
      else
          %sourceClient = %sourceObject.getOwnerClient();        
   }
   else 
      %sourceClient = 0;
      
   %targetTeam = getTargetSensorGroup(%targetObject.getTarget());

   if(%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      %sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle with a controlled turret
   }
   else
   {
      %sourceTeam = isObject(%sourceObject) ? getTargetSensorGroup(%sourceObject.getTarget()) : -1;
      // Client is allready defined and this spams console - ZOD
      //%sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle from a vehicle
   }

    // vehicles no longer obey team damage -JR
    if(!$teamDamage && (%targetTeam == %sourceTeam) && %damageType != $DamageType::BomberBombs)
       return;
       
    //but we do want to track the destroyer
    if(%sourceObject)
    {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamageType = %damageType;
    }
    else
        %targetObject.lastDamagedBy = 0;

   // ----------------------------------------------------------------------------------
   // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle
   if(%sourceClient && %sourceClient.playEnemyHitSound)
   {
      if(%targetTeam != %sourceTeam)
      {
//         if((%damageType > 0  && %damageType < 11) ||
//             (%damageType == 13)                    ||
//             (%damageType > 15 && %damageType < 24) ||
//             (%damageType > 25 && %damageType < 32) ||
//             (%damageType > 37 && %damageType < 48) ||
//             (%damageType > 53 && %damageType < 70))
//         {
          if($DamageGroup[%damageType] & $DamageGroupMask::HitSound)
            messageClient(%sourceClient, 'MsgClientHit', "~wfx/weapons/mine_switch.wav");
//         }
      }
   }
   // ----------------------------------------------------------------------------------

   if(%damageType == $DamageType::EMP)
      if(!%targetObject.isSyn)
          if( ( %amount * 100 ) > %targetObject.getEnergyLevel() )					// +soph
             %targetObject.EMPObject();
          else												// +soph
             %targetObject.setEnergyLevel( %targetObject.getEnergyLevel() - ( %amount * 100 ) );	// +soph
      else
          zapVehicle(%targetObject, "FXPulse");
          
//   else if(%damageType == $DamageType::Burn)
//        %targetObject.BurnObject(%sourceObject);

//   echo(%amount);
//   %defenseMod = %targetObject.defenseMod !$= "" ?  %targetObject.defenseMod : 0;
//   %amount *= 1 - %defenseMod;
//   echo(%amount SPC %defenseMod);
      
   // Scale damage type & include shield calculations...
//   if(%data.isShielded && (%data.getName() $= "ScoutFlyer" && %sourceObject.weapon[1, Name] $= "Shrike 50k Bombs"))
//      %amount *= 0.9;

     if(%data.isShielded)
          if( isObject( %targetObject.repulsorField ) )
               %amount = %data.shieldCapCheck( %targetObject , %position , %amount * 2 , %damageType ) / 2 ;	// +soph
          else
          {
               if(%targetObject.shieldCap)
                    %amount = %data.shieldCapCheck(%targetObject, %position, %amount, %damageType);
               else
                    %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);
          }

     %targetObject.shieldBleed = %amount ;	// +soph
     
//   %damageScale = %data.damageScale[%damageType];
   %damageScale = $InheritDamageProfile[%damageType] > 0 ? %data.damageScale[$InheritDamageProfile[%damageType]] : %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

  if( %damageType != $damageType::Impact 	// +[soph]
   && %damageType != $damageType::Ground	// armor shrug code
   && %damageType != $damageType::OutOfBounds	// +
   && %damageType != $damageType::Lava 		// +
   && %damageType != $damageType::NexusCamping	// +
   && %amount > 0 && %data.armorShrug > 0 )	// +
        if( %amount > %data.armorShrug )	// +
           %amount -= %data.armorShrug ;	// +
        else					// +
           %amount = 0 ;			// +[/soph]

     %dynamicResistFactor = %targetObject.dynamicResistance[getDamageGroup(%damageType)];
     if(%dynamicResistFactor $= "")
          %dynamicResistFactor = 1.0;
     %amount *= %dynamicResistFactor;
  
     %universalResistFactor = %targetObject.universalResistFactor;
     if(%universalResistFactor $= "")
          %universalResistFactor = 1.0; 
     %amount *= %universalResistFactor;

     if(%amount != 0)
          %targetObject.applyDamage(%amount);

//     if( %amount >= 0.20 )
//          createHitDebris( %position ) ; 

   if(%targetObject.getDamageState() $= "Destroyed")
   {
      if(isObject(%sourceObject) && %sourceObject.getType() & $TypeMasks::PlayerObjectType && !%sourceObject.client.isAIControlled())
           addStatTrack(%sourceObject.client.guid, "vehiclekills", 1);
         
      if( %momVec !$= "")
         %targetObject.setMomentumVector(%momVec);
   }
}

function isVehicleMenu(%obj, %client, %data) // How in the world did I come up with this again?! wow. lol
{
   if(%data $= "Terragravs")
   {
      %client.vsmenu = 1;
      commandToClient(%client, 'StationVehicleHideHud');
      commandToClient(%client, 'StationVehicleShowHud');
      return true;
   }
   else if(%data $= "Turbogravs")
   {
      %client.vsmenu = 2;
      commandToClient(%client, 'StationVehicleHideHud');
      commandToClient(%client, 'StationVehicleShowHud');
      return true;
   }
   else if(%data $= "Heavy")
   {
      %client.vsmenu = 3;
      commandToClient(%client, 'StationVehicleHideHud');
      commandToClient(%client, 'StationVehicleShowHud');
      return true;
   }
   else if(%data $= "Ground")
   {
      %client.vsmenu = 4;
      commandToClient(%client, 'StationVehicleHideHud');
      commandToClient(%client, 'StationVehicleShowHud');
      return true;
   }
   else if(%data $= "Transports")
   {
      %client.vsmenu = 5;
      commandToClient(%client, 'StationVehicleHideHud');
      commandToClient(%client, 'StationVehicleShowHud');
      return true;
   }
   else if(%data $= "Back")
   {
      %client.vsmenu = 0;
      commandToClient(%client, 'StationVehicleHideHud');
      commandToClient(%client, 'StationVehicleShowHud');
      return true;
   }
   else
   {
      %client.vsmenu = 0;
      return false;
   }
}

function serverCmdBuyVehicle(%client, %blockName)
{
   if(isVehicleMenu(%client.player, %client, %blockname))
      return;

   %time = getSimTime();

   if(%client.scbvt)
      if((%time - %client.scbvt) < 4000) //MrKeen: fix for massive vehicle buying
         return;

   %client.scbvt = %time;

//   commandToClient(%client, 'StationVehicleHideHud');						// moved down -soph
   %team = %client.getSensorGroup();
   if( isObject( %blockName @ "Static" ) )							// +[soph]
   {												// +
      if( %client.mechOverweight[ %client.currentMechSlot ] )					// +
      {												// +
         messageClient( %client , "" , "Mech creation failed: Mech loadout is overweight.~wfx/misc/misc.error.wav" ) ;
         return ;										// +
      }												// +
      %modArmorVal = %client.modArmorVal[ %mech.client.currentMechSlot ] !$= "" ? %client.modArmorVal[ %mech.client.currentMechSlot ] : 0;
      %armorTon = %modArmorVal + $MechDefaultArmorWeight[%mechID] ;				// +									// +
      %station = %client.player.station ;							// +
      if( %client.mechWeight > %station.mechTons )						// +
      {												// +
         messageClient( %client , "" , "Mech creation failed: Station lacks required tonnage.~wfx/misc/misc.error.wav" ) ;
         return ;										// +
      }												// +
      %client.player.mechStation = %station ;							// +
      %mechOverride = true ;									// +
   }												// +[/soph]

   if( vehicleCheck( %blockName , %team ) || %mechOverride )					// if(vehicleCheck(%blockName, %team)) -soph
   {
      %station = %client.player.station.pad;
      if( (%station.ready) && (%station.station.vehicle[%blockName]) )
      {
//         if(%client.mechOverweight[%blockName])						// -[soph]
//         {											// -
//              messageClient(%client, "", "Error creating Mech: Mech is overweight.~wfx/misc/misc.error.wav");
//              return;										// -
//         }											// -[/soph]
               
         %trans = %station.getTransform();
         %pos = getWords(%trans, 0, 2);
         %matrix = VectorOrthoBasis(getWords(%trans, 3, 6));
         %yrot = getWords(%matrix, 3, 5);
         %p = vectorAdd(%pos,vectorScale(%yrot, -3));
         %p =  getWords(%p,0, 1) @ " " @ getWord(%p,2) + 4;

//         error(%blockName);
//         error(%blockName.spawnOffset);

         %p = vectorAdd(%p, %blockName.spawnOffset);
         %rot = getWords(%trans, 3, 5);
         %angle = getWord(%trans, 6) + 3.14;
         %mask = $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType |
                 $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType;
	      InitContainerRadiusSearch(%p, %blockName.checkRadius, %mask);

	      %clear = 1;
         for (%x = 0; (%obj = containerSearchNext()) != 0; %x++)
         {
            if((%obj.getType() & $TypeMasks::VehicleObjectType) && (%obj.getDataBlock().checkIfPlayersMounted(%obj)))
            {
               %clear = 0;
               break;
            }
            else
               %removeObjects[%x] = %obj;
         }
         if(%clear)
         {
            %fadeTime = 0;
            for(%i = 0; %i < %x; %i++)
            {
               if(%removeObjects[%i].getType() & $TypeMasks::PlayerObjectType)
               {
                  %pData = %removeObjects[%i].getDataBlock();
                  %pData.damageObject(%removeObjects[%i], 0, "0 0 0", 1000, $DamageType::VehicleSpawn);
               }
               else
               {
                  if( %removeObjects[%i].frame )						// +[soph] 
                  {										// + hack fix for the bomber freeze 
                     %removeObjects[%i].frame.startFade( 500, 0, true );			// +
                     %removeObjects[%i].frame.schedule(501, "delete");				// +
                  }										// +
                  if( %removeObjects[%i].getMountNodeObject( 2 ) )				// + hack fix for turret freeze
                  {										// +
                     %removeObjects[%i].getMountNodeObject( 2 ).startFade( 500, 0, true );	// +
                     %removeObjects[%i].getMountNodeObject( 2 ).schedule(501, "delete");	// +
                  }										// +[/soph] seems to work
                  %removeObjects[%i].mountable = 0;
                  %removeObjects[%i].startFade( 1000, 0, true );
                  %removeObjects[%i].schedule(1001, "delete");
                  %fadeTime = 1500;
               }
            }
            commandToClient( %client , 'StationVehicleHideHud' ) ;				// from above +soph
            schedule(%fadeTime, 0, "createVehicle", %client, %station, %blockName, %team , %p, %rot, %angle);
         }
         else
            MessageClient(%client, "", 'Can\'t create vehicle. A player is mounted in a vehicle on the creation pad.');
      }
   }
}

function createMech(%client, %station, %blockName, %team , %pos, %rot, %angle)
{
   %mechChassis = %blockName@"Static";
   
   %obj = addObject(%client.player, %mechChassis, "0 0 0 1 0 0 0 ", 0);
   
   if(%obj)
   {
      addStatTrack(%client.guid, "vehiclesbought", 1);
      addStatTrack(0, "vehiclesbought", 1);      
      %station.ready = false;
      %obj.team = %team;

      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      %obj.setCloaked(true);
      %obj.setTransform(%pos @ " " @ %rot @ " " @ %angle);

      %obj.schedule(3700, "playAudio", 0, VehicleAppearSound);
      %obj.schedule(4800, "setCloaked", false);

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };

     vehicleListAdd(%blockName, %obj);
     
     if(%obj.getTarget() != -1)
          setTargetSensorGroup(%obj.getTarget(), %client.team);

     %obj.dataName = %blockName;
     %mechChassis.schedule(5000, onCollision, %obj, %client.player);
   }
}

function createVehicle(%client, %station, %blockName, %team , %pos, %rot, %angle)
{
   %mechBlockName = %blockName@"Static";
   
   if(isObject(%mechBlockName)) // && %mechBlockName.isTacticalMech)
   {
      createMech(%client, %station, %blockName, %team, %pos, %rot, %angle);
      return;
   }
   
   %obj = %blockName.create(%team);   
//   %obj = buildVehicle(%blockName, %team, %client);
   if(%obj)
   {
      addStatTrack(%client.guid, "vehiclesbought", 1);
      addStatTrack(0, "vehiclesbought", 1);      
      //-----------------------------------------------
      // z0dd - ZOD, 4/25/02. MPB Teleporter.
      if ( %blockName $= "MobileBaseVehicle" )
      {
         %station.station.teleporter.MPB = %obj;
         %obj.teleporter = %station.station.teleporter;
      }
      //-----------------------------------------------
      %station.ready = false;
      %obj.team = %team;
      %obj.useCreateHeight(true);
      %obj.schedule(5500, "useCreateHeight", false);
      %obj.getDataBlock().isMountable(%obj, false);
      %obj.getDataBlock().schedule(6500, "isMountable", %obj, true);
      
      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      vehicleListAdd(%blockName, %obj);
      MissionCleanup.add(%obj);
                                  
      %turret = %obj.getMountNodeObject(10);
      if(%turret > 0)
      {
         %turret.setCloaked(true);
         %turret.schedule(4800, "setCloaked", false);
      }

      %obj.setCloaked(true);
      %obj.setTransform(%pos @ " " @ %rot @ " " @ %angle);
   
      %obj.schedule(3700, "playAudio", 0, VehicleAppearSound);
      %obj.schedule(4800, "setCloaked", false);

      %blockName.applyVehicleMods(%obj, %client);
      
      if(%client.player.lastVehicle)
      {
         %client.player.lastVehicle.lastPilot = "";
         vehicleAbandonTimeOut(%client.player.lastVehicle);
         %client.player.lastVehicle = "";
      }   
      %client.player.lastVehicle = %obj;
      %obj.lastPilot = %client.player;

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };

      if ( %client.isVehicleTeleportEnabled() )
         %obj.getDataBlock().schedule(5000, "mountDriver", %obj, %client.player);
   }
   if(%obj.getTarget() != -1)
      setTargetSensorGroup(%obj.getTarget(), %client.getSensorGroup());
   // We are now closing the vehicle hud when you buy a vehicle, making the following call
   // unnecessary (and it breaks stuff, too!)
   //VehicleHud.updateHud(%client, 'vehicleHud');
}

function FlyingVehicle::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getDamageState() !$= "Destroyed" && !%obj.liquidDamageProof)
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
      passengerLiquidDamage(%obj, %damageAmount, %damageType);
   }
   else
      %obj.lDamageSchedule = "";
}

function Turret::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getDamageState() !$= "Destroyed" && !%obj.liquidDamageProof)
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
      passengerLiquidDamage(%obj, %damageAmount, %damageType);
   }
   else
      %obj.lDamageSchedule = "";
}

function TurretData::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getDamageState() !$= "Destroyed" && !%obj.liquidDamageProof)
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
      passengerLiquidDamage(%obj, %damageAmount, %damageType);
   }
   else
      %obj.lDamageSchedule = "";
}

function WheeledVehicle::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getDamageState() !$= "Destroyed" && !%obj.liquidDamageProof)
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
      passengerLiquidDamage(%obj, %damageAmount, %damageType);
   }
   else
      %obj.lDamageSchedule = "";
}

function HoverVehicle::liquidDamage(%obj, %data, %damageAmount, %damageType)
{
   if(%obj.getDamageState() !$= "Destroyed" && !%obj.liquidDamageProof)
   {
      %data.damageObject(%obj, 0, "0 0 0", %damageAmount, %damageType);
      %obj.lDamageSchedule = %obj.schedule(50, "liquidDamage", %data, %damageAmount, %damageType);
      passengerLiquidDamage(%obj, %damageAmount, %damageType);
   }
   else
      %obj.lDamageSchedule = "";
}

function VehicleData::onDestroyed(%data, %obj, %prevState)
{
//   trace(1);

   if(!isObject(%obj)) // heh oops
      return;

    %obj.ignoreDED = true;

    %team = %obj.getControllingClient().team; //%obj.client.team;

    if(%data.canControl)
    {
        %cl = %obj.getControllingClient();

        if(isObject(%cl))
           %cl.setControlObject(%cl.player);
    }

    if(%obj.lastDamagedBy)
    {
        %destroyer = %obj.lastDamagedBy;

        if(!%data.jetfire)
             game.vehicleDestroyed(%obj, %destroyer);
    }

    if(!%data.jetfire)										// -soph
        radiusVehicleExplosion(%data, %obj);
   
   if(%obj.turretObject)
      if(%obj.turretObject.getControllingClient())
         %obj.turretObject.getDataBlock().playerDismount(%obj.turretObject);

    if(%data.jetfire)
    {
      %obj.prevDest = true;
      %flingee = %obj.getMountNodeObject(0);
      %flingee.getDataBlock().doDismount(%flingee, true);					// moved here +soph
      %flingee.damage( %obj.lastDamagedBy , %obj.getPosition() , 100 , %obj.lastDamageType );	// %flingee.damage(0, %obj.getPosition(), 100, $DamageType::Explosion); -soph
     // %flingee.getDataBlock().doDismount(%flingee, true);					// moved up -soph

      %data.deleteAllMounted(%obj);

      if(isObject(%obj))
      {
         schedule(500, %obj, setPosition, %obj, vectorAdd(%obj.position, "40 -27 10000"));
         %obj.schedule(2000, "delete");
      }

       return;
    }

//   if(%obj.getDatablock().SMPB)
//   {
//      $SMPBBuilding[%obj.team] = true;
//      $TeamDeployedCount[%obj.team, SMPB]--;
//      schedule($SMPBTimeout*60000, 0, resetSMPB(%obj.team));
//   }

   for(%m = 0; %m < 8; %m++)
   {
      if(%obj.getMountNodeObject(%m) && %obj.getMountNodeObject(%m).isExtension)
         deleteSlotExtension(%obj, %m);
   }

   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
//      echo("damaging objects...");
      if (%obj.getMountNodeObject(%i)) { // && %i.getDatablock().className $= "Armor") {
         %flingee = %obj.getMountNodeObject(%i);
         %flingee.getDataBlock().doDismount(%flingee, true);
         %xVel = 250.0 - (getRandom() * 500.0);
         %yVel = 250.0 - (getRandom() * 500.0);
         %zVel = (getRandom() * 100.0) + 50.0;
         %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
         %flingee.applyImpulse(%flingee.getTransform(), %flingVel);
//         echo("got player..." @ %flingee.getClassName());
         %flingee.damage(0, %obj.getPosition(), 0.7, $DamageType::Crash);
      }
   }

   %data.deleteAllMounted(%obj);

   if(isObject(%obj.shieldCap))
       %obj.shieldCap.delete();

   schedule(500, %obj, setPosition, %obj, vectorAdd(%obj.position, "40 -27 10000"));
   %obj.schedule(2000, "delete");
//   trace(0);
}

function resetSMPB(%team)
{
   $SMPBBuilding[%team] = "";
}

function findEmptySeat(%vehicle, %player, %forceNode)
{
   %minNode = 1;
   %node = -1;
   %dataBlock = %vehicle.getDataBlock();
   %dis = %dataBlock.minMountDist;
   %playerPos = getWords(%player.getTransform(), 0, 2);
   %message = "";
   %armor = %player.client.armor;
   %armorData = %player.getDatablock();
   %isTacticalMech = %armordata.isTacticalMech;
   %isBattleAngel = %armordata.isBattleAngel;
   
   %mask = %armorData.armorMask;   

//   switch(%mask)						// -[soph] 
//   {								// -
//     case $ArmorMask::Light:					// -
//          %armorType = 0;					// -
//     								// -
//     case $ArmorMask::Medium:					// -
//          %armorType = 1;					// -
//								// -
//     case $ArmorMask::Engineer:				// -
//          %armorType = 1;					// -
//								// -
//     case $ArmorMask::Heavy:					// -
//          %armorType = 2;					// -
//								// -
//     case $ArmorMask::BattleAngel:				// -
//          %armorType = 2;					// -
//								// -
//     default:							// -
//          %armorType = -1;					// -
//   }          						// -[/soph]
                                                                    
   
//   if(%mask & $ArmorMask::Light)
//   {
//      %armorType = 0;
//   }
//   else if(%mask & $ArmorMask::Medium | $ArmorMask::Engineer)
//   {
//      %armorType = 1;
//   }
//   else if(%mask & $ArmorMask::Heavy | $ArmorMask::BattleAngel)
//   {
//      %armorType = 2;
//   }
//   else
//      %armorType = -1;
   
//   if(%dataBlock.lightOnly)					// -[soph]
//   {								// -
//      if(%armorType == 0)					// -
//         %minNode = 0;					// -
//      else							// -
//         %message = '\c2Only Scout Armors can pilot this vehicle.~wfx/misc/misc.error.wav';
//   }								// -
//   if(%armorType == 0 || %armorType == 1)			// -
//      %minNode = 0;						// -
//   else if(%armorType == 2)					// -
//      %minNode = 1; //findFirstHeavyNode(%dataBlock);		// -
//   else							// -
//      %message = '\c2This armor is not compatible with this vehicle slot.~wfx/misc/misc.error.wav';
//								// -[/soph]

   if(%forceNode !$= "")
      %node = %forceNode;
   else
   {
      for(%i = 0; %i < %dataBlock.numMountPoints; %i++)
         if(!%vehicle.getMountNodeObject(%i))
         {
            if(%i == 0 && (%armorType == 2))
               continue;

            %seatPos = getWords(%vehicle.getSlotTransform(%i), 0, 2);
            %disTemp = VectorLen(VectorSub(%seatPos, %playerPos));
            if(%disTemp <= %dis)
            {
               %node = %i;
               %dis = %disTemp;
//               echo("Node: "@%node);
            }
         }
    }
   if( %mask & %dataBlock.mountArmorsAllowed[ %node ] || %dataBlock.jetfire)	// +[soph]
   {										// + clean mount
   }										// +[/soph]
   else										// if(%node != -1 && %node < %minNode) -soph
   {
//      echo("Node: "@%node);
//      echo("minNode: "@%minNode);

//      if(%message $= "")							// -[soph]
//      {									// -
//         if(%node == 0)							// -
//            %message = '\c2Only Scout, Engineer, Assault Armors can pilot this vehicle.~wfx/misc/misc.error.wav';
//         else									// -
//            %message = '\c2Only Scout, Engineer, Assault Armors can use that position.~wfx/misc/misc.error.wav';
//      }									// -[/soph]

      if( %node != -1 && !%player.noSitMessage )				// if(!%player.noSitMessage) -soph
      {
         %count = 0 ;							
         if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::Light )	// +[soph]
         {									// +
            %list[ %count ] = "Scouts" ;					// +
            %count++ ;								// +
         }									// +
         else if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::MagIon )	// +
         {									// +
            %list[ %count ] = "MagIons" ;					// +
            %count++ ;								// +
         }									// +
         if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::Engineer )	// +
         {									// +
            %list[ %count ] = "Engineers" ;					// +
            %count++ ;								// +
         }									// +
         if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::Medium )	// +
         {									// +
            %list[ %count ] = "Assaults" ;					// +
            %count++ ;								// +
         }									// +
         if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::Blastech )	// +
         {									// +
            %list[ %count ] = "Blastechs" ;					// +
            %count++ ;								// +
         }									// +
         if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::Heavy )	// +
         {									// +
            %list[ %count ] = "Juggernauts" ;					// +
            %count++ ;								// +
         }									// +
         if( %dataBlock.mountArmorsAllowed[ %node ] & $ArmorMask::BattleAngel )	// +
         {									// +
            %list[ %count ] = "Battle Angels" ;					// +
            %count++ ;								// +
         }									// +
         if( %count > 1 )							// +
         {									// +
            for( %i = 0 ; %i < %count - 1 ; %i++ )				// +
               %message = %message SPC %list[ %i ] @ "," ;			// +
            %message = %message SPC "and" SPC %list[ %count - 1 ] ;		// +
         }									// +
         else									// +
            %message = %list[ 0 ] SPC "and" SPC %list[ 1 ] ;			// +
         if( !%node )								// +
            %message = "Only" SPC %message SPC "can pilot this vehicle" ;	// +
         else									// +
            %message = "Only" SPC %message SPC "can occupy this position" ;	// +
										// +[/soph]
         %player.noSitMessage = true;
         %player.schedule(2000, "resetSitMessage");
         messageClient( %player.client , 'MsgArmorCantMountVehicle' , '\c2%1.~wfx/misc/misc.error.wav' , %message ) ;	// messageClient(%player.client, 'MsgArmorCantMountVehicle', %message); -soph
      }
      %node = -1;
   }
   return %node;
}

function VehicleData::onCollision(%data, %obj, %col)
{
   // Keep vehicle ghost from harming players?
   if(%obj.getDamageState() $= "Destroyed")
      return;

   if(!isObject(%obj) || !isObject(%col))
      return;

   %vecLen = vectorLen(%obj.getVelocity());

   // prevent mech collision catapulting
//   if(%col.isMech)
//   {
//     %col.schedule(32, setVelocity, "0 0 0");
//     %obj.setDamageState("Destroyed");
//     return;
//   }
   
   // associated "crash" sounds
   if(%vecLen > %data.hardImpactSpeed)
        %obj.playAudio(0, %data.hardImpactSound);
   else if(%vecLen > %data.softImpactSpeed)
        %obj.playAudio(0, %data.softImpactSound);

//     Parent::onCollision(%data, %obj, %col);
}

function VehicleData::onImpact(%data, %vehicleObject, %collidedObject, %vec, %vecLen)
{
   if(%vecLen > %data.minImpactSpeed)
      %data.damageObject(%vehicleObject, 0, VectorAdd(%vec, %vehicleObject.getPosition()),
                         %vecLen * %data.speedDamageScale, $DamageType::Ground);

   // associated "crash" sounds
   if(%vecLen > %data.hardImpactSpeed)
      %vehicleObject.playAudio(0, %data.hardImpactSound);
   else if(%vecLen > %data.softImpactSpeed)
      %vehicleObject.playAudio(0, %data.softImpactSound);
}

function vehicleAbandonTimeOut(%vehicle)			// had to overwrite this +[soph]
{								// ts tail turret freaking it out
   if(%vehicle.getDatablock().cantAbandon $= "" && %vehicle.lastPilot $= "")
   {
      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
      {
         %passenger = %vehicle.getMountNodeObject(%i);		// +soph
         if ( isObject( %passenger ) && !%passenger.dead )	// (%vehicle.getMountNodeObject(%i)) -soph
         {
            if( %passenger != %vehicle.rearTurret )		// tail turret fix +soph
            {
//               %passenger = %vehicle.getMountNodeObject(%i);	// ^up^ -soph
               if(%passenger.lastVehicle !$= "")
                  schedule(15000, %passenger.lastVehicle,"vehicleAbandonTimeOut", %passenger.lastVehicle);
               %passenger.lastVehicle = %vehicle;   
               %vehicle.lastPilot = %passenger;   
               return;
            }
//            else						// -soph
//               %killturret = true;				// -soph
         }
      }

      if(%vehicle.respawnTime !$= "")
         %vehicle.marker.schedule = %vehicle.marker.data.schedule(%vehicle.respawnTime, "respawn", %vehicle.marker); 
      %vehicle.mountable = 0;
      %vehicle.startFade(1000, 0, true);
      %vehicle.schedule(1001, "delete");
      if( isObject( %vehicle.rearTurret ) )			// if(%killturret) -soph
      {
         %vehicle.rearTurret.startFade(1000, 0, true);		// %vehicle.getMountNodeObject( 2 ).startFade(1000, 0, true); -soph
         %vehicle.rearTurret.schedule(1001, "delete");		// %vehicle.getMountNodeObject( 2 ).schedule(1001, "delete"); -soph
      }
      if( isObject( %vehicle.turret ) )				// +[soph]
      {
         %vehicle.rearTurret.startFade(1000, 0, true);
         %vehicle.rearTurret.schedule(1001, "delete");
      }								// +[/soph]
   }
}

// DarkDragonDX: Mostly meant for practice mode when the game tries to delete your vehicle with you in it
function vehicleClearAllVehicleHuds(%vehicle)
{
	%datablock = %vehicle.getDatablock();
	for (%i = 0; %i < %datablock.numMountPoints; %i++)
		commandToClient(%vehicle.getMountNodeObject(%i).client, 'setHudMode', 'Standard', "", 0);
}

function FlyingVehicle::clearAllVehicleHuds(%this) { vehicleClearAllVehicleHuds(%this); }
function WheeledVehicle::clearAllVehicleHuds(%this) { vehicleClearAllVehicleHuds(%this); }
function HoverVehicle::clearAllVehicleHuds(%this) { vehicleClearAllVehicleHuds(%this); }
// End DDDX

exec("scripts/modscripts/vmods/modules.cs");
exec("scripts/modscripts/vmods/weaponMods.cs");
exec("scripts/modscripts/vmods/armorMods.cs");
exec("scripts/modscripts/vmods/shieldMods.cs");
