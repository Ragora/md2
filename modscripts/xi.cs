//-----------------------------------------------------------------------------
// Xi Command System v2.0
// Dynamic command system - would be much faster if torque supported structs
//--------------------------------------

function Xi::onAdd(%this)
{
    Xi.commandCount = 0;
    Xi.Version = 2.0;
}

function Xi::onRemove(%this)
{
   // Thou shalt not spam
}

if(!isObject(Xi))
   Meltdown.addClass(Xi); // Xi command system

//--------------------------------------------------------------------------
// String Table
//--------------------------------------

$XI::Player = 0;
$XI::Admin = 1;
$XI::SuperAdmin = 2;

//--------------------------------------------------------------------------
// Helper Functions
//--------------------------------------

function nameToClient(%name)
{
   %count = ClientGroup.getCount();
   
   for(%i = 0; %i < %count; %i++)
   {
      %obj = ClientGroup.getObject(%i);
      
      if(%obj.nameBase $= %name)
          return %obj;
   }

   for(%i = 0; %i < %count; %i++)
   {
      if(strstr(strlwr(%obj.nameBase), strlwr(%name)) != -1)
         return %obj;
   }
   
   return 0;
}

function GUIDToClientID(%id)
{
   %count = ClientGroup.getCount();

   for(%i = 0; %i < %count; %i++)
   {
      %obj = ClientGroup.getObject(%i);

      if(%obj.guid == %id)
         return %obj;
   }

   return 0;
}

function nameToGUID(%name)
{
   %client = nameToClient(%name);
   
   return %client ? %client.guid : 0;
}

function nameToIP(%name)
{
   %client = nameToClient(%name);
   
   return %client ? %client.getAddress() : "127.0.0.1";
}

function getAdminLevel(%client)
{
     if(%client.isAIControlled())
          return $XI::Player;
          
     if(%client.isAdmin)
     {
          if(%client.isSuperAdmin)
               return $XI::SuperAdmin;
          else
               return $XI::Admin;
     }
     else
          return $XI::Player;
}

function canTorture(%challenger, %target)
{
     return getAdminLevel(%challenger) > getAdminLevel(%target);
}

//--------------------------------------------------------------------------
// Server chat command overload
//--------------------------------------

if($Host::MaxMessageLen < 1) // check
   $Host::MaxMessageLen = 255;

function serverCmdTeamMessageSent(%client, %text)
{
   // Special case handling - won't be able to tell if /me is used in team-only or global context if used in a plugin
   if(getSubStr(%text, 0, 4) $= "/me ")
   {
      chatMessageTeam(%client, %client.team, '\c1* %1 %2', %client.namebase, getSubStr(%text, 4, $Host::MaxMessageLen));

      if($Host::LogTeamChat)
           logChat("[TEAM "@%client.team@"]" SPC %client.nameBase@":" SPC %text);
           
      return;
   }
   
   if(getSubStr(%text, 0, 1) $= "/" && getSubStr(%text, 1, 1) !$= "")
   {
      Xi.evalCommand(%client, getSubStr(%text, 1, $Host::MaxMessageLen));

      return;
   }

   if(strlen(%text) >= $Host::MaxMessageLen)
      %text = getSubStr(%text, 0, $Host::MaxMessageLen);

   %name = %client.showRank ? addTaggedString($StatRankID[getTrackedStat(%client.guid, "rank")] SPC getTaggedString(%client.name)) : %client.name;
   
   chatMessageTeam(%client, %client.team, '\c3%1: %2', %client.namebase, %text);

   if($Host::LogTeamChat)
   {
      if(%client.guid !$= "")
          logChat("[TEAM "@%client.team@"]" SPC %client.nameBase@":" SPC %text);
   }
}

function serverCmdMessageSent(%client, %text)
{
   // Special case handling - won't be able to tell if /me is used in team-only or global context if used in a plugin
   if(getSubStr(%text, 0, 4) $= "/me ")
   {
      chatMessageAll(%client, '\c1* %1 %2', %client.namebase, getSubStr(%text, 4, $Host::MaxMessageLen));

      if($Host::LogChat)
         logChat("[CHAT] "@%client.nameBase@":" SPC %text);
         
      return;
   }

   if(getSubStr(%text, 0, 1) $= "/" && getSubStr(%text, 1, 1) !$= "")
   {
      Xi.evalCommand(%client, getSubStr(%text, 1, $Host::MaxMessageLen));

      return;
   }

   if(strlen(%text) >= $Host::MaxMessageLen)
      %text = getSubStr(%text, 0, $Host::MaxMessageLen);

   %name = %client.showRank ? addTaggedString($StatRankID[getTrackedStat(%client.guid, "rank")] SPC getTaggedString(%client.name)) : %client.name;
   
   chatMessageAll(%client, '\c4%1: %2', %name, %text);

   if($Host::LogChat)
   {
      if(%client.guid !$= "")
          logChat("[CHAT] "@%client.nameBase@":" SPC %text);
   }
}

function cannedChatMessageAll( %sender, %msgString, %name, %string, %keys )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;

   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
      cannedChatMessageClient( ClientGroup.getObject(%i), %sender, %msgString, %name, %string, %keys );
      
   if($Host::LogChat)
   {
      if(%client.guid !$= "")
         logChat("[VOICE]" SPC "["@%keys@"]" SPC %sender.nameBase@":" SPC %string);
   }
}

function cannedChatMessageTeam( %sender, %team, %msgString, %name, %string, %keys )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;

   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
   {
      %obj = ClientGroup.getObject( %i );
      if ( %obj.team == %sender.team )
         cannedChatMessageClient( %obj, %sender, %msgString, %name, %string, %keys );
   }

   if($Host::LogTeamChat)
   {
      if(%client.guid !$= "")
         logChat("[T"@%team@" VOICE]" SPC "["@%keys@"]" SPC %sender.nameBase@":" SPC %string);
   }
}

//--------------------------------------------------------------------------
// Commands

// keen: considering using get/setField() to parse tabs and switching to argc/argv for Xi commands
function Xi::evalCommand(%this, %cl, %msg)
{
    %obj = %cl.player;
    %cmd = firstWord(%msg); // what IS the command?
    %len = (%tmplen = (strlen(%cmd) + 1)) < 0 ? 0 : %tmplen; // how LONG is the command?

    if(Xi.isCommand(%cmd))
    {
        EngineBase.logManager.logToFile("AdminLog", timestamp() SPC "[Xi] User "@%cl.nameBase@" command used:" SPC %msg);
        
        if(Xi.authCommand(%cl, %cmd))
        {
            // edit by SoLo: allow for eval() to process "'s
            %val = strReplace(getSubStr(%msg, %len, 128), "\"", "\\\"");
            eval("Xi."@%cmd@"("@%cl@", \""@%val@"\");");
            return;
        }
        else
            Xi.logProtectedAttempt(%cl, %cmd, "Insufficient access level to use this command.");
    }
    else
        messageClient(%cl, 'MsgXIError', '\c5Xi[Error]: Command does not exist.');
}

function Xi::LogProtectedAttempt(%this, %cl, %cmd, %errorcode)
{
    if(%errorcode $= "")
        %errorcode = "Attempted protected function without access.";

    messageClient(%cl, 'MsgXIInvalidCommand', '\c5Xi[Error]: %1', %errorcode);
    
    EngineBase.logManager.logToFile("AdminLog", timestamp() SPC "[Admin] Access denied for user" SPC %cl.nameBase SPC "("@%cl.guid@"@"@%cl.getAddress()@")" SPC "using" SPC %cmd SPC "- Reason:" SPC %errorcode);
    return;
}

function Xi::AuthCommand(%this, %cl, %command)
{
    if(getAdminLevel(%cl) >= Xi.getCommandLevel(%command))
        return true;
    else
        return false;
}

function Xi::addCommand(%this, %level, %cmd, %desc)
{
    if(%cmd $= "")
        return;
        
    if(%desc $= "")
        %desc = "No description.";
        
    if(%level < 0)
        %level = 1;

    %count = Xi.commandCount;

    for(%i = 0; %i < %count; %i++)
        if(%cmd $= Xi.commandList[%i])
            return;
    
    Xi.commandList[%count] = %cmd;
    Xi.commandLevel[%count] = %level;
    Xi.commandDescription[%count] = %desc;
    
    Xi.commandCount++;
    
    return 0;
}

function Xi::listLevelCommands(%this, %cl)
{
    %cmdlist = "";
    %cmdcount = 0;
    %adminLevel = getAdminLevel(%cl);
    
    for(%i = 0; %i < Xi.commandCount; %i++)
    {
        if(%adminLevel >= Xi.commandLevel[%i])
        {
             %cmdlist = %cmdlist@" "@Xi.commandList[%i];
             %cmdcount++;
        }
    }

    %total = %cmdcount@" "@%cmdlist;

    return %total;
}

function Xi::cmdToID(%this, %cmd)
{
    for(%i = 0; %i < Xi.commandCount; %i++)
        if(%cmd $= Xi.commandList[%i])
            return %i;

    return 0;
}

function Xi::isCommand(%this, %cmd)
{
    if(Xi.cmdToID(%cmd))
        return true;
    else
        return false;
}

function Xi::getCmdDesc(%this, %cmd)
{
    return Xi.commandDescription[Xi.cmdToID(%cmd)];
}

function Xi::getCommandLevel(%this, %cmd)
{
    return Xi.commandLevel[Xi.cmdToID(%cmd)];
}

//-----------------------------------------------------------------------------
// Server command callbacks for admin system

function serverCmdXiEval(%cl, %msg)
{
    Xi.evalCommand(%cl, %msg);
}

function serverCmdListLevelCommands(%cl)
{
    commandToClient(%cl, 'XiListAdminCommands', Xi.listLevelCommands(%cl));
}

// Load Xi modules
execDir("modscripts/Xi");
execDirBase("Modules");
