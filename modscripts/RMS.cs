if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//========================================

function destroyAttachments(%obj)     // Death~Bot
{
   %inc = 0;
   while((%int = %obj.attachment[%inc]) !$= "")
   {
      %int.setDamageState(Destroyed);

      %obj.attachment[%inc] = "";
      %inc++;
   }
}

//====================================== Remote Missile System
$TeamDeployableMax[RMSSPack] = 1;
//$TeamDeployableMax[DeployedRMSSTargetPack] = $Feature[RMSsalvoMax];  // --- (ST)
//$RMSChargeTime = 60000;
$RMSMinimumRange = 700 ;	// +soph 

datablock AudioProfile(RMSSHumSound)
{
filename = "fx/powered/turret_heavy_idle.wav";
description = ClosestLooping3d;
    preload = true;
};

datablock AudioProfile(RMSSDeploySound)
{
filename = "fx/vehicles/MPB_deploy_turret.wav";
description = AudioBIGExplosion3d;
    preload = true;
};

datablock AudioProfile(RMSSBDeploySound)
{
filename = "fx/misc/red_alert.wav";
description = AudioBIGExplosion3d;
    preload = true;
};

datablock StaticShapeData(RMSStube) : StaticShapeDamageProfile
{
   shapeFile = "stackable2l.dts";   // stackable2m
   maxDamage = 15;			// = 5; -soph
   destroyedLevel = 15;			// = 5; -soph
   disabledLevel = 15;			// = 5; -soph
   explosion = VehicleExplosion;

   deployedObject = true;
   heatSignature = 1;
   humSound = RMSSHumSound;

   targetNameTag = 'Remote';
   targetTypeTag = 'Missile Silo';	// = 'Nuke Silo'; -soph

   cmdCategory = "Tactical";
   cmdIcon = "CMDSwitchIcon";//CMDGeneratorIcon
   cmdMiniIconName = "commander/MiniIcons/com_switch_grey";

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
};

datablock StaticShapeData(RMSST2) : StaticShapeDamageProfile
{
   shapeFile = "stackable2m.dts";   // teamlogo_projector
   maxDamage = 2.0;			// = 5; -sop
   destroyedLevel = 2.0;		// = 5; -sop
   disabledLevel = 2.0;			// = 5; -sop
   explosion = deployablesExplosion;

   deployedObject = true;
   heatSignature = 1;
   humSound = RMSSHumSound;

   targetNameTag = 'Mutli';		// = 'Remote'; -soph
   targetTypeTag = 'Missile Tube';	// = 'Nuke Silo'; -soph

   //targetTypeTag = 'Remote Missile Salvo System';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
};

//datablock StaticShapeData(RMSNuke) : StaticShapeDamageProfile
//{
//   shapeFile = "vehicle_air_scout.dts";   // teamlogo_projector
//   maxDamage = 3;
//   destroyedLevel = 3;
//   disabledLevel = 3;
//   explosion = VehicleExplosion;

//   deployedObject = true;
////   heatSignature = 1;
////   humSound = RMSSHumSound;

//   targetNameTag = 'Illudium-Pew 36';
//   targetTypeTag = 'Explosive Space Modulator';

//   //targetTypeTag = 'Remote Missile Salvo System';

//   debrisShapeName = "debris_generic_small.dts";
//   debris = DeployableDebris;
//};

function RMSNuke::onDestroyed(%data, %obj, %prevState)
{
   beginNuke(%proj.bkSourceObject, %pos);   

   Parent::onDestroyed(%data, %obj, %prevState);
}

datablock ShapeBaseImageData(RMSSImage)
{
   mass = 10;

   shapeFile = "stackable2m.dts";
   item = RMSSPack;
   mountPoint = 1;
   offset = "0 -0.2 -0.2";
   deployed = RMSStube;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = RMSSDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(RMSSPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "RMSSImage";
   pickUpName = "a Remote Missile System";
   heatSignature = 0;

   emap = true;
};

//datablock LinearProjectileData(RMSSupShot)
//{
//   projectileShapeName = "vehicle_land_mpbase.dts";
//   scale               = "1 1 1";
//   faceViewer          = true;
//   directDamage        = 30;
//   hasDamageRadius     = true;
//   indirectDamage      = 30;
//   damageRadius        = 50.0;
//   kickBackStrength    = 2000;
//   radiusDamageType    = $DamageType::Nuke;

//   sound 				= ScoutFlyerThrustSound;
//   explosion           = "LargeMissileExplosion";
//   underwaterExplosion = UnderwaterHandGrenadeExplosion;
//   splash              = PlasmaSplash;

//   baseEmitter         = ShrikeEngineEmitter;
//   delayEmitter        = ContrailEmitter;

//   dryVelocity       = 150.0;
//   wetVelocity       = 350.0;
//   velInheritFactor  = 0.3;
//   fizzleTimeMS      = 10000;
//   lifetimeMS        = 10000;
//   explodeOnDeath    = false;
//   reflectOnWaterImpactAngle = 0.0;
//   explodeOnWaterImpact      = false;
//   deflectionOnWaterImpact   = 0.0;
//   fizzleUnderwaterMS        = 2000;
//   ignoreReflection = true;
//   activateDelayMS = -1;
//};

datablock LinearProjectileData(RMSSdownShot)
{
   projectileShapeName = "vehicle_land_mpbase.dts";
   scale               = "1 1 1";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 4.0;
   damageRadius        = 37.5;
   kickBackStrength    = 5000;
   radiusDamageType    = $DamageType::Nuke;

   sound 				= ScoutFlyerThrustSound;
   explosion           = "LargeMissileExplosion";
   underwaterExplosion = UnderwaterHandGrenadeExplosion;
   splash              = PlasmaSplash;

   baseEmitter         = ShrikeEngineEmitter;
   delayEmitter        = ContrailEmitter;

   dryVelocity       = 275.0;
   wetVelocity       = 167.5;
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;
   ignoreReflection = true;
   activateDelayMS = -1;
};

datablock SeekerProjectileData(RMSSCruiseMissile)	// +[soph]
{
   scale = "10.0 10.0 10.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "vehicle_grav_scout.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 2.0;
   damageRadius        = 75.0;
   radiusDamageType    = $DamageType::RMSS;
   kickBackStrength    = 5500;

   explosion           = "noneExplosion";
   underwaterExplosion = "noneExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 0.0;		// launched from silo, no inheritance +soph

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 100;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 30000 ;
   muzzleVelocity      = 10.0 ;
   maxVelocity         = 200 ;
   turningSpeed        = 40 ;		// = 20;
   acceleration        = 50.0 ;

   proximityRadius     = 5 ;

   terrainAvoidanceSpeed    = 0 ;	// = 10;
   terrainScanAhead         = 0 ;	// = 25;
   terrainHeightFail        = 0 ;	// = 12;
   terrainAvoidanceRadius   = 0 ;	// = 10;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 7.0;
   lightColor  = "0.4 0.15 0.1";

   useFlechette = false;
   flechetteDelayMs = 50;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock LinearFlareProjectileData(RMSSCruiseMissileShock)
{
   projectileShapeName = "turret_muzzlePoint.dts" ;
   faceViewer          = false ;
   directDamage        = 0.0 ;
   hasDamageRadius     = true ;
   indirectDamage      = 0.35 ;
   damageRadius        = 75.0 ;
   radiusDamageType    = $DamageType::Nuke ;
   kickBackStrength    = 11000 ;

   explosion           = "BAExplosion" ;
   underwaterExplosion = "BAExplosion" ;

   splash              = PlasmaSplash ;
   velInheritFactor    = 0 ;

   dryVelocity       = 0.1 ;
   wetVelocity       = 0.1 ;
   fizzleTimeMS      = 32 ;
   lifetimeMS        = 32 ;
   explodeOnDeath    = true ;
   reflectOnWaterImpactAngle = 90 ;
   explodeOnWaterImpact      = false ;
   deflectionOnWaterImpact   = 0.0 ;
   fizzleUnderwaterMS        = 32 ;

   activateDelayMS = -1 ;

   numFlares         = 0 ;
   flareColor        = "0 0 0" ;
   flareModTexture   = "flaremod" ;
   flareBaseTexture  = "flarebase" ;

   hasLight    = true ;
   lightRadius = 20 ;
   lightColor  = "0.50 0.30 0.10" ;
};							// +[/soph]

function RMSSdownShot::onExplode(%data, %proj, %pos, %mod)
{
   %player = %proj.client;
   beginNuke(%proj.bkSourceObject, %pos);

   Parent::onExplode(%data, %proj, %pos, %mod);
}

function RMSSCruiseMissile::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{
   %p = new LinearFlareProjectile()
   {
      dataBlock        = RMSSCruiseMissileShock ;
      initialDirection = "0 0 1" ;
      initialPosition  = %pos ;
      sourceObject     = %proj.RMSTarget ;
      sourceSlot       = 0 ;
      bkSourceObject   = %proj.bksourceObject ;
   };
   MissionCleanup.add( %p ) ;

   Parent::onExplode( %data , %proj , %pos , %mod ) ;
}									// +[/soph]

function RMSSPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function RMSSImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

//RMSS Functions

function getAreaTarget( %launchPos , %pos , %radius )
{
	if( !%pos )
		return ;

	if( !%radius )
		%radius = 50 ;

	%x = getWord(%pos, 0);
	%y = getWord(%pos, 1);
	%x += getRandom(%radius * -1, %radius);
	%y += getRandom(%radius * -1, %radius);
	%radius = 80 - %radius ;						// +[soph]
	%z = getWord( %pos , 2 ) + getRandom( %radius * -1 , %radius ) ;	// + various maths to arrive at the desired raycast point
										// +
	%castSource = vectorSub( %launchPos , %pos ) ;				// +
	%castSource = vectorNormalize( getWord( %castSource , 0 ) SPC getWord( %castSource , 1 ) SPC "0" ) ;
	%castSource = vectorNormalize( getWord( %castSource , 0 ) SPC getWord( %castSource , 1 ) SPC "1.5" ) ;
	%castSource = vectorAdd( %pos , vectorScale( %castSource , 150 ) ) ;	// +
	%castTarget = %x SPC %y SPC %z ;					// +
	%castTarget = vectorSub( %castTarget , %castSource ) ;			// +
	%castTarget = vectorScale( %castTarget , 1.2 ) ;			// +
	%castTarget = vectorAdd( %castSource , %castTarget ) ;			// +
	%hitPoint = containerRayCast( %castSource , %castTarget , $TypeMasks::StaticShapeObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType ) ;
										// +
	if( %hitPoint )								// +
		return getWord( %hitPoint , 1 ) SPC getWord( %hitPoint , 2 ) SPC getWord( %hitPoint , 3 ) ;
	else									// +
		return %castTarget ;						// +[/soph]
}

function getAirTarget( %pos , %radius , %zMin )
{
   if(!%pos)
      return;

   if(!%radius)
      %radius = 50;

   %x = getWord( %pos , 0 );
   %y = getWord( %pos , 1 );
   %z = getWord( %pos , 2 );
   %x += getRandom( %radius * -1 , %radius );
   %y += getRandom( %radius * -1 , %radius );
   %z += getRandom( %radius * -1 , %radius );

   if( %z < getTerrainHeight( %x @ " " @ %y @ " " @ %z ) )
      %z = getTerrainHeight( %x @ " " @ %y @ " " @ %z );
   if( %z <= %zMin++ )
      %z = %zMin;

   return %x @ " " @ %y @ " " @ %z;
}

function sequenceRMSS(%obj, %player)
{
   %pos = vectorAdd(%obj.getPosition(), "0 0 10");
   %scVec = "0 0 1000";
   %end = VectorAdd(%pos, %scVec);
   %searchResult = containerRayCast(%pos, %end, $TypeMasks::DefaultLOSType, %obj);

   if(%searchResult)
      %obj.noRain = true;
   else
      %obj.noRain = false;

//   for(%i = 0; %i < 96; %i++)
      schedule(1000, %obj, "fireRMSS", %obj, %player, %i); // fire time varies

   %obj.RMSwarningSound = play3D(%obj, "RMSSBDeploySound"); //---before it fires play this
}

function sequenceRMSSingle(%obj, %player)
{
   %pos = vectorAdd(%obj.getPosition(), "0 0 10");
   %scVec = "0 0 1000";
   %end = VectorAdd(%pos, %scVec);
   %searchResult = containerRayCast(%pos, %end, $TypeMasks::DefaultLOSType, %obj);

   if(%searchResult)
      %obj.noRain = true;
   else
      %obj.noRain = false;

   schedule(100, %obj, fireRMSSingle, %obj, %player);
}

function fireRMSS(%obj, %src, %seq)
{
   if(!isObject(%obj))
      return;

   %pos = posFromTransform(%obj.attachment[getRandom(5)].getWorldBoxCenter()); // much better effect... loops through all 6 attachments and fires randomly out of one
   %npos = VectorAdd(%pos, "0 0 1");

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%npos);
   %FFRObject.schedule(32, delete);

   %p = new LinearProjectile()
   {
      dataBlock        = "RMSSupShot";
      initialDirection = "0 0 1";
      initialPosition  = %npos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %src;
   };

//   %obj.playAudio(0, "MechRocketFireSound"); // --- RMSSFireSound here
   %obj.play3d(MechRocketFireSound);
   MissionCleanup.add(%p);

   if(!%obj.noRain)
      schedule(5000, %obj, "rainRMSS", %obj, %src);
}

function fireRMSSingle(%obj, %src)
{
   if(!isObject(%obj))
      return;

   %pos = posFromTransform(%obj.attachment[getRandom(5)].getWorldBoxCenter()); // much better effect... loops through all 6 attachments and fires randomly out of one
   %npos = VectorAdd(%pos, "0 0 1");

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%npos);
   %FFRObject.schedule(32, delete);

   %p = new LinearProjectile()
   {
      dataBlock        = "RMSSupShot";
      initialDirection = "0 0 1";
      initialPosition  = %npos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %src;
   };

   %obj.play3d(MechRocketFireSound);
   MissionCleanup.add(%p);
   
   if(!%obj.noRain)
      schedule(1000, %obj, rainRMSSingle, %obj, %src);
}

function rainRMSS(%obj, %src)
{
//   %radius = $Feature[RMSradius];
//   if(!%radius)
      %radius = 1;
      
   %pos = VectorAdd(%obj.targetPos, "0 0 1000");
   %dir = VectorSub(getAreaTarget(%pos, %radius), %pos);

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%pos);
   %FFRObject.schedule(32, delete);

   %p = new (LinearProjectile)()
   {
      dataBlock        = "RMSSDownShot";
      initialDirection = VectorNormalize(%dir);
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %src;
   };
   MissionCleanup.add(%p);
}

function rainRMSSingle(%obj, %src)
{
   %pos = VectorAdd(%obj.targetPos, "0 0 1000");
   %dir = VectorSub(getTerrainHeight(%pos), %pos);

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%pos);
   %FFRObject.schedule(32, delete);

   %p = new LinearProjectile()
   {
      dataBlock        = "RMSSDownShot";
      initialDirection = "0 0 -1"; //VectorNormalize(%dir);
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %src;
   };
   MissionCleanup.add(%p);
}

function RMSSImage::extendedDeployChecks(%item, %plyr)
{
   %dist = %plyr.isNearbyFriendlyBase();									// if(!%plyr.isNearbyFriendlyBase(100)) -soph
   if( %dist > 150 )												// +soph
   {
      $EDC::Reason = "You are trying to deploy" SPC mFloor( %dist - 149 ) @ "m too far away from the base.";	// $EDC::Reason = "You are trying to deploy too far away from the base.";
      return true;
   }
   else 
      if ( %dist < 0 )										// [soph]
      {
         $EDC::Reason = "There is no friendly base node in this region, silo cannot be deployed.";
         return true;
      }												// [/soph]


   $EDC::Reason = "";
   return false;
}

function RMSSImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 4"));

   %trans = vectorAdd(%item.surfacePt, "0 0 -1.5") SPC "0 1 0 0";	// "0 0 -0.5") SPC "0 1 0 0"; -soph
   %depltube = addObject(%plyr, "RMSStube", %trans, "", "8 10 1.8");	// "8 10 0.8"); -soph
   %depltube.isRMS = true;
//   addObject(%plyr, "RMSMarker", %item.surfacePt SPC "0 1 0 0", %depltube);

//   %trans[1] = vectorAdd(%item.surfacePt, "2 0 0") SPC "0 1 0 0";
//   %deplObj1 = addObject(%plyr, "RMSST2", %trans[1], %depltube, "4 4 4");
//   %trans[2] = vectorAdd(%item.surfacePt, "-2 0 0") SPC "0 1 0 0";
//   %deplObj2 = addObject(%plyr, "RMSST2", %trans[2], %depltube, "4 4 4");
//   %trans[3] = vectorAdd(%item.surfacePt, "2 4 0") SPC "0 1 0 0";
//   %deplObj3 = addObject(%plyr, "RMSST2", %trans[3], %depltube, "4 4 4");
//   %trans[4] = vectorAdd(%item.surfacePt, "-2 4 0") SPC "0 1 0 0";
//   %deplObj4 = addObject(%plyr, "RMSST2", %trans[4], %depltube, "4 4 4");
//   %trans[5] = vectorAdd(%item.surfacePt, "2 -4 0") SPC "0 1 0 0";
//   %deplObj5 = addObject(%plyr, "RMSST2", %trans[5], %depltube, "4 4 4");
//   %trans[6] = vectorAdd(%item.surfacePt, "-2 -4 0") SPC "0 1 0 0";
//   %deplObj6 = addObject(%plyr, "RMSST2", %trans[6], %depltube, "4 4 4");

   %depltube.play3d(%item.deploySound);

//   %depltube.attachment[0] = %deplObj1;
//   %depltube.attachment[1] = %deplObj2;
//   %depltube.attachment[2] = %deplObj3;
//   %depltube.attachment[3] = %deplObj4;
//   %depltube.attachment[4] = %deplObj5;
//   %depltube.attachment[5] = %deplObj6;

   for( %i = 0; %i < 6; %i++ )						// +soph
      %depltube.tube[%i] = 0 ;						// +soph

   %pta = vectorAdd(%item.surfacePt, "5 5 1.5");			// "5 5 0.5"); -soph
   %ptb = vectorAdd(%item.surfacePt, "-5 5 1.5");			// "-5 5 0.5"); -soph
   %ptc = vectorAdd(%item.surfacePt, "5 -5 1.5");			// "5 -5 0.5"); -soph
   %ptd = vectorAdd(%item.surfacePt, "-5 -5 1.5");			// "-5 -5 0.5"); -soph

   %pt1337 = vectorAdd(%item.surfacePt, "-3 -2.7 1.5");			// "-3 -2.7 0.5"); -soph
   %pt1338 = vectorAdd(%item.surfacePt, "-3 2.7 1.5");			// "-3 2.7 0.5"); -soph
   %pt1339 = vectorAdd(%item.surfacePt, "3 -2.7 1.5");			// "3 -2.7 0.5"); -soph
   %pt1340 = vectorAdd(%item.surfacePt, "3 2.7 1.5");			// "3 2.7 0.5"); -soph

   %vec1 = getVectorFromPoints(%pta, %ptb);
   %vec2 = getVectorFromPoints(%ptb, %ptd);
   %vec3 = getVectorFromPoints(%ptd, %ptc);
   %vec4 = getVectorFromPoints(%ptc, %pta);

   %rota = vectorToRotZ(%vec1);
   %rotb = vectorToRotZ(%vec2);
   %rotc = vectorToRotZ(%vec3);
   %rotd = vectorToRotZ(%vec4);

   %lena = getDistance3D(%pta, %ptb);
   %lenb = getDistance3D(%ptb, %ptd);
   %lenc = getDistance3D(%ptd, %ptc);
   %lend = getDistance3D(%ptc, %pta);

   %scale = ".2 .2 5";

   %depltube.shield[0] = addForcefield(%plyr, "DeployedTeamSlowFieldBareGreen", %pta, %rota, %scale, %depltube);
   %depltube.shield[1] = addForcefield(%plyr, "DeployedTeamSlowFieldBareGreen", %ptb, %rotb, %scale, %depltube);
   %depltube.shield[2] = addForcefield(%plyr, "DeployedTeamSlowFieldBareGreen", %ptc, %rotc, %scale, %depltube);
   %depltube.shield[3] = addForcefield(%plyr, "DeployedTeamSlowFieldBareGreen", %ptd, %rotd, %scale, %depltube);

   %depltube.damageStateCounter = 0;
   %depltube.owner = %plyr;
   $TeamDeployedCount[%plyr.team, RMSSPack]++;
   $RMSSilo[%plyr.team] = %depltube; // --- (ST)
   //schedule(3000, 0, "SearchLoopRMSS", %depltube); // --- oops... old news (ST)
   %depltube.owner = %plyr;
   %depltube.position = %item.surfacePt;
   %depltube.tubeNum = -1;
   %depltube.isLoaded = false;
   %depltube.reloading = true;							// +[soph]
   messageClient( %plyr.client , 'MsgDEDWarmup' , '\c5You have deployed your team\'s Remote Missile Silo.\n The first salvo will be ready in 60 seconds.' );
   messageTeamExcept( %plyr.client , 'MsgDEDWarmup', '\c5%1 has deployed your team\'s Remote Missile Silo.\n The first salvo will be ready in 60 seconds.' , %plyr.client.name );
   %depltube.loadThread = schedule( 60000 , 0 , "RMSSAutoLoad" , %depltube );	// +[/soph]
}

function RMSSAutoLoad( %obj )			// +[soph]
{						// the following five functions comprise the core of the new RMS
   if( !isObject( %obj ) )
      return;

   for( %i = 0; %i < 6; %i++ )
   {
      %obj.tubeNum++;

      if( %obj.tubeNum > 5 )
         %obj.tubeNum = 0;

      if( !%obj.tube[%obj.tubeNum] )
         break;					// found a free slot
   }
   if( %i >= 6 )
   {
      if( %obj.loadThread )
         cancel( %obj.loadThread );
      %obj.loadThread = false;
      return;
   }

   switch( %obj.tubeNum )
   {
      case 0:
         %trans = vectorAdd(%obj.position, "2 -4 0.5");
      case 1:
         %trans = vectorAdd(%obj.position, "2 0 0.5");
      case 2:
         %trans = vectorAdd(%obj.position, "2 4 0.5");
      case 3:
         %trans = vectorAdd(%obj.position, "-2 -4 0.5");
      case 4:
         %trans = vectorAdd(%obj.position, "-2 0 0.5");
      case 5:
         %trans = vectorAdd(%obj.position, "-2 4 0.5");
   }

   %tube = addObject( %obj.owner, "RMSST2" , %trans SPC "0 1 0 0" , %obj , "4 4 4" );
   schedule( 32 , 0 , RMSSLoadSalvoAnim , %tube , %trans , 16 );
						// the above auto-adds to the first free .tube index
   %obj.tube[%obj.tubeNum] = %tube ;		// and this stores the tube's location

   %obj.isLoaded = true;
   %obj.reloading = false;
   if( %obj.loadThread )
      cancel( %obj.loadThread );		// to be sure
   %obj.loadThread = schedule( 30000 , 0 , "RMSSAutoLoad" , %obj );
}

function RMSSLoadSalvoAnim( %tube , %trans , %tick )
{
   if( !isObject( %tube ) )
      return;

   %trans = vectorAdd( %trans , "0 0 0.125" ) SPC "0 1 0 0";
   %tube.setTransform(%trans);
   if( %tick-- > 0 )
      schedule( 32 , 0 , RMSSLoadSalvoAnim , %tube , %trans , %tick );
}

function RMSSQuadFire( %obj , %src , %pos )
{
   if(!isObject(%obj))
   {
      centerPrint( %src.client , "Missile Silo is error.  Please contact technical support." , 10 , 1);
      return;
   }

   %j = %obj.tubeNum;
   %k = 0;
   %tube = -1;
   for( %i = 0; %i < 6; %i++ )
   {
      %j++ ;
      if( %j > 5 )
         %j = 0 ;

      if( %obj.tube[ %j ] )
         if( isObject( %obj.tube[ %j ] ) )
            if( %tube == -1 )
               %tube = %j ;
            else
               %k++ ;
         else
            %obj.tube[%j] = 0 ;
   }

   if( %tube == -1 )
   {
      if( !%obj.loadThread ) 
         %obj.loadThread = schedule( 45000 , 0 , RMSSAutoLoad , %obj );
      centerPrint( %src.client , "Missile Silo empty.  Please try again later." , 10 , 1);
      return;
   }

   %launchPos = posFromTransform( %obj.tube[%tube].getWorldBoxCenter() );
   %launchPos[0] = VectorAdd( %launchPos , "1.5 -1.5 1.5" );
   %launchPos[1] = VectorAdd( %launchPos , "-1.5 -1.5 1.5" );
   %launchPos[2] = VectorAdd( %launchPos , "1.5 1.5 1.5" );
   %launchPos[3] = VectorAdd( %launchPos , "-1.5 1.5 1.5" );

   %distance = getDistance3D( %launchPos , %pos );
   if( %distance < $RMSMinimumRange )
   {
      bottomPrint( %src.client , "Target within" SPC $RMSMinimumRange @ "m of launch.  Firing short-range munitions." , 10 , 1);	// +soph
      %projectile = "PumaMissile";
   }
   else 
   {
      %projectile = "RMSSCruiseMissile";
      if( %distance > 4000 )
      {
         centerPrint( %src.client , "Target beyond maximum range.  Launch aborted." , 10 , 1);
         return;
      }
   }

   for( %i = 0; %i < 4; %i++ )
      schedule( %i * 100 , 0 , RMSSCruiseLaunch , %src , %obj.tube[%tube] , %projectile , %launchPos[%i] , %pos , 10 + ( %i * 10 ) , %distance );
   for( %i = 0; %i < 4; %i++ )
      schedule( 400 + ( %i * 100 ) , 0 , RMSSCruiseLaunch , %src , %obj.tube[%tube] , %projectile , %launchPos[%i] , %pos , 50 + ( %i * 10 ) , %distance );

   %obj.tube[%tube].schedule( 800, "delete" );
   %obj.tube[%tube] = 0;

   if( !%obj.loadThread ) 
      %obj.loadThread = schedule( 45000 , 0 , RMSSAutoLoad , %obj );

   centerPrint( %src.client , "RMS tube "@ ( %tube + 1 ) @" fired!  "@ %k @" salvos remaining." , 10 , 1 );
   if( !%k )
      %depltube.isLoaded = false;
   %depltube.reloading = true;
}

function RMSSCruiseLaunch( %source , %object , %projectile , %launchPos , %targetPos , %variance , %distance )
{
   if( !isObject( %object) )
      return;

   %object.play3d(MechRocketFireSound);

   %p = new SeekerProjectile()
   {
      dataBlock        = %projectile;
      initialDirection = "0 0 1";
      initialPosition  = %launchPos;
      sourceObject     = %object;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %source;
   };
   MissionCleanup.add(%p);

   %target = new StaticShape()
   {
      dataBlock        = mReflector ;
   };
   MissionCleanup.add( %target ) ;
   %target.setPosition( getWord( %launchPos , 0 ) @" "@ getWord( %launchPos , 1 ) @" "@ ( getWord( %launchPos , 2 ) + 1000 ) ) ;
   %p.setObjectTarget( %target ) ;
   %p.RMSTarget = %target ;

   if( %projectile $= "RMSSCruiseMissile" )
   {
      %targetPos = getAreaTarget( %launchPos , %targetPos , %variance ) ;
      schedule( 1000 , 0 , cruiseMissileTargetTrack , %p , %target , %targetPos , %distance ) ;
   }
   else
   {
      %targetPos = getAirTarget( %targetPos , %variance / 1.5 , getWord( %launchPos , 2 ) ) ;
      schedule( 250 , 0 , cruiseMissileTargetTrack , %p , %target , %targetPos , %distance ) ;
   }
}

function cruiseMissileTargetTrack( %missile , %target , %targetPos , %initialDistance )
{
   if( !isObject( %missile ) || %missile.exploded )
      %target.delete() ;

   if( !isObject( %target ) )
      return ;

   %distance = getDistance2D( %missile.getPosition() , %targetPos ) ;
   %elevation = %distance * %distance / %initialDistance ;
   %target.setPosition( getWord( %targetPos , 0 ) SPC getWord( %targetPos , 1 ) SPC ( getWord( %targetPos , 2 ) + %elevation ) ) ;
   schedule( 100 , 0 , cruiseMissileTargetTrack , %missile , %target , %targetPos , %initialDistance ) ;
}						// +[/soph]

function RMSStube::onDestroyed(%this, %obj, %prevState)
{
   if(%obj.RMSSTarget !$= "")			// this shouldn't be $= -soph
      %obj.RMSSTarget.schedule(450, "delete");

   Parent::onDestroyed(%this, %obj, %prevState);
   $TeamDeployedCount[%obj.team, RMSSPack]--;
   $RMSSilo[%obj.team] = ""; // --- Silo (ST)
   %obj.schedule(500, "delete");

   for( %i = 0; %i < 6; %i++ )				// +[soph]
      if( %obj.tube[%i] )
         %obj.tube[%i].schedule( 500, "delete" );	// +[/soph]

   destroyAttachments(%obj);
   destroyForcefields(%obj);

   if(%obj.isLoaded)
   {
     if(%obj.disassembled)
        return;
        
       // %src = createReflectorForObject(%obj);	// -[soph ]
       // %src.team = 0;				// no longer nuclear 
        
       // beginNuke(%src, %obj.getWorldBoxCenter());	// -[/soph]  
   }
}

function RMSST2::onDamage( %this , %obj )
{
   // Set damage state based on current damage level
   %damage = %obj.getDamageLevel();
   if(%damage >= %this.destroyedLevel)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         %obj.setDamageState(Destroyed);
         // if object has an explosion damage radius associated with it, apply explosion damage

         if(isObject(%obj.deadSmokeEmitter))
            %obj.deadSmokeEmitter.delete();

         if(%this.expDmgRadius)
            RadiusExplosion(%obj, %obj.getWorldBoxCenter(), %this.expDmgRadius, %this.expDamage, %this.expImpulse, %obj, $DamageType::Explosion);
         %obj.setDamageLevel(%this.maxDamage);

         if(%obj.relaySet)
            %obj.relaySet.remove(%obj);

         if(%obj.DESetSet)	// alterations to get the D+D working right - soph
	 {
            %obj.DESetSet.remove(%obj);
	    %obj.DESetMember = false;
	 }

         %obj.clearLoopingDamages();
	 %obj.ignoreDED = true;
         %obj.ignoreRepairThis = true;
	 %obj.inDEDField = false;	// don't think this matters for shapes, but better safe than sorry -soph
         
          if(isObject(%obj.ownerShieldBeacon))
              %obj.ownerShieldBeacon.setDamageState(Destroyed);
      }

//      %this.onStateDestroyed(%obj);
   }
   else
   {
      if(%damage >= %this.disabledLevel)
      {
         if(%obj.getDamageState() !$= "Disabled")
            %obj.setDamageState(Disabled);
      }
      else
      {
         if(%obj.getDamageState() !$= "Enabled")
	 {
            %obj.setDamageState(Enabled);
	    %obj.ignoreRepairThis = false;
	    %obj.ignoreDED = false;	
	 }
      }
   }
}

function RMSST2::onDestroyed(%this, %obj, %prevState)
{
  // Parent::onDestroyed(%this, %obj, %prevState);	// -soph
   if( !%obj.parent.loadThread )			// +[soph]
      %obj.parent.loadThread = schedule( 45000 , 0 , RMSSAutoLoad , %obj.parent );

   for( %i = 0; %i < 6; %i++ )
      if( %obj == %obj.parent.tube[%i] )
         %obj.parent.tube[%i] = 0 ;			// +[/soph]

   %obj.schedule(500, "delete");
}

function RMSStube::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function DeployedRMSSTargetPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function DeployedRMSSTargetImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function DeployedRMSSTargetImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 2"));

   %trans[0] = vectorAdd(%item.surfacePt, "0 0 -5") SPC "0 1 0 0";
   %deplTRG = addObject(%plyr, "DeployedRMSSTarget", %trans[0], "", "7 7 12");

   %deplTRG.play3d(%item.deploySound);

   $TeamDeployedCount[%plyr.team, DeployedRMSSTargetPack]++;
}

function DeployedRMSSTargetImage::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   $TeamDeployedCount[%obj.team, DeployedRMSSTargetPack]--;
   %obj.schedule(500, "delete");
}

function DeployedRMSSTarget::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(500, "delete");
//   %obj.parent.setDamageState(Destroyed);
}


////////////////////////--------------------\\\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////------end RMSS------\\\\\\\\\\\\\\\\\\\\\\\\\
////////////////////////--------------------\\\\\\\\\\\\\\\\\\\\\\\\\
