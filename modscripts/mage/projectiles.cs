// Mage Ion
// Projectiles

datablock SeekerProjectileData(MagicMissileProj)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 0.25;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::ElectroMissile;
   kickBackStrength    = 1250;

   explosion           = "MBSingularityExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = OldMMFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 4000;	// = 3000; -soph
   muzzleVelocity      = 100.0;
   maxVelocity         = 200.0;
   turningSpeed        = 150.0;
   acceleration        = 35.0;	// 50.0; -soph

   proximityRadius     = 1;	// 8; proximity outside blast radius makes for a lot of dud 'hits' -soph

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = DiscProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 1;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock LinearFlareProjectileData(SoulStrikeProj)
{
   scale               = "4.5 4.5 4.5";
   faceViewer          = true;
   directDamage        = 0.00;	// = 0.25; -soph
   directDamageType    = $DamageType::MitziTransparent; 
   hasDamageRadius     = true;
   indirectDamage      = 1.25;	// = 1.0; -soph
   damageRadius        = 6.0;
   kickBackStrength    = 2500;
   radiusDamageType    = $DamageType::MitziTransparent;

   explosion           = "RumbleBlastExplosion";
   underwaterExplosion = "RumbleBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = RBTrailEmitter;

   dryVelocity       = 175.0;
   wetVelocity       = 175.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "0.5 0.4 0.6";	// "0.15 0.2 1"; -soph
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   ignoreReflection = true;

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0.5 0.4 0.6";		// "0.15 0.2 1"; -soph
};

datablock TargetProjectileData(MitziDeathRayFX)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 200;
   beamColor           	= "1 1 1";
								
   startBeamWidth			= 0.40;
   pulseBeamWidth 	   = 0.65;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 36.0;
   pulseLength         	= 0.15;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

datablock SniperProjectileData(MageDetectBeam200)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "noneExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 200;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.29;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 5.0;
   lightColor          = "1 1 1";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

function MageDetectBeam200::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
     call(%projectile.detectScript, %data, %projectile, %targetObject, %modifier, %position, %normal);
}

datablock ShockLanceProjectileData(HealGreen)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.5;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;

   boltSpeed[0] = 6.0;
   boltSpeed[1] = -3.0;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "liquidTiles/LushWater01_Algae";
   texture[1] = "liquidTiles/LushWater01";
   texture[2] = "liquidTiles/LushWater01_Algae";
   texture[3] = "liquidTiles/LushWater01";
};

datablock LinearFlareProjectileData(ShockBlasterBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.53 ;		// 0.0; -soph
   directDamageType    = $DamageType::ShockCannon;
   hasDamageRadius     = true;
   indirectDamage      = 0.53;
   damageRadius        = 5.0;
   kickBackStrength    = 2500;
   radiusDamageType    = $DamageType::ShockCannon;
   explosion           = EnergyRifleExplosion;
   splash              = PlasmaSplash;

   dryVelocity       = 88.0;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 6000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "2.5 2.5 2.5";
   numFlares         = 20;
   flareColor        = "0.6 0.1 0.8";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function SBLanceRandomThread(%obj)
{
   if( !isObject( %obj ) || %obj.exploded )			// if(!isObject(%obj) && (!isObject(%obj.sourceObject) || !isObject(%obj.bkSourceObject))) -soph
        return;

      if(%obj.bkSourceObject)
         if(isObject(%obj.bkSourceObject))
            %obj.sourceObject = %obj.bkSourceObject;

//      if(!%obj.lastBlinkPos)					// -soph
//         %obj.lastBlinkPos = "999 999 999";			// -soph

      if( !( %obj.damageMod > 0 ) )				// +soph
         %obj.damageMod = 1 ;					// +soph

      %pos = %obj.position;

//      if(vectorCompare(%pos, %obj.lastBlinkPos))		// -soph
//         return;						// -soph

     %muzzlePos = %obj.position;
     %obj.lastBlinkPos = %muzzlePos;
//     %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType; // | $TypeMasks::ItemObjectType;
     %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |	// +[soph]
		    $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |	// +
		    $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |	// +
		    $TypeMasks::DamagableItemObjectType ;				// +[/soph]

     if(!isObject(%obj.reflectorObj))
     {
          %obj.reflectorObj = createReflectorForObject(%obj);
          %obj.reflectorObj.team = %obj.sourceObject.team;
     }

     %FFRObject = %obj.reflectorObj;
     %FFRObject.setPosition(%muzzlePos);

     %time = getSimTime() ;								// +soph

     InitContainerRadiusSearch(%obj.position, 15, %damageMasks);

     while((%target = containerSearchNext()) != 0)
     {
//          if(getRandom(100) < 50)
//               continue;

//          if(%target == %obj.sourceObject)
//               continue;

          if(%target.team == %obj.sourceObject.team)
               continue;

//          %tgtPos = %target.getType() & $TypeMasks::ItemObjectType ? %target.position : %target.getWorldBoxCenter();
          %tgtPos = %target.getWorldBoxCenter();

          %hit = ContainerRayCast(%obj.position, %tgtPos, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::ForceFieldObjectType, %obj);

          if(%hit)
               continue;
          else
          {
               %vector = VectorNormalize(VectorSub(%tgtPos, %obj.position));

               %p = new ShockLanceProjectile()
               {
                    dataBlock        = BasicShocker2;
                    initialDirection = %vector;
                    initialPosition  = %obj.position;
                    sourceObject     = %FFRObject;
                    targetId         = %target;
                    sourceslot       = 0;
                    bkSourceObject   = %obj.sourceObject;
               };
//               %target.mbr_lanceProj.schedule(250, delete);
             %p.sourceObject = %obj.sourceObject;

             %target.play3D(ShockLanceHitSound);
             %targetData = %target.getDatablock();

             // This should not be necessary, stupid T2..
//             if(%targetData.isBlastech)									// -[soph]
//                  %targetData.damageObject(%target, %obj.sourceObject, %target.position, ( 0.3 * %obj.damageMod ) , $DamageType::ShockCannon);	// %damageMod +soph
//             else												// -[/soph]
                  %targetData.damageObject(%target, %obj.sourceObject, %target.position, ( 0.20 * %obj.damageMod ) , $DamageType::ShockCannon);		// ditto +soph

             %target.SBLance_time = %time ;
             schedule ( 150 , %target , SBLanceChain , %obj.sourceObject , %target , %obj.damageMod * 0.75 ) ;	// +soph
             %obj.damageMod *= 0.925 ;										// +soph
          }
     }

   schedule( %interval , %obj , SBLanceRandomThread , %obj ) ;							// schedule( %time, %obj, SBLanceRandomThread, %obj); -soph
}

function SBLanceChain( %sourceObj , %bounceObj , %damageMod )			// +[soph]
{										// +
	if( !isObject( %sourceObj ) )						// +
		return ;							// +
										// +
	%time = getSimTime() ;							// +
										// +
	%damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
		       $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
		       $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |
		       $TypeMasks::DamagableItemObjectType ;			// +
										// +
	%initialPosition = %bounceObj.getWorldBoxCenter() ;			// +
										// +
	InitContainerRadiusSearch( %initialPosition , 30 , %damageMasks ) ;	// +
	while( ( %target = containerSearchNext() ) != 0 )			// +
	{									// +
		if( %target.SBLance_time > %time - 50 )				// +
			continue ;						// +
		%targetPosition = %target.getWorldBoxCenter() ;			// +
		%obstacle = ContainerRayCast( %initialPosition , %targetPosition , $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::VehicleObjectType , %bounceObj ) ;
		%obstacle = getWord( %obstacle , 0 ) ;				// +
		if( %obstacle != %target )					// +
                	continue ;						// +
		else 								// +
		{								// +
			%vector = VectorSub( %initialPosition , %targetPosition ) ;
			%p = new ShockLanceProjectile()				// +
			{							// +
				dataBlock        = BasicShocker2 ;		// +
				initialDirection = %vector ;			// +
				initialPosition  = %initialPosition ;		// +
				sourceObject     = %bounceObj ;			// +
				targetId         = %target ;			// +
				sourceslot       = 1 ;				// +
				bkSourceObject   = %sourceProj.sourceObject ;	// +
			} ;							// +
			MissionCleanup.add( %p ) ;				// +
			%target.play3D( ShockLanceHitSound ) ;			// +
			%target.getDatablock().damageObject( %target , %sourceObj , %targetPosition , ( 0.2 * %damageMod ) , $DamageType::EMP ) ;
			%target.SBLance_time = %time ;				// +
			%damageMod *= 0.75 ;					// +
			if( %damageMod > 0.07 )					// +
				schedule ( 100 , 0 , SBLanceChain , %sourceObj , %target , %damageMod ) ;
			%zapSuccess = true ;					// +
			break ;							// +
		}								// +
	}									// +
	if( %zapSuccess || ( %bounceObj.getType() & $TypeMasks::PlayerObjectType && !%bounceObj.isTacticalMech ) )
		return ;							// +
	else									// +
	{									// +
		%bounceObj.SBLance_time = %time ;				// +
		%damageMod *= 0.825 ;						// +
		%bounceObj.getDataBlock().damageObject( %bounceObj , %sourceObj , %initialPosition , ( 0.1 * %damageMod ) , $DamageType::EMP ) ;
		if( %damageMod > 0.07 )						// +
			schedule ( 65 , 0 , SBLanceChain , %sourceObj , %bounceObj , %damageMod ) ;
	}									// +
}										// +[/soph]

function ShockBlasterBolt::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{									// +
	%proj.damageMod *= 5 ;						// +
        schedule( 1 , 0 , SBLanceRandomThread , %proj , 32 ) ;		// +
        %proj.damageMod /= 5 ;						// +
        Parent::onExplode( %data , %proj , %pos , %mod ) ;		// +
	%proj.exploded = true ;						// +
}									// +[/soph]