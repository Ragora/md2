// Mage Ion
// 

function mageIonProcessFire(%data, %obj, %slot, %enUse, %soulUse, %projType, %proj, %timeout, %damageMod, %fireSound, %reloadSound, %failSound, %spread, %script)
{
     %time = getSimTime();

     if(%obj.mageFireTimeout[%data] > %time)
          return 0;

     if(%spread $= "")
          %spread = 0;
          
     %energy = %obj.getEnergyLevel();
     
     if(%obj.powerRecirculator)
          %enUse *= 0.75;
     
     if(%enUse <= %energy && %soulUse <= %obj.soulCount)
     {
          %vector = %obj.getMuzzleVector(%slot);
          
          if(%spread)
               %vector = calcSpreadVector(%vector, %spread);	// fix +soph
          
          %mod = 0.0;
          %p = 0;
          
          if(%proj !$= "" && %projType !$= "")
          {
               %dmod = 1;
               
               if(%obj.soulShotActive)
               {
                    %dmod++;
                    %obj.soulShotActive = false;
                    deactivateDeploySensor(%obj);
               }
               
               %useEnergyObj = %obj.getObjectMount();
     
               if(!%useEnergyObj)
                    %useEnergyObj = %obj;
     
               %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;
     
               // Vehicle Damage Modifier
               if(%vehicle)
                    %vehicleMod = %vehicle.damageMod;
     
               if(%obj.damageMod)
                    %damagemod *= %obj.damageMod;	// %dmod = %obj.damageMod; -soph
               
               if( %damageMod >= 0 )			// if(%damageMod) -soph
                    %dmod *= ( %damageMod );		// %dmod += %damageMod - 1; -soph
     
               %p = new (%projType)()
               {
                    dataBlock = %proj;
                    initialDirection = %vector;
                    initialPosition  = %obj.getMuzzlePoint(%slot);
                    sourceObject     = %obj;
                    sourceSlot       = %slot;
                    detectScript     = %script;
                    damageMod        = %dmod; 
                    vehicleMod       = %vehicleMod;
               };
               
               MissionCleanup.add(%p);
          }
          else
          {
               %mod = 1.0;

               if(%obj.soulShotActive)
               {
                    %mod++;
                    %obj.soulShotActive = false;
                    deactivateDeploySensor(%obj);
               }
              
               %useEnergyObj = %obj.getObjectMount();
     
               if(!%useEnergyObj)
                    %useEnergyObj = %obj;
     
               %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;
     
               // Vehicle Damage Modifier
               if(%vehicle)
                    %mod *= %vehicle.damageMod;
     
               if(%obj.damageMod)
                    %mod *= %obj.damageMod;
               
               if(%damageMod)
                    %mod *= %damageMod;
          }
          
          %obj.play3D(%fireSound); 
          %obj.setEnergyLevel(%energy - %enUse);
          
          if(%obj.efficiencyMod && %obj.maxSoulPassive )		// if(%obj.efficiencyMod)
          {								// soul bug fix +soph
               if( getRandom() > ( %obj.soulCount * 0.025 ) )		// if(getRandom() > 0.25)
                    %obj.soulCount -= %soulUse;
          }								// soul bug fix +soph
          else
                    %obj.soulCount -= %soulUse;
                    
          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);
          calculateSoulBonuses(%obj, %obj.soulCount);          

          if(%timeout > 0)
          {
               %obj.mageFireTimeout[%data] = %time + %timeout;
               if( %reloadSound )	// +soph
                    %obj.schedule(%timeout, play3D, %reloadSound);
          }
                    
          if(%mod)
               return %mod;
          else
               return %p;
     }
     else
     {
          %obj.play3D(%failSound);
          %obj.mageFireTimeout[%data] = %time + (%timeout / 2);
          return 0;
     }
}

function mageIonProcessRequire(%obj, %enUse, %soulUse, %failSound)
{
     %time = getSimTime();

     if(%obj.mageFireTimeout > %time)
          return 0;

     if(%obj.meditateTrigger)
     {
          %obj.play3D(%failSound);
          return 0;
     }
          
     if(%spread $= "")
          %spread = 0;
          
     %energy = %obj.getEnergyLevel();
     
     if(%enUse <= %energy && %soulUse <= %obj.soulCount)
          return true;
     else
     {
          %obj.play3D(%failSound);
          return false;
     }
}

function mageIonSetTimeout(%obj, %timeout, %reloadSound)
{
     %obj.mageFireTimeout = getSimTime() + %timeout;
     %obj.schedule(%timeout, play3D, %reloadSound);
}

//----------------------------------------------------------------------------
datablock ItemData(MagicMissile)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_plasma.dts";
   image = MagicMissileImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "Soul Missile Launcher";
   emap = true;
};

datablock ShapeBaseImageData(MagicMissileImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   offset = "0.0 -0.20 -0.20";	// +soph
   armThread = lookms;		// +soph
   item = MagicMissile;
   emap = false;

   usesEnergy = true;
   minEnergy = -1;

   isSeeker     = true;
   seekRadius   = 300; // 600
   maxSeekAngle = 8;
   seekTime     = 0.5;
   minSeekHeat  = 0.7;

   // only target objects outside this range
   minTargetingDistance             = 5;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Ready";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";

   stateName[1] = "Ready";
   stateTransitionOnTriggerDown[1] = "Fire";

   stateName[2] = "Fire";
   stateTransitionOnTimeout[2] = "Reload";
   stateTimeoutValue[2] = 0.04;
   stateScript[2] = "onFire";

   stateName[3] = "Reload";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.01;
};

function MagicMissileImage::onMount(%data, %obj, %slot)
{
     Parent::onMount(%data, %obj, %slot);

     %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
     commandToClient(%obj.client, 'setAmmoHudCount', %graph);

     %obj.play3D(ChaingunSwitchSound);
     
     %obj.client.setWeaponsHudActive("MissileLauncher");     
}

function MagicMissileImage::onUnmount(%data, %obj, %slot)
{
     Parent::onUnmount(%data, %obj, %slot);
}

function MagicMissileImage::onFire(%data, %obj, %slot)
{
     %rank = %obj.client.mode[%obj.getMountedImage(0).item] + 1;
               
     //%p = mageIonProcessFire(%data, %obj, %slot, 10 * %rank, 1, SeekerProjectile, MagicMissileProj, 2000, %rank, "MagMissileLauncherFireSound", "MissileReloadSound", "MissileDryFireSound");
     switch$ ( %rank )	//[soph]
     {
          case 1:
               %p = mageIonProcessFire(%data, %obj, %slot, 40, 0, SeekerProjectile, MagicMissileProj, 2000, 2, "MagMissileLauncherFireSound", "MissileReloadSound", "MissileDryFireSound");
          case 2:
               %p = mageIonProcessFire(%data, %obj, %slot, 50, 1, SeekerProjectile, MagicMissileProj, 2000, 7, "MagMissileLauncherFireSound", "MissileReloadSound", "MissileDryFireSound");
          case 3:
               %p = mageIonProcessFire(%data, %obj, %slot, 60, 2, SeekerProjectile, MagicMissileProj, 2000, 15, "MagMissileLauncherFireSound", "MissileReloadSound", "MissileDryFireSound");
          case 4:
               if ( %obj.soulShotActive )
                    %bonus = 2;
               else
                    %bonus = 1;
               %p = mageIonProcessFire(%data, %obj, %slot, 13, 1, SeekerProjectile, MagicMissileProj, 2000, 1, "MagMissileLauncherFireSound", "", "MissileDryFireSound");
               if ( %p )
                    schedule(300 , 0 , MagicMissileLoop , %data , %obj , %slot , %obj.getMountedImage(0) , %bonus , 1 );
     }
             if(%p)
             {
                  %target = %obj.getLockedTarget();
               
                  if(%target)
                  {
                     if(%target.getDatablock().jetfire)
                        assignTrackerTo(%target, %p);
                     else
                        %p.setObjectTarget(%target);
                  }
                  else if(%obj.isLocked())
                     %p.setPositionTarget(%obj.getLockedPosition());
     //             else
     //                %p.setNoTarget();          
             }
}

function MagicMissileLoop( %data , %obj , %slot , %weaponImage , %bonus , %num )	// [soph]
{
	if ( %obj.getMountedImage(0) == %weaponImage )
	{
		%obj.mageFireTimeout[%data] = getSimTime();
		%p = mageIonProcessFire(%data, %obj, %slot, 15 + %num, 0, SeekerProjectile, MagicMissileProj, 2000, %bonus, "MagMissileLauncherFireSound", "", "MissileDryFireSound");
		if ( %p )
		{
			%target = %obj.getLockedTarget();
			if(%target)							// duplicated from above
			{
				if(%target.getDatablock().jetfire)
					assignTrackerTo(%target, %p);
				else
					%p.setObjectTarget(%target);
			}
			else 
				if(%obj.isLocked())
					%p.setPositionTarget(%obj.getLockedPosition());
			schedule(300 , 0 , MagicMissileLoop , %data , %obj , %slot , %weaponImage , %bonus , %num++ );
		}
		else
		{
			%obj.mageFireTimeout[%data] = getSimTime() + 1700;
			%obj.schedule( 1700 , play3D , "MissileReloadSound" );
		}
	}
}											// [/soph]

//----------------------------------------------------------------------------
datablock ItemData(MitziDeathRay)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_sniper.dts";
   image = MitziDeathRayImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "Mitzi Blaster Rifle";
   emap = true;
};

datablock ShapeBaseImageData(MitziDeathRayImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_sniper.dts";
   item = MitziDeathRay;
   emap = false;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Ready";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";

   stateName[1] = "Ready";
   stateTransitionOnTriggerDown[1] = "Fire";

   stateName[2] = "Fire";
   stateTransitionOnTimeout[2] = "Reload";
   stateTimeoutValue[2] = 0.04;
   stateScript[2] = "onFire";

   stateName[3] = "Reload";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.01;
};

datablock AudioProfile(DeathRayFireSound)
{
   filename    = "fx/misc/gridjump.wav";
   description = AudioDefault3d;
   preload = true;
};

function MitziDeathRayImage::onMount(%data, %obj, %slot)
{
     Parent::onMount(%data, %obj, %slot);
     
     %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
     commandToClient(%obj.client, 'setAmmoHudCount', %graph);

     %obj.play3D(SniperRifleSwitchSound);
          
     %obj.client.setWeaponsHudActive("GrenadeLauncher");     
}

function MitziDeathRayImage::onUnmount(%data, %obj, %slot)
{
     Parent::onUnmount(%data, %obj, %slot);
}

function MitziDeathRayImage::onFire(%data, %obj, %slot)
{
//     %p = mageIonProcessFire(%data, %obj, %slot, 45, 0, SniperProjectile, MageDetectBeam200, 1500, 1, "DeathRayFireSound", "SynomiumEngage", "ShockLanceDryFireSound", 0, "MitziDeathRayAttack");			// -soph
	%p = mageIonProcessFire( %data , %obj , %slot , 40 , 0 , SniperProjectile , MageDetectBeam200 , 1750 , 1 , "DeathRayFireSound" , "SynomiumEngage" , "ShockLanceDryFireSound" , 0 , "MitziDeathRayAttack" ) ;	// +soph

     if(%p)
     {
          %t = new TargetProjectile()
          {
               dataBlock        = "MitziDeathRayFX";
               initialDirection = %obj.getMuzzleVector(%slot);
               initialPosition  = %obj.getMuzzlePoint(%slot);
               sourceObject     = %obj;
               sourceSlot       = %slot;
               vehicleObject    = 0;
          };
     
          MissionCleanup.add(%t);
          %t.schedule(450, delete);
     }
}

function MitziDeathRayAttack(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
//   %damageAmount = %projectile.damageFactor;				// -[soph]
									// -
//   if(%projectile.bkSourceObject)					// - does nothing
//      if(isObject(%projectile.bkSourceObject))			// -
//         %projectile.sourceObject = %projectile.bkSourceObject;	// -[/soph]

   %dist = vectorDist(%projectile.sourceObject.getWorldBoxCenter(), %position);
   %falloff = 100;
   %minMod = 0.25;
   %mod = 1.0;
   
   if(%dist > %falloff)
   {
     %mod = 1 - ((%dist - %falloff) / (%data.maxRifleRange - %falloff));
     %mod = %mod < %minMod ? %minMod : %mod;
   }
   
   %modifier = 1;

   checkMAHit(%projectile, %targetObject, %position);

   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);
      
   if(%targetObject.getType() & $TypeMasks::PlayerObjectType)
        %targetObject.getOwnerClient().headShot = 0;   
   
   %amount = 0.5 * %mod ;						// (getRandom(70, 90) / 100) * %mod; -soph
   %modifier = %projectile.damageMod;
   
   serverPlay3D("TelePadBeamSound", %position);
   
   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %amount * %modifier, $DamageType::MB);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %amount * %modifier, $DamageType::MB);
}

//----------------------------------------------------------------------------
datablock ItemData(SoulShocker)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_grenade_launcher.dts";
   image = SoulShockerImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "Soul Shocker";
   emap = true;
};

datablock ShapeBaseImageData(SoulShockerImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_grenade_launcher.dts";
   offset = "0.0 -0.20 -0.20";	// +soph
   armThread = lookms;		// +soph
   item = SoulShocker;
   emap = false;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Ready";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";

   stateName[1] = "Ready";
   stateTransitionOnTriggerDown[1] = "Fire";

   stateName[2] = "Fire";
   stateTransitionOnTimeout[2] = "Reload";
   stateTimeoutValue[2] = 0.04;
   stateScript[2] = "onFire";

   stateName[3] = "Reload";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.01;
};

function SoulShockerImage::onMount(%data, %obj, %slot)
{
     Parent::onMount(%data, %obj, %slot);

     %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
     commandToClient(%obj.client, 'setAmmoHudCount', %graph);

     %obj.play3D(MortarSwitchSound);
          
     %obj.client.setWeaponsHudActive("Mortar");
}

function SoulShockerImage::onUnmount(%data, %obj, %slot)
{
     Parent::onUnmount(%data, %obj, %slot);
}

function SoulShockerImage::onFire(%data, %obj, %slot)
{
     %rank = %obj.client.mode[%obj.getMountedImage(0).item];	// +[soph]
     %damageMod = %obj.damageMod;				// + if the ion gets a turbo
     if( !( %damageMod > 0 ) )					// +
          %damageMod = 1 ;					// +
     if( %obj.soulShotActive )					// +
     {								// +
          %damageMod *= 2 ;					// +
          %soulShot = true ;					// +
     }								// +
     switch$ ( %rank )						// +
     {								// +
          case "1" :						// +
               %p = mageIonProcessFire( %data , %obj , %slot , 0 , 5 , LinearFlareProjectile , ShockBlasterBolt , 0 , 1 , "PBLFireSound" , "ShocklanceReloadSound" , "ShocklanceDryFireSound" , 12 ) ;
								// +
               if( %p )						// +
               {						// +
                    %p.lanceThread = true ;			// +
                    %p.damageMod = %damageMod ;			// +
                    SBLanceRandomThread( %p , 90 ) ;		// +
								// +
                    if( %soulShot )				// +
                         %obj.soulShotActive = true ;		// +
                    %p = mageIonProcessFire( %data , %obj , %slot , 0 , 0 , LinearFlareProjectile , ShockBlasterBolt , 4000 , 1 , "PBLFireSound" , "ShocklanceReloadSound" , "ShocklanceDryFireSound" , 12 ) ;
                    %p.lanceThread = true ;			// +
                    %p.damageMod = %damageMod ;			// +
                    SBLanceRandomThread( %p , 90 ) ;		// +
								// +
                    %obj.play3D( BlasterFireSound ) ;		// +
               }						// +
          case "2" :						// +
               if( %obj.soulShotActive )			// +
                    %soulShot = true ;				// +
               %p = mageIonProcessFire( %data , %obj , %slot , 0 , 6 , LinearFlareProjectile , ShockBlasterBolt , 0 , 1 , "PBLFireSound" , "ShocklanceReloadSound" , "ShocklanceDryFireSound" , 24 ) ;
								// +
               if( %p )						// +
               {						// +
                    %p.lanceThread = true ;			// +
                    %p.damageMod = %damageMod ;			// +
                    SBLanceRandomThread( %p , 90 ) ;		// +
								// +
                    if( %soulShot )				// +
                         %obj.soulShotActive = true ;		// +
                    %p = mageIonProcessFire( %data , %obj , %slot , 0 , 0 , LinearFlareProjectile , ShockBlasterBolt , 0 , 1 , "PBLFireSound" , "ShocklanceReloadSound" , "ShocklanceDryFireSound" , 24 ) ;
                    %p.lanceThread = true ;			// +
                    %p.damageMod = %damageMod ;			// +
                    SBLanceRandomThread( %p , 90 ) ;		// +
								// +
                    if( %soulShot )				// +
                         %obj.soulShotActive = true ;		// +
                    %p = mageIonProcessFire( %data , %obj , %slot , 0 , 0 , LinearFlareProjectile , ShockBlasterBolt , 4000 , 1 , "PBLFireSound" , "ShocklanceReloadSound" , "ShocklanceDryFireSound" , 24 ) ;
                    %p.lanceThread = true ;			// +
                    %p.damageMod = %damageMod ;			// +
                    SBLanceRandomThread( %p , 90 ) ;		// +
								// +
                    %obj.play3D( BlasterFireSound ) ;		// +
               }						// +
          default :						// +[/soph]
               %p = mageIonProcessFire(%data, %obj, %slot, 0, 3, LinearFlareProjectile, ShockBlasterBolt, 4000, 1, "PBLFireSound", "ShocklanceReloadSound", "ShocklanceDryFireSound");

               if(%p)
               {
                    %p.lanceThread = true;
     
                    SBLanceRandomThread(%p , 90 ) ;		// SBLanceRandomThread(%p); -soph
                    %p.damageMod = %damageMod ;			// +soph
                    %obj.play3D(BlasterFireSound);      
               }
     }
}

//----------------------------------------------------------------------------
datablock ItemData(SoulComet)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_energy.dts";
   image = SoulCometImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "Soul Comet";
   emap = true;
};

datablock ShapeBaseImageData(SoulCometImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "0.0 -0.20 -0.20";	// +soph
   armThread = lookms;		// +soph
   item = SoulComet;
   emap = false;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Ready";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";

   stateName[1] = "Ready";
   stateTransitionOnTriggerDown[1] = "Fire";

   stateName[2] = "Fire";
   stateTransitionOnTimeout[2] = "Reload";
   stateTimeoutValue[2] = 0.04;
   stateScript[2] = "onFire";

   stateName[3] = "Reload";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.01;
};

function SoulCometImage::onMount(%data, %obj, %slot)
{
     Parent::onMount(%data, %obj, %slot);

     %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
     commandToClient(%obj.client, 'setAmmoHudCount', %graph);

     %obj.play3D(ChaingunSwitchSound);
          
     %obj.client.setWeaponsHudActive("Blaster");          
}

function SoulCometImage::onUnmount(%data, %obj, %slot)
{
     Parent::onUnmount(%data, %obj, %slot);
}

function SoulCometImage::onFire(%data, %obj, %slot)
{
     %rank = %obj.client.mode[%obj.getMountedImage(0).item] + 1;
     if( %rank > 1 )										// +[soph]
     {												// +
          if( %obj.soulShotActive )								// +
               %soulShot = true ;								// +
          %p = mageIonProcessFire( %data , %obj , %slot , 0 , 3 , LinearFlareProjectile , CometCannonBlast , 0 , 1 , "AAFireSound" , "PBWSwitchSound" , "PlasmaDryFireSound" , 1) ;
												// +
          if(%p)										// +
          {											// +
               spawnProjectile( %p , LinearFlareProjectile , CCDisplayCharge ) ;		// +
               projectileTrail( %p , 150 , LinearFlareProjectile , GaussTrailCharge , true ) ;	// +
												// +
               if( %soulShot )									// +
                    %obj.soulShotActive = true ;						// +
               %p = mageIonProcessFire( %data , %obj , %slot , 0 , 0 , LinearFlareProjectile , CometCannonBlast , 2000 , 1 , "AAFireSound" , "PBWSwitchSound" , "PlasmaDryFireSound" , 11 ) ;     
												// +
               spawnProjectile( %p , LinearFlareProjectile , CCDisplayCharge ) ;		// +
               projectileTrail( %p , 150 , LinearFlareProjectile , GaussTrailCharge , true ) ;	// +
          }											// +
     }												// +
     else											// +
     {												// +[/soph]
          %p = mageIonProcessFire(%data, %obj, %slot, 0, 2, LinearFlareProjectile, CometCannonBlast, 2000, 1, "AAFireSound", "PBWSwitchSound", "PlasmaDryFireSound");     
     
          if(%p)
          {
               spawnProjectile(%p, LinearFlareProjectile, CCDisplayCharge);
               projectileTrail(%p, 150, LinearFlareProjectile, GaussTrailCharge, true);
          }
     }
}

//----------------------------------------------------------------------------
datablock ItemData(SoulHammer)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = SoulHammerImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "Soul Hammer";
   emap = true;
};

datablock ShapeBaseImageData(SoulHammerImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   offset = "0.0 -0.20 -0.20";	// +soph
   armThread = lookms;		// +soph
   item = SoulHammer;
   emap = false;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Ready";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";

   stateName[1] = "Ready";
   stateTransitionOnTriggerDown[1] = "Fire";

   stateName[2] = "Fire";
   stateTransitionOnTimeout[2] = "Reload";
   stateTimeoutValue[2] = 0.04;
   stateScript[2] = "onFire";

   stateName[3] = "Reload";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.01;
};

function SoulHammerImage::onMount(%data, %obj, %slot)
{
     Parent::onMount(%data, %obj, %slot);

     %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
     commandToClient(%obj.client, 'setAmmoHudCount', %graph);

     %obj.play3D(MortarSwitchSound);
     
     %obj.client.setWeaponsHudActive("MissileLauncher");
}

function SoulHammerImage::onUnmount(%data, %obj, %slot)
{
     Parent::onUnmount(%data, %obj, %slot);
}

function SoulHammerImage::onFire(%data, %obj, %slot)
{
     %rank = %obj.client.mode[%obj.getMountedImage(0).item] + 1;
	if( %rank > 1 )										// +[soph]
	{											// +
		if( %obj.soulShotActive )							// +
			%soulShot = true ;							// +
		%p = mageIonProcessFire( %data , %obj , %slot , 0 , 10 , LinearProjectile , BHRocket , 0 , 1 , "StarHammerFireSound" , "StarHammerReloadSound" , "MortarDryFireSound" ) ;
		if(%p)										// +
		{										// +
			soulShove( %obj , 300 , 0.5 , 15000 , 25 ) ;				// +
			spawnProjectile( %p , LinearFlareProjectile , MBDisplayCharge ) ;	// +
			projectileTrail( %p , 100 , LinearFlareProjectile , ReaverCharge , true ) ;
			for( %i = 1 ; %i < 20 ; %i++ )						// +
			{									// +
				if( %soulShot )							// +
					%obj.soulShotActive = true ;				// +
				%p = mageIonProcessFire( %data , %obj , %slot , 0 , 0 , LinearProjectile , BHRocket , 0 , 1 , "StarHammerFireSound" , "StarHammerReloadSound" , "MortarDryFireSound" , 15 * %i ) ;     
				spawnProjectile( %p , LinearFlareProjectile , MBDisplayCharge ) ;										// +
				schedule( %i % 4 * 32 , 0 , projectileTrail , %p , 100 , LinearFlareProjectile , ReaverCharge , false ) ;
			}									// +
			mageIonSetTimeout( %obj , 3000 , "" ) ;					// +
			DaiSelfDestruct( %obj , %obj , %p.getDatablock().radiusDamageType ) ;	// +
		}										// +
	}											// +
	else											// +
	{											// +[/soph]
          %p = mageIonProcessFire(%data, %obj, %slot, 0, 2, LinearProjectile, SBRocket, 3000, 1, "StarHammerFireSound", "StarHammerReloadSound", "MortarDryFireSound");     
     
          if(%p)
          {
               spawnProjectile(%p, LinearFlareProjectile, MBDisplayCharge);
               projectileTrail(%p, 64, LinearFlareProjectile, ReaverCharge, false);
          }
     }
}

function soulShove( %obj , %range , %spread , %force , %raw )	// +[soph]
{								// +
	%origin = %obj.getWorldBoxCenter() ;			// +
	%vector = %obj.getForwardVector() ;			// +
	InitContainerRadiusSearch( %origin , %range , $TypeMasks::PlayerObjectType	|
						      $TypeMasks::VehicleObjectType	|
						      $TypeMasks::CorpseObjectType	|
						      $TypeMasks::ItemObjectType	) ;
	while ( ( %targetObject = containerSearchNext() ) != 0 )
	{							// +
		if( !isObject( %targetObject ) )		// +
			continue ;				// +
								// +
		if( %targetObject.isMounted() )			// +
			continue ;				// +
								// +
		%distance = containerSearchCurrRadDamageDist() ;
		if ( %distance > %range )			// +
			continue ;				// +
								// +
		%targetPosition = %targetObject.getPosition() ;	// +
		%difference = VectorNormalize( VectorSub( %targetPosition , %origin ) ) ;
		%dot = VectorDot( %difference , %vector ) ;	// +
		%dot *= %dot ;					// +
		if( %dot <= %spread )				// + 
			continue ; 				// +
								// +
		%coverage = calcExplosionCoverage( %origin , %targetObject , $TypeMasks::InteriorObjectType	|
									     $TypeMasks::TerrainObjectType	|
									     $TypeMasks::ForceFieldObjectType	|
									     $TypeMasks::StaticShapeObjectType	|
									     $TypeMasks::VehicleObjectType	) ;
		if( %coverage == 0 )				// +
			continue ;				// +
								// +
		if( ( %impulse && %data.shouldApplyImpulse( %targetObject ) ) || %type & $TypeMasks::VehicleObjectType )
		{						// +
			%pushVector = VectorNormalize( VectorSub( %targetPosition , %origin ) ) ;
			%kick = %force + %raw * %obj.getDataBlock().mass ;
			%modifier = %distance / %range ;	// +
			%reduction = ( %dot - %spread ) / ( 1 - %spread ) ;
			%push = %kick * %modifier ;		// +
			%kick -= %push ;			// +
			%push *= %reduction ;			// +
			%push += %kick ;			// +
			%impulse = VectorScale( %pushVector , %push * ( 1.0 - %modifier ) ) ;
			if( !%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive )
				%targetObject.applyImpulse( %origin , %impulse ) ;
		}						// +
	}							// +
}								// +[/soph]

//----------------------------------------------------------------------------
datablock ItemData(SoulStrike)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_shocklance.dts";
   image = SoulStrikeImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "Soul Swarmer";
   emap = true;
};

datablock ShapeBaseImageData(SoulStrikeImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_shocklance.dts";
   offset = "0.0 -0.20 -0.20";	// +soph
   armThread = lookms;		// +soph
   item = SoulStrike;
   emap = false;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Ready";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";

   stateName[1] = "Ready";
   stateTransitionOnTriggerDown[1] = "Fire";

   stateName[2] = "Fire";
   stateTransitionOnTimeout[2] = "Reload";
   stateTimeoutValue[2] = 0.04;
   stateScript[2] = "onFire";

   stateName[3] = "Reload";
   stateTransitionOnTimeout[3] = "Ready";
   stateTimeoutValue[3] = 0.01;
};

function SoulStrikeImage::onMount(%data, %obj, %slot)
{
     Parent::onMount(%data, %obj, %slot);

     %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
     commandToClient(%obj.client, 'setAmmoHudCount', %graph);

     %obj.play3D(ShocklanceSwitchSound);
          
     %obj.client.setWeaponsHudActive("Mortar");     
}

function SoulStrikeImage::onUnmount(%data, %obj, %slot)
{
     Parent::onUnmount(%data, %obj, %slot);
}

function SoulStrikeImage::onFire(%data, %obj, %slot)
{
     %rank = %obj.client.mode[%obj.getMountedImage(0).item] + 1;
     
     if(%rank > 1)
     {
         // %timer = (%rank - 1) * 200;			// +soph
          %count = %obj.soulCount;
          if( %count )					// +[soph]
          {
//               mageIonProcessFire( %data, %obj, %slot, 0, %count, LinearFlareProjectile, SoulStrikeProj, 0, %count, "plasmaExpSound", "", "PlasmaFireWetSound" );
               for(%i = 0; %i < %count; %i++)
               {	
                    mageIonProcessFire( %data, %obj, %slot, 0, 1, LinearFlareProjectile, SoulStrikeProj, 0, 1, "plasmaExpSound", "", "PlasmaFireWetSound" );
//                    schedule(%timer, %obj, mageIonProcessFire, %data, %obj, %slot, 0, 1, LinearFlareProjectile, SoulStrikeProj, 0, 1, "plasmaExpSound", "", "PlasmaFireWetSound");
                   // schedule(%timer, %obj, mageIonProcessFire, %data, %obj, %slot, 0, 1, LinearFlareProjectile, SoulStrikeProj, 0, 1, "plasmaExpSound", "", "PlasmaFireWetSound");
               }
          }
          mageIonSetTimeout(%obj, 4000, "");
     }
     else
          mageIonProcessFire(%data, %obj, %slot, 0, 1, LinearFlareProjectile, SoulStrikeProj, 2000, 1, "plasmaExpSound", "", "PlasmaFireWetSound");
}

//----------------------------------------------------------------------------