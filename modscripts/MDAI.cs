if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();
   
//-----------------------------------------------------------------------------
// AI Modifications for Meltdown 2
//

// AI Chat

function initBotTalkThread()
{
     if(EngineBase.botTalkThread)
          return;

     EngineBase.botTalkThread = true;
          
     botTalkThread();
}

function botTalkThread()
{
     $AIDisableChat = $Host::DisableBotTalk;
     schedule(30000, 0, botTalkThread);
}

initBotTalkThread();

function addMultiRandomBot(%num)
{
   for(%i = 0; %i < %num; %i++)
      aiConnect("name", "", getRandomB()+1, "offense", "voice", "voicePitch");
}

function deleteAllBots() // keen - doesn't work right for some odd reason...
{
     for(%i = 0; %i < ClientGroup.getCount(); %i++)
	{
	    %client = ClientGroup.getObject(%i);

          if(%client.isAIControlled())
          {
//               %client.schedule(500, delete);
//               %client.drop();

               cancel(%client.respawnThread);	// +[soph]
               cancel(%client.objectiveThread);	// just throwing random crap here to see what sticks
	       AIUnassignClient(%client);	// +[/soph]

               %client.player.scriptKill(0);
               %client.setDisconnectReason("bot");
               %client.schedule(500, delete);
          }
     }
}

// Name generator for bots
exec("scripts/modscripts/MDAINameGen.cs");

function AIConnection::onAIConnect(%client, %name, %team, %skill, %offense, %voice, %voicePitch)
{
   if(%skill $= "")
      %skill = getRandom();

   %name = AIGenerateRandomName();

   // Sex/Race defaults
   %client.race = getRandomB() ? "Human" : "Bioderm";

   if(%client.race $= "Human")
        %client.sex = getRandomB() ? "Male" : "Female";
   else
        %client.sex = "Male";

   %client.armor = getRandomB() ? "Light" : "Medium";

   if(%client.race $= "Human" && %client.sex $= "Male")
        %voice = getRandomB() ? "Bot1" : "Male"@getRandom(1,5);
   else if(%client.race $= "Human" && %client.sex $= "Female")
        %voice = "Fem"@getRandom(1,5);
   else
        %voice = "Derm"@getRandom(1,3);

   %client.voice = %voice;
   %client.voiceTag = addTaggedString(%voice);

//   if (%voicePitch $= "" || %voicePitch < 0.5 || %voicePitch > 2.0)
//      %voicePitch = 1.0;
   %voicePitch = 1 - ((getRandom(20) - 10)/100);
   %client.voicePitch = %voicePitch;

   %client.name = addTaggedString( "\cp\c9" @ %name @ "\co" );
   %client.nameBase = %name;

   echo(%client.name);
   echo("CADD: " @ %client @ " " @ %client.getAddress());
   $HostGamePlayerCount++;

   //set the initial team - Game.assignClientTeam() should be called later on...
   %client.team = %team;

   if(%voice $= "Bot1")
        %skin = %client.team == 1 ? "basebot" : "basebbot";
   else if(%client.race $= "Human")
        %skin = %client.team == 1 ? "beagle" : "swolf";
   else
        %skin = %client.team == 1 ? "base" : "baseb";
        
//   if ( %client.team & 1 )
   %client.skin = addTaggedString(%skin);

	//setup the target for use with the sensor net, etc...
   %client.target = allocClientTarget(%client, %client.name, %client.skin, %client.voiceTag, '_ClientConnection', 0, 0, %client.voicePitch);

   if($currentMissionType $= "SinglePlayer")
		messageAllExcept(%client, -1, 'MsgClientJoin', "", %name, %client, %client.target, true);
   else
	   messageAllExcept(%client, -1, 'MsgClientJoin', '\c1%1 joined the game.', %name, %client, %client.target, true);

	//assign the skill
	%client.setSkillLevel(%skill);

	//assign the affinity
   %client.offense = getRandomB() == 1 ? true : false;

   //clear any flags
   %client.stop(); // this will clear the players move state
   %client.clearStep();
   %client.lastDamageClient = -1;
   %client.lastDamageTurret = -1;
   %client.setEngageTarget(-1);
   %client.setTargetObject(-1);
   %client.objective = "";

	//clear the defaulttasks flag
	%client.defaultTasksAdded = false;

	//if the mission is already running, spawn the bot
   if ($missionRunning)
      %client.startMission();
}

function DefaultGame::assignClientTeam(%game, %client, %respawn )
{
//error("DefaultGame::assignClientTeam");
   // this function is overwritten in non-team mission types (e.g. DM)
   // so these lines won't do anything
   //if(!%game.numTeams)
   //{
   //   setTargetSkin(%client.target, %client.skin);
   //   return;
   //}

   //  camera is responsible for creating a player
   //  - counts the number of players per team
   //  - puts this player on the least player count team
   //  - sets the client's skin to the servers default

   %numPlayers = ClientGroup.getCount();
   for(%i = 0; %i <= %game.numTeams; %i++)
      %numTeamPlayers[%i] = 0;

   for(%i = 0; %i < %numPlayers; %i = %i + 1)
   {
      %cl = ClientGroup.getObject(%i);
      if(%cl != %client)
         %numTeamPlayers[%cl.team]++;
   }
   %leastPlayers = %numTeamPlayers[1];
   %leastTeam = 1;
   for(%i = 2; %i <= %game.numTeams; %i++)
   {
      if( (%numTeamPlayers[%i] < %leastPlayers) ||
         ( (%numTeamPlayers[%i] == %leastPlayers) &&
         ($teamScore[%i] < $teamScore[%leastTeam] ) ))
      {
         %leastTeam = %i;
         %leastPlayers = %numTeamPlayers[%i];
      }
   }

   %client.team = %leastTeam;
   %client.lastTeam = %team;

   // Assign the team skin: - disabled here so bots don't get weird skins on mission change - keen
//   if ( %client.isAIControlled() )
//   {
//      if ( %leastTeam & 1 )
//      {
//         %client.skin = addTaggedString( "basebot" );
//         setTargetSkin( %client.target, 'basebot' );
//      }
//      else
//      {
//         %client.skin = addTaggedString( "basebbot" );
//         setTargetSkin( %client.target, 'basebbot' );
//      }
//   }
//   else
      setTargetSkin( %client.target, %game.getTeamSkin(%client.team) );
      //setTargetSkin( %client.target, %client.skin );

   // might as well standardize the messages
   //messageAllExcept( %client, -1, 'MsgClientJoinTeam', '\c1%1 joined %2.', %client.name, $teamName[%leastTeam], %client, %leastTeam );
   //messageClient( %client, 'MsgClientJoinTeam', '\c1You joined the %2 team.', $client.name, $teamName[%client.team], %client, %client.team );
   messageAllExcept( %client, -1, 'MsgClientJoinTeam', '\c1%1 joined %2.', %client.name, %game.getTeamName(%client.team), %client, %client.team );
   messageClient( %client, 'MsgClientJoinTeam', '\c1You joined the %2 team.', %client.name, %game.getTeamName(%client.team), %client, %client.team );

   updateCanListenState( %client );

   logEcho(%client.nameBase@" (cl "@%client@") joined team "@%client.team);
}

// Bot Inventory Mods
//------------------------------
//AI Inventory functions
//10 - 05 -03
 function AIUseInventoryTask::monitor(%task, %client)
{
	//make sure we still need equipment
	%player = %client.player;
	if (!isObject(%player))
		return;

	%damage = %player.getDamagePercent();
	%weaponry = AIEngageWeaponRating(%client);
	if (%damage < 0.3 && %weaponry >= 40 && !%client.spawnUseInv)
	{
		%task.buyInvTime = getSimTime();
		return;
	}
     // changed for DM defaults lay out..more usage
	//pick a random set based on armor...
	%randNum = getRandom( 24 ) ;	// getRandom(16); -soph
 //
	if (%randNum < 0)// 0.4)
		%buySet = "LightEnergyDefault";// MediumEnergySet HeavyEnergySet";
    else if (%randNum < 1)
        %buySet = "MediumEnergySet";
    else if (%randNum < 2)
         %buySet = "HeavyEnergySet";
	else if (%randNum < 3)// 0.6)
		%buySet = "LightShieldSet";// MediumShieldSet HeavyShieldSet";
    else if (%randNum < 4)// 0.6)
		%buySet = "MediumShieldSet";

    else if (%randNum < 5)// 0.6)
		%buySet = "HeavyShieldSet";
     else if (%randNum < 6)// 0.6)
		%buySet = "LightEnergyELF";
   else if (%randNum < 7)// 0.6)
		%buySet = "MediumRepairSet";
   else if (%randNum < 8)// 0.6)
		%buySet = "HeavyAmmoSet";
   else if (%randNum < 9)// 0.6)
		%buySet = "LightEnergySniper";
    else if (%randNum < 10)// 0.6)
		%buySet = "MediumEnergySet";
    else if (%randNum < 11)// 0.6)
		%buySet = "HeavyEnergySet";
     else if (%randNum < 12)// 0.6)
		%buySet = "MagIonEnergy";
     else if (%randNum < 13)// 0.6)
		%buySet = "BlastechSet";

   else if (%randNum < 14)// 0.6)
		%buySet = "LightCloakSet";
    else if (%randNum < 15)// 0.6)
		%buySet = "MediumMissileSet";

    else if (%randNum < 16)
		%buySet = "HeavyRepairSet";
    else if (%randNum < 17)
		%buySet = "HeavyIndoorTurretSet";
    else if (%randNum < 18)
		%buySet = "HeavyInventorySet";
    else if (%randNum < 19)
		%buySet = "MediumOutdoorTurretSet";
    else if (%randNum < 20)
		%buySet = "MediumIndoorTurretSet";
    else if (%randNum < 21)
		%buySet = "MediumInventorySet";
    else if (%randNum < 22)
		%buySet = "LightRepairSet";
    else if (%randNum < 23)
		%buySet = "BAAmmoSet";
    else
		%buySet = "LightSniperChain";

//   else if (%randNum < 0.8)
//		%buySet = "LightEnergyELF MediumRepairSet HeavyAmmoSet";
//   else
//      %buySet = "LightEnergySniper MediumEnergySet HeavyEnergySet";
      //echo(%randNum);
	//process the inv buying state machine
   %result = AIBuyInventory(%client, "", %buySet, %task.buyInvTime);

   //if we succeeded, reset the spawn flag
   if (%result $= "Finished")
      %client.spawnUseInv = false;

   //if we succeeded or failed, reset the state machine...
	if (%result !$= "InProgress")
		%task.buyInvTime = getSimTime();


	//this call works in conjunction with AIEngageTask
	%client.setEngageTarget(%client.shouldEngage);
}
$ObjectiveTable['AttackPlayer', type]               = "AIOAttackPlayer";
$ObjectiveTable['AttackPlayer', weightLevel1]       = 2500;
$ObjectiveTable['AttackPlayer', weightLevel2]       = 1000;
$ObjectiveTable['AttackPlayer', weightLevel3]       = 500;
$ObjectiveTable['AttackPlayer', weightLevel4]       = 100;
$ObjectiveTable['AttackPlayer', equipment]          = "";
$ObjectiveTable['AttackPlayer', desiredEquipment]   = "";
$ObjectiveTable['AttackPlayer', buyEquipmentSet]    = "MediumEnergySet";
$ObjectiveTable['AttackPlayer', offense]            = "true";
$ObjectiveTable['AttackPlayer', defense]            = "true";
$ObjectiveTable['AttackPlayer', assignTimeout]      = "5000";   // period allowed for humans to ack
$ObjectiveTable['AttackPlayer', maxAssigned]        = "2";      // not added if at least this many playrs have ack'd


$ObjectiveTable['TakeFlag', type]               = "AIOEscortPlayer";
$ObjectiveTable['TakeFlag', weightLevel1]       = 2500;
$ObjectiveTable['TakeFlag', weightLevel2]       = 1000;
$ObjectiveTable['TakeFlag', weightLevel3]       = 500;
$ObjectiveTable['TakeFlag', weightLevel4]       = 100;
$ObjectiveTable['TakeFlag', equipment]          = "";
$ObjectiveTable['TakeFlag', desiredEquipment]   = "MediumEnergySet";
$ObjectiveTable['TakeFlag', buyEquipmentSet]    = "";
$ObjectiveTable['TakeFlag', offense]            = "true";
$ObjectiveTable['TakeFlag', defense]            = "true";
$ObjectiveTable['TakeFlag', assignTimeout]      = "5000";   // period allowed for humans to ack
$ObjectiveTable['TakeFlag', maxAssigned]        = "1";	// "0"; -soph      // not added if at least this many play 

//-------
function AINeedEquipment(%equipmentList, %client)
{
   %index = 0;
   %item = getWord(%equipmentList, %index);

    //Start Edit here -Nite-
	//first, see if we're testing the armor class as well...
	if (%item $= "BattleAngel" || %item $= "Heavy" || %item $= "Medium" || %item $= "Engineer"   || %item $= "Light" || %item $= "MagIon" ||%item $= "Blastech" )
      {
     if (%client.player.getArmorSize() !$= %item)
			return true;
		%index++;
	   %item = getWord(%equipmentList, %index);
	}

   while (%item !$= "")
   {
      if( %item $= "or" )		// +[soph]
      {					// + previous item already cleared
         %index += 2 ;			// + skip next entry
         %item = getWord( %equipmentList , %index ) ;
         continue ;			// +
      }					// +[/soph]

      if (%client.player.getInventory(%item) == 0)
					// +[soph]
         if( getWord( %equipmentList , %index++ ) $= "or" )
            %index++ ;			// + check next entry before failing
         else				// +
            if( %item $= "RepairPack" )	// + engineer repair exception
            {				// +
               if( %client.player.getArmorSize() !$= "Engineer" )
                  return true ;		// +
            }				// +
            else			// +[/soph]
			return true;

      //get the next item
      %index++;
      %item = getWord(%equipmentList, %index);
   }

	//made it through the list without needing anything
	return false;
}

function AIBuyInventory(%client, %requiredEquipment, %equipmentSets, %buyInvTime)
{
   //make sure we have a live player
	%player = %client.player;
	if (!isObject(%player))
		return "Failed";

	if (! AIClientIsAlive(%client))
		return "Failed";

	//see if we've already initialized our state machine
	if (%client.buyInvTime == %buyInvTime)
		return AIProcessBuyInventory(%client);

	//if the closest inv station is not a remote, buy the first available set...
   %result = AIFindClosestInventory(%client, false);
   %closestInv = getWord(%result, 0);
	%closestDist = getWord(%result, 1);
	if (%closestInv <= 0)
		return "Failed";

	//see if the closest inv station was a remote
	%buyingSet = false;
	%usingRemote = false;
   if (%closestInv.getDataBlock().getName() $= "DeployedStationInventory")
	{
		//see if we can buy at least the required equipment from the set
		if (%requiredEquipment !$= "")
		{
			if (! AIMustUseRegularInvStation(%requiredEquipment, %client))
				%canUseRemote = true;
			else
				%canUseRemote = false;
		}
		else
		{
			%inventorySet = AIFindSameArmorEquipSet(%equipmentSets, %client);
			if (%inventorySet !$= "")
				%canUseRemote = true;
			else
				%canUseRemote = false;
		}

		//if we can't use a remote, we need to look for a regular inv station
		if (! %canUseRemote)
		{
		   %result = AIFindClosestInventory(%client, true);
		   %closestInv = getWord(%result, 0);
			%closestDist = getWord(%result, 1);
			if (%closestInv <= 0)
				return "Failed";
		}
		else
			%usingRemote = true;
	}

	//at this point we've found the closest inv, see which set/list we need to buy
	if (!%usingRemote)
	{
		//choose the equipment first equipment set
		if (%equipmentSets !$= "")
		{
//			%inventorySet = getWord(%equipmentSets, 0);							// -soph
			%inventorySet = getWord( %equipmentSets , getRandom( getWordCount( %equipmentSets ) - 1 ) ) ;	// +soph
			%buyingSet = true;
		}
		else
		{
			%inventorySet = %requiredEquipment;
			%buyingSet = false;
		}
	}
	else
	{
		%inventorySet = AIFindSameArmorEquipSet(%equipmentSets, %client);
		if (%inventorySet $= "")
		{
			%inventorySet = %requiredEquipment;
			%buyingSet = false;
		}
		else
			%buyingSet = true;
	}

	//init some vars for the state machine...
	%client.buyInvTime = %buyInvTime;		//used to mark the begining of the inv buy session
	%client.invToUse = %closestInv;			//used if we need to go to an alternate inv station
	%client.invWaitTime = "";					//used to track how long we've been waiting
	%client.invBuyList = %inventorySet;		//the list/set of items we're going to buy...
	%client.buyingSet = %buyingSet;			//whether it's a list or a set...
	%client.isSeekingInv = false;
   %client.seekingInv = "";

	//now process the state machine
	return AIProcessBuyInventory(%client);
}

function AIProcessBuyInventory(%client)
{
	//get some vars
	%player = %client.player;
	if (!isObject(%player))
		return "Failed";

	%closestInv = %client.invToUse;
	%inventorySet = %client.invBuyList;
	%buyingSet = %client.buyingSet;

	//make sure it's still valid, enabled, and on our team
	if (! (%closestInv > 0 && isObject(%closestInv) &&
		(%closestInv.team <= 0 || %closestInv.team == %client.team) && %closestInv.isEnabled()))
	{
		//reset the state machine
		%client.buyInvTime = 0;
		return "InProgress";
	}

	//make sure the inventory station is not blocked
	%invLocation = %closestInv.getWorldBoxCenter();
   InitContainerRadiusSearch(%invLocation, 2, $TypeMasks::PlayerObjectType);
   %objSrch = containerSearchNext();
	if (%objSrch == %client.player)
		%objSrch = containerSearchNext();

	//the closestInv is busy...
	if (%objSrch > 0)
	{
		//have the AI range the inv
		if (%client.seekingInv $= "" || %client.seekingInv != %closestInv)
		{
			%client.invWaitTime = "";
			%client.seekingInv = %closestInv;
		   %client.stepRangeObject(%closestInv, "DefaultRepairBeam", 5, 10);
		}

		//inv is still busy - see if we're within range
		else if (%client.getStepStatus() $= "Finished")
		{
			//initialize the wait time
			if (%client.invWaitTime $= "")
				%client.invWaitTime = getSimTime() + 5000 + (getRandom() * 10000);

			//else see if we've waited long enough
			else if (getSimTime() > %client.invWaitTime)
			{
			   schedule(250, %client, "AIPlayAnimSound", %client, %objSrch.getWorldBoxCenter(), "vqk.move", -1, -1, 0);
				%client.invWaitTime = getSimTime() + 5000 + (getRandom() * 10000);
			}
		}
		else
		{
			//in case we got bumped, and are ranging the target again...
			%client.invWaitTime = "";
		}
	}

	//else if we've triggered the inv, automatically give us the equipment...
	else if (isObject(%closestInv) && isObject(%closestInv.trigger) && VectorDist(%closestInv.trigger.getWorldBoxCenter(), %player.getWorldBoxCenter()) < 1.5)
	{
		//first stop...
		%client.stop();

	   %index=0; // was =0; AI armors buy
		if (%buyingSet)
		{
			//first, clear the players inventory
			%player.clearInventory();
			%item = $AIEquipmentSet[%inventorySet, %index];
		}
		else
			%item = getWord(%inventorySet, %index);


		//armor must always be bought first
	   if (%item $= "Light" || %item $= "Blastech" ||%item $= "MagIon" || %item $= "Medium" || %item $= "Engineer" || %item $= "Heavy" || %item $= "BattleAngel")//-Nite-)
	   {
	      %player.setArmor(%item);
	      %index++;
	   }

		//set the data block after the armor had been upgraded
      %playerDataBlock = %player.getDataBlock();

           if( %item $= "Blastech" )			// +[soph]
           {						// bot bt fix
                schedule( 32 , %player , BlastechRecharge , %player ) ; 
                %player.dynamicResistance[ $DamageGroupMask::Misc ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Energy ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Explosive ] = 0.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Kinetic ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Plasma ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Mitzi ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Poison ] = 1.0 ;   
           }
           else
           {				
                %player.dynamicResistance[ $DamageGroupMask::Misc ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Energy ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Explosive ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Kinetic ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Plasma ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Mitzi ] = 1.0 ;
                %player.dynamicResistance[ $DamageGroupMask::Poison ] = 1.0 ;   
           }						// +[/soph]

		//next, loop through the inventory set, and buy each item
		if (%buyingSet)
			%item = $AIEquipmentSet[%inventorySet, %index];
		else
			%item = getWord(%inventorySet, %index);
		while (%item !$= "")
		{
                        if( %item $= "RandomPack" )	// +[soph]
                        {
                                %num = getWordCount( $RandomPackList[ %client.armor ] ) - 1 ;
                                if( %num )
                                {
                                        %num = getRandom( %num ) ;
                                        %item = getWord( $RandomPackList[ %client.armor ] , %num ) ;
                                        %ammoNum = getWordCount( $RandomPackAmmoList[ %client.armor ] ) ;
                                }
                                else
                                        %item = "RepairPack" ;
                                if( %ammoNum && %num < %ammoNum )
                                {
                                        %quantity = %player.getDataBlock().max[ %item ] ; 
                                        %player.setInventory( %item , %quantity ) ;
                                        %item = getWord( $RandomPackAmmoList[ %client.armor ] , %num ) ;
                                }
                        }				// +[/soph]
                        
			//set the inventory amount to the maximum quantity available
			if (%player.getInventory(AmmoPack) > 0)
				%ammoPackQuantity = AmmoPack.max[%item];
			else
				%ammoPackQuantity = 0;

         %quantity = %player.getDataBlock().max[%item] + %ammoPackQuantity;
			//if ($InvBanList[$CurrentMissionType, %item])
				//%quantity = 0;
         %player.setInventory(%item, %quantity);

			//get the next item
			%index++;
			if (%buyingSet)
				%item = $AIEquipmentSet[%inventorySet, %index];
			else
				%item = getWord(%inventorySet, %index);
		}

		//put a weapon in the bot's hand...
		%player.cycleWeapon();

		//return a success
		return "Finished";
	}

	//else, keep moving towards the inv station
	else
	{
		if (isObject(%closestInv) && isObject(%closestInv.trigger))
		{
			//quite possibly we may need to deal with what happens if a bot doesn't have a path to the inv...
			//the current premise is that no inventory stations are "unpathable"...
			//if (%client.isSeekingInv)
			//{
			//   %dist = %client.getPathDistance(%closestInv.trigger.getWorldBoxCenter());
			//	if (%dist < 0)
			//		error("DEBUG Tinman - still need to handle bot stuck trying to get to an inv!");
			//}

			%client.stepMove(%closestInv.trigger.getWorldBoxCenter(), 1.5);
			%client.isSeekingInv = true;
		}
		return "InProgress";
	}
}

function AIFindSameArmorEquipSet(%equipmentSets, %client)
{
	%clientArmor = %client.player.getArmorSize();
	%index = 0;
	%set = getWord(%equipmentSets, %index);
        %foundIndex = 0 ;				// +soph
	while (%set !$= "")
	{
		if ($AIEquipmentSet[%set, 0] $= %clientArmor)
//			return %set;			// -soph
                {					// +[soph]
			%found[ %foundIndex ] = %set ;	// +
                        %foundIndex++ ;			// +
                }					// +[/soph]

		//get the next equipment set in the list of sets
		%index++;
		%set = getWord(%equipmentSets, %index);
	}
	if( %foundIndex )				// +[soph]
		return %found[ getRandom( %foundIndex - 1 ) ] ;
	else						// +[/soph]
		return "";
}

function AIMustUseRegularInvStation(%equipmentList, %client)
{
	%clientArmor = %client.player.getArmorSize();

	//first, see if the set contains an item not available
	%needRemoteInv = false;
	%index = 0;
   %item = getWord(%equipmentList, 0);
   while (%item !$= "")
	{
		if (%item $= "InventoryDeployable" || (%clientArmor !$= "Light" && %item $= "SniperRifle") ||
			(%clientArmor $= "Light" && (%item $= "Mortar" || %item $= "MissileLauncher")))
		{
			return true;
		}
		else
		{
			%index++;
	      %item = getWord(%equipmentList, %index);
		}
	}
	if (%needRemoteInv)
		return true;


	//otherwise, see if the set begins with an armor class
	%needArmor = %equipmentList[0];
 if (%needArmor !$= "Light" && %needArmor !$= "Medium" && %needArmor !$= "Engineer" && %needArmor !$= "Heavy"  && %needArmor !$= "BattleAngel" && %needArmor !$= "MagIon" && %needArmor !$= "Blastech" )
		return false;

	//also including looking for an inventory set
	if (%needArmor != %client.player.getArmorSize())
		return true;

	//we must be fine...
	return false;
}

function AICouldUseItem(%client, %item)
{
   if(!AIClientIsAlive(%client))
      return false;

	%player = %client.player;
	if (!isObject(%player))
		return false;

	%playerDataBlock = %client.player.getDataBlock();
	%armor = %player.getArmorSize();
	%type = %item.getDataBlock().getName();

	//check packs first
//	if (%type $= "RepairPack" || %type $= "EnergyPack" || %type $= "ShieldPack" || %type $= "CloakingPack" || %type $= "AmmoPack" || %type $= "GravitronPack"  || %type $= "MagIonPack" || %type $= "MagIonHeatPack" || %type $= "HeatShieldPack"  )
//
//	{
//		if (%client.player.getMountedImage($BackpackSlot) <= 0)
//			return true;
//		else
//			return false;
//	}
	if( %item.getDataBlock().className $= "Pack" )
	{
         	if( %client.player.getMountedImage($BackpackSlot) <= 0 && %playerDataBlock.max[ %type ] > 0 )
			return true;
		else
			return false;
	}

   //if the item is acutally, a corpse, check the corpse inventory...
	if ( %item.isCorpse )
	{	// what was with the %player.getInventory(%type)?  doesn't that just return the corpse's inventory of corpses? 
		%corpse = %item;
		// were originally of the form if(%corpse.getInventory(WEAPON) > 0 && %player.getInventory(AMMO) < %playerDataBlock.max[AMMO])
		// should now be of the form if(%player.getInventory(WEAPON) > 0 && %corpse.getInventory(AMMO) && %player.getInventory(AMMO) < %playerDataBlock.max[AMMO])
		if( %player.getInventory( "Chaingun" ) > 0 && %corpse.getInventory( "ChaingunAmmo" ) > 0 && %player.getInventory("ChainGunAmmo") < %playerDataBlock.max[ChainGunAmmo] )
			return true;
		if( %player.getInventory( "Plasma" ) > 0 && %corpse.getInventory( "PlasmaAmmo" ) > 0 && %player.getInventory("PlasmaAmmo") < %playerDataBlock.max[PlasmaAmmo])
			return true;
		if( ( %player.getInventory( "Multifusor" ) > 0 || %player.getInventory( "Disc" ) > 0 ) && %corpse.getInventory( "DiscAmmo" ) > 0 && %player.getInventory("DiscAmmo") < %playerDataBlock.max[DiscAmmo])
			return true;
		if( %player.getInventory( "GrenadeLauncher" ) > 0 && %corpse.getInventory( "GrenadeLauncherAmmo" ) > 0 && %player.getInventory( "GrenadeLauncherAmmo" ) < %playerDataBlock.max[ GrenadeLauncherAmmo ] )
			return true;
		if( %player.getInventory( "Mortar" ) > 0 && %corpse.getInventory( "MortarAmmo" ) > 0 && %player.getInventory("MortarAmmo") < %playerDataBlock.max[ MortarAmmo ] )
			return true;

		//MD2
		if( %player.getInventory( "Protron" ) > 0 && %corpse.getInventory( "ProtronAmmo" ) > 50 && %player.getInventory( "ProtronAmmo" ) < ( %playerDataBlock.max[ ProtronAmmo ] - 50 ) )  // Added more to AIcheck
			return true;
		if( %player.getInventory( "EnergyRifle" ) > 0 && %corpse.getInventory( "EnergyRifleAmmo" ) > 0 && %player.getInventory("EnergyRifleAmmo") < %playerDataBlock.max[EnergyRifleAmmo])  // Added more to AIcheck
			return true;
		// note: skipping BA corpse items due to the whole explodey thing
		if( %player.getInventory( "SniperRifle" ) > 0 && %corpse.getInventory( "Sniper3006Ammo" ) > 0 && %player.getInventory( "Sniper3006Ammo" ) < %playerDataBlock.max[ Sniper3006Ammo ] )
			return true;
		if( %player.getInventory( "MissileLauncher" ) > 0 && %corpse.getInventory( "MissileLauncherAmmo" ) > 0 && %player.getInventory( "MissileLauncherAmmo" ) < %playerDataBlock.max[ MissileLauncherAmmo ] )
			return true;
		if( %player.getInventory( "PCR" ) > 0 && %corpse.getInventory( "PCRAmmo" ) > 15 && %player.getInventory( "PCRAmmo" ) < ( %playerDataBlock.max[ PCRAmmo ] - 15 ) )
			return true;
		if( %player.getInventory( "AutoCannon" ) > 0 && %corpse.getInventory( "AutoCannonAmmo" ) > 0 && %player.getInventory( "AutoCannonAmmo" ) < %playerDataBlock.max[ AutoCannonAmmo ] )
			return true;
	}
	else if( %item.getDataBlock().className $= "Ammo" )
	{
		//check ammo
		%quantity = mFloor(%playerDataBlock.max[%type]);
		if (%player.getInventory(%type) < %quantity)
		{
			if( %type $= "ChainGunAmmo" && %player.getInventory( "Chaingun" ) > 0 )
				return true;
			if( %type $= "PlasmaAmmo" && %player.getInventory( "Plasma" ) > 0 )
				return true;
			if( %type $= "DiscAmmo" && ( %player.getInventory( "Multifusor" ) > 0 || %player.getInventory( "Disc" ) > 0 ) )
				return true;
			if( %type $= "GrenadeLauncherAmmo" && %player.getInventory( "GrenadeLauncher" ) > 0 )
				return true;
			if (%type $= "MortarAmmo" && %player.getInventory("Mortar") > 0)
				return true;

      			//MD2
			if( %type $= "MechRocketgunAmmo" && %player.getInventory( "MechRocketgun" ) > 0 )
				return true;
			if( %type $= "MechMinigunAmmo" && %player.getInventory( "MechMinigun" ) > 0 )
				return true;
			if( %type $= "RAXXAmmo" && %player.getInventory( "RAXX" ) > 0 )
				return true;
			if( %type $= "ProtronAmmo" && %player.getInventory( "Protron" ) > 0 )
				return true;
			if( %type $= "EnergyRifleAmmo" && %player.getInventory( "EnergyRifle" ) > 0 )
				 return true;
			if( %type $= "Sniper3006Ammo" && %player.getInventory( "SniperRifle" ) > 0 )
				 return true;
			if( %type $= "MissileLauncherAmmo" && %player.getInventory( "MissileLauncher" ) > 0 )
				 return true;
			if( %type $= "PCRAmmo" && %player.getInventory( "PCR" ) > 0 )
				 return true;
			if( %type $= "AutoCannonAmmo" && %player.getInventory( "AutoCannon" ) > 0 )
				 return true;
		}
	}
	else if( %item.getDataBlock().className $= "Weapon" )
	{
		if (AICanPickupWeapon(%client, %type))
			return true;
	}
	else if( %item.getDataBlock().isGrenade )
	{
                %matchingGrenadeCount = %player.getInventory( %type ) ;
		if( %matchingGrenadeCount > 0 )
		{
			if( %matchingGrenadeCount < %playerDataBlock.max[ %type ] )
				return true ;
		}
		else if( %playerDataBlock.max[ %type ] <= 0 )
			return false ;
		else
		{
			for( %i = 0 ; $InvGrenade[ %i ] !$= "" ; %i++ )
			{
				%grenade = $NameToInv[ $InvGrenade[ %x ] ] ;
				if( %player.getInventory( %grenade ) > 0 )
					return false ;
			}
			return true ;
		}
	}

	//guess we didn't find anything useful...  (should still check for mines and grenades)
   return false;
}

function AIEngageOutofAmmo(%client)
{
	//this function only cares about weapons used in engagement...
	//no mortars, or missiles
   %player = %client.player;
	if (!isObject(%player))
		return false;

   %ammoWeapons = 0;
   %energyWeapons = 0;

   //get our inventory
   %hasBlaster = (%player.getInventory("Blaster") > 0);
   %hasPlasma  = (%player.getInventory("Plasma") > 0);
   %hasChain   = (%player.getInventory("Chaingun") > 0);
   %hasDisc    = (%player.getInventory("Disc") > 0);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0);
   %hasSniper  = (%player.getInventory("SniperRifle") > 0) ;									// && (%player.getInventory("EnergyPack") > 0); -soph
   %hasELF     = (%player.getInventory("ELFGun") > 0);
   %hasMortar  = (%player.getInventory("Mortar") > 0);
   %hasMissile = (%player.getInventory("MissileLauncher") > 0);
   %hasLance   = (%player.getInventory("ShockLance") > 0);
    //MD2
   %hasMBCannon  = (%player.getInventory("MBCannon") > 0);
   %hasProtron = (%player.getInventory("Protron") > 0);
   %hasEnergyRifle =(%player.getInventory("EnergyRifle") > 0);
   %hasRAXX = (%player.getInventory("RAXX") > 0);
   %hasRFL = (%player.getInventory("PCR") > 0);
   %hasPlasmaCannon =(%player.getInventory("PlasmaCannon") > 0);
   %hasMultifusor = (%player.getInventory("Multifusor") > 0);
   %hasMechMinigun =(%player.getInventory("MechMinigun") > 0);
   %hasRocketGun = (%player.getInventory("MechRocketGun") > 0);

   if (%hasBlaster || %hasSniper || %hasElf || %hasLance || %hasMBCannon  || %hasPlasmaCannon || %hasProtron || %hasRFL )	// added rfl +soph
      return false;

   else
   {
      // we only have ammo type weapons
      if(%hasDisc && (%player.getInventory("DiscAmmo") > 0))
         return false;
      else if(%hasChain && (%player.getInventory("ChainGunAmmo") > 0))
         return false;
      else if(%hasGrenade && (%player.getInventory("GrenadeLauncherAmmo") > 0))
         return false;
      else if(%hasPlasma && (%player.getInventory("PlasmaAmmo") > 0))
         return false;
      //Md2
       else if(%hasMBCannon && (%player.getInventory("MBCannonCapacitor") > 0))
         return false;
       else if(%hasProtron && (%player.getInventory("ProtronAmmo") > 10))	// > 0)) -soph
         return false;
       else if(%hasEnergyRifle && (%player.getInventory("EnergyRifleAmmo") > 0))
         return false;
       else if(%hasMultifusor && (%player.getInventory("DiscAmmo") > 0))
         return false;
       else if(%hasRAXX && (%player.getInventory("RAXXAmmo") > 0))
         return false;
       else if(%hasMechMinigun && (%player.getInventory("MechMinigunAmmo") > 0))
         return false;
        else if(%hasRocketGun && (%player.getInventory("MechRocketGunAmmo") > 0))
          return false;
         else if(%hasRFL && (%player.getInventory("PCRAmmo") > 0))
         return false;
    }
   return true; // were out!
}

function AICanPickupWeapon(%client, %weapon)
{
	//first, make sure it's not a weapon we already have...
	%player = %client.player;
	if (!isObject(%player))
		return false;

	%armor = %player.getArmorSize();
	if (%player.getInventory(%weapon) > 0)
		return false;

	//make sure the %weapon given is a weapon they can use for engagement
	if (%weapon !$= "Blaster" && %weapon !$= "Plasma" && %weapon !$= "Chaingun" && %weapon !$= "Disc" && %weapon !$= "MBCannon" && %weapon !$= "Protron" &&  %weapon !$= "EnergyRifle"  &&  %weapon !$= "RAXX"
		&& %weapon !$= "MechRocketgun" && %weapon !$= "MechMinigun" && %weapon !$= "MultiFusor" && %weapon !$= "PlasmaCannon" && %weapon !$= "PCR" && %weapon !$= "Mortar" && %weapon !$= "SniperRifle" && %weapon !$= "ELFGun" && %weapon !$= "ShockLance")
	{
		return false;
	}

	%weaponCount = 0;
	if (%player.getInventory("Blaster") > 0)
		%weaponCount++;
	if (%player.getInventory("Plasma") > 0)
		%weaponCount++;
	if (%player.getInventory("Chaingun") > 0)
		%weaponCount++;
	if (%player.getInventory("Disc") > 0)
		%weaponCount++;
	if (%player.getInventory("GrenadeLauncher") > 0)
		%weaponCount++;
	if (%player.getInventory("SniperRifle") > 0)
		%weaponCount++;
	if (%player.getInventory("ELFGun") > 0)
		%weaponCount++;
	if (%player.getInventory("Mortar") > 0)
		%weaponCount++;
	if (%player.getInventory("MissileLauncher") > 0)
		%weaponCount++;
	if (%player.getInventory("ShockLance") > 0)
		%weaponCount++;
  //Md2
    if (%player.getInventory("MBCannon") > 0)
		%weaponCount++;
    if (%player.getInventory("Protron") > 0)
		%weaponCount++;
    if (%player.getInventory("RAXX") > 0)
		%weaponCount++;
    if (%player.getInventory("EnergyRifle") > 0)
		%weaponCount++;
    if (%player.getInventory("MutliFusor") > 0)
		%weaponCount++;
     if (%player.getInventory("MechMinigun") > 0)
     	%weaponCount++;
     if (%player.getInventory("MechRocketgun") > 0)
		%weaponCount++;
     if (%player.getInventory("PCR") > 0)
		%weaponCount++;
     if (%player.getInventory("PlasmaCannon") > 0)
		%weaponCount++;


 if ((%armor $= "Light" && %weaponCount < 3) ||(%armor $= "MagIon" && %weaponCount < 2)||(%armor $= "Blastech" && %weaponCount < 3) ||(%armor $= "Medium" && %weaponCount < 4) ||(%armor $= "Engineer" && %weaponCount < 5) || (%armor $= "BattleAngel" && %weaponCount < 6) ||
																	(%armor $= "Heavy" && %weaponCount < 5))
	{
		if ((%type $= "Mortar" && %armor !$= "Heavy") || (%type $= "MissileLauncher" && %armor $= "Light") ||
																			(%type $= "SniperRifle" && %armor !$= "Light"))
			return false;
		else
			return true;
	}

	//else we're full of weapons already...
	return false;
}

function AIEngageWeaponRating(%client)
{
   %player = %client.player;
	if (!isObject(%player))
		return;

	%playerDataBlock = %client.player.getDataBlock();

   //get our inventory
   %hasBlaster = (%player.getInventory("Blaster") > 0);
   %hasPlasma  = (%player.getInventory("Plasma") > 0 && %player.getInventory("PlasmaAmmo") >= 1);
   %hasChain   = (%player.getInventory("Chaingun") > 0 && %player.getInventory("ChaingunAmmo") >= 1);
   %hasDisc    = (%player.getInventory("Disc") > 0 && %player.getInventory("DiscAmmo") >= 1);
   %hasMortar = (%player.getInventory("Mortar") > 0 && %player.getInventory("MortarAmmo") >= 1);
   %hasSniper  = (%player.getInventory("SniperRifle") > 0);	// removed epack requirement -soph
   %hasELF     = (%player.getInventory("ELFGun") > 0);
   %hasLance   = (%player.getInventory("ShockLance") > 0);
    //MD2
   %hasMBCannon   = (%player.getInventory("MBCannon") > 0 && %player.getInventory("MBCannonCapacitor") >= 1);
   %hasProtron = (%player.getInventory("Protron") > 0 && %player.getInventory("ProtronAmmo") >= 25 );	// 1); -soph
   %hasEnergyRifle = (%player.getInventory("EnergyRifle") > 0 && %player.getInventory("EnergyRifleAmmo") >= 1);
   %hasRFL  = (%player.getInventory("PCR") > 0 && %player.getInventory("PCRAmmo") >= 1);
   %hasPlasmaCannon = (%player.getInventory("PlasmaCannon") > 1);
   %hasRAXX = (%player.getInventory("RAXX") > 0 && %player.getInventory("RAXXAmmo") >= 1);
   %hasMutliFusor = (%player.getInventory("Multifusor") > 0 && %player.getInventory("DiscAmmo") >= 1);
   %hasMechMinigun = (%player.getInventory("MechMinigun") > 0 && %player.getInventory("MechMinigunAmmo") >= 1);
   %hasRocketgun   = (%player.getInventory("MechRocketgun") > 0 && %player.getInventory("MechRocketgunAmmo") >= 1);

	//check ammo
	%quantity = mFloor(%playerDataBlock.max[%type] * 0.7);

	%rating = 0;
	if (%hasBlaster)
		%rating += 9;
	if (%hasSniper)
		%rating += 9;
	if (%hasElf)
		%rating += 9;
    if (%hasLance)
         %rating += 15;
  //md2
     if (%hasPlasmaCannon)
         %rating += 15;
     if (%hasRFL)
		%rating += 15;

	if (%hasDisc)
	{
		%quantity = %player.getInventory("DiscAmmo") / %playerDataBlock.max["DiscAmmo"];
		%rating += 10 + (10 * %quantity);
	}
	if (%hasPlasma)
	{
		%quantity = %player.getInventory("PlasmaAmmo") / %playerDataBlock.max["PlasmaAmmo"];
		%rating += 15 + (15 * %quantity);
	}
	if (%hasChain)
	{
		%quantity = %player.getInventory("ChainGunAmmo") / %playerDataBlock.max["ChainGunAmmo"];
		%rating += 15 + (15 * %quantity);
	}
    //MD2
    if (%hasMBCannon)
	{
		%quantity = %player.getInventory("MBCannonCapacitor") / %playerDataBlock.max["MBCannonCapacitor"];
		%rating += 15 + (15 * %quantity);
	}
     if (%hasProtron)
	{
		%quantity = %player.getInventory("ProtronAmmo") / %playerDataBlock.max["ProtronAmmo"];
		%rating += 15 + (15 * %quantity);
	}
    if (%hasEnergyRifle)
	{
		%quantity = %player.getInventory("EnergyRifleAmmo") / %playerDataBlock.max["EnergyRifleAmmo"];
		%rating += 15 + (15 * %quantity);
	}
     if (%hasRAXX)
	{
		%quantity = %player.getInventory("RAXXAmmo") / %playerDataBlock.max["RAXXAmmo"];
		%rating += 15 + (15 * %quantity);
	}
     if (%hasMultifusor)
	{
		%quantity = %player.getInventory("DiscAmmo") / %playerDataBlock.max["DiscAmmo"];
		%rating += 15 + (15 * %quantity);
	}
    if (%hasMechMinigun)
	{
		%quantity = %player.getInventory("MechMinigunAmmo") / %playerDataBlock.max["MechMinigunAmmo"];
		%rating += 15 + (15 * %quantity);
	}
    if (%hasRocketgun)
	{
		%quantity = %player.getInventory("MechRocketgunAmmo") / %playerDataBlock.max["MechRocketgunAmmo"];
		%rating += 15 + (15 * %quantity);
	}
     if (%hasMortar)
	{
		%quantity = %player.getInventory("MortarAmmo") / %playerDataBlock.max["MortarAmmo"];
		%rating += 15 + (15 * %quantity);
	}
//not really an effective weapon for hand to hand...
	if (%hasGrenade)
	{
		%quantity =  %player.getInventory("GrenadeLauncherAmmo") / %playerDataBlock.max["GrenadeLauncherAmmo"];
		%rating += 10 + (15 * %quantity);
	}

	//note a rating of 20+ means at least two energy weapons, or an ammo weapon with at least 1/3 ammo...
	return %rating;
}

function AIFindSafeItem(%client, %needType)
{
	%player = %client.player;
	if (!isObject(%player))
		return -1;

	%closestItem = -1;
	%closestDist = 32767;

	%itemCount = $AIItemSet.getCount();
	for (%i = 0; %i < %itemCount; %i++)
	{
		%item = $AIItemSet.getObject(%i);
		if (%item.isHidden())
			continue;

		%type = %item.getDataBlock().getName();
		if ((%needType $= "Health" && (%type $= "RepairKit" || %type $= "RepairPatch") && %player.getDamagePercent() > 0) ||
			(%needType $= "Ammo" && (%type $= "ChainGunAmmo" || %type $= "PlasmaAmmo" || %type $= "DiscAmmo" ||%type $= "ProtronAmmo" || %type $= "MechMinigunAmmo" || %type $= "MechRocketgunAmmo" ||  %type $= "EnergyRifleAmmo" || %type $= "RAXXAmmo" ||
												%type $= "GrenadeLauncherAmmo" || %type $= "MortarAmmo") && AICouldUseItem(%client, %item)) ||
			(%needType $= "Ammo" && AICanPickupWeapon(%type)) ||
			((%needType $= "" || %needType $= "Any") && AICouldUseItem(%client, %item)))
		{
			//first, see if it's close to us...
         %distance = %client.getPathDistance(%item.getTransform());
			if (%distance > 0 && %distance < %closestDist)
			{
				//now see if it's got bad enemies near it...
				%clientCount = ClientGroup.getCount();
				for (%j = 0; %j < %clientCount; %j++)
				{
					%cl = ClientGroup.getObject(%j);
					if (%cl == %client || %cl.team == %client.team || !AIClientIsAlive(%cl))
						continue;

					//if the enemy is stronger, see if they're close to the item
					if (AIEngageWhoWillWin(%client, %cl) == %cl)
					{
						%tempDist = %client.getPathDistance(%item.getWorldBoxCenter());
						if (%tempDist > 0 && %tempDist < %distance + 50)
							continue;
					}

					//either no enemy, or a weaker one...
					%closestItem = %item;
					%closestDist = %distance;
				}
			}
		}
	}

	return %closestItem;
}

function AIChooseObjectWeapon(%client, %targetObject, %distToTarg, %mode, %canUseEnergyStr, %environmentStr)
{
   //get our inventory
   %player = %client.player;
	if (!isObject(%player))
		return;

	if (!isObject(%targetObject))
		return;

	%canUseEnergy = (%canUseEnergyStr $= "true") && !%targetObject.isEMP ;	// (%canUseEnergyStr $= "true"); -soph
	%inWater = (%environmentStr $= "water");
	%outdoors = (%environmentStr $= "outdoors");				// needed +soph
        %mySpeed = VectorDist( "0 0 0" , %client.player.getVelocity() ) ;	// useful +soph
   %hasBlaster = (%player.getInventory("Blaster") > 0) && %canUseEnergy;
   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0) && !%inWater;
   %hasChain = (%player.getInventory("Chaingun") > 0) && (%player.getInventory("ChaingunAmmo") > 0);
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0) && (%player.getInventory("GrenadeLauncherAmmo") > 0);
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0);
   %hasRepairPack = (%player.getInventory("RepairPack") > 0) && %canUseEnergy;
   %hasTargetingLaser = (%player.getInventory("TargetingLaser") > 0) && %canUseEnergy;
   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);
   %hasLance = (%player.getInventory("ShockLance") > 0) && %canUseEnergy && !%inWater;
    //   MD2
   %hasMBCannon = (%player.getInventory("MBCannon") > 0) && (%player.getInventory("MBCannonCapacitor") > 10 ) ;	//  0); -soph
   %hasRAXX = (%player.getInventory("RAXX") > 0) && (%player.getInventory("RAXXAmmo") > 0) && !%inWater;
   %hasMechMinigun = (%player.getInventory("MechMinigun") > 0) && (%player.getInventory("MechMinigunAmmo") > 0);
   %hasRocketgun = (%player.getInventory("MechRocketgun") > 0) && (%player.getInventory("MechRocketgunAmmo") > 0);
   %hasRFL = (%player.getInventory("PCR") > 0) && (%player.getInventory("PCRAmmo") > 7 ) ;	// 0); -soph
   %hasMultifusor = (%player.getInventory("Multifusor") > 0) && (%player.getInventory("DiscAmmo") > 0);
   %hasPlasmaCannon = (%player.getInventory("PlasmaCannon") > 0) && %canUseEnergy;
   %hasProtron = (%player.getInventory("Protron") > 0) && (%player.getInventory("ProtronAmmo") > 25 ) ;	// 0); -soph
   %hasEnergyRifle = (%player.getInventory("EnergyRifle") > 0) && (%player.getInventory("EnergyRifleAmmo") > 0);
   %hasAutocannon = (%player.getInventory("AutoCannon") > 0) && (%player.getInventory("AutoCannonAmmo") > 0);	// +soph

// REWRITE ALL THE AI +[soph]
   %hasReassembler = ( %player.getArmorType() & $ArmorMask::Engineer && %canUseEnergy ) ;
   %useWeapon = "NoAmmo" ;

   if( %targetObject.isEMP ) 
      %targetShielded = false ;
   else if( %targetObject.isShielded || %targetObject.getDataBlock().isShielded )
      %targetShielded = true ;
   else if( isObject( %targetObject.shieldCap ) )
      %targetShielded = true ;
   else 
   {
      %mountObject = %targetObject.getObjectMount() ;
      if( isObject( %mountObject ) && ( %mountObject.isShielded || %mountObject.getDataBlock().isShielded ) )
         %targetShielded = true ;
      else
         %targetShielded = false ;
   }

   %speed = VectorDist( "0 0 0" , %targetObject.getVelocity() ) ;
   if( %speed $= "" )
      %speed = 0 ;

// blow shit up
   if (%mode $= "Destroy")
   {
      if( %targetObject.getDataBlock().shapeFile $= "mine.dts" )
      {
         if (%hasMBCannon)
         {
            %useWeapon = "MBCannon" ;
            %client.mode[ "MBCannon" ] = 1 ;
         }
         else if (%hasProtron)
         {
            %useWeapon = "Protron" ;  
            %client.mode[ "Protron" ] = 0 ;
         }
         else if (%hasPlasmaCannon)
            %useWeapon = "PlasmaCannon";
         else if ( %hasRAXX && %distToTarg < 40 && %distToTarg > 10 )
            %useWeapon = "RAXX";
         else if ( %hasMechMinigun && %distToTarg < 35 )
            %useWeapon = "MechMinigun";
         else if (%hasMultifusor)
         {
            %useWeapon = "Multifusor" ;
            %client.mode[ "Multifusor" ] = 0 ;
         }  
         else if (%hasRocketgun)
         {
            %useWeapon = "MechRocketgun" ;
            %client.mode[ "MechRocketgun" ] = 0 ;
         }  
         else if ( %hasMortar && %distToTarg > 20 )
         {
            %useWeapon = "Mortar" ;
            %client.mode[ "Mortar" ] = 1 ;
         }  
         else if (%hasAutoCannon)
         {
            %useWeapon = "AutoCannon" ;
            %client.mode[ "AutoCannon" ] = 1 ;
         }    
         else if (%hasPlasma)
         {
            %useWeapon = "Plasma" ;
            %client.mode[ "Plasma" ] = 0 ;
         }  
         else if (%hasDisc)
         {
            %useWeapon = "Disc" ;
            %client.mode[ "Disc" ] = 0 ;
         }  
         else if (%hasEnergyRifle)
         {
            %useWeapon = "EnergyRifle" ;
            %client.mode[ "EnergyRifle" ] = 1 ;
         }    
         else if ( %hasChain && %distToTarg < 45 )
         {
            %useWeapon = "Chaingun" ;
            %client.mode[ "Chaingun" ] = 0 ;
         }
         else if (%hasBlaster)
         {
            %useWeapon = "Blaster" ;
            %client.mode[ "Blaster" ] = 2 ;
         }    
      }
      else
      {
         if( %distToTarg < 40 )
         {
            if ( %hasRAXX )
               %useWeapon = "RAXX";
            else if ( %hasMechMinigun )
               %useWeapon = "MechMinigun";
            else if ( %hasPlasma && %player.getArmorSize() $= "Medium" && %player.getInventory("PlasmaAmmo") > 1 )
            {
               %useWeapon = "Plasma" ;
               %client.mode[ "Plasma" ] = 1 ;
            }  
            else if ( %hasChain )
            {
               %useWeapon = "Chaingun" ;
               %client.mode[ "Chaingun" ] = 0 ;
            }  
         }
         else if( %distToTarg > 100 )
         {
            if (%hasMortar)
            {
               %useWeapon = "Mortar" ;
               if( %targetShielded && %targetObject.getEnergyLevel() > 40 && %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1  )
                  %client.mode[ "Mortar" ] = 2 ;
               else
                  %client.mode[ "Mortar" ] = 0 ;
            }
            else if (%hasMultifusor)
            {
               %useWeapon = "Multifusor" ;
               %client.mode[ "Multifusor" ] = 0 ; 
            }  
            else if (%hasGrenade)
            {
               %useWeapon = "GrenadeLauncher";
               if( %targetShielded && %targetObject.getEnergyLevel() > 10 && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
                  %client.mode[ "GrenadeLauncher" ] = 1 ;
               else if( %distToTarg < 70 && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
                  %client.mode[ "GrenadeLauncher" ] = 2 ;
               else
                  %client.mode[ "GrenadeLauncher" ] = 0 ;
            }
            else if (%hasRocketgun)
            {
               %useWeapon = "MechRocketgun" ;
               %client.mode[ "MechRocketgun" ] = 1 ;
            }
            else if (%hasPlasmaCannon)
               %useWeapon = "PlasmaCannon";
            else if (%hasMBCannon)
            {
               %useWeapon = "MBCannon" ;
               %client.mode[ "MBCannon" ] = 0 ;
            }
         }
      }
   }

// hummmmm
   else if (%mode $= "Laze")
   {
      if (%hasTargetingLaser)
         %useWeapon = "TargetingLaser";
      else
         %useWeapon = "NoAmmo";
   }

// green balls of death. or maybe gray. or something else
   else if( %mode $= "Mortar" )
   {
      %muzzlePoint = %player.getMuzzlePoint( 0 ) ;
      %targetPosition = %targetObject.getWorldBoxCenter() ;
      %vectorToTarget = vectorSub( %targetPosition , %muzzlePoint ) ;
      %distance = VectorLen( %vectorToTarget ) ;
      %distance2D = VectorLen( getWord( %vectorToTarget , 0 ) SPC getWord( %vectorToTarget , 1 ) SPC "0" ) ;
      %heightDelta = getWord( %vectorToTarget , 2 ) ;
      %hitTarget = ContainerRayCast( %muzzlePoint , %targetPosition , $TypeMasks::InteriorObjectType |
                                                                      $TypeMasks::TerrainObjectType | 
                                                                      $TypeMasks::ForceFieldObjectType | 
                                                                      $TypeMasks::StaticShapeObjectType |
                                                                      $TypeMasks::VehicleObjectType , %player ) ;
      %hasLoS = ( getWord( %hitTarget , 0 ) == %targetObject ) ? true : false ;

      if( %targetShielded && %targetObject.getEnergyLevel() > 50 )
      {
         if( %hasMBCannon && %distance <= 145 && !%inWater && 
             %player.getInventory( "MBCannonCapacitor" ) >= 100 && 
             ( %player.getArmorSize() $= "BattleAngel" || 
               ( %player.getArmorSize() $= "Heavy" && %mySpeed < 3 )
           ) )
         {
            %useWeapon = "MBCannon" ;
            %client.mode[ "MBCannon" ] = 4 ;
            if( %player.getArmorSize() $= "Heavy" )
            {
               %escapePosition = vectorSub( %mzzlePoint , vectorScale( %vectorToTarget , 15 ) ) ;
               %escapePosition = getWords( %escapePosition , 0 , 1 ) SPC ( getWord( %escapePosition , 2 ) + 5 ) ;
               %client.stepMove( %escapePosition , 10 , $AIModeExpress ) ;
            }
         }
         else if( %hasMBCannon && !%inWater && 
                  %player.getInventory( "MBCannonCapacitor" ) >= 125 &&
                  %player.getArmorSize() $= "BattleAngel" )
         {
            %useWeapon = "MBCannon" ;
            %client.mode[ "MBCannon" ] = 5 ;
         }
         else if( %hasGrenade && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 &&
                  ( ( %distance2D < 175 && %heightDelta < 100 ) ||
                    ( %distance2D < 250 && %heightDelta < 50  ) ||
                    ( %distance2D < 325 && %heightDelta < 25  ) 
                ) )
         {
            %useWeapon = "GrenadeLauncher";
            %client.mode[ "GrenadeLauncher" ] = 1 ;
         }
         else if( %hasLoS && %hasMissile && %distance < 495 )
         {
            %useWeapon = "MissileLauncher";
            %client.mode[ "MissileLauncher" ] = 2 ;
         }
         else if( %hasLoS && %hasRocketGun )
         {
            %useWeapon = "MechRocketgun" ;
            %client.mode[ "MechRocketgun" ] = 1 ;
         }
         else if( %hasMortar && %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1 && %distance2D > 400 )
         {
            %useWeapon = "Mortar" ;
            %client.mode[ "Mortar" ] = 3 ;
         }
         else if( %hasMortar && %distance < 400 )
         {
            %useWeapon = "Mortar" ;
            if( %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 3 )
               %client.mode[ "Mortar" ] = 2 ;
            else
               %client.mode[ "Mortar" ] = 0 ;
         }
      }
      else 
      {
         if( %hasLoS && %hasRocketGun && %distance > 200 && %player.getInventory("MechRocketgunAmmo") > 14 )
         {
            %useWeapon = "MechRocketgun" ;
            %client.mode[ "MechRocketgun" ] = 1 ;
         }
         else if( %hasMortar && %player.getInventory( "MortarAmmo" ) > 1 && %player.getArmorSize() $= "BattleAngel" && %distance2D > 400 )
         {
            %useWeapon = "Mortar" ;
            %client.mode[ "Mortar" ] = 3 ;
         }
         else if( %hasMortar && %distance > 150 && %distance < 400 )
         {
            %useWeapon = "Mortar" ;
            %client.mode[ "Mortar" ] = 0 ;
         }
         else if( %hasLoS && %hasMissile && %distance < 495 )
         {
            %useWeapon = "MissileLauncher";
            if( %targetShielded )
               %client.mode[ "MissileLauncher" ] = 2 ;
            else if( %distance > 150 )
               %client.mode[ "MissileLauncher" ] = 4 ;
            else
               %client.mode[ "MissileLauncher" ] = 0 ;
         }
         else if( %hasLoS && %hasRocketGun )
         {
            %useWeapon = "MechRocketgun" ;
            if( %distance > 25 )
               %client.mode[ "MechRocketgun" ] = 1 ;
            else 
               %client.mode[ "MechRocketgun" ] = 0 ;
         }
         else if( %hasMortar && %distance < 400 )
         {
            %useWeapon = "Mortar" ;
            if( %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1 && %distance2D > 400 )
               %client.mode[ "Mortar" ] = 3 ;
            else if( %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 3 )
               %client.mode[ "Mortar" ] = 2 ;
            else
               %client.mode[ "Mortar" ] = 0 ;
         }
         
      }
   }

// all the missiles
   else if (%mode $= "Missile" || %mode $= "MissileNoLock")
   // else if (%mode $= "MissileNoLock")
   {
      if ( %hasMissile && %distToTarg < 495 )
      {
         %useWeapon = "MissileLauncher" ;
         if( %speed > 5 )
         {
            if( %speed > 200 )
               if( %distToTarg < 100 )
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ;
               else if( %targetShielded )
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 0 : 2 ;
               else
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ;
            else if( %speed > 75 )
               if( %targetShielded )
                  %client.mode[ "MissileLauncher" ] = getRandom( 2 ) ? getRandom ( 1 ) + 1 : getRandom( 2 ) ;
               else
                  %client.mode[ "MissileLauncher" ] = getRandom( 2 ) ? getRandom ( 1 ) : 4 ;
            else 
               %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 0 : 4 ;
            if( !%client.mode[ "MissileLauncher" ] && !getRandom( 2 ) )
               %client.mode[ "MissileLauncher" ] = 6 ;
         }
         else if( %targetShielded )
            %client.mode[ "MissileLauncher" ] = 2 ;
         else if( %distToTarg < 140 )
            %client.mode[ "MissileLauncher" ] = 0 ;
         else
            %client.mode[ "MissileLauncher" ] = 4 ;
      }
      else if (%hasMBCannon)
      {
         %useWeapon = "MBCannon";
         %client.mode[ "MBCannon" ] = 0 ;
      }
      else
         %useWeapon = "NoAmmo";
   }

// default logic for when all else fails (which could be pretty often)
   
// repairs first bleh
   if( %targetObject.team == %player.team && %targetObject.getDataBlock().shapeFile !$= "mine.dts" )
   {
      if( %mode $= "Repair" )
         if( %hasReassembler )
            %useWeapon = "Reassembler" ;
         else if (%hasRepairPack)
            %useWeapon = "RepairPack";
         else
            %useWeapon = "NoAmmo";
      else
         %useWeapon = "NoAmmo";
   }
// all other fallbacks
   else
   {
      if( %useWeapon $= "NoAmmo" )
         if ( %hasRAXX && %distToTarg < 45 && %distToTarg > 10 )
            %useWeapon = "RAXX";  
         else if ( %hasMechMinigun && %distToTarg < 65  )
            %useWeapon = "MechMinigun";
         else if (%hasRocketgun && %distToTarg > 250 )
         {
            %useWeapon = "MechRocketgun" ;
            %client.mode[ "MechRocketgun" ] = 1 ;
         }
         else if( %hasMortar && %distToTarg > 90 && %distToTarg < 400 )
         {
            %useWeapon = "Mortar" ;
            if( %targetShielded && %targetObject.getEnergyLevel() > 40 && %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 3  )
               %client.mode[ "Mortar" ] = 2 ;
            else
               %client.mode[ "Mortar" ] = 0 ;
         }
         else if ( %hasMissile && %distToTarg > 10 && %distToTarg < 495  )
         {
            %useWeapon = "MissileLauncher" ; 
            if( %targetShielded )
               if( %speed > 150 ) 
                  %client.mode[ "MissileLauncher" ] = getRandom( 2 ) ;   
               else
                  %client.mode[ "MissileLauncher" ] = 2 ;             
            else if( %distToTarg > 35 && %player.getArmorSize() !$= "Blastech" )
               %client.mode[ "MissileLauncher" ] = 4 ;
            else 
               %client.mode[ "MissileLauncher" ] = 0 ;
         }
         else if( %hasMultifusor && %distToTarg < 450)
         {
            %useWeapon = "Multifusor" ; 
            %client.mode[ "Multifusor" ] = 0 ;
         }
         else if( %hasGrenade && %distToTarg < 300 )
         {
            %useWeapon = "GrenadeLauncher";
            if( %targetShielded && %targetObject.getEnergyLevel() > 20 && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
               %client.mode[ "GrenadeLauncher" ] = 1 ;
            else if( %distToTarg < 50 && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
               %client.mode[ "GrenadeLauncher" ] = 2 ;
            else
               %client.mode[ "GrenadeLauncher" ] = 0 ;
         }
         else if (%hasProtron)
         {
            %useWeapon = "Protron" ;
            if( %distToTarg < 35 )
               %client.mode[ "Protron" ] = 2 ;
            else if( %distToTarg < 150 && %targetShielded )
               %client.mode[ "Protron" ] = 1 ;
            else
               %client.mode[ "Protron" ] = 0 ;
         }
         else if (%hasBlaster && %distToTarg < 50 )
         {
            %useWeapon = "Blaster" ;
            if( %targetShielded )
               %client.mode[ "Blaster" ] = 0 ;
            else 
               %client.mode[ "Blaster" ] = 1 ;
         }
         else if (%hasAutocannon)
         {
            %useWeapon = "Autocannon" ;
            if( %distToTarg < 100 )
               %client.mode[ "Autocannon" ] = 1 ;
            else
               %client.mode[ "Autocannon" ] = 0 ;
         }
         else if (%hasPlasma)
         {
            %useWeapon = "Plasma" ;
            if( %distToTarg < 55 && %player.getArmorSize() $= "Medium" && %player.getInventory( "PlasmaAmmo" ) > 1 )
               %client.mode[ "Plasma" ] = 1 ;
            else
               %client.mode[ "Plasma" ] = 0 ;
         }
         else if( %hasEnergyRifle && %distToTarg < 60 && %player.getInventory( "EnergyRifle" ) > 3 )
         {
               %useWeapon = "EnergyRifle" ;
               %client.mode[ "EnergyRifle" ] = 1 ;
         }
         else if ( %hasChain && %distToTarg < 60 )
         {
            %useWeapon = "Chaingun" ;
            %client.mode[ "Chaingun" ] = 0 ;
         }
         else if (%hasDisc)
         {
            %useWeapon = "Disc" ;
            if( %myEnergy > 0.4 && %distToTarg > 20 && %player.getInventory( "DiscAmmo" ) > 2 )
               %client.mode[ "Disc" ] = 2 ;
            else
               %client.mode[ "Disc" ] = 0 ;
         }
         else if (%hasEnergyRifle)
         {
            %useWeapon = "EnergyRifle" ; 
         %client.mode[ "EnergyRifle" ] = 0 ;
         }
         else if (%hasBlaster)
         {
            %useWeapon = "Blaster" ;
            if( %distToTarg > 100 )
               %client.mode[ "Blaster" ] = 2 ;
            else if( %targetShielded )
               %client.mode[ "Blaster" ] = 0 ;
            else 
               %client.mode[ "Blaster" ] = 1 ;
         }
         else if( %hasMortar && %distToTarg < 400 )
         {
            %useWeapon = "Mortar" ;
            if( %targetShielded && %targetObject.getEnergyLevel() > 40 && %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 3 )
               %client.mode[ "Mortar" ] = 2 ;
            else
               if( %distToTarg > 80 )
                  %client.mode[ "Mortar" ] = 0 ;
               else
                  %client.mode[ "Mortar" ] = 1 ;
         }
         else if (%hasRocketgun && %distToTarg > 15)
         {
            %useWeapon = "MechRocketgun" ;
         if( %distToTarg > 35 ) 
            %client.mode[ "MechRocketgun" ] = 1 ;
         else
            %client.mode[ "MechRocketgun" ] = 0 ;
         }
         else if (%hasPlasmaCannon)
            %useWeapon = "PlasmaCannon";
         else if (%hasMBCannon)
         {
            %useWeapon = "MBCannon" ;
            %client.mode[ "MBCannon" ] = 0 ;
         }
         else if ( %hasMechMinigun )
            %useWeapon = "MechMinigun";
         else if ( %hasRAXX )
            %useWeapon = "RAXX";
         else if ( %hasRFL )
            %useWeapon = "PCR";
         else
            %useWeapon = "NoAmmo";
   }

// ai proficiency modulator
   %skill = %client.getSkillLevel() ;
   if( %skill !$=  "" )
   {
      if( %client.baseSkill $= "" )
         %client.baseSkill = %skill ;
      else if( %client.player.getMountedImage( 0 ).item != %useWeapon )
         %skill = ( 2 * %client.baseSkill + %skill ) / 3 ;
      %marksman = false ;
      %adjustMark = ( 1.05 + %client.baseSkill ) / 2 ;
      if( %mySpeed < 0.25 )
      {
         if( %skill < %adjudstMark )
            %client.setSkillLevel( %skill + ( %adjustMark - %client.baseSkill ) / 250 ) ;
         %marksman = true ;
      }
      if( %speed < 0.25 )
      {
         if( %marksman && %skill < 1.05 )
            %client.setSkillLevel( %skill + ( 1.05 - %client.baseSkill ) / 150 ) ;
         else if( %skill < %adjudstMark )
            %client.setSkillLevel( %skill + ( %adjustMark - %client.baseSkill ) / 150 ) ;
         else
            %client.setSkillLevel( 99 ) ;
      }
      else 
         if( %marksman )
         {
            %skill /= 1.025 ;
            if( %skill != %client.baseSkill )
               if( %skill > %client.baseSkill ) 
                  %client.setSkillLevel( %skill ) ;
               else
                  %client.setSkillLevel( %client.baseSkill ) ;
         }
         else 
            %client.setSkillLevel( %client.baseSkill ) ;
   }
   else
      %client.setSkillLevel( 0.5 ) ;

   //now select the weapon
   switch$ (%useWeapon)
   {
      case "Blaster":
         %client.player.use("Blaster");
         if( %client.mode[ "Blaster" ] == 2 )
            %client.setWeaponInfo( "FissionBolt" , 1, 500, 1, 0.1);
         else
            %client.setWeaponInfo("EnergyBolt", 25, 100, 1, 0.1);    //50

      case "Plasma":
         %client.player.use("Plasma");
         %client.setWeaponInfo("PlasmaBolt", 10, 200);

      case "Chaingun":
         %client.player.use("Chaingun");
         %client.setWeaponInfo("ChaingunBullet", 1, 100, 150);      //150

      case "Disc":
         %client.player.use("Disc");
         if( %client.mode[ "Disc" ] == 2 )
            %client.setWeaponInfo( "DiscProjectile" , 20 , 200 );
         else if( %client.mode[ "Disc" ] == 1 )
            %client.setWeaponInfo( "TurboDisc" , 10 , 300 , 1 , 0.3 );
         else
            %client.setWeaponInfo( "PowerDiscProjectile" , 30 , 125 , 1 , 0.5 );

      case "GrenadeLauncher":
         %client.player.use("GrenadeLauncher");
         if( %client.mode[ "GrenadeLauncher" ] == 2 )
            %client.setWeaponInfo( "RPGGrenade" , 20 , 125 );
         else if( %client.mode[ "GrenadeLauncher" ] == 2 )
            %client.setWeaponInfo( "EMPLauncherGrenade" , 60 , 225 );
         else
            %client.setWeaponInfo( "BasicGrenade" , 40 , 172 );

      case "Mortar":
         %client.player.use("Mortar");
         if( %client.mode[ "Mortar" ] == 3 )
            %client.setWeaponInfo("MortarShot" , 150 , 900 ) ;
         else if( %client.mode[ "Mortar" ] == 1 )
            %client.setWeaponInfo("MortarShot" , 35 , 400 ) ;
         else
            %client.setWeaponInfo("MortarShot" , 125 , 400 ) ;

      case "SniperRifle":
         %client.player.use("SniperRifle");
         if( %client.mode[ "SniperRifle" ] == 1 )
            %client.setWeaponInfo( "Sniper3006Bullet" , 50 , 500 , 1 , 0 , 0.3 ) ;
         else
            %client.setWeaponInfo( "BasicSniperShot" , 50 , 500 , 1 , 0.2 , 0.4 ) ;

      case "ELFGun":
         %client.player.use("ELFGun");
         %client.setWeaponInfo("BasicELF", 25, 45, 90, 0.1);

      case "ShockLance":
         %client.player.use("ShockLance");
         %client.setWeaponInfo("BasicShocker", 0.1, 20, 1, 0.1);         //8

      case "MissileLauncher":
         %client.player.use("MissileLauncher");
         if( %mySpeed < 0.25 )
            %client.setWeaponInfo( "BasicSniperShot" , 50 , 500 ) ;	// wat lol +soph
         else
            %client.setWeaponInfo( "MegaMissile" , 50 , 500 ) ;
            

        //MD2
      case "MBCannon":
         %client.player.use("MBCannon");
         if( %client.mode[ "MBCannon" ] == 1 || %client.mode[ "MBCannon" ] == 5 )
            %client.setWeaponInfo( "MitziSingularityBlast" , 25, 250);
         else if( %client.mode[ "MBCannon" ] == 4 )
            %client.setWeaponInfo( "MitziAnnihalator" , 50, 150 ) ;
         else
            %client.setWeaponInfo( "MitziBlast" , 15, 125);

     case "AutoCannon":
         %client.player.use("AutoCannon");
         if( %client.mode[ "AutoCannon" ] == 2 )
            %client.setWeaponInfo( "ReaverRocket" , 35 , 200 , 150 ); //150
         else if( %client.mode[ "AutoCannon" ] == 1 )
            %client.setWeaponInfo( "AutoCannonAPBullet", 5, 150, 150 ); //150
         else
            %client.setWeaponInfo( "AutoCannonBullet", 5, 350, 150 ); //150

     case "Protron":
         %client.player.use("Protron");
         if( %client.mode[ "Protron" ] == 2 )
            %client.setWeaponInfo( "ProtronFireBolt", 1, 35, 150); //150
         else if( %client.mode[ "Protron" ] == 1 )
            %client.setWeaponInfo( "PulseBolt", 20, 200, 150); //150
         else
            %client.setWeaponInfo( "ProtronBoltR", 15, 300, 150); //150

      case "MechMiniGun":
         %client.player.use("MechMiniGun");
         %client.setWeaponInfo("MechMiniBullet" , 1 , 125 , 175 );  //150

      case "MechRocketGun":
         %client.player.use("MechRocketGun");
         if( %client.mode[ "MechRocketGun" ] ) 
            %client.setWeaponInfo("MechHowitzerMissile" , 25 , 700 ); 
         else
            %client.setWeaponInfo("MechRocket" , 25 , 250 );

      case "RAXX":
         %client.player.use("RAXX");
         //%client.pressFire(15);
        %client.setWeaponInfo("RAXXFlame", 10 , 50 , 75 );

      case "EnergyRifle":
         %client.player.use("EnergyRifle");
         %client.setWeaponInfo("EnRifleBlast", 15, 225, 100);

      case "Multifusor":
         %client.player.use("MultiFusor");
         %client.setWeaponInfo("DiscProjectile" , 10 , 350 );    //75

       case "PlasmaCannon":
         %client.player.use("PlasmaCannon");
         %client.setWeaponInfo("PlasmaCannonBolt" , 20, 300 );

      case "PCR":
         %client.player.use("PCR");
           %client.setWeaponInfo("PCRBolt" , 20 , 175 );

// special cases
      case "RepairPack":
         if (%player.getImageState($BackpackSlot) $= "Idle")
	    %player.use( "RepairPack" );
         %client.setWeaponInfo( "DefaultRepairBeam" , 40 , 75 , 300 , 0.1 );

      case "Reassembler" :
         if( %client.player.getMountedImage( 0 ) !$= "ReassemblerImage" )
         {
            %client.player.unmountImage( 0 ) ;
            %client.player.mountImage( ReassemblerImage , 0 ) ;
         }
         %client.setWeaponInfo( "DefaultRepairBeam" , 40 , 75 , 300 , 0.1 );

      case "TargetingLaser":
         if( %client.player.getMountedImage( 0 ) !$= "TargetingLaserMountImage" )
         {
            %client.player.unmountImage( 0 ) ;
            %client.player.mountImage( TargetingLaserMountImage , 0 ) ;
         }
         %client.setWeaponInfo("BasicTargeter", 20, 500, 300, 0.1);

      case "NoAmmo":
         %client.setWeaponInfo("NoAmmo", 30, 75);
   }
}

function AIChooseEngageWeapon(%client, %targetClient, %distToTarg, %canUseEnergyStr, %environmentStr)
{
	//get some status
   %player = %client.player;
	if (!isObject(%player))
		return;

   %enemy = %targetClient.player;
	if (!isObject(%enemy))
		return;
	// Idea 1
     // %mask = $TypeMasks::StaticShapeObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType;
	//  %start = %enemy.getWorldBoxCenter();
     // %distance = mFloor(VectorDist(%start, 15));
   //   %end = getWord(%start, 0) SPC getWord(%start, 1) SPC getWord(%start, 2) - 10;

  // %grounded = InitContainerRadiusSearch(%start, %end, $TypeMasks::TerrainObjectType);
   //%grounded = containerRayCast(%start, %end, $TypeMasks::TerrainObjectType, 0);
   // if (%grounded == 0 )
       //  %grounded = true;
        // else
      //   %grounded = false;
     // echo(%mask@" I am checking?");
    //  echo(%grounded@" I am ground");
    //  echo(%distance@" i am far");

    //
   // %playerDataBlock = %client.player.getDataBlock();
	%armor = %targetClient.player.getArmorSize();
	//%type = %item.getDataBlock().getName();
    //  echo(%armor);
	%canUseEnergy = (%canUseEnergyStr $= "true") && !%enemy.isEMP ;		// (%canUseEnergyStr $= "true"); -soph
	%inWater = (%environmentStr $= "water");
	%outdoors = (%environmentStr $= "outdoors");
	%targVelocity = %targetClient.player.getVelocity();
	%targEnergy = %targetClient.player.getEnergyPercent();
	%targDamage = %targetClient.player.getDamagePercent();
	%myEnergy = %player.getEnergyPercent();
	%myDamage = %player.getDamagePercent();

   //get our inventory
   %hasBlaster = (%player.getInventory("Blaster") > 0) && %canUseEnergy;
   %hasPlasma = (%player.getInventory("Plasma") > 0) && (%player.getInventory("PlasmaAmmo") > 0) && !%inWater;
   %hasChain = (%player.getInventory("Chaingun") > 0) && (%player.getInventory("ChaingunAmmo") > 15);			// > 0); -soph
   %hasDisc = (%player.getInventory("Disc") > 0) && (%player.getInventory("DiscAmmo") > 0);
   %hasGrenade = (%player.getInventory("GrenadeLauncher") > 0) && (%player.getInventory("GrenadeLauncherAmmo") > 0);
   %hasSniper = (%player.getInventory("SniperRifle") > 0) && %canUseEnergy && !%inWater;				// && (%player.getInventory("EnergyPack") > 0) && %canUseEnergy && !%inWater; -soph
   %hasELF = (%player.getInventory("ELFGun") > 0) && %canUseEnergy && !%inWater;
   %hasMortar = (%player.getInventory("Mortar") > 0) && (%player.getInventory("MortarAmmo") > 0);
   %hasMissile = (%player.getInventory("MissileLauncher") > 0) && (%player.getInventory("MissileLauncherAmmo") > 0);
   %hasLance = (%player.getInventory("ShockLance") > 0) && %canUseEnergy && !%inWater;
   //MD2
   %hasMBCannon = (%player.getInventory("MBCannon") > 0) && (%player.getInventory("MBCannonCapacitor") > 10 ) ;		// > 0); -soph
   %hasPlasmaCannon = (%player.getInventory("PlasmaCannon") > 0) && %canUseEnergy;
   %hasProtron = (%player.getInventory("Protron") > 0) && (%player.getInventory("ProtronAmmo") > 25 ) ; 		// > 0); -soph
   %hasEnergyRifle = (%player.getInventory("EnergyRifle") > 0) && (%player.getInventory("EnergyRifleAmmo") > 0);
   %hasRAXX = (%player.getInventory("RAXX") > 0) && (%player.getInventory("RAXXAmmo") > 0) && !%inWater;
   %hasMechMinigun = (%player.getInventory("MechMinigun") > 0) && (%player.getInventory("MechMinigunAmmo") > 31);	// > 0); -soph
   %hasRocketgun = (%player.getInventory("MechRocketGun") > 0) && (%player.getInventory("MechRocketgunAmmo") > 1);	// > 0); -soph
   %hasRFL = (%player.getInventory("PCR") > 0) && (%player.getInventory("PCRAmmo") > 7 ) ;				// > 0); -soph
   %hasMultifusor = (%player.getInventory("Multifusor") > 0) && (%player.getInventory("DiscAmmo") > 0);
   %hasAutocannon = (%player.getInventory("AutoCannon") > 0) && (%player.getInventory("AutoCannonAmmo") > 7);		// +soph

   //choose the weapon
   //%hasMBCannon2 = %hasMBCannon;

// REWRITE BEGINS +[soph]

// first we need a few more parameters to work with...
   %useWeapon = "NoAmmo";
   %vehicleMount = %enemy.isMounted() ? %enemy.getObjectMount() : 0 ;
   %playerClass = %player.getArmorSize() ;

   %speed = VectorDist("0 0 0", %targVelocity);
   %mySpeed = VectorDist( "0 0 0" , %client.player.getVelocity() ) ;

   %playerZ = getWord(%player.getTransform(), 2);
   %targetZ = getWord(%enemy.getTransform(), 2);
   %targetZVel = getWord(%targVelocity, 2);

   %start = %enemy.getPosition() ;
   %start = getWord( %start , 0 ) SPC getWord( %start , 1 ) SPC getWord( %start , 2 ) + 1 ; 
   %end = getWord( %start , 0 ) SPC getWord( %start , 1 ) SPC getWord( %start , 2 ) - 4 ;
   %zVelTolerance = 3 ;
   %mask = $TypeMasks::StaticShapeObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType ;
   if( !%grounded )
   {
      %end = getWord( %start , 0 ) SPC getWord( %start , 1 ) SPC getWord( %start , 2 ) - 10 ;
      %grounded = containerRayCast( %start , %end , %mask , %enemy ) ;
      %zVelTolerance = 0 ;
   }
   if( !%grounded )
   {
      InitContainerRadiusSearch( %start , 5 , %mask ) ;
      %grounded = ContainerSearchNext() ;
      %zVelTolerance = 1.5 ;
   }
   if( %grounded && %targetZVel < %zVelTolerance )
      %grounded = true ;

   if( %playerZ + 10 > %targetZ && %grounded && %targetZVel > -20 )
      %groundFodder = true ;

// weapon selection
// vehicles
   if ( %enemy.getHeat() > 0.45 || %vehicleMount )
      if( %hasMissile && %distToTarg > 40 )
      {
         %useWeapon = "MissileLauncher";
         if( %speed > 200 || %distToTarg > 400 )
            %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 1 : 6 ;	// fast or clus
         else if( %distToTarg > 250 )
            if( %speed > 100 )
               %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 1 : 6 ;	// standard or mega
            else if( %speed > 50 )
               if( !%vehicleMount && !%enemy.isEMP )
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) + 1 ;	// fast or emp
               else if( %vehicleMount && !%vehicleMount.isEMP )
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) + 1 ;	// fast or emp
               else
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ;		// std or fast
            else
               %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 0 : 4 ;	// standard or mega
         else
            if( %speed > 30 )
               if( !%enemy.isEMP )
                  %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 0 : 2 ;	// std, emp
               else
                  %client.mode[ "MissileLauncher" ] = 0 ;			// std
            else
               %client.mode[ "MissileLauncher" ] = getRandom( 1 ) ? 0 : 4 ;	// standard or mega
         if( %enemy.getArmorType() & $ArmorMask::Blastech && %client.mode[ "MissileLauncher" ] != 2 )
            %client.mode[ "MissileLauncher" ] = getRandom( 2 ) ? 3 : 2 ;	
      }
      else if( %hasAutocannon && %distToTarg > 50 && %distToTarg < 190 && %myEnergy > 0.1 && ( !( %enemy.getArmorType() & $ArmorMask::Blastech ) || %vehicleMount ) )
      {
         %useWeapon = "AutoCannon";
         %client.mode[ "AutoCannon" ] = 2 ;
      }
      else if( %hasRocketgun && %distToTarg > 125 )
      {
         %useWeapon = "MechRocketgun" ;
         %client.mode[ "MechRocketgun" ] = 1 ;
      }
      else if( %hasMechMinigun && %vehicleMount && %distToTarg < 100 )
         %useWeapon = "MechMinigun" ;
      else if( %hasAutocannon )
      {
         %useWeapon = "AutoCannon";
         if( %distToTarg < 65 )
            %client.mode[ "AutoCannon" ] = 1 ;
         else
            if( %enemy.getArmorType() & $ArmorMask::Blastech )
               if( getRandom( 2 ) )
               {
                  %client.mode[ "AutoCannon" ] = 0 ;
                  %badChoice = 1 ;
               }
               else 
                  %client.mode[ "AutoCannon" ] = 1 ;
            else 
               %client.mode[ "AutoCannon" ] = 0 ;
      }
      else if( %hasRocketgun )
      {
         %useWeapon = "MechRocketgun" ;
         if( %distToTarg > 50 )
            %client.mode[ "MechRocketgun" ] = 1 ;
         else
            %client.mode[ "MechRocketgun" ] = 0 ;
      }

// non-vehicle far targets
   else if( %distToTarg > 300 - ( %client.baseSkill * 50 ) )
      if( %hasMortar && %groundFodder && !( %enemy.getArmorType() & $ArmorMask::Blastech && %enemy.getEnergyLevel() > 15 ) )
      {
         %useWeapon = "Mortar" ;
         if( %enemy.isShielded && %enemy.getEnergyLevel() > 90 && %playerClass $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1 )
            %client.mode[ "Mortar" ] = 2 ;
         else if( !%enemy.isEMP && %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1 )
            %client.mode[ "Mortar" ] = getRandom( 2 ) ? 2 : 0 ;
         else
            %client.mode[ "Mortar" ] = 0 ;
      }
      else if( %hasSniper )
      {
         %useWeapon = "SniperRifle" ;
         if( %targDamage > 0.85 ) 
            %client.mode[ "SniperRifle" ] = 1 ;
         else if( %myEnergy > ( ( 1 - %targDamage ) / 1.5 ) && %player.getInventory( "EnergyPack" ) > 0 )
            %client.mode[ "SniperRifle" ] = 0 ;
         else if( %player.getInventory( "Sniper3006Ammo" ) > 0 )
            %client.mode[ "SniperRifle" ] = 2 ;
         else
            %client.mode[ "SniperRifle" ] = 1 ;
      }
      else if( %hasGrenade && %distToTarg < 350 && %groundFodder && !( %enemy.getArmorType() & $ArmorMask::Blastech && %enemy.getEnergyLevel() > 15 ) )
      {
         %useWeapon = "GrenadeLauncher" ;
         if( %enemy.isShielded && %enemy.getEnergyLevel() > 20 && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
            %client.mode[ "GrenadeLauncher" ] = 1 ;
         else if( !%enemy.isEMP && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
            %client.mode[ "GrenadeLauncher" ] = getRandom( 2 ) ? 0 : 1 ;
         else
            %client.mode[ "GrenadeLauncher" ] = 0 ;
      }
      else if( %hasRocketgun )
      {
         %useWeapon = "MechRocketgun" ;
         %client.mode[ "MechRocketgun" ] = 1 ;
      }
      else if( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 15 )
         if( %hasMortar && %player.getArmorSize() $= "BattleAngel" && %grounded && %player.getInventory( "MortarAmmo" ) > 1 )
         {
            %useWeapon = "Mortar" ;
            %client.mode[ "Mortar" ] = 2 ;
         }
         else if( %hasGrenade && %groundFodder && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
         {
            %useWeapon = "GrenadeLauncher" ;
            %client.mode[ "GrenadeLauncher" ] = 1 ;
         }

// close targets
   if( %useWeapon $= "NoAmmo" && %distToTarg < 30 + ( %client.baseSkill * 10 ) )
      if( %hasRAXX )					
      {
         if( %speed < 15 && %distToTarg > 10 )					// roast the slow ones
            %useWeapon = "RAXX" ;
         else 
            if( %hasMechMinigun )						// rail the fast ones
               %useWeapon = "MechMinigun" ;
            else if( %hasAutocannon )
            {									// no mmg; try atc
               %useWeapon = "AutoCannon" ;
               %client.mode[ "AutoCannon" ] = 1 ;
            }
            else
               %useWeapon = "RAXX" ;						// oh well, back to the flamer
        // %client.pressFire(15) ;
      }
      else if( %hasMechMinigun )
         %useWeapon = "MechMinigun" ;
      else if( %hasLance && %distToTarg < 20  )
      {
         %useWeapon = "ShockLance" ;
         if( %enemy.isMounted() ) 
            %client.mode[ "ShockLance" ] = 0 ;					// vehicle- damage/impulse
         else if( !%enemy.isFlaming ) 
            %client.mode[ "ShockLance" ] = getRandom( 1 ) ;			// !flaming- roll thermal
         else if( !%enemy.isPoisoned )
            %client.mode[ "ShockLance" ] = getRandom( 1 ) ? 0 : 2 ;		// !poisoned- roll toxic
         else
            %client.mode[ "ShockLance" ] = 0 ;					// default
      }
      else if( %hasProtron )
      {
         %useWeapon = "Protron" ;
         %client.mode[ "Protron" ] = 2 ;
      }
      else if( %hasEnergyRifle )
      {
         %useWeapon = "EnergyRifle" ;
         %client.mode[ "EnergyRifle" ] = 1 ;
      }
      else if( %hasAutocannon )
      {
         %useWeapon = "AutoCannon";
         %client.mode["AutoCannon"] = 1 ;
      }
      else if( %hasELF && !%enemy.isSyn )
         %useWeapon = "ELFGun" ;

// shielded targets
   if( %useWeapon $= "NoAmmo" && %enemy.isShielded )
      if( %hasGrenade && %distToTarg > 60 && %groundFodder && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
      {
         %useWeapon = "GrenadeLauncher" ;
         %client.mode[ "GrenadeLauncher" ] = getRandom( 1 ) ;
      }
      else if( %hasMortar && %playerClass $= "BattleAngel" && %groundFodder && %distToTarg > 90 && %player.getInventory( "MortarAmmo" ) > 1 )
      {
         %useWeapon = "Mortar" ;
         %client.mode[ "Mortar" ] = getRandom( 1 ) ? 0 : 2 ;
      }
      else if( %hasProtron )
      {
         %useWeapon = "Protron" ;
         %client.mode[ "Protron" ] = getRandom( 1 ) ;
      }
      else if( %hasBlaster && %distToTarg < 60 )
      {
         %useWeapon = "Blaster" ;
         %client.mode[ "Blaster" ] = 0;
      }

// default logic
   if( %useWeapon $= "NoAmmo" )
      if( %hasMBCannon && %distToTarg <= 140 &&
          %player.getInventory( "MBCannonCapacitor" ) >= 100 &&
          ( %player.getArmorSize() $= "BattleAngel" || 
          ( %player.getArmorSize() $= "Heavy" && %mySpeed < 3 && %myEnergy > 0.35 && %myDamage > 0.75 ) ) && 
          ( %targDamage < 0.5 || %myDamage > 0.75 ) &&
          !%inWater && %outdoors )
      {
         %useWeapon = "MBCannon" ;
         %client.mode[ "MBCannon" ] = 4 ;
         if( %player.getArmorSize() $= "Heavy" )
         {
            %escapePosition = vectorSub( %muzzlePoint , vectorScale( %vectorToTarget , 15 ) ) ;
            %escapePosition = getWords( %escapePosition , 0 , 1 ) SPC ( getWord( %escapePosition , 2 ) + 5 ) ;
            %client.stepMove( %escapePosition , 10 , $AIModeExpress ) ;
         }
      }
      else if( %hasPlasmaCannon && %myEnergy > 0.8 && %player.isShielded && %distToTarg > 125 )
         %useWeapon = "PlasmaCannon";
      else if( %hasMechMinigun && ( %distToTarg < 75 || ( %distToTarg < 125 && %targDamage > 0.9 ) ) )
         %useWeapon = "MechMinigun" ;
      else if( %hasRocketgun && %targDamage < 0.35 )
      {
         %useWeapon = "MechRocketgun" ;
         if( %distToTarg > 150 )
            %client.mode[ "MechRocketgun" ] = 1 ;
         else if( %enemy.getArmorType() & $ArmorMask::Blastech  )
            %client.mode[ "MechRocketgun" ] = getRandom( 1 ) ;
         else
            %client.mode[ "MechRocketgun" ] = 0 ;
      }
      else if( %hasAutocannon )
      {
         %useWeapon = "AutoCannon" ;
         if( %enemy.getArmorType() & $ArmorMask::Blastech  )
            %client.mode[ "AutoCannon" ] = getRandom( 1 ) ;
         else
            %client.mode[ "AutoCannon" ] = 0 ;
      }
      else if( %hasMortar && %groundFodder && !( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 30 ) )
      {
         %useWeapon = "Mortar" ;
         if( %distToTarg > 90 )
            if( %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1 ) 
               if( !%enemy.isEMP )
                  %client.mode[ "Mortar" ] = getRandom( 2 ) ? 2 : 0 ;
               else 
                  %client.mode[ "Mortar" ] = 0 ;
            else
               %client.mode[ "Mortar" ] = 0 ;
         else
            %client.mode[ "Mortar" ] = 1 ;
      }
      else if( %hasMultiFusor )
      {
         %useWeapon = "MultiFusor" ;
         if( %enemy.getArmorType() & $ArmorMask::Blastech  )
            %client.mode[ "MultiFusor" ] = 1 ;
         else 
            %client.mode[ "MultiFusor" ] = 0 ;
      }
      else if( %hasPlasmaCannon )
         %useWeapon = "PlasmaCannon";
      else if( %hasRocketgun )			// nonconditional rehash
      {
         %useWeapon = "MechRocketgun" ;
         if( %distToTarg > 200 )
            %client.mode[ "MechRocketgun" ] = 1 ;
         else if( ( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 15 ) || ( %distToTarg > 75 && %targDamage < 0.6 ) )
            %client.mode[ "MechRocketgun" ] = getRandom( 1 ) ;
         else
            %client.mode[ "MechRocketgun" ] = 0 ;
      }
      else if( %hasSniper && %distToTarg > 100 && %myEnergy > 0.15 && !%vehicleMount )
      {
         %useWeapon = "SniperRifle" ;
         if( %targDamage > 0.85 ) 
            %client.mode[ "SniperRifle" ] = 1 ;
         else if( %myEnergy > ( 1 - %targDamage ) && %player.getInventory( "EnergyPack" ) > 0 )
            %client.mode[ "SniperRifle" ] = 0 ;
         else if( %player.getInventory( "Sniper3006Ammo" ) > 0 )
            %client.mode[ "SniperRifle" ] = 2 ;
         else
            %client.mode[ "SniperRifle" ] = 1 ;
      }
      else if( %hasGrenade && !( %enemy.getArmorType() & $ArmorMask::Blastech && %enemy.getEnergyLevel() > 10 ) && %distToTarg < 150 )
      {
         %useWeapon = "GrenadeLauncher";
         if( ( %distToTarg < 75 || !%groundFodder ) && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 ) 
            %client.mode[ "GrenadeLauncher" ] = 2 ;
         else
            %client.mode[ "GrenadeLauncher" ] = 0 ;
      }
      else if( %hasRFL && %groundFodder && %targDamage > 0.6 )
         %useWeapon = "PCR" ;
      else if( %hasEnergyRifle )
      {
         %useWeapon = "EnergyRifle" ;
         if( %distToTarg < 50 )
            %client.mode[ EnergyRifle] = 1 ;
         else
            %client.mode[ EnergyRifle] = 0 ;
      }
      else if( %hasChain && ( %distToTarg < 100 || %targDamage > 0.8 ) )
      {
         %useWeapon = "Chaingun" ;
         if( ( !%enemy.isShielded || %enemy.isEMP ) && !%enemy.isMounted() && !%enemy.isPoisoned && %player.getInventory( "ChaingunAmmo" ) > 100 )
            %client.mode["Chaingun"] = getRandom( 1 ) ;
         else
            %client.mode["Chaingun"] = 0 ;
      }
      else if( %hasPlasma )
         %useWeapon = "Plasma" ;
      else if( %hasProtron )
      {
         %useWeapon = "Protron" ;
         if( %enemy.isShielded )
            %client.mode[ "Protron" ] = 1 ;
         else if( !%enemy.isEMP && %groundFodder )
            %client.mode[ "Protron" ] = getRandom( 1 ) ;
         else
            %client.mode[ "Protron" ] = 0 ;
      }
      else if( %hasDisc && !( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 15 ) )
      {
         %useWeapon = "Disc" ;
         if( %distToTarg > 125 && ( !%enemy.isShielded || %enemy.isEMP ) )
            if( %speed > 15 )
               %client.mode[ "Disc" ] = 1 ;
            else
               %client.mode[ "Disc" ] = getRandom( 1 ) ;
         else
            if( %myEnergy > 0.6 && %distToTarg > 20 )
               if( %speed < 5 )
                  %client.mode[ "Disc" ] = getRandom( 2 ) ? 2 : 0 ;
               else
                  %client.mode[ "Disc" ] = getRandom( 2 ) ? 0 : 2 ;
            else
               %client.mode[ "Disc" ] = 0 ;
      }
      else if( %hasBlaster )
      {
         %useWeapon = "Blaster" ;
         if( %distToTarg < 75 )
            if( %enemy.isShielded || %vehicleMount )
               %client.mode[ "Blaster" ] = 0;
            else
               %client.mode[ "Blaster" ] = 1;
         else 
            %client.mode[ "Blaster" ] = 2;
      }
      else if( %hasMBCannon )
      {
         %useWeapon = "MBCannon";
         if( %distToTarg < 100 )
            %client.mode["MBCannon"] = 1;
         else
            %client.mode["MBCannon"] = 0;
      }
      else if( %hasRFL )
         %useWeapon = "PCR" ;
      else if( %hasChain )
      {
         %useWeapon = "Chaingun" ;
         %client.mode["Chaingun"] = 0 ;
      }
      else if( %hasGrenade && !( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 10 ) )	// nonconditional rehash
      {
         %useWeapon = "GrenadeLauncher";
         if( %distToTarg < 100 && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 ) 
            %client.mode[ "GrenadeLauncher" ] = 2 ;
         else
            %client.mode[ "GrenadeLauncher" ] = 0 ;
      }
      else if( %hasMortar && !( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 15 ) )	// nonconditional rehash
      {
         %useWeapon = "Mortar" ;
         if( %distToTarg > 125 )
            if( %enemy.isShielded && !%enemy.isEMP && %player.getArmorSize() $= "BattleAngel" && %player.getInventory( "MortarAmmo" ) > 1 )
               %client.mode["Mortar"] = 2;
            else
               %client.mode["Mortar"] = 0;
         else
            %client.mode["Mortar"] = 1;
      }
      else if( %enemy.getArmorType() & $ArmorMask::Blastech  && %enemy.getEnergyLevel() > 15 )
         if( %hasMortar && %player.getArmorSize() $= "BattleAngel" && %groundFodder && %player.getInventory( "MortarAmmo" ) > 1 )
         {
            %useWeapon = "Mortar" ;
            %client.mode[ "Mortar" ] = 2 ;
         }
         else if( %hasGrenade && %groundFodder && %player.getInventory( "GrenadeLauncherAmmo" ) > 1 )
         {
            %useWeapon = "GrenadeLauncher" ;
            %client.mode[ "GrenadeLauncher" ] = 1 ;
         }

// ai proficiency modulator
   %skill = %client.getSkillLevel() ;
   if( %skill !$=  "" )
   {
      if( %client.baseSkill $= "" )
         %client.baseSkill = %skill ;
      else if( %client.player.getMountedImage( 0 ).item != %useWeapon )
         %skill = ( 2 * %client.baseSkill + %skill ) / 3 ;
      %marksman = false ;
      %adjustMark = ( 1.05 + %client.baseSkill ) / 2 ;
      if( %mySpeed < 0.25 )
      {
         if( %skill < %adjudstMark )
            %client.setSkillLevel( %skill + ( %adjustMark - %client.baseSkill ) / 250 ) ;
         %marksman = true ;
      }
      if( %speed < 0.25 )
      {
         if( %marksman && %skill < 1.05 )
            %client.setSkillLevel( %skill + ( 1.05 - %client.baseSkill ) / 150 ) ;
         else if( %skill < %adjudstMark )
            %client.setSkillLevel( %skill + ( %adjustMark - %client.baseSkill ) / 150 ) ;
         else
            %client.setSkillLevel( 99 ) ;
      }
      else 
         if( %marksman )
         {
            %skill /= 1.025 ;
            if( %skill != %client.baseSkill )
               if( %skill > %client.baseSkill ) 
                  %client.setSkillLevel( %skill ) ;
               else
                  %client.setSkillLevel( %client.baseSkill ) ;
         }
         else 
            %client.setSkillLevel( %client.baseSkill ) ;
   }
   else
      %client.setSkillLevel( 0.5 ) ;

   //now select the weapon
   switch$ (%useWeapon)
   {
      case "Blaster":
         %client.player.use("Blaster");
         if( %client.mode[ "Blaster" ] == 2 )
         {
            %projectile = "FissionBolt" ;
            %minRange = 100 ;
            %maxRange = 250 + ( 250 * %client.baseSkill ) ;
         }
         else
         {
            %projectile = "EnergyBolt" ;
            %minRange = 10 ;
            %maxRange = 75 ;
         }

      case "Plasma":
         %client.player.use("Plasma");
         %projectile = "PlasmaBolt" ;
         %minRange = 25;
         %maxRange = 200 ;

      case "Chaingun":
         %client.player.use("Chaingun");
         %projectile = "ChaingunBullet" ;
         %minRange = 10 ;
         %maxRange = 100 ;
         %triggerCount = 150 ;

      case "Disc":
         %client.player.use("Disc");
         if( %client.mode[ "Disc" ] == 2 )
         {
            %projectile = "DiscProjectile" ;
            %minRange = 25 ;
            %maxRange = 100 ;
         }
         else if( %client.mode[ "Disc" ] == 1 )
         {
            %projectile = "DiscProjectile" ;
            %minRange = 100 ;
            %maxRange = 300 ;
            %triggerCount = 1 ;
            %requiredEnergy = 0.3 ;
         }
         else
         {
            %projectile = "DiscProjectile" ;
            %minRange = 50 ;
            %maxRange = 200 ;
            %triggerCount = 1 ;
            %requiredEnergy = 0.5 ;
         }

      case "GrenadeLauncher":
         %client.player.use("GrenadeLauncher");
         if( %client.mode[ "GrenadeLauncher" ] == 2 )
         {
            %projectile = "RPGGrenade" ;
            %minRange = 25 ;
            %maxRange = 150 ;
         }
         else if( %client.mode[ "GrenadeLauncher" ] == 2 )
         {
            %projectile = "EMPLauncherGrenade" ;
            %minRange = 40 + ( 25 * %client.baseSkill ) ;
            %maxRange = 225 ;
         }
         else
         {
            %projectile = "BasicGrenade" ;
            %minRange = 30 + ( 20 * %client.baseSkill ) ;
            %maxRange = 172 ;
         }

      case "Mortar":
         %client.player.use("Mortar");
         %projectile = "MortarShot" ;
         if( %client.mode[ "Mortar" ] == 1 )
         {
            %minRange = 40 ;
            %maxRange = 300 ;
         }
         else
         {
            %minRange = 150 ;
            %maxRange = 400 - ( 50 * %client.baseSkill ) ;
         }

      case "SniperRifle":
         %client.player.use("SniperRifle");
         if( %client.mode[ "SniperRifle" ] == 1 )
         {
            %projectile = "Sniper3006Bullet" ;
            %minRange = 150 ;
            %maxRange = 300 + ( 600 * %client.baseSkill ) ;
            %triggerCount = 1 ;
            %requiredEnergy = 0 ;
            %errorFactor = 0.3 - ( 0.2 * %client.baseSkill ) ;
         }
         else
         {
            %projectile = "BasicSniperShot" ;
            %minRange = 200 ;
            %maxRange = 200 + ( 400 * %client.baseSkill ) ;
            %triggerCount = 1 ;
            %requiredEnergy = 0.2 ;
            %errorFactor = 0.4 - ( 0.1 * %client.baseSkill ) ;
         }

      case "ELFGun":
         %client.player.use("ELFGun");
         %projectile = "BasicELF" ;
         %minRange = 15 ;
         %maxRange = 30 ;
         %triggerCount = 90 ;
         %requiredEnergy = 0.1 ;

      case "ShockLance":
         %client.player.use("ShockLance");
         %projectile = "BasicShocker" ;
         %minRange = 2.5 ;
         %maxRange = 7.5 + ( 10 * %client.baseSkill ) ;
         %triggerCount = 1 ;
         %requiredEnergy = 0.35 ;

      case "MissileLauncher":
         %client.player.use("MissileLauncher");
         if( %distToTarg < 60 )
         {
            %minRange = 20 ;
            %maxRange = 125 - ( 25 * %client.baseSkill ) ;
            switch$ ( %client.mode[ "MissileLauncher" ] )
            {
               case "1":
                  %projectile = "FastMissile" ;
               case "2":
                  %projectile = "EMPMissile" ;
               case "3":
                  %projectile = "IncendiaryMissile" ;
               case "4":
                  %projectile = "MegaMissile" ;
               default:
                  %projectile = "ShoulderMissile" ;
            }
         }
         else
         {
            %projectile = "BasicSniperShot" ;			// wat lol +soph
            %minRange = 90 ;
            %maxRange = 375 + ( 125 * %client.baseSkill ) ;
         }

        //MD2
      case "MBCannon":
         %client.player.use("MBCannon");
         if( %client.mode[ "MBCannon" ] == 1 || %client.mode[ "MBCannon" ] == 5 )
         {
            %projectile = "MitziSingularityBlast" ;
            %minRange = 25 ;
            %maxRange = 250 ;
         }
         else if( %client.mode[ "MBCannon" ] == 4 )
         {
            %projectile = "MechMortarPomf" ;
            %minRange = 50 ;
            %maxRange = 150 ;
            %client.setWeaponInfo( "MechMortarPomf" , 50, 150 ) ;
            %client.setDangerLocation( %player.getMuzzlePoint( 0 ) , 150 ) ;
            %client.setDangerLocation( vectorAdd( %player.getMuzzlePoint( 0 ) , %player.getMuzzleVector( 0 ) ) , 150) ;
         }
         else
         {
            %projectile = "MitziBlast" ;
            %minRange = 15 ;
            %maxRange = 100 + ( 50 * %client.baseSkill ) ;
         }

     case "AutoCannon":
         %client.player.use("AutoCannon");
         if( %client.mode[ "AutoCannon" ] == 2 )
         {
            %projectile = "ReaverRocket" ;
            %minRange = 15 + ( 15 * %client.baseSkill ) ;
            %maxRange = 250 - ( 50 * %client.baseSkill ) ;
            %triggerCount = 150 ;
            %requiredEnergy = 0.1 ;
         }
         else if( %client.mode[ "AutoCannon" ] == 1 )
         {
            %projectile = "AutoCannonAPBullet" ;
            %minRange = 7 ;
            %maxRange = 150 ;
            %triggerCount = 150 ;
         }
         else
         {
            %projectile = "AutoCannonBullet" ;
            %minRange = 5 ;
            %maxRange = 300 ;
            %triggerCount = 150 ;
         }

     case "Protron":
         %client.player.use("Protron");
         if( %client.mode[ "Protron" ] == 2 )
         {
            %projectile = "ProtronBoltR" ;
            %minRange = 5 ;
            %maxRange = 30 ;
            %triggerCount = 150 ;	// 150 
         }
         else if( %client.mode[ "Protron" ] == 1 )
         {
            %projectile = "PulseBolt" ;
            %minRange = 20 ;
            %maxRange = 200 ;
            %triggerCount = 150 ;	// 150 
         }
         else
         {
            %projectile = "ProtronBoltR" ;
            %minRange = 30 ;
            %maxRange = 300 ; 
            %triggerCount = 150 ;	// 150 
         }

      case "MechMiniGun":
         %client.player.use("MechMiniGun");
         %projectile = "MechMiniBullet" ;
         %minRange = 1 ;
         %maxRange = 175 - ( 50 * %client.baseSkill ) ;
         %triggerCount = 175 ;	// 150 

      case "MechRocketGun":
         %client.player.use("MechRocketGun");
         if( %client.mode[ "MechRocketGun" ] ) 
         {
            %projectile = "MechHowitzerMissile" ;
            %minRange = 300 + ( 600 * %client.baseSkill ) ;
            %maxRange = %minRange + 10 ;
         }
         else
         {
            %projectile = "MechRocket" ;
            %minRange = 40 ;
            %maxRange = 100 + ( 50 * %client.baseSkill );
         }

      case "RAXX":
         %client.player.use("RAXX");
         //%client.pressFire(15);
         %projectile = "RAXXFlame" ;
         %minRange = 10 ;
         %maxRange = 30 + ( 20 * %client.baseSkill ) ;
         %triggerCount = 75 ;

      case "EnergyRifle":
         %client.player.use("EnergyRifle");
         %projectile = "EnRifleBlast" ;
         %minRange = 15 + ( 20 * %client.baseSkill ) ;
         %maxRange = 225 ;
         %triggerCount = 25 ;

      case "Multifusor":
         %client.player.use("MultiFusor");
         %projectile = "DiscProjectile" ;
         %minRange = 20 + ( 30 * %client.baseSkill ) ;
         %maxRange = 275 ;	// 75

       case "PlasmaCannon":
         %client.player.use("PlasmaCannon");
         %projectile = "PlasmaCannonBolt" ;
         %minRange = 30 + ( 40 * %client.baseSkill ) ;
         %maxRange = 325 ;

      case "PCR":
         %client.player.use("PCR");
         %projectile = "PCRBolt" ;
         %minRange = 25 + ( 15 * %client.baseSkill ) ;
         %maxRange = 175 ;

      case "NoAmmo":
         %projectile = "NoAmmo" ;
         %minRange = 500 ;
         %maxRange = 500 ;
         %badChoice = true ;
   }
  
   if( %badChoice )
      %minRange = %distToTarg + 20 ;
   else
      if( %maxRange < %distToTarg )
         if( %distToTarg < 25 ) 
            %maxRange = %distToTarg + 1 ;
         else if( %distToTarg < 75 ) 
            %maxRange = %distToTarg + 2 ;
         else if( %distToTarg < 250 ) 
            %maxRange = %distToTarg + 3 ;
         else 
            %maxRange = %distToTarg + 5 ;
   if( %maxRange > Sky.visibleDistance + 10 )
      %maxRange = Sky.visibleDistance + 10 ;

   if( %triggerCount )
      if( %requiredEnergy )
         if( %errorFactor )
            %client.setWeaponInfo( %projectile , %minRange , %maxRange , %triggerCount , %requiredEnergy , %errorFactor ) ;
         else
            %client.setWeaponInfo( %projectile , %minRange , %maxRange , %triggerCount , %requiredEnergy ) ;
      else
         %client.setWeaponInfo( %projectile , %minRange , %maxRange , %triggerCount ) ;
   else
      %client.setWeaponInfo( %projectile , %minRange , %maxRange ) ;
}

//function is called once per frame, to handle packs, healthkits, grenades, etc...
function AIProcessEngagement(%client, %target, %type, %projectile)
{
   //make sure we're still alive
	if (! AIClientIsAlive(%client))
		return;

	//clear the pressFire
	%client.pressFire(-1);

	//see if we have to use a repairkit
	%player = %client.player;
	if (!isObject(%player))
		return;

	if( %palyer.vehicleTurret )											// +soph
		return ;												// +soph

	if( %client.isMountingVehicle() && isObject( %target ) )
		%client.pressFire( 35 ) ;

//	if (%client.getSkillLevel() > 0.1 && %player.getDamagePercent() > 0.3 && %player.getInventory("RepairKit") > 0)	// -soph
        if( %player.getInventory("RepairKit") > 0 )									// +[soph]
	{														// +
		if( %player.isPoisoned )										// +
		{													// +
			%skillValue = %client.getSkillLevel() * %client.getSkillLevel() * 10 ;				// +
			%skillValue *= %player.lDamageStack[ Poison ] * ( 1 + %player.getDamagePercent() ) ;		// +						// +
			if( getRandom() * 100 < %skillValue )								// +
				%player.use("RepairKit");								// +
		}													// +
		else if( %player.isFlaming || ( %player.isEMP && %player.getDamagePercent() > 0.4 ) )			// +
		{													// +
			%skillValue = %client.getSkillLevel() * %client.getSkillLevel() * 10 ;				// +
			if( getRandom() * 100 < %skillValue )								// +
				%player.use("RepairKit");								// +
		}													// +
	        else if( %player.getDamagePercent() > 0.75 )								// +[/soph]
		{
			//add in a "skill" value to delay the using of the repair kit for up to 10 seconds...
			%elapsedTime = getSimTime() - %client.lastDamageTime;
			%skillValue = (1.2 - %client.getSkillLevel()) * (1.2 - %client.getSkillLevel());
			if (%elapsedTime > (%skillValue * 20000))   //tweaked skill nite
		      %player.use("RepairKit");
		}			
	}

	//see if we've been blinded
	if (%player.getWhiteOut() > 0.6)
		%client.setBlinded(2000);

	//else see if there's a grenade in the vicinity...
	else
	{
		%count = $AIGrenadeSet.getCount();
		for (%i = 0; %i < %count; %i++)
		{
			%grenade = $AIGrenadeSet.getObject(%i);

			//make sure the grenade isn't ours
			if (%grenade.sourceObject.client != %client)
			{
				//see if it's within 15 m
				if (VectorDist(%grenade.position, %client.player.position) < 15)
				{
					%client.setDangerLocation(%grenade.position, 20);	
					break;
				}
			}
		}
	}

	//if we're being hunted by a seeker projectile, throw a flare grenade
	if (%player.getInventory("FlareGrenade") > 0)
	{
		%missileCount = MissileSet.getCount();
		for (%i = 0; %i < %missileCount; %i++)
		{
			%missile = MissileSet.getObject(%i);
			if (%missile.getTargetObject() == %player)
			{
				//see if the missile is within range
				if (VectorDist(%missile.getTransform(), %player.getTransform()) < 50)
				{
					%player.throwStrength = 1.5;
					%player.use("FlareGrenade");
					break;
				}
			}
		}
	}

	//see what we're fighting
	switch$ (%type)
	{
		case "player":
			//make sure the target is alive
			if(AIClientIsAlive(%target))
			{
				%targPos = %target.player.getWorldBoxCenter();			// _here_ +soph
				%clientPos = %player.getWorldBoxCenter();			// _here_ +soph
				//if the target is in range, and within 10-40 m, and heading in this direction, toss a grenade
				if (!$AIDisableGrenades && %client.getSkillLevel() >= 0.3)
				{
					if (%player.getInventory("Grenade") > 0)
						%grenadeType = "Grenade";
					else if (%player.getInventory("FlashGrenade") > 0)
						%grenadeType = "FlashGrenade";
					else if (%player.getInventory("ConcussionGrenade") > 0)
						%grenadeType = "ConcussionGrenade";
                    else if (%player.getInventory("FireGrenade") > 0)
						%grenadeType = "FireGrenade";
                    else if (%player.getInventory("EMPGrenade") > 0)
						%grenadeType = "EMPGrenade";
                    else if (%player.getInventory("PosionGrenade") > 0)
						%grenadeType = "PoisonGrenade";
                   else if (%player.getInventory("MortarGrenade") > 0)
						%grenadeType = "MortarGrenade";
					else %grenadeType = "";
					if (%grenadeType !$= "" && %client.targetInSight())
					{
						//see if the predicted location of the target is within 10m
					//	%targPos = %target.player.getWorldBoxCenter();	// ^up^ -soph
					//	%clientPos = %player.getWorldBoxCenter();	// ^up^ -soph

						//make sure we're not *way* above the target
						if (getWord(%clientPos, 2) - getWord(%targPos, 2) < 3)
						{
							%dist = VectorDist(%targPos, %clientPos);
							%direction = VectorDot(VectorSub(%clientPos, %targPos), %target.player.getVelocity());
							%facing = VectorDot(VectorSub(%client.getAimLocation(), %clientPos), VectorSub(%targPos, %clientPos));
							if (%dist > 20 && %dist < 45 && (%direction > 0.9 || %direction < -0.9) && (%facing > 0.9))
							{
								%player.throwStrength = 1.5;
								%player.use(%grenadeType);
							}
						}
					}
				}

				%pack = %player.getMountedImage( $BackpackSlot ) ;		// +[soph]
				if( %pack && %player.getImageState( $BackpackSlot ) $= "Idle" )	// +
					if( %player.getMountedImage( 7 ).className  $= "WeaponImage" && ( %pack.ammo $= "" || %player.getInventory( %pack.ammo ) > 0 ) )
					{							// + we have a weapon
						%useWeapon = %player.getMountedImage(0).item ;	// +
						if( %useWeapon !$= "GrenadeLauncher" && %useWeapon !$= "Mortar" )
						{						// + not using an arcing weapon
							if( !%facing )				// +
								%facing = VectorDot( VectorSub( %client.getAimLocation() , %clientPos ) , VectorSub( %targPos , %clientPos ) ) ;
							if( %facing > 0.75 )			// +
							{					// + unobstructed target
								%muzzlePoint = %player.getMuzzlePoint( 7 ) ;
								%hitObj = ContainerRayCast( %muzzlePoint , %targPos , $TypeMasks::InteriorObjectType |
														      $TypeMasks::TerrainObjectType | 
														      $TypeMasks::ForceFieldObjectType | 
														      $TypeMasks::StaticShapeObjectType |
														      $TypeMasks::VehicleObjectType , %player ) ;
								%hitObj = getWord( %hitObj , 0 ) ;
	                                                        if( %hitObj == %target.player || %hitObj.team != %player.team )
								{				// + no obstructions
									if( !%dist )		// +
										%dist = VectorDist(%targPos, %clientPos);
									if( %dist > 10 && %dist < Sky.visibleDistance + 10 && %dist < 400 )
									{			// +
										%losLoc = vectorAdd( vectorScale( %player.getMuzzleVector( 7 ) , %dist ) , %muzzlePoint ) ;
										%hitObj = ContainerRayCast( %muzzlePoint , %losLoc , $TypeMasks::InteriorObjectType |
																     $TypeMasks::TerrainObjectType | 
																     $TypeMasks::ForceFieldObjectType | 
																     $TypeMasks::StaticShapeObjectType |
																     $TypeMasks::VehicleObjectType , %player ) ;
										%hitObj = getWord( %hitObj , 0 ) ;
										if( !%hitObj )	// +
										{		// + nothing in the way
											%player.setImageTrigger( $BackpackSlot , true ) ;
											%player.setImageTrigger( $BackpackSlot , false ) ;
										}		// +
										else if( !( %hitObj.getType() & $TypeMasks::InteriorObjectType || %hitObj.getType() & $TypeMasks::TerrainObjectType ) )
											if( %hitObj == %target.player || %hitObj.team != %player.team || !%hitObj.getDatablock().maxDamage )
											{	// + low chance of friendly fire
												%player.setImageTrigger( $BackpackSlot , true ) ;
												%player.setImageTrigger( $BackpackSlot , false ) ;
											}	// +
									}			// +
								}				// +
							}					// +
						}						// +
					}							// +[/soph]

				//see if we have a shield pack that we need to use
				if (%player.getInventory("ShieldPack") > 0)
				{
					if (%projectile > 0 && %player.getImageState($BackpackSlot) $= "Idle")
					{
						%player.use("Backpack");
					}
					else if (%projectile <= 0 && %player.getImageState($BackpackSlot) $= "activate")
					{
						%player.use("Backpack");
					}
				}
			}

		case "object":
                                if( %player.team == %target.team )			// +[soph]
                                {							// + scratch bot teamturretkill
					if( %target.getDataBlock().shapeFile !$= "mine.dts" )
					{						// +
						%client.lastDamageTurret = -1 ;		// + 
						%client.setTargetObject( -1 ) ;		// +
						%client.stop() ;			// +
						AIUnassignClient( %client ) ;		// +
						Game.AIChooseGameObjective( %client ) ;	// +
						return ;				// +
					}						// +
                                        %client.setDangerLocation( %target.getPosition() , 60 ) ;
					%client.schedule( 250 + 750 * %client.baseSkill , pressFire , 5 ) ;
			        }							// +
				%pack = %player.getMountedImage( $BackpackSlot ) ;	// +
				if( %pack )						// +
				       // if( %client.targetInSight() && %player.getMountedImage( 7 ).className  $= "WeaponImage" && ( %pack.ammo $= "" || %player.getInventory( %pack.ammo ) > 0 ) )
					if( %player.getMountedImage( 7 ).className  $= "WeaponImage" && ( %pack.ammo $= "" || %player.getInventory( %pack.ammo ) > 0 ) )
					{						// +
						%player.setImageTrigger( $BackpackSlot , true ) ;
						%player.setImageTrigger( $BackpackSlot , false ) ;
				        }						// +
                                if( %player.getInventory( "Grenade" ) > 0 )		// +
                                        %grenadeType = "Grenade" ;			// +
                                else if( %player.getInventory( "EMPGrenade" ) > 0 )	// +
					%grenadeType = "EMPGrenade" ;			// +
				else if( %player.getInventory( "MortarGrenade" ) > 0 )	// +
					%grenadeType = "MortarGrenade" ;		// +
				else 							// +
					%grenadeType = "" ;				// +
				if( %grenadeType !$= "" && %client.targetInSight() )	// +
				{							// +
					%targPos = %target.getWorldBoxCenter() ;	// +
					%clientPos = %player.getWorldBoxCenter() ;	// +
					if( getWord( %clientPos, 2 ) - getWord( %targPos , 2 ) < 3 )
					{						// +
						%dist = VectorDist( %targPos , %clientPos ) ;
						%facing = VectorDot( VectorSub( %client.getAimLocation() , %clientPos ) , VectorSub( %targPos , %clientPos ) ) ;
						if ( %dist > 20 && %dist < 45 && %facing > 0.9 )
						{					// +
							%player.throwStrength = 1.5 ;	// +
							%player.use(%grenadeType) ;	// +
						}					// +
					}						// +
				}							// +[/soph]

				if( %player.getInventory( "ShieldPack" ) > 0 )
					if (%projectile > 0 && %player.getImageState($BackpackSlot) $= "Idle")
						%player.use("Backpack");
					else if (%projectile <= 0 && %player.getImageState($BackpackSlot) $= "activate")
						%player.use("Backpack");
				else if( %player.getInventory( "CloakingPack" ) > 0 )	// +[soph]
					if( %player.getEnergyLevel() > 25 )		// +
						if ( %projectile <= 1 && %player.getImageState($BackpackSlot) $= "Idle")
							%player.use("Backpack");	// +[/soph]

//				%hasGrenade = %player.getInventory("Grenade");		// -[soph]
//				if (%hasGrenade && %client.targetInRange())		// -
//				{							// -
//					%targPos = %target.getWorldBoxCenter();		// -
//					%myPos = %player.getWorldBoxCenter();		// -
//					%dist = VectorDist(%targPos, %myPos);		// -
//					if (%dist > 5 && %dist < 20)			// -
//					{						// -
//						%player.throwStrength = 1.0;		// -
//						%player.use("Grenade");			// -
//					}						// -
//				}							// -[/soph]

		case "none":
			//use the repair pack if we have one
			if (%player.getDamagePercent() > 0 && %player.getInventory(RepairPack) > 0)
			{
				if (%player.getImageState($BackpackSlot) $= "Idle")
		         %client.player.use("RepairPack");
				else
					//sustain the fire for 30 frames - this callback is timesliced...
					%client.pressFire(30);
			}
//			else if( %player.getInventory( "CloakingPack" ) > 0 )		// +[soph]
//					if( %player.getEnergyLevel() > 25 )
//						if ( %projectile <= 1 && %player.getImageState($BackpackSlot) $= "Idle")
//							%player.use("Backpack");	// +[/soph]
	}
}

function AIFindClosestInventory(%client, %armorChange)
{
	%closestInv = -1;
	%closestDist = 32767;

   %depCount = 0;
   %depGroup = nameToID("MissionCleanup/Deployables");
   if (%depGroup > 0)
      %depCount = %depGroup.getCount();

   // there exists a deployed station, lets find it
   if(!%armorChange && %depCount > 0)
   {
      for(%i = 0; %i < %depCount; %i++)
      {
         %obj = %depGroup.getObject(%i);

         if(%obj.getDataBlock().getName() $= "DeployedStationInventory" && %obj.team == %client.team && %obj.isEnabled())
         {
            %distance = %client.getPathDistance(%obj.getTransform());
            if (%distance > 0 && %distance < %closestDist)
            {
               %closestInv = %obj;
               %closestDist = %distance;
            }
         }
      }
   }

   // still check if there is one that is closer
   %invCount = $AIInvStationSet.getCount();
	for (%i = 0; %i < %invCount; %i++)
	{
		%invStation = $AIInvStationSet.getObject(%i);
		if (%invStation.team <= 0 || %invStation.team == %client.team)
		{
			//error("DEBUG: found an inventory station: " @ %invStation @ "  status: " @ %invStation.isPowered());
			//make sure the station is powered
			if (!%invStation.isDisabled() && %invStation.isPowered())
			{
				%dist = %client.getPathDistance(%invStation.getTransform());
				if (%dist > 0 && %dist < %closestDist)
				{
					%closestInv = %invStation;
					%closestDist = %dist;
				}
			}
		}
	}

	return %closestInv @ " " @ %closestDist;
}

//------------------------------
//find the closest inventories for the objective weight functions
function AIFindClosestInventories(%client)
{
	%closestInv = -1;
	%closestDist = 32767;
	%closestRemoteInv = -1;
	%closestRemoteDist = 32767;

   %depCount = 0;
   %depGroup = nameToID("MissionCleanup/Deployables");

	//first, search for the nearest deployable inventory station
	if (isObject(%depGroup))
	{
	   %depCount = %depGroup.getCount();
	   for (%i = 0; %i < %depCount; %i++)
	   {
	      %obj = %depGroup.getObject(%i);

	      if (%obj.getDataBlock().getName() $= "DeployedStationInventory" && %obj.team == %client.team && %obj.isEnabled())
	      {
	         %distance = %client.getPathDistance(%obj.getTransform());
	         if (%distance > 0 && %distance < %closestRemoteDist)
	         {
	            %closestRemoteInv = %obj;
	            %closestRemoteDist = %distance;
	         }
	      }
	   }
	}

   // now find the closest regular inventory station
   %invCount = $AIInvStationSet.getCount();
	for (%i = 0; %i < %invCount; %i++)
	{
		%invStation = $AIInvStationSet.getObject(%i);
		if (%invStation.team <= 0 || %invStation.team == %client.team)
		{
			//make sure the station is powered
			if (!%invStation.isDisabled() && %invStation.isPowered())
			{
				%dist = %client.getPathDistance(%invStation.getTransform());
				if (%dist > 0 && %dist < %closestDist)
				{
					%closestInv = %invStation;
					%closestDist = %dist;
				}
			}
		}
	}

	//if the regular inv station is closer than the deployed, don't bother with the remote
	if (%closestDist < %closestRemoteDist)
		%returnStr = %closestInv SPC %closestDist;
	else
		%returnStr = %closestInv SPC %closestDist SPC %closestRemoteInv SPC %closestRemoteDist;

	return %returnStr;

}
function Buyme(%buySet)
{
%randNum = getRandom(7);
 //  if (%item $= "BattleAngel" || %item $= "Heavy" || %item $= "Medium" || %item $= "Engineer"   || %item $= "Light" || %item $= "MagIon" ||%item $= "Blastech" )
	if (%randNum < 0)// 0.4)
		%buySet = "Light";// MediumEnergySet HeavyEnergySet";
    else if (%randNum < 1)
        %buySet = "Medium";
    else if (%randNum < 2)
         %buySet = "Heavy";
	else if (%randNum < 3)// 0.6)
		%buySet = "Engineer";// MediumShieldSet HeavyShieldSet";
    else if (%randNum < 4)// 0.6)
		%buySet = "BattelAngel";

    else if (%randNum < 5)// 0.6)
		%buySet = "Blastech";
     else if (%randNum < 6)// 0.6)
		%buySet = "MagIon";
   // $byme = %buySet;
    //Echo(%buySet);
}

//------------------------------
//AI Equipment Configs
//--------------------------------
$EquipConfigIndex = -1;									// +[soph]
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "BattleAngel" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "RandomPack" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MechMinigun" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MechMinigunAmmo" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "AutoCannon" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "AutoCannonAmmo" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "RAXX" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "RAXXAmmo" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MechRocketGun" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MechRocketGunAmmo" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "Mortar" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MortarAmmo" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MissileLauncher" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "MissileLauncherAmmo" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "RepairKit" ;
$AIEquipmentSet[BASiegeSet, $EquipConfigIndex++] = "FlareGrenade" ;			// +[/soph]

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "RandomPack" ;			// "GravitronPack"; -soph
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "PCR"; 
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "PCRAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "FireGrenade";
$AIEquipmentSet[HeavyAmmoSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "BattleAngel";
//$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "ShieldPack";		
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MechMinigun";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MechMinigunAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "PlasmaCannon";
//$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RFL";
//$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RFLCapacitor";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RAXX";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RAXXAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MechRocketGun";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MechRocketGunAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MissileLauncher" ;		// "Multifusor"; -soph
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "MissileLauncherAmmo" ;		// "DiscAmmo"; -soph
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Grenade";
//$AIEquipmentSet[HeavyShieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;									// +[soph]
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "BattleAngel" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "AmmoPack" ;		
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MechMinigun" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MechMinigunAmmo" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "AutoCannon" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "AutoCannonAmmo" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "RAXX" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "RAXXAmmo" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MechRocketGun" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MechRocketGunAmmo" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "Mortar" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MortarAmmo" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MissileLauncher" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "MissileLauncherAmmo" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "RepairKit" ;
$AIEquipmentSet[BAAmmoSet, $EquipConfigIndex++] = "Grenade" ;				// +[/soph]

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "RandomPack" ;			// "EnergyPack"; -soph
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "MBCannonCapacitor" ;
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "PCR";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "PCRAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "GrenadeLauncher" ;		// "PlasmaCannon"; -soph
//$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "GrenadeLauncherAmmo" ;	// -soph
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "EmpGrenade";
$AIEquipmentSet[HeavyEnergySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "AmmoPack" ;			// "GravitronPack"; -soph
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "MissileLauncher" ;		// "PCR"; -soph
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "MissileLauncherAmmo";		// "PCRAmmo"; -soph
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "GrenadeLauncher";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "GrenadeLauncherAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "PoisonGrenade";
$AIEquipmentSet[HeavyRepairSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "HeatShieldPack" ;		// "EnergyPack"; -soph
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;	// +soph
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Multifusor" ;		// "Disc"; -soph
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "GrenadeLauncher";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "GrenadeLauncherAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Mortar";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "MortarAmmo";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "EMPGrenade";
$AIEquipmentSet[HeavyIndoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Heavy";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "SensorJammerPack" ;		// "EnergyPack"; -soph
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "GrenadeLauncher";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "GrenadeLauncherAmmo";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "PlasmaCannon" ;		// +soph
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "FireGrenade";
$AIEquipmentSet[HeavyInventorySet, $EquipConfigIndex++] = "Mine";

//------------------------------

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "AutoCannon" ;			// "PlasmaCannon"; -soph
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "AutoCannonAmmo" ;		// +soph
//$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Ammo";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "EMPGrenade";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "ShieldPack";
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Chaingun" ;			// "Disc"; -soph
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "ChaingunAmmo" ;		// "DiscAmmo"; -soph
//$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "PlasmaCannon";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "GrenadeLauncher" ;		// +soph
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "GrenadeLauncherAmmo" ;		// +soph
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "FireGrenade";
$AIEquipmentSet[MediumShieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "HeatShieldPack" ;		// "EnergyPack"; -soph
$AIEquipmentSet[MediumRepairSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "MissileLauncher" ;		// "PlasmaCannon"; -soph
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "MissileLauncherAmmo" ;		// +soph
//$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "EmpGrenade";
$AIEquipmentSet[MediumEnergySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Medium";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "RandomPack" ;			// "EnergyPack"; -soph 
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Autocannon" ;			// "MBCannon"; -soph
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "AutocannonAmmo" ;		// "MBCannonCapacitor"; -soph
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "MissileLauncher";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "MissileLauncherAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "FireGrenade";
$AIEquipmentSet[MediumMissileSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "Engineer";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "TurretOutdoorDeployable";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;	// +soph
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "Multifusor" ;		// "Disc"; -soph
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "PCR" ;			// +soph
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "PCRAmmo" ;		// +soph
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "ELFGun";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[MediumOutdoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Engineer";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "TurretIndoorDeployable";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;	// +soph
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "MissileLauncher" ;	// +soph
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "MissileLauncherAmmo" ;	// +soph
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Reassembler";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[MediumIndoorTurretSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Engineer";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "InventoryDeployable";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "SubspaceRegenMod" ;		// +soph
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "MultiFusor" ;		// "Disc"; -soph
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Protron";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "ProtronAmmo";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "AutoCannon";		// +soph
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "AutoCannonAmmo";		// +soph
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Reassembler";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "ConcussionGrenade";
$AIEquipmentSet[MediumInventorySet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "Engineer" ;			// +[soph]
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "AmmoPack" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "SubspaceRegenMod" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "MBCannon" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "MBCannonCapacitor" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "Chaingun" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "ChaingunAmmo" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "Protron" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "ProtronAmmo" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "MultiFusor" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "DiscAmmo" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "AutoCannon" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "AutoCannonAmmo" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "Reassembler" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "RepairKit" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "EMPGrenade" ;
$AIEquipmentSet[EngineerRepairSet, $EquipConfigIndex++] = "Mine" ;			// +[/soph]

//------------------------------

$EquipConfigIndex = -1;  // what does this do?
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "SensorJammerPack" ;		// "EnergyPack"; -soph
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "SniperRifle" ;		// "MBCannon"; -soph
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Sniper3006Ammo" ;		// "MBCannonCapacitor"; -soph
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "FireGrenade";
$AIEquipmentSet[LightEnergyDefault, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Shocklance";
//$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "SniperRifle";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Sniper3006Ammo";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "FireGrenade";
$AIEquipmentSet[LightEnergySniper, $EquipConfigIndex++] = "Mine";
//
$EquipConfigIndex = -1;
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "RandomPack" ;			// "AmmoPack"; -soph DiscPackAmmo
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "Disc" ;				// "MBCannon"; -soph
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "DiscAmmo" ;			// "MBCannonCapacitor"; -soph
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "EnergyRifleAmmo";
//$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Chaingun";
//$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "ChaingunAmmo";
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "GrenadeLauncher" ;		// +soph
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "GrenadeLauncherAmmo" ;		// +soph
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "Grenade" ;			// "FlareGrenade"; -soph
$AIEquipmentSet[MagIonEnergy, $EquipConfigIndex++] = "Mine";

//
$EquipConfigIndex = -1;
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "Blastech";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "RandomPack" ;			// "AmmoPack"; -soph
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "GrenadeLauncher" ;			// +soph
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "GrenadeLauncherAmmo" ;		// +soph
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "MortarGrenade";
$AIEquipmentSet[BlastechSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Blastech";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "SensorJammerPack" ;		// "AmmoPack"; -soph
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "GrenadeLauncher" ;		// "ELFGun"; -soph
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "GrenadeLauncherAmmo" ;		// +soph
//$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Protron" ;			// +soph
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "ProtronAmmo" ;			// +soph
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "MortarGrenade";
$AIEquipmentSet[LightEnergyELF, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Blastech";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "AmmoPack";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "AutoCannon";			// "ELFGun"; -soph
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "AutoCannonAmmo";		// +soph
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Chaingun";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "ChaingunAmmo";
//$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "MissileLauncher";		// "MBCannon"; -soph
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "MissileLauncherAmmo";		// "MBCannonCapacitor"; -soph
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "FlareGrenade" ;			// "MortarGrenade"; -soph
$AIEquipmentSet[LightShieldSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "CloakingPack";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Plasma";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "PlasmaAmmo";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "ShockLance";
//$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "GrenadeLauncherAmmo";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "FlashGrenade";
$AIEquipmentSet[LightCloakSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "RepairPack";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "MBCannon";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "MBCannonCapacitor";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "EnergyRifle";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "EnergyRifleAmmo";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Grenade";
$AIEquipmentSet[LightRepairSet, $EquipConfigIndex++] = "Mine";

$EquipConfigIndex = -1;
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "Light";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "EnergyPack";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "Disc";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "DiscAmmo";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "EnergyRifle" ;		// "MBCannon"; -soph
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "EnergyRifleAmmo" ;		// "MBCannonCapacitor"; -soph
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "SniperRifle";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "Sniper3006Ammo";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "TargetingLaser";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "RepairKit";
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "FlareGrenade" ;		// "Grenade"; -soph
$AIEquipmentSet[LightSniperChain, $EquipConfigIndex++] = "Mine";

// random pack strings 									// +[soph]
// ammoed weps MUST come first to sync with ammo!					// +
$RandomPackList[ Light ] = "DiscCannonPack ShotgunCannonPack EnergyPack ShieldPack HeatShieldPack" ;
$RandomPackAmmoList[ Light ] = "DiscPackAmmo" ;						// +
$RandomPackList[ Medium ] = "SRM1Pack AutoCCannonPack BlasterCannonPack HeatShieldPack SynomiumPack" ;
$RandomPackAmmoList[ Medium ] = "SRM4Ammo ACPackAmmo" ;					// +
$RandomPackList[ Heavy ] = "SRM2Pack StarHammerPack CometCannonPack PulseCannonPack ShieldPack SynomiumPack RepairPack" ;
$RandomPackAmmoList[ Heavy ] = "SRM4Ammo StarHammerAmmo" ;				// +
$RandomPackList[ MagIon ] = "DiscCannonPack ShotgunCannonPack SynomiumPack SensorJammerPack" ;
$RandomPackAmmoList[ MagIon ] = "DiscPackAmmo" ;					// +
$RandomPackList[ Engineer ] = "BlasterCannonPack AmmoPack" ;				// +
$RandomPackAmmoList[ Engineer ] = "" ;							// +
$RandomPackList[ BattleAngel ] = "SRM4Pack StarHammerPack ShockBlasterPack CometCannonPack PulseCannonPack SensorJammerPack SynomiumPack RepairPack" ;
$RandomPackAmmoList[ BattleAngel ] = "SRM4Ammo StarHammerAmmo" ;			// +
$RandomPackList[ Blastech ] = "SRM1Pack DiscCannonPack SynomiumPack RepairPack" ;	// +
$RandomPackAmmoList[ Blastech ] = "SRM4Ammo DiscPackAmmo" ;				// +
											// +
// task loadout overrides								// +
$ObjectiveTable['AttackLocation', desiredEquipment]   = "" ;				// +
$ObjectiveTable['DefendObjective', desiredEquipment]   = "" ;				// +
$ObjectiveTable['DefendObject', desiredEquipment]   = "" ;				// +
$ObjectiveTable['AttackObject', desiredEquipment]   = "" ;				// +
$ObjectiveTable['EscortPlayer', desiredEquipment]   = "" ;				// +
$ObjectiveTable['AttackPlayer', desiredEquipment]   = "" ;				// +
											// +
$ObjectiveTable['AttackPlayer', buyEquipmentSet]    = "LightSniperChain LightCloakSet LightEnergySniper MediumEnergySet MediumShieldSet HeavyIndoorTurretSet HeavyEnergySet HeavyAmmoSet BASiegeSet" ;
$ObjectiveTable['AttackObject', buyEquipmentSet]    = "LightShieldSet MagIonEnergy MediumMissileSet MediumShieldSet BASiegeSet" ;
$ObjectiveTable['AttackLocation', buyEquipmentSet]    = "LightShieldSet BlastechSet MagIonEnergy MediumMissileSet MediumShieldSet" ;
$ObjectiveTable['EscortPlayer', buyEquipmentSet]    = "LightSniperChain LightEnergyELF LightEnergySniper LightEnergyDefault MediumShieldSet MediumRepairSet HeavyInventorySet HeavyEnergySet" ;
$ObjectiveTable['DefendObject', buyEquipmentSet]    = "LightEnergyELF HeavyInventorySet HeavyShieldSet HeavyAmmoSet" ;
$ObjectiveTable['DefendLocation', buyEquipmentSet]    = "LightEnergyELF MediumMissileSet MediumShieldSet HeavyInventorySet HeavyShieldSet BASiegeSet" ;
$ObjectiveTable['DefendObjective', buyEquipmentSet]    = "HeavyInventorySet HeavyShieldSet" ;
$ObjectiveTable['RepairPlayer', buyEquipmentSet]    = "LightRepairSet BlastechSet EngineerRepairSet MediumRepairSet" ;
$ObjectiveTable['RepairObject', buyEquipmentSet]    = "LightRepairSet BlastechSet EngineerRepairSet MediumRepairSet" ;
$ObjectiveTable['MortarObject', buyEquipmentSet]    = "HeavyIndoorTurretSet HeavyRepairSet BAAmmoSet HeavyAmmoSet BASiegeSet" ;
$ObjectiveTable['LazeObject', buyEquipmentSet]    = "LightCloakSet" ;			// +
$ObjectiveTable['TakeFlag', buyEquipmentSet]    = "LightSniperChain LightCloakSet LightEnergySniper LightEnergyDefault MediumMissileSet MediumShieldSet MediumRepairSet" ;
$ObjectiveTable['GiveFlag', buyEquipmentSet]    = "LightEnergyDefault MediumShieldSet MediumRepairSet" ;
											// +
function AIFindClosestEnemyVehicle( %client , %radius , %losTimeout )			// + generally adapted from AIFindClosestEnemyPilot
{											// + intended to find unpiloted vehicles, i.e. camping tanks
	%closestThreatVehicle = -1 ;							// +
	%closestThreatDist = %radius ;							// +
	%closestThreatOccupant = -1 ;							// +
	%closestEmptyVehicle = -1 ;							// +
	%closestEmptyDist = %radius ;							// +
											// +
	%count = $AIVehicleSet.getCount() ;						// +
	for( %i = 0 ; %i < %count ; %i++ )						// +
	{										// +
		%vehicle = $AIVehicleSet.getObject( %i ) ;				// +
		if( !isObject( %vehicle ) )						// +
			continue ;							// +
		%mounted = false ;							// +
		%nodeCount = %vehicle.getDatablock().numMountPoints ;			// +
		for( %j = 1 ; %j < %nodeCount ; %j++ )					// +
		{									// +
			%occupant = %vehicle.getMountNodeObject( %j ) ;			// +
      			if( %occupant > 0 ) 						// +
				if( isObject( %occupant.client ) &&			// +
				    %occupant.client.team != %client.team )		// +
					break ;						// +
				else							// +
					%occupant = 0 ;					// +
		}									// +
											// +
		if( %occupant > 0 )							// +
		{									// +
			%hasLOS = %client.hasLOSToClient( %occupant.client ) ;		// +
			%losTime = %client.getClientLOSTime( %occupant.client ) ;	// +
			if( %hasLOS || %losTime < %losTimeout )				// +
			{								// +
				%clientPos = %client.player.getWorldBoxCenter() ;	// +
				%occupantPos = %occupant.getWorldBoxCenter() ;		// +
				%dist = VectorDist( %clientPos , %occupantPos ) ;	// +
				if( %dist < %closestThreatDist )			// +
				{							// +
					%closestThreatVehicle = %vehicle ;		// +
					%closestThreatDist = %dist ;			// +
					%closestThreatOccupant = %occupant ;		// +
				}							// +
			}								// +
		}									// +
		else if( %vehicle.team != %client.team )				// +
		{									// +
			%clientPos = %client.player.getWorldBoxCenter() ;		// +
                        if( getDistance3D( %clientPos , %vehicle.getPosition() ) < Sky.visibleDistance )
				%hasLOS = calcExplosionCoverage( %clientPos , %vehicle , $TypeMasks::InteriorObjectType		|
											 $TypeMasks::TerrainObjectType		|
											 $TypeMasks::ForceFieldObjectType	|
											 $TypeMasks::StaticShapeObjectType	) ;
			if( %hasLOS > 0 )						// +
			{								// +
				%vehiclePos = %vehicle.getPosition() ;			// +
				%dist = VectorDist( %clientPos , %vehiclePos ) ;	// +
				if( %dist < %closestEmptyDist )				// +
				{							// +
					%closestEmptyVehicle = %vehicle ;		// +
					%closestEmptyDist = %dist ;			// +
				}							// +
			}								// +
		}									// +
	}										// +
	return %closestThreatVehicle SPC %closestThreatDist SPC %closestThreatOccupant SPC %closestEmptyVehicle SPC %closestEmptyDist ;
}											// +[/soph]
