// Universal handy function library collection, release 7
// Created by MrKeen
// All functions used here are made strictly by MrKeen
// Functions made in the other attached files are made by their respective authors.

// Max Datablocks: 2048 (not 2500, sorry)
// Max Datablocks before it kicks clients randomly when reload() is used: 1950

// String Table
//

// Todo: ContainerBoxFillSet(), getLOSInfo() - T1 functions

$TypeMasks::AllObjectType = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType | $TypeMasks::StaticObjectType;
$TypeMasks::DefaultLOSType = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
$TypeMasks::CoreObjectType = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;
$TypeMasks::NonTransparent = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::MoveableObjectType;
$TypeMasks::DamageableObjectType = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ForceFieldObjectType;
$TypeMasks::EverythingType = -1;


$FuncDebugMode = false;

// Datablock Cache
//

datablock ShockLanceProjectileData(FXZap)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shockLightning01";
   texture[1] = "special/shockLightning02";
   texture[2] = "special/shockLightning03";
   texture[3] = "special/ELFBeam";
};

datablock ShockLanceProjectileData(FXPulse)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/nonlingradient";
   texture[1] = "special/nonlingradient";
   texture[2] = "special/nonlingradient";
   texture[3] = "special/nonlingradient";
};

datablock ParticleData(TeleporterParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.5;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 100;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1 1 1 1";
   colors[1]     = "0.75  0.75 1.0";
   colors[2]     = "0.5 0.5 0.5 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1;
   sizes[2]      = 2.5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(TeleporterEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 6;
   velocityVariance = 2.9;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 5;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TeleporterParticle";
};

// ShapeBase/Parent calls
//

function SimObject::getRotation(%obj)
{
     getRotation(%obj);
}

function SimObject::getSlotRotation(%obj, %slot)
{
     getSlotRotation(%obj, %slot);
}

function SimObject::getSlotPosition(%obj, %slot)
{
     getSlotPosition(%obj, %slot);
}

function SimObject::isVehicle(%obj)
{
     isVehicle(%obj);
}

function SimObject::isPlayer(%obj)
{
     isPlayer(%obj);
}

function ShapeBase::zapObject(%obj)
{
     zapObject(%obj);
}

function ShapeBase::zap2Object(%obj)
{
     zap2Object(%obj);
}

function ShapeBase::stopZap(%obj)
{
     stopZap(%obj);
}

function ShapeBase::applyKick(%obj, %force)
{
     applyKick(%obj, %force);
}

function ShapeBase::useEnergy(%obj, %amount)
{
     useEnergy(%obj, %amount);
}

function ShapeBase::takeDamage(%obj, %amount)
{
     takeDamage(%obj, %amount);
}

function SimObject::setPosition(%obj, %pos)
{
     setPosition(%obj, %pos);
}

function SimObject::setRotation(%obj, %rot)
{
     setRotation(%obj, %rot);
}

function SimObject::play3D(%obj, %sound)
{
     play3D(%obj, %sound);
}

function ShapeBase::teleportStartFX(%obj, %bool)
{
     teleportStartFX(%obj, %bool);
}

function ShapeBase::teleportEndFX(%obj)
{
     teleportEndFX(%obj);
}

function ShapeBase::getEyePoint(%obj)
{
     getEyePoint(%obj);
}

function ShapeBase::getMuzzleRaycastPt(%obj, %slot, %dist)
{
     getMuzzleRaycastPt(%obj, %slot, %dist);
}

function ShapeBase::getEyeRaycastPt(%obj, %dist)
{
     getEyeRaycastPt(%obj, %dist);
}

function ShapeBase::getForwardRaycastPt(%obj, %dist)
{
     getForwardRaycastPt(%obj, %dist);
}

function ShapeBase::getMass(%obj)
{
     getMass(%obj);
}

function ShapeBase::getMaxEnergy(%obj)
{
     getMaxEnergy(%obj);
}

function ShapeBase::getMaxDamage(%obj)
{
     getMaxDamage(%obj);
}

function ShapeBase::getDamageLeft(%obj)
{
     getDamageLeft(%obj);
}

function ShapeBase::getDamageLeftPct(%obj)
{
     getDamageLeftPct(%obj);
}

function ShapeBase::getEnergyPct(%obj)
{
     getEnergyPct(%obj);
}

function ShapeBase::getEnergyCapPct(%obj)
{
     getEnergyCapPct(%obj);
}

function ShapeBase::getDamagePct(%obj)
{
     getDamagePct(%obj);
}

function ShapeBase::getTransformAngle(%trans)
{
     getTransformAngle(%trans);
}

// Function library
//

function debugMode() // attach $FuncDebugMode to whatever function you want, and you cna trigger "debug mode" on and off with this function
{
   $FuncDebugMode = $FuncDebugMode == true ? $FuncDebugMode = false : $FuncDebugMode = true;
   %word = $FuncDebugMode == true ? "ON" : "OFF";
   error("Debug mode is "@%word);
}

function posFrRaycast(%transform)
{
   %position = getWord(%transform, 1) @ " " @ getWord(%transform, 2) @ " " @ getWord(%transform, 3);
   return %position;
}

function normalFrRaycast(%transform)
{
   %norm = getWord(%transform, 4) @ " " @ getWord(%transform, 5) @ " " @ getWord(%transform, 6);
   return %norm;
}

function getEyePoint(%obj)
{
     %eyePt = getWords(%obj.getEyeTransform(), 0, 2);
     return %eyePt;
}

function play3D(%obj, %sound)
{
     serverPlay3D(%sound, %obj.getTransform());
}

function killit(%k) // can use %obj.schedule(timeMS, delete); instead
{
   if(isObject(%k))
      %k.delete();
}

function scanArea(%pos, %radius, %mask)
{
   InitContainerRadiusSearch(%pos, %radius, %mask);
   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int)
         return true;
   }

   return false;
}

function testPosition(%obj)
{
     InitContainerRadiusSearch(%obj.getWorldBoxCenter(), 0.5, $TypeMasks::DefaultLOSType);

     while ((%test = ContainerSearchNext()) != 0)
     {
          if(%test)
               return false;
          else
               return true;
     }
}

function createEmitter(%pos, %emitter, %rot)
{
    %dummy = new ParticleEmissionDummy()
    {
          position = %pos;
          rotation = %rot;
          scale = "1 1 1";
          dataBlock = defaultEmissionDummy;
          emitter = %emitter;
          velocity = "1";
    };
    MissionCleanup.add(%dummy);

    if(isObject(%dummy))
       return %dummy;
}

function createLifeEmitter(%pos, %emitter, %lifeMS, %rot)
{
    %dummy = createEmitter(%pos, %emitter, %rot);
    %dummy.schedule(%lifeMS, delete);
    return %dummy;
}

function sqr(%num)
{
     return %num*%num;
}

function cube(%num)
{
     return %num*%num*%num;
}

function useEnergy(%obj, %amount)
{
   if(isObject(%obj))
   {
      %energy = %obj.getEnergyLevel() - %amount;
      %obj.setEnergyLevel(%energy);
   }
}

function takeDamage(%obj, %amount)
{
   if(isObject(%obj))
   {
      %dmg = %obj.getDamageLevel() + %amount;
      %obj.setDamageLevel(%dmg);
   }
}

function vectorCompare(%v1, %v2)
{
   if(getWord(%v1, 0) == getWord(%v2, 0))
   {
      if(getWord(%v1, 1) == getWord(%v2, 1))
      {
         if(getWord(%v1, 2) == getWord(%v2, 2))
            return true;

         return false;
      }
      return false;
   }
   return false;
}

function modifyTransform(%trans1, %trans2)
{
     %tpx1 = getWord(%trans1, 0);
     %tpy1 = getWord(%trans1, 1);
     %tpz1 = getWord(%trans1, 2);
     %tpa1 = getWord(%trans1, 3);
     %tpb1 = getWord(%trans1, 4);
     %tpc1 = getWord(%trans1, 5);
     %tpd1 = getWord(%trans1, 6);

     %tpx2 = getWord(%trans2, 0);
     %tpy2 = getWord(%trans2, 1);
     %tpz2 = getWord(%trans2, 2);
     %tpa2 = getWord(%trans2, 3);
     %tpb2 = getWord(%trans2, 4);
     %tpc2 = getWord(%trans2, 5);
     %tpd2 = getWord(%trans2, 6);

     %trans = (%tpx1+%tpx2)@" "@(%tpy1+%tpy2)@" "@(%tpz1+%tpz2)@" "@(%tpa1+%tpa2)@" "@(%tpb1+%tpb2)@" "@(%tpc1+%tpc2)@" "@(%tpd1+%tpd2);
     return %trans;
}

function getTransformAngle(%trans)
{
     return getWord(%trans, 6);
}

function setPosition(%obj, %pos)
{
    %rot = getWords(%obj.getTransform(), 3, 6);
    %trans = %pos@" "@%rot;
    %obj.setTransform(%trans);
}

function setRotation(%obj, %rot)
{
    %pos = getWords(%obj.getTransform(), 0, 2);
    %trans = %pos@" "@%rot;
    %obj.setTransform(%trans);
}

function getSlotPosition(%obj, %slot)
{
    return getWords(%obj.getSlotTransform(%slot), 0, 2);
}

function getSlotRotation(%obj, %slot)
{
    return getWords(%obj.getSlotTransform(%slot), 3, 6);
}

function getRotation(%obj)
{
    return getWords(%obj.getTransform(), 3, 6);
}

function getRandomN(%max, %min) // negative getrandom
{
   if(%max !$= "" && %min !$= "")
      return getRandom(%max, %min) * -1;
   else if(%max !$= "")
      return getRandom(%max) * -1;
   else
      return getRandom() * -1;
}

function getRandomB() // boolean getRandom
{
   if(getRandom(1))
      return true;
   else
      return false;
}

function getRandomT(%max, %min) // true getRandom (randomly negative)
{
   if(%max !$= "" && %min !$= "")
      return getRandomB() ? getRandom(%max, %min) : getRandomN(%max, %min);
   else if(%max !$= "")
      return getRandomB() ? getRandom(%max) : getRandomN(%max);
   else
      return getRandomB() ? getRandom() : getRandomN();
}

function shutdownServer(%time, %msg, %lines)
{
     %left = %time*1000;
     centerPrintAll(%msg, %left, %lines);
     schedule(%left, 0, "quit");
}

function velToSingle(%vel, %bool) // this function is also vectorLength :)
{
	%x = getWord(%vel, 0);
	%y = getWord(%vel, 1);
	%z = getWord(%vel, 2);

    if(%bool)
        return mSqrt((%x*%x)+(%y*%y)+(%z*%z));
    else
        return mFloor(mSqrt((%x*%x)+(%y*%y)+(%z*%z)));
}

function projectileTrail(%obj, %time, %projType, %proj, %bSkipFirst, %offset)
{
   if(isObject(%obj) && isObject(%obj.sourceObject))
   {
      if(!%obj.lastBlinkPos)
         %obj.lastBlinkPos = "65536 65536 65536";

      if(%bSkipFirst)
      {
         schedule(%time, 0, "projectileTrail", %obj, %time, %projType, %proj, false, %offset);
         return;
      }

      if(%offset)
         %pos = vectorAdd(%obj.position, vectorScale(%obj.initialDirection, %offset));
      else
         %pos = %obj.position;

      if( %obj.exploded )	// +soph
         return;		// +soph

      if(vectorCompare(%pos, %obj.lastBlinkPos)) //same position...BUG!
         return;

      %p = new (%projType)()
      {
         dataBlock        = %proj;
         initialDirection = %obj.initialDirection;
         initialPosition  = %pos;
         sourceObject     = %obj.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
      %p.exploded = true;	// +soph

      schedule(%time, 0, projectileTrail, %obj, %time, %projType, %proj, false, %offset);
      %obj.lastBlinkPos = %pos;
   }
}

function spawnProjectile(%obj, %projType, %proj, %offset)
{
   if(isObject(%obj) && isObject(%obj.sourceObject))
   {
      if(%offset)
         %pos = vectorAdd(%obj.getWorldBoxCenter(), vectorScale(%obj.initialDirection, %offset));
      else
         %pos = %obj.position;

      %p = new (%projType)()
      {
         dataBlock        = %proj;
         initialDirection = %obj.initialDirection;
         initialPosition  = %pos;
         sourceObject     = %obj.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
   }
}

function spawnShockwave(%data, %pos, %vec) // originally found by Lt. Earthworm
{
   %wave = new Shockwave() 
   {
      dataBlock = %data; 
      pos = %pos;
      normal = %vec;
   }; 
   MissionCleanup.add(%wave);

   return %wave;
}

function spawnShockwaveTrail(%obj, %time, %data)
{
   if(isObject(%obj))
   {
      %pos = %obj.position; 

      if(vectorCompare(%pos, %obj.lastShockBlinkPos)) //same position...BUG!
         return;

      %wave = spawnShockwave(%data, %pos, %obj.initialDirection);

      schedule(%time, 0, spawnShockwaveTrail, %obj, %time, %data);
      %obj.lastShockBlinkPos = %pos;
   }
}

function MStoKPH(%vel, %bool) // for more accurate conversions
{
    if(%bool)
        return %vel * 3.6;
    else
        return mFloor(%vel * 3.6);
}

function KPHtoMPH(%vel, %bool)
{
    if(%bool)
        return %vel * 0.6214;
    else
        return mFloor(%vel * 0.6214);
}

function vectorNeg(%vec)
{
     %v1 = getWord(%vec, 0) * -1;
     %v2 = getWord(%vec, 1) * -1;
     %v3 = getWord(%vec, 2) * -1;
     return %v1@" "@%v2@" "@%v3; 
}

function vectorRandom(%vec)
{
   return getRandomT()@" "@getRandomT()@" "@getRandomT();
}

function vectorToRotZ(%vec) // only for Z rotations
{
   %radian = mAcos(getWord(%vec, 1));
	%angle = (%radian / 0.0175);
	%newrot = " 0 0 1 " @ (270 - %angle);
   return %newrot;
}

function getDistance2D(%a, %b)
{
     %x1 = getWord(%a, 0);
     %x2 = getWord(%b, 0);	// %y1 = getWord(%b, 0); -soph
     %y1 = getWord(%a, 1);	// %x2 = getWord(%a, 1); -soph
     %y2 = getWord(%b, 1);
     
     %dist = mSqrt(sqr(%x2 - %x1) + sqr(%y2 - %y1));
     return %dist;
}

function getObjectDistance(%obj1, %obj2)
{
     %obj1b = %obj1.getWorldBoxCenter();
     %obj2b = %obj2.getWorldBoxCenter();

     %tpx1 = getWord(%obj1b, 0);
     %tpy1 = getWord(%obj1b, 1);
     %tpz1 = getWord(%obj1b, 2);

     %tpx2 = getWord(%obj2b, 0);
     %tpy2 = getWord(%obj2b, 1);
     %tpz2 = getWord(%obj2b, 2);

     %dist = mSqrt(sqr(%tpx2 - %tpx1) + sqr(%tpy2 - %tpy1) + sqr(%tpz2 - %tpz1));
     return %dist;
}

function getVectorFromPoints(%pt1, %pt2)
{
     return VectorNormalize(VectorSub(%pt2, %pt1));
}

function getVectorFromObjects(%obj1, %obj2)
{
     %obj1b = %obj1.getWorldBoxCenter();
     %obj2b = %obj2.getWorldBoxCenter();

     return VectorNormalize(VectorSub(%obj2b, %obj1b));
}

function getVector2DFromPoints(%pt1, %pt2)
{
     %x1 = getWord(%pt1, 0);
     %y1 = getWord(%pt2, 0);
     %x2 = getWord(%pt1, 1);
     %y2 = getWord(%pt2, 1);
     %p1 = %x1 SPC %y1 SPC "0";
     %p2 = %x2 SPC %y2 SPC "0";

     return VectorNormalize(VectorSub(%p2, %p1));
}

function getVector2DFromObjects(%obj1, %obj2)
{
     %obj1b = %obj1.getWorldBoxCenter();
     %obj2b = %obj2.getWorldBoxCenter();

     %x1 = getWord(%obj1b, 0);
     %y1 = getWord(%obj2b, 0);
     %x2 = getWord(%obj1b, 1);
     %y2 = getWord(%obj2b, 1);
     %p1 = %x1 SPC %y1 SPC "0";
     %p2 = %x2 SPC %y2 SPC "0";

     return VectorNormalize(VectorSub(%p2, %p1));
}

function getDistance3D(%ptA, %ptB)
{
     %vec = VectorSub(%ptB, %ptA);
     %len = vectorLen(%vec);
     
     return %len;
}

//function by Founder (Martin Hoover)
function getRotationFromPoints(%posOne, %posTwo)
{
   %vec = VectorSub(%posTwo, %posOne);

   // pull the values out of the vector
   %x = firstWord(%vec);
   %y = getWord(%vec, 1);
   %z = getWord(%vec, 2);

   //this finds the distance from origin to our point
   %len = vectorLen(%vec);

   //---------XY-----------------
   //given the rise and length of our vector this will give us the angle in radians
   %rotAngleXY = mATan( %z, %len );

   //---------Z-----------------
   //get the angle for the z axis
   %rotAngleZ = mATan( %x, %y );

   //create 2 matrices, one for the z rotation, the other for the x rotation
   %matrix = MatrixCreateFromEuler("0 0" SPC %rotAngleZ * -1);
   %matrix2 = MatrixCreateFromEuler(%rotAngleXY SPC "0 0");

   //now multiply them together so we end up with the rotation we want
   %finalMat = MatrixMultiply(%matrix, %matrix2);

   //we&#180;re done, send the proper numbers back
   //return getWords(%finalMat, 3, 6);
   %rt = getWords(%finalMat, 3, 6);

   return %rt; // Our rotation
}

function moveObjectInSteps(%obj, %startPos, %endPos, %steps, %stepTime)
{
   if(isObject(%obj))
   {
      if(%steps < 1)
         %steps = 2;

      %dist = getDistance3D(%startPos, %endPos);
      if(%dist > 1)
      {
         %vector = getVectorFromPoints(%startPos, %endPos);
         %stepLen = vectorScale(%vector, (1/%steps));
         %obj.stepVector = %vector;
         moveStepLoop(%obj, %stepLen, %steps, %stepTime);
      }
      else
         error("moveObjectInSteps: min step size = 1");
   }
}

function moveStepLoop(%obj, %moveVecChunk, %iterations, %time)
{
   if(isObject(%obj))
   {
      if(!%obj.loopBreak[moveObjectInSteps])
      {
         if(%iterations > 0)
         {
            %pos = %obj.getPosition();
            %endPos = vectorAdd(%pos, %moveVecChunk);
            %obj.setPosition(%endPos);
            schedule(%time, 0, "moveStepLoop", %obj, %moveVecChunk, %iterations--, %time);
         }
      }
   }
}

function zapObject(%obj)
{
     %obj.zap = new ShockLanceProjectile()
     {
          dataBlock        = "fxzap"; 
          initialDirection = "0 0 0";
          initialPosition  = %obj.getWorldBoxCenter();
          sourceObject     = %obj;
          sourceSlot       = 0;
          targetId         = %obj;
     };
     MissionCleanup.add(%obj.zap);
}

function zap2Object(%obj)
{
     %obj.zap = new ShockLanceProjectile()
     {
          dataBlock        = "fxpulse";
          initialDirection = "0 0 0";
          initialPosition  = %obj.getWorldBoxCenter();
          sourceObject     = %obj;
          sourceSlot       = 0;
          targetId         = %obj;
     };
     MissionCleanup.add(%obj.zap);
}

function zapObjectRed(%obj)
{
     %obj.zap = new ShockLanceProjectile()
     {
          dataBlock        = "MBDenseLance";
          initialDirection = "0 0 0";
          initialPosition  = %obj.getWorldBoxCenter();
          sourceObject     = %obj;
          sourceSlot       = 0;
          targetId         = %obj;
     };
     MissionCleanup.add(%obj.zap);
}

function stopZap(%obj)
{
     if(isObject(%obj))
     {
          if(isObject(%obj.zap))
               %obj.zap.delete();
     }
}

function holdObject(%obj, %pos, %time)
{
   if(isObject(%obj) && %obj.f_hold)
   {
      if(!%time)
         %time = 100;

      %obj.setPosition(%pos);
      %obj.setVelocity("0 0 0");
      schedule(%time, %obj, holdobject, %obj, %pos);
   }
}

function disableHoldObject(%obj)
{
   if(isObject(%obj) && %obj.f_hold)
      %obj.f_hold = false;
}

function ejectFlag(%obj)
{
   if(%obj.holdingFlag)
   {
      %flag = %obj.holdingFlag;
      %flag.setVelocity("0 0 0");
      %flag.setTransform(%obj.getWorldBoxCenter());
      %flag.setCollisionTimeout(%obj);
      %obj.throwObject(%flag);
   }
}

function teleportStartFX(%obj, %ignoreFlag)
{
     if(%obj.holdingFlag && %ignoreFlag $= "")
     {
          %flag = %obj.holdingFlag;
          %flag.setVelocity("0 0 0");
          %flag.setPosition(getRandomT(10000) SPC getRandomT(10000) SPC getRandomN(10000, 5000));
          %flag.setCollisionTimeout(%obj);
          %obj.throwObject(%flag);
          messageClient(%obj.client, 'MsgCannotTeleWithFlag', '\c2You cannot teleport with the flag.');
     }

     %obj.playShieldEffect("0 0 1");
     %obj.setCloaked(true);
     %obj.startFade( 1000, 0, true );
     %obj.play3D(TeleportSound);

     createLifeEmitter(%obj.getWorldBoxCenter(), "TeleporterEmitter", 2500);
}

function teleportEndFX(%obj)
{
     %obj.setCloaked(false);
     %obj.playShieldEffect("0 0 1");
     %obj.startFade(1000, 0, false );
     %obj.play3D(UnTeleportSound);
     createLifeEmitter(%obj.getWorldBoxCenter(), "TeleporterEmitter", 2500);
}

function getMuzzleRaycastPt(%obj, %slot, %dist)
{
     %vec = %obj.getMuzzleVector(%slot);
     %pos = %obj.getMuzzlePoint(%slot);
     %nVec = VectorNormalize(%vec);
     %scVec = VectorScale(%nVec, %dist);
     %end = VectorAdd(%pos, %scVec);
     %searchResult = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);
     %raycastPt = posFrRaycast(%searchResult);
     %raycastPt.raycast = %searchResult;
     
     return %raycastPt;
}

function getEyeRaycastPt(%obj, %dist)
{
     %vec = %obj.getEyeVector();
     %pos = %obj.getEyePoint();
     %nVec = VectorNormalize(%vec);
     %scVec = VectorScale(%nVec, %dist);
     %end = VectorAdd(%pos, %scVec);
     %searchResult = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);
     %raycastPt = posFrRaycast(%searchResult);
     %raycastPt.raycast = %searchResult;
     
     return %raycastPt;
}

function getForwardRaycastPt(%obj, %dist)
{
     %vec = %obj.getForwardVector();
     %pos = %obj.getWorldBoxCenter();
     %nVec = VectorNormalize(%vec);
     %scVec = VectorScale(%nVec, %dist);
     %end = VectorAdd(%pos, %scVec);
     %searchResult = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);
     %raycastPt = posFrRaycast(%searchResult);
     %raycastPt.raycast = %searchResult;
     
     return %raycastPt;
}

function getLOSInfo(%obj, %dist)
{
   %vec = %obj.getMuzzleVector(0);
   %pos = %obj.getMuzzlePoint(0);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return %target;
   else
      return false;
}

function getDamageableLOS(%obj, %dist)
{
   %vec = %obj.getMuzzleVector(0);
   %pos = %obj.getMuzzlePoint(0);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, $TypeMasks::DamageableObjectType, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return %target;
   else
      return false;
}

function getLOSEnd(%obj, %dist)
{
   %vec = %obj.getMuzzleVector(0);
   %pos = %obj.getMuzzlePoint(0);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);
   %target = getWord(%result, 0);
   %pos = getWords(%result, 1, 3);

   if(isObject(%target))
      return %pos;
   else
      return "0 0 300";
}

function getLOSNormal(%obj, %dist)
{
   %vec = %obj.getMuzzleVector(0);
   %pos = %obj.getMuzzlePoint(0);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, $TypeMasks::EverythingType, %obj);
   %target = getWord(%result, 0);
   %normal = getWords(%result, 4, 6);

   if(isObject(%target))
      return %normal;
   else
      return "0 0 1";
}

function getLOSOffset(%obj, %vec, %dist, %pos)
{
   if(isObject(%obj))
   {
      if(%pos $= "")
         %pos = %obj.getPosition();

      %dir = vectorNormalize(%vec);
      %nvec = vectorScale(%dir, %dist);
      return vectorAdd(%pos, %nvec);
   }
   return "0 0 300"; //no object.... default pos
}

function getVectorOffset(%pos, %vec, %dist)
{
   if(%pos $= "")
      %pos = "0 0 0";

   %dir = vectorNormalize(%vec);
   %nvec = vectorScale(%dir, %dist);
   return vectorAdd(%pos, %nvec);
}

function applyKick(%obj, %force)
{
    %vec = VectorScale(%obj.getMuzzleVector(0), %force);
    %obj.applyImpulse(%obj.getTransform(), %vec);
}

function getMass(%obj)
{
    return %obj.getDatablock().mass;
}

function getMaxEnergy(%obj)
{
    return %obj.getDatablock().maxEnergy;
}

function getMaxDamage(%obj)
{
    return %obj.getDatablock().maxDamage;
}

function getEnergyPct(%obj)
{
   return %obj.getEnergyLevel() / %obj.getMaxEnergy();
}

function getEnergyCapPct(%obj)
{
   return %obj.getCapacitorLevel() / %obj.getDatablock().maxCapacitorEnergy;
}

function getDamagePct(%obj)
{
   return %obj.getDamageLevel() / %obj.getMaxDamage();
}

function getDamageLeft(%obj)
{
   return %obj.getMaxDamage() - %obj.getDamageLevel();
}

function getDamageLeftPct(%obj)
{
   return %obj.getDamageLeft() / %obj.getMaxDamage();
}

function setFlightCeiling(%val)
{
    %missionArea = nameToID("MissionGroup/MissionArea");

    if(%missionArea != -1)
        %missionArea.FlightCeiling = %val;
}

function findWater(%pos, %radius)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::WaterObjectType);

   if((%obj = ContainerSearchNext()))
      return %obj;
   else
      return false;
}

function reload(%script)
{
    exec(%script);
    graphWindowDynamicAll(%script, 100, 1, 4, true, "<color:00FF00>Server is reloading ");
    %count = ClientGroup.getCount();

    for(%i = 0; %i < %count; %i++)
    {
        %cl = ClientGroup.getObject(%i);

        if(!%cl.isAIControlled()) // no sending bots datablocks.. LOL
            %cl.transmitDataBlocks(0); // all clients on server
    }
}

function isVehicle(%obj)
{
     return %obj.getType() & $TypeMasks::VehicleObjectType;
//    if(isObject(%obj))
//    {
//        %data = %obj.getDataBlock();
//        %className = %data.className;
//
//        if(%className $= WheeledVehicleData || %className $= FlyingVehicleData || %className $= HoverVehicleData)
//            return true;
//        else
//            return false;
//    }
}

function isPlayer(%obj)
{
     return %obj.getType() & $TypeMasks::PlayerObjectType;

//    if(isObject(%obj))
//    {
//        %data = %obj.getDataBlock();
//        %className = %data.className;
//
//        if(%className $= Armor)
//            return true;
//        else
//            return false;
//    }
}

function createFlash(%pos, %radius, %intensity, %disableFlashOverlap)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);

   while((%flashMe = containerSearchNext()) != 0)
   {
      %eyePointVec = getVectorFromPoints(%flashMe.getEyePoint(), %pos);
      %eyeVec = %flashMe.getEyeVector();
      %eyeDot = (%d = VectorDot(%eyeVec, %eyePointVec)) <= 0 ? 0 : %d;
      %eyeDistPct = getDistance3D(%flashMe.getEyePoint(), %pos) / %radius;
      %eyeFlashPct = (%intensity * %eyeDistPct) / 2 + (%intensity * %eyeDot) / 2;
      %whiteoutVal = %disableFlashOverlap == true ? %eyeFlashPct : %flashMe.getWhiteOut() + %eyeFlashPct;
      %flashMe.setWhiteOut(%whiteoutVal);
   }
}

function createRealisticFlash(%pos, %radius, %intensity, %disableFlashOverlap)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);

   while((%flashMe = containerSearchNext()) != 0)
   {
      %eyePointVec = getVectorFromPoints(%flashMe.getEyePoint(), %pos);
      %vec = VectorScale(%eyePointVec, %radius);
      %end = VectorAdd(%pos, %vec);
      %canFlash = containerRayCast(%pos, %end, $TypeMasks::NonTransparent);

      if(%canFlash)
      {
         %eyeVec = %flashMe.getEyeVector();
         %eyeDistPct = getDistance3D(%flashMe.getEyePoint(), %pos) / %radius;
         %eyeDot = VectorDot(%eyeVec, %eyePointVec);
         %eyeDot = %eyeDot > 0 ? %eyeDot : 0;
         %eyeFlashPct = %intensity * %eyeDot * %eyeDistPct;

         if($FuncDebugMode) // example of FuncDebugMode in action
         {
            echo("Flash Obj  :" SPC %flashMe);
            echo("Intensity  :" SPC %intensity);
            echo("Max Radius :" SPC %radius);
            echo("DistPct    :" SPC %eyeDistPct);
            echo("Dot        :" SPC %eyeDot);
            echo("FlashPct   :" SPC %eyeFlashPct);
         }

         %whiteoutVal = %disableFlashOverlap == true ? %eyeFlashPct : %flashMe.getWhiteOut() + %eyeFlashPct;
         %flashMe.setWhiteOut(%whiteoutVal);
      }
   }
}

function createWind(%pos, %area, %force, %time)
{
   if(%time <= 0)
      return;

   %time -= 100;

   InitContainerRadiusSearch(%pos, %area, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ItemObjectType | $TypeMasks::CorpseObjectType); // );

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %area)
         continue;

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];
      %distPct = %dist / %area;

      %coverage = calcExplosionCoverage(%pos, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType));
      if (%coverage == 0)
         continue;

      %tgtPos = %targetObject.getWorldBoxCenter();
      %vec = vectorNeg(vectorNormalize(vectorSub(%pos, %tgtPos)));
      %nForce = %force * %distPct * 10; 
      %forceVector = vectorScale(%vec, %nForce);

      if(!%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive)
          %targetObject.applyImpulse(%pos, %forceVector);
   }
   schedule(100, 0, "createWind", %pos, %area, %force, %time);
}

function edgeCenterPosition(%pos, %scale) // for forcefields and waterblocks, scale being 1 unit
{
   return getWord(%pos, 0) - (%scale / 2)@" "@getWord(%pos, 1) - (%scale / 2)@" "@getWord(%pos, 2);
}

function edgeCenterPosition3D(%pos, %scale) // scale being 1 unit
{
   return getWord(%pos, 0) - (%scale / 2)@" "@getWord(%pos, 1) - (%scale / 2)@" "@getWord(%pos, 2) - (%scale / 2);
}

function createLightning(%pos, %rad, %strikesPerMin, %hitPercentage, %scale1000)
{
   %obj = new Lightning(Lightning)
   {
      position = %pos;
     	rotation = "1 0 0 0";
     	scale = %scale1000 ? %rad SPC %rad SPC (%rad+1000) : %rad SPC %rad SPC %rad;
    	dataBlock = "DefaultStorm";
    	lockCount = "0";
    	homingCount = "0";
    	strikesPerMinute =%strikesPerMin;
    	strikeWidth = "2.5";
    	chanceToHitTarget = %hitPercentage;
    	strikeRadius = "20";
    	boltStartRadius = "20";
    	color = "1.000000 1.000000 1.000000 1.000000";
    	fadeColor = "0.100000 0.100000 1.000000 1.000000";
    	useFog = "1";
  	};

   return %obj;
}

function createMeteorShower(%pos, %rad, %dropsPerMin, %hitPercentage)
{
   %obj = new FireballAtmosphere(FireballAtmosphere)
   {
      position = %pos;
     	rotation = "1 0 0 0";
     	scale = "1 1 1";
     	dataBlock = "fireball";
     	lockCount = "0";
     	homingCount = "0";
     	dropRadius = %rad;
     	dropsPerMinute = %dropsPerMin;
     	minDropAngle = "0";
     	maxDropAngle = "30";
     	startVelocity = "300";
     	dropHeight = "1000";
     	dropDir = "0.212 0.212 -0.953998";
  	};

   return %obj;
}

function createWater(%pos, %area, %density, %wavePercent, %opacity, %reflectivity)
{
   %scPos = edgeCenterPosition(%pos, %area);

   %obj = new WaterBlock()
   {
   	position = %scPos;
   	rotation = "1 0 0 0";
   	scale = %sc1;
   	liquidType = "RiverWater";
   	density = %density; // 1
   	viscosity = "5";
   	waveMagnitude = %wavePercent; // 5.5
   	surfaceTexture = "LiquidTiles/BlueWater";
   	surfaceOpacity = %opacity; // 0.6
   	envMapTexture = "lush/skies/lushcloud1";
   	envMapIntensity = %reflectivity; // 0.2
   	removeWetEdges = "0";
  	};

   return %obj;
}

function loopDamageFX(%obj, %radius, %count, %rate, %blocktype, %block) // default; for displaying random explosions around a player
{
   if(!isObject(%obj))
      return;

   if(%blocktype $= "" || %block $= "")
      return;

   if(%rate > 31)
   {
      error("loopDamageFX::rate cannot exceed 31 iterations/second due to engine limitations");
      %rate = 31;
   }

   if(%rate < 1)
   {
      error("loopDamageFX::rate cannot be lower than 1 iteration/second");
      %rate = 1;
   }

   if(%obj.DFX_Count <= %count && %obj)
   {
      %pos = %obj.getPosition();
      %px = getWord(%pos, 0) + getRandomT(%radius);
      %py = getWord(%pos, 1) + getRandomT(%radius);
      %pz = getWord(%pos, 2) + getRandomT(%radius);
      %npos = %px SPC %py SPC %pz;

      %p = new (%blocktype)() {
         dataBlock        = %block;
         initialDirection = "0 0 0";
         initialPosition  = %npos;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };
      %p.ignoreReflections = true ;	// +soph
      MissionCleanup.add(%p);

      %obj.DFX_Count++;

      schedule((1/%rate)*1000, 0, "loopDamageFX", %obj, %radius, %count, %rate, %blocktype, %block);
   }
   else
      %obj.DFX_Count = 0;
}

$DFX_ID = 0;

function loopDamageFX2(%obj, %pos, %radius, %count, %rate, %blocktype, %block, %id) // enhanced; for displaying random explosions around a position
{
   if(!isObject(%obj))
      return;

   if(%blocktype $= "" || %block $= "")
      return;

   if(%rate > 31)
   {
      error("loopDamageFX::rate cannot exceed 31 iterations/second due to engine limitations");
      %rate = 31;
   }

   if(%rate < 1)
   {
      error("loopDamageFX::rate cannot be lower than 1 iteration/second");
      %rate = 1;
   }

   if(%id $= "")
   {
      $DFX_ID++;
      %id = $DFX_ID;
   }

   if(%obj.proj[%id, DFX_Count] <= %count && %obj)
   {
      %px = getWord(%pos, 0) + getRandomT(%radius);
      %py = getWord(%pos, 1) + getRandomT(%radius);
      %pz = getWord(%pos, 2) + getRandomT(%radius);
      %npos = %px SPC %py SPC %pz;

      %p = new (%blocktype)() {
         dataBlock        = %block;
         initialDirection = "0 0 0";
         initialPosition  = %npos;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);

      %obj.proj[%id, DFX_Count]++;
      schedule((1/%rate)*1000, 0, "loopDamageFX2", %obj, %pos, %radius, %count, %rate, %blocktype, %block, %id);
   }
   else
      %obj.DFX_Count = 0;
}

function capValue(%val, %cap)
{
   if(%val > %cap)
      %val = %cap;

   if(%val < 0)
      return %val *= -1;

   return %val;
}

function generateRandom(%num, %rndInt)
{
   %out = ""; // init var
   %found = false;

   for(%i = 0; %i < %num; %i++)
   {
      %rnd = mFloor(getRandom() * %rndInt);

      if(%rnd == %rndInt)
         %found = true;

      if(%out $= "")
         %out = %rnd;
      else
         %out = %out SPC %rnd;
   }

   if(%found)
      echo("Found!");

   echo(%out);
}

function checkTimeExceeded(%time, %len)
{
//   echo(%time SPC %len SPC getSimTime());

   if((%time + %len) > getSimTime())
      return true;
   else
      return false;
}

function changeServerAllowAliases(%bool)
{
   $Host::NoSmurfs = %bool;
}

function changeServerPlayerCount(%num)
{
   $Host::MaxPlayers = %num;
}

function changeAdminPassword(%pass)
{
   if(%pass)
      $Host::AdminPassword = %pass;
   else
      $Host::AdminPassword = "";
}

function changeServerPassword(%pass)
{
   if(%pass)
      $Host::Password = %pass;
   else
      $Host::Password = "";
}

function setServerPrefs(%aliases, %admin, %password, %num)
{
   $Host::CRCTextures = 0;
   if(%aliases !$= "")
      changeServerAllowAliases(%aliases);
   if(%num !$= "")
      changeServerPlayerCount(%num);

   changeAdminPassword(%admin);
   changeServerPassword(%password);
}

function calcSpreadVector(%vector, %val) // (%val / 1000)
{
   %x = (getRandom() - 0.5) * 2 * 3.1415926 * (%val / 1000);
   %y = (getRandom() - 0.5) * 2 * 3.1415926 * (%val / 1000);
   %z = (getRandom() - 0.5) * 2 * 3.1415926 * (%val / 1000);
   %mat = MatrixCreateFromEuler(%x SPC %y SPC %z);
   return MatrixMulVector(%mat, %vector);
}

// These functions work for groups and sets
function foreachInGroup(%group, %function, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    if(isObject(%group) && %group.getCount() > 1)
    {
        %count = %group.getCount();

        for(%i = 0; %i < %count; %i++)
        {
            %obj = %group.getObject(%i);

            if(isObject(%obj))
                eval(%function@"("@%obj@","@%arg1@","@%arg2@","@%arg3@","@%arg4@","@%arg5@","@%arg6@","@%arg7@","@%arg8@","@%arg9@","@%arg10@");");
        }
    }
}


function foreachInArray(%arrayName, %function, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    if(%arrayName !$= "")
    {
        %index = 0;

        while(%arrayName[%index] !$= "")
        {
            %obj = %arrayName[%index];

            if(isObject(%obj))
                eval(%function@"("@%obj@","@%arg1@","@%arg2@","@%arg3@","@%arg4@","@%arg5@","@%arg6@","@%arg7@","@%arg8@","@%arg9@","@%arg10@");");

            %index++;
        }
    }
}

function foreachInArea(%pos, %radius, %mask, %function, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9, %arg10)
{
    InitContainerRadiusSearch(%pos, %radius, %mask);

    // Add all items found to the set, it has it's own internal counting and shelfkeeping mechanisms
    while((%int = ContainerSearchNext()) != 0)
        eval(%function@"("@%int@","@%arg1@","@%arg2@","@%arg3@","@%arg4@","@%arg5@","@%arg6@","@%arg7@","@%arg8@","@%arg9@","@%arg10@");");
}

function createRandomSimSet()
{
    %rnd = "RandomSimSetN"@mFloor(getRandom(1, 9999));

    if(!isObject(%rnd))
        %set = new SimSet(%rnd);
    else
        %set = createRandomSimSet();

    return %set;
}

function createRandomSimGroup()
{
    %rnd = "RandomSimGroupN"@mFloor(getRandom(1, 9999));

    if(!isObject(%rnd))
        %set = new SimGroup(%rnd);
    else
        %set = createRandomSimGroup();

    return %set;
}
