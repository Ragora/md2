//----------------------------------------------------------------------------
// Score HUD Overrides

// String table for menus
$Menu::Main = 0;
$MDMenuName[0] = "Main Menu";

$Menu::Score = 1;
$MDMenuName[1] = "Score Menu";

$Menu::Vehicle = 2;
$MDMenuName[2] = "Vehicle Config";

$Menu::Mech = 3;
$MDMenuName[3] = "Mech Lab";

$Menu::Manual = 4;
$MDMenuName[4] = "Manual";

$Menu::Stats = 5;
$MDMenuName[5] = "Stats";

$Menu::Account = 6;
$MDMenuName[6] = "Account";

$MenuState::Default = 0;
$MenuState::Static = 1;

// Gametype overloads
// updateScoreHud
function DefaultGame::updateScoreHud(%game, %client, %tag)
{
     EngineBase.updateScoreHud(%game, %client, %tag);
}

function HuntersGame::updateScoreHud(%game, %client, %tag)
{
     EngineBase.updateScoreHud(%game, %client, %tag);
}

function CTFGame::updateScoreHud(%game, %client, %tag)
{
     EngineBase.updateScoreHud(%game, %client, %tag);
}

function DMGame::updateScoreHud(%game, %client, %tag)
{
     EngineBase.updateScoreHud(%game, %client, %tag);
}

function SiegeGame::updateScoreHud(%game, %client, %tag)
{
     EngineBase.updateScoreHud(%game, %client, %tag);
}

function ConstructionGame::updateScoreHud(%game, %client, %tag)
{
     EngineBase.updateScoreHud(%game, %client, %tag);
}

// processGameLink
function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     EngineBase.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     EngineBase.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     EngineBase.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     EngineBase.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     EngineBase.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

function DefaultGame::processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     EngineBase.processGameLink(%game, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
}

// Code
function EngineBase::updateScoreHud(%this, %game, %client, %tag)
{
     if(%client.scoreHudMenuState $= "")
          %client.scoreHudMenuState = $MenuState::Default;
     
     if(%client.scoreHudMenuState == $MenuState::Static)
          return;
     
     if(%client.scoreHudMenu $= "")
          if(%client.defaultMenu $= "") 
               %client.scoreHudMenu = $Menu::Main;
          else
               %client.scoreHudMenu = %client.defaultMenu;
          
     %client.menuHudTag = %tag;
     
     messageClient(%client, 'SetScoreHudHeader', "", "");
     messageClient(%client, 'SetScoreHudHeader', "", '<just:center><a:gamelink\tGL\t%1>Score</a> | <a:gamelink\tGL\t%2>Vehicle Config</a> | <a:gamelink\tGL\t%3>Mech Lab</a> | <a:gamelink\tGL\t%4>Manual</a> | <a:gamelink\tGL\t%5>Stats</a> | <a:gamelink\tGL\t%6>Account</a> | <a:gamelink\tClose>Close</a>', $Menu::Score, $Menu::Vehicle, $Menu::Mech, $Menu::Manual, $Menu::Stats, $Menu::Account);               
     renderMainMenu(%game, %client, %tag);
}

function EngineBase::processGameLink(%this, %game, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     %tag = %client.menuHudTag;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     %index = 0;
     
     if(%arg1 $= "GL")
     {
          %client.scoreHudMenu = %arg2;
          %game.processGameLink(%client, "Main");
          return;  
     }
     
     switch(%client.scoreHudMenu)
     {
          case $Menu::Score:
               scoreProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
               
          case $Menu::Vehicle:
               vehicleProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Mech:
               mechProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Stats:
               statsProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Manual:
               manualProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);

          case $Menu::Account:
               accountProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5);
                                                                           
          default:
//               echo(%arg1 SPC %arg2 SPC %arg3);
//               %client.scoreHudMenu = %arg2;
//               %game.processGameLink(%client, %arg1);
               closeModuleHud(%client);
     }
}

function renderMainMenu(%game, %client, %tag)
{
     switch(%client.scoreHudMenu)
     {
          case $Menu::Score:
//               %client.scoreHudMenuState = $MenuState::Default;          
               scoreMainMenuRender(%game, %client, %tag);
               
          case $Menu::Vehicle:      
               vehicleMainMenuRender(%game, %client, %tag);
               
          case $Menu::Mech:
               mechMainMenuRender(%game, %client, %tag);

          case $Menu::Stats:
               statsMainMenuRender(%game, %client, %tag);

          case $Menu::Manual:
               manualMainMenuRender(%game, %client, %tag);

          case $Menu::Account:
               accountMainMenuRender(%game, %client, %tag);
                                                            
          default:
               %index = 0;
               
               messageClient(%client, 'ClearHud', "", %tag, 0);     
               messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Meltdown v%1 -- Menu Selector', EngineBase.ModVersion);
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Click on an option above to browse that specific sub-menu.');
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'New to Meltdown? <a:gamelink\tGL\t%1>Click here to browse the MANUAL</a>!' , $Menu::Manual ) ;
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Click the <a:gamelink\tGL\t%1>VEHICLE CONFIG</a> tab to customize your vehicles.' , $Menu::Vehicle ) ;
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Click the <a:gamelink\tGL\t%1>MECH LAB</a> tab to customize your mechs.' , $Menu::Mech ) ;
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , '( Mechs can be bought at vehicle stations. )' , $Menu::Mech ) ;
               %index++;
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>You may also drop by the forums at <a:wwwlink\tforum.radiantage.com>http://forum.radiantage.com/</a>');
               %index++;
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>for Meltdown 2 updates and downloads, and other Radiant Age projects.');
               %index++;
                                        
               messageClient(%client, 'ClearHud', "", %tag, %index);          
     }
}

function closeModuleHud(%client)
{
     %tag = %client.menuHudTag;
     messageClient(%client, 'ClearHud', "", %tag, 0);
     serverCmdHideHud(%client, 'scoreScreen');
     commandToClient(%client, 'setHudMode', 'Standard', "", 0);
     %client.scoreHudMenuState = $MenuState::Default;
     %client.scoreHudMenu = %client.defaultMenu;

     %obj = %client.player;	// +[soph]
     if( !isObject( %obj ) )	// this lot had -better- fix it!
          return;
     if( %obj.isMounted() )
     {
          %mount = %obj.getObjectMount();
          %block = %mount.getDataBlock();
          for (%i = 0; %i < %block.numMountPoints; %i++)
          {
               if ( %mount.getMountNodeObject( %i ) == %obj )
               {
                    %node = %i;
                    break;
               }
          }
          %block.playerMounted( %mount , %obj , %node );
     }				// +[/soph]
}

//------------------------------------------------------------------------------
function scoreMainMenuRender(%game, %client, %tag)
{
   if (Game.numTeams > 1)
   {
      // Send header:
//      messageClient( %client, 'SetScoreHudHeader', "", '<tab:15,315>\t%1<rmargin:260><just:right>%2<rmargin:560><just:left>\t%3<just:right>%4', 
//            %game.getTeamName(1), $TeamScore[1], %game.getTeamName(2), $TeamScore[2] );

      // Send subheader:
      messageClient( %client, 'SetScoreHudSubheader', "", '<tab:15,315>\t%3 (%1)<rmargin:260><just:right>SCORE: %4<rmargin:560><just:left>\t%5 (%2)<just:right>SCORE: %6',
            $TeamRank[1, count], $TeamRank[2, count], %game.getTeamName(1), $TeamScore[1], %game.getTeamName(2), $TeamScore[2] );

      %index = 0;
      while ( true )
      {
         if ( %index >= $TeamRank[1, count]+2 && %index >= $TeamRank[2, count]+2 )
            break;

         //get the team1 client info
         %team1Client = "";
         %team1ClientScore = "";
         %col1Style = "";
         if ( %index < $TeamRank[1, count] )
         {
            %team1Client = $TeamRank[1, %index];
            %team1ClientScore = %team1Client.score $= "" ? 0 : %team1Client.score;
            %col1Style = %team1Client == %client ? "<color:dcdcdc>" : "";
            %team1playersTotalScore += %team1Client.score;
         }
         else if( %index == $teamRank[1, count] && $teamRank[1, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team1ClientScore = "--------------";
         }
         else if( %index == $teamRank[1, count]+1 && $teamRank[1, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team1ClientScore = %team1playersTotalScore != 0 ? %team1playersTotalScore : 0;
         }
         //get the team2 client info
         %team2Client = "";
         %team2ClientScore = "";
         %col2Style = "";
         if ( %index < $TeamRank[2, count] )
         {
            %team2Client = $TeamRank[2, %index];
            %team2ClientScore = %team2Client.score $= "" ? 0 : %team2Client.score;
            %col2Style = %team2Client == %client ? "<color:dcdcdc>" : "";
            %team2playersTotalScore += %team2Client.score;
         }
         else if( %index == $teamRank[2, count] && $teamRank[2, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team2ClientScore = "--------------";
         }
         else if( %index == $teamRank[2, count]+1 && $teamRank[2, count] != 0 && %game.class $= "CTFGame") // z0dd - ZOD, 9/29/02. Removed T2 demo code from here
         {
            %team2ClientScore = %team2playersTotalScore != 0 ? %team2playersTotalScore : 0;
         }

         //if the client is not an observer, send the message
         if (%client.team != 0)
         {
            messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush>%5<clip:200>%1</clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:200>%3</clip><just:right>%4', 
                  %team1Client.name, %team1ClientScore, %team2Client.name, %team2ClientScore, %col1Style, %col2Style );
         }
         //else for observers, create an anchor around the player name so they can be observed
         else
         {
            messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush>%5<clip:200><a:gamelink\t%7>%1</a></clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:200><a:gamelink\t%8>%3</a></clip><just:right>%4', 
                  %team1Client.name, %team1ClientScore, %team2Client.name, %team2ClientScore, %col1Style, %col2Style, %team1Client, %team2Client );
         }

         %index++;
      }
   }
   else
   {
      //tricky stuff here...  use two columns if we have more than 15 clients...
      %numClients = $TeamRank[0, count];
      if ( %numClients > $ScoreHudMaxVisible )
         %numColumns = 2;

      // Clear header:
//      messageClient( %client, 'SetScoreHudHeader', "", "" );

      // Send header:
      if (%numColumns == 2)
         messageClient(%client, 'SetScoreHudSubheader', "", '<tab:15,315>\tPLAYER<rmargin:270><just:right>SCORE<rmargin:570><just:left>\tPLAYER<just:right>SCORE');
      else
         messageClient(%client, 'SetScoreHudSubheader', "", '<tab:15>\tPLAYER<rmargin:270><just:right>SCORE');

      %countMax = %numClients;
      if ( %countMax > ( 2 * $ScoreHudMaxVisible ) )
      {
         if ( %countMax & 1 )
            %countMax++;
         %countMax = %countMax / 2;
      }
      else if ( %countMax > $ScoreHudMaxVisible )
         %countMax = $ScoreHudMaxVisible;

      for ( %index = 0; %index < %countMax; %index++ )
      {
         //get the client info
         %col1Client = $TeamRank[0, %index];
         %col1ClientScore = %col1Client.score $= "" ? 0 : %col1Client.score;
         %col1Style = %col1Client == %client ? "<color:dcdcdc>" : "";

         //see if we have two columns
         if ( %numColumns == 2 )
         {
            %col2Client = "";
            %col2ClientScore = "";
            %col2Style = "";

            //get the column 2 client info
            %col2Index = %index + %countMax;
            if ( %col2Index < %numClients )
            {
               %col2Client = $TeamRank[0, %col2Index];
               %col2ClientScore = %col2Client.score $= "" ? 0 : %col2Client.score;
               %col2Style = %col2Client == %client ? "<color:dcdcdc>" : "";
            }
         }

         //if the client is not an observer, send the message
         if (%client.team != 0)
         {
            if ( %numColumns == 2 )
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush>%5<clip:195>%1</clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:195>%3</clip><just:right>%4', 
                     %col1Client.name, %col1ClientScore, %col2Client.name, %col2ClientScore, %col1Style, %col2Style );
            else
               messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:25>\t%3<clip:195>%1</clip><rmargin:260><just:right>%2', 
                     %col1Client.name, %col1ClientScore, %col1Style );
         }
         //else for observers, create an anchor around the player name so they can be observed
         else
         {
            if ( %numColumns == 2 )
               messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush>%5<clip:195><a:gamelink\t%7>%1</a></clip><rmargin:260><just:right>%2<spop><rmargin:560><just:left>\t%6<clip:195><a:gamelink\t%8>%3</a></clip><just:right>%4', 
                     %col1Client.name, %col1ClientScore, %col2Client.name, %col2ClientScore, %col1Style, %col2Style, %col1Client, %col2Client );
            else
               messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:25>\t%3<clip:195><a:gamelink\t%4>%1</a></clip><rmargin:260><just:right>%2', 
                     %col1Client.name, %col1ClientScore, %col1Style, %col1Client );
         }
      }

   }

   // Tack on the list of observers:
   %observerCount = 0;
   for (%i = 0; %i < ClientGroup.getCount(); %i++)
   {
      %cl = ClientGroup.getObject(%i);
      if (%cl.team == 0)
         %observerCount++;
   }

   if (%observerCount > 0)
   {
	   messageClient( %client, 'SetLineHud', "", %tag, %index, "");
      %index++;
		messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:10, 310><spush><font:Univers Condensed:22>\tOBSERVERS (%1)<rmargin:260><just:right>TIME<spop>', %observerCount);
      %index++;
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %cl = ClientGroup.getObject(%i);
         //if this is an observer
         if (%cl.team == 0)
         {
            %obsTime = getSimTime() - %cl.observerStartTime;
            %obsTimeStr = %game.formatTime(%obsTime, false);
		      messageClient( %client, 'SetLineHud', "", %tag, %index, '<tab:20, 310>\t<clip:150>%1</clip><rmargin:260><just:right>%2',
		                     %cl.name, %obsTimeStr );
            %index++;
         }
      }
   }

   //clear the rest of Hud so we don't get old lines hanging around...
   messageClient( %client, 'ClearHud', "", %tag, %index );
}

function scoreProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
   //the default behavior when clicking on a game link is to start observing that client
   %targetClient = %arg1;
   
   if(%arg1 $= "Close")
//   {
        closeModuleHud(%client);
//        return;		// -soph
//   }
   else if( %arg1 $= "Main" )	// +[soph]
        renderMainMenu(%game, %client, %tag);
   else				// +[/soph]
   if ((%client.team == 0) && isObject(%targetClient) && (%targetClient.team != 0))
   {
      %prevObsClient = %client.observeClient;
      
      // update the observer list for this client
      observerFollowUpdate( %client, %targetClient, %prevObsClient !$= "" );
      
      serverCmdObserveClient(%client, %targetClient);
      displayObserverHud(%client, %targetClient);
      
      if (%targetClient != %prevObsClient)
      {
         messageClient(%targetClient, 'Observer', '\c1%1 is now observing you.', %client.name);  
         messageClient(%prevObsClient, 'ObserverEnd', '\c1%1 is no longer observing you.', %client.name);  
      }
   }
}

//------------------------------------------------------------------------------
function vehicleMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);     
    // messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tVSelect>List Vehicles</a>');			// -soph
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tVSelect>CLICK HERE TO CONFIGURE A VEHICLE</a>');	// +soph
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Welcome to the Vehicle Configuration Screen! This is the screen that will allow');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>you to configure your vehicles as you see fit. Click on List Vehicles to get started.');
     %index++;

     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Keys for Vehicles<color:42dbea>');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Pack key: Use Module');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Next/Previous Weapon: Switch between weapons');
     %index++;   
     messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Mine key: Reload secondary weapon banks (Shrike only)' ) ;
     %index++ ;     

//     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//     %index++;     

//     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Saved Presets:');
//     %index++;     
                         
     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function vehicleProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
//     %tag = %client.moduleHudTag;
//     messageClient(%client, 'ClearHud', "", %tag, 0);
//     %index = 0;
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        case "VSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select Vehicle');
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Below is a list of customizable vehicles.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Click a vehicle name to customize its weaponry and equipment.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Your custom vehicle can then be created at your team\'s vehicle station.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
             %index++;

             for(%i = 0; %i < $VehicleCount; %i++)
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,250>\t<spush><clip:260><a:gamelink\tModVehicle\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $VehicleName[%i], $VehicleStats[%i], %i);	// messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush><clip:260><a:gamelink\tModVehicle\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $VehicleName[%i], $VehicleStats[%i], %i); -soph
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "ModVehicle":
             %index = 0;
             %client.scoreHudMenuName = "Select";
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Vehicle: %1', $VehicleName[%arg2]);
             %vData = $VehicleData[%arg2];

             %selected = %client.vModPriWeapon[%vData] $= "" ? $VehicleDefaultPrimaryWep[%arg2] : %client.vModPriWeapon[%vData];
             
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Primary Weapon:');
             %index++;

             %count = 0;
             
             for(%i = 0; %i < $VehicleWeaponCount; %i++)
                 if(%vData.vehicleType & $VehicleWeaponMask[%i])
                      if($VehicleWeaponSlot[%i] == 0)
                      {
                           if(%i == %selected)
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tPriWeaponMod\t%3\t%4>%1: %2</a></clip>', $VehicleWeaponName[%i], $VehicleWeaponDesc[%i], %i, %arg2);
                           else
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tPriWeaponMod\t%3\t%4>%1: %2</a></clip>', $VehicleWeaponName[%i], $VehicleWeaponDesc[%i], %i, %arg2);
                                
                           %index++;
                           %count++;
                      }

             if(%count == 0)
             {
                  messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600>%1: %2</clip>', $VehicleWeaponName[$VehicleWeapon::None], $VehicleWeaponDesc[$VehicleWeapon::None]);
                  %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Secondary Weapon:');
             %index++;

             %selected = %client.vModSecWeapon[%vData] $= "" ? $VehicleDefaultSecondaryWep[%arg2] : %client.vModSecWeapon[%vData];
             
             %count = 0;
             
             for(%i = 0; %i < $VehicleWeaponCount; %i++)
                 if(%vData.vehicleType & $VehicleWeaponMask[%i])
                      if($VehicleWeaponSlot[%i] == 1)
                      {
                           if(%i == %selected)
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tSecWeaponMod\t%3\t%4>%1: %2</a></clip>', $VehicleWeaponName[%i], $VehicleWeaponDesc[%i], %i, %arg2);
                           else
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tSecWeaponMod\t%3\t%4>%1: %2</a></clip>', $VehicleWeaponName[%i], $VehicleWeaponDesc[%i], %i, %arg2);
                                
                           %index++;
                           %count++;
                      }

             if(%count == 0)
             {
                  messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600>%1: %2</clip>', $VehicleWeaponName[$VehicleWeapon::None], $VehicleWeaponDesc[$VehicleWeapon::None]);
                  %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Module:');
             %index++;

             %selected = %client.vModule[%vData] $= "" ? $VehicleDefaultModule[%arg2] : %client.vModule[%vData];
             
             for(%i = 0; %i < $VehicleModuleCount; %i++)
                      if(%vData.vehicleType & $VehicleModuleMask[%i])
                      {
                           if(%i == %selected)
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleMod\t%3\t%4>%1: %2</a></clip>', $VehicleModuleName[%i], $VehicleModuleDesc[%i], %i, %arg2);
                           else
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleMod\t%3\t%4>%1: %2</a></clip>', $VehicleModuleName[%i], $VehicleModuleDesc[%i], %i, %arg2);
                                
                           %index++;
                      }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Shield:');
             %index++;

             %selected = %client.vModShield[%vData] $= "" ? $VehicleDefaultShield[%arg2] : %client.vModShield[%vData];
             
             for(%i = 0; %i < $VehicleShieldCount; %i++)
                      if(%vData.vehicleType & $VehicleShieldMask[%i])
                      {
                           if(%i == %selected)
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleShield\t%3\t%4>%1: %2</a></clip>', $VehicleShieldName[%i], $VehicleShieldDesc[%i], %i, %arg2);
                           else
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleShield\t%3\t%4>%1: %2</a></clip>', $VehicleShieldName[%i], $VehicleShieldDesc[%i], %i, %arg2);
                                
                           %index++;
                      }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Armor:');
             %index++;

             %selected = %client.vModArmor[%vData] $= "" ? $VehicleDefaultArmor[%arg2] : %client.vModArmor[%vData];
             
             for(%i = 0; %i < $VehicleArmorCount; %i++)
                      if(%vData.vehicleType & $VehicleArmorMask[%i])
                      {
                           if(%i == %selected)
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleArmor\t%3\t%4>%1: %2</a></clip>', $VehicleArmorName[%i], $VehicleArmorDesc[%i], %i, %arg2);
                           else
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleArmor\t%3\t%4>%1: %2</a></clip>', $VehicleArmorName[%i], $VehicleArmorDesc[%i], %i, %arg2);
                           %index++;
                      }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tVehicleResetDefaults\t%2\t%3><color:FFFF00>Reset vehicle defaults for %1</a>', $VehicleName[%arg2], %i, %arg2);
             %index++;

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tVSelect\t1><color:FF0000>Done modifying %1</a>', $VehicleName[%arg2]);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "PriWeaponMod":
             %vData = $VehicleData[%arg3];
             %client.vModPriWeapon[%vData] = %arg2;
             %game.processGameLink(%client, "ModVehicle", %arg3);
             return;
             
        case "SecWeaponMod":
             %vData = $VehicleData[%arg3];
             %client.vModSecWeapon[%vData] = %arg2;
             %game.processGameLink(%client, "ModVehicle", %arg3);
             return;

        case "VehicleMod":
             %vData = $VehicleData[%arg3];
             %client.vModule[%vData] = %arg2;
             %game.processGameLink(%client, "ModVehicle", %arg3);
             return;

        case "VehicleShield":
             %vData = $VehicleData[%arg3];
             %client.vModShield[%vData] = %arg2;
             %game.processGameLink(%client, "ModVehicle", %arg3);
             return;
             
        case "VehicleArmor":
             %vData = $VehicleData[%arg3];
             %client.vModArmor[%vData] = %arg2;
             %game.processGameLink(%client, "ModVehicle", %arg3);
             return;

        case "VehicleResetDefaults":
             %vData = $VehicleData[%arg3];
             %client.vModPriWeapon[%vData] = "";
             %client.vModSecWeapon[%vData] = "";             
             %client.vModule[%vData] = "";             
             %client.vModShield[%vData] = "";
             %client.vModArmor[%vData] = "";
             %game.processGameLink(%client, "ModVehicle", %arg3);                                       
             
        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

//------------------------------------------------------------------------------
function mechMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     
     messageClient(%client, 'ClearHud', "", %tag, 0);
    // messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tMSelect>List Mechs</a>');				// -soph
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tMSelect>CLICK HERE TO CONFIGURE A MECH</a>');	// +soph
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Welcome to the Mech Lab! This is the screen that will allow you to');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>configure your Mechs as you see fit. Click on List Mechs to get started.');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>If you are in a Mech, and wish to reconfigure your Firegroup setup, you');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>may also do that by selecting the Mech you are currently in and changing');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>the firegroup for your current weapons.');
     %index++;
               
     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Keys for Mechs<color:42dbea>');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Pack key: Fire all ready weapons at once');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Next/Previous Weapon: Switch between firegroups');
     %index++;     
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Beacon key: Flush coolant');
     %index++;     
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Mine key: Toggles missile targeting range; long-range needs more time to lock');
     %index++;     
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Grenade key: Shut down mech, cooling faster');
     %index++;     
     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Jump key: Double-tap to disembark from mech, but only when shut down');
     %index++;     
     
//     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//     %index++;     

//     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Saved Presets:');
//     %index++;     
     
     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function mechProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        case "MSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select Mech');

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Following is a list of available mech configurations.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>The loadout highlighted in white is your active mech configuration.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Your active mech will be provided when you buy a mech from a vehicle station.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Click a mech\'s name to set it as your active and view/edit its loadout.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "" ) ;
             %index++;
             for(%i = 0; %i < $MechCount; %i++)
             {
                 if( %client.currentMechSlot == %i )	// +[soph]
                     messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush><clip:260><color:FFFFFF><a:gamelink\tConfigMech\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $MechName[%i], $MechStats[%i], %i);
                 else					// +[/soph]
                     messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush><clip:260><a:gamelink\tConfigMech\t%3>%1</a></clip><spop><rmargin:720><just:left>\t<clip:320>%2</clip>', $MechName[%i], $MechStats[%i], %i);
                 %index++;
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "ConfigMech":
             %index = 0;
//             %client.scoreHudMenuName = "Select";
             %client.currentMechSlot = %arg2;		// +soph
             %baseWeight = $MechWeightLimit[%arg2];
             %name = $MechName[%arg2];
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Selected Mech: %1 (%2T)', %name, %baseWeight);
             %vData = $MechData[%arg2];
             %armorTon = $MechDefaultArmorWeight[%arg2];
             %modArmorVal = %client.modArmorVal[%arg2] !$= "" ? %client.modArmorVal[%arg2] : 0;
             %hp = %armorTon * $MechHPPerTon;
             %healthMod = ((%modArmorVal + %armorTon) * $MechHPPerTon / %hp);
             %health = %healthMod * %hp;
             %displayHP = mFloor(%health * 100);
             %heatsinkTon = $MechDefaultHeatSinkWeight[%arg2];
             %modHeatsinkVal = %client.modHeatsinkVal[%arg2] !$= "" ? %client.modHeatsinkVal[%arg2] : 0;
             %equipWeight = %armorTon + %modArmorVal + %heatsinkTon + %modHeatsinkVal;
             // show green if over, white if exact, red if above
             %numWeapons = $MechMaxWeapons[%arg2];

             %count = 0;             
             for(%i = 2; %i < 2+%numWeapons; %i++)	// for(%i = 1 + %numWeapons; %i > 1; %i--)
             {
                   %visNum = %numWeapons + 2 - %i;	// %visNum = %i - 1;
                   %idxNum = %i - 2;			// %idxNum = %numWeapons + 1 - %i;	
                   %wep = $MechHardPointSlot[%arg2, %i] & $MechHardpointType::None ? 0 : (%client.mechWeaponSlot[%arg2, %i] $= "" ? $MechHardPointSlot[%arg2, %i] : %client.mechWeaponSlot[%arg2, %i]);

                   if(!%wep)
                      continue;
                   else
                   {
                         %weaponID = %client.mechWeaponSlotID[%arg2, %idxNum] $= "" ? $MechDefaultWeapon[%arg2, %idxNum] : %client.mechWeaponSlotID[%arg2, %idxNum];
                         %display[%count] = "Weapon" SPC %visNum@":" SPC "["@$MechWeaponWeight[%weaponID]@"T]" SPC $MechWeaponName[%weaponID];
                         %hdpWeaponID[%count] = %weaponID;
                         %equipWeight += $MechWeaponWeight[%weaponID];
                         %client.mechSlotFireGroup[%arg2, %i] = %client.mechSlotFireGroup[%arg2, %i] $= "" ? $MechWeaponDefaultFiregroup[%weaponID] : %client.mechSlotFireGroup[%arg2, %i];
                         %firegroup[%count] = %client.mechSlotFireGroup[%arg2, %i]; 

                         for(%f = 0; %f < $MechMaxFireGroups; %f++)
                         {
                              %fgName = "<a:gamelink\tToggleFireGroup\t"@%arg2@"\t"@%i@"\t"@$MechFiregroupMask[%f]@">"@$MechFiregroupName[%f]@"</a>";
                              %inFiregroup = %firegroup[%count] & $MechFiregroupMask[%f] ? "<color:FFFFFF>"@%fgName@"<color:42DBEA>" : %fgName;
                              %firegroupText[%count] = %firegroupText[%count] $= "" ? %inFiregroup : %firegroupText[%count] SPC %inFiregroup;
                         }
                                                  
                         %count++;
                   }
             }

//             %modSelected = %client.mModule[%arg2] $= "" ? 0 : %client.mModule[%arg2];	// -[soph]
//             %moduleCount = 0;								// -
//												// -
//             for(%i = 0; %i < $MechModuleCount; %i++)						// -
//                  if($MechMask[%vData] & $MechModuleMask[%i])
//                  {
//                         %moduleID[%moduleCount] = %i;
//
//                         if(%modSelected == %i)
//                              %equipWeight += $MechModuleWeight[%moduleCount];                         
//
//                         %moduleCount++;
//                  }
//
//             %equipSelected = %client.mEquip[%arg2] $= "" ? 0 : %client.mEquip[%arg2];	// -[soph]
             %equipCount = 0;

             for(%i = 0; %i < $MechEquipCount; %i++)
                  if($MechMask[%vData] & $MechEquipMask[%i])
                  {
                         %equipID[%equipCount] = %i;

//                         if(%equipSelected == %i)						// -soph
                         if( %client.mEquip[ %arg2 , %i ] )					// +soph
                              %equipWeight += $MechEquipWeight[%equipCount];                         

                         %equipCount++;
                  }             
                               
             if(%equipWeight > %baseWeight)
             {
                 %client.mechOverweight[%arg2] = true;
                 %colorFlag = "<color:FF0000>";
             }
             else if(%equipWeight < %baseWeight)
             {
                 %client.mechOverweight[%arg2] = false;
                 %client.mechWeight = %equipWeight ;						// +soph             
                 %colorFlag = "<color:00FF00>";
             }
             else
             {
                 %client.mechOverweight[%arg2] = false;
                 %client.mechWeight = %equipWeight ;						// +soph  
                 %colorFlag = "<color:FFFF00>";
             }     

             if( %armorTon + %modArmorVal == 10 )
                 %armorColorFlag = "<color:FFFF00>" ;
             else
                 %armorColorFlag = "<color:00FF00>" ;

             if( %heatsinkTon + %modHeatsinkVal == 5 )
                 %heatColorFlag = "<color:FFFF00>" ;
             else
                 %heatColorFlag = "<color:00FF00>" ;

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:00FF00>Mech Stats');
             %index++;
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Armor: %1T (%2 HP) | Heat Sinks: %3 | Current Weight: %5%4T', (%armorTon+%modArmorVal), %displayHP, (%heatsinkTon+%modHeatsinkVal), %equipWeight, %colorFlag, %arg2);	// -soph
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center>Armor: %1%2T<color:42DBEA> (%3 HP) | Heat Sinks: %4%5T<color:42DBEA> | Current Weight: %7%6T' ,										// +soph
                            %armorColorFlag , ( %armorTon + %modArmorVal ) , %displayHP , %heatColorFlag , ( %heatsinkTon + %modHeatsinkVal ) , %equipWeight , %colorFlag , %arg2 ) ;										// +soph
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Armor <a:gamelink\tMechArmMod\t%6\t1>[+1]</a> <a:gamelink\tMechArmMod\t%6\t5>[+5]</a> <a:gamelink\tMechArmMod\t%6\t10>[+10]</a> <a:gamelink\tMechArmMod\t%6\t-1>[-1]</a> <a:gamelink\tMechArmMod\t%6\t-5>[-5]</a> <a:gamelink\tMechArmMod\t%6\t-10>[-10]</a>', (%armorTon+%modArmorVal), %displayHP, (%heatsinkTon+%modHeatsinkVal), %equipWeight, %colorFlag, %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Heat Sinks <a:gamelink\tMechHSMod\t%6\t1>[+1]</a> <a:gamelink\tMechHSMod\t%6\t5>[+5]</a> <a:gamelink\tMechHSMod\t%6\t10>[+10]</a> <a:gamelink\tMechHSMod\t%6\t-1>[-1]</a> <a:gamelink\tMechHSMod\t%6\t-5>[-5]</a> <a:gamelink\tMechHSMod\t%6\t-10>[-10]</a>', (%armorTon+%modArmorVal), %displayHP, (%heatsinkTon+%modHeatsinkVal), %equipWeight, %colorFlag, %arg2);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
                          
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:00FF00>Weapon Hardpoints (click on one to change weapons)');
             %index++;

             for(%i = 0; %i < %count; %i++)							// for(%i = %count - 1 ; %i >= 0 ; %i-- ) -soph
             {
                 messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tMechHDPConfigure\t%2\t%3\t%4>%1</a> - Firegroups: %5</clip>', %display[%i], %arg2, %hdpWeaponID[%i], %i, %firegroupText[%i]);             
                 %index++;
             }
             
//             messageClient(%client, 'SetLineHud', "", %tag, %index, "");			// -[soph]
//             %index++;									// -
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:00FF00>Module');
//             %index++;									// -
//												// -
//             for(%i = 0; %i < %moduleCount; %i++)						// -
//             {										// -
//                    if(%i == %modSelected)							// -
//                         messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tMechMod\t%3\t%4>%1: %2</a></clip>', "["@$MechModuleWeight[%moduleID[%i]]@"T]" SPC $MechModuleName[%moduleID[%i]], $MechModuleDesc[%moduleID[%i]], %arg2, %moduleID[%i]);
//                    else									// -
//                         messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tMechMod\t%3\t%4>%1: %2</a></clip>', "["@$MechModuleWeight[%moduleID[%i]]@"T]" SPC $MechModuleName[%moduleID[%i]], $MechModuleDesc[%moduleID[%i]], %arg2, %moduleID[%i]);
//                                								// -
//                    %index++;									// -
//             }										// -[/soph]

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:00FF00>Equipment');	// -soph
//             %index++;									// -soph

             for(%i = 0; %i < %equipCount; %i++)
             {
                  if( %client.mEquip[ %arg2 , %i ] )						// if(%i == %equipSelected) -soph
                         messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tMechEquip\t%3\t%4>[EQUIPPED] %1: %2</a></clip>', "["@$MechEquipWeight[%equipID[%i]]@"T]" SPC $MechEquipName[%equipID[%i]], $MechEquipDesc[%equipID[%i]], %arg2, %equipID[%i]);
                  else
                         messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tMechEquip\t%3\t%4>[NOT EQUIPPED] %1: %2</a></clip>', "["@$MechEquipWeight[%equipID[%i]]@"T]" SPC $MechEquipName[%equipID[%i]], $MechEquipDesc[%equipID[%i]], %arg2, %equipID[%i]);
                               
                  %index++;
             }

//             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//             %index++;
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Shield:');
//             %index++;

//             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//             %index++;
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Shield:');
//             %index++;
//
//             %selected = %client.vModShield[%vData] $= "" ? $MechDefaultShield[%arg2] : %client.vModShield[%vData];
//             
//             for(%i = 0; %i < $MechShieldCount; %i++)
//                      if(%vData.vehicleType & $MechShieldMask[%i])
//                      {
//                           if(%i == %selected)
//                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleShield\t%3\t%4>%1: %2</a></clip>', $MechShieldName[%i], $MechShieldDesc[%i], %i, %arg2);
//                           else
//                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleShield\t%3\t%4>%1: %2</a></clip>', $MechShieldName[%i], $MechShieldDesc[%i], %i, %arg2);
//                                
//                           %index++;
//                      }
//
//             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//             %index++;
//             messageClient(%client, 'SetLineHud', "", %tag, %index, '<color:00FF00>Armor:');
//             %index++;
//
//             %selected = %client.vModArmor[%vData] $= "" ? $MechDefaultArmor[%arg2] : %client.vModArmor[%vData];
//             
//             for(%i = 0; %i < $MechArmorCount; %i++)
//                      if(%vData.vehicleType & $MechArmorMask[%i])
//                      {
//                           if(%i == %selected)
//                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tVehicleArmor\t%3\t%4>%1: %2</a></clip>', $MechArmorName[%i], $MechArmorDesc[%i], %i, %arg2);
//                           else
//                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tVehicleArmor\t%3\t%4>%1: %2</a></clip>', $MechArmorName[%i], $MechArmorDesc[%i], %i, %arg2);
//                           %index++;
//                      }
//
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><a:gamelink\tMechResetDefaults\t%2\t%3><color:FFFF00>Reset Mech defaults for %1</a>', $MechName[%arg2], %i, %arg2);
             %index++;

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><a:gamelink\tMSelect\t1><color:FF0000>Done modifying %1</a>', $MechName[%arg2]);
             %index++;
             messageClient(%client, 'ClearHud', "", %tag, %index);
             return;

        case "MechHDPConfigure":
             %index = 0;
             
             %vData = $MechData[%arg2];
             %selected = %client.mechWeaponSlotID[%arg2, %arg4] $= "" ? $MechDefaultWeapon[%arg2, %arg4] : %client.mechWeaponSlotID[%arg2, %arg4];
             
             %count = 0;

             for(%i = 0; %i < $MechWeaponCount; %i++)
                 if($MechHardPointSlot[%arg2, %arg4+2] & $MechWeaponHardpoint[%i]) //%vData.vehicleType & $MechWeaponMask[%i])
                 {
                      if($MechMask[%vData] & $MechWeaponMask[%i])
                      {
                           if(%i == %selected)
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><color:FFFFFF><a:gamelink\tMechUpdateHDP\t%3\t%4\t%5>%1: %2</a></clip>', "["@$MechWeaponWeight[%i]@"T]" SPC $MechWeaponName[%i], $MechWeaponDesc[%i], %arg2, %i, %arg4);
                           else
                                messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25><clip:600><a:gamelink\tMechUpdateHDP\t%3\t%4\t%5>%1: %2</a></clip>', "["@$MechWeaponWeight[%i]@"T]" SPC $MechWeaponName[%i], $MechWeaponDesc[%i], %arg2, %i, %arg4);
                                     
                           %index++;
                      }
                 }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tConfigMech\t%1><color:FFFFFF>Back</a>', %arg2);
             %index++;

             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "MechUpdateHDP":
//             echo("MechUpdateHDP" SPC %arg1 SPC %arg2 SPC %arg3 SPC %arg4);        
             %vData = $MechData[%arg2];
             %client.mechWeaponSlotID[%arg2, %arg4] = %arg3;
             %client.mechSlotFireGroup[%arg2, %arg4] = $MechWeaponDefaultFiregroup[%arg3];
             %game.processGameLink(%client, "ConfigMech", %arg2);
             return;

        case "ToggleFireGroup":
//             echo("ToggleFireGroup" SPC %arg1 SPC %arg2 SPC %arg3 SPC %arg4);       
             %vData = $MechData[%arg2];
             %client.mechSlotFireGroup[%arg2, %arg3] ^= %arg4;

             if(%client.player.isMech && %client.player.mechID == %arg2)
                  %client.player.slotFireGroup[%arg3] = %client.mechSlotFireGroup[%arg2, %arg3];
                  
             %game.processGameLink(%client, "ConfigMech", %arg2);
             return;
             
        case "MechArmMod":
             %vData = $MechData[%arg2];
             %client.modArmorVal[%arg2] += %arg3;

             if( %client.modArmorVal[ %arg2 ] + $MechDefaultArmorWeight[ %arg2 ] < 10 )		// if((%client.modArmorVal[%arg2] + $MechDefaultArmorWeight[%arg2]) < 1) -soph
                    %client.modArmorVal[%arg2] = 10 - $MechDefaultArmorWeight[ %arg2 ] ;	// %client.modArmorVal[%arg2] -= %arg3; -soph
                    
             %game.processGameLink(%client, "ConfigMech", %arg2);
             return;
             
        case "MechHSMod":
             %vData = $MechData[%arg2];
             %client.modHeatsinkVal[%arg2] += %arg3;

             if( %client.modHeatsinkVal[ %arg2 ] + $MechDefaultHeatSinkWeight[ %arg2 ] < 5 )	// if((%client.modHeatsinkVal[%arg2] + $MechDefaultHeatSinkWeight[%arg2]) < 0) -soph
                    %client.modHeatsinkVal[%arg2] = 5 - $MechDefaultHeatSinkWeight[ %arg2 ] ;	// %client.modHeatsinkVal[%arg2] =- %arg3; -soph
                    
             %game.processGameLink(%client, "ConfigMech", %arg2);
             return;
             
        case "MechMod":
             %vData = $MechData[%arg2];
             %client.mModule[%arg2] = %arg3;
             %game.processGameLink(%client, "ConfigMech", %arg2);
             return;

        case "MechEquip":
             %vData = $MechData[%arg2];
												// +[soph]
             if( !%client.mEquip[ %arg2 , %arg3 ] && $MechMask[ %vData ] & $MechEquipMask[ %arg3 ] )
                    %client.mEquip[ %arg2 , %arg3 ] = true ; 					// +
             else										// +[/soph]
                    %client.mEquip[ %arg2 , %arg3 ] = false ;					// %client.mEquip[%arg2] = %arg3; -soph
             %game.processGameLink(%client, "ConfigMech", %arg2);
             return;

        case "MechResetDefaults":
             %slot = %client.currentMechSlot ;		// +[soph]
             %client.modArmorVal[ %slot ] = 0 ;	
             %client.modHeatsinkVal[ %slot ] = 0 ;
             %numWeapons = $MechMaxWeapons[ %slot ] ;
             for( %i = 2 ; %i < 2 + %numWeapons ; %i++ )
             {
                %idxNum = %i - 2 ;
                %client.mechWeaponSlot[ %slot , %i ] = $MechHardPointSlot[ %slot , %i ] ;
                %client.mechWeaponSlotID[ %slot , %idxNum ] = $MechDefaultWeapon[ %slot , %idxNum ] ;
                %client.mechSlotFireGroup[ %slot , %i ] = $MechWeaponDefaultFiregroup[ $MechDefaultWeapon[ %slot , %idxNum ] ] ;
             }
//             %client.mModule[ %slot ] = $MechDefaultModule[ %slot ] ;
//             %client.mEquip[ %slot ] = $MechDefaultEquip[ %slot ] ;
             for( %i = 0 ; %i < $MechEquipCount ; %i++ )
                %client.mEquip[ %slot , %i ] = $MechDefaultEquip[ %slot , %i ] ;
             %client.mechOverweight[ %slot ] = false ;	// +
             %client.mechWeight = %equipWeight ;	// + 
             %game.processGameLink( %client , "ConfigMech" , %slot ) ;
             return;					// +[/soph]

          %vData = $MechData[%arg2];

             %modArmorVal = %client.modArmorVal[%arg2] !$= "" ? %client.modArmorVal[%arg2] : 0;
             %hp = %armorTon * $MechHPPerTon;
             %healthMod = ((%modArmorVal + %armorTon) * $MechHPPerTon / %hp);
             %health = %healthMod * %hp;
             %displayHP = mFloor(%health * 100);
             %heatsinkTon = $MechDefaultHeatSinkWeight[%arg2];
             %modHeatsinkVal = %client.modHeatsinkVal[%arg2] !$= "" ? %client.modHeatsinkVal[%arg2] : 0;
             %equipWeight = %armorTon + %modArmorVal + %heatsinkTon + %modHeatsinkVal;
             // show green if over, white if exact, red if above
             %numWeapons = $MechMaxWeapons[%arg2];
//             echo("pulling stats:" SPC %armorTon SPC %modArmorVal SPC %hp SPC %healthMod SPC %health SPC %displayHP);
             
             %count = 0;             
             for(%i = 2; %i < 2+%numWeapons; %i++)
             {
                   %visNum = %i - 1;
                   %idxNum = %i - 2;
                   %wep = $MechHardPointSlot[%arg2, %i] & $MechHardpointType::None ? 0 : (%client.mechWeaponSlot[%arg2, %i] $= "" ? $MechHardPointSlot[%arg2, %i] : %client.mechWeaponSlot[%arg2, %i]);

                   if(!%wep) 
                      continue;
                   else
                   {
                         %weaponID = %client.mechWeaponSlotID[%arg2, %idxNum] $= "" ? $MechDefaultWeapon[%arg2, %idxNum] : %client.mechWeaponSlotID[%arg2, %idxNum];
                         %display[%count] = "Weapon" SPC %visNum@":" SPC "["@$MechWeaponWeight[%weaponID]@"T]" SPC $MechWeaponName[%weaponID];
                         %hdpWeaponID[%count] = %weaponID;
                         %equipWeight += $MechWeaponWeight[%weaponID];
                         %client.mechSlotFireGroup[%arg2, %i] = %client.mechSlotFireGroup[%arg2, %i] $= "" ? $MechWeaponDefaultFiregroup[%weaponID] : %client.mechSlotFireGroup[%arg2, %i];
                         %firegroup[%count] = %client.mechSlotFireGroup[%arg2, %i]; 

                         for(%f = 0; %f < $MechMaxFireGroups; %f++)
                         {
                              %fgName = "<a:gamelink\tToggleFireGroup\t"@%arg2@"\t"@%i@"\t"@$MechFiregroupMask[%f]@">"@$MechFiregroupName[%f]@"</a>";
                              %inFiregroup = %firegroup[%count] & $MechFiregroupMask[%f] ? "<color:FFFFFF>"@%fgName@"<color:42DBEA>" : %fgName;
                              %firegroupText[%count] = %firegroupText[%count] $= "" ? %inFiregroup : %firegroupText[%count] SPC %inFiregroup;
                         }
                                                  
                         %count++;
                   }
             }

//             %modSelected = %client.mModule[%arg2] $= "" ? 0 : %client.mModule[%arg2];	// -[soph]
//             %moduleCount = 0;								// -
//												// -
//             for(%i = 0; %i < $MechModuleCount; %i++)						// -
//                  if($MechMask[%vData] & $MechModuleMask[%i])					// -
//                  {										// -
//                         %moduleID[%moduleCount] = %i;					// -
//												// -
//                         if(%modSelected == %i)						// -
//                              %equipWeight += $MechModuleWeight[%moduleCount];		// -
//												// -
//                         %moduleCount++;							// -
//                  }										// -
//												// -
//             %equipSelected = %client.mEquip[%arg2] $= "" ? 0 : %client.mEquip[%arg2];	// -[/soph]
             %equipCount = 0;

             for(%i = 0; %i < $MechEquipCount; %i++)
                  if($MechMask[%vData] & $MechEquipMask[%i])
                  {
                         %equipID[%equipCount] = %i;

//                         if(%equipSelected == %i)						// -soph
                         if( %client.mEquip[ %arg2 , %i ] )					// +soph                 
                              %equipWeight += $MechEquipWeight[%equipCount];                         

                         %equipCount++;
                  }
                               
             if(%equipWeight > %baseWeight)
             {
                 %client.mechOverweight[%arg2] = true; 
                 %colorFlag = "<color:FF0000>";
             }
             else if(%equipWeight < %baseWeight)
             {
                 %client.mechOverweight[%arg2] = false;   
                 %client.mechWeight = %equipWeight ;	// +soph            
                 %colorFlag = "<color:00FF00>";
             }
             else
             {
                 %client.mechOverweight[%arg2] = false;
                 %client.mechWeight = %equipWeight ;	// +soph 
                 %colorFlag = "<color:FFFF00>";
             }     

             
//        case "VehicleShield":
//             %vData = $MechData[%arg3];
//             %client.vModShield[%vData] = %arg2;
//             %game.processGameLink(%client, "ModVehicle", %arg3);
//             return;
             
//        case "VehicleArmor":
//             %vData = $MechData[%arg3];
//             %client.vModArmor[%vData] = %arg2;
//             %game.processGameLink(%client, "ModVehicle", %arg3);
//             return;
             
        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

//------------------------------------------------------------------------------
function statsMainMenuRender(%game, %client, %tag)
{
     renderInfoCard(%client, %tag, %client);
}

function renderInfoCard(%client, %tag, %viewer)
{
     %index = 0;
     messageClient(%viewer, 'ClearHud', "", %tag, 0);     
     messageClient(%viewer, 'SetScoreHudSubheader', "", '<just:left>Infocard for %1<just:right><a:gamelink\tSServerSelect>[View Server Infocard]</a> <a:gamelink\tSSelect>[View Player Infocards]</a>', %client.name);
     
     %rank = getTrackedStat(%client.guid, "rank");

     if(%rank == 0 || %rank $= "")
          %rank = 0;

     %kills = getTrackedStat(%client.guid, "kills");
     %deaths = getTrackedStat(%client.guid, "deaths");

     if(%deaths == 0)
          %kdRatio = %kills;
     else
          %kdRatio = %kills ? %kills / %deaths : 0;

     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>General Information<color:42dbea>');
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Rank:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Account ID:</clip><just:right>%2', $StatRankID[%rank], %client.guid);
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Maps played:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Times logged in:</clip><just:right>%2', getTrackedStat(%client.guid, "mapsPlayed"), getTrackedStat(%client.guid, "connectCount"));
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Flag grabs:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Flag captures:</clip><just:right>%2', getTrackedStat(%client.guid, "flagtouches"), getTrackedStat(%client.guid, "flagcaps"));
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Total times kicked:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Total times banned:</clip><just:right>%2', getTrackedStat(%client.guid, "kicks"), getTrackedStat(%client.guid, "bans"));
     %index++;

     messageClient(%viewer, 'SetLineHud', "", %tag, %index, "");
     %index++;
                  
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Combat<color:42dbea>');
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Kills:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Deaths:</clip><just:right>%2', %kills, %deaths);
     %index++;     
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Suicides:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Kill-death Ratio:</clip><just:right>%2', getTrackedStat(%client.guid, "suicides"), %kdRatio);
     %index++;     
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Vehicle kills:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Mech kills:</clip><just:right>%2', getTrackedStat(%client.guid, "vehiclekills"), getTrackedStat(%client.guid, "mechkills"));
     %index++;     
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Bot kills:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Mid-air hits:</clip><just:right>%2', getTrackedStat(%client.guid, "botkills"), getTrackedStat(%client.guid, "MAs"));
     %index++;     
     messageClient( %viewer , 'SetLineHud' , "" , %tag , %index , '<tab:20,320>\t<spush><clip:200>Teamkills:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Team-killed:</clip><just:right>%2' , getTrackedStat( %client.guid , "teamkills" ) , getTrackedStat( %client.guid , "teamkilled" ) ) ;	// +soph
     %index++ ;     																																						// +soph

     messageClient(%viewer, 'SetLineHud', "", %tag, %index, "");
     %index++;
     
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFFFF>Inventory/Logistics<color:42dbea>');
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Inventory stations used:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Vehicles bought:</clip><just:right>%2', getTrackedStat(%client.guid, "inventoryusage"), getTrackedStat(%client.guid, "vehiclesbought"));
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Grenades thrown:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Mines thrown:</clip><just:right>%2', getTrackedStat(%client.guid, "throwngrenades"), getTrackedStat(%client.guid, "thrownmines"));
     %index++;
     messageClient(%viewer, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Healthkits used:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Total Soul Power gained:</clip><just:right>%2', getTrackedStat(%client.guid, "repairKitsUsed"), getTrackedStat(%client.guid, "totalsoulpower"));
     %index++;
                         
     messageClient( %viewer, 'ClearHud', "", %tag, %index);
}

function statsProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        case "SSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Select player for Infocard');
             
             for(%i = 0; %i < ClientGroup.getCount(); %i++)
             {
                 %tgt = ClientGroup.getObject(%i);

                 if(!%tgt.isAIControlled())
                 {                 
                      %rank = getTrackedStat(%tgt.guid, "rank");
               
                      if(%rank == 0 || %rank $= "")
                           %rank = 0;
                           
                      messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:25,325>\t<spush><clip:260><a:gamelink\tSInfocard\t%1>%2</a></clip><spop><rmargin:720><just:left>\t<clip:320>%3</clip>', %tgt, %tgt.name, $StatRankID[%rank]);
                      %index++;
                 }
             }

             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<a:gamelink\tMain\t1>Back</a>');
             %index++;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SServerSelect":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient(%client, 'ClearHud', "", %tag, 0);
             messageClient(%client, 'SetScoreHudSubheader', "", '<just:left>Infocard for %1<just:right><a:gamelink\tSSelect>[View Player Infocards]</a> <a:gamelink\tMain\t1>[Back]</a>', $Host::GameName);
          
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Server-wide Statistics');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Flag captures:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Objective captures:</clip><just:right>%2', getTrackedStat(0, "flagcaps"), getTrackedStat(0, "objectivecaps"));
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Inventory Stations used:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Maps played:</clip><just:right>%2', getTrackedStat(0, "inventoryusage"), getTrackedStat(0, "mapsplayed"));
             %index++;
             
             messageClient(%client, 'SetLineHud', "", %tag, %index, "");
             %index++;

             messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Server-wide Information');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Max Players:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Players online:</clip><just:right>%2', $Host::MaxPlayers, ClientGroup.getCount() - $HostGameBotCount);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Anti-baserape count:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Map time limit (minutes):</clip><just:right>%2', $Host::MD2::AntiBaseRapeCount, $Host::TimeLimit);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Repulsor Tick rate:</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Wormhole Tick rate:</clip><just:right>%2', $Host::RepulsorTickRate, $Host::WormholeTickRate);
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index, '<tab:20,320>\t<spush><clip:200>Respawn time (seconds):</clip><rmargin:260><just:right>%1<spop><rmargin:560><just:left>\t<clip:200>Suicide respawn (seconds):</clip><just:right>%2', $Host::MD2::RespawnTime, $Host::MD2::SuicideRespawnTime);
             %index++;

             messageClient( %client, 'ClearHud', "", %tag, %index);
             return;

        case "SInfocard":
           renderInfoCard(%arg2, %tag, %client);
           return;
             
        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

//------------------------------------------------------------------------------
function manualMainMenuRender(%game, %client, %tag)
{
			// major revisions, this whole section is getting rewritten +soph
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);     
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tMain>Overview</a> | <a:gamelink\tArmors>Armors</a> | <a:gamelink\tArmorMod>Armor Modules</a> | <a:gamelink\tWeapons>Weapons</a> | <a:gamelink\tPacks>Packs</a>');	// uncommented -soph
    // messageClient(%client, 'SetScoreHudSubheader', "", '<just:center><a:gamelink\tMain>Overview</a>');
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:FFFF00>Meltdown 2 In-Game Reference - Overview');
     %index++;     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>Click the four tabs above for more information on their specific topics.');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center>This information and more can also be found on the official wiki at');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><a:wwwlink\twiki.radiantage.com/index.php?title=Meltdown_2_Manual>http://wiki.radiantage.com/index.php?title=Meltdown_2_Manual</a>');
     %index++;
          
     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;
     
     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:00FF00>Special Functions');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '* Press your BEACON key [default: "H"] to change weapon modes. Most weapons' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '  have at least two firing modes, and some have more. Every mode has a use!' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '  You can still plant beacons as normal when close to an appropriate surface.' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '* Use your TARGETING LASER key [default: "L"] to access your armor type\'s ');
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '  "CSL", or "Class-Specific Laser". See the "ARMORS" tab above for the ' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '  specifics. You can cycle between your available CSLs and the standard  ' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '  targeting laser by pressing the same key again.' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '* Consume a REPAIR KIT [default: "Q"] if you catch on fire, become poisoned,' );
     %index++;
     messageClient(%client, 'SetLineHud', "", %tag, %index, '  or suffer EMP shock; this will remedy these otherwise dangerous conditions.' );
     %index++;

     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
     %index++;

//     messageClient(%client, 'SetLineHud', "", %tag, %index, "");
//     %index++;
     
//     messageClient(%client, 'SetLineHud', "", %tag, %index, '<just:center><color:0000FF>Damage Types');
//     %index++;
//     messageClient(%client, 'SetLineHud', "", %tag, %index, 'Meltdown 2 has 5 "types" of damage, these are: Energy, Explosive, Kinetic, Mitzi, and Plasma.');
//     %index++;
                                                                 
//     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function manualProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
     switch$(%arg1)
     {
			// i'm reviving and revising this whole thing for the current version +soph
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Static;
             return;

        case "Armors":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:FFFF00>Meltdown 2 In-Game Reference - Armors');
             %index++;     

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Scout');
             %index++;
                  
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', LightMaleHumanArmor.maxDamage * 100, LightMaleHumanArmor.maxWeapons, LightMaleHumanArmor.maxForwardSpeed * 3.6, LightMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: Small, fast, and maneuverable, but lacking in durability');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Sniper Rifle; Cloak, Vectorport, and Portal packs; Jetfire module.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: EMP pistol.');
             %index++;             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'While quite vulnerable in open combat, the Scout can engage enemies at');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'extreme range with its sniper rifle, make use of several unique infiltration');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'packs, and take to the air on demand with the Jetfire personal combat pod.');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Magnetic Ion');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', MagIonMaleHumanArmor.maxDamage * 100, MagIonMaleHumanArmor.maxWeapons, MagIonMaleHumanArmor.maxForwardSpeed * 3.6, MagIonMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: Ultralight ambusher/harasser.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Gains strength from killed enemies.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: Activates module ability.');
             %index++;             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'The definition of \'gnat\', the MagIon is adept at picking off weakened');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'foes. It also gains power with each kill made, and can burn this power');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'as ammo for its devastating "Soul" weapons.');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Blastech');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', BlastechMaleHumanArmor.maxDamage * 100, BlastechMaleHumanArmor.maxWeapons, BlastechMaleHumanArmor.maxForwardSpeed * 3.6, BlastechMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: Experimental, weighty medium frame.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Immunity to Explosive damage, massive energy pool.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: Siphon ELF.');
             %index++;             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Prototype medium armor protected by a special shield which absorbs Explosive');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'type damage. This shield is powered by a massive capacitor which also allows');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'for extensive use of jets and energy-hungry weaponry. This energy supply is');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'not infinite, however, and once exhausted will not regenerate on its own.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Lost energy can be recovered by making kills, leeching from nearby generators,');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'or draining enemies via ELF.');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Assault');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', MediumMaleHumanArmor.maxDamage * 100, MediumMaleHumanArmor.maxWeapons, MediumMaleHumanArmor.maxForwardSpeed * 3.6, MediumMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: Mid-range armor for a variety of roles.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Grenade Cannon and Autocannon Burst packs, Plasma Rifle 2.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: Repulsor - hold down to deflect nearby projectiles.');
             %index++;             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'The sturdiest medium armor class, and still plenty maneuverable. Brings a fair');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'selection of firepower to the table, especially with its pack weapons. An');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'excellent choice for supporting heavier teammates on offense.');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Engineer');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', EngineerMaleHumanArmor.maxDamage * 100, EngineerMaleHumanArmor.maxWeapons, EngineerMaleHumanArmor.maxForwardSpeed * 3.6, EngineerMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: Construction and support specialist, fragile but not toothless.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Deployables.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: Tools - Reassembler (adv. repair tool), Turret Barrel Swapper,');
             %index++;             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Disassembler (removes friendly deployables).');
             %index++;                          
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'While its main roles are maintaining the team\'s base assets and fielding');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'deployables, the Engineer can also offer its teammates support fire and field');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'repairs. While it is agile and surprisingly well-armed, its utter lack of');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'protection leads to quick and sudden deaths when facing competent threats.');
             %index++;
             

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Juggernaut');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', HeavyMaleHumanArmor.maxDamage * 100, HeavyMaleHumanArmor.maxWeapons, HeavyMaleHumanArmor.maxForwardSpeed * 3.6, HeavyMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: Heavily armed and armored, while still reasonably mobile.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Heavy weaponry; Stimpack module.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: Defender Designator - increases target\'s vulnerability.');
             %index++;             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'While both respectably durable and mobile, the Juggernaut can be a tad delicate');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'for its size and weight. Still, it brings impressive firepower to bear in a');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'hurry, but it is still best kept away from return fire whenever possible.');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:BBBBBB>Battle Angel');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF00>Armor: %1 HP - Weapons: %2 - Land Speed: %3 KPH - Energy: %4 KW', BattleAngelMaleHumanArmor.maxDamage * 100, BattleAngelMaleHumanArmor.maxWeapons, BattleAngelMaleHumanArmor.maxForwardSpeed * 3.6, BattleAngelMaleHumanArmor.maxEnergy);
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>Summary: The heaviest armor and most fearful armament.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF00>Specialties: Massive firepower; Overdrive and BASS Shield Extender modules.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:00FF88>C.S.L.: Varies depending on selected pack.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Battle Angels are the heaviest infantry found in the field, sporting an armory');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'of vehicle-grade weapons and powered by a prototype reactor that teeters on the');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'brink of explosive meltdown. While no easy mark to bring down, these armors are');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'hampered by dismal agility on the ground and in the air. Best suited for base');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'bombardment and defense; they are much more vulnerable in the open.');
             %index++;

             return;

        case "ArmorMod":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:FFFF00>Meltdown 2 In-Game Reference - Armor Modules');
             %index++;     
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '"Armor Modules" act as a sort of secondary pack for player armor. They tend');
             %index++;     
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'to be more passive in nature, and in some cases only function if the player');
             %index++;     
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'also carries a specific backpack. Modules can only be swapped out at full');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'service inventory stations, and cannot be otherwise discarded or lost.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Modules with the [A] prefix have to be triggered or turned on/off with the ');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '"Mine" key [default: "M"]. Modules with the "D+D" prefix only function');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '(properly) within range of a friendly deployed Defense+ Device.');
             %index++;

//             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
//             %index++;
//             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Light Armor Plating');
//             %index++;
//             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Lowers your weight at the expense of armor.');
//             %index++;
//             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Juggernaut');
//             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Heavy Armor Plating');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Increases your armor\'s durability, especially reducing damage' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'taken from rapid-fire weapons.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Blastech, Assault, Juggernaut, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Hardened Armor Plating');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Provides significant protection from Explosive damage and partial');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'protection from Plasma damage, but proves vulnerability to Energy damage.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Assault, Juggernaut, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Refractive Armor Plating');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Provides significant protection from Energy damage and partial');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'protection from Kinetic damage, but proves vulnerability to Explosive damage.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault, Juggernaut, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Deflective Armor Plating');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Provides significant protection from Kinetic damage and partial');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'protection from Explosive damage, but proves vulnerability to Plasma damage.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault, Juggernaut, Battle Angel');
             %index++;
	
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Insulated Armor Plating');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Provides significant protection from Plasma damage and partial');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'protection from Energy damage, but proves vulnerability to Kinetic damage.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault, Juggernaut, Battle Angel');
             %index++;  

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Anti-Concussion Armor');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Protects against concussion grenades.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault, Juggernaut, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Power Recirculator Module');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Reduces weapon energy consumption by 25%.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Assault, Juggernaut, Battle Angel');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Damage Amplifier Module');
             %index++;
             messageClient(%client, 'SetLineHud', "", %tag, %index , 'Function: Increases your weapons\' damage and armor\'s vulnerability.' ) ;
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault, Juggernaut, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Shocklance Extender Module');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Doubles shocklance range and energy cost.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>D+D Power Link Module');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Increases energy regeneration within D+D range.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Assault, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Soul Shield');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Absorbs ONE fatal hit, then expires.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer');
             %index++;
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Subspace Regenerator');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Slowly restores lost health, operates better with repair pack.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Assault, Juggernaut, Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Dual Mortar Fitting');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Allows Dual Grenade Cannon pack to fire Mortar grenades.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>[A] D+D Overdrive Mod');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Activate with the [mine] key.  Increases ground speed and damage output,');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  but suffers self-damage if used outside of a friendly D+D field.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>[A] Stimpack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Activate with the [mine] key.  Increases ground speed, damage output,');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  and energy regeneration for 30 seconds, but also increases damage sustained');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  and negates any self-repair.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Juggernaut');
             %index++;
                 
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Shield Hardener');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Increases shield pack durability by 25%.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Assault, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>D+D BASS Shield Extender');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Shield pack projects a stationary repulsion globe within range');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  of the team\'s deplyed D+D field.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>[A] Jetfire Transformer');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Activate with the [mine] key.  Encases the user in a light');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  aerial combat pod.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>D+D Final Stand');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Delays death by a few seconds when killed within range of the');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  team\'s deployed D+D.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++;  

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>[A] Shield Pulse Activator');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Requires Shield pack. Activate with the [mine] key. Deflects');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  nearby enemies and projectiles.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Assault, Battle Angel');
             %index++;  

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>(MagIon) Efficiency');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Reduces energy consumed by energy-reliant weapons.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  CSL activation costs 1 soul and instantly propels the');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  user forward.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Magnetic Ion');
             %index++;  

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>(MagIon) Power');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Increases damage output from all weapons. Improves as the');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  MagIon makes kills. CSL activation costs 1 soul and doubles the');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  power of the user\'s next "Soul" attack.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Magnetic Ion');
             %index++;  

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>(MagIon) Guardian');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Increases damage resistance. Improves as the MagIon makes');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  kills. CSL activation costs 4 souls and renders the user');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  invulnerable for a few seconds.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Magnetic Ion');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>(MagIon) Archmage');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Function: Prevents poisoning, combustion, and energy loss to EMP.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  CSL activation costs 2 souls and will blast away nearby enemies,');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  missiles, and bombs.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Magnetic Ion');
             %index++; 
                                                                                                                                                                                                                       
             return;

        case "Weapons":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:FFFF00>Meltdown 2 In-Game Reference - Weapons');
             %index++;     
     
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF88>Overview');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'There are too many weapons, with too many firing modes between them, ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'to list here. Most weapons will provide information at the bottom of ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'the screen when you take them out. Also, weapons with multiple firing ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'modes will provide details on each mode as you cycle through them ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'with the "place beacon" key [default: "H"]. Some weapons have up to ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'seven firing modes, but most have two or three. A few weapons have ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'modes that are not available to all classes that can carry the ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'weapon; these are skipped. Experiment with these different weapons ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'and modes: each and every one has its uses. ' );
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF88>Mitzi Blast Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'For new players reading this, you may want to get familiar with the ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Mitzi Blast Cannon. This weapon is available and useful to all armor ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'classes and is one of several weapons with recharging ammunition. The ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Mitzi\'s mode 4 "Booster" warrants your attention; even if you do not ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'make use of it, some enemies will and it will be useful to know what ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'they can do with it and its limitations. Also note that the fifth and ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'sixth modes can only be fired by heavy classes. ' );
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF88>Damage Types');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Every projectile in Meltdown 2 carries a damage type. Listed below are ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'the seven main type: ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* ENERGY bolts of many sorts can be partially absorbed with the Energy ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Resistance Plating armor module. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* KINETIC projectiles can be partially shrugged off by the Kinetic ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Resistance Plating armor module. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* EXPLOSIVE damage can be softened through use of an Explosive ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Resistance Plating armor module; the Blastech class\'s shield also ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  wholly nullifies this damage type so long as it has energy. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* PLASMA bursts and incendiaries damage players and can also light ' );
             %index++; 
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  enemies on fire. The damage can be dampened with a Plasma Resistance ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Plating armor module, which will also prevent unwanted candescence. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* Few weapons generate MITZI-type damage, but there is also no known ' );
             %index++; 
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  armor construct that resists this damage type. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* POISON weapons do not usually cause significant immediate harm, but ' );
             %index++; 
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  those on the receiving end will suffer gradual loss of health which ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  will become fatal if left untreated. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '* EMP blasts do not cause damage, per se, but instead drain the power ' );
             %index++; 
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  cores of everything within range. Fully-depleted cores will also ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  cease energy generation for a few seconds. ' );
             %index++;

             return;
             
        case "Packs":
             %index = 0;
             %client.scoreHudMenuState = $MenuState::Static;
             messageClient( %client, 'ClearHud', "", %tag, %index);
             
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:FFFF00>Meltdown 2 In-Game Reference - Packs');
             %index++;   

 // weapon packs
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF88>Weapon Packs');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center>These packs are shoulder-mounted weapons, and can be used at the same');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center>time as whatever the user has in their hands. They are fired with the');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center>PACK key [Default: "R"], so long as their ammo/energy needs are met.');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Shotgun Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a spread of energy bolts.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes energy from the user\'s energy core.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Magnetic Ion, Blastech');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Disc Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires an explosive disc on par with the handheld spinfusor.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes ammo from a 30-disc bin.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Magnetic Ion');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Mitzi Light Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a fast, light Mitzi bolt.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes ammunition from user\'s Mitzi Blast Cannon capacitor.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer, Assault');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>AutoCannon Burst');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a five-spread of autocannon shells.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes ammunition from a 150-shell drum.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Assault');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Streak SRM1 Mount');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires compact missiles which lock on to vehicles in line-of-sight. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Can also lock onto hot targets with a handheld Missile Launcher, ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  and will mimic the launcher\'s mode settings. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes missiles from a 12- or 16-warhead stock.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Assault, Blastech');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Grenade Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a self-propelled impact-triggered grenade.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes user\'s hand grenades.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Assault');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Streak SRM2 Mount');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires compact missiles in pairs, locks on to vehicles in ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  line-of-sight. Can also lock onto hot targets with a handheld ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Missile Launcher, and will mimic the launcher\'s mode settings. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes missiles from a 28-warhead stock.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Juggernaut');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Dual Pulse Cannons');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires pairs of modest-strength, explosive balls of energy.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes energy from the user\'s power core.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Star Hammer');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a heavy explosive propelled by a pulse-detonation drive.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes ammunition from a 10- or 20-shell supply.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Comet Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a devastating energy blast.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes energy from the user\'s power core.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Phaser Blast Cannon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a powerful kinetic blast.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes ammunition from a self-charging supply.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Dual Greande Cannons');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires a pair of hand grenades.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes user\'s hand grenades.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Streak SRM4 Mount');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Fires salvos of 4 mid-range missiles, locks on to vehicles in ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  line-of-sight. Can also lock onto hot targets with a handheld' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Missile Launcher, and will mimic the launcher\'s mode settings.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Consumes missiles from a 48-warhead stock.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Battle Angel');
             %index++; 

 // standard packs
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF88>Standard Backpacks');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Ammunition Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Increases ammunition supply/limit for most weapons. When ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  activated, regenerates all spent ammunition at the cost of the ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  wearer\'s full energy reserve. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Engineer, Blastech, Assault, Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Cloak pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Hides the wearer from pulse sensors. When activated, renders ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  the user invisibile but constantly drains energy.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Energy Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Increases energy regeneration rate. When activated converts ');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  a full bar of energy into an instant directional boost.');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Blastech, Assault, Juggernaut, Battle Angel');
             %index++;   

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Turbocharger');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Increases damage output of all weapons. Use with caution, may' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  detonate when shot, whether on the ground or someone\'s back.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Assault, Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Portal Generator');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  When activated, provides a handheld portal gun. This device creates' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  energy fields of one of two frequencies (dependant on mode). ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Objects entering one field will be expelled from the other.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Repair Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Enhances all repairs made to the wearer, including repair kits. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  When activated, provides a handheld repair tool.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Blastech, Assault, Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Sensor Jammer Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Renders the wearer and nearby allies invisible to pulse sensors.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Also counteracts nearby enemy cloaking and jamming devices.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Magnetic Ion, Blastech, Assault, Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Shield Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  When activated, protects the wearer by converting most incoming' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  damage into energy drain. Fails once energy is exhausted.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Assault, Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Synomium Device');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  When activated, protects the wearer from EMP and ELF weapons. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Also increases the rate of energy regeneration and weapons using ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  self-generating ammunition supplies. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Magnetic Ion, Blastech, Assault, Juggernaut, Battle Angel');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>VectorPort');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Hides the wearer from pulse sensors.  When activated, teleports ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  the wearer a short distance forward, passing through certain common ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  deployable barriers such as forcefields and blastwalls. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Satchel Charge');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  When activated, deploys a small explosive charge. Activate ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  again to remotely detonate the charge once it arms. ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Scout, Magnetic Ion, Engineer, Blastech, Assault, Juggernaut');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Ion Tag Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Targeting mechanism for a MANTA Satellite. When activated, ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  provides a red targeting laser which must be held steady on target ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  for 8 seconds to call in a MANTA ion strike.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

 // deployable packs
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center><color:00FF88>Deployable Packs');
             %index++;

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Defense+ Device');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Defence Enhancement device. Strengthens friendly defenses and' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  deployables within 325m range.  A core component of base protection.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Requires power from a nearby generator; takes 60 seconds to activate.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Base Turret');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Heavy deployable turret base with reconfigurable barrel.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Requires power from a nearby generator.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Sentry Turret');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable indoor turret with built-in motion sensor. Cannot be ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  deployed near flags.  Requires power from a nearby generator.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Landspike Turret');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable terrain-mounted defense turret.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Spider Clamp Turret');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable structure-mounted defense turret.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Blast Wall');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Sturdy deployable barrier, impassable until destroyed.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Blast Door');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Sturdy deployable barrier, opens to friendly touch or fire.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Forcefield Array');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable pylons which project forcefield barriers between pairs.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Remote Blastech Bunker');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable remote outpost. Provides limited shelter and' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  inventory access, and is also immune to Explosive-type damage. Must be' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  unpacked with a repair tool if deployed outside of D+D range.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Remote Defense Base');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable remote outpost. Provides limited shelter and' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  inventory access, as well as teleport access to friendly MANTAs. Must' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  be unpacked with a repair tool if deployed outside of D+D range.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Remote Vehicle Pad');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable vehicle bay. Requires power from a nearby' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  generator, Blastech Bunker, or Defense Base.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Teleporter Pad');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable teleporter. Once two are deployed, players may' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  step on either pad to be transferred to the other. Caution, players can' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  also make use of unprotected enemy pads.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>MANTA Satellite');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  High-altitude weapon platform. Once loaded, can discharge a' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  devastating beam at a single target. Must be manually loaded with a' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  fresh MANTA Ion Battery before each shot.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>MANTA Ion Battery');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Energy cell for a MANTA Satellite. Sufficient for one shot.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Must be deployed on a friendly MANTA Satellite.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>RMS Silo');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable artillery missile system. Automatically ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  manufactures and arms missiles which can then be targeted by a ' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Nightshade Support turbograv.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Large Inv Station');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable full-service inventory station.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Requires power from a nearby generator.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Weapon Repulsor');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Projects a bubble shield over an area which deflects enemy fire' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  while allowing teammates\' projectiles to pass unhindered.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Jammer Beacon');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable sensor jammer.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer');
             %index++; 


             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Inventory Station');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Deployable remote resupply station.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer, Assault');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Motion Sensor Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Two deployable motion sensors. Responds to movement, and can detect' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  nearby enemies under the protection of sensor jammers.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer, Assault');
             %index++; 

             messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<color:BBBBBB>Pulse Sensor Pack');
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , '  Two deployable pulse sensors.  Detects nearby enemies.' );
             %index++;
             messageClient( %client , 'SetLineHud' , "" , %tag , %index , 'Availabiltiy: Engineer, Assault');
             %index++; 
             return;
             
        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

//------------------------------------------------------------------------------
function accountMainMenuRender(%game, %client, %tag)
{
     %index = 0;
     messageClient(%client, 'ClearHud', "", %tag, 0);     
     messageClient(%client, 'SetScoreHudSubheader', "", '<just:center>Account Settings</a>'); //<a:gamelink\tVSelect>List Vehicles</a>');
     
//     messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center>Welcome to the Vehicle Configuration Screen! This is the screen that will allow');
     messageClient( %client , 'SetLineHud' , "" , %tag , %index , '<just:center>Account Information for: %1', %client.nameBase);     
     %index++;

     messageClient( %client , 'SetLineHud' , "" , %tag , %index , "");
     %index++;
     
     messageClient( %client , 'SetLineHud' , "" , %tag , %index , "<just:center><color:FFFFFF>Current IP: "@%client.getAddress()@" | Last login: "@$MDAccountData[%client.guid, "lastLogin"]@"<color:42dbea>");
     %index++;

     messageClient( %client , 'SetLineHud' , "" , %tag , %index , "<just:center><color:FFFFFF>Current Voice: "@$MDAccountData[%client.guid, "voice"]@"\tPitch: "@$MDAccountData[%client.guid, "voicePitch"]@"<color:42dbea>");
     %index++;
     
     messageClient( %client , 'SetLineHud' , "" , %tag , %index , "<just:center>Default Score Menu");
     %index++;

     %selected = %client.defaultMenu;
     %menuList = "";
     
     for(%i = 0; %i < 7; %i++)
     {
          %text = "<a:gamelink\tDSMenu\t"@%i@">"@$MDMenuName[%i]@"</a>";
          
          if(%i == %selected)
               %text = "<color:FFFFFF>"@%text@"<color:42DBEA>";
               
          if(%i == 0)
               %menuList = "<just:center>"@%text;
          else if(%i == 4)
          {
               messageClient( %client , 'SetLineHud' , "" , %tag , %index , %menuList);               
               %index++;
               %menuList = "<just:center>"@%text;
          }
          else
               %menuList = %menuList SPC "|" SPC %text;               
     }
     
     messageClient( %client , 'SetLineHud' , "" , %tag , %index , %menuList);
     %index++;

     if(%client.defaultLoadScreen)
          messageClient( %client , 'SetLineHud' , "" , %tag , %index , "Startup screen: <a:gamelink\tDefaultLoadScreen\t0>Mod Information and Picture</a> | <color:FFFFFF>Mod Information and Chat<color:42dbea>");
     else
          messageClient( %client , 'SetLineHud' , "" , %tag , %index , "Startup screen: <color:FFFFFF>Mod Information and Picture<color:42dbea> | <a:gamelink\tDefaultLoadScreen\t1>Mod Information and Chat</a>");
     %index++;

     %selected = %client.grenadeTimer;
     %menuList = "";
          
     for(%i = 0; %i < 3; %i++)
     {
          %time = %i+1;
          %text = "<a:gamelink\tgrenTimer\t"@%time@">"@%time@" sec</a>";
          
          if(%time == %selected)
               %text = "<color:FFFFFF>"@%text@"<color:42DBEA>";
               
          if(%i == 0)
               %menuList = %text;
          else
               %menuList = %menuList SPC "|" SPC %text;               
     }
          
     messageClient( %client , 'SetLineHud' , "" , %tag , %index , "Handgrenade fuse time:" SPC %menuList);
     %index++;     

     if(%client.modekeys)
          messageClient( %client , 'SetLineHud' , "" , %tag , %index , "1-6 Keys control: <a:gamelink\tModekeys\t0>Weapon Selection</a> | <color:FFFFFF>Direct Mode 1-6 selection<color:42dbea>");
     else
          messageClient(%client, 'SetLineHud', "", %tag, %index, "1-6 Keys control: <color:FFFFFF>Weapon Selection<color:42dbea> | <a:gamelink\tModekeys\t1>Direct Mode 1-6 selection</a>");
     %index++;

     if(%client.playEnemyHitSound)
          messageClient(%client, 'SetLineHud', "", %tag, %index, "Damaging hits on an enemy will play a sound: <a:gamelink\tHitsound\t0>No</a> | <color:FFFFFF>Yes<color:42dbea>");
     else
          messageClient(%client, 'SetLineHud', "", %tag, %index, "Damaging hits on an enemy will play a sound: <color:FFFFFF>No<color:42dbea> | <a:gamelink\tHitsound\t1>Yes</a>");
     %index++;

     if(%client.mitziAutoMode)
          messageClient(%client, 'SetLineHud', "", %tag, %index, "Mitzi AutoBoost: <a:gamelink\tAutoboost\t0>Disabled</a> | <color:FFFFFF>Enabled<color:42dbea>");
     else
          messageClient(%client, 'SetLineHud', "", %tag, %index, "Mitzi AutoBoost: <color:FFFFFF>Disabled<color:42dbea> | <a:gamelink\tAutoboost\t1>Enabled</a>");
     %index++;

     if(%client.showRank)
          messageClient(%client, 'SetLineHud', "", %tag, %index, "Show rank in chat messages: <a:gamelink\tShowrank\t0>No</a> | <color:FFFFFF>Yes<color:42dbea>");
     else
          messageClient(%client, 'SetLineHud', "", %tag, %index, "Show rank in chat messages: <color:FFFFFF>No<color:42dbea> | <a:gamelink\tShowrank\t1>Yes</a>");
     %index++;

     if( %client.deathFreeFly )						// +[soph]
          messageClient( %client , 'SetLineHud' , "" , %tag , %index , "Deathcam Freefly: <a:gamelink\tdeathFreeFly\t0>Disabled</a> | <color:FFFFFF>Enabled<color:42dbea>");
     else								// +
          messageClient( %client , 'SetLineHud' , "" , %tag , %index , "Deathcam Freefly: <color:FFFFFF>Disabled<color:42dbea> | <a:gamelink\tdeathFreeFly\t1>Enabled</a>");
     %index++ ;								// +[/soph]

     messageClient(%client, 'SetLineHud', "", %tag, %index, "Spawn Loadout: <a:gamelink\tSpawnLoadout\t0>Random</a> | <a:gamelink\tSpawnLoadout\t1>Disc, Blaster, Mitzi</a> | <a:gamelink\tSpawnLoadout\t2>Disc, Chaingun, Mitzi</a>");
     %index++;      
     messageClient(%client, 'SetLineHud', "", %tag, %index, "<a:gamelink\tSpawnLoadout\t3>Disc, Plasma, Protron</a> | <a:gamelink\tSpawnLoadout\t4>Mitzi, ELF, Plasma</a> | <a:gamelink\tSpawnLoadout\t5>Mitzi, Shocklance, Plasma</a>");
     %index++;      
     messageClient(%client, 'SetLineHud', "", %tag, %index, "Spawn Pack: <a:gamelink\tSpawnPack\t0>Repair Pack</a> | <a:gamelink\tSpawnPack\t1>Energy Pack</a> | <a:gamelink\tSpawnPack\t2>Turbocharger</a>");	// removed _plasma_ turbocharger -soph
     %index++; 
     messageClient(%client, 'SetLineHud', "", %tag, %index, "<a:gamelink\tSpawnPack\t3>Ammo Pack</a> | <a:gamelink\tSpawnPack\t4>Cloaking Pack</a> | <a:gamelink\tSpawnPack\t5>Synomium Device</a>");          
     %index++; 

     messageClient( %client, 'ClearHud', "", %tag, %index);
}

function accountProcessGameLink(%game, %tag, %client, %arg1, %arg2, %arg3, %arg4, %arg5)
{
//     %tag = %client.moduleHudTag;
//     messageClient(%client, 'ClearHud', "", %tag, 0);
//     %index = 0;
     switch$(%arg1)
     {
        case "Main":
             renderMainMenu(%game, %client, %tag);
             %client.scoreHudMenuState = $MenuState::Default;
             return;

        case "DSMenu":
             %client.defaultMenu = %arg2;
             %game.processGameLink(%client, "Main");
             return;
             
        case "DefaultLoadScreen":
             %client.defaultLoadScreen = %arg2;
             %game.processGameLink(%client, "Main");
             return;

        case "Modekeys":
             %client.modekeys = %arg2;
             %game.processGameLink(%client, "Main");
             return;

        case "Hitsound":
             %client.playEnemyHitSound = %arg2;
             %game.processGameLink(%client, "Main");
             return;

        case "Autoboost":
             %client.mitziAutoMode = %arg2;
             %game.processGameLink(%client, "Main");
             return;

        case "Showrank":
             %client.showRank = %arg2;
             %game.processGameLink(%client, "Main");
             return;

        case "SpawnLoadout":
             %client.spawnLoadout = %arg2;
             bottomprint(%client, "Spawn Loadout:" SPC slTranslateValue(%arg2), 5, 1);
             %game.processGameLink(%client, "Main");
             return;

        case "SpawnPack":
             %client.spawnPack = pTranslateValue(%arg2);
             bottomprint(%client, "Spawn Pack:" SPC spTranslateValue(%arg2), 5, 1);
             %game.processGameLink(%client, "Main");
             return;
                                       
        case "grenTimer":
             %client.grenadeTimer = %arg2;
             %game.processGameLink(%client, "Main");
             return;     
        
        case "deathFreeFly":					// +[soph]
             %client.deathFreeFly = %arg2 ;			// +
             %game.processGameLink( %client , "Main" ) ;	// +
             return ;						// +[/soph]
             
        default:
             closeModuleHud(%client);
             return;
     }

     // Just in case...
     closeModuleHud(%client);
}

function slTranslateValue(%value)
{
   switch(%value)
   {
      case 0:
         return "Random";
      case 1:
         return "Disc, Blaster, Mitzi";
      case 2:
         return "Disc, Chaingun, Mitzi";
      case 3:
         return "Disc, Plasma, Protron";
      case 4:
         return "Mitzi, ELF, Plasma";
      case 5:
         return "Mitzi, Shocklance, Plasma";

      default:
         return "NULL";
   }
}

function pTranslateValue(%value)
{
   switch(%value)
   {
      case 0:
         return "RepairPack";
      case 1:
         return "EnergyPack";
      case 2:
         return "HeatShieldPack";
      case 3:
         return "AmmoPack";
      case 4:
         return "CloakingPack";
      case 5:
         return "SynomiumPack";

      default:
         return "NULL";
   }
}

function spTranslateValue(%value)
{
   switch(%value)
   {
      case 0:
         return "Repair Pack";
      case 1:
         return "Energy Pack";
      case 2:
         return "Turbocharger";	// "Plasma Turbocharger"; -soph
      case 3:
         return "Ammunition Pack";
      case 4:
         return "Cloaking Pack";
      case 5:
         return "Synomium Device";

      default:
         return "NULL";
   }
}
