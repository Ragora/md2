if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ParticleData(PCRSparkParticle)
{
    dragCoefficient = 0.707317;
    gravityCoefficient = 0;
    windCoefficient = 0;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 500;
    lifetimeVarianceMS = 350;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.314961 1.000000 0.576000 1.000000";
    colors[1] = "0.560000 0.360000 0.260000 1.000000";
    colors[2] = "1.000000 0.360000 0.260000 0.000000";
    sizes[0] = 2.31452;
    sizes[1] = 0.25;
    sizes[2] = 0.25;
};

datablock ParticleEmitterData(PCRSparkEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 0;
    ejectionVelocity = 18;
    velocityVariance = 6.75;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
   lifeTimeMS = 200;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "PCRSparkParticle";
};

datablock ParticleData(GreenFireParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "0.110236 1.000000 0.136000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 3.83871;
    sizes[1] = 1.35484;
    sizes[2] = 0.8;
};

datablock ParticleEmitterData(GreenFireEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 17.4194;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "GreenFireParticle";
};

datablock ParticleData(WadBlueTrailParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "0.149606 0.296000 1.000000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 1.75;
    sizes[1] = 1.35484;
    sizes[2] = 0.8;
};

datablock ParticleEmitterData(WadBlueTrailEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 13.7903;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "WadBlueTrailParticle";
};

datablock ParticleData(WadGreenTrailParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "0.149606 1.000000 0.112000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 1.75;
    sizes[1] = 1.35484;
    sizes[2] = 0.8;
};

datablock ParticleEmitterData(WadGreenTrailEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 13.7903;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "WadGreenTrailParticle";
};

datablock ParticleData(ShrikeEngineParticle)
{
    dragCoefficient = 1.39024;
    gravityCoefficient = -0.2;
    windCoefficient = 0;
    inheritedVelFactor = 0;
    constantAcceleration = 0;
    lifetimeMS = 483;
    lifetimeVarianceMS = 427;
    useInvAlpha = 0;
    spinRandomMin = -96.7742;
    spinRandomMax = 629.032;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.25;
    times[2] = 1;
    colors[0] = "0.000000 0.248000 1.000000 1.000000";
    colors[1] = "1.000000 0.224000 0.000000 1.000000";
    colors[2] = "0.244094 0.248000 0.256000 0.000000";
    sizes[0] = 4.45968;
    sizes[1] = 3.10484;
    sizes[2] = 2.08871;
};

datablock ParticleEmitterData(ShrikeEngineEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 2;
    ejectionVelocity = 6.51613;
    velocityVariance = 3.32258;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 21.0484;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "ShrikeEngineParticle";
};

datablock ParticleData(SmallFlameParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 1654;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.892000 0.304000 0.340000 1.000000";
    colors[1] = "0.850394 0.304000 0.192000 0.850394";
    colors[2] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 1.97581;
    sizes[1] = 2.15163;
    sizes[1] = 2.35484;
};

datablock ParticleEmitterData(SmallFlameEmitter)
{
    ejectionPeriodMS = 18;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles = 0;
    orientOnVelocity = 1;
    particles = "SmallFlameParticle";
};

datablock ParticleData(OnFireParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0.3;
    windCoefficient = 0;
    inheritedVelFactor = 0.862903;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 749; //750; --- can't be > or = (ST)
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
//    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.850394 0.304000 0.192000 0.709677";
    colors[1] = "0.000000 0.000000 0.000000 0.451613";
    sizes[0] = 1.97581;
    sizes[1] = 1.35484;
};

datablock ParticleEmitterData(OnFireEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "OnFireParticle";
};

datablock ParticleData(PulseCannonTrailParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 1532;
    lifetimeVarianceMS = 282;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.850394 1.000000 0.256000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 0.790323;
    sizes[1] = 1.35484;
};

datablock ParticleEmitterData(PulseCannonTrailEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 3.25;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 7.98387;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "PulseCannonTrailParticle";
};

datablock ParticleData(WadRedTrailParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "1.000000 0.304000 0.120000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 1.75;
    sizes[1] = 1.35484;
    sizes[2] = 0.8;
};

datablock ParticleEmitterData(WadRedTrailEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 13.7903;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "WadRedTrailParticle";
};

datablock ParticleData(StarburstFlameParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0.3;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 749; //750; --- can't be > or = (ST)
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.850394 0.304000 0.192000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 1.97581;
    sizes[1] = 1.35484;
};

datablock ParticleEmitterData(StarburstFlameEmitter)
{
    ejectionPeriodMS = 15;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "StarburstFlameParticle";
};

// armor emitters

datablock ParticleData(LightArmorJetParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 150;
   lifetimeVarianceMS   = 50;
   textureName          = "particleTest";
   colors[0]     = "1.0 0.27 0.35 1.0";
   colors[1]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.75;
   sizes[1]      = 0.5;
};

datablock ParticleEmitterData(LightArmorJetEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 2.9;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 5;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "LightArmorJetParticle";
};

datablock ParticleData(MagIonArmorJetParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 150;
   lifetimeVarianceMS   = 50;
   textureName          = "particleTest";
   colors[0]     = "0.2 0.2 1.0 1.0";
   colors[1]     = "0.2 0.2 1.0 0";
   sizes[0]      = 0.75;
   sizes[1]      = 0.5;
};

datablock ParticleEmitterData(MagIonArmorJetEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 2.9;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 5;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MagIonArmorJetParticle";
};

datablock ParticleData(BlastechArmorJetParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 150;
   lifetimeVarianceMS   = 50;
   textureName          = "particleTest";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "1.0 1.0 1.0 0";
   sizes[0]      = 0.75;
   sizes[1]      = 0.5;
};

datablock ParticleEmitterData(BlastechArmorJetEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 2.9;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 5;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "BlastechArmorJetParticle";
};

datablock ParticleData(AssaultArmorJetParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 200;
    lifetimeVarianceMS = 50;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.149606 1.000000 0.112000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 0.5;
    sizes[1] = 1;
};

datablock ParticleEmitterData(AssaultArmorJetEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 1;
    velocityVariance = 1;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "AssaultArmorJetParticle";
};

datablock ParticleData(EngineerArmorJetParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 200;
    lifetimeVarianceMS = 50;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "1.149606 0.149606 0.112000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 0.5;
    sizes[1] = 1;
};

datablock ParticleEmitterData(EngineerArmorJetEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 1;
    velocityVariance = 1;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "EngineerArmorJetParticle";
};

datablock ParticleData(JuggernautArmorJetParticle)
{
    dragCoefficient = 2;
    gravityCoefficient = 0.2;
    windCoefficient = 0;
    inheritedVelFactor = 0.9;
    constantAcceleration = -1.1129;
    lifetimeMS = 137;
    lifetimeVarianceMS = 66;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "special/cloudflash2.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "1.000000 0.104000 0.000000 0.758065";
    colors[1] = "1.000000 0.648000 0.384000 0.000000";
    sizes[0] = 1.5;
    sizes[1] = 0.5;
};

datablock ParticleEmitterData(JuggernautArmorJetEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 2;
    ejectionVelocity = 24;
    velocityVariance = 6;
    ejectionOffset = 0;
    thetaMin = 0;
    thetaMax = 2.90323;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "JuggernautArmorJetParticle";
};

datablock ParticleData(BattleAngelArmorJetParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0.01;
    windCoefficient = 1;
    inheritedVelFactor = 0.624194;
    constantAcceleration = -1.6129;
    lifetimeMS = 325;
    lifetimeVarianceMS = 96;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "particleTest.png";
    times[0] = 0;
    times[1] = 0.112903;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 0.209677";
    colors[1] = "0.000000 0.304000 1.000000 0.314516";
    colors[2] = "0.464567 0.608000 1.000000 0.000000";
    sizes[0] = 1.5;
    sizes[1] = 1.5;
    sizes[2] = 0.5;
};

datablock ParticleEmitterData(BattleAngelArmorJetEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 2;
    ejectionVelocity = 9.78226;
    velocityVariance = 3.17742;
    ejectionOffset = 0;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "BattleAngelArmorJetParticle";
};

datablock ParticleData(Teleporter1Particle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -1.0;
    lifetimeMS = 1612;
    lifetimeVarianceMS = 0;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 314.516;
    textureName = "special/cloudflash7.png";
    times[0] = 0;
    times[1] = 0.532258;
    times[2] = 0.75;
    times[3] = 1;
    colors[0] = "0.125984 0.112000 0.920000 1.000000";
    colors[1] = "0.456693 0.208000 0.200000 0.774194";
    colors[2] = "1.000000 1.000000 1.000000 0.500000";
    colors[3] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 3.10484;
    sizes[1] = 4.50484;
    sizes[2] = 4.10484;
    sizes[3] = 3.10484;
};

datablock ParticleEmitterData(Teleporter1Emitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 0;
    ejectionVelocity = 10.25;
    velocityVariance = 0.25;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "Teleporter1Particle";
};

datablock ParticleData(CoolBlueTrailParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0.02;
    windCoefficient = 1;
    inheritedVelFactor = 0.1;
    constantAcceleration = 0;
    lifetimeMS = 1200;
    lifetimeVarianceMS = 100;
    useInvAlpha = 0;
    spinRandomMin = -90;
    spinRandomMax = 90;
    textureName = "special/expFlare.png";
    times[0] = 0;
    times[1] = 0.1;
    times[2] = 1;
    colors[0] = "0.228346 0.450000 1.000000 1.000000";
    colors[1] = "0.300000 0.300000 1.000000 0.800000";
    colors[2] = "0.300000 0.300000 1.000000 0.000000";
    sizes[0] = 1;
    sizes[1] = 2;
    sizes[2] = 3;
};

datablock ParticleEmitterData(CoolBlueTrailEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 1.5;
    velocityVariance = 0.3;
    ejectionOffset =   1;
    thetaMin = 0;
    thetaMax = 50;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "CoolBlueTrailParticle";
};
