// Meltdown 2 Client v1.0
// Compatible with Meltdown 2 v2.7.0+

if(!isObject(MeltdownClient))
{
   new ScriptObject(MeltdownClient)
   {
      class = MeltdownClient;
   };
}

// Meltdown 2 server loading speed improvements
//-----------------------------------------------------------------------------
function clientCmdMissionStartPhase1(%seq, %missionName, %musicTrack)
{
   echo( "got client StartPhase1..." );

   // Register Meltdown 2 Client with the server
   commandToServer('RegisterMDClient');
   
   // Reset the loading progress controls:
   LoadingProgress.setValue( 0 );
   DB_LoadingProgress.setValue( 0 );
   LoadingProgressTxt.setValue( "Downloading Datablocks..." );
   DB_LoadingProgressTxt.setValue( "Downloading Datablocks..." );

   clientCmdPlayMusic(%musicTrack);
   commandToServer('MissionStartPhase1Done', %seq);
   clientCmdResetCommandMap();
}

function ghostAlwaysStarted(%ghostCount)
{
   echo( "starting to ghost " @ %ghostCount @ " server objects....");

   LoadingProgress.setValue( 0.5 );
   DB_LoadingProgress.setValue( 0.5 );
   LoadingProgressTxt.setValue( "Loading Datablocks..." );
   DB_LoadingProgressTxt.setValue( "Loading Datablocks..." );
   Canvas.repaint();
   $ghostCount = %ghostCount;
   $ghostsRecvd = 0;
}

function ghostAlwaysObjectReceived()
{
   $ghostsRecvd++;
   %pct = ($ghostsRecvd / $ghostCount) / 2;

   if(%pct <= 0.5)
        %pct += 0.5;

   %lpct = mFloor(($ghostsRecvd / $ghostCount)*100);
   LoadingProgress.setValue( %pct );
   DB_LoadingProgress.setValue( %pct );
   LoadingProgressTxt.setValue( "Rendering live objects... "@$ghostsRecvd@"/"@$ghostCount@" ("@%lpct@"%)" );
   DB_LoadingProgressTxt.setValue( "Rendering live objects... "@$ghostsRecvd@"/"@$ghostCount@" ("@%lpct@"%)" );

    if(%pct == 1)
    {
        LoadingProgressTxt.setValue( "- Objects Loaded -" );
        DB_LoadingProgressTxt.setValue( "- Objects Loaded -" );
        LoadingProgress.setValue( 1 );
        DB_LoadingProgress.setValue( 1 );
    }

   Canvas.repaint();
}

function ClientReceivedDataBlock(%index, %total)
{
   %pct = (%index / %total) / 2;
   %dpct = mFloor((%index / %total)*100);
   LoadingProgress.setValue( %pct );
   LoadingProgress.setValue( %pct );
   LoadingProgressTxt.setValue( "Downloading Datablocks... "@%index@"/"@%total@" ("@%dpct@"%)" );
   DB_LoadingProgressTxt.setValue( "Downloading Datablocks... "@%index@"/"@%total@" ("@%dpct@"%)" );

    if(%pct == 0.5)
    {
        LoadingProgressTxt.setValue( "- Download Complete -" );
        DB_LoadingProgressTxt.setValue( "- Download Complete -" );
    }

   Canvas.repaint();
}

function clientCmdMissionStartPhase3(%seq, %missionName)
{
   $MSeq = %seq;

   //Reset Inventory Hud...
   if($Hud['inventoryScreen'] !$= "")
   {
      %favList = $Hud['inventoryScreen'].data[0, 1].type TAB $Hud['inventoryScreen'].data[0, 1].getValue();
      for ( %i = 1; %i < $Hud['inventoryScreen'].count; %i++ )
         if($Hud['inventoryScreen'].data[%i, 1].getValue() $= invalid)
            %favList = %favList TAB $Hud['inventoryScreen'].data[%i, 1].type TAB "EMPTY";
         else
            %favList = %favList TAB $Hud['inventoryScreen'].data[%i, 1].type TAB $Hud['inventoryScreen'].data[%i, 1].getValue();
      commandToServer( 'setClientFav', %favList );
   }
   else
      commandToServer( 'setClientFav', $pref::Favorite[$pref::FavCurrentSelect]);

   // needed?
   $MissionName = %missionName;
   //commandToServer( 'getScores' );

   // only show dialog if actually lights
   if(lightScene("sceneLightingComplete", $LaunchMode $= "SceneLight" ? "forceWritable" : ""))
   {
      error("beginning SceneLighting....");
      schedule(1, 0, "updateLightingProgress");
      $lightingMission = true;
      LoadingProgress.setValue( 0.0 );
      DB_LoadingProgress.setValue( 0.0 );
      LoadingProgressTxt.setValue( "Building Map Lights..." );
      DB_LoadingProgressTxt.setValue( "Building Map Lights..." );
      $missionLightStarted = true;
      Canvas.repaint();
   }
}

function sceneLightingComplete()
{
   LoadingProgress.setValue( 1 );
   DB_LoadingProgress.setValue( 1 );

   LoadingProgressTxt.setValue( "Get ready to rock and roll!" );
   DB_LoadingProgressTxt.setValue( "Get ready to rock and roll!" );

   echo("Scenelighting done...");
   $lightingMission = false;

   cleanUpHuds();

   if($LaunchMode $= "SceneLight")
   {
      quit();
      return;
   }

   clientCmdResetHud();
   commandToServer('SetVoiceInfo', $pref::Audio::voiceChannels, $pref::Audio::decodingMask, $pref::Audio::encodingLevel);
   commandToServer('EnableVehicleTeleport', $pref::Vehicle::pilotTeleport );
   commandToServer('MissionStartPhase3Done', $MSeq);
}

function GuiControl::updateAltitude(%this)
{
   %alt = getControlObjectAltitude();
   vAltitudeText.setValue(%alt);
   %this.altitudeCheck = %this.schedule(100, "updateAltitude");
}

function GuiControl::updateSpeed(%this)
{
   %vel = getControlObjectSpeed();
   // convert from m/s to km/h
   %cVel = mFloor(%vel * 3.6); // m/s * (3600/1000) = km/h
   vSpeedText.setValue(%cVel);
   %this.speedCheck = %this.schedule(100, "updateSpeed");
}

// Meltdown 2 Client Hooks
//-----------------------------------------------------------------------------
function clientCmdClientEchoText(%text)
{
   echo(%text);
}

function clientCmdClientWarnText(%text)
{
   warn(%text);
}

function clientCmdClientErrorText(%text)
{
   error(%text);
}

function clientCmdCreateMessageBoxOK(%title, %text)
{
   MessageBoxOK(%title, %text);
}

// Meltdown 2 Clientside Graphing
//-----------------------------------------------------------------------------
function createColorGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createMonoGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createGraph(%symbol, %maxLines, %pct, %blankSymbol)
{
     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = "";

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%blankSymbol;

     %graph = "["@%level@"]";

     return %graph;
}

function clientCmdGraphVehHUD(%armor, %shield, %weapon)
{
     %armorgraph = createColorGraph("|", 22, %armor, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mFloor(%armor * 100);
     %shieldgraph = createColorGraph("|", 22, %shield, "<color:FF0000>", "<color:FFFF00>", "<color:00FF00>", "<color:777777>", 0.3, 0.6) SPC mFloor(%shield * 100);

     clientCmdBottomPrint("<just:left>Hull: "@%armorgraph@"% <just:right>Shield: "@%shieldgraph@"%\n<just:center>Speed: "@mFloor(getControlObjectSpeed() * 3.6)@" KPH | Current Weapon: "@%weapon, 1, 2);
}

function clientCmdGraphMechHUD(%heatPct, %graphHeat, %status, %word, %coolant)
{
     %color = %heatPct > 0.6 ? (%heatPct > 0.8 ? "<color:FF0000>" : "<color:FFFF00>") : "<color:00FF00>";
     %numLines = 60;
     %symbol = "|";
     %displayLines = mCeil(%heatPct * %numLines);
     %leftLines = %numLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@"<color:777777>";

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     clientCmdBottomPrint("<just:left> Heat:" SPC %graph SPC "<just:right>"@%color SPC mCeil(%graphHeat) SPC "<color:42dbea>K" NL "<just:center>Targeting Computer:" SPC %status SPC "| Firegroup:" SPC %word SPC "| Coolant:" SPC %coolant, 1, 2);
}

function clientCmdSetMode(%mode, %weapon)
{
     if($WeaponModeData[%weapon, %mode] !$= "")
          clientCmdBottomPrint("<color:ffffff>[Mode "@%mode+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %mode] NL $WeaponModeDescription[%weapon, %mode] NL $WeaponModeData[%weapon, %mode], 5, 3);
     else
          clientCmdBottomPrint("<color:ffffff>[Mode "@%mode+1@"/"@$WeaponModeCount[%weapon]+1@"] <color:42dbea>"@$WeaponModeName[%weapon]@" -> "@$WeaponMode[%weapon, %mode] NL $WeaponModeDescription[%weapon, %mode], 5, 2);
}

function clientCmdDisplayWeapon(%weapon, %weaponName, %mode, %line3)
{
   %weaponData = $WeaponData[%weapon] !$= "" ? true : false;

   if($WeaponModeCount[%weapon])
      %line2 = "<color:42dbea>Total <color:99FC99>"@$WeaponModeCount[%weapon]+1@"<color:42dbea> modes. Current mode: "@%mode+1@" [<color:FC9999>"@$WeaponMode[%weapon, %mode]@"<color:42dbea>]";
   else if(%weaponData)
      %line2 = $WeaponData[%weapon];

   if($WeaponModeCount[%weapon])
      clientCmdBottomPrint("<font:times new roman:18>Now using "@%weaponName@"\n"@%line2@"\nTo change fire modes, press the beacon key.", 5, 3);
   else if(%weaponData)
      clientCmdBottomPrint("<font:times new roman:18>Now using "@%weaponName@"\n"@%line2 NL %line3, 5, 3);
   else
      clientCmdBottomPrint("<font:times new roman:18>Now using "@%weaponName@"\n"@%line3, 3, 2);
}

// Old MD2 custom message plugs for dynamic window rendering clientside - v1200
//-----------------------------------------------------------------------------
function clientCmdGraphWindowDynamic(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   csGraphWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
}

function csGraphWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count)
{
   if(MeltdownClient.graphWindowThread)
      cancel(MeltdownClient.graphWindowThread);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;

         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3); // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%out@""@%msfhdr, %time+1, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%out@""@%msfhdr, %time+1, %lines);

         MeltdownClient.graphWindowThread = schedule(%timeMS, 0, csGraphWindowDynamicLoop, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%message, %timeout, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%message, %timeout, %lines);
      }
}

function clientCmdFlashWindowDynamic(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   csFlashWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
}

function csFlashWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   if(MeltdownClient.graphWindowThread)
      cancel(MeltdownClient.graphWindowThread);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 75;

      if(%colorTag $= "")
         %colorTag = "<color:42dbea>";

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;

         if((%count - 5) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 5);

         %nLength = %length - %count;

         if(%nLength < 0)
            %nLength = 0;

         if(%count < 5)
            %hcount = %count;
         else
            %hcount = 5;

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, %hcount)@""@%colorTag; // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);
         %end = getSubStr(%message, %count, %nlength);

         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);

         MeltdownClient.graphWindowThread = schedule(%timeMS, 0, csFlashWindowDynamicLoop, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%message, %timeout, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%message, %timeout, %lines);
      }
}

function clientCmdLineWindowDynamic(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   csLineWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
}

function csLineWindowDynamicLoop(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count) // founder points out the obvious on this one... LOL
{
   if(MeltdownClient.graphWindowThread)
      cancel(MeltdownClient.graphWindowThread);

      if(%count $= "")
         %count = 0;

      if(%lines $= "")
         %lines = 1;

      if(%timeout $= "")
         %timeout = 1;

      if(%timeMS $= "")
         %timeMS = 100;

      if(%colorTag $= "")
         %colorTag = "<color:42dbea>";

      %length = strlen(%message);

      if(%count <= %length)
      {
         %count++;

         if((%count - 3) < 0)
            %ncount = 0;
         else
            %ncount = (%count - 3);

         %nLength = %length - %count;

         if(%nLength < 0)
            %nLength = 0;

         %msfHdr = "<color:ffffff>"@getSubStr(%message, %ncount, 3)@""@%colorTag; // <font:Univers:16>
         %out = getSubStr(%message, 0, %ncount);
         %end = getSubStr($g_line100, %count, %nlength);


         %time = (%timeMS + 250) / 1000;

         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%out@""@%msfhdr@""@%end, %time+1, %lines);

         MeltdownClient.graphWindowThread = schedule(%timeMS, 0, csLineWindowDynamicLoop, %message, %timeMS, %lines, %timeout, %bCenter, %colorTag, %count);
      }
      else
      {
         if(%bCenter)
            clientCmdCenterPrint(%colorTag@""@%message, %timeout, %lines);
         else
            clientCmdBottomPrint(%colorTag@""@%message, %timeout, %lines);
      }
}

function clientGraphWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
	{
		%cl = ClientGroup.getObject(%i);

      if(!%cl.isAIControlled() && %cl.hasMD2Client)
      {
         %client.excludeFromWDA = true;
         commandToClient(%client, 'GraphWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
      }
   }
}

function clientFlashWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
	{
		%cl = ClientGroup.getObject(%i);

      if(!%cl.isAIControlled() && %cl.hasMD2Client)
      {
         %client.excludeFromWDA = true;
         commandToClient(%client, 'FlashWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
      }
   }
}

function clientLineWindowDynamicAll(%message, %timeMS, %lines, %timeout, %bCenter, %colorTag)
{
   %count = ClientGroup.getCount();
   for(%i = 0; %i < %count; %i++)
	{
		%cl = ClientGroup.getObject(%i);

      if(!%cl.isAIControlled() && %cl.hasMD2Client)
      {
         %client.excludeFromWDA = true;
         commandToClient(%client, 'LineWindowDynamic', %message, %timeMS, %lines, %timeout, %bCenter, %colorTag);
      }
   }
}

// Backup functions for the client
function loadMDClient()
{
     exec("scripts/autoexec/MDClient.cs");
     commandToServer('RegisterMDClient');
}

function initMDClient()
{
     if(!$MDClientInit)
     {
          exec("scripts/autoexec/MDClient.cs");
          $MDClientInit = true;
     }
}

schedule(1000, 0, initMDClient);
