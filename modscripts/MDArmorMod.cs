if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();
   
// MD Armor Modules
//----------------------------------------------------------------------------
function ArmorMod::onInventory(%data, %obj, %amount)
{
     if(!%obj.isPlayer())
          return;

     if(%amount)
          %data.onApply(%obj);
     else
          %data.onUnApply(%obj);
}

// Active Module counter for dynamic reorganizing (if necessary)
$ActiveModuleCount = 0;

//----------------------------------------------------------------------------
//datablock ItemData(NoneArmorMod)
//{
//   className = ArmorMod;
//   catagory = "ArmorMods";
//   shapeFile = "turret_muzzlepoint.dts";
//   elasticity = 0.2;
//   friction = 0.6;
//   pickupRadius = 2;
//   rotate = true;

//   image = "GravArmorPlatePackImage";
//	pickUpName = "an armor mod";
//   };

function NoneArmorMod::onApply(%data, %obj)
{
     // Do nothing!
}

function NoneArmorMod::onUnApply(%data, %obj)
{
     // Do nothing!
}

$ActiveModule::NoneArmorMod = $ActiveModuleCount;
$ActiveModuleCount++;


//----------------------------------------------------------------------------
function GravArmorPlatePack::onApply(%data, %obj)
{
     %obj.universalResistFactor += 0.33;
     %obj.mountImage(%data.image, $FlagSlot);	
     %obj.preventFlagCaps = true;		
}

function GravArmorPlatePack::onUnApply(%data, %obj)
{
     %obj.universalResistFactor -= 0.33;
     %obj.unmountImage($FlagSlot);		
     %obj.preventFlagCaps = false;		
}

//----------------------------------------------------------------------------
function PolarArmorPlatePack::onApply(%data, %obj)
{
     %obj.universalResistFactor -= 4 / 14 ;	// 0.33 ; -soph
     %obj.armorShrug = 0.04 ;			// +soph
//     %obj.mountImage(%data.image, $FlagSlot);	// -soph
//     %obj.preventFlagCaps = true;		// -soph
}

function PolarArmorPlatePack::onUnApply(%data, %obj)
{
     %obj.universalResistFactor += 4 / 14 ;	// 0.33 ; -soph
     %obj.armorShrug = 0.00 ;			// +soph
//     %obj.unmountImage($FlagSlot);		// -soph
//     %obj.preventFlagCaps = false;		// -soph
}

//----------------------------------------------------------------------------

datablock ItemData(MortarAdaptorMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function MortarAdaptorMod::onApply(%data, %obj)
{
     %obj.mortarNadeFitting = true;
}

function MortarAdaptorMod::onUnApply(%data, %obj)
{
     %obj.mortarNadeFitting = false;
}

//----------------------------------------------------------------------------

datablock ItemData(DaishiPowerMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function DaishiPowerMod::onApply(%data, %obj)
{
     %obj.daishiPowerModded = true;
     %obj.appliedPowerMod = false;
     
     daishiPowerCheck(%data, %obj);
}

function DaishiPowerMod::onUnApply(%data, %obj)
{
     %obj.daishiPowerModded = false;
     if( %obj.appliedPowerMod )							// +soph
          %obj.setRechargeRate( %obj.getRechargeRate() - 0.25 );		// +soph
}

function daishiPowerCheck(%data, %obj)
{
     if(isObject(%obj))
     {
          if(%obj.daishiPowerModded )						// && %obj.inDEDField) -soph
          {
               if( %obj.inDEDField )						// +soph
               {
                    if(!%obj.appliedPowerMod)
                    {
                         %obj.setRechargeRate(%obj.getRechargeRate() + 0.25);	// %obj.setRechargeRate(%obj.getRechargeRate() + 0.15); -soph
                         %obj.appliedPowerMod = true;
                    }
               
//                    schedule(200, 0, daishiPowerCheck, %data, %obj);		// +soph
//                    return;     						// +soph
               }
               else if(%obj.appliedPowerMod)
               {
                    %obj.setRechargeRate(%obj.getRechargeRate() - 0.25);	// %obj.setRechargeRate(%obj.getRechargeRate() - 0.15); -soph
                    %obj.appliedPowerMod = false;     
               }
               schedule( 250 , 0, daishiPowerCheck, %data, %obj);		// 500, 0, daishiPowerCheck, %data, %obj); -soph
          }
     }
}

//----------------------------------------------------------------------------
datablock ItemData(MagHandClampMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function MagHandClampMod::onApply(%data, %obj)
{
     %obj.magneticClamp = true;
	%obj.dynamicResistance[ $DamageGroupMask::Misc ] -= 1 / 2 ;	// +soph
}

function MagHandClampMod::onUnApply(%data, %obj)
{
     %obj.magneticClamp = false;
	%obj.dynamicResistance[ $DamageGroupMask::Misc ] += 1 / 2 ;	// +soph
}

//----------------------------------------------------------------------------
datablock ItemData(ReactiveArmorPlating)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function ReactiveArmorPlating::onApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Explosive ] -= 1 / 2 ;	// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Plasma ] -= 1 / 3 ;		// +
	%obj.dynamicResistance[ $DamageGroupMask::Energy ] += 1 / 4 ;		// +[/soph]
}

function ReactiveArmorPlating::onUnApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Explosive ] += 1 / 2 ;	// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Plasma ] += 1 / 3 ;		// +
	%obj.dynamicResistance[ $DamageGroupMask::Energy ] -= 1 / 4 ;		// +[/soph]
}

//----------------------------------------------------------------------------
datablock ItemData(ElectronFieldPlating)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function ElectronFieldPlating::onApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Energy ] -= 1 / 2 ;			// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Kinetic ] -= 1 / 3 ;			// +
	%obj.dynamicResistance[ $DamageGroupMask::Explosive ] += 1 / 4 ;		// +[/soph]
}

function ElectronFieldPlating::onUnApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Energy ] += 1 / 2 ;			// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Kinetic ] += 1 / 3 ;			// +
	%obj.dynamicResistance[ $DamageGroupMask::Explosive ] -= 1 / 4 ;		// +[/soph]
}

//----------------------------------------------------------------------------
datablock ItemData(InertialDampenerPlating)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function InertialDampenerPlating::onApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Kinetic ] -= 1 / 2 ;			// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Explosive ] -= 1 / 3 ;		// +
	%obj.dynamicResistance[ $DamageGroupMask::Plasma ] += 1 / 4 ;			// +[/soph]
}

function InertialDampenerPlating::onUnApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Kinetic ] += 1 / 2 ;			// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Explosive ] += 1 / 3 ;		// +
	%obj.dynamicResistance[ $DamageGroupMask::Plasma ] -= 1 / 4 ;			// +[/soph]
}

//----------------------------------------------------------------------------
datablock ItemData(PowerRecircMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function PowerRecircMod::onApply(%data, %obj)
{
     %obj.powerRecirculator = true;
}

function PowerRecircMod::onUnApply(%data, %obj)
{
     %obj.powerRecirculator = false;
}

//----------------------------------------------------------------------------
datablock ItemData(ShockExtendMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function ShockExtendMod::onApply(%data, %obj)
{
     %obj.extendedLanceMod = true;
}

function ShockExtendMod::onUnApply(%data, %obj)
{
     %obj.extendedLanceMod = false;
}

//----------------------------------------------------------------------------
datablock ItemData(AmpMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function AmpMod::onApply(%data, %obj)
{
     %obj.universalResistFactor += 0.15 ;
     %obj.damageMod += 0.25 ;
}

function AmpMod::onUnApply(%data, %obj)
{
     %obj.universalResistFactor -= 0.15 ;
     %obj.damageMod -= 0.25 ;     
}

//----------------------------------------------------------------------------
datablock ItemData(SoulShieldMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function SoulShieldMod::onApply(%data, %obj)
{
     if( %obj.currentArmorMod $= %data && %obj.soulShieldDamaged )				// +[soph]
     {												// +
          messageClient( %obj.client , 'MsgSoulShieldAdded' , '\c2* Soul Shield recharged.' ) ;	// +
          %obj.soulShieldDamaged = false ;							// +
     }												// +[/soph]
     %obj.soulShield = true;
}

function SoulShieldMod::onUnApply(%data, %obj)
{
     if( !%obj.soulShield )									// +[soph]
          %obj.soulShieldDamaged = true ;							// +
     else											// +
     {												// +
          %obj.soulShieldDamaged = false ;							// +[/soph]
          %obj.soulShield = false;
     }
}

//----------------------------------------------------------------------------
datablock ItemData(SubspaceRegenMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function SubspaceRegenMod::onApply(%data, %obj)
{
     %obj.subspaceRegen = true;

     if(%obj.ssrThread)
     {
        cancel(%obj.ssrThread);
        %obj.ssrThread = "";
     }
     
     subspaceModTick(%obj.getDatablock(), %obj);
}

function SubspaceRegenMod::onUnApply(%data, %obj)
{
     %obj.subspaceRegen = false;

     if(%obj.ssrThread)
        cancel(%obj.ssrThread);
     
     %obj.ssrThread = "";
}

function subspaceModTick(%data, %obj)
{
     if(isObject(%obj) && %obj.isAlive && %obj.subspaceRegen)
     {
          if(!%obj.isCloaked())
               %obj.takeDamage( ( -0.0045 + %data.maxDamage * -0.003 ) * %obj.repairKitFactor);	// was -0.03 -soph

     
          %obj.ssrThread = schedule(1000, %obj, subspaceModTick, %data, %obj);		// was 5000 -soph
     }
}

//----------------------------------------------------------------------------
datablock ItemData(ThermalArmorPlating)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

//   image = "GravArmorPlatePackImage";
	pickUpName = "an armor mod";
};

function ThermalArmorPlating::onApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Plasma ] -= 1 / 2 ;		// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Energy ] -= 1 / 3 ;		// +
	%obj.dynamicResistance[ $DamageGroupMask::Kinetic ] += 1 / 4 ;		// +[/soph]
	%obj.burnproof = true;
}

function ThermalArmorPlating::onUnApply(%data, %obj)
{
	%obj.dynamicResistance[ $DamageGroupMask::Plasma ] += 1 / 2 ;		// +[soph]
	%obj.dynamicResistance[ $DamageGroupMask::Energy ] += 1 / 3 ;		// +
	%obj.dynamicResistance[ $DamageGroupMask::Kinetic ] -= 1 / 4 ;		// +[/soph]
	%obj.burnproof = true;
}

//----------------------------------------------------------------------------
datablock ItemData(BAOverdriveMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function BAOverdriveMod::onApply(%data, %obj)
{
     %obj.activeModule = $ActiveModule::BAOverdriveMod;
     %obj.activeModuleScript = "BAODDDLoop";
}

function BAOverdriveMod::onUnApply(%data, %obj)
{
     %obj.activeModule = false;
     %obj.activeModuleScript = "";
}

$ActiveModule::BAOverdriveMod = $ActiveModuleCount;
$ActiveModuleCount++;

function BAODDDLoop(%data, %obj)
{
     if(!%obj.dead && %obj.activeModule == $ActiveModule::BAOverdriveMod)
     {
          %client = %obj.client;
          %obj.baodActive = %obj.baodActive ? false : true;          
          
          if(%obj.baodActive)
          {
               bottomPrint(%client, "Overdrive enabled.", 5, 1);
               BAODTransform(%data, %obj, true);

               // Changing armor datablocks will temporarily kick us out of the D+D, so delay if if we are.
               if(%obj.inDEDField)
                    schedule(250, %obj, BAODLoopCheck, %obj);
               else
                    BAODLoopCheck(%obj);
          }
          else
          {
               BAODTransform(%data, %obj, false);          
               bottomPrint(%client, "Overdrive disabled.", 5, 1);
          }
     }
}

function BAODLoopCheck(%obj)
{
     if(%obj.dead)
          return;

     if(isObject(%obj) && %obj.activeModule == $ActiveModule::BAOverdriveMod)
     {
          if(%obj.isEMP)
          {
               BAODTransform(%data, %obj, false);
               bottomPrint(%client, "Overdrive function disabled due to EMP.", 5, 1);
          }

          if(%obj.baodActive)
          {
               if(!%obj.inDEDField)
               {
                    %obj.getDatablock().damageObject(%obj, %obj, %obj.position, 0.07, $DamageType::Suicide, %obj.getVelocity(), false);
                    %obj.setDamageFlash(0.4);
               }

               schedule(250, %obj, BAODLoopCheck, %obj);
          }
     }
}

function BAODTransform(%data, %obj, %dir)
{
     %client = %obj.client;

     %am = %dir ? "MASC" : "Armor";
          
     %dmg = %obj.getDamageLevel();
     %nrg = %obj.getEnergyLevel();
     %rate = %obj.getRechargeRate();
     %size = "BattleAngel";
     
     if(%client.race $= "Bioderm")
        %armor = %size @ "Male" @ %client.race @ %am;
     else
        %armor = %size @ %client.sex @ %client.race @ %am;

     if(%obj.DESetMember)
     {
          %obj.DESetSet.remove(%obj);
          %obj.inDEDField = false;
          %obj.DESetSet = "";
          %obj.DESetMember = "";
     }
     
     %obj.setDataBlock(%armor);
     %client.armor = %size;

     %obj.setDamageLevel(%dmg);
     %obj.setRechargeRate(%rate);
     %obj.setEnergyLevel(%nrg);
     %obj.zapObject();

     %obj.damageMod += %dir ? 0.2 : -0.2;	// += %dir ? 0.5 : -0.5; -soph
}

//----------------------------------------------------------------------------
datablock ItemData(ShieldHardenerMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function ShieldHardenerMod::onApply(%data, %obj)
{
     %obj.usingHardener = true;
}

function ShieldHardenerMod::onUnApply(%data, %obj)
{
     %obj.usingHardener = false;
}

//----------------------------------------------------------------------------
datablock ItemData(BASSMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function BASSMod::onApply(%data, %obj)
{
     %obj.baShieldSystem = true;
}

function BASSMod::onUnApply(%data, %obj)
{
     %obj.baShieldSystem = false;
}

//----------------------------------------------------------------------------
datablock ItemData(JetfireMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function JetfireMod::onApply(%data, %obj)
{
     %obj.activeModule = true;
     %obj.activeModuleScript = "toggleJetfire";
}

function JetfireMod::onUnApply(%data, %obj)
{
     %obj.activeModule = false;
     %obj.activeModuleScript = "";
}

function toggleJetfire(%data, %obj)
{
     jetfireEnable(%obj);
}

//----------------------------------------------------------------------------
//datablock ItemData(AmmoHarvesterMod)
//{
//   className = ArmorMod;
//   catagory = "ArmorMods";
//   shapeFile = "turret_muzzlepoint.dts";
//   elasticity = 0.2;
//   friction = 0.6;
//   pickupRadius = 2;
//   rotate = true;
//
//   pickUpName = "an armor mod";
//};

function AmmoHarvesterMod::onApply(%data, %obj)
{

}

function AmmoHarvesterMod::onUnApply(%data, %obj)
{

}

//----------------------------------------------------------------------------
datablock ItemData(FinalStandMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function FinalStandMod::onApply(%data, %obj)
{
     %obj.finalStandMode = true;
}

function FinalStandMod::onUnApply(%data, %obj)
{
     %obj.finalStandMode = false;
}

function BAFinalStandPulseUpdate(%obj)
{
     zapEffect(%obj, "BlueShift");
}

function BattleAngelFinalStand(%data, %obj)
{
     %client = %obj.client;
     
     if(%obj.overdriveMode)
          return;
     
     if(%obj.scriptKilled)
          return;
     
//     if(!%obj.inDEDField)				// -[soph]
//     {						// -
//         BattleAngelFSOff(%data, %obj);		// -
//         bottomPrint(%client, "DANGER! Battle Angel Core containment breach!", 5, 1);
//         return;					// -
//     }						// -[/soph]
     
     %obj.setDamageLevel(%data.maxDamage - 0.01);
    // %obj.setEnergyLevel(%data.maxEnergy);		// -soph
     %obj.setRechargeRate(4);
     
     %obj.overdriveMode = true;
     zapEffect(%obj, "BlueShift");
     %obj.play3d(RMSSBDeploySound);
     %obj.universalResistFactor = 0;
     %obj.notRepairable = true;
     %obj.ignoreRepairThis = true;

     if( !%obj.inDEDField )				// +[soph]
        %rand = getRandom( 4 , 7 ) ;			// +
     else						// +[/soph]
        %rand = getRandom( 8 , 12 ) ;			// %rand = getRandom(7, 10); -soph
     
//     bottomPrint(%client, "<color:FF3300>>>> DANGER! <<<<color:FFFFFF>\nBattle Angel Core containment breach, ETA" SPC %rand SPC "seconds!", %rand, 2);	// -soph
//     %obj.finalThread = schedule(%rand * 1000, %obj, BattleAngelFSOff, %data, %obj);	// -soph
     BAFinalStandPulseLoop( %data , %obj , %rand ) ;	// BAFinalStandPulseLoop(%data, %obj); -soph
}

function BattleAngelFSOff(%data, %obj)
{
     if( %obj.BAPackID == 5 )					// +[soph]
     {								// +
          %forwardVec = %obj.getForwardVector() ;		// +
          %objDir2D  = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0" ;
          TurboChargerBreach( %obj , %obj , %damageType , %objDir2D ) ;
     }								// +[/soph]

     %client = %obj.client;
//     bottomPrint(%client, "Core repair failed!", 5, 1);
     %obj.overdriveMode = false;
     %obj.BACritical = true;
     %obj.universalResistFactor = 1.0;     
     %obj.finalStandMode = false;
     %data.damageObject(%obj, %obj.fs_lastDamagedBy, "0 0 0", 10000, %obj.fs_lastDamagedType, %obj.getVelocity());
}

function BAFinalStandPulseLoop( %data , %obj , %time )			// BAFinalStandPulseLoop(%data, %obj) -soph
{
     if(%obj.getState() !$= "Dead" && %obj.overdriveMode)
     {
          zapEffect(%obj, "BlueShift");
//          schedule(250, %obj, BAFinalStandPulseLoop, %data, %obj);	// -soph
          %time -= 0.25 ;						// +[soph]
          if( %time > 0 )						// +
          {								// +
               if( mCeil( %time ) == %time )				// +
                    bottomPrint( %obj.client , "<color:FF3300>>>> DANGER! <<<<color:FFFFFF>\nBattle Angel Core containment breach, ETA" SPC %time SPC "seconds!", 1 , 2 ) ;
               schedule( 250 , %obj , BAFinalStandPulseLoop , %data , %obj , %time ) ;
          }								// +
          else								// +
          {								// +
               bottomPrint( %obj.client , "<color:FF3300>>>> DANGER! <<<\n!! BREACH IMMINENT !!" , 5 , 2 ) ;
               schedule( 250 , %obj , BattleAngelFSOff , %data , %obj ) ;
               BASelfDestruct( %obj ) ;					// +
          }								// +[/soph]
     }
}

//----------------------------------------------------------------------------
//datablock ItemData(ProjectileAccelMod)
//{
//   className = ArmorMod;
//   catagory = "ArmorMods";
//   shapeFile = "turret_muzzlepoint.dts";
//   elasticity = 0.2;
//   friction = 0.6;
//   pickupRadius = 2;
//   rotate = true;
//
//   pickUpName = "an armor mod";
//};

function ProjectileAccelMod::onApply(%data, %obj)
{
     %obj.projectileAccelerator = true;
}

function ProjectileAccelMod::onUnApply(%data, %obj)
{
     %obj.projectileAccelerator = false;
}

//----------------------------------------------------------------------------
datablock ItemData(ShieldPulseMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

datablock ShockwaveData( ShieldPulseShockwave )	// +[soph]
{
   width = 0 ;
   numSegments = 32 ;
   numVertSegments = 24 ;
   velocity = 250 ;
   acceleration = -1000 ;
   lifetimeMS = 300 ;
   height = 25.0 ;
   verticalCurve = 8 ;
   is2D = false ;

   texture[0] = "special/shockLightning03" ; // "special/shieldmap" ; // "special/shockwave4" ;
   texture[1] = "special/gradient" ;
   texWrap = 5.0 ;

   times[0] = 0.0 ;
   times[1] = 0.5 ;
   times[2] = 1.0 ;

   colors[0] = "0 0.75 1 0.5" ;
   colors[1] = "0.2 0.55 1 0.8" ;
   colors[2] = "0.4 0.35 1 0.3" ;

   mapToTerrain = false ;
   orientToNormal = false ;
   renderBottom = true ;
};

datablock ExplosionData( ShieldPulseExplosion )
{
   soundProfile   = MechFire2Sound ;

   shockwave = ShieldPulseShockwave ;

   emitter[0] = "" ;
   emitter[1] = "" ;

   shakeCamera = true ;
   camShakeFreq = "0.5 0.75 0.75" ;
   camShakeAmp = "5.0 5.0 5.0" ;
   camShakeDuration = 0.5 ;
   camShakeRadius = 60.0 ;
};

datablock LinearFlareProjectileData( ShieldPulseProjectile )
{
   projectileShapeName = "turret_muzzlePoint.dts" ;
   faceViewer          = false ;
   directDamage        = 0.0 ;
   hasDamageRadius     = true ;
   indirectDamage      = 0.0 ;
   damageRadius        = 0.0 ;
   radiusDamageType    = $DamageType::ShieldPulse ;
   kickBackStrength    = 4000 ;

   explosion           = ShieldPulseExplosion ;
   underwaterExplosion = ShieldPulseExplosion ;

   splash              = MissileSplash ;
   velInheritFactor    = 0 ;

   dryVelocity       = 150 ;
   wetVelocity       = 150 ;
   fizzleTimeMS      = 35 ;
   lifetimeMS        = 35 ;
   explodeOnDeath    = true ;
   reflectOnWaterImpactAngle = 90 ;
   explodeOnWaterImpact      = false ;
   deflectionOnWaterImpact   = 0.0 ;
   fizzleUnderwaterMS        = 35 ;

   activateDelayMS = -1 ;

   numFlares         = 0 ;
   flareColor        = "0 0 0" ;
   flareModTexture   = "flaremod" ;
   flareBaseTexture  = "flarebase" ;
};						// +[/soph]

function ShieldPulseMod::onApply(%data, %obj)
{
     %obj.activeModule = true;
     %obj.activeModuleScript = "ShieldPulseInit";
}

function ShieldPulseMod::onUnApply(%data, %obj)
{
     %obj.activeModule = false;
     %obj.activeModuleScript = "";
}

$ActiveModule::ShieldPulseMod = $ActiveModuleCount;
$ActiveModuleCount++;

function ShieldPulseInit(%data, %obj)
{
     %client = %obj.client;
     
     %time = getSimTime();
     
     if(%time > %obj.lastShieldPulseTime)
     {     
          if(%obj.isShielded)
          {
               if(%obj.getEnergyLevel() > 25)
               {
                    %obj.useEnergy(25);
                    %obj.play3D(MechFire2Sound);
                    engageShieldRepulsorPulse(%obj);
                    %obj.lastShieldPulseTime = %time + 4000;
                    %obj.setImageTrigger($BackpackSlot, false);
               }
               else     
               {
                    bottomPrint(%client, "Not enough energy to initiate shield repulsion.", 5, 1);
                    return;
               }
          }
          else     
          {
               bottomPrint(%client, "Requires an active Shield Pack to initiate shield repulsion.", 5, 1);
               return;
          }
     }
}

function engageShieldRepulsorPulse(%obj)
{
   if(isObject(%obj))
   {
      %p = new LinearFlareProjectile()						// +[soph]
      {
         dataBlock        = ShieldPulseProjectile ;
         initialDirection = "0 0 -1" ;
         initialPosition  = %obj.getworldBoxCenter() ;
         sourceObject     = %obj ;
         sourceSlot       = 0 ;
      };
      MissionCleanup.add( %p ) ;						// +[/soph]

      %reflectType = $DefaultReflectableProjectles;
      %scanDist = 35;
       
      %refpos = %obj.getWorldBoxCenter();
      InitContainerRadiusSearch(%refpos, %scanDist, $TypeMasks::ProjectileObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::CorpseObjectType  | $TypeMasks::VehicleObjectType ); // added vehicles +soph
     
      while((%int = ContainerSearchNext()) != 0)
      {
         if( %int.getType & $TypeMasks::ProjectileObjectType )			// +soph
         {
            if(isReflectableProjectile(%int, %reflectType))			// inverted +soph
            {
               if(%int.lastReflectedFrom == %obj)
                  continue;
          
               if(%int.sourceObject == %obj || %int.bkSourceObject == %obj || %int.magneticClamp)
                  continue;

               %coverage = calcExplosionCoverage(%refPos, %int, $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType ); // added vehicles +soph
             
               if(%coverage == 0)
                  continue;
           
               if(!isObject(%obj.reflectorObj))
                  %obj.reflectorObj = createReflectorForObject(%obj);
                    
               %FFRObject = %obj.reflectorObj;
        
               %FFRObject.setPosition(%int.getPosition());

               if(!%int.noreflect && !%int.exploded )				// (!%int.noreflect) -soph
               {
                  if(%int.getClassName() $= "BombProjectile")
                     %newVec = vectorScale(calcSpreadVector(vectorNeg(%int.initialDirection), 6), 350);
                  else
                     %newVec = calcSpreadVector(vectorNeg(%int.initialDirection), 6);
        
//                  if(!isObject(%int.sourceObject))
//                     %src = %int.bkSourceObject;
//                  else
//                      %src = %int.sourceObject;
        

                  if( %int.originTime )						// +[soph]
                  {								// +
                     %airTimeMod = getSimTime() - %int.originTime ; 		// +
                     %data = %int.getDatablock().getName() ;			// +
                     if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
                        %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
                     else							// +
                        %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
		     %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;	// +
                  }								// +[/soph]

                  %p = new(%int.getClassName())()
                  {
                     dataBlock         = %int.getDatablock().getName();
                     initialDirection  = %newVec;
                     initialPosition   = %int.getPosition();
                     sourceObject      = %FFRObject;
                     sourceSlot        = %int.sourceSlot;
                     lastReflectedFrom = %obj;
//                     bkSourceObject    = %src;
                     bkSourceObject    = %obj;
                     starburstMode     = %int.starburstMode;
                     damageMod         = %int.damageMod;
                     vehicleMod        = %int.vehicleMod;
                  };
                  MissionCleanup.add(%p);
     
//                  %p.sourceObject = %src;
                  %p.sourceObject = %obj;
       
                  %p.originTime = %int.originTime ;				// +soph

                  if( %src.lastMortar == %int ) 				// if(%src.lastMortar !$= "") -soph
                     %src.lastMortar = %p;

                  if(%int.trailThread)
                     projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);
     
                  if(%int.proxyThread)
                     rBeginProxy(%p);
     
                  if(%int.lanceThread)
                  {        
                      SBLanceRandomThread(%p);
                      %p.lanceThread = true;
                  }
     
                  createLifeEmitter(%newPos, "PCRSparkEmitter", 750);
     
                  %int.delete();
     //             %FFRObject.schedule(32, delete);
               }
            }
         }
         else 
         {
//            if(!%int.isMech && %int.client.team != %obj.client.team)		// -soph
            if( %int != %obj )							// +soph
            {
               %coverage = calcExplosionCoverage(%refPos, %int, $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType );	// added vehicles +soph

               if(%coverage == 0)
                  continue;
                   
               if( %int.getType() & $TypeMasks::PlayerObjectType )		// +soph
               {
                  if( !%int.blowedup )						// +soph
                     zapEffect(%int, "PoisonBolt");
               }
               else
                  zapVehicle( %int , "PoisonBolt" );				// +soph
                      
               %mass = %obj.getDatablock().mass * 2.5;
               %point = %int.getWorldBoxCenter();
               %ref = %obj.getWorldBoxCenter();
               %vec = getVectorFromPoints(%point, %ref);
//               %distPct = 1 - ( vectorDist(%point, %ref) / %scanDist );	// -soph
               %distPct = 1 - ( vectorDist(%point, %ref) / %scanDist );								// = 1 + vectorDist(%point, %ref) / %scanDist; -soph
               %force = vectorScale(%vec, -1 * %mass * %distPct);
               if( !%int.isMech )						// +soph
                  %int.applyImpulse(%point, %force);
               %int.getDataBlock().damageObject(%int, %obj, %point, ( 0.5 * %distPct ), $DamageType::ShieldPulse);	// %int.getDataBlock().damageObject(%int, %obj, %point, 0.5, $DamageType::ShieldPulse); -soph
            }
         }
      }
   }
}

// Fix console spam
function Precipitation::applyImpulse()
{
     return false;
}

//----------------------------------------------------------------------------
datablock ItemData(EfficiencyMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function EfficiencyMod::onApply(%data, %obj)
{
     %obj.efficiencyMod = true;
     %obj.powerRecirculator = true; 
}

function EfficiencyMod::onUnApply(%data, %obj)
{
     %obj.efficiencyMod = false;
     %obj.powerRecirculator = false;     
}

//----------------------------------------------------------------------------
datablock ItemData(PowerMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function PowerMod::onApply(%data, %obj)
{
     %obj.powerMod = true;
     %obj.powerModActive = false;     
}

function PowerMod::onUnApply(%data, %obj)
{
     %obj.powerMod = false;
     %obj.powerModActive = false;
}

//----------------------------------------------------------------------------
datablock ItemData(GuardianMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function GuardianMod::onApply(%data, %obj)
{
     %obj.guardianMod = true;
     %obj.guardianModActive = false;     
}

function GuardianMod::onUnApply(%data, %obj)
{
     %obj.guardianMod = false;
     %obj.guardianModActive = false;
}

//----------------------------------------------------------------------------
datablock ItemData(ArchMageMod)
{
   className = ArmorMod;
   catagory = "ArmorMods";
   shapeFile = "turret_muzzlepoint.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;

   pickUpName = "an armor mod";
};

function ArchMageMod::onApply(%data, %obj)
{
     %obj.archMageMod = true;
}

function ArchMageMod::onUnApply(%data, %obj)
{
     %obj.archMageMod = false;
}

//----------------------------------------------------------------------------