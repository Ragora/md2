//---------------------------------------------------------------------------
// AI Name Generator
// Takes strings below to piece together a name

$RandomBotGenName[0] = "Demon";
$RandomBotGenName[1] = "Zabutom";
$RandomBotGenName[2] = "Azarok";
$RandomBotGenName[3] = "Murkhofud";
$RandomBotGenName[4] = "Zhek";
$RandomBotGenName[5] = "Yellowfang";
$RandomBotGenName[6] = "Hrreshig";
$RandomBotGenName[7] = "Marakh";
$RandomBotGenName[8] = "Kolkhris";
$RandomBotGenName[9] = "Ribshatter";
$RandomBotGenName[10] = "Chol";
$RandomBotGenName[11] = "Gerekh";
$RandomBotGenName[12] = "Suntalon";
$RandomBotGenName[13] = "Omakhros";
$RandomBotGenName[14] = "Arhakhral";
$RandomBotGenName[15] = "Lunatic";
$RandomBotGenName[16] = "Stoneskin";
$RandomBotGenName[17] = "Zhor";
$RandomBotGenName[18] = "Khan";
$RandomBotGenName[19] = "Morax";
$RandomBotGenName[20] = "Mrazhurr";
$RandomBotGenName[21] = "Skullcrusher";
$RandomBotGenName[22] = "Boneboiler";
$RandomBotGenName[23] = "Murder Beast";
$RandomBotGenName[24] = "Zura";
$RandomBotGenName[25] = "Durakh Zon";
$RandomBotGenName[26] = "Mormoghast";
$RandomBotGenName[27] = "Devilclaw";
$RandomBotGenName[28] = "Scarred-by-Plasma";
$RandomBotGenName[29] = "Ku Annorkh";
$RandomBotGenName[30] = "Tribekiller";
$RandomBotGenName[31] = "Bluefang";
$RandomBotGenName[32] = "Dreadbite";
$RandomBotGenName[33] = "Skarjett";
$RandomBotGenName[34] = "Irokhirr";
$RandomBotGenName[35] = "Mercury";
$RandomBotGenName[36] = "Liverthief";
$RandomBotGenName[37] = "Tumoz Agarh";
$RandomBotGenName[38] = "Malevolox";
$RandomBotGenName[39] = "Monkeykiller";
$RandomBotGenName[40] = "Mordhul Nom";
$RandomBotGenName[41] = "Octogonapus";
$RandomBotGenName[42] = "Poring";
$RandomBotGenName[43] = "Alex";
$RandomBotGenName[44] = "Terminator";
$RandomBotGenName[45] = "Tuborg";
$RandomBotGenName[46] = "wragmuffin";
$RandomBotGenName[47] = "Nite";
$RandomBotGenName[48] = "Death Strike";
$RandomBotGenName[49] = "The Machine";
$RandomBotGenName[50] = "Weebl";
$RandomBotGenName[51] = "Candlejack";
$RandomBotGenName[52] = "Lemming";
$RandomBotGenName[53] = "Nebulon";
$RandomBotGenName[53] = "Trogdor";
$RandomBotGenName[54] = "Fishbait";
$RandomBotGenName[55] = "Skulker";
$RandomBotGenName[56] = "Dogstar";
$RandomBotGenName[57] = "Bonehead";
$RandomBotGenName[58] = "Torus";
$RandomBotGenName[59] = "Glitter";
$RandomBotGenName[60] = "Ironbreath";
$RandomBotGenName[61] = "Jetsam";
$RandomBotGenName[62] = "Retch";
$RandomBotGenName[63] = "Hotfoot";
$RandomBotGenName[64] = "Zigzag";
$RandomBotGenName[65] = "Troglodyte";
$RandomBotGenName[66] = "Simrionic";
$RandomBotGenName[67] = "Cathode Kiss";
$RandomBotGenName[68] = "Demonshriek";
$RandomBotGenName[69] = "Terrapin";
$RandomBotGenName[70] = "Perilous";
$RandomBotGenName[71] = "Nova-9";
$RandomBotGenName[72] = "Hotspur";
$RandomBotGenName[73] = "DustWitch";
$RandomBotGenName[74] = "Mohican";
$RandomBotGenName[75] = "Punch";
$RandomBotGenName[76] = "Mameluke";
$RandomBotGenName[77] = "Sorrow";
$RandomBotGenName[78] = "Neon Blossom";
$RandomBotGenName[79] = "Shiver";
$RandomBotGenName[80] = "GLaDOS";
$RandomBotGenName[81] = "DuKat";
$RandomBotGenName[82] = "Inu";
$RandomBotGenName[83] = "Elwood";
$RandomBotGenName[84] = "GLaDOS";
$RandomBotGenName[85] = "Chopper D";
$RandomBotGenName[86] = "Negromancer";
$RandomBotGenName[87] = "Cow";
$RandomBotGenName[88] = "Chicken";
$RandomBotGenName[89] = "Shazbot";
$RandomBotGenName[90] = "Solid";
$RandomBotGenName[91] = "Deepy";
$RandomBotGenName[92] = "Red Serpent";
$RandomBotGenName[93] = "Primus";
$RandomBotGenName[94] = "Jazz";
$RandomBotGenName[95] = "Darkness";
$RandomBotGenName[96] = "Kool-Aid";
$RandomBotGenName[97] = "Datachild";
$RandomBotGenName[98] = "Drakkar";
$RandomBotGenName[99] = "Atlas";
$RandomBotGenName[100] = "Monkey";
$RandomBotGenName[101] = "Ningyo";
$RandomBotGenName[102] = "Smiley";
$RandomBotGenName[103] = "Duster";
$RandomBotGenName[104] = "Sam";
$RandomBotGenName[105] = "Altec";
$RandomBotGenName[106] = "Phong";
$RandomBotGenName[107] = "Slash";
$RandomBotGenName[108] = "Zen";
$RandomBotGenName[109] = "Neko";
$RandomBotGenName[110] = "Hydrox";
$RandomBotGenName[111] = "Zalza";
$RandomBotGenName[112] = "Doopy";
$RandomBotGenName[113] = "Frolek";
$RandomBotGenName[114] = "Knuckles";
$RandomBotGenName[115] = "Goey Tord";
$RandomBotGenName[116] = "Tank";
$RandomBotGenName[117] = "Joliet";
$RandomBotGenName[118] = "Kugghjul";
$RandomBotGenName[119] = "DelshiftB";
$RandomBotGenName[120] = "Slayer X";
$RandomBotGenName[121] = "Moon Moon";
$RandomBotGenName[122] = "DelshiftB";
$RandomBotGenName[123] = "Farmer Joe";
$RandomBotGenName[124] = "Honest Bob";
$RandomBotGenName[125] = "Gouraud";
$RandomBotGenName[126] = "Blinn";
$RandomBotGenName[127] = "Wheatbrain";
$RandomBotGenName[128] = "Torrance";
$RandomBotGenName[129] = "Blueshell";
$RandomBotGenName[130] = "Torrance";
$RandomBotGenName[131] = "Hodor";
$RandomBotGenName[132] = "Heavy Arms";
$RandomBotGenName[133] = "Starslug";
$RandomBotGenName[134] = "Bastian";
$RandomBotGenName[135] = "Saline";
$RandomBotGenName[136] = "Kuro Neko";
$RandomBotGenName[137] = "Variel";
$RandomBotGenName[138] = "Jammalade";
$RandomBotGenName[139] = "Revonel";
$RandomBotGenName[140] = "Bastian";
$RandomBotGenName[141] = "Xerxes";
$RandomBotGenName[142] = "Spline";
$RandomBotGenName[143] = "Bastian";
$RandomBotGenName[144] = "Kronos";
$RandomBotGenName[145] = "Remington";
$RandomBotGenName[146] = "Smergk";
$RandomBotGenName[147] = "Raptor Pouch";
$RandomBotGenName[148] = "Banana";
$RandomBotGenName[149] = "Skippy";
$RandomBotGenName[150] = "Mutton";
$RandomBotGenName[151] = "Derpy";
$RandomBotGenName[152] = "Muffin";
$RandomBotGenName[153] = "Hiss";
$RandomBotGenName[154] = "Ass";
$RandomBotGenName[155] = "Snowflake";
$RandomBotGenName[156] = "Cuddlebug";
$RandomBotGenName[157] = "Bandwagon";
$RandomBotGenName[158] = "Moldybutt";
$RandomBotGenName[159] = "Bear";
$RandomBotGenName[160] = "Sausage";
$RandomBotGenName[161] = "Sniff";
$RandomBotGenName[162] = "Mayonnaise";
$RandomBotGenName[163] = "Bloodsucker";
$RandomBotGenName[164] = "Fluffy";

$RandomBotGenNameCount = 164;

$RandomBotGenPrefix[0] = "Dr.";
$RandomBotGenPrefix[1] = "Private";
$RandomBotGenPrefix[2] = "Lt.";
$RandomBotGenPrefix[3] = "Major";
$RandomBotGenPrefix[4] = "General";
$RandomBotGenPrefix[5] = "Flaymaster";
$RandomBotGenPrefix[6] = "Red";
$RandomBotGenPrefix[7] = "Blood Drinker";
$RandomBotGenPrefix[8] = "Gul";
$RandomBotGenPrefix[9] = "Commodore";
$RandomBotGenPrefix[10] = "Terrible";
$RandomBotGenPrefix[11] = "Horrible";
$RandomBotGenPrefix[12] = "Overlord";
$RandomBotGenPrefix[13] = "Guardian";
$RandomBotGenPrefix[14] = "Stonefist";
$RandomBotGenPrefix[15] = "Maverick";
$RandomBotGenPrefix[16] = "Chainbreaker";
$RandomBotGenPrefix[17] = "Plagued";
$RandomBotGenPrefix[18] = "Obsidian";
$RandomBotGenPrefix[19] = "Chief";
$RandomBotGenPrefix[20] = "Officer";
$RandomBotGenPrefix[21] = "Mayor";
$RandomBotGenPrefix[22] = "Violent";
$RandomBotGenPrefix[23] = "Deep One";
$RandomBotGenPrefix[24] = "Destroyer";
$RandomBotGenPrefix[25] = "Wrecker";
$RandomBotGenPrefix[26] = "Slayer";
$RandomBotGenPrefix[27] = "Sgt.";
$RandomBotGenPrefix[28] = "Admiral";
$RandomBotGenPrefix[29] = "Seeker";
$RandomBotGenPrefix[30] = "Slicer";
$RandomBotGenPrefix[31] = "Vanguard";
$RandomBotGenPrefix[32] = "Heretic";
$RandomBotGenPrefix[33] = "Velomancer";
$RandomBotGenPrefix[34] = "Widowmaker";
$RandomBotGenPrefix[35] = "Devourer";
$RandomBotGenPrefix[36] = "Backbreaker";
$RandomBotGenPrefix[37] = "Manslayer";
$RandomBotGenPrefix[38] = "Rockthrower";
$RandomBotGenPrefix[39] = "Anklebiter";
$RandomBotGenPrefix[39] = "Technician";
$RandomBotGenPrefix[40] = "Oracle";
$RandomBotGenPrefix[41] = "Master";
$RandomBotGenPrefix[42] = "Defender";
$RandomBotGenPrefix[43] = "Symmetriad";
$RandomBotGenPrefix[44] = "Brain Slapper";
$RandomBotGenPrefix[45] = "Terrorist";
$RandomBotGenPrefix[46] = "Elemental";
$RandomBotGenPrefix[47] = "Corporal";
$RandomBotGenPrefix[48] = "Cumulous";
$RandomBotGenPrefix[49] = "Dryback";
$RandomBotGenPrefix[49] = "Fleshconverter";
$RandomBotGenPrefix[49] = "Guile";
$RandomBotGenPrefix[49] = "Sigma";
$RandomBotGenPrefix[49] = "Delta";
$RandomBotGenPrefix[49] = "Lambda";
$RandomBotGenPrefix[49] = "Signor";
$RandomBotGenPrefix[49] = "Astral";
$RandomBotGenPrefix[50] = "Fopkjewa";
$RandomBotGenPrefix[51] = "Timeblaster";
$RandomBotGenPrefix[52] = "Hodor";
$RandomBotGenPrefix[53] = "Guru";
$RandomBotGenPrefix[54] = "Fat";
$RandomBotGenPrefix[55] = "Slim";
$RandomBotGenPrefix[56] = "Shady";
$RandomBotGenPrefix[57] = "Noxious";
$RandomBotGenPrefix[58] = "Bot";
$RandomBotGenPrefix[59] = "Mecha";
$RandomBotGenPrefix[60] = "Gundam";
$RandomBotGenPrefix[61] = "Spitfire";
$RandomBotGenPrefix[62] = "Villager";
$RandomBotGenPrefix[63] = "Full Contact";
$RandomBotGenPrefix[64] = "Spitfire";
$RandomBotGenPrefix[65] = "Synthesizer";
$RandomBotGenPrefix[66] = "Maximizer";
$RandomBotGenPrefix[67] = "Tracer";
$RandomBotGenPrefix[68] = "Spitfire";
$RandomBotGenPrefix[69] = "Harmless";
$RandomBotGenPrefix[70] = "Useless";
$RandomBotGenPrefix[71] = "Malfunctioning";
$RandomBotGenPrefix[72] = "Temporal";
$RandomBotGenPrefix[73] = "Little";
$RandomBotGenPrefix[74] = "Big";
$RandomBotGenPrefix[75] = "Funky";
$RandomBotGenPrefix[76] = "Deferred";
$RandomBotGenPrefix[77] = "Dread Pirate";
$RandomBotGenPrefix[78] = "Mathemagician";
$RandomBotGenPrefix[79] = "Tacomancer";
$RandomBotGenPrefix[80] = "Violator";
$RandomBotGenPrefix[81] = "Sigma";
$RandomBotGenPrefix[82] = "Cannibal";
$RandomBotGenPrefix[83] = "Lord";
$RandomBotGenPrefix[84] = "Captain";
$RandomBotGenPrefix[85] = "I am";
$RandomBotGenPrefix[86] = "Gyrating";
$RandomBotGenPrefix[87] = "Crotchhunter";
$RandomBotGenPrefix[88] = "Crosseyed";
$RandomBotGenPrefix[89] = "Rabid";
$RandomBotGenPrefix[90] = "Boneraker";
$RandomBotGenPrefix[91] = "Disco";
$RandomBotGenPrefix[92] = "Addict";
$RandomBotGenPrefix[93] = "Beefy";
$RandomBotGenPrefix[94] = "Cuddly";
$RandomBotGenPrefix[95] = "Generic";
$RandomBotGenPrefix[96] = "Rampant";
$RandomBotGenPrefix[97] = "Princess";
$RandomBotGenPrefix[98] = "Friendly";
$RandomBotGenPrefix[99] = "Hot";
$RandomBotGenPrefix[100] = "Smashed";
$RandomBotGenPrefix[101] = "Bananabomber";
$RandomBotGenPrefix[102] = "Evil";
$RandomBotGenPrefix[103] = "Flaming";
$RandomBotGenPrefix[104] = "Suicidal";
$RandomBotGenPrefix[105] = "Silly";
$RandomBotGenPrefix[106] = "Bloodsucking";
$RandomBotGenPrefix[107] = "Bearded";
$RandomBotGenPrefix[108] = "Fabulous";
$RandomBotGenPrefix[109] = "Submersible";
$RandomBotGenPrefix[110] = "Can of";

$RandomBotGenPrefixCount = 110;

$RandomBotGenSuffix[0] = "the Strong";
$RandomBotGenSuffix[1] = "the Invincible";
$RandomBotGenSuffix[2] = "of Steel";
$RandomBotGenSuffix[3] = "of Stone";
$RandomBotGenSuffix[4] = "of Fire";
$RandomBotGenSuffix[5] = "of the Bear";
$RandomBotGenSuffix[6] = "of the Tiger";
$RandomBotGenSuffix[7] = "the Broken";
$RandomBotGenSuffix[8] = "Jenkins";
$RandomBotGenSuffix[9] = "the Outcast";
$RandomBotGenSuffix[10] = "the Stinger";
$RandomBotGenSuffix[11] = "the Proctologist";
$RandomBotGenSuffix[12] = "the Cannon";
$RandomBotGenSuffix[13] = "the Alpha";
$RandomBotGenSuffix[14] = "the Maverick";
$RandomBotGenSuffix[15] = "of Potatoes";
$RandomBotGenSuffix[16] = "of Bombs";
$RandomBotGenSuffix[17] = "of Knives";
$RandomBotGenSuffix[18] = "of Blades";
$RandomBotGenSuffix[19] = "the Rotten";
$RandomBotGenSuffix[20] = "the Outlaw";
$RandomBotGenSuffix[21] = "the Stryker";
$RandomBotGenSuffix[22] = "the Flayer";
$RandomBotGenSuffix[23] = "Cosby";
$RandomBotGenSuffix[24] = "Bot";
$RandomBotGenSuffix[25] = "the Raven";
$RandomBotGenSuffix[26] = "the Mighty";
$RandomBotGenSuffix[27] = "the Brave";
$RandomBotGenSuffix[28] = "the Coward";
$RandomBotGenSuffix[29] = "the Mind-Taker";
$RandomBotGenSuffix[30] = "the Corruptor";
$RandomBotGenSuffix[31] = "the Hallowed";
$RandomBotGenSuffix[32] = "the Risen";
$RandomBotGenSuffix[33] = "of Champions";
$RandomBotGenSuffix[34] = "the Legend";
$RandomBotGenSuffix[35] = "Payne";
$RandomBotGenSuffix[36] = "the Wise";
$RandomBotGenSuffix[37] = "of Ice";
$RandomBotGenSuffix[38] = "the Honored";
$RandomBotGenSuffix[39] = "of Glory";
$RandomBotGenSuffix[40] = "the Dark";
$RandomBotGenSuffix[41] = "the Burninator";
$RandomBotGenSuffix[42] = "the Golden";
$RandomBotGenSuffix[43] = "Deathwind";
$RandomBotGenSuffix[44] = "Evenkill";
$RandomBotGenSuffix[45] = "Oddkill";
$RandomBotGenSuffix[46] = "the Omega";
$RandomBotGenSuffix[47] = "the Insane";
$RandomBotGenSuffix[48] = "the Juicer";
$RandomBotGenSuffix[49] = "Griffin";
$RandomBotGenSuffix[50] = "the Boss";
$RandomBotGenSuffix[51] = "Rogers";
$RandomBotGenSuffix[52] = "Seuss";
$RandomBotGenSuffix[53] = "Bond";
$RandomBotGenSuffix[54] = "Skywalker";
$RandomBotGenSuffix[55] = "Einstien";
$RandomBotGenSuffix[56] = "McDonald";
$RandomBotGenSuffix[57] = "O'Brien";
$RandomBotGenSuffix[58] = "Hawking";
$RandomBotGenSuffix[59] = "the Toothfairy";
$RandomBotGenSuffix[60] = "Clinton";
$RandomBotGenSuffix[61] = "the Emotracker";
$RandomBotGenSuffix[62] = "the Spy";
$RandomBotGenSuffix[63] = "the Mechanic";
$RandomBotGenSuffix[64] = "the Echidna";
$RandomBotGenSuffix[65] = "the Karate Muffin";
$RandomBotGenSuffix[66] = "of the Forge";
$RandomBotGenSuffix[67] = "the Clome";
$RandomBotGenSuffix[68] = "the Lintflinger";
$RandomBotGenSuffix[69] = "of Gatorade";
$RandomBotGenSuffix[70] = "the Clome";
$RandomBotGenSuffix[71] = "of the Scourge";
$RandomBotGenSuffix[72] = "Porgys";
$RandomBotGenSuffix[73] = "the Freshmaker";
$RandomBotGenSuffix[74] = "Astral";
$RandomBotGenSuffix[75] = "Lambert";
$RandomBotGenSuffix[76] = "of Salads";
$RandomBotGenSuffix[77] = "Lambert";
$RandomBotGenSuffix[78] = "Hodor";
$RandomBotGenSuffix[79] = "the Interpolator";
$RandomBotGenSuffix[80] = "of Sublimation";
$RandomBotGenSuffix[81] = "the Cheese Pie";
$RandomBotGenSuffix[82] = "Grinder";
$RandomBotGenSuffix[83] = "of Teapots";
$RandomBotGenSuffix[84] = "Grinder";
$RandomBotGenSuffix[85] = "the Uncomfortable";
$RandomBotGenSuffix[86] = "the Curious";
$RandomBotGenSuffix[87] = "of Vectors";
$RandomBotGenSuffix[88] = "of the Lag";
$RandomBotGenSuffix[89] = "the Shazbot";
$RandomBotGenSuffix[90] = "Tron";
$RandomBotGenSuffix[91] = "Prime";
$RandomBotGenSuffix[92] = "Pie";
$RandomBotGenSuffix[93] = "of the Paw";
$RandomBotGenSuffix[94] = "of the Bag";
$RandomBotGenSuffix[95] = "the Incompetent";
$RandomBotGenSuffix[96] = "the Ass";
$RandomBotGenSuffix[97] = "the Annoying";
$RandomBotGenSuffix[98] = "the Stache";
$RandomBotGenSuffix[99] = "the Lab Rat";
$RandomBotGenSuffix[100] = "Spleen";
$RandomBotGenSuffix[101] = "Roadkill";
$RandomBotGenSuffix[102] = "the Gondolier";
$RandomBotGenSuffix[103] = "on the Moon";
$RandomBotGenSuffix[104] = "the Cuddler";
$RandomBotGenSuffix[105] = "Butt";
$RandomBotGenSuffix[106] = "the Constipated";
$RandomBotGenSuffix[107] = "the Licker";
$RandomBotGenSuffix[108] = "of Mayonnaise";
$RandomBotGenSuffix[109] = "on Fire";
$RandomBotGenSuffix[110] = "the Bearded";
$RandomBotGenSuffix[111] = "the Terrible";
$RandomBotGenSuffix[112] = "the Committed";

$RandomBotGenSuffixCount = 112;

// Generate name
function AIGenerateRandomName()
{
     %prefix = getRandom(1);
     %suffix = getRandom(1);
     %name = "";
     %tries = 0;
          
     if(%prefix)
          %name = $RandomBotGenPrefix[getRandom($RandomBotGenPrefixCount)];

     %rnd = getRandom($RandomBotGenNameCount);
     
     // Make sure we don't have any duplicates
     if($RandomBotNameAllocated[%rnd])
     {
          // Choose another unused bot name until we've found a free one...
          // If this fails (runs out of tries) it will be inevitable and use the
          // last generated one
          while($RandomBotNameAllocated[%rnd] && (%tries < $RandomBotGenNameCount))
          {
               %rnd = getRandom($RandomBotGenNameCount);
               %tries++;
          }

          // If we do overflow, just start over          
          if(%tries == $RandomBotGenNameCount)
               AIClearUsedNamePool();
     }

     // Mark this name as taken
     $RandomBotNameAllocated[%rnd] = true;
          
     %mainName = $RandomBotGenName[%rnd];
     
     // Sanity check - somehow name is blank, this solves the problem although 
     // potentially counter-productive...
     if(%mainName $= "")
          %mainName = $RandomBotGenName[getRandom($RandomBotGenNameCount)];
     
     %name = %name $= "" ? %mainName : %name SPC %mainName;

     if(%suffix)
          %name = %name SPC $RandomBotGenSuffix[getRandom($RandomBotGenSuffixCount)];
     
     // DarkDragonDX: Hopefully fix weird prepended spaces in bot names
     return trim(%name);
}

function AIClearUsedNamePool()
{
     %count = 0;
     
     while(%count < $RandomBotGenNameCount)
     {
          $RandomBotNameAllocated[%count] = false;
          %count++;
     }
}
