if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//============================================= >Modification Zone<

$TurretIndoorSpaceRadius  = 15;  // deployed turrets must be this many meters apart // z0dd - ZOD, 8/14/02. Was 20
$InventorySpaceRadius  = 10;  // deployed inventory must be this many meters apart // z0dd - ZOD, 8/14/02. Was 20
$TurretIndoorSphereRadius = 50;  // radius for turret frequency check
$TurretIndoorMaxPerSphere = 5;   // # of turrets allowed in above radius // z0dd - ZOD, 8/14/02. Was 4

$TurretOutdoorSpaceRadius  = 15;  // deployed turrets must be this many meters apart // z0dd - ZOD, 8/14/02. Was 25
$TurretOutdoorSphereRadius = 60;  // radius for turret frequency check
$TurretOutdoorMaxPerSphere = 5;   // # of turrets allowed in above radius // z0dd - ZOD, 8/14/02. Was 4

$TeamDeployableMax[InventoryDeployable]      = 5;
$TeamDeployableMax[TurretIndoorDeployable]   = 15;
$TeamDeployableMax[TurretOutdoorDeployable]  = 15;
$TeamDeployableMax[PulseSensorDeployable]    = 15;
$TeamDeployableMax[MotionSensorDeployable]   = 15;

$TeamDeployableMin[TurretIndoorDeployable] = 10;
$TeamDeployableMin[TurretOutdoorDeployable] = 10;

$NotDeployableReason::OutOfBounds = 12;
$NotDeployableReason::EDCFailed = 13;
$NotDeployableReason::TurretTimeout = 14;

// Deploy Functions

datablock SensorData(DeployMotionSensorObj)
{
   detects = true;
   detectsUsingLOS = true;
   detectsActiveJammed = true;
   detectsPassiveJammed = true;
   detectsCloaked = true;
   detectionPings = false;
   detectMinVelocity = 2;
   detectRadius = 100;
};

function Deployables::searchView(%obj, %searchRange, %mask)
{
   // get the eye vector and eye transform of the player
   %eyeVec   = %obj.getEyeVector();
   %eyeTrans = %obj.getEyeTransform();

   // extract the position of the player's camera from the eye transform (first 3 words)
   %eyePos = posFromTransform(%eyeTrans);

   // normalize the eye vector
   %nEyeVec = VectorNormalize(%eyeVec);

   // scale (lengthen) the normalized eye vector according to the search range
   %scEyeVec = VectorScale(%nEyeVec, %searchRange);

   // add the scaled & normalized eye vector to the position of the camera
   %eyeEnd = VectorAdd(%eyePos, %scEyeVec);

   // see if anything gets hit
   %searchResult = containerRayCast(%eyePos, %eyeEnd, %mask, 0);

   return %searchResult;
}

//-----------------------//
// Deployable Procedures //
//-----------------------//

//-------------------------------------------------
function ShapeBaseImageData::testMaxDeployed(%item, %plyr)
{
   if(%item.item $= TurretOutdoorDeployable || %item.item $= TurretIndoorDeployable)
      %itemCount = countTurretsAllowed(%item.item);
   else
      %itemCount = $TeamDeployableMax[%item.item];

   return $TeamDeployedCount[%plyr.team, %item.item] >= %itemCount;
}

//-------------------------------------------------
function ShapeBaseImageData::testNoSurfaceInRange(%item, %plyr)
{
   return ! Deployables::searchView(%plyr, $MaxDeployDistance, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType);
}

//-------------------------------------------------
function ShapeBaseImageData::testSlopeTooGreat(%item)
{
   if (%item.surface)
   {
      return getTerrainAngle(%item.surfaceNrm) > %item.maxDepSlope;
   }
}

//-------------------------------------------------
function ShapeBaseImageData::testSelfTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, $MinDeployDistance, $TypeMasks::PlayerObjectType);

   return containerSearchNext() == %plyr;
}

//-------------------------------------------------  got this one down @LN #405 (ST)
//function ShapeBaseImageData::testObjectTooClose(%item)
//{
//   %mask =    ($TypeMasks::VehicleObjectType     | $TypeMasks::MoveableObjectType   |
//               $TypeMasks::StaticShapeObjectType |
//               $TypeMasks::ForceFieldObjectType  | $TypeMasks::ItemObjectType       |
//               $TypeMasks::PlayerObjectType      | $TypeMasks::TurretObjectType);
//
//   InitContainerRadiusSearch( %item.surfacePt, $MinDeployDistance, %mask );
//
//   %test = containerSearchNext();
//   return %test;
//}


//-------------------------------------------------
function TurretOutdoorDeployableImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function ShapeBaseImageData::testNoTerrainFound(%item, %surface)
{
   //don't check this for non-Landspike turret deployables
}

function ShapeBaseImageData::testNoTerrainFound(%item, %surface)
{
   //don't check this for non-Landspike turret deployables
}

//-------------------------------------------------
function TurretIndoorDeployableImage::testNoInteriorFound(%item)
{
   return %item.surface.getClassName() !$= InteriorInstance;
}

function ShapeBaseImageData::testNoInteriorFound(%item, %surface)
{
   //don't check this for non-Clasping turret deployables
}

//-------------------------------------------------
function TurretIndoorDeployableImage::testHavePurchase(%item, %xform)
{
   %footprintRadius = 0.34;
   %collMask = $TypeMasks::InteriorObjectType;
   return %item.deployed.checkDeployPurchase(%xform, %footprintRadius, %collMask);
}

function ShapeBaseImageData::testHavePurchase(%item, %xform)
{
   //don't check this for non-Clasping turret deployables
   return true;
}

//-------------------------------------------------
function ShapeBaseImageData::testInventoryTooClose(%item, %plyr)
{
   return false;
}

function InventoryDeployableImage::testInventoryTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, $InventorySpaceRadius, $TypeMasks::StaticShapeObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if( (%foundName $= DeployedStationInventory) )
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

function TurretIndoorDeployableImage::testTurretTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, $TurretIndoorSpaceRadius, $TypeMasks::StaticShapeObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundname $= TurretDeployedFloorIndoor) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) )
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

function TurretOutdoorDeployableImage::testTurretTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, $TurretOutdoorSpaceRadius, $TypeMasks::StaticShapeObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundname $= TurretDeployedFloorIndoor) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) )
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

function ShapeBaseImageData::testTurretTooClose(%item, %plyr)
{
   //don't check this for non-turret deployables
}

//-------------------------------------------------
function TurretIndoorDeployableImage::testTurretSaturation(%item)
{
   %highestDensity = 0;
   InitContainerRadiusSearch(%item.surfacePt, $TurretIndoorSphereRadius, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
   while(%found)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundname $= TurretDeployedFloorIndoor) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) )
      {
           //found one
           %numTurretsNearby++;

       %nearbyDensity = testNearbyDensity(%found, $TurretIndoorSphereRadius);
       if (%nearbyDensity > %highestDensity)
          %highestDensity = %nearbyDensity;
      }
     %found = containerSearchNext();
   }

   if (%numTurretsNearby > %highestDensity)
      %highestDensity = %numTurretsNearby;
   return %highestDensity > $TurretIndoorMaxPerSphere;
}

function TurretOutdoorDeployableImage::testTurretSaturation(%item)
{
   %highestDensity = 0;
   InitContainerRadiusSearch(%item.surfacePt, $TurretOutdoorSphereRadius, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
   while(%found)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundname $= TurretDeployedFloorIndoor) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) )
      {
           //found one
           %numTurretsNearby++;

       %nearbyDensity = testNearbyDensity(%found, $TurretOutdoorSphereRadius);
       if (%nearbyDensity > %highestDensity)
          %highestDensity = %nearbyDensity;
      }
     %found = containerSearchNext();
   }

   if (%numTurretsNearby > %highestDensity)
      %highestDensity = %numTurretsNearby;
   return %highestDensity > $TurretOutdoorMaxPerSphere;
}

function ShapeBaseImageData::testTurretSaturation(%item, %surfacePt)
{
   //don't check this for non-turret deployables
}

function testNearbyDensity(%item, %radius)
{
   //this checks how many turrets are in adjacent spheres in case placing a new one overloads them.
   %surfacePt = posFromTransform(%item.getTransform());
   %turretCount = 0;

   InitContainerRadiusSearch(%surfacePt, %radius, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
   while(%found)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundname $= TurretDeployedFloorIndoor) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) )
         %turretCount++;
     %found = containerSearchNext();
   }
   return %turretCount;
}

function ShapeBaseImageData::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      $MaxDeployDistance,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::StaticShapeObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSlopeTooGreat(%surface, %surfaceNrm))
   {
      %disqualified = $NotDeployableReason::SlopeTooGreat;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if (%item.testObjectTooClose(%surfacePt))
   {
      %disqualified = $NotDeployableReason::ObjectTooClose;
   }
   else if (%item.testTurretTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::TurretTooClose;
   }
   else if (%item.testInventoryTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::InventoryTooClose;
   }
   else if (%item.testTurretSaturation())
   {
      %disqualified = $NotDeployableReason::TurretSaturation;
   }
   else if(%item.extendedDeployChecks(%plyr))
   {
      %disqualified = $NotDeployableReason::EDCFailed;
   }
   else if(%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      if(%item.deployed.className $= "DeployedTurret")
      {
         %xform = %item.deployed.getDeployTransform(%item.surfacePt, %item.surfaceNrm);
      }
      else if(%item.deployed $= StationInventory)
      {
         %xform = %surfaceNrm; // hahahak (ST)
      }
      else
      {
         %xform = %surfacePt SPC %rot;
      }

      if(isObject(%item.deployed))
         if(!%item.deployed.checkDeployPos(%xform))
            %disqualified = $NotDeployableReason::ObjectTooClose;
      else if (!%item.testHavePurchase(%xform))
      {
         %disqualified = $NotDeployableReason::SurfaceTooNarrow;
      }
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(25, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function Deployables::displayErrorMsg(%item, %plyr, %slot, %error)
{
   deactivateDeploySensor(%plyr);

   %errorSnd = '~wfx/misc/misc.error.wav';
   switch (%error)
   {
      case $NotDeployableReason::None:
         %item.onDeploy(%plyr, %slot);
         messageClient(%plyr.client, 'MsgTeamDeploySuccess', "");
         return;

      case $NotDeployableReason::NoSurfaceFound:
         %msg = '\c2Item must be placed within reach.%1';

      case $NotDeployableReason::MaxDeployed:
         %msg = '\c2Your team\'s control network has reached its capacity for this item.%1';

      case $NotDeployableReason::SlopeTooGreat:
         %msg = '\c2Surface is too steep to place this item on.%1';

      case $NotDeployableReason::SelfTooClose:
         %msg = '\c2You are too close to the surface you are trying to place the item on.%1';

      case $NotDeployableReason::ObjectTooClose:
         %msg = '\c2You cannot place this item so close to another object.%1';

      case $NotDeployableReason::NoTerrainFound:
         %msg = '\c2You must place this on outdoor terrain.%1';

      case $NotDeployableReason::NoInteriorFound:
         %msg = '\c2You must place this on a solid surface.%1';

      case $NotDeployableReason::TurretTooClose:
         %msg = '\c2Interference from a nearby turret prevents placement here.%1';

      case $NotDeployableReason::TurretSaturation:
         %msg = '\c2There are too many turrets nearby.%1';

      case $NotDeployableReason::SurfaceTooNarrow:
         %msg = '\c2There is not adequate surface to clamp to here.%1';

      case $NotDeployableReason::InventoryTooClose:
         %msg = '\c2Interference from a nearby inventory prevents placement here.%1';

      case $NotDeployableReason::EDCFailed:
         %msg = '\c2%2%1';

      case $NotDeployableReason::OutOfBounds:
         %msg = '\c2You are out of bounds.%1';

      // --------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//      case $NotDeployableReason::OrganicTooClose:
//         %msg = '\c2You cannot place this item so close to an organic object.%1';
      // --------------------------------------------------------------------------------------

      default:
         %msg = '\c2Deploy failed.';
   }

   messageClient(%plyr.client, 'MsgDeployFailed', %msg, %errorSnd, $EDC::Reason);
}

function ShapeBaseImageData::testObjectTooClose(%item)
{
   %mask =    ($TypeMasks::VehicleObjectType     | $TypeMasks::MoveableObjectType   |
//               $TypeMasks::StaticShapeObjectType |
               $TypeMasks::ForceFieldObjectType  | $TypeMasks::ItemObjectType       |
               $TypeMasks::PlayerObjectType      | $TypeMasks::TurretObjectType);

   InitContainerRadiusSearch( %item.surfacePt, $MinDeployDistance, %mask );

   %test = containerSearchNext();
   return %test;
}

function ShapeBase::isFlagInArea(%obj, %rad, %erad)
{
   %team = %obj.team;
   %homeFlag = $TeamFlag[%team];

   if(isObject(%homeFlag)) // test 1 - near our flag?
   {
      %dist = VectorDist(%homeFlag.originalPosition, %obj.position);
      if(%dist <= %rad)
         return true;
   }

   if(%erad)
   {
      %team = %obj.team == 1 ? 2 : 1; // test 2 - near their flag?
      %homeFlag = $TeamFlag[%team];

      if(isObject(%homeFlag))
      {
         %dist = VectorDist(%homeFlag.originalPosition, %obj.position);
         if(%dist <= %erad)
            return true;
      }
   }

   return false;
}

function ShapeBase::isEnemyGenInArea(%obj, %rad)
{
//   %team = %obj.team == 1 ? 2 : 1; // near their gen
   %team = %obj.client.team;

   if(%rad)
   {
      InitContainerRadiusSearch(%obj.getPosition(), %rad, $TypeMasks::StaticShapeObjectType);

      %gen = containerSearchNext();

      while(%gen)
      {
         if(isObject(%gen))
         {
            %name = %gen.getDatablock().getName();

            if(%name $= "GeneratorLarge" || %name $= "SolarPanel")
            {
//               echo("found gen" SPC %gen SPC "myteam" SPC %obj.team SPC "itsteam" SPC %team);

               if(%gen.team != %team)
                     return true;
            }
            %gen = containerSearchNext();
         }
      }
   }

   return false;
}

function ShapeBase::isFriendlyGenInArea(%obj, %rad)
{
//   %team = %obj.team == 1 ? 2 : 1; // near their gen
   %team = %obj.client.team;

   if(%rad)
   {
      InitContainerRadiusSearch(%obj.getPosition(), %rad, $TypeMasks::StaticShapeObjectType);

      %gen = containerSearchNext();

      while(%gen)
      {
         if(isObject(%gen))
         {
            %name = %gen.getDatablock().getName();

            if(%name $= "GeneratorLarge" || %name $= "SolarPanel")
            {
               if(%gen.team == %team)
               {
                    if(%gen.owner)
                         continue;
                    else
                         return true;
               }
            }
            %gen = containerSearchNext();
         }
      }
   }

   return false;
}

function getTeamSpawnSphereCount(%team)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
      return %group.getCount();
   else
      return 0;
}

function getTeamSpawnSphere(%team, %index)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
   {
      %count = %group.getCount();

      if(%index > %count)
          return 0;

      %sphere = %group.getObject(%index);

      if(isObject(%sphere))
           return %sphere;

      return 0; // no spawnsphere found, nuts
   }
}

function getTeamSpawnSpheres(%team)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
   {
      %count = %group.getCount();
      %spheres = %count;

      for(%i = 0; %i < %count; %i++)
      {
          %sphere = %group.getObject(%i);

          if(isObject(%sphere))
              %spheres = %spheres SPC %sphere;
      }

      return %spheres; // no spawnsphere found, nuts
   }
}

function getTeamNearestSpawnSphere(%team, %pos)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops"@%team;
   %sphereIndex = 0;
   %group = nameToID(%teamDropsGroup);
   %lowestDist = 0;
   %lowestDistID = 0;

   if(%group != -1)
   {
      %count = %group.getCount();

      if(%count < 1)
           return isObject(%group.getObject(0)) ? %group.getObject(0) : 0;

      for(%i = 0; %i < %count; %i++)
      {
          %sphere = %group.getObject(%i);

          if(isObject(%sphere))
          {
              %sphereArray[%sphereIndex] = %sphere;
              %dist = VectorDist(%pos, %sphere.position);

              if(%dist < %lowestDist)
              {
                   %lowestDist = %dist;
                   %lowestDistID = %i;
              }

              %sphereIndex++;
          }
      }

      return %sphereArray[%lowestDistID];
   }

   return 0; // No sphere
}

function getEnemyNearestSpawnSphere(%team, %pos)
{
     %team = %team == 2 ? 1 : 2;
     return getTeamNearestSpawnSphere(%team, %pos);
}

function ShapeBase::isNearbyFriendlyBase(%obj)	// function ShapeBase::isNearbyFriendlyBase(%obj, %rad) -soph
{
   %team = %obj.team;
   %pos = %obj.getWorldBoxCenter();

   %friendlyBase = getTeamNearestSpawnSphere(%team, %pos);

   if(isObject(%friendlyBase))
   {
      %dist = VectorDist(%friendlyBase.position, %pos);

//      if(%dist <= %rad)			// -soph
         return %dist;				// return true; -soph
   }

   return -1;					// return false; -soph
}

function ShapeBase::isNearbyEnemyBase(%obj, %rad)
{
   %team = %obj.team == 2 ? 1 : 2;
   %pos = %obj.getWorldBoxCenter();

   %friendlyBase = getTeamNearestSpawnSphere(%team, %pos);

   if(isObject(%friendlyBase))
   {
      %dist = VectorDist(%friendlyBase.position, %pos);

      if(%dist <= %rad)
         return true;
   }

   return false;
}

function ShapeBaseImageData::extendedDeployChecks(%item, %plyr)
{
   $EDC::Reason = "";
   return false;
}

function ShapeBaseImageData::testNoSurfaceInRange(%item, %plyr)
{
   return !Deployables::searchView(%plyr, $MaxDeployDistance, $TypeMasks::StaticShapeObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType);
}

function ShapeBaseImageData::testSlopeTooGreat(%item)
{
   if(%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > %item.maxDepSlope;
}

function ShapeBaseImageData::testInventoryTooClose(%item, %plyr)
{
   return false;
}

function InventoryDeployableImage::testInventoryTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, $InventorySpaceRadius, $TypeMasks::StaticShapeObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if( (%foundName $= DeployedStationInventory) )
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

function ForceFieldBareData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

   %pz = new PhysicalZone() {
      position = %obj.position;
      rotation = %obj.rotation;
      scale    = %obj.scale;
      polyhedron = "0.000000 1.0000000 0.0000000 1.0000000 0.0000000 0.0000000 0.0000000 -1.0000000 0.0000000 0.0000000 0.0000000 1.0000000";
      velocityMod  = 1.0;
      gravityMod   = 1.0;
      appliedForce = "0 0 0";
		ffield = %obj;
   };
	%pzGroup = nameToID("MissionCleanup/PZones");
	if(%pzGroup <= 0) {
		%pzGroup = new SimGroup("PZones");
		MissionCleanup.add(%pzGroup);
	}
	%pzGroup.add(%pz);
   %obj.pz = %pz;
   //MissionCleanupGroup.add(%pz);
}

function componentDestroy(%int)
{
   %int.setDamageState(Destroyed);
}

$CONSTRUCT::BuildingDestroyTimeMax = 8000;
$CONSTRUCT::BuildingDestroyTimeMin = 5000;
//-Nite- Nuke
//function handleBuildingNuked(%obj)
//{
//   %obj.schedule(500, "delete");
//
//   if(%obj.isParent)
//   {
//      $TeamDeployedCount[%obj.team, %obj.watDisIz]--;
//      InitContainerRadiusSearch(%obj.getPosition(), 50, $TypeMasks::StaticObjectType | $TypeMasks::StationObjectType | $TypeMasks::StaticShapeObjectType);
//      while((%int = ContainerSearchNext()) != 0)
//      {
//         if(%int.parent && %int.parent == %obj)
//         {
//            if(isObject(%int.trigger))
//               %int.trigger.delete();
//
//            if(isObject(%int))
//               schedule(getRandom($CONSTRUCT::BuildingDestroyTimeMax, $CONSTRUCT::BuildingDestroyTimeMin), %int, "componentDestroy", %int);
//         }
//      }
//   }
//   else if(%obj.parent)
//      if(isObject(%obj.parent))
//         %obj.parent.setDamageState(Destroyed);
//}

//exec("scripts/MDBuildings.cs");

datablock StaticShapeData(MultiObjectManager)
{
   shapeFile = "turret_muzzlepoint.dts";
};

function MultiObjectManager::onDestroyed(%data, %obj, %prevState)
{
   Parent::onDestroyed(%data, %obj, %prevState);
   destroyAllAttachments(%obj);

   if(%obj.shield[0])
      destroyForcefields(%obj);

//   if(%obj.type == 1)
//      $TeamDeployedCount[%obj.team, BorgCubePack]--;
//   else if(%obj.type == 2)
//      $TeamDeployedCount[%obj.team, VehiclePadPack]--;
//   else if(%obj.type == 3)
//      $TeamDeployedCount[%obj.team, DefenseBasePack]--;
}

function addMultiObjectManager(%plyr, %trans)
{
   %deplObj = new StaticShape()
   {
      dataBlock = MultiObjectManager;
      team = %plyr.client.team;
   };
   MissionCleanup.add(%deplObj);

   %deplObj.setTransform(%trans);

   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);
//   addToDeployGroup(%deplObj);

   %deplObj.multiObjectManager = true;

   return %deplObj;
}

function addStation(%plyr, %trans, %parent, %genPower)
{
   %obj = new StaticShape()
   {
      dataBlock = StationInventory;
      team = %plyr.client.team;
   };

   %obj.setTransform(%trans);
   %obj.fake = true;
   %obj.parent = %parent;
   MissionCleanup.add(%obj);

//   if($SelfPower[LargeInvStation])
//   {
//      %obj.setSelfPowered();
//      %obj.setRechargeRate(%obj.getDatablock().rechargeRate);
//   }

   %trigger = new Trigger()
   {
      dataBlock = stationTrigger;
      polyhedron = "-0.75 0.75 0.1 1.5 0.0 0.0 0.0 -1.5 0.0 0.0 0.0 2.3";
   };
   MissionCleanup.add(%trigger);
   %trigger.setTransform(%obj.getTransform());

   %trigger.station = %obj;
   %trigger.mainObj = %obj;
   %trigger.disableObj = %obj;
   %obj.trigger = %trigger;

   %obj.team = %plyr.client.team;
   %obj.owner = %plyr.client;

   if(%obj.getTarget() != -1)
      setTargetSensorGroup(%obj.getTarget(), %plyr.client.team);

//   addToDeployGroup(%obj);
   AIDeployObject(%plyr.client, %obj);
//   %stationObj.deploy();

//  echo("addStation" SPC %plyr SPC %trans SPC %parent SPC %genPower);

// findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %obj);

   return %obj;
}

function addTurret(%plyr, %trans, %parent, %barrel, %genPower)
{
   if(%barrel $= "")
      %barrel = "PlasmaBarrelLarge";

   %turret = new Turret()
   {
      dataBlock = "FreeBaseTurret";
      initialBarrel = %barrel;
   };

   MissionCleanup.add(%turret);

   %turret.setTransform(%trans);
   %turret.dgenAccessible = true;
   %turret.parent = %parent;

//   if($SelfPower[TurretBase])
//   {
//      %turret.setSelfPowered();
//      %turret.setRechargeRate(%turret.getDatablock().rechargeRate);
//   }

   %turret.team = %plyr.client.team;
   %turret.owner = %plyr.client;

   if(%turret.getTarget() != -1)
      setTargetSensorGroup(%turret.getTarget(), %plyr.client.team);

//   addToDeployGroup(%turret);
   AIDeployObject(%plyr.client, %turret);

//   findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %turret);

   return %turret;
}

function addObject(%plyr, %data, %trans, %parent, %scale, %genPower)
{
   if(%scale $= "")
      %scale = "1 1 1";

   %deplObj = new StaticShape()
   {
      dataBlock = %data;
      team = %plyr.client.team;
      scale = %scale;
   };
   MissionCleanup.add(%deplObj);

   %deplObj.setTransform(%trans);
   %deplObj.parent = %parent;

   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);
//   addToDeployGroup(%deplObj);

//   findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %deplObj);

   return %deplObj;
}

function addObjectV(%plyr, %data, %pos, %vec, %parent, %scale, %genPower)
{
   if(%scale $= "")
      %scale = "1 1 1";

   %deplObj = new StaticShape()
   {
      dataBlock = %data;
      team = %plyr.client.team;
      scale = %scale;
   };
   MissionCleanup.add(%deplObj);

   %deplObj.setDeployRotation(%pos, %vec);
   %deplObj.parent = %parent;

   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);
//   addToDeployGroup(%deplObj);

//   findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %deplObj);

   return %deplObj;
}

function addForcefield(%plyr, %data, %pos, %rot, %scale, %parent)
{
   %ff = new forceFieldBare()
   {
      position = %pos;
      rotation = %rot;
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %plyr.client.team);
   MissionCleanup.add(%ff);
   %ff.parent = %parent;

   if(!%parent.attachedFF)
      %parent.attachedFF = 0;

   %parent.shield[%parent.attachedFF] = %ff;
   %parent.attachedFF++;

   return %ff;
}

function addForcefieldV(%plyr, %data, %pos, %vec, %scale, %parent)
{
   %ff = new forceFieldBare()
   {
      position = %pos;
//      rotation = "";
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };
   %ff.setDeployRotation(%pos, %vec);

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %plyr.client.team);
   MissionCleanup.add(%ff);
   %ff.parent = %parent;

   if(!%parent.attachedFF)
      %parent.attachedFF = 0;

   %parent.shield[%parent.attachedFF] = %ff;
   %parent.attachedFF++;

   return %ff;
}

////////////////////////////////////////// Team variants of the above functions
function addMultiObjectManagerO(%owner, %trans)
{
   %deplObj = new StaticShape()
   {
      dataBlock = MultiObjectManager;
      team = %plyr.client.team;
   };
   MissionCleanup.add(%deplObj);

   %deplObj.setTransform(%trans);

   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %owner.team;
   %deplObj.owner = %owner;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %owner.team);
//   addToDeployGroup(%deplObj);

   %deplObj.multiObjectManager = true;

   return %deplObj;
}

function addStationO(%owner, %trans, %parent, %genPower)
{
   %obj = new StaticShape()
   {
      dataBlock = StationInventory;
      team = %owner.team;
   };

   %obj.setTransform(%trans);
   %obj.fake = true;
   %obj.parent = %parent;
   MissionCleanup.add(%obj);

//   if($SelfPower[LargeInvStation])
//   {
//      %obj.setSelfPowered();
//      %obj.setRechargeRate(%obj.getDatablock().rechargeRate);
//   }

   %trigger = new Trigger()
   {
      dataBlock = stationTrigger;
      polyhedron = "-0.75 0.75 0.1 1.5 0.0 0.0 0.0 -1.5 0.0 0.0 0.0 2.3";
   };
   MissionCleanup.add(%trigger);
   %trigger.setTransform(%obj.getTransform());

   %trigger.station = %obj;
   %trigger.mainObj = %obj;
   %trigger.disableObj = %obj;
   %obj.trigger = %trigger;

   %obj.team = %owner.team;
   %obj.owner = %owner;

   if(%obj.getTarget() != -1)
      setTargetSensorGroup(%obj.getTarget(), %owner.team);

//   addToDeployGroup(%obj);
   AIDeployObject(%owner, %obj);
//   %stationObj.deploy();

//  echo("addStation" SPC %plyr SPC %trans SPC %parent SPC %genPower);

// findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %obj);

   return %obj;
}

function addTurretO(%owner, %trans, %parent, %barrel, %genPower)
{
   if(%barrel $= "")
      %barrel = "PlasmaBarrelLarge";

   %turret = new Turret()
   {
      dataBlock = "FreeBaseTurret";
      initialBarrel = %barrel;
   };

   MissionCleanup.add(%turret);

   %turret.setTransform(%trans);
   %turret.dgenAccessible = true;
   %turret.parent = %parent;

//   if($SelfPower[TurretBase])
//   {
//      %turret.setSelfPowered();
//      %turret.setRechargeRate(%turret.getDatablock().rechargeRate);
//   }

   %turret.team = %owner.team;
   %turret.owner = %owner;

   if(%turret.getTarget() != -1)
      setTargetSensorGroup(%turret.getTarget(), %owner.team);

//   addToDeployGroup(%turret);
   AIDeployObject(%owner, %turret);

//   findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %turret);

   return %turret;
}

function addObjectO(%owner, %data, %trans, %parent, %scale, %genPower)
{
   if(%scale $= "")
      %scale = "1 1 1";

   %deplObj = new StaticShape()
   {
      dataBlock = %data;
      team = %owner.team;
      scale = %scale;
   };
   MissionCleanup.add(%deplObj);

   %deplObj.setTransform(%trans);
   %deplObj.parent = %parent;

   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %owner.team;
   %deplObj.owner = %owner;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %owner.team);
//   addToDeployGroup(%deplObj);

//   findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %deplObj);

   return %deplObj;
}

function addObjectVO(%owner, %data, %pos, %vec, %parent, %scale, %genPower)
{
   if(%scale $= "")
      %scale = "1 1 1";

   %deplObj = new StaticShape()
   {
      dataBlock = %data;
      team = %owner.team;
      scale = %scale;
   };
   MissionCleanup.add(%deplObj);

   %deplObj.setDeployRotation(%pos, %vec);
   %deplObj.parent = %parent;

   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %owner.team;
   %deplObj.owner = %owner;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %owner.team);
//   addToDeployGroup(%deplObj);

//   findFriendlyGenInArea(%deplObj, 250);

   if(%parent !$= "")
      addAttachment(%parent, %deplObj);

   return %deplObj;
}

function addForcefieldO(%owner, %data, %pos, %rot, %scale, %parent)
{
   %ff = new forceFieldBare()
   {
      position = %pos;
      rotation = %rot;
      scale = %scale;
      dataBlock = %data;
      team = %owner.team;
   };

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %owner.team);
   MissionCleanup.add(%ff);
   %ff.parent = %parent;

   if(!%parent.attachedFF)
      %parent.attachedFF = 0;

   %parent.shield[%parent.attachedFF] = %ff;
   %parent.attachedFF++;

   return %ff;
}

function addForcefieldVO(%owner, %data, %pos, %vec, %scale, %parent)
{
   %ff = new forceFieldBare()
   {
      position = %pos;
//      rotation = "";
      scale = %scale;
      dataBlock = %data;
      team = %owner.team;
   };
   %ff.setDeployRotation(%pos, %vec);

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %owner.team);
   MissionCleanup.add(%ff);
   %ff.parent = %parent;

   if(!%parent.attachedFF)
      %parent.attachedFF = 0;

   %parent.shield[%parent.attachedFF] = %ff;
   %parent.attachedFF++;

   return %ff;
}



function destroyTAttachments(%obj)
{
   %inc = 0;
   while((%int = %obj.attachment[%inc]) !$= "")
   {
      %int.setDamageState(Destroyed);

      %obj.attachment[%inc] = "";
      %inc++;
   }
}

function detectDeployingOn(%deplObj, %pos)
{
   InitContainerRadiusSearch(%pos, 1, $TypeMasks::StaticShapeObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType); //$TypeMasks::StaticObjectType |
   %found = 0;

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.getClassName() !$= "TerrainBlock" && %int.getClassName() !$= "InteriorInstance" && %int.getClassName() !$= "StaticTSObject" && !%int.isPlayer() && !%int.isVehicle()) // && (%int.getClassName() $= "StaticShape" || )
      {
//         echo("attaching to object "@%int SPC %int.getClassName());

         if(!%int.looseAttachment)
         {
            if($DebugMode)
               echo("attaching to object "@%int SPC %int.getClassName() SPC detag(%int.getDatablock().targetNameTag) SPC detag(%int.getDatablock().targetTypeTag));

            addAttachment( %int , %deplObj , 0 , true ) ;	// addAttachment(%int, %deplObj); wrong team bug -soph
            %deplObj.looseAttachment = true;

//            if(%deplObj.powerFromAttachment)
//               %deplObj.setSelfPowered();
         }
         else
         {
            if($DebugMode)
               echo("skipping loose attachment "@%int SPC %int.getClassName());

            continue;
         }

//         if(addAttachment(%int, %deplObj))
//         {
//            %found = %int;
//            break;
//         }
      }
   }

   if($DebugMode)
   {
      if(isObject(%found))
      {
         %attach = hasAttachments(%found);
         echo("("@%found SPC %found.getClassName()@") has attachments? "@%attach);

         if(%attach)
            listAttachments(%found);
      }
   }
}

function addAttachment(%obj, %attach, %externalAttachment , %preserveTeam )	// (%obj, %attach, %externalAttachment) -soph
{
   if(isObject(%obj) && isObject(%attach))
   {
      %idx = 0;

      if(%obj == %attach) // no point in attaching you to yourself. LOL
      {
         if($DebugMode)
            echo("object tried to attach itself: "@%obj);

         return false;
      }

      while(%obj.attachment[%idx] !$= "") // get the last used index
      {
         if(isObject(%obj.attachment[%idx])) // get the first object that doesn't exist and take it's place
            %idx++;
         else
            %obj.attachment[%idx] = ""; // clear it and rerun the loop so it will detect it.
      }

      if(getAttachementByIndex(%attach, %obj) != -1)
      {
         if($DebugMode)
            echo("object tried to attach itself indirectly: "@%obj);

         return false;
      }

      %obj.attachment[%idx] = %attach;
      %obj.attachment[%idx].attachedToObject = true;
      %obj.attachment[%idx].externalAttachment = %externalAttachment;
//      %attach.parent = %obj;
      if( !%preserveTeam )							// +soph
      %attach.team = %obj.team;

      if($DebugMode)
         echo("attached! ("@%obj SPC %obj.getClassName()@").attachment["@%idx@"] = "@%attach SPC %attach.getClassName());

      return true;
   }
   return false;
}

function hasAttachments(%obj)
{
   if(isObject(%obj))
      if(%obj.attachmentCount || %obj.attachment[0])
         return true;

   return false;
}

function listAttachments(%obj, %level)
{
   if(isObject(%obj))
   {
      if(hasAttachments(%obj))
      {
         %inc = 0;

         if(%level $= "")
           %level = 0;

         %levelText = "";

         for(%l = 0; %l < %level; %l++)
            %levelText = %levelText@"   ";

         while((%int = %obj.attachment[%inc]) !$= "")
         {
            if(isObject(%int))
            {
               echo(%levelText@%inc SPC %int SPC %int.getClassName());
               listAttachments(%obj.attachment[%inc], %level++);
            }
            %inc++;
         }
      }
   }
}

function destroyAllAttachments(%obj)
{
echo("destroyAllAttachments("@%obj@")"@%obj.getDataBlock().getName());
   if(isObject(%obj))
   {
      if(hasAttachments(%obj))
      {
         if($DebugMode)
         {
            echo("("@%obj@") destroying all attachments:");
            listAttachments(%obj);
         }
         %inc = 0;

         while((%int = %obj.attachment[%inc]) !$= "")
         {
            if(isObject(%int))
            {
               if( %obj.disassembled )	// +[soph]
               {
                  %int.disassembled = true ;
                  if( %obj.DEFeedbackPercent > 0 )
                     %int.DEFeedbackPercent = %obj.DEFeedbackPercent ;
               }			// +[/soph]
               %obj.attachment[%inc].attachedToObject = false;
               destroyAllAttachments(%obj.attachment[%inc]);

               if(isObject(%int) && %int != %obj)
                  %int.setDamageState(Destroyed);

               %obj.attachment[%inc] = "";
            }
            %inc++;
         }
      }
   }
}

function getAttachementByIndex(%obj, %attach)
{
   if(isObject(%obj))
   {
      if(hasAttachments(%obj))
      {
         %idx = 0;

         while(%obj.attachment[%idx] !$= "") // get the last used index
         {
            if(isObject(%obj.attachment[%idx])) // get the first object that doesn't exist and take it's place
            {
               if(%obj.attachment[%idx] == %attach)
                  return %idx;
            }
            %idx++;
         }
         return -1;
      }
      return -1;
   }
   return -1;
}

function destroyAttachment(%obj, %idx)
{
   if(isObject(%obj))
   {
      if(isObject(%obj.attachment[%idx]))
      {
         %obj.attachment[%idx].attachedToObject = false;
         %obj.attachment[%idx].setDamageState(Destroyed);
//         %obj.attachment[%idx] = "";

         if(%obj.attachmentCount)
            %obj.attachmentCount--;
      }
   }
}

function unattachAttachment(%obj, %idx)
{
   if(isObject(%obj))
   {
      if(isObject(%obj.attachment[%idx]))
      {
         %obj.attachment[%idx].attachedToObject = false;
//         %obj.attachment[%idx] = "";

         if(%obj.attachmentCount)
            %obj.attachmentCount--;
      }
   }
}

function destroyForcefields(%obj)
{
   %inc = 0;
   while((%int = %obj.shield[%inc]) !$= "")
   {
      %int.setPosition("-10000 -10000 -10000");

      if(isObject(%int.pz))
         %int.pz.setPosition(%int.getPosition());

      %int.getDatablock().losePower(%int);
      %int.delete();

      %obj.shield[%inc] = "";
      %inc++;
   }
}

//                                     -->>DEPLOY CODE BEGIN <<--

//====================================== Deployable Turret Base
//$TeamDeployableMax[TurretBasePack] = 0;

function calculateTurretMax()
{
     %count = 0;

     for(%i = 0; %i < ClientGroup.getCount(); %i++)
     {
          if(!ClientGroup.getObject(%i).isAIControlled())
               %count++;
     }

     $TeamDeployableMax[TurretBasePack] = 2 + mFloor(%count / 4);
     $TeamDeployableMax[DeployableSentryPack] = 2 + mFloor(%count / 8);
     
     // Vehicle and other defs to refresh here:
     refreshVehicleCounts();
     verifyMechCount();
     fixSky();
     
     $InvBanList[$CurrentMissionType, "Mortar"] = 0;

     schedule(30000, 0, calculateTurretMax);
}

if($TeamDeployableMax[TurretBasePack] $= "")
     calculateTurretMax();

datablock TurretData(FreeBaseTurret) : TurretDamageProfile
{
   className      = TurretBase;
   catagory       = "Turrets";
   shapeFile      = "turret_base_large.dts";
   preload        = true;

   mass           = 1.0;  // Not really relevant

   maxDamage      = 2.9;	// 1.4; -soph
   destroyedLevel = 2.9;	// 1.4; just enough to not die from an MSTB -soph
   disabledLevel  = 1.9;	// 1.0; goes down to 3 mega blaster bolts -soph
   explosion      = TurretExplosion;
	expDmgRadius = 25.0;
	expDamage = 2.7;
	expImpulse = 2000.0;
   repairRate     = 0;
   emap = true;

   thetaMin = 15;
   thetaMax = 140;
   deployedObject = true;

   isShielded           = true;
   energyPerDamagePoint = 50;
   maxEnergy = 150;		// 125; -soph
   rechargeRate = 0.25;		// 0.35; -soph
   humSound = SensorHumSound;
   pausePowerThread = true;

   canControl = true;
   cmdCategory = "Tactical";
   cmdIcon = CMDTurretIcon;
   cmdMiniIconName = "commander/MiniIcons/com_turretbase_grey";
   targetNameTag = 'Deployed Base';
   targetTypeTag = 'Turret';
//   sensorData = TurretBaseSensorObj;
//   sensorRadius = TurretBaseSensorObj.detectRadius;
//   sensorColor = "0 212 45";

   firstPersonOnly = true;

   debrisShapeName = "debris_generic.dts";
   debris = TurretDebris;

   undeployPack = "TurretBasePack";
};

datablock ShapeBaseImageData(TurretBaseImage)
{
   mass = 20;

   shapeFile = "pack_deploy_turreti.dts";
   item = TurretBasePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = FreeBaseTurret;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 360;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(TurretBasePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_turreti.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "TurretBaseImage";
   pickUpName = "a deployable turret base";
   heatSignature = 0;

   emap = true;
};

function TurretBasePack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function TurretBaseImage::extendedDeployChecks(%item, %plyr)
{
//   if(getSimTime() < ($TurretDeployedTime[%plyr.client.team] + 60000))
//   {
//      $EDC::Reason = "Supply ship is reloading a Deployable Turret Base, stand by...";
//      return true;
//   }

   if(%plyr.isFlagInArea(4))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   %dist = %plyr.isNearbyFriendlyBase();							// if(!%plyr.isNearbyFriendlyBase(300)) -soph
   if( %dist > 300 )										// +soph
   {
      $EDC::Reason = "You are trying to deploy" SPC mFloor( %dist - 299 ) @ "m too far away from the base.";	// $EDC::Reason = "You are trying to deploy too far away from the base.";
      return true;
   }
   else 
      if ( %dist < 0 )										// [soph]
      {
         $EDC::Reason = "There is no friendly base node in this region, turret cannot be deployed.";
         return true;
      }												// [/soph]

   if(%plyr.isNearbyEnemyBase(200))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   InitContainerRadiusSearch(%item.surfacePt, 1, $TypeMasks::StaticShapeObjectType);
   while((%found = containerSearchNext()) != 0)
   {
         if((%found.team == %plyr.team) && %found.blastEmplacement)
         {
            $EDC::Reason = "Cannot deploy turret on a Blast Wall or Blast Door.";
            return true;
         }
   }

   $EDC::Reason = "";
   return false;
}

function TurretBaseImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);

   initSpecialDeploySequenceItemT("Base Turret", 500, 3, %item, %plyr, %slot, %rot, %true);
}

function FreeBaseTurret::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   if(!%obj.parent)
   {
      $TeamDeployedCount[%obj.team, TurretBasePack]--;
      feedbackDED( %obj , 1.7 );		// +soph
   }
//   else
//      %obj.parent.setDamageState(Destroyed);

   %obj.schedule(500, "delete");
}

function TurretBaseImage::testTurretTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, 8, $TypeMasks::TurretObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
//      %foundName = %found.getDataBlock().getName();
//      if(true) //(%foundname $= TurretDeployedFloorIndoor) || (%foundname $= FreeBaseTurret) || (%foundName $= TurretDeployedWallIndoor) || (%foundName $= TurretDeployedCeilingIndoor) || (%foundName $= TurretDeployedOutdoor) || (%foundname $= BlastWall) || (%foundname $= BlastDoor))
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }

   return %turretInRange;
}

//====================================== Deployable Sentry Turret
//$TeamDeployableMax[DeployableSentryPack] = 4;

datablock TurretData(DeployableSentryTurret) : TurretDamageProfile
{
   catagory = "Turrets";
   shapeFile = "turret_sentry.dts";
   mass = 5.0;

   barrel = SentryTurretBarrel;

   maxDamage = 1.0;
   destroyedLevel = 1.0;
   disabledLevel = 0.70;
   explosion      = ShapeExplosion;
   expDmgRadius = 5.0;
   expDamage = 0.4;
   expImpulse = 1000.0;
   repairRate = 0;

   thetaMin = 89;
   thetaMax = 175;
   emap = true;

   isShielded           = true;
   energyPerDamagePoint = 100;
   maxEnergy = 150;
   rechargeRate = 0.40;

   canControl = true;
   cmdCategory = "Tactical";
   cmdIcon = CMDTurretIcon;
   cmdMiniIconName = "commander/MiniIcons/com_turret_grey";
   targetNameTag = 'Sentry';
   targetTypeTag = 'Turret';
   sensorData = SentryMotionSensor;
   sensorRadius = SentryMotionSensor.detectRadius;
   sensorColor = "9 136 255";
   firstPersonOnly = true;
};

datablock ShapeBaseImageData(DeployableSentryPackImage)
{
   mass = 15;
   shapeFile = "pack_deploy_turreti.dts";
   item = DeployableSentryPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = "DeployableSentryTurret";

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   emap = true;

   maxDepSlope = 360;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(DeployableSentryPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_turreti.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "DeployableSentryPackImage";
   pickUpName = "a deployable sentry";
   emap = true;
};

function DeployableSentryPackImage::testNoTerrainFound(%item)
{
}

function DeployableSentryPackImage::testNoInteriorFound(%item)
{
   return %item.surface.getClassName() !$= InteriorInstance;
}

function DeployableSentryPackImage::testObjectTooClose(%item)
{
//   return false;

   %mask =    ($TypeMasks::VehicleObjectType     | $TypeMasks::MoveableObjectType   |
               $TypeMasks::ForceFieldObjectType  | $TypeMasks::ItemObjectType       |
               $TypeMasks::PlayerObjectType      | $TypeMasks::TurretObjectType);

   InitContainerRadiusSearch( %item.surfacePt, 0.5, %mask );

   %test = containerSearchNext();
   return %test;
}

function DeployableSentryTurret::checkDeployPos(%item)
{
   return true;
}

function DeployableSentryPackImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   %dist = %plyr.isNearbyFriendlyBase();									// if(!%plyr.isNearbyFriendlyBase(150)) -soph
   if( %dist > 150 )												// +soph
   {
      $EDC::Reason = "You are trying to deploy" SPC mFloor( %dist - 149 ) @ "m too far away from the base.";	// $EDC::Reason = "You are trying to deploy too far away from the base.";
      return true;
   }
   else 
      if ( %dist < 0 )										// [soph]
      {
         $EDC::Reason = "There is no friendly base node in this region, sentry cannot be deployed.";
         return true;
      }												// [/soph]

   if(%plyr.isNearbyEnemyBase(200))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   InitContainerRadiusSearch(%item.surfacePt, 1, $TypeMasks::StaticShapeObjectType);
   while((%found = containerSearchNext()) != 0)
   {
         if((%found.team == %plyr.team) && %found.blastEmplacement)
         {
            $EDC::Reason = "Cannot deploy turret on a Blast Wall or Blast Door.";
            return true;
         }
   }

   $EDC::Reason = "";
   return false;
}

function DeployableSentryPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);

   %deplObj = new Turret()
   {
      dataBlock = %item.deployed;
      initialBarrel = "SentryTurretBarrel";
   };

   %deplObj.setDeployRotation(%item.surfacePt, vectorNeg(%item.surfaceNrm));
//   %deplObj.setTransform(%item.surfacePt SPC %rot);

//   %deplObj.setSelfPowered();
//   if(%deplObj.getDatablock().rechargeRate)
//      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "DeployableSentryPack"]++;
   %deplObj.deploy();

   findFriendlyGenInArea(%deplObj, 150);
   detectDeployingOn(%beacon, %item.surfacePt);
}

function DeployableSentryPackImage::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function DeployableSentryTurret::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   $TeamDeployedCount[%obj.team, "DeployableSentryPack"]--;
   feedbackDED( %obj , 0.4 );		// +soph
   %obj.schedule(250, "delete");
}

// GeneratorData

datablock StaticShapeData(BaseGenerator) : StaticShapeDamageProfile // no more base destroyer cheat! AHHAHAHahahaHAHAhaHAah
{
   shapeFile      = "station_generator_large.dts";
   explosion      = MortarImpactExplosion;
   maxDamage      = 1.5;
   destroyedLevel = 1.5;
   disabledLevel  = 1.5;
   expDmgRadius = 25.0;
   expDamage = 10;
   expImpulse = 2000.0;

   maxEnergy = 50;
   rechargeRate = 0.05;
   deployableGen = true;
   depGenTime = 12;

   targetNameTag = 'Deployable Base';
   targetTypeTag = 'Generator';

   dynamicType = $TypeMasks::SensorObjectType ;
   isShielded = true ;
   energyPerDamagePoint = 30 ;
   maxEnergy = 50 ;
   rechargeRate = 0.05 ;
   humSound = GeneratorHumSound ;

//   cmdCategory = "Support";
//   cmdIcon = "CMDGeneratorIcon";
//   cmdMiniIconName = "commander/MiniIcons/com_generator";

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;
};

function BaseGenerator::onAdd(%this, %obj, %prevState)
{
   detectDeployingOn(%obj, %obj.getPosition());
   %obj.baseGenerator = true; // for FF Dmg Code

   powerRelayNodeUpdate(%obj);
}

function BaseGenerator::onDestroyed(%this, %obj, %prevState)
{
   powerRelayNodeShutdown(%obj);

   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

function GameBase::findTeamGenerator(%obj, %team, %rad)
{
   if(%rad)
   {
      InitContainerRadiusSearch(%obj.getPosition(), %rad, $TypeMasks::StaticShapeObjectType);
      %gen = containerSearchNext();

      while(%gen)
      {
//          echo("searching...:" SPC %gen SPC %gen.getDatablock().getName());
         if(isObject(%gen))
         {
            if(%gen.getDatablock().getName() $= "generatorLarge")
               if(%gen.team == %team)
                     return %gen;

            %gen = containerSearchNext();
         }
      }
   }

   return 0;
}


function findFriendlyGenInArea(%deplObj, %dist)
{
     if(!isObject(%deplObj))
          return;
//     trace(1);
      %nearbyGen = %deplObj.findTeamGenerator(%deplObj.team, %dist);

      if(%nearbyGen)
      {
//         echo("Found nearby gen" SPC %nearbyGen SPC %nearbyGen.getDatablock().getName());

         %nearbyGenGroup = %nearbyGen.getGroup();
         %nearbyGenGroup.add(%deplObj);
         %nearbyGenGroup.powerInit();
         return true;	// +soph
      }
      return false;	// +soph
//      trace(0);
}

function powerRelayNodeUpdate(%obj)
{
     if(!isObject(%obj))
          return;

//     echo("node update: relay power group -" SPC %obj.relayPowerGroup);
     if(%obj.relayPowerGroup $= "")
          %obj.relayPowerGroup = createRandomSimSet();

     %team = %obj.team;
     %noGenMap = false;

     // Find generator group
     %powerGenGroup = %obj.relayPowerGroup.group;
//     %genGroup = %powerGenGroup !$= "" ? %powerGenGroup : %obj.findTeamGenerator(%team, 10000).getGroup();
     %foundGen = %obj.findTeamGenerator(%team, 10000);

     if(isObject(%foundGen))
          %foundGroup = %foundGen.getGroup();
//     else
//     {
//          %foundGroup = createRandomSimGroup();
//          %noGenMap = true;
//     }

     %genGroup = %powerGenGroup ? %powerGenGroup : %foundGroup;

     // If we have a generator at home, add the locals
     if(%genGroup)
     {
          if(!%powerGenGroup)
          {
               %obj.relayPowerGroup.group = %genGroup;
               %genGroup.add(%obj);
          }

          InitContainerRadiusSearch(%obj.getPosition(), 100, $TypeMasks::StaticShapeObjectType | $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType);
          %item = containerSearchNext();

          %change = false;

          // Add them to our simset for easy power yanking
          while(%item)
          {
//                  echo("found item:" SPC %item SPC %item.getDatablock().getName() SPC %item.getGroup().getName());

               if((%item.getGroup().getName() $= "Deployables" || %item.getGroup().getName() $= "MissionCleanup") && %item.team == %team && !%item.selfPower && !%item.relayPowered)
               {
                    %obj.relayPowerGroup.add(%item);
                    %genGroup.add(%item);

                    %item.selfPower = "";
                    %item.dgenSelfPowered = "";
                    %item.relayPowered = true;
                    %item.relaySet = %obj.relayPowerGroup;

                    // Act as a generator if there are no generators on the map...
//                    if(%noGenMap)
//                    {
//                         %item.setSelfPowered();
//                         %item.setRechargeRate(%item.getDatablock().rechargeRate);
//                    }

                    // We got something, update the powergroup
                    %change = true;
               }

               %item = containerSearchNext();
          }

          // Update power real-time
          if(%change && !%noGenMap)
               %genGroup.powerInit();
     }

     schedule(1000, %obj, powerRelayNodeUpdate, %obj);
}

function powerRelayNodeShutdown(%obj)
{
     if(%obj.relayPowerGroup)
     {
          %count = %obj.relayPowerGroup.getCount();

          if(%count != -1)
          {
               for(%i = 0; %i < %count; %i++)
               {
                    %int = %obj.relayPowerGroup.getObject(%i);

                    if(!isObject(%int))
                    {
                         %obj.relayPowerGroup.remove(%int);
                         continue;
                    }

//                    echo("removing item" SPC %int);
                    %int.relayPowered = false;
                    %int.clearPower();
                    %int.powerCheck(0);
                    %int.relaySet = "";
                    %obj.relayPowerGroup.remove(%int);
                    MissionCleanup.add(%int);
               }
          }

          %obj.relayPowerGroup.delete();
     }
}

function feedbackDED( %obj , %dmg )	// +[soph]
{
  // InitContainerRadiusSearch(%obj.getPosition(), 325, $TypeMasks::StaticShapeObjectType);
  // while((%int = ContainerSearchNext()) != 0)
  // {
  //    if(isObject(%int) && %int != %obj && %int.isDEDevice)
  //       %obj.linkedShieldSource = %int;
  //       break;
  // }

   if( %obj.linkedShieldSource && isObject( %obj.linkedShieldSource ) && %dmg > 0 )
      if( %obj.DEFeedbackPercent > 0 )
      {
         %obj.linkedShieldSource.applyDamage( %dmg * %obj.DEFeedbackPercent );
         %obj.DEFeedbackPercent = 0;
      }
}					// +[/soph]

//====================================== Deployable Teleport Pads
$TeamDeployableMax[TelePack] = 2;

datablock StaticShapeData(TelePad) : StaticShapeDamageProfile
{
   shapeFile = "nexuscap.dts";   // teamlogo_projector
   maxDamage = 3.5;
   destroyedLevel = 3.5;
   disabledLevel = 3.5;
   explosion = DeployablesExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// $TypeMasks::StaticShapeObjectType; -soph
   
   deployedObject = true;

   targetNameTag = 'Teleport';
   targetTypeTag = 'Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "TelePack";
};

datablock StaticShapeData(TelePadCap) : StaticShapeDamageProfile
{
   shapeFile = "nexuscap.dts";   // teamlogo_projector
   maxDamage = 3.5;
   destroyedLevel = 3.5;
   disabledLevel = 3.5;
   explosion = DeployablesExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// $TypeMasks::StaticShapeObjectType; -soph

   deployedObject = true;

   targetNameTag = 'Teleport';
   targetTypeTag = 'Pad';

    debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "TelePack";
};

datablock StaticShapeData(TelePadBeam) : StaticShapeDamageProfile
{
   catagory = "Objectives";
   shapefile = "nexus_effect.dts";
   mass = 10;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   heatSignature = 0;

   cmdCategory = "DSupport";
   cmdIcon = CMDSensorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_sensor_grey";
   targetNameTag = 'Teleporter';
   targetTypeTag = 'Pad';

   undeployPack = "TelePack";
};

datablock ShapeBaseImageData(TelePackImage)
{
   mass = 10;
   emap = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = TelePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = TelePad;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 360;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(TelePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "TelePackImage";
   pickUpName = "a deployable teleport pack";
   heatSignature = 0;

   emap = true;
};

function TelePackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);
   %rot = %item.getInitialRotation(%plyr);

   %trans = %item.surfacePt SPC "0 1 0 0";
   %deplObj = addMultiObjectManager(%plyr, %trans);
   %teleBase = addObjectV(%plyr, "TelePad", vectorAdd(%item.surfacePt, "0 0 .5"), "0 0 -1", %deplObj, "2 2 2");
   %teleCap = addObject(%plyr, "TelePadCap", vectorAdd(%item.surfacePt, "0 0 5.5") SPC "0 1 0 0", %deplObj, "2 2 2");
   %teleBeam = addObject(%plyr, "TelePadBeam", %item.surfacePt SPC "0 1 0 0", %deplObj, "2 2 0.75");

   %teleBase.isTele = true;
   %teleBase.playThread(1, "ambient");
   %teleCap.playThread(1, "ambient");
   %teleBeam.playThread(1, "ambient");

   %teleBase.cap = %teleCap;
   %teleBase.beam = %teleBeam;

   %deplObj.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, "TelePack"]++;     // just fine

   detectDeployingOn(%deplObj, %item.surfacePt);
}

function teleCalSec(%obj)
{
    if(isObject(%obj))
    {
        if(%obj.teleCalTime > 0)
        {
            %obj.teleCalTime--;
            schedule(1000, 0, "teleCalSec", %obj);
        }
        else teleCalibrate(%obj, 0);
    }
}

function teleFadeFX(%obj, %time)
{
   %obj.startFade(250, 0, true);
   %obj.startFade(250, %time-2000, false);
//   %obj.startFade(1000, 0, true);
//   %obj.startFade(%time, 1000, false);
}

function teleCalibrate(%obj, %time)
{
    if(isObject(%obj))
    {
        if(!%obj.isDisabled())
        {
            %obj.setDamageState(Disabled);
            %obj.teleCalTime = mFloor(%time / 1000);
            teleCalSec(%obj);
        }
        else
        {
            %obj.setDamageState(Enabled);
            %obj.playThread(0, "ambient");
            %obj.cap.playThread(0, "ambient");
            %obj.beam.playThread(0, "ambient");
            %obj.teleCalibrating = false;
        }
    }
}

function checkForTele(%obj, %col, %range)
{
   InitContainerRadiusSearch(%obj.getPosition(), %range, $TypeMasks::StaticShapeObjectType);

   while((%pad = ContainerSearchNext()) != 0)
   {
      if(%pad.isTele)
         if(%pad != %obj)
            if(%pad.team == %obj.team)
               return %pad;

      else continue;
   }
   return false;
}

function finishTele(%pad1, %pad2, %pl, %dist, %time)
{
      ejectFlag(%pl);
      %pl.setMoveState(false);
      %trans = %pad2.getTransform();
      %pl.setTransform(%trans);
      %pl.teleportEndFX();
      %pad2.play3d(UnTeleportSound);
      %pad1.playThread(0, "transition");
      %pad2.playThread(0, "transition");
      %pad1.cap.playThread(0, "transition");
      %pad2.cap.playThread(0, "transition");
      %pad1.beam.playThread(0, "transition");
      %pad2.beam.playThread(0, "transition");
      %time = (%dist / 35) * 0.5 * 1000;
      teleCalibrate(%pad2, %time);
      schedule(1100, %pl, "checkCloakOff", %pl);

      if(%time > 4000)
      {
         moveObjectInSteps(%pad1.cap, vectorAdd(%pad1.getPosition(), "0 0 -3"), %pad1.cap.oldPos, 10, 100);
         moveObjectInSteps(%pad2.cap, vectorAdd(%pad2.getPosition(), "0 0 -3"), %pad2.cap.oldPos, 10, 100);
      }
}

function startTele(%pad1, %pad2, %pl, %dist)
{
      %pl.setMoveState(true);
      %pl.teleportStartFX();
      %pad1.play3d(TeleportSound);
      %pad1.playThread(0, "flash");
      %pad2.playThread(0, "flash");
      %pad1.cap.playThread(0, "flash");
      %pad2.cap.playThread(0, "flash");
      %pad1.beam.playThread(0, "transition");
      %pad2.beam.playThread(0, "transition");
      %time = (%dist / 35) * 0.5 * 1000;

      if(%time > 5000)
      {
         schedule(4000, 0, teleFadeFX, %pad1.beam, mFloor(%time));
         schedule(4000, 0, teleFadeFX, %pad2.beam, mFloor(%time));
      }

      if(%time > 4000)
      {
         %pad1.cap.oldPos = %pad1.cap.getPosition();
         %pad2.cap.oldPos = %pad2.cap.getPosition();
         moveObjectInSteps(%pad1.cap, %pad1.cap.oldPos,  vectorAdd(%pad1.getPosition(), "0 0 -3"), 10, 100);
         moveObjectInSteps(%pad2.cap, %pad2.cap.oldPos,  vectorAdd(%pad2.getPosition(), "0 0 -3"), 10, 100);
      }

      teleCalibrate(%pad1, %time);
      schedule(2000, 0, "finishTele", %pad1, %pad2, %pl, %dist, %time);
}

function TelePadTimeout(%obj)
{
   if(isObject(%obj))
      %obj.telePadCollTime = 0;
}

function TelePad::onCollision(%data, %obj, %col)
{
     if(%col.getDataBlock().className !$= "Armor" || %col.getState() $= "Dead")
          return;

     if(%col.telePadCollTime || %col.isTacticalMech)
        return;

//     else if(%obj.team != %col.client.team)
//     {
//          messageClient(%col.client, 'MsgNoUseEnemyTeles', '\c2As much as you probably want to, you cant use enemy teleporters!');
//          return;
//     }
     else if(%obj.isDisabled())
     {
          messageClient(%col.client, 'MsgTeleRecharge', '\c2Teleporter system is re-calibrating, %1 seconds', %obj.teleCalTime);
          return;
     }
     else
     {
        %pad = checkForTele(%obj, %col, 10000);   // /me special script for teleporting :)
        if(%pad && !%obj.isDisabled()) // && %obj.isPowered()) // final check
        {
           %pos = %obj.getWorldBoxCenter();
           %col.setVelocity("0 0 0");
           %rot = getWords(%col.getTransform(),3, 6);
           %col.setTransform(getWord(%pos,0) @ " " @ getWord(%pos,1) @ " " @ getWord(%pos,2)+0.8 @ " " @ %rot);//center player on object
           %col.setVelocity("0 0 0");
           %dist = vectorDist(%obj.getPosition(), %pad.getPosition());

           startTele(%obj, %pad, %col, %dist);
           %col.telePadCollTime = 12000;
           schedule(%col.telePadCollTime, 0, TelePadTimeout, %col);
           return;
        }
        else if(%obj.isDisabled())
        {
           messageClient(%col.client, 'MsgTeleRecharge', '\c2Teleporter system is re-calibrating, %1 seconds', %obj.teleCalTime);
           return;
        }
//        else if(!%obj.isPowered())
//        {
//           messageClient(%col.client, 'MsgTelePower', '\c2Teleporter is not powered.', %obj.teleCalTime);
//           return;
//        }
        else
        {
           messageClient(%col.client, 'Msg2ndTeleNoExist', '\c2Second telepad has not been placed.');
           return;
        }
    }

   %col.telePadCollTime = 3000;
      schedule(%col.telePadCollTime, 0, "TelePadTimeout", %col);
}

function TelePad::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   $TeamDeployedCount[%obj.team, TelePack]--;
   feedbackDED( %obj , 0.1 );		// +soph
   %obj.schedule(500, "delete");
//   %obj.cap.setDamageState(Destroyed);
   %obj.beam.delete();
}

function TelePadCap::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(500, "delete");
//   %obj.base.setDamageState(Destroyed);
}

function TelePad::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//====================================== Deployable Inventory Station
$TeamDeployableMax[DeployableInvHumanPack] = 3;

datablock ShapeBaseImageData(DeployableInvHumanImage) : StaticShapeDamageProfile
{
   mass = 10;

   shapeFile = "pack_deploy_inventory.dts";
   item = DeployableInvHumanPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = StationInventory;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(DeployableInvHumanPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "DeployableInvHumanImage";
   pickUpName = "a deployable inventory station";
   heatSignature = 0;

   emap = true;
};

function DeployableInvHumanPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function DeployableInvHumanImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(4))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function DeployableInvHumanImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %trans = %item.surfacePt SPC %rot;
   %deplObj = addStation(%plyr, %trans, 0, true);

   findFriendlyGenInArea(%deplObj, 250);

   %deplObj.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, "DeployableInvHumanPack"]++;     // just fine

   detectDeployingOn(%deplObj, %item.surfacePt);
}

function DeployableInvHumanImage::testSlopeTooGreat(%item)
{
   if (%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > 45;
}

function StationInventory::checkDeployPos(%deplObj, %norm) // --- (ST)
{
   return  getTerrainAngle(%norm) < 30;
}

//package sInvOverload
//{

function StationInventory::onDestroyed(%this, %obj, %prevState)
{
   if(%obj.fake)
   {
        Parent::onDestroyed(%this, %obj, %prevState);
        %obj.schedule(500, "delete");

        if(isObject(%obj.trigger))
           %obj.trigger.delete();

          if(!%obj.parent)
          {
             $TeamDeployedCount[%obj.team, "DeployableInvHumanPack"]--;
             feedbackDED( %obj , 0.2 );		// +soph
          }
//          else
//            if(isObject(%obj.parent))
//               %obj.parent.setDamageState(Destroyed);

//           %obj.parent.setDamageState(Destroyed);
   }
   else
   {
        Parent::onDestroyed(%this, %obj, %prevState);
        feedbackDED( %obj , 0.3 );		// +soph
   }
}

//====================================== Deployable Forcefield
$TeamDeployableMax[ShieldGeneratorPack]   = 16;

$FF::MaxConnections = 2;
$FF::CheckHeight = 1000; // --- 50 (ST)

datablock AudioProfile(TelePadBeamSound)
{
   filename    = "fx/powered/nexus_deny.WAV";
   description = AudioExplosion3d;
   preload = true;
};

function ShieldBeaconPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function ShieldGeneratorDeployableImage::testNoTerrainFound(%item)
{
     return false;   // HAK!!
}

function ShieldGeneratorDeployedBase::checkDeployPos(%item) // --- (ST)
{
   return true;
}

function ShieldGeneratorDeployableImage::onInventory(%this,%player,%value)
{
   // %this = Sensor pack datablock
   // %player = player
   // %value = 1 if gaining a pack, 0 if losing a pack

   if(%player.getClassName() $= "Player")
   {
      if(%value)
      {
         // player picked up or bought a motion sensor pack
         %player.deploySensors = 2;
         %player.client.updateSensorPackText(%player.deploySensors);
      }
      else
      {
         // player dropped or sold a motion sensor pack
         if(%player.throwSensorPack)
         {
            // player threw the pack
            %player.throwSensorPack = 0;
            // everything handled in ::onThrow above
         }
         else
         {
            //the pack was sold at an inventory station, or unmounted because the player
            // used all the sensors
            %player.deploySensors = 0;
            %player.client.updateSensorPackText(%player.deploySensors);
         }
      }
   }
   Pack::onInventory(%this,%player,%value);
}

function ShieldGeneratorPack::onInventory(%this,%player,%value)
{
   // %this = Sensor pack datablock
   // %player = player
   // %value = 1 if gaining a pack, 0 if losing a pack

   if(%player.getClassName() $= "Player")
   {
      if(%value)
      {
         // player picked up or bought a motion sensor pack
         %player.deploySensors = 2;
         %player.client.updateSensorPackText(%player.deploySensors);
      }
      else
      {
         // player dropped or sold a motion sensor pack
         if(%player.throwSensorPack)
         {
            // player threw the pack
            %player.throwSensorPack = 0;
            // everything handled in ::onThrow above
         }
         else
         {
            //the pack was sold at an inventory station, or unmounted because the player
            // used all the sensors
            %player.deploySensors = 0;
            %player.client.updateSensorPackText(%player.deploySensors);
         }
      }
   }
   Pack::onInventory(%this,%player,%value);
}

function ShieldGeneratorDeployedBase::onInventory(%this,%player,%value)
{
   // %this = Sensor pack datablock
   // %player = player
   // %value = 1 if gaining a pack, 0 if losing a pack

   if(%player.getClassName() $= "Player")
   {
      if(%value)
      {
         // player picked up or bought a motion sensor pack
         %player.deploySensors = 2;
         %player.client.updateSensorPackText(%player.deploySensors);
      }
      else
      {
         // player dropped or sold a motion sensor pack
         if(%player.throwSensorPack)
         {
            // player threw the pack
            %player.throwSensorPack = 0;
            // everything handled in ::onThrow above
         }
         else
         {
            //the pack was sold at an inventory station, or unmounted because the player
            // used all the sensors
            %player.deploySensors = 0;
            %player.client.updateSensorPackText(%player.deploySensors);
         }
      }
   }
   Pack::onInventory(%this,%player,%value);
}

function deleteFF(%obj, %i)
{
   %shield = %obj.shield[%i];

  if(isObject(%shield))
  {
      %shield.setPosition("-10000 -10000 -10000");

      if(isObject(%shield.pz))
         %shield.pz.setPosition(%shield.getPosition());

      %shield.getDatablock().losePower(%shield);
      %shield.delete();
  }
}

function ShieldGeneratorDeployedBase::onEndSequence(%data, %obj, %thread)
{
//   Parent::onEndSequence(%data, %obj, %thread);
}

function ShieldGeneratorDeployedBase::onDamage(%this, %obj)
{
     Parent::onDamage(%this, %obj);

     if(isObject(%obj.cap))
          %obj.cap.setDamageLevel(%obj.getDamageLevel());
//     else
//         %obj.setDamageState(Destroyed);

     if(isObject(%obj.base))
          %obj.base.setDamageLevel(%obj.getDamageLevel());
//     else
//          %obj.setDamageState(Destroyed);
}

function ShieldGeneratorDeployedBase::onDestroyed(%this, %obj, %prevState)
{
Parent::onDestroyed(%this, %obj, %prevState);

for(%i = 0; %i < $FF::maxConnections; %i++)
      deleteFF(%obj, %i);

   %shield = %obj.beam;

  if(isObject(%shield))
  {
      %shield.setPosition("-10000 -10000 -10000");

      if(isObject(%shield.pz))
         %shield.pz.setPosition(%shield.getPosition());

      %shield.getDatablock().losePower(%shield);
      %shield.delete();
  }
  if(%obj.cap !$= "") // --- if obj is a base to prevent double decrimenting (ST)
  {
      $TeamDeployedCount[%obj.team, ShieldGeneratorPack]--;
      feedbackDED( %obj , 0.5 );		// +soph
      %obj.cap.setDamageState(Destroyed);
  }

   %obj.schedule(250, "delete");
}

//function ShieldGeneratorPack::onAdd(%this)
//{
//   Parent::onAdd(%this);
//%this.connected[1] = 0;
//%this.connected[2] = 0;
// %this.connections = 0;
//
//   // created to prevent console errors
//}

function ShieldGeneratorPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function ShieldGenUpdate(%obj)
{
   if(!isObject(%obj))
      return;

//   if(!%obj.isPowered())
//   {
//        for(%i = 0; %i < %obj.connections; %i++)
//        {
//           if(%obj.connected[%i] && !isObject(%obj.connected[%i]))
//           {
//              deleteFF(%obj, %i);
//              %obj.connected[%i] = "";
//              %obj.connections--;
//              %obj.playAudio(1, TelePadBeamSound);
//           }
//        }
//
//        schedule(2000, %obj, "ShieldGenUpdate", %obj);
//        return;
//   }

   for(%i = 0; %i < %obj.connections; %i++)
   {
      if(%obj.connected[%i] && !isObject(%obj.connected[%i]))
      {
         deleteFF(%obj, %i);
         %obj.connected[%i] = "";
         %obj.connections--;
         %obj.playAudio(1, TelePadBeamSound);
      }
   }

   %surfacePt = %obj.getPosition();
   %found = getClosestFF(%obj);

   if(%found)
   {
      %distance_to = %obj.distances[%found];
      if(%obj.connections > $FF::MaxConnections)
      {
         %farthest = getFarthestFF(%obj);
         if(%distance_to < %obj.distances[%farthest]) // there is a non connected one closer then our farthest connection..
         {
            for(%i = 0; %i <= %obj.connections; %i++) // destroy the one that is far away..
            {
               if(%obj.connected[%i] == %farthest)
               {
                  deleteFF(%obj, %i);
                  %obj.connected[%i] = "";
                  %obj.connections--;
                  %obj.playAudio(1, TelePadBeamSound);
               }
            }
         }
      }
      if(%obj.connections < $FF::maxConnections)
      {
         %fnd_pos = %found.getPosition();
         %target = setWord(%fnd_pos, 2, 0);
         %src = setWord(%surfacePt, 2, 0);
         %vec = VectorNormalize(VectorSub(%target, %src));
         %mask = $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType |
                 $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType |
                 $TypeMasks::TurretObjectType |//$TypeMasks::PlayerObjectType | --- Hope you're on my team LOL (ST)
                 $TypeMasks::StaticObjectType | $TypeMasks::TerrainObjectType;

         %res = containerRayCast(VectorAdd(%surfacePt, "0 0 1.5"), VectorAdd(%fnd_pos, "0 0 1.5"), %mask, 0);
         if(!%res)
         {
            %obj.connections++;
            %radian = mAcos(getWord(%vec, 1));
            %angle = (%radian / 0.0175);
            %newrot = " 0 0 1 " @ (270 - %angle);
//            %height = 6; --- (ST)

            if(isObject(%obj.shield[0]))
               %newshieldidx = 1;
            else
               %newshieldidx = 0;

            %src_z = getWord(%surfacePt, 2);
            %tar_z = getWord(%fnd_pos, 2);
//            if(%src_z > %tar_z)
//            %height -= (%src_z - %tar_z); --- (ST)

// --- Begin auto-height field-size/pos (ST)
            if(%src_z >= %tar_z) // --- Who's higher =) (ST)
            {
               %diff = %src_z - %tar_z;
               %up = 0.4;
               if((%src_z + %obj.height) > (%tar_z + %found.height))
               {
                  %shortest = %found.height;
                  %fieldHeight = (%tar_z + %found.height) - (%src_z + 0.5);
               }
               else
               {
                  %shortest = %obj.height;
                  %fieldHeight = (%src_z + %obj.height) - (%src_z + 0.5);
               }
            }
            else
            {
               %diff = %tar_z - %src_z;
               %up = %diff + 0.4;
               if((%src_z + %obj.height) > (%tar_z + %found.height))
               {
                  %shortest = %found.height;
                  %fieldHeight = (%tar_z + %found.height) - (%tar_z + 0.5);
               }
               else
               {
                  %shortest = %obj.height;
                  %fieldHeight = (%src_z + %obj.height) - (%tar_z + 0.5);
               }
            }

            if(%fieldHeight > %shortest / 2) // --- Min. height of potential forcefield (ST)
            {
// --- End auto-height field-size/pos (ST)

               %obj.playAudio(1, TelePadBeamSound);
               %obj.shield[%newshieldidx] = new forceFieldBare()
               {
                  position = vectorAdd(%surfacePt, "0 0 "@ %up);//0.3"); --- (ST)
                  rotation = %newrot;
                  scale = %distance_to @ " .05" SPC %fieldHeight;//%height; --- (ST)
                  dataBlock = "DeployedTeamSlowFieldBareGreen";
                  team = %obj.team;
               };

               %obj.shield[%newshieldidx].deployBase = %obj;
               %obj.shield[%newshieldidx].isFF = true;

//               %obj.shield[%newshieldidx].getDatablock().gainPower(%obj.shield[%newshieldidx]);
               %obj.shield[%newshieldidx].target = createTarget(%obj.shield[%newshieldidx], "Force Field", "", "", "", 0, 0);
               setTargetSensorGroup(%obj.shield[%newshieldidx].getTarget(), %obj.team);

               MissionCleanup.add(%obj.shield[%newshieldidx]);
               %obj.connected[%newshieldidx] = %found;

               // Add to parent's power group
               %found.getGroup().add(%obj.shield[%newshieldidx]);
            }// (ST)
         }
      }
   }
   schedule(1000, %obj, "ShieldGenUpdate", %obj);
}

function getClosestFF(%obj)
{
   if(!isObject(%obj))
      return 0;

   %surfacePt = posFromTransform(%obj.getTransform());
   InitContainerRadiusSearch(VectorAdd(%surfacePt, "0 0 0.3"), 24, $TypeMasks::StaticShapeObjectType);
   %found = containerSearchNext();
%obj.closest = 0;

  %nearestDist = 2000;
while(%found)
   {
// --- Moved (ST)
//   %nearestDist = 2000;

      %foundName = %found.getDataBlock().getName();
   if(%foundName $= "ShieldGeneratorDeployedBase" &&
   %found != %obj &&
   %found.team == %obj.team &&
   %found.FieldBasePoint &&
   %found.connections < 2 &&
   %obj.connected[0] != %found &&
   %obj.connected[1] != %found &&
   %found.connected[0] != %obj &&
   %found.connected[1] != %obj)
   {

%obj_pos = %obj.getPosition();
%fnd_pos = %found.getPosition();

// ============================= Ignore all non-visible beacons
// --- Don't connect if all of these raycasts are interrupted (ST)
//  o___o
//   \ /
//    X     =)
//   /_\
//  o   0
         %coverage = calcExplosionCoverage(%obj_pos, %found, %mask);
         if(%coverage < 1)
            %coverage = calcExplosionCoverage(VectorAdd(%obj_pos, "0 0 0.35"), %found.cap, %mask);
         if(%coverage < 1)
            %coverage = calcExplosionCoverage(VectorAdd(%obj_pos, "0 0 "@ %obj.height), %found, %mask);
         if(%coverage < 1)
            %coverage = calcExplosionCoverage(VectorAdd(%obj_pos, "0 0 "@ %obj.height), %found.cap, %mask);
         if(%coverage == 1)
         {
   %obj_x = getWord(%obj_pos, 0);
   %obj_y = getWord(%obj_pos, 1);
   %obj_z = getWord(%obj_pos, 2);
   %fnd_x = getWord(%fnd_pos, 0);
   %fnd_y = getWord(%fnd_pos, 1);
   %fnd_z = getWord(%fnd_pos, 2);

   %target = setWord(%fnd_pos, 2, 0);
    %src = setWord(%obj_pos, 2, 0);

   if((%src > %target) && ((%fnd_z - %obj_z) < 0.3))
   {
   %distance_to = VectorDist(%src, %target);

   %obj.distances[%found] = %distance_to;
   if(%distance_to < %nearestDist)
   {
                  %nearestDist = %distance_to;
   %obj.closest = %found;
   }
            }
} // ...and, of course (ST)
}
  %found = containerSearchNext();
}
return %obj.closest;
}

function getFarthestFF(%obj)
{
   if(!isObject(%obj))
      return -1;

%farthest = -1;

for(%i = 1; %i <= %obj.connections; %i ++)
{
if(%obj.distances[%obj.connected[%i]] > %farthest)
{
%farthest = %obj.distances[%obj.connected[%i]];
%ff = %obj.connected[%i];
}
}
return %ff;
}


function ShieldGeneratorDeployableImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(4))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isNearbyEnemyBase(200))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   InitContainerRadiusSearch( %item.surfacePt , 10 , $TypeMasks::StaticShapeObjectType ) ;	// +[soph]
   %count = 0 ;											// +
   while( ( %found = containerSearchNext() ) != 0 )						// +
   {												// +
      if( %found.getDataBlock() == %item )							// +
      {												// +
         %distance = getDistance2D( %item.surfacePt , %found.getWorldBoxCenter() ) ;		// +
         if( %distance > 3 )									// +
         {											// +
            $EDC::Reason = "You are trying to deploy too close to another field emitter." ;	// +
            return true ;									// +
         }											// +
         else											// +
            %count++ ;										// +
      }												// +
   }												// +
   if( %count > 4 )										// +
   {												// +
      $EDC::Reason = "There are too many field emitters in this area." ;			// +
      return true ;										// +
   }												// +[/soph]

   $EDC::Reason = "";
   return false;
}

function ShieldGeneratorDeployableImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.deploySensors--;
   %plyr.client.updateSensorPackText(%plyr.deploySensors);

   if(%plyr.deploySensors <= 0)
   {
      // take the deployable off the player's back and out of inventory
      %plyr.unmountImage(%slot);
      %plyr.decInventory(%item.item, 1);
   }

   // create the actual deployable
   %rot = %item.getInitialRotation(%plyr);
// --- Not needed, will always be beacon (ST)
//   if(%item.deployed.className $= "DeployedTurret")
//      %className = "Turret";
//   else
//      %className = "StaticShape";

   %deplObj = new ("StaticShape") () { //(%className)() {
      dataBlock = %item.deployed;
   };


   // set orientation
// --- Not needed, will always be beacon (ST)
//   if(%className $= "Turret")
//      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
//   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   // set the recharge rate right away
//   if(%deplObj.getDatablock().rechargeRate)
//      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   // set team, owner, and handle
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   // set the sensor group if it needs one
   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   // place the deployable in the MissionCleanup/Deployables group (AI reasons)
//   addToDeployGroup(%deplObj);
   MissionCleanup.add(%deplObj);

   //let the AI know as well...
   AIDeployObject(%plyr.client, %deplObj);

   // play the deploy sound
   serverPlay3D(%item.deploySound, %deplObj.getTransform());

   // increment the team count for this deployed object
   $TeamDeployedCount[%plyr.team, %item.item]++;
//   %deplObj.deploy();
   %deplObj.playThread($DeployThread, "deploy");

// --- Begin auto-height adjusting (ST)
   %pos = %deplObj.getPosition();
   %height = $FF::CheckHeight;
   %maxHeight = VectorAdd(%pos, "0 0 "@ %height);
   %obstructMask = $TypeMasks::InteriorObjectType | //$TypeMasks::TerrainObjectType | --- =( der... (ST)
                   $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType;

   %obstruction = ContainerRayCast(%pos, %maxHeight, %obstructMask, %deplObj);
   if(%obstruction)
      %height = getWord(%obstruction, 3) - getWord(%pos, 2) - 0.4;
   else // --- outdoors (ST)
      %height = 7;

   %capPos = VectorAdd(%pos, "0 0 "@ %height);
// --- End auto-height adjusting (ST)
   %capObj = new ("StaticShape")() {
      dataBlock = ShieldGeneratorDeployedBase;
   };
   %deplObj.FieldBasePoint = true;
   %deplObj.cap = %capObj;
   %deplObj.height = %height; // --- (ST)
   %capObj.base = %deplObj;
   %capObj.team = %deplObj.team; // --- (ST)
   addAttachment(%deplObj, %capObj);
//   addAttachment(%capObj, %deplObj);

   %deplObj.connections = 0;
   %deplObj.connected[0] = 0;
   %deplObj.connected[1] = 0;

//   %rot = vectorAdd(%rot, "0 0 1");
   %cappoint = vectorAdd(%item.surfacePt, "0 0 "@ %height);//6.2"); (ST)
   %capObj.setTransform(%cappoint SPC %rot);
   setTargetSensorGroup(%capObj.getTarget(), %deplObj.team);
   %capObj.playThread($AmbientThread, "ambient");
   %deplObj.playThread($AmbientThread, "ambient");

   %deplObj.beam = new forceFieldBare()
   {
      position = VectorAdd(%item.surfacePt, "-0.0375 -0.0375 0");// --- centering (ST)
      rotation = %rot;//"0 0 0 0"; (ST)
      scale = "0.075 0.075 "@ %height;//6.4"; (ST)
      dataBlock = "DeployedShieldBeam";
      team = %deplObj.team;
   };
   %deplObj.beam.setSelfPowered();

   %deplObj.beam.isFF = true;
   %deplObj.beam.deployBase = %capObj;

   MissionCleanup.add(%deplObj.beam);
   schedule(1000, %deplObj, "ShieldGenUpdate", %deplObj);

   detectDeployingOn(%deplObj, %item.surfacePt);

   findFriendlyGenInArea(%deplObj, 250);

   if($DebugMode)
   {
      if(%obstruction)
         echo("ffBeacon obstruction = "@ firstWord(%obstruction).getClassName());
      else
         echo("ffBeacon obstruction = None");

      echo("ffBeacon pos = "@ %pos);
      echo("ffBeacon height = "@ %height);
   }
}

function DeployedTeamSlowFieldBareGreen::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   %base = %targetObject.deployBase;
   %base.getDataBlock().damageObject(%base, %sourceObject, %position, %amount, %damageType);

   %otherBase = %base.connected[0];
   %otherBase = %base.connected[1];

   if (%otherBase == %targetObject)
      %other.getDataBlock().damageObject(%other, %sourceObject, %position, %amount, %damageType);
   else
   {
      %other = %base.connected[1];
      %other.getDataBlock().damageObject(%other, %sourceObject, %position, %amount, %damageType);
   }
}

datablock ShapeBaseImageData(ShieldGeneratorDeployableImage)
{
   mass = 15;
   emap = true;

   shapeFile = "pack_deploy_sensor_motion.dts";
   item = ShieldGeneratorPack;
   mountPoint = 1;
   offset = "0 0 0";

   deployed = ShieldGeneratorDeployedBase;
   heatSignature = 0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;
};

datablock ItemData(ShieldGeneratorPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_sensor_motion.dts";
   mass = 4.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = false;
   image = "ShieldGeneratorDeployableImage";
   pickUpName = "a deployable array beacon";
   heatSignature = 0;

   emap = true;
};

datablock SensorData(ShieldGeneratorBaseSensorObj)
{
detects = false;
detectsUsingLOS = true;
detectsPassiveJammed = false;
detectsActiveJammed = false;
detectsCloaked = false;
detectionPings = true;
detectRadius = 10;
};

datablock StaticShapeData(ShieldGeneratorDeployedBase)
{
   className = "Station";
   catagory = "Deployables";
   //shapefile = "solarpanel.dts";
   shapefile = "deploy_sensor_motion.dts";
   rechargeRate = 0.31;

   needsNoPower = true;
   mass = 2.0;
   maxDamage = 14;	// = 24; -[soph]
   destroyedLevel = 14;	// = 24;
   disabledLevel = 14;	// = 24; -[/soph]
   repairRate = 0;
explosion= SmallTurretExplosion;
expDmgRadius = 2.0;
expDamage = 0.8;
expImpulse = 2000.0;

deployedObject = true;

   energyPerDamagePoint = 50;
   maxEnergy = 50;

  humSound = SensorHumSound;
   heatSignature = 0;
pausePowerThread = true;

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;

   cmdIcon = CMDSwitchIcon;
   cmdCategory = "DSupport";
   cmdMiniIconName = "commander/MiniIcons/com_switch_grey";
   targetNameTag = 'Deployable';
   targetTypeTag = 'Forcefield Beacon';

   sensorData = ShieldGeneratorBaseSensorObj;
   sensorRadius = ShieldGeneratorBaseSensorObj.detectRadius;
   sensorColor = "0 212 45";

   firstPersonOnly = true;

   damageScale[$DamageType::Sniper]       = 0.2;

   //lightOnlyStatic = true;
   lightType = "PulsingLight";
   lightColor = "0 1 0 1";
   lightTime = 1200;
   lightRadius = 6;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;

   undeployPack = "ShieldGeneratorPack";
};

datablock ForceFieldBareData(DeployedTeamSlowFieldBareGreen): StaticShapeDamageProfile
{
   fadeMS           = 1000;
   baseTranslucency = 0.2;
   powerOffTranslucency = 0.0;
   dynamicType = $TypeMasks::DamagableItemObjectType;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.28 0.89 0.31";
   powerOffColor    = "0.0 0.0 0.0";

   cmdCategory = "DSupport";

   targetNameTag = 'Force Field';

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   damageScale[$DamageType::Sniper]       = 0.2;

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
   isFF = true;
};

datablock ForceFieldBareData(DeployedShieldBeam)
{
   fadeMS           = 1000;
   baseTranslucency = 0.3;
   powerOffTranslucency = 0.3;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.28 0.28 0.99";
   powerOffColor    = "0 0 0";
   targetTypeTag    = 'Force Field';

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
   isFF = true;
};

//====================================== Deployable Shield Beacon
$TeamDeployableMax[ShieldBeaconPack] = 10;

//datablock StaticShapeData(ShieldBeacon) : StaticShapeDamageProfile	// -[soph]
//{									// - removed
////   className = "StaticShape";					// -
////   dynamicType = $TypeMasks::StaticShapeObjectType;			// -
//   shapefile = "deploy_sensor_motion.dts";				// -
//   rechargeRate = 0.31;						// -
//									// -
//   needsNoPower = true;						// -
//   mass = 2.0;							// -
//   maxDamage = 15;							// -
//   destroyedLevel = 15;						// -
//   disabledLevel = 15;						// -
//   repairRate = 0;							// -
//	explosion	= SmallTurretExplosion;				// -
//	expDmgRadius = 10.0;						// -
//	expDamage = 1.0;						// -
//	expImpulse = 2000.0;						// -
//									// -
//	deployedObject = true;						// -
//									// -
//   energyPerDamagePoint = 50;						// -
//   maxEnergy = 50;							// -
//									// -
//  	humSound = SensorHumSound;					// -
//   heatSignature = 0;							// -
//									// -
//   debrisShapeName = "debris_generic_small.dts";			// -
//   debris = DeployableDebris;						// -
//									// -
//   targetNameTag = 'Deployable';					// -
//   targetTypeTag = 'Shield Beacon';					// -
//									// -
//   cmdIcon = CMDSwitchIcon;						// -
//   cmdCategory = "DSupport";						// -
//   cmdMiniIconName = "commander/MiniIcons/com_switch_grey";		// -
//   firstPersonOnly = true;						// -
//									// -
//   minDeployDis       = 0.5;						// -
//   maxDeployDis       = 5.0;						// -
//									// -
//   undeployPack = "ShieldBeaconPack";					// -
//};									// -
									// -
//datablock ShapeBaseImageData(ShieldBeaconPackImage)			// -
//{									// -
//   mass = 20;								// -
//									// -
//   shapeFile = "deploy_sensor_motion.dts";				// -
//   item = ShieldBeaconPack;						// -
//   mountPoint = 1;							// -
//   offset = "0 0 0";							// -
//   deployed = ShieldBeacon;						// -
//									// -
//   stateName[0] = "Idle";						// -
//   stateTransitionOnTriggerDown[0] = "Activate";			// -
//									// -
//   stateName[1] = "Activate";						// -
//   stateScript[1] = "onActivate";					// -
//   stateTransitionOnTriggerUp[1] = "Idle";				// -
//									// -
//   emap = true;							// -
//									// -
//   maxDepSlope = 180;							// -
//   deploySound = TurretDeploySound;					// -
//									// -
//   minDeployDis                       =  0.5;				// -
//   maxDeployDis                       =  5.0;  //meters from body	// -
//};									// -
									// -
//datablock ItemData(ShieldBeaconPack)					// -
//{									// -
//   className = Pack;							// -
//   catagory = "Deployables";						// -
//   shapeFile = "pack_deploy_turreti.dts";				// -
//   mass = 3.0;							// -
//   elasticity = 0.2;							// -
//   friction = 0.6;							// -
//   pickupRadius = 1;							// -
//   rotate = false;							// -
//   image = "ShieldBeaconPackImage";					// -
//   pickUpName = "a deployable shield beacon";				// -
//   heatSignature = 0;							// -
//									// -
//   emap = true;							// -
//};									// -[/soph]

function ShieldBeaconPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

$ShieldBDDist = 500;

function nodeShieldObjects(%obj)
{
   if(!isObject(%obj))
      return;

   %obj.nodeDead = false;
   %obj.attachedToSm = false;
   InitContainerRadiusSearch(%obj.getPosition(), 2, $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::StaticShapeObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(!isObject(%int.ownerShieldBeacon) && %int != %obj && !%int.isShieldBeacon && !%int.unbeaconable && !%int.isDEDevice && !%int.isRepulsor && %int.getDataBlock().getName() !$= "TurretDeployedOutdoor" && %int.getDataBlock().getName() !$= "MobileTurretBase" && %int.getDataBlock().getName() !$= "WarpBubble")
      {
         %int.playShieldEffect("0 0 1");
//         zapObject(%int);
         %int.ownerShieldBeacon = %obj;
         %obj.protectingObject = %int;
         addAttachment(%int, %obj);
         %obj.attachedToSm = true;
         break;
      }
   }

   if(isObject(%obj.owner) && !%obj.attachedToSm)
   {
         messageClient(%obj.owner, 'MsgSBNAO', '\c1This object cannot be beaconed or no attachment found.');
         %obj.schedule(2000, setDamageState, Destroyed); // suicide if no avaliable objects to protect
         return;
   }

   InitContainerRadiusSearch(%obj.getPosition(), $ShieldBDDist, $TypeMasks::StaticShapeObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(isObject(%int) && %int != %obj && %int.isDEDevice)
      {
         %obj.playShieldEffect("0 0 1");
         %int.playShieldEffect("0 0 1");
//         zapObjectRed(%obj);
//         zapObject(%int);
         %obj.linkedShieldSource = %int;
         addAttachment(%int, %obj);
         break;
      }
   }

   if(!isObject(%obj.linkedShieldSource))
   {
      if(isObject(%obj.owner))
         messageClient(%obj.owner, 'MsgSBMALF', '\c1Defense+ Device required in order for this device to function, must be deployed within %1m of one.', $ShieldBDDist);

      %obj.schedule(2000, setDamageState, Destroyed); // suicide if no avaliable objects to protect
      return;
   }

   if(!%obj.protectingObject)
      %obj.schedule(2000, setDamageState, Destroyed); // suicide if no avaliable objects to protect

   loopShieldTest(%obj);
}

function loopShieldTest(%obj)
{
     if(isObject(%obj.linkedShieldSource))
          schedule(1000, %obj, loopShieldTest, %obj);
     else
          %obj.schedule(2000, setDamageState, Destroyed);
}

function loopShieldObjects(%obj)
{
   if(!%obj.nodeDead)
   {
      if(!isObject(%obj.linkedShieldSource))
          %obj.setDamageState(Destroyed);

      schedule(500, %obj, loopShieldObjects, %obj);
   }
   else
        %obj.protectingObject.ownerShieldBeacon = "";
}

function ShieldBeaconPackImage::testObjectTooClose(%item, %plyr)
{
   return false;
}

function ShieldBeacon::checkDeployPos(%item)
{
   return true;
}

function ShieldBeaconPackImage::onDeploy(%item, %plyr, %slot)
{
     %plyr.unmountImage(%slot);
     %plyr.decInventory(%item.item, 1);

     %rot = %item.getInitialRotation(%plyr);

     %turret = new StaticShape()
     {
        dataBlock = %item.deployed;
     };
     MissionCleanup.add(%turret);
//     %turret.setTransform(%item.surfacePt SPC %rot);
     %turret.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
     %turret.setSelfPowered();          // unlock

     if(%turret.getDatablock().rechargeRate)
           %turret.setRechargeRate(%turret.getDatablock().rechargeRate);

     %turret.team = %plyr.client.Team;
     %turret.owner = %plyr.client;

     if(%turret.getTarget() != -1)
           setTargetSensorGroup(%turret.getTarget(), %plyr.client.team);

     addToDeployGroup(%turret);
     AIDeployObject(%plyr.client, %turret);
     serverPlay3D(%item.deploySound, %turret.getTransform());
     $TeamDeployedCount[%plyr.team, "ShieldBeaconPack"]++;     // just fine
     %turret.deploy();
     %turret.isShieldNode = true;
     %turret.isShieldBeacon = true;
//     %turret.isInvincible = true;
     nodeShieldObjects(%turret);

//   detectDeployingOn(%turret, %item.surfacePt);
}

function ShieldBeacon::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   $TeamDeployedCount[%obj.team, ShieldBeaconPack]--;
   %obj.nodeDead = true;
   %obj.protectingObject.ownerShieldBeacon = "";
   %obj.linkedShieldSource.applyDamage(1);
   %obj.schedule(250, "delete");
}

//====================================== Deployable Vehicle Pad
$TeamDeployableMax[VehiclePadPack] = 1;

datablock StaticShapeData(DeployableStationVehicle) : StaticShapeDamageProfile
{
   className = Station;
   catagory = "Stations";
   shapeFile = "vehicle_pad_station.dts";
   maxDamage = 3.60;		// = 1.20; -soph
   destroyedLevel = 3.60;	// = 1.20; -soph
   disabledLevel = 2.52;	// = 0.84; -soph
   explosion      = ShapeExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

	expDmgRadius = 10.0;
	expDamage = 0.4;
	expImpulse = 1500.0;
   dynamicType = $TypeMasks::StationObjectType;
	isShielded = true;
	energyPerDamagePoint = 33;
	maxEnergy = 250;
	rechargeRate = 0.31;
   humSound = StationVehicleHumSound;
	// don't let these be damaged in Siege missions

   cmdCategory = "DSupport";
   cmdIcon = CMDVehicleStationIcon;
   cmdMiniIconName = "commander/MiniIcons/com_vehicle_pad_inventory";
   targetNameTag = 'Deployable';
   targetTypeTag = 'Vehicle Station';

   debrisShapeName = "debris_generic.dts";
   debris = StationDebris;
};

datablock StaticShapeData(DeployableVehiclePad)
{
//   className = StaticShape;
//   dynamicType = $TypeMasks::DamageableItemObjectType;
   shapeFile = "vehicle_pad.dts";
   maxDamage = 7.5;
   destroyedLevel = 7.5;
   disabledLevel = 7.5;
   explosion      = ShapeExplosion;
	expDmgRadius = 10.0;
	expDamage = 0.4;
	expImpulse = 1500.0;
   rechargeRate = 0.05;
   targetTypeTag = 'Deployable Vehicle Pad';
};

function DeployableStationVehicle::createTrigger(%this, %obj)
{
   %trigger = new Trigger()
   {
      dataBlock = stationTrigger;
      polyhedron = "-0.75 0.75 0.0 1.5 0.0 0.0 0.0 -1.5 0.0 0.0 0.0 2.0";
   };
   MissionCleanup.add(%trigger);
   %trigger.setTransform(%obj.getTransform());
   %trigger.station = %obj;
   %obj.trigger = %trigger;
   %obj.mechTons = 0 ;			// +[soph]
   %obj.mechTonnageLoop = schedule( $MechStationTonnageSchedule , 0 , MechTonnageLoop , %obj ) ;
					// +[/soph]
}

function DeployableStationVehicle::stationReady(%data, %obj)
{
   // Make sure none of the other popup huds are active:
   messageClient( %obj.triggeredBy.client, 'CloseHud', "", 'scoreScreen' );
   messageClient( %obj.triggeredBy.client, 'CloseHud', "", 'inventoryScreen' );

   //Display the Vehicle Station GUI
   commandToClient(%obj.triggeredBy.client, 'StationVehicleShowHud');
}

function DeployableStationVehicle::stationFinished(%data, %obj)
{
   //Hide the Vehicle Station GUI
   if(!%obj.triggeredBy.isMounted())
      commandToClient(%obj.triggeredBy.client, 'StationVehicleHideHud');
   else
      commandToClient(%obj.triggeredBy.client, 'StationVehicleHideJustHud');
}

function DeployableStationVehicle::getSound(%data, %forward)
{
   if(%forward)
      return "StationVehicleAcitvateSound";
   else
      return "StationVehicleDeactivateSound";
}

function DeployableStationVehicle::setPlayersPosition(%data, %obj, %trigger, %colObj)
{
   %vel = getWords(%colObj.getVelocity(), 0, 1) @ " 0";
   if((VectorLen(%vel) < 22) && (%obj.triggeredBy != %colObj))
   {
      %client = %colObj.client ;	// +[soph]
      if( !%client.isAIControlled() )	// +
         messageClient( %client , 'MsgVehStationMechTonnage' , "This station has" SPC %obj.mechTons SPC "mech tonnage available.\nYour currently selected mech weighs" SPC %client.mechWeight SPC "." ) ;
					// +[/soph]
      %posXY = getWords(%trigger.getTransform(),0 ,1);
      %posZ = getWord(%trigger.getTransform(), 2);
      %rotZ =  getWord(%obj.getTransform(), 5);
      %angle =  getWord(%obj.getTransform(), 6);
	   %angle += 3.141592654;
      if(%angle > 6.283185308)
         %angle = %angle - 6.283185308;
      %colObj.setvelocity("0 0 0");
      %colObj.setTransform(%posXY @ " " @ %posZ + 0.2 @ " " @ "0 0 "  @ %rotZ @ " " @ %angle );//center player on object
      return true;
   }
   return false;
}

function DeployableVehiclePad::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %obj.destroyed = false;				// multiplication fix +soph
	%obj.ready = true;
   %obj.setRechargeRate(%obj.getDatablock().rechargeRate);
   %obj.mobileBaseVehicle = "Removed" ;			// +soph
   schedule( 20000 , 0 , AllowDeploaybleMPB , %obj ) ;	// +soph

   if($CurrentMissionType $= %obj.missionTypesList || %obj.missionTypesList $="")
      %this.schedule(0, "createStationVehicle", %obj);
}

function AllowDeploaybleMPB( %obj )			// +[soph]
{							// +
   if( isObject( %obj ) )				// +
      %obj.mobileBaseVehicle = 0 ;			// +
}							// +[/soph]

function DeployableVehiclePad::onEndSequence(%data, %obj, %thread)
{
   if(%thread == $ActivateThread)
   {
      %obj.ready = true;
      %obj.stopThread($ActivateThread);
   }
   Parent::onEndSequence(%data, %obj, %thread);
}

function DeployableVehiclePad::createStationVehicle(%data, %obj)
{
   // This code used to be called from StationVehiclePad::onAdd
   // This was changed so we can add the station to the mission group
   // so it gets powered properly and auto cleaned up at mission end.

   // Get the v-pads mission group so we can place the station in it.
   %group = %obj.getGroup();

   // Set the default transform based on the vehicle pads slot
   %xform = %obj.getSlotTransform(0);
   %position = getWords(%xform, 0, 2);
   %rotation = getWords(%xform, 3, 5);
   %angle = (getWord(%xform, 6) * 180) / 3.14159;

   // Place these parameter's in the v-pad datablock located in mis file.
   // If the mapper doesn't move the station, use the default location.
   if(%obj.stationPos $= "" || %obj.stationRot $= "")
   {
      %pos = %position;
      %rot = %rotation @ " " @ %angle;
   }
   else
   {
      %pos = %obj.stationPos;
      %rot = %obj.stationRot;
   }

   %sv = new StaticShape() {
	scale = "1 1 1";
      dataBlock = "DeployableStationVehicle"; //"DeployableVehicleStation";
	lockCount = "0";
	homingCount = "0";
	team = %obj.team;
      position = %pos;
      rotation = %rot;
   };

   // Add the station to the v-pads mission group for cleanup and power.
   %group.add(%sv);
   %sv.setPersistent(false); // set the station to not save.

   // Apparently called to early on mission load done, call it now.
//   %sv.getDataBlock().gainPower(%sv);

//   %sv.dgenAccessible = true;
//   %obj.dgenAccessible = true;

   // Create the trigger
   %sv.getDataBlock().createTrigger(%sv);
   %sv.pad = %obj;
   %obj.station = %sv;
   %sv.trigger.mainObj = %obj;
   %sv.trigger.disableObj = %sv;

   findFriendlyGenInArea(%sv, 250);
   findFriendlyGenInArea(%obj, 250);
//   %obj.parent.attachment[3] = %sv.trigger;
//   %obj.parent.attachment[4] = %sv;

   // Set the sensor group.
   if(%sv.getTarget() != -1)
      setTargetSensorGroup(%sv.getTarget(), %obj.team);

   %sv.isFake = true;

   %sv.vehicle[scoutVehicle] = true;
   %sv.vehicle[scoutFlyer] = true;
   %sv.vehicle[AssaultVehicle] = true;
   %sv.vehicle[AssaultFlyer] = true;
   %sv.vehicle[bomberFlyer] = true;
   %sv.vehicle[hapcFlyer] = true;
   %sv.vehicle[HoverBike] = true;
   %sv.vehicle[PyroXL] = true;
   %sv.vehicle[Wolfhound] = true;
   %sv.vehicle[Skyranger] = true;
   %sv.vehicle[ATV] = true;
   %sv.vehicle[tPOC] = true;
   %sv.vehicle[WarpFlyer] = true;
   %sv.vehicle[Explorer] = true;
}

function DeployableStationVehicle::onDestroyed(%data, %obj, %prevState) // stationObject: special case
{
   Parent::onDestroyed(%data, %obj, %prevState);
   %obj.pad.setDamageState(Destroyed);
   %obj.pad.parent.setDamageState(Destroyed);
   %obj.trigger.schedule(200, "delete");
   %obj.schedule(200, "delete");
}

function DeployableVehiclePad::onEndSequence(%data, %obj, %thread)
{
   if(%thread == $ActivateThread)
   {
      %obj.ready = true;
      %obj.stopThread($ActivateThread);
   }
   Parent::onEndSequence(%data, %obj, %thread);
}

function DeployableVehiclePad::gainPower(%data, %obj)
{
//   %obj.station.setSelfPowered();
   Parent::gainPower(%data, %obj);
}

function VehiclePadBase::gainPower(%data, %obj)
{
     // Dummy script
}

function DeployableVehiclePad::losePower(%data, %obj)
{
//   %obj.station.clearSelfPowered();
   Parent::losePower(%data, %obj);
}

datablock ShapeBaseImageData(VehiclePadPackImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = VehiclePadPack;
   mountPoint = 1;
   offset = "0 0 0";
   heatSignature = 0;
   deployed = DeployableVehiclePad;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body

};

datablock ItemData(VehiclePadPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "VehiclePadPackImage";
   pickUpName = "a deployable vehicle pad";
   heatSignature = 0;

   emap = true;
};

function VehiclePadPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

datablock StaticShapeData(VehiclePadBase) : StaticShapeDamageProfile
{
   className = StaticShape;
//   dynamicType = $TypeMasks::StaticShapeObjectType;
   shapeFile = "xmiscf.dts";   // stackable2l
   maxDamage = 10;
   destroyedLevel = 10;
   disabledLevel = 10;
   explosion = VehicleBombExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   targetNameTag = 'Vehicle';
   targetTypeTag = 'Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "VehiclePadPack";
};

function VehiclePadBase::shouldApplyImpulse(){}
function IxmucaneBase::shouldApplyImpulse(){}
function IxmucaneBaseTop::shouldApplyImpulse(){}

function VehiclePadPackImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isNearbyEnemyBase(300))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function VehiclePadPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);
   %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 5"));
   %rot = %item.getInitialRotation(%plyr);

   %trans[0] = vectorAdd(%item.surfacePt, "0 2.5 0.5") SPC "0 0 1 3.14";
   %deplObj = addMultiObjectManager(%plyr, %trans[0]);
   %vpad = addObject(%plyr, %item.deployed, %trans[0], %deplObj);

   %trans[1] = vectorAdd(%item.surfacePt, "0 0 0.5") SPC "0 1 0 0";
   %deplObjPad = addObject(%plyr, "VehiclePadBase", %trans[1], %deplObj, "10 15 1.5");
   %deplObjPad.vpad = %vpad;
   findFriendlyGenInArea(%obj, 250);

   %trans[2] = vectorAdd(%item.surfacePt, "10 -19.75 1.25") SPC "0 0 1 1.57";
   %s1 = addStation(%plyr, %trans[2], %deplObj);
   findFriendlyGenInArea(%s1, 250);

   %trans[3] = vectorAdd(%item.surfacePt, "-10 -19.75 1.25") SPC "0 0 1 -1.57";
   %station = addStation(%plyr, %trans[3], %deplObj);
   findFriendlyGenInArea(%station, 250);

   %deplObj.play3d(%item.deploySound);
   $TeamDeployedCount[%plyr.team, %item.item]++;

   detectDeployingOn(%deplObj, %item.surfacePt);
}

function DeployableVehiclePad::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.station.setDamageState(Destroyed);
   if( !%obj.destroyed )	// multiplication fix +soph
      $TeamDeployedCount[%obj.team, VehiclePadPack]--;
   %obj.destroyed = true;	// ditto +soph
   feedbackDED( %obj , 0.6 );		// +soph
   %obj.schedule(250, "delete");
}

function VehiclePadBase::onDestroyed(%this, %obj, %prevState)
{
//   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
}

//====================================== Deployable Defense Base
$TeamDeployableMax[DefenseBasePack] = 1;

datablock StaticShapeData(DefensePad) : StaticShapeDamageProfile
{
   shapeFile = "stackable2l.dts";   // teamlogo_projector
   maxDamage = 36;	// = 30; -soph
   destroyedLevel = 36;	// = 30; -soph
   disabledLevel = 36;	// = 30; -soph
   explosion = VehicleBombExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   deployedObject = true;

   targetNameTag = 'Remote';
   targetTypeTag = 'Defense Base';

   cmdCategory = "DSupport";
   cmdIcon = "CMDGeneratorIcon";
   cmdMiniIconName = "commander/MiniIcons/com_generator";

   sensorData = VehiclePulseSensor;
   sensorRadius = 100;
   sensorColor = "225 40 195";

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "DefenseBasePack";
};

datablock StaticShapeData(DefenseTop) : StaticShapeDamageProfile
{
   shapeFile = "stackable2l.dts";   // teamlogo_projector
   maxDamage = DefensePad.maxDamage ;		// = 30; -soph
   destroyedLevel = DefensePad.maxDamage ;	// = 30; -soph
   disabledLevel = DefensePad.maxDamage ;	// = 30; -soph
   explosion = ImpactMortarExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   deployedObject = true;

   targetNameTag = 'Remote';
   targetTypeTag = 'Defense Base';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "DefenseBasePack";
};

datablock StaticShapeData(DefenseTelePad) : StaticShapeDamageProfile
{
   shapeFile = "station_teleport.dts";
   maxDamage = 7.5;
   destroyedLevel = 7.5;
   disabledLevel = 7.5;
   explosion = ImpactMortarExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// $TypeMasks::StaticShapeObjectType; -soph

   deployedObject = true;

   targetNameTag = 'Base to MANTA';
   targetTypeTag = 'TelePad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

datablock ForceFieldBareData(DefenseBaseField)
{
   fadeMS           = 1000;
   baseTranslucency = 0.2;
   powerOffTranslucency = 0.0;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.99 0.47 0.28";
   powerOffColor    = "0.99 0.47 0.28";

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
};

datablock ForceFieldBareData(BorgBaseField)
{
   fadeMS           = 1000;
   baseTranslucency = 0.2;
   powerOffTranslucency = 0.0;

   teamPermiable    = true;
   otherPermiable   = false;
   color            = "0.28 0.28 0.99";
   powerOffColor    = "0.28 0.28 0.99";

   texture[0] = "skins/forcef1";
   texture[1] = "skins/forcef2";
   texture[2] = "skins/forcef3";
   texture[3] = "skins/forcef4";
   texture[4] = "skins/forcef5";

   framesPerSec = 10;
   numFrames = 5;
   scrollSpeed = 15;
   umapping = 1.0;
   vmapping = 0.15;
};

datablock ShapeBaseImageData(DefenseBaseImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = DefenseBasePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = DefensePad;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 360;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(DefenseBasePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "DefenseBaseImage";
   pickUpName = "a deployable remote base";
   heatSignature = 0;

   emap = true;
};

function DefenseBaseImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function DefenseBaseImage::testObjectTooClose(%item)
{
   InitContainerRadiusSearch( vectorAdd(%item.surfacePt, "0 0 5"), 50,  $TypeMasks::ForceFieldObjectType); // 0.5
   %test = containerSearchNext();

   return %test;
}

function DefenseBaseImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isNearbyEnemyBase(300))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function newBaseItemAppear(%obj, %pos, %time) // MrKeen
{
   %pos = getWords(%pos, 0, 2); // truncate transformsn if supplied

   if(%time < 250 || %time $= "")
      %time = 250;

   %obj.setPosition(vectorAdd(%pos, "0 0 -1000"));

   if(!%obj.pz) // exclude forcefields
   {
      %obj.setCloaked(true);
      %obj.schedule(%time+500, "setCloaked", false);

//      if(isObject(%obj.dpl_beacon))				// -soph
//         setDamagePercent(%obj, %obj.dpl_beacon.dmgpercent);	// -soph
   }

   %obj.schedule(%time, "setPosition", %pos);
}

function DefenseBaseImage::onDeploy(%item, %plyr, %slot) // ALL YOUR BASE ARE BELONG TO US
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);
   %rot = %item.getInitialRotation(%plyr);
//   %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 44"));

   initSpecialDeploySequenceItemT("Remote Base", 15000, 1, %item, %plyr, %slot, %rot);
}

function DefensePad::onAdd( %this , %obj )	// +[soph]
{						// multiplication fix
   Parent::onAdd( %this , %obj );
   %obj.destroyed = false;
}						// +[/soph]

function DefensePad::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   if( !%obj.destroyed )			// multiplication fix +soph
      $TeamDeployedCount[%obj.team, DefenseBasePack]--;
   %obj.destroyed = true;			// ditto +soph
   feedbackDED( %obj , 5 );			// +soph
   %obj.schedule(250, "delete");
//   destroyForcefields(%obj);
}

function DefenseTop::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
}

function DefenseTelePad::clearTeleTimeout(%data, %pad)
{
   %pad.teleTimeout = 0;
}

function DefenseTelePad::finishTele(%data, %pad, %pl)
{
   %pl.setMoveState(false);
   %pad.teleTimeout = 1;
   %data.schedule(6000, clearTeleTimeout, %pad);

   %pl.setPosition(%pad.getPosition());
   %pad.setThreadDir(1, false);
   %pad.playThread(1, "activate");

   if(isObject(%pad.teletgt))
   {
        %pad.teletgt.setThreadDir(1, false);
        %pad.teletgt.playThread(1, "activate");
   }

   %pl.teleportEndFX();
   %pad.playAudio($PlaySound, UnTeleportSound);
}

function DefenseTelePad::startTele(%data, %pad, %pl)
{
   %pl.setMoveState(true);
   %pad.TeleTimeout = 1;
   %data.schedule(6000, clearTeleTimeout, %pad);

   %pl.teleportStartFX();
   %pad.setThreadDir(1, true);
   %pad.teletgt.setThreadDir(1, true);
   %pad.playThread(1, "activate");
   %pad.teletgt.playThread(1, "activate");
   %pad.playAudio($PlaySound, TeleportSound);
   %data.schedule(2000, finishTele, %pad.teletgt, %pl);
}

function checkForMANTATele(%obj)
{
   InitContainerRadiusSearch(%obj.getPosition(), 10000, $TypeMasks::StaticObjectType);

   while((%pad = ContainerSearchNext()) != 0)
   {
      if(%pad.isMANTATele)
         if(%pad != %obj)
            if(%pad.team == %obj.team)
               return %pad;

      else continue;
   }
   return false;
}

function DefenseTelePad::onCollision(%data, %obj, %col)
{
   if(%col.getDataBlock().className !$= "Armor" || %col.getState() $= "Dead")
      return;

   else if(%obj.teleTimeout > 0)
   {
      messageClient(%col.client, 'MsgTeleIsCool', '\c2Telepad is recharging.');
      return;
   }

   if(isObject(%obj.teletgt))
      %pad = %obj.teletgt;
   else
      %pad = checkForMANTATele(%obj);

   if(!isObject(%pad))
   {
      if(%obj.isMantaBasedPad)
         messageClient(%col.client, 'MsgMantaTeleNonExistB', '\c2Cannot teleport; There is no Defense Base to teleport to.');
      else
         messageClient(%col.client, 'MsgMantaTeleNonExistM', '\c2Cannot teleport; There is no MANTA Ion Cannon to teleport to.');

      return;
   }
   else
   {
      %obj.teletgt = %pad;
      %col.setvelocity("0 0 0");
      %col.setPosition(%obj.getPosition());
      %data.startTele(%obj, %col);
   }
}

function DefenseTelePad::onDestroyed(%this, %obj, %prevState)
{
//   %obj.teletgt.setDamageState(Destroyed);
   %obj.teletgt.teletgt = "";
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

function DefensePad::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//====================================== Deployable Bombardment Base
$TeamDeployableMax[BorgCubePack] = 1;

datablock ShapeBaseImageData(BorgCubeImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = BorgCubePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BorgCubePad;
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 360;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(BorgCubePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BorgCubeImage";
   pickUpName = "a remote blastech bunker";
   heatSignature = 0;

   emap = true;
};

datablock StaticShapeData(BorgCubePad) : BombardmentDamageProfile
{
   shapeFile = "stackable2l.dts";   // teamlogo_projector
   maxDamage = 20;	// = 15; -soph
   destroyedLevel = 20;	// = 15; -soph
   disabledLevel = 20;	// = 15; -soph
   explosion = VehicleExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   deployedObject = true;
   heatSignature = 0;

   targetNameTag = 'Remote';
   targetTypeTag = 'Blastech Bunker';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;

   undeployPack = "BorgCubePack";
};

datablock StaticShapeData(BorgCubeTop) : BombardmentDamageProfile
{
   shapeFile = "stackable2l.dts";   // teamlogo_projector
   maxDamage = BorgCubePad.maxDamage ;		// = 15; -soph
   destroyedLevel = BorgCubePad.maxDamage ;	// = 15; -soph
   disabledLevel = BorgCubePad.maxDamage ;	// = 15; -soph
   explosion = VehicleExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   deployedObject = true;

   targetNameTag = 'Remote';
   targetTypeTag = 'Blastech Bunker';

   cmdCategory = "DSupport";
   cmdIcon = "CMDGeneratorIcon";
   cmdMiniIconName = "commander/MiniIcons/com_generator";

   sensorData = VehiclePulseSensor;
   sensorRadius = 100;
   sensorColor = "225 40 195";

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "BorgCubePack";
};

function BorgCubeImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

function BorgCubeImage::testObjectTooClose(%item)
{
//   ShapeBaseImageData::testObjectTooClose(%item);
//}

//   return false;

   %mask = ( $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType ); //| $TypeMasks::StaticShapeObjectType);

   InitContainerRadiusSearch( %item.surfacePt, 50,  %mask); // 0.5

   %test = containerSearchNext();
//   if(
//   if(%test)
//      echo(%test);

   return %test;
}

function BorgCubeImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(25))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isNearbyEnemyBase(150))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function BorgCubeImage::onDeploy(%item, %plyr, %slot) // MrKeen
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
//   %plyr.setPosition(vectorAdd(%plyr.getPosition(), "0 0 4"));
   initSpecialDeploySequenceItemT("Remote Blastech Bunker", 10000, 0, %item, %plyr, %slot, %rot);
}

function BorgCubePad::onAdd( %this , %obj )	// +[soph]
{						// multiplication fix
   Parent::onAdd(%this, %obj);
   %obj.destroyed = false;
}						// +[/soph]

function BorgCubePad::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
   if( !%obj.destroyed )			// multiplication fix +soph
      $TeamDeployedCount[%obj.team, BorgCubePack]--;
   %obj.destroyed = true;			// ditto +soph
   feedbackDED( %obj , 4 );		// +soph
//   destroyForcefields(%obj);
}

function BorgCubeTop::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
}

function BorgCubePad::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//====================================== Deployable Repulsor Beacon
$TeamDeployableMax[RepulsorBeaconPack] = 2;

datablock StaticShapeData(RepulsorBeacon) : StaticShapeDamageProfile
{
//   className = "StaticShape";
//   dynamicType = $TypeMasks::StaticShapeObjectType;
   shapefile = "stackable2m.dts"; 
   rechargeRate = 0.31;

   needsNoPower = true;
   mass = 2.0;
   maxDamage = 40;
   destroyedLevel = 40;
   disabledLevel = 40;
   repairRate = 0;
	explosion	= VehicleBombExplosion;
	expDmgRadius = 25.0;
	expDamage = 3;
	expImpulse = 2000.0;

	deployedObject = true;

   energyPerDamagePoint = 50;
   maxEnergy = 50;

  	humSound = SensorJammerActivateSound;
   heatSignature = 0;

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDCameraIcon;
   cmdMiniIconName = "commander/MiniIcons/com_camera_grey";

   targetNameTag = 'Deployable';
   targetTypeTag = 'Repulsor Beacon';

   firstPersonOnly = true;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;

   undeployPack = "RepulsorBeaconPack";
};

datablock ShapeBaseImageData(RepulsorBeaconPackImage)
{
   mass = 20;

   shapeFile = "deploy_sensor_motion.dts";
   item = RepulsorBeaconPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = RepulsorBeacon;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   emap = true;

   maxDepSlope = 180;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(RepulsorBeaconPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_turreti.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "RepulsorBeaconPackImage";
   pickUpName = "a deployable repulsor beacon";
   heatSignature = 0;

   emap = true;
};

function RepulsorBeaconPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function initReflectObjects(%obj, %item)
{
   if(!isObject(%obj))
      return;

   %field = new StaticShape()
   {
       scale = "14 14 14";
       dataBlock = "WarpBubble";
   };

//   %field.setPosition(%obj.getPosition());
   %field.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   %field.playThread($DeployThread, "deploy");
   %obj.field = %field;
   addAttachment(%obj, %field);

   InitContainerRadiusSearch(%obj.getPosition(), $ShieldBDDist, $TypeMasks::StaticShapeObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(isObject(%int) && %int != %obj && %int.isDEDevice)
      {
//         zapObject(%int);
         %obj.linkedShieldSource = %int;
         break;
      }

     // addAttachment(%int, %obj);	// DAMN IT!!!!!1 ghhrhhrghjlkjjhfFFF -soph
   }

   if(!isObject(%obj.linkedShieldSource))
   {
      if(isObject(%obj.owner))
         messageClient(%obj.owner, 'MsgSBMALF', '\c1Defense+ Device required in order for this device to function, must be deployed within %1m of one.', $ShieldBDDist);

      %obj.schedule(2000, setDamageState, Destroyed); // suicide if no avaliable objects to protect
      return;
   }

   loopReflectObjects(%obj);
}

function RepulsorBeacon::onEndSequence(%data, %obj, %thread)
{
     // No self powering
}

function loopReflectObjects(%obj)
{
   if(!isObject(%obj))
      return;

   objectReflectProjectiles(%obj, 25, "", true);

   if(!isObject(%obj.linkedShieldSource))
       %obj.schedule(2000, setDamageState, Destroyed);

   schedule($Host::RepulsorTickRate, %obj, loopReflectObjects, %obj);
}

function RepulsorBeaconPackImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(20))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   %dist = %plyr.isNearbyFriendlyBase();									// if(!%plyr.isNearbyFriendlyBase(300)) -soph
   if( %dist > 300 )												// +soph
   {
      $EDC::Reason = "You are trying to deploy" SPC mFloor( %dist - 299 ) @ "m too far away from the base.";	// $EDC::Reason = "You are trying to deploy too far away from the base.";
      return true;
   }
   else 
      if ( %dist < 0 )										// [soph]
      {
         $EDC::Reason = "There is no friendly base node in this region, repulsor cannot be deployed.";
         return true;
      }												// [/soph]

   if(%plyr.isNearbyEnemyBase(200))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function RepulsorBeaconPackImage::onDeploy(%item, %plyr, %slot)
{
     %plyr.unmountImage(%slot);
     %plyr.decInventory(%item.item, 1);

     %rot = %item.getInitialRotation(%plyr);

     %beacon = new StaticShape()
     {
        dataBlock = %item.deployed;
     };
     MissionCleanup.add(%beacon);

     %beacon.setScale("1 1 3");
//     %beacon.setTransform(%item.surfacePt SPC %rot);
     %beacon.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
     %beacon.setSelfPowered();          // unlock

     if(%beacon.getDatablock().rechargeRate)
           %beacon.setRechargeRate(%beacon.getDatablock().rechargeRate);

     %beacon.team = %plyr.client.Team;
     %beacon.owner = %plyr.client;

     if(%beacon.getTarget() != -1)
           setTargetSensorGroup(%beacon.getTarget(), %plyr.client.team);

     addToDeployGroup(%beacon);
     AIDeployObject(%plyr.client, %beacon);
     serverPlay3D(%item.deploySound, %beacon.getTransform());
     $TeamDeployedCount[%plyr.team, "RepulsorBeaconPack"]++;     // just fine
     %beacon.deploy();
     initReflectObjects(%beacon, %item);
     %beacon.playthread(0, "ambient");
   %beacon.isRepulsor = true;

   detectDeployingOn(%beacon, %item.surfacePt);
}

function RepulsorBeacon::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   if( !%obj.destroyed )			// multiplication fix +soph
      $TeamDeployedCount[%obj.team, RepulsorBeaconPack]--;
   %obj.destroyed = true;			// ditto +soph
//   %obj.linkedShieldSource.applyDamage(3);	// -soph
   feedbackDED( %obj , 3 );			// +soph
   %obj.schedule(250, "delete");
   %obj.field.delete();
}

function RepulsorBeaconPackImage::testTurretTooClose(%item)
{
   InitContainerRadiusSearch(%item.surfacePt, 30, 8192); //$TypeMasks::StaticObjectType = 1);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundName $= RepulsorBeacon))
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

function RepulsorBeaconPackImage::testInventoryTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, 30, $TypeMasks::StaticShapeObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundname $= RepulsorBeacon) || (%foundname $= RepulsorBaecon))
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

function RepulsorBeaconPackImage::testObjectTooClose(%item, %plyr)
{
   InitContainerRadiusSearch(%item.surfacePt, 30, $TypeMasks::StaticShapeObjectType);

   // old function was only checking whether the first object found was a turret -- also wasn't checking
   // which team the object was on
   %turretInRange = false;
   while((%found = containerSearchNext()) != 0)
   {
      %foundName = %found.getDataBlock().getName();
      if((%foundName $= RepulsorBeacon))
         if (%found.team == %plyr.team)
         {
            %turretInRange = true;
            break;
         }
   }
   return %turretInRange;
}

//====================================== Deployable Jammer Beacon
$TeamDeployableMax[JammerBeaconPack] = 3;

datablock SensorData(JammerBeaconSensor)
{
   detects = true;
   detectsUsingLOS = false;
   detectsPassiveJammed = false;
   detectsActiveJammed = false;
   detectsCloaked = true;
   detectionPings = false;
   detectRadius = 50;

   jams = true;
   jamsOnlyGroup = true;
   jamsUsingLOS = false;
   jamRadius = 50;
};

datablock StaticShapeData(JammerBeacon) : StaticShapeDamageProfile
{
   shapeFile = "camera.dts";
   explosion = DeployablesExplosion;
   maxDamage = 1;
   disabledLevel = 1;
   destroyedLevel = 1;
   targetNameTag = 'Deployable';
   targetTypeTag = 'Jammer Beacon';

   deployedObject = true;

   cmdCategory = "DSupport";
   cmdIcon = CMDCameraIcon;
   cmdMiniIconName = "commander/MiniIcons/com_camera_grey";

   dynamicType = $TypeMasks::SensorObjectType;
   heatSignature = 0;
   debrisShapeName = "debris_generic_small.dts";
   debris = SmallShapeDebris;

   minDeployDis       = 0.5;
   maxDeployDis       = 5.0;

   undeployPack = "JammerBeaconPack";
};

datablock ShapeBaseImageData(JammerBeaconPackImage)
{
   mass = 5;

   shapeFile = "deploy_sensor_motion.dts";
   item = JammerBeaconPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = JammerBeacon;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   emap = true;

   maxDepSlope = 360;
   deploySound = MotionSensorDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(JammerBeaconPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_turreti.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "JammerBeaconPackImage";
   pickUpName = "a deployable Jammer beacon";
   heatSignature = 0;

   emap = true;
};

function JammerBeaconPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function jammerBeaconCloakingLoop(%obj)
{
   return; // :/

   if(isObject(%obj))
   {
      InitContainerRadiusSearch(%obj.getPosition(), 50, $TypeMasks::PlayerObjectType);
      while((%int = ContainerSearchNext()) != 0)
      {
         if(%int.client.team != %obj.team)
         {
            if(%int.isCloaked() && (%int.client.team != %obj.team) && %int.SJDetectCloaked)
            {
               messageClient(%int.client, 'MsgCloakPackJammed', '\c2Cloaking pack off.  Jammed.');
               %int.setCloaked(false);
               %int.setImageTrigger($BackpackSlot, false);
            }
         }
      }
      schedule(250, %obj, jammerBeaconCloakingLoop, %obj);
   }
}

function JammerBeaconPackImage::testObjectTooClose(%item)
{
//   return false;

   %mask =    ($TypeMasks::VehicleObjectType     | $TypeMasks::MoveableObjectType   |
               $TypeMasks::ForceFieldObjectType  | $TypeMasks::ItemObjectType       |
               $TypeMasks::PlayerObjectType      | $TypeMasks::TurretObjectType);

   InitContainerRadiusSearch( %item.surfacePt, 0.5, %mask );

   %test = containerSearchNext();
   return %test;
}

function JammerBeacon::checkDeployPos(%item)
{
   return true;
}

function JammerBeaconPackImage::onDeploy(%item, %plyr, %slot)
{
     %plyr.unmountImage(%slot);
     %plyr.decInventory(%item.item, 1);

     %rot = %item.getInitialRotation(%plyr);

     %beacon = new StaticShape()
     {
        dataBlock = %item.deployed;
     };
     MissionCleanup.add(%beacon);

//     %beacon.setTransform(%item.surfacePt SPC %rot);
     %beacon.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
     %beacon.setSelfPowered();          // unlock

     if(%beacon.getDatablock().rechargeRate)
           %beacon.setRechargeRate(%beacon.getDatablock().rechargeRate);

     %beacon.team = %plyr.client.Team;
     %beacon.owner = %plyr.client;

     if(%beacon.getTarget() != -1)
           setTargetSensorGroup(%beacon.getTarget(), %plyr.client.team);

     addToDeployGroup(%beacon);
     AIDeployObject(%plyr.client, %beacon);
     serverPlay3D(%item.deploySound, %beacon.getTransform());
     $TeamDeployedCount[%plyr.team, "JammerBeaconPack"]++;     // just fine
     %beacon.deploy();
     %beacon.isTapper = true;
     setTargetSensorData(%beacon.getTarget(), JammerBeaconSensor);
     %beacon.setJammerFX(true);
     %beacon.playthread(0, "deploy");

   detectDeployingOn(%beacon, %item.surfacePt);
}

function JammerBeacon::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   $TeamDeployedCount[%obj.team, JammerBeaconPack]--;
   %obj.schedule(250, "delete");
}

//====================================== Defense Enhancement Device
$TeamDeployableMax[DefEnhancePack] = 1;

datablock StaticShapeData(DEDevice) : StaticShapeDamageProfile
{
//   catagory       = "Generators";
   shapeFile      = "stackable3l.dts";
   explosion      = ShapeExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   maxDamage      = 10;	// = 7; -soph
   destroyedLevel = 10;	// = 7; -soph
   disabledLevel  = 10;	// = 7; -soph
   expDmgRadius = 10.0;
   expDamage = 5;
   expImpulse = 1500.0;
//   noIndividualDamage = true; //flag to make these invulnerable for certain mission types

   dynamicType = $TypeMasks::StaticShapeObjectType;
   isShielded = true;	// false; -soph
   energyPerDamagePoint = 50;
   maxEnergy = 125;
   rechargeRate = 0.2;
//   humSound = DEDHumSound;

   cmdCategory = "DSupport";
   cmdIcon = "CMDGeneratorIcon";
   cmdMiniIconName = "commander/MiniIcons/com_generator";
   targetNameTag = 'Defense+';
   targetTypeTag = 'Device';

   sensorData = VehiclePulseSensor;
   sensorRadius = 325;	// = 250; -soph
   sensorColor = "200 200 200";

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   undeployPack = "DefEnhancePack";
};

datablock ShapeBaseImageData(DefEnhanceImage)
{
   mass = 10;

   shapeFile = "pack_deploy_inventory.dts";
   item = DefEnhancePack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = DEDevice;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = TurretDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(DefEnhancePack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "DefEnhanceImage";
   pickUpName = "a defense enhancement device";
   heatSignature = 0;

   emap = true;
};

function DefEnhancePack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function DefEnhanceImage::extendedDeployChecks(%item, %plyr)
{
   %dist = %plyr.isNearbyFriendlyBase();									// if(!%plyr.isNearbyFriendlyBase(100)) -soph
   %isGen = %plyr.isFriendlyGenInArea( 100 );									// +soph

   if( !%isGen )												// if friendly gen, auto-win
      if( %dist > 100 )												// +soph
      {
        // $EDC::Reason = "You are trying to deploy too far away from the base.";				// -soph
         $EDC::Reason = "You are trying to deploy" SPC mFloor( %dist - 99 ) @ "m too far away from the base.";	// +soph
         return true;
      }
      else 
      {
         if ( %dist < 0 )											// +[soph]
         {
            $EDC::Reason = "There is no friendly base node in this region, D+D must be deployed near a Generator.";
            return true;
         }													// +[/soph]
      }

   $EDC::Reason = "";
   return false;
}

function DefEnhanceImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };
   MissionCleanup.add(%deplObj);

   %data = %deplObj.getDatablock();
//   %deplObj.setSelfPowered();
   %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);     // ripped from station.cs
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   %deplObj.setRechargeRate(%data.rechargeRate);
   %deplObj.setEnergyLevel(%data.MaxEnergy);

//   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "DefEnhancePack"]++;     // just fine

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);
   MissionCleanup.add(%deplObj);
//   %deplObj.deploy();

   findFriendlyGenInArea(%deplObj, 250);
      

   %deplObj.isDEDevice = true;
   %deplObj.unbeaconable = true;
  // DEDUpdate(%deplObj);							// -soph
   schedule( 1000 , 0 , DEDWarmup ,  %deplObj , 12 , getSimTime() + 5000 );	// +[soph]
   messageClient( %plyr.client , 'MsgDEDWarmup' , '\c5You have deployed your team\'s Defense+ Device, activation in 60 seconds.' );
   messageTeamExcept( %plyr.client , 'MsgDEDWarmup', '\c5%1 has deployed your team\'s Defense+ Device, activation in 60 seconds.' , %plyr.client.name );
   										// +[/soph]
//   echo("D+D Started");
   detectDeployingOn(%deplObj, %item.surfacePt);
}

function DefEnhanceImage::testSlopeTooGreat(%item)
{
   if(%item.surface)
      return getTerrainAngle(%item.surfaceNrm) > 45;
}

function DEDevice::onDestroyed(%this, %obj, %prevState)
{
   if(!%obj.shutdown)
   {
      %obj.shutdown = true;
      shutdownDED(%obj);
      $TeamDeployedCount[%obj.team, "DefEnhancePack"]--;
   }

   %obj.schedule(250, "delete");

   Parent::onDestroyed(%this, %obj, %prevState);
}

function DEDWarmup( %obj , %secondsRemaining , %predictedTime )	// +[soph]
{
  // this should compensate for schedule()'s tendancy to crack up with server load
   if( !isObject( %obj ) || %obj.shutdown )
      return;

   %actualTime = getSimTime();
   %delta = 5000 - ( %actualTime - %predictedTime );
   if( %secondsRemaining )
      schedule( %delta , 0 , DEDWarmup , %obj , %secondsRemaining-- , %actualTime + %delta );
   else
   {
      messageTeam( %obj.team , 'MsgDEDWarmup', '\c5Your team\'s Defense+ Device is now operational.' );
      DEDUpdate( %obj );
   }
}								// +[/soph]

function DEDUpdate(%obj)
{
   if(%obj.shutdown)
      return;

   if(%obj.DESet $= "")
       %obj.DESet = createRandomSimSet();

   %powerStatus = %obj.isPowered();

   InitContainerRadiusSearch(%obj.getPosition(), 325, $TypeMasks::PlayerObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType);

   %obj.DERemoveCount = 0;

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.team == %obj.team && !%int.DESetMember && !%int.ignoreDED)
      {
         if(%int.getType() & $TypeMasks::PlayerObjectType && %int.dead) // stupid dog
                   %int.ignoreDED = true;
         else
	 {
//              echo("DED Found: " SPC %int SPC %int.getDatablock().getName());	// -soph
              %obj.DESet.add(%int);
              %int.DESetMember = true;
              %int.DESetSet = %obj.DESet;
              %int.inDEDField = %powerStatus;
              %int.linkedShieldSource = %obj;					// +soph
              %int.DEFeedbackPercent = 1 - %int.getDamagePercent();		// +soph
	 }
      }
   }

   %count = %obj.DESet.getCount();

   if(%count != -1)
   {
//        echo(%count);
        for(%i = 0; %i < %count; %i++)
        {
             %int = %obj.DESet.getObject(%i);
//           echo(%int SPC %i SPC (%count > %int));

             if(%int == -1)
                  continue;

             %dist = vectorDist(%obj.getPosition(), %int.getPosition()); // Make sure we're in range still

             if(%dist > 325 || %int.ignoreDED || %int.team != %obj.team )	// if(%dist > 325 || %int.ignoreDED) to prevent switching objects -soph
             {
//                  echo("DED Lost: " SPC %int SPC %int.getDatablock().getName());	// -soph
                  %int.DESetMember = false;
                  %int.DESetSet = "";
                  %int.inDEDField = false;
                  %obj.DERemoveObject[%obj.DERemoveCount] = %int;
                  %obj.DERemoveCount++;
                  %int.linkedShieldSource = "";						// +soph
             }
             else
             {										// [soph]
                   if ( %powerStatus )							// sorry, but I just couldn't wrap my head around what was here
		   {									// I scrapped much of it and rewrote the logic 
                        if(!%int.ignoreRepairThis)
			     if(%int.getType() & $TypeMasks::PlayerObjectType) 
			     {
			          if(!%int.dead) 
                                       %int.takeDamage(-0.002 * %int.repairKitFactor);	// %int.takeDamage(-0.00175 * %int.repairKitFactor);
			     }
                             else if( %int != %obj )					// objects get more love
                             {
                                  %int.takeDamage(-0.0025);				// %int.takeDamage(-0.00175); -soph
                                  %integrity = 1 - %int.getDamagePercent();
                                  if( %integrity > %int.DEFeedbackPercent )
                                       %int.DEFeedbackPercent = %integrity;
                             }
                             else
                                  %int.takeDamage(-0.00125);				// %int.takeDamage(-0.00175); 
		   }
                   else if ( %int.getDatablock( ).getName( ) $= "generatorLarge" )	// if power is down
                        %int.takeDamage( -0.003 );					// autorepair gens, takes ~40 secs
                   if(%obj.lastPowerStatus != %powerStatus) 
                        %int.inDEDField = %powerStatus;	// Bind DED field to power status
              }										// [/soph]
          }

          %obj.lastPowerStatus = %powerStatus;
     }

     // do another pass to remove all objects outside DED Field
     for(%i = 0; %i < %obj.DERemoveCount; %i++)
     {
          %idx = %obj.DERemoveObject[%idx];

          if(isObject(%idx))
               %obj.DESet.remove(%idx);
     }

     schedule(250, %obj, DEDUpdate, %obj);
}

function shutdownDED(%obj)
{
     %count = %obj.DESet.getCount();
     %obj.DERemoveCount = 0;

     for(%i = 0; %i < %count; %i++)
     {
          %int = %obj.DESet.getObject(%i);
          %int.DESetMember = false;
          %int.inDEDField = false;
          %int.DESetSet = "";
          %obj.DERemoveObject[%obj.DERemoveCount] = %int;
          %obj.DERemoveCount++;
          %int.linkedShieldSource = "";	// +soph
     }

     // do another pass to remove all objects
     for(%i = 0; %i < %obj.DERemoveCount; %i++)
     {
          %idx = %obj.DERemoveObject[%idx];

          if(isObject(%idx))
               %obj.DESet.remove(%idx);
     }

     %obj.shutdown = true;
}

//====================================== Blast Wall
$TeamDeployableMax[BlastWallPack] = 6;

datablock StaticShapeData(BlastWall) : BlastParametricsDamageProfile
{
//   catagory       = "Generators";
   shapeFile      = "bmiscf.dts";
   explosion      = ShapeExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   maxDamage      = 28;	// = 48; -soph
   destroyedLevel = 28;	// = 48; -soph
   disabledLevel  = 28;	// = 48; -soph

   expDmgRadius = 5.0;
   expDamage = 0.1;
   expImpulse = 1500.0;

//   noIndividualDamage = true; //flag to make these invulnerable for certain mission types

//   dynamicType = $TypeMasks::StaticShapeObjectType;
   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   targetTypeTag = 'Blast Wall';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   undeployPack = "BlastWallPack";
};

datablock ShapeBaseImageData(BlastWallPackImage)
{
   mass = 10;

   shapeFile = "pack_upgrade_shield.dts";
   item = BlastWallPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BlastWall;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(BlastWallPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_upgrade_shield.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BlastWallPackImage";
   pickUpName = "a blast wall";
   heatSignature = 0;

   emap = true;
};

function BlastWallPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function BlastWallPackImage::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      5,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::StaticShapeObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
//            echo("why");
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if(%item.testObjectTooClose(%item))
   {
      %disqualified = $NotDeployableReason::ObjectTooClose; //$EDC::Reason;
   }
   else if(%item.extendedDeployChecks(%plyr))
   {
      %disqualified = $NotDeployableReason::EDCFailed;
   }
   else if (%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      %xform = %surfacePt SPC %rot;
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(50, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function BlastWallPackImage::testObjectTooClose(%item)
{
//   InitContainerRadiusSearch(%item.surfacePt, 5, $TypeMasks::StaticShapeObjectType);
//
//   %test = containerSearchNext();
//
//   if(%test.notRepairable || %test.trigger)
//      return %test;
//   else
      return false;
}

function BlastWallPackImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(4))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isNearbyEnemyBase(150))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function BlastWallPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();
//   %deplObj.setDeployRotation(%item.surfacePt, "0 0 1"); //%item.surfaceNrm

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("2.75 0.2 20");
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;
//   %deplobj.notRepairable = true;
   %deplObj.blastEmplacement = true;
   %deplObj.unbeaconable = true;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "BlastWallPack"]++;
   MissionCleanup.add(%deplObj);

   detectDeployingOn(%deplObj, %item.surfacePt);
   %data.damageObject( %deplObj , 0 , %item.surfacePt , %data.destroyedLevel * 3 / 5 , 0 ) ;	// +soph
}

function BlastWallPackImage::testSlopeTooGreat(%item)
{
//   if(%item.surface)
//      return getTerrainAngle(%item.surfaceNrm) > 90;
   return false; // deplpyable at all angles
}

function BlastWall::onDestroyed(%this, %obj, %prevState)
{
   $TeamDeployedCount[%obj.team, "BlastWallPack"]--;
   feedbackDED( %obj , 1.5 );			// +soph
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Blast Door
$TeamDeployableMax[BlastDoorPack] = 4;

datablock StaticShapeData(BlastDoor) : BlastParametricsDamageProfile
{
//   catagory       = "Generators";
   shapeFile      = "dmiscf.dts";
   explosion      = ShapeExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   maxDamage      = 19;	// = 32; -soph // 22
   destroyedLevel = 19;	// = 32; -soph
   disabledLevel  = 19;	// = 32; -soph

   expDmgRadius = 5.0;
   expDamage = 0.1;
   expImpulse = 1500.0;

//   noIndividualDamage = true; //flag to make these invulnerable for certain mission types

//   dynamicType = $TypeMasks::StaticShapeObjectType;
   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   targetTypeTag = 'Blast Door';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   undeployPack = "BlastDoorPack";
};

datablock ShapeBaseImageData(BlastDoorPackImage)
{
   mass = 10;

   shapeFile = "pack_upgrade_shield.dts";
   item = BlastDoorPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BlastDoor;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 181;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(BlastDoorPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_upgrade_shield.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BlastDoorPackImage";
   pickUpName = "a blast door";
   heatSignature = 0;

   emap = true;
};

function BlastDoorPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function BlastDoorPackImage::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      5,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::StaticShapeObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
//            echo("why");
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if(%item.testObjectTooClose(%item))
   {
      %disqualified = $NotDeployableReason::ObjectTooClose; //$EDC::Reason;
   }
   else if(%item.extendedDeployChecks(%plyr))
   {
      %disqualified = $NotDeployableReason::EDCFailed;
   }
   else if (%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      %xform = %surfacePt SPC %rot;
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(50, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function BlastDoorPackImage::testObjectTooClose(%item)
{
//   InitContainerRadiusSearch(%item.surfacePt, 5, $TypeMasks::StaticShapeObjectType);
//
//   %test = containerSearchNext();
//
//   if(%test.notRepairable || %test.trigger)
//      return %test;
//   else
      return false;
}

function BlastDoorPackImage::extendedDeployChecks(%item, %plyr)
{
   if(%plyr.isFlagInArea(4))
   {
      $EDC::Reason = "You are trying to deploy too close to the flag.";
      return true;
   }

   if(%plyr.isNearbyEnemyBase(150))
   {
      $EDC::Reason = "You are trying to deploy too close to the enemy base.";
      return true;
   }

   $EDC::Reason = "";
   return false;
}

function BlastDoorPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();
//   %deplObj.setDeployRotation(%item.surfacePt, "0 0 1"); //%item.surfaceNrm
//   %deplObj.setTransform(%item.surfacePt SPC %rot);
//   %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("2.75 0.2 20");
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;
   %deplObj.lastPos = %item.surfacePt;
//   %deplobj.notRepairable = true;
   %deplObj.blastEmplacement = true;
   %deplObj.unbeaconable = true;
   %deplObj.isBlastDoor = true;
   
   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "BlastDoorPack"]++;
   MissionCleanup.add(%deplObj);

   detectDeployingOn(%deplObj, %item.surfacePt);
   %data.damageObject( %deplObj , 0 , %item.surfacePt , %data.destroyedLevel * 3 / 5 , 0 ) ;	// +soph
}

function BlastDoorPackImage::testSlopeTooGreat(%item)
{
//   if(%item.surface)
//      return getTerrainAngle(%item.surfaceNrm) > 90;
   return false; // deplpyable at all angles
}

function BlastDoorPackImage::testNoInteriorFound(%item)
{
   return %item.surface.getClassName() !$= InteriorInstance;
}

function moveBlastDoor(%obj, %dir)
{
   if(%dir)
   {
      %movePt = vectorAdd(%obj.lastPos, "0 0 -10000");
      %obj.setPosition(%movePt);
      schedule(2500, %obj, "moveBlastDoor", %obj, false);
   }
   else
   {
      %obj.setPosition(%obj.lastPos);
      %obj.startFade(250, 0, false);
   }
}

function BlastDoor::onCollision(%data, %obj, %col)
{
//   %struck = %col.getClassName();
//   if(%struck $= "Player" || %struck $= "WheeledVehicle" || %struck $= "FlyingVehicle" || %struck $= "HoverVehicle")
   if(%col.isVehicle() || %col.isPlayer())
   {
      if(%col.team == %obj.team)
      {
         %obj.startFade(250, 0, true);
         schedule(250, %obj, "moveBlastDoor", %obj, true);
      }
   }

//   Parent::onCollision(%data, %obj, %col);
}

function BlastDoor::onDestroyed(%this, %obj, %prevState)
{
   $TeamDeployedCount[%obj.team, "BlastDoorPack"]--;
   feedbackDED( %obj , 1.25 );			// +soph
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Deployable Manta Satellite
$TeamDeployableMax[MANTASatPack] = 1;
$MantaChargeTime = 60000;

datablock StaticShapeData(MantaPad) : StaticShapeDamageProfile
{
   shapeFile = "nexuscap.dts";   // teamlogo_projector
   maxDamage = 10;
   destroyedLevel = 10;
   disabledLevel = 10;
   explosion = ImpactMortarExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   deployedObject = true;

   targetNameTag = 'MANTA';
   targetTypeTag = 'Ion Cannon';

   cmdCategory = "Tactical";
   cmdIcon = CMDCameraIcon;
   cmdMiniIconName = "commander/MiniIcons/com_camera_grey";

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "MANTASatPack";
};

datablock StaticShapeData(MantaLaunchPad) : StaticShapeDamageProfile
{
   shapeFile = "nexusbase.dts";   // teamlogo_projector
   maxDamage = 100;
   destroyedLevel = 100;
   disabledLevel = 100;
   explosion      = TurretExplosion;
	expDmgRadius = 15.0;
	expDamage = 0.7;
	expImpulse = 2000.0;

   deployedObject = true;

   targetNameTag = 'Manta';
   targetTypeTag = 'Launch Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

//   undeployPack = "MANTASatPack";
};

datablock StaticShapeData(MantaSupport) : StaticShapeDamageProfile
{
   shapeFile = "int_flagstand.dts";   // teamlogo_projector
   maxDamage = 10;
   destroyedLevel = 10;
   disabledLevel = 10;
   explosion = VehicleBombExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// +soph

   deployedObject = true;

   targetNameTag = 'MANTA';
   targetTypeTag = 'Ion Cannon';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;

   undeployPack = "MANTASatPack";
};

datablock StaticShapeData(MANTATelePad) : StaticShapeDamageProfile
{
   shapeFile = "station_teleport.dts";
   maxDamage = 10;
   destroyedLevel = 10;
   disabledLevel = 10;
   explosion = ImpactMortarExplosion;
   dynamicType = $TypeMasks::SensorObjectType ;	// $TypeMasks::StaticShapeObjectType; -soph
   deployedObject = true;

   targetNameTag = 'MANTA to Base';
   targetTypeTag = 'TelePad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

datablock ShapeBaseImageData(MantaSatImage)
{
   mass = 10;
   emap = true;
   isLarge = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = MantaSatPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = "LOLOL"; // MrKeen - whoa... rofl
   heatSignature = 1.0;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 360;
   deploySound = SensorDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
};

datablock ItemData(MantaSatPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "MantaSatImage";
   pickUpName = "a MANTA Satellite";
   heatSignature = 0;

   emap = true;
};

function MantaSatImage::testNoTerrainFound(%item)
{
   return %item.surface.getClassName() !$= TerrainBlock;
}

datablock SeekerProjectileData(MantaSatMissile)
{
   scale = "15.0 15.0 15.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.0; --- Must be > 0 (ST)
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 2000;

   explosion           = "LargeMissileExplosion";
   underwaterExplosion = UnderwaterHandGrenadeExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 5500;
   muzzleVelocity      = 0.1;
   maxVelocity         = 1000.0;
   turningSpeed        = 90.0;
   acceleration        = 500.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 3000;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

function MantaSatImage::extendedDeployChecks(%item, %plyr)
{
   %dist = %plyr.isNearbyFriendlyBase();									// if(!%plyr.isNearbyFriendlyBase(300)) -soph
   if( %dist > 300 )												// +soph
   {
      $EDC::Reason = "You are trying to deploy" SPC mFloor( %dist -= 299 ) @ "m too far away from the base.";	// $EDC::Reason = "You are trying to deploy too far away from the base.";
      return true;
   }
   else 
      if ( %dist < 0 )										// [soph]
      {
         $EDC::Reason = "There is no friendly base node in this region, MANTA cannot be deployed.";
         return true;
      }												// [/soph]

   $EDC::Reason = "";
   return false;
}

function MantaSatImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %pos = vectorAdd(%item.surfacePt, "0 0 10");
   %scVec = "0 0 1000";
   %end = VectorAdd(%pos, %scVec);
   %searchResult = containerRayCast(%pos, %end, $TypeMasks::DefaultLOSType, %obj);

   if(!%searchResult)
   {
      %rot = %item.getInitialRotation(%plyr);

      %trans[0] = vectorAdd(%item.surfacePt, "0 0 1003.5") SPC "0 1 0 0";
      %deplObj = addMultiObjectManager(%plyr, %trans[0]);
   //   addObject(%plyr, "MANTAMarker", %trans[0], %deplObj);

      %Mantapad = addObject(%plyr, "MantaPad", %trans[0], %deplObj, "15 15 11");
      %Mantapad.playThread(0, "ambient");

      %trans[1] = vectorAdd(%item.surfacePt, "0 0 1004");
      %deplObjPad = addObjectV(%plyr, "MantaSupport", %trans[1], "0 0 -1", %deplObj, "10 10 75");
      %deplObjPad.team = %plyr.team;

      %trans[2] = vectorAdd(%item.surfacePt, "0 0 1009") SPC "0 1 0 0";
      %tele = addObject(%plyr, "MANTATelePad", %trans[2], %deplObj);
      %tele.isMantaBasedPad = true;
      %tele.isMANTATele = true;

      %deplObj.play3d(%item.deploySound);
      $TeamDeployedCount[%plyr.team, "MantaSatPack"]++;     // just fine

      %deplObj.isManta = true;
      %deplObj.isLoaded = false;

      $MantaSat[%plyr.team] = %deplObj;
   }

   %pos = vectorAdd(%item.surfacePt, "0 0 0.75");

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%pos);
   %FFRObject.schedule(3200, delete);

   %dapad = addObject(%plyr, "MantaLaunchPad", %item.surfacePt);
   %dapad.playThread(0, "ambient");
   %dapad.schedule(2800, setDamageState, Destroyed);
   %dapad.schedule(3000, delete);

   %p = new SeekerProjectile()
   {
      dataBlock        = MantaSatMissile;
      initialDirection = "0 0 1"; //"0 0 -1";
      initialPosition  = %pos;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
   };
   MissionCleanup.add(%p);

   schedule(2900, 0, SpawnProjectile, %p, LinearFlareProjectile, MBDisplayCharge);
   commandToClient(%plyr.client, 'BottomPrint', "Satellite launch in 3 seconds.", 3, 1);
}

function MantaPad::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
   $TeamDeployedCount[%obj.team, "MantaSatPack"]--;
}

function MantaSupport::onDestroyed(%this, %obj, %prevState)
{
   Parent::onDestroyed(%this, %obj, %prevState);
   %obj.schedule(250, "delete");
   $MantaSat[%obj.team] = "";
}

function MantaPad::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function MANTATelePad::clearTeleTimeout(%data, %pad)
{
   %pad.teleTimeout = 0;
}

function MANTATelePad::finishTele(%data, %pad, %pl)
{
   %pl.setMoveState(false);
   %pad.teleTimeout = 1;
   %data.schedule(6000, clearTeleTimeout, %pad);

   %pl.setPosition(%pad.getPosition());
   %pad.setThreadDir(1, false);
   %pad.playThread(1, "activate");
   %pad.teletgt.setThreadDir(1, false);
   %pad.teletgt.playThread(1, "activate");
   %pl.teleportEndFX();
   %pad.playAudio($PlaySound, UnTeleportSound);
}

function MANTATelePad::startTele(%data, %pad, %pl)
{
   %pl.setMoveState(true);
   %pad.TeleTimeout = 1;
   %data.schedule(6000, clearTeleTimeout, %pad);

   %pl.teleportStartFX();
   %pad.setThreadDir(1, true);
   %pad.teletgt.setThreadDir(1, true);
   %pad.playThread(1, "activate");
   %pad.teletgt.playThread(1, "activate");
   %pad.playAudio($PlaySound, TeleportSound);
   %data.schedule(2000, finishTele, %pad.teletgt, %pl);
}

function checkForDefBaseTele(%obj)
{
   InitContainerRadiusSearch(%obj.getPosition(), 10000, $TypeMasks::StaticObjectType);

   while((%pad = ContainerSearchNext()) != 0)
   {
      if(%pad.isDefTele)
         if(%pad != %obj)
            if(%pad.team == %obj.team)
               return %pad;

      else continue;
   }
   return false;
}

function MANTATelePad::onCollision(%data, %obj, %col)
{
   if(%col.getDataBlock().className !$= "Armor" || %col.getState() $= "Dead")
      return;

   else if(%obj.teleTimeout > 0)
   {
      messageClient(%col.client, 'MsgTeleIsCool', '\c2Telepad is recharging.');
      return;
   }

   if(isObject(%obj.teletgt))
      %pad = %obj.teletgt;
   else
      %pad = checkForMANTATele(%obj);

   if(!isObject(%pad))
   {
      if(%obj.isMantaBasedPad)
         messageClient(%col.client, 'MsgMantaTeleNonExistB', '\c2Cannot teleport; There is no Defense Base to teleport to.');
      else
         messageClient(%col.client, 'MsgMantaTeleNonExistM', '\c2Cannot teleport; There is no MANTA Ion Cannon to teleport to.');

      return;
   }
   else
   {
      %obj.teletgt = %pad;
      %col.setvelocity("0 0 0");
      %col.setPosition(%obj.getPosition());
      %data.startTele(%obj, %col);
   }
}

function MANTATelePad::onDestroyed(%this, %obj, %prevState)
{
//   %obj.teletgt.setDamageState(Destroyed);
   %obj.teletgt.teletgt = "";
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}