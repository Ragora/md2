function MechWNoMechWeapon::fire(%data, %obj, %slot)
{
   // This space left intentionally blank
}

function MechWStarHammer::fire(%data, %obj, %slot)
{

               %p = new LinearProjectile()
               {
                    dataBlock        = SBRocket;
                    initialDirection = %obj.getMuzzleVector(%slot);
                    initialPosition  = %obj.getMuzzlePoint(%slot);
                    sourceObject     = %obj;
                    sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               spawnProjectile(%p, LinearFlareProjectile, MBDisplayCharge);
               projectileTrail(%p, 150, LinearFlareProjectile, ReaverCharge, true);
               %obj.play3d(StarHammerFireSound);
}

function MechWCometCannon::fire(%data, %obj, %slot)
{
               %p = new LinearFlareProjectile()
               {
                    dataBlock        = CometCannonBlast;
                    initialDirection = %obj.getMuzzleVector(%slot);
                    initialPosition  = %obj.getMuzzlePoint(%slot);
                    sourceObject     = %obj;
                    sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               spawnProjectile(%p, LinearFlareProjectile, CCDisplayCharge);
               %obj.play3d(StarHammerFireSound);
               %obj.play3D(AAFireSound);
}

function MechWBlasterCannon::fire(%data, %obj, %slot)	// +[soph]
{							// +
               %vector = %obj.getMuzzleVector( %slot ) ;
               %point = %obj.getMuzzlePoint( %slot ) ;	// +
               %p = new LinearFlareProjectile()		// +
               {					// +
                    dataBlock         = TridentBlasterBall ;
                    initialDirection  = %vector ;	// +
                    initialPosition   = %point ;	// +
                    sourceObject      = %obj ;		// +
                    sourceSlot        = %slot ;		// +
                    ignoreReflections = true ;		// +
               } ;					// +
               MissionCleanup.add( %p ) ;       	// +
               %p = new LinearFlareProjectile()		// +
               {					// +
                    dataBlock         = TridentBlasterBall0 ;
                    initialDirection  = %vector ;	// +
                    initialPosition   = %point ;	// +
                    sourceObject      = %obj ;		// +
                    sourceSlot        = %slot ;		// +
                    ignoreReflections = true ;		// +
               } ;					// +
               MissionCleanup.add( %p ) ;		// +
               %p = new LinearFlareProjectile()		// +
               {					// +
                    dataBlock         = TridentBlasterBall1 ;
                    initialDirection  = %vector ;	// +
                    initialPosition   = %point ;	// +
                    sourceObject      = %obj ;		// +
                    sourceSlot        = %slot ;		// +
                    ignoreReflections = true ;		// +
               } ;					// +
               MissionCleanup.add( %p ) ;		// +
               %obj.play3D( PulseCannonFire ) ;		// +
               spawnProjectile( %p , LinearFlareProjectile , MegaBlasterDispCharge ) ;
}							// +[/soph]

function MechWAC5::fire(%data, %obj, %slot)
{
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 3);

               %p = new TracerProjectile()
               {
                    dataBlock        = AutoCannonBullet;
                    initialDirection = %vector;
                    initialPosition  = %obj.getMuzzlePoint(%slot);
                    sourceObject     = %obj;
                    sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               schedule( 1504 + getRandom( 4 ) * 32 , 0 , AtCCleanup , %p ) ;	// ~450m range +soph

               %obj.play3D(ChainTFireSound);
}

function MechWAutogun::fire( %data , %obj , %slot )	// +[soph]
{							// +
               %vector = calcSpreadVector( %obj.getMuzzleVector( %slot ) , 3) ;
               %p = new TracerProjectile()		// +
               {					// +
                    dataBlock        = TurretChaingunBullet ;
                    initialDirection = %vector ;	// +
                    initialPosition  = %obj.getMuzzlePoint( %slot ) ;
                    sourceObject     = %obj ;		// +
                    sourceSlot       = %slot ;		// +
               } ;					// +
               MissionCleanup.add( %p ) ;		// +
               %obj.play3D( ChaingunImpact2 ) ;		// +
}							// +[/soph]

function MechWSpikeCannon::fire(%data, %obj, %slot)
{
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 1);

               %p = new TracerProjectile()
               {
                   dataBlock        = VehicleSpikeBolt;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               %obj.play3d(OBLFireSound);
}

function MechWPlasmaCannon::fire(%data, %obj, %slot)
{
               %p = new LinearFlareProjectile()
               {
                   dataBlock        = PlasmaCannonBolt;
                   initialDirection = %obj.getMuzzleVector(%slot);
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               %obj.play3d(PBLFireSound);
}

function MechWSRM4::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::SRM4;

               MechStaggerFire(%obj, %slot, 250, 4, %type);
               MechSetSlotCooldown(%obj, %slot, $MechWeaponFireTime[%type]);
               MechUseAmmo(%obj, %slot, -1);
}

function MechWSRM6::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::SRM6;

               MechStaggerFire(%obj, %slot, 150, 6, %type);
               MechSetSlotCooldown(%obj, %slot, $MechWeaponFireTime[%type]);
               MechUseAmmo(%obj, %slot, -1);
}

function MechWLRM5::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::LRM5;

               %obj.play3d(MechFire2Sound);
               MechStaggerFire(%obj, %slot, 200, 5, %type);
               MechSetSlotCooldown(%obj, %slot, $MechWeaponFireTime[%type]);
               MechUseAmmo(%obj, %slot, -1);
}

function MechWLRM10::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::LRM10;

               %obj.play3d(MechFire2Sound);
               MechStaggerFire(%obj, %slot, 100, 10, %type);
               MechSetSlotCooldown(%obj, %slot, $MechWeaponFireTime[%type]);
               MechUseAmmo(%obj, %slot, -1);
}

function MechWLRM10::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::LRM10;

               %obj.play3d(MechFire2Sound);
               MechStaggerFire(%obj, %slot, 100, 10, %type);
               MechSetSlotCooldown(%obj, %slot, $MechWeaponFireTime[%type]);
               MechUseAmmo(%obj, %slot, -1);
}

function MechWRaptor::fire( %data , %obj , %slot )		// +[soph]
{								// +
               %obj.play3d( MechFire2Sound ) ;			// +
               %p = new SeekerProjectile()			// +
               {						// +
                   dataBlock        = "BomberCruiseMissile" ;	// +
                   initialDirection = %obj.getMuzzleVector( %slot ) ;
                   initialPosition  = %obj.getMuzzlePoint( %slot ) ;
                   sourceObject     = %obj ;			// +
                   sourceSlot       = %slot ;			// +
                   damageMod        = 1.75 ;			// +
                   rangeMod         = 0.75 ;			// +
               } ;						// +
               MissionCleanup.add( %p ) ;			// +
               %target = %obj.getLockedTarget() ;		// +
               if( %target )					// +
               {						// +
                    if( %target.getDatablock().jetfire )	// +
                         assignTrackerTo( %target , %p ) ;	// +
                    else					// +
                         %p.schedule( 500 , setObjectTarget , %target ) ;
               }						// +
               else if( %obj.isLocked() )			// +
                    %p.setPositionTarget( %obj.getLockedPosition() ) ;
               else						// +
                    %p.setNoTarget() ;				// +
}								// +[/soph]

function MechWPhaserCannon::fire(%data, %obj, %slot)
{
//               %p = new LinearFlareProjectile()		// -[soph]
//               {						// -
//                  dataBlock        = PCRCannonBolt;		// -
//                  initialDirection = %obj.getMuzzleVector(%slot);
//                  initialPosition  = %obj.getMuzzlePoint(%slot);
//                  sourceObject     = %obj;			// -
//                  sourceSlot       = %slot;			// -
//               };						// -
//               MissionCleanup.add(%p);			// -
//								// -
//               %obj.play3d(PCRFireSound);			// -[/soph]

								// +[soph]
               MechStaggerFire( %obj , %slot , 96 , 3 , $MechWeapon::PhaserCannon ) ;
               MechSetSlotCooldown( %obj , %slot , $MechWeaponFireTime[ $MechWeapon::PhaserCannon ] ) ;
               MechUseAmmo( %obj , %slot , -1 ) ;		// +[/soph]
}

function MechWPPC::fire(%data, %obj, %slot)
{
               %p = new LinearFlareProjectile()
               {
                  dataBlock        = PPCBolt;
                  initialDirection = %obj.getMuzzleVector(%slot);
                  initialPosition  = %obj.getMuzzlePoint(%slot);
                  sourceObject     = %obj;
                  sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               %obj.play3d(PBWFireSound);
}

function MechWUltraAC10::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::UltraAC10;

               %obj.play3d(MortarFireSound);
               MechStaggerFire(%obj, %slot, 0, 4, %type);	// 0, 3, %type); -soph
               MechUseAmmo(%obj, %slot, 9);
}

function MechWKM::fire(%data, %obj, %slot)
{
               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 5);

               %p = new TracerProjectile()
               {
                   dataBlock        = KMPlusBullet;	// = MoonBikeEnergyBullet; -soph
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);

               %obj.play3d(BomberTurretFireSound);
}

function MechWPBW::fire(%data, %obj, %slot)
{
               %t = new TargetProjectile()
               {
                       dataBlock        = "TPBWLaserProj";
                       initialDirection = %obj.getMuzzleVector(%slot);
                       initialPosition  = %obj.getMuzzlePoint(%slot);
                       sourceObject     = %obj;
                       sourceSlot       = %slot;
                       vehicleObject    = 0;
               };
               MissionCleanup.add(%t);
//               %t.schedule(350, delete);				// -soph

              // %p = new SniperProjectile()				// -[soph]
              // {							// - now uses TPBWFragment loop
              //     dataBlock        = "ParticleBeam";			// -
              //     initialDirection = %obj.getMuzzleVector(%slot);	// -
              //     initialPosition  = %obj.getMuzzlePoint(%slot);	// -
              //     sourceObject     = %obj;				// -
              //     damageFactor     = 1.66;				// -
              //     damageForce      = 4800;				// -
              //     sourceSlot       = %slot;				// -
              // };							// -
              // MissionCleanup.add(%p);				// -[/soph]

              MPBWFragment( %data , %obj , %slot , 19 , %t );		// 16 up from 10 // +soph
               %obj.play3d(PBWFireSound);
}

function MPBWFragment( %data , %obj , %slot , %fragments , %t )	// +[soph]
{								// +
   %p = new SniperProjectile() {				// +
      dataBlock        = "TParticleBeam" ;			// +
      initialDirection = %obj.getMuzzleVector( %slot ) ;	// +
      initialPosition  = %obj.getMuzzlePoint( %slot ) ;		// +
      sourceObject     = %obj ;					// +
      damageFactor     = 0.235 ;				// + less than standard but longer
      damageForce      = 930 ;					// + same force, greater range
      sourceSlot       = %slot ;				// +
   } ;								// +
   MissionCleanup.add( %p ) ;					// +
   serverPlay3D( underwaterDiscExpSound , %obj.getTransform() ) ;
								// +
   // AI hook							// +
   if( %obj.client )						// +
      %obj.client.projectile = %p ;				// +
   if( %fragments-- > 0 )					// +
      schedule( 30 , 0 , MPBWFragment , %data , %obj , %slot , %fragments , %t ) ;
   else								// +
      %t.delete() ;						// +
}								// +[/soph]

function MechWGauss::fire(%data, %obj, %slot)
{
               %p = new LinearProjectile()
               {
                   dataBlock        = "MechRail";
                   initialDirection = %obj.getMuzzleVector(%slot);
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
               projectileTrail(%p, 50, LinearFlareProjectile, GaussTrailCharge, true);
               %obj.play3d(MILFireSound);
}

function MechWCoilgun::fire(%data, %obj, %slot)			// +[soph]
{								// +
               %p = new LinearProjectile()			// +
               {						// +
                   dataBlock        = "MassDriverBullet" ;	// +
                   initialDirection = %obj.getMuzzleVector( %slot ) ;
                   initialPosition  = %obj.getMuzzlePoint( %slot ) ;
                   sourceObject     = %obj ;			// +
                   sourceSlot       = %slot ;			// +
               };						// +
               MissionCleanup.add( %p ) ;			// +
               %obj.play3d( SRFireSound ) ;			// +
}								// +[/soph]

function MechWFlamer::fire(%data, %obj, %slot)
{
               %p = new LinearProjectile()
               {
                   dataBlock        = "RAXXFlame";
                   initialDirection = %obj.getMuzzleVector(%slot);
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
}

function MechWLPLAS::fire(%data, %obj, %slot)
{
    %type = $MechWeapon::LPLAS;

               %t = new TargetProjectile()
               {
                       dataBlock        = "MedLaserProj";
                       initialDirection = %obj.getMuzzleVector(%slot);
                       initialPosition  = %obj.getMuzzlePoint(%slot);
                       sourceObject     = %obj;
                       sourceSlot       = %slot;
                       vehicleObject    = 0;
               };
               MissionCleanup.add(%t);
               %t.schedule(1100, delete);

               MechStaggerFire(%obj, %slot, 300, 3, %type);

}

function MechWLLAS::fire(%data, %obj, %slot)
{
               %p = new SniperProjectile()
               {
                   dataBlock        = MechPunchBeam;	// = MechPulseBeam; -soph
                   initialDirection = %obj.getMuzzleVector(%slot);
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   damageFactor     = 0.9;
                   sourceSlot       = %slot;
                   noHeadshot       = true;
               };
               %p.setEnergyPercentage(0.0);
               MissionCleanup.add(%p);

               %t = new TargetProjectile()
               {
                       dataBlock        = "HeavyLaserProj";
                       initialDirection = %obj.getMuzzleVector(%slot);
                       initialPosition  = %obj.getMuzzlePoint(%slot);
                       sourceObject     = %obj;
                       sourceSlot       = %slot;
                       vehicleObject    = 0;
               };
               MissionCleanup.add(%t);
               %t.schedule(500, delete);

               %obj.play3d(SniperRifleFireSound);
}

function MechWBlastMortar::fire(%data, %obj, %slot)
{

               %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 5);

               %p = new GrenadeProjectile()
               {
                   dataBlock        = MechMortar;
                   initialDirection = %vector;
                   initialPosition  = %obj.getMuzzlePoint(%slot);
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%p);
               %obj.play3d(SRFireSound);
               %obj.play3D(MechFire2Sound);
               %t = new GrenadeProjectile()
               {
                   dataBlock        = MechMortarPomf;
                   initialDirection = %vector;
                   initialPosition  = %p.initialPosition;
                   sourceObject     = %obj;
                   sourceSlot       = %slot;
               };
               MissionCleanup.add(%t);
}

//function MechWIonCannon::fire(%data, %obj, %slot)
//{
//        %point = vectorAdd(%obj.getMuzzlePoint(%slot), vectorScale(%obj.getMuzzleVector(%slot), 2));
//
//               %p = new EnergyProjectile()
//               {
//                  dataBlock        = SmallIONBeam;
//                  initialDirection = %obj.getMuzzleVector(%slot);
//                  initialPosition  = %point;
//                  sourceObject     = %obj;
//                  sourceSlot       = %slot;
//                  ignoreReflections = true;
//               };
//               MissionCleanup.add(%p);
//
//               %h = new LinearFlareProjectile()
//               {
//                  dataBlock        = SmallIONBeamHead;
//                  initialDirection = %obj.getMuzzleVector(%slot);
//                  initialPosition  = %point;
//                  sourceObject     = %obj;
//                  sourceSlot       = %slot;
//                  ignoreReflections = true;
//               };
//               MissionCleanup.add(%h);
//
//          %obj.play3d(PCRFireSound);
//          spawnProjectile(%p, LinearFlareProjectile, TBDisplayCharge);
//          %obj.play3D(AAFireSound);
//          projectileTrail(%h, 50, LinearFlareProjectile, IonCannonTrailCharge);
//}

function MechWShockBlaster::fire(%data, %obj, %slot)
{
               %p = new LinearFlareProjectile()
               {
                  dataBlock        = ShockBlasterBolt;
                  initialDirection = %obj.getMuzzleVector(%slot);
                  initialPosition  = %obj.getMuzzlePoint(%slot);
                  sourceObject     = %obj;
                  sourceSlot       = %slot;
                  ignoreReflections = true;
                  damageMod        = 1.0 ;	// +soph
               };
               MissionCleanup.add(%p);

          %obj.play3d(PBLFireSound);
          %obj.play3D(BlasterFireSound);
          %p.lanceThread = true;
          SBLanceRandomThread(%p);
}

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }
