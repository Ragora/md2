if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Meltdown2 Extra weapons data

function projObserveCheck(%obj, %cl)
{
   if(isObject(%cl))
   {
      if(isObject(%obj))
         schedule(1250, 0, "projObserveCheck", %obj, %cl);
      else
         %cl.setControlObject(%cl.player);
   }
}

function assignTrackerTo(%obj, %proj)
{
   if(!%obj.lockedMissileCount)
      %obj.lockedMissileCount = 0;

   %proj.setObjectTarget(%obj);
   %obj.lockedMissile[%obj.lockedMissileCount] = %proj;
   %obj.lockedMissileCount++;
}

function checkTrackerObj(%obj, %pl)
{
   if(!%obj.lockedMissileCount)
      return;

   for(%i = 0; %i < %obj.lockedMissileCount; %i++)
   {
      if(%obj.lockedMissile[%i])
      {
         if(isObject(%obj.lockedMissile[%i]))
         {
            %pl.setHeat(1.0);
            %obj.lockedMissile[%i].setObjectTarget(%pl);
         }
      }
   }
}

function setLoadingFlag(%data, %obj, %time) // modified so we can put in time vars (laser cannon has no reload time)
{
   if(!%obj.wep_reloading[%data])
   {
      %obj.wep_reloading[%data] = true;
      schedule((%len = %time ? %time : %data.reloadTime), %obj, clearLoadingFlag, %data, %obj);
   }
}

function clearLoadingFlag(%data, %obj)
{
   if(isObject(%obj))
      if(%obj.wep_reloading[%data])
         %obj.wep_reloading[%data] = false;
}

function MissileLauncherImage::onFire(%data,%obj,%slot)
{
  // if(%obj.wep_reloading[%data])			// -soph
  //    return;						// -soph

//   %data.lightStart = getSimTime();
   if( %obj.srm4active && !%obj.packFireLink )		// (%obj.srm4active) -soph
   {
	%obj.packFireLink = true ;			// +soph
        %obj.setImageTrigger($BackpackSlot, true);
        %obj.setImageTrigger(0, false);
       // setLoadingFlag(%data, %obj);			// -soph
        %obj.setImageTrigger($BackpackSlot, false);
       // return;					// -soph
	%obj.packFireLink = false ;			// +soph
   }

   %useEnergyObj = %obj.getObjectMount();
   %mountDamageMod = 0;

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %weapon = %obj.getMountedImage(0).item;

//         if(%obj.client.mode[%weapon] == 0)		// -[soph]
//               %projectile = "ShoulderMissile";
//         else if(%obj.client.mode[%weapon] == 1)
//               %projectile = "FastMissile";
//         else if(%obj.client.mode[%weapon] == 2)
//               %projectile = "EMPMissile";
//         else if(%obj.client.mode[%weapon] == 3)
//               %projectile = "IncendiaryMissile";
//         else if(%obj.client.mode[%weapon] == 4)
//               %projectile = "MegaMissile";               
//         else if(%obj.client.mode[%weapon] == 5)
//               %projectile = "ShoulderMissile";	// -[/soph]
   switch ( %obj.client.mode[ %weapon ] )		// +[soph]
   {							// +
         case 1:					// +
               %projectile = "FastMissile" ;		// +
         case 2:					// +
               %projectile = "EMPMissile" ;		// +
         case 3:					// +
               %projectile = "IncendiaryMissile" ;	// +
         case 4:					// +
               %projectile = "MegaMissile" ;        	// +
         case 6:					// +
               handleClusterLaunch( %data , %obj , %slot ) ;
               return;					// +
         default: 					// +
               %projectile = "ShoulderMissile" ;	// +
   }							// +[/soph]

      %p = new (%data.projectileType)() {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = 0;
      };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo, 1);

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;
     
   %target = %obj.getLockedTarget();

   if( %target && %obj.client.mode[%weapon] != 5)	// if(%target) -soph
   {
      if(%target.getDatablock().jetfire) // || %target.getDatablock().MegaDisc)
         assignTrackerTo(%target, %p);
      else
         %p.setObjectTarget(%target);
   }
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget();
    //  rBeginSearchPlayer(%p);

    if(%obj.client.mode[%weapon] == 5) // Laser Targeting mode
    {
        if(isObject(%target))
            %target.homingCount--;
        %p.damageMod *= 0.8;				// +soph    
        %p.setObjectTarget(%obj.tetherBeacon);
    }
    else
        MissileSet.add(%p);    

   if(isObject(%target))
       %p.noPlayerEMP = !(%obj.client.mode[%weapon] == 2 && %target) && (%target.getType() & $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType);
   
  // setLoadingFlag(%data, %obj);			// -soph
}

function handleClusterLaunch( %data , %obj , %slot )	// +[soph]
{
      %p = new SeekerProjectile() {
         dataBlock        = "ShoulderMissile";
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = 0;
      };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo, 1);

   if( %obj.damageMod )
     %p.damageMod = %obj.damageMod * 3 / 4 ;
   else
     %p.damageMod = 3 / 4 ;

   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;
     
   %target = %obj.getLockedTarget();

   if(%target)
   {
      if(%target.getDatablock().jetfire)
         assignTrackerTo(%target, %p);
      else
         %p.setObjectTarget(%target);
   }
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget();

   MissileSet.add(%p);    

//   if(isObject(%target))	// no longer meaningful -soph
//       %p.noPlayerEMP = !(%obj.client.mode[%obj.getMountedImage(0).item] == 2 && %target) && (%target.getType() & $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType);
   
   schedule( 1150 , 0 , clusterMissileBurst , %data , %obj , %target, %obj.isLocked() ? %obj.getLockedPosition() : false , %p );
}							// [/soph]

function clusterMissileBurst( %data , %obj , %target , %lockPosition , %projectile )	// +[soph]
{
   if( !%projectile || !isObject( %projectile ) || %projectile.exploded )
       return;

   if( ( !%target || !isObject( %target ) ) && !%lockPosition )
      return ; 

   %position = %projectile.getWorldBoxCenter();
   %vector = vectorNormalize( vectorSub( %position , %projectile.initialPosition ) ) ;

   %source = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add( %source );
   %source.setPosition( %position  );
   %source.schedule(32, delete);

   for(%i = 0; %i < 4; %i++)
   {
      if( %i )
         %newVector = calcSpreadVector( %vector , 384 );
      else
         %newVector = %vector;
      %p = new SeekerProjectile() {
         dataBlock        = "ConcussionMissile";
         initialDirection = %newVector;
         initialPosition  = %position;
         sourceObject     = %source;
         sourceSlot       = 0;
         vehicleObject    = 0;
         bkSourceObject   = %obj;
      };
      if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
         %obj.lastProjectile.delete();

      %obj.lastProjectile = %p;
         %obj.deleteLastProjectile = %data.deleteLastProjectile;
      MissionCleanup.add(%p);

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %p.damageMod = %projectile.damageMod * 4 / 3;

      if( %projectile.ignoreReflect )
        %p.ignoreReflect = true;

      if( %target && isObject( %target ) )
      {
         if(%target.getDatablock().jetfire) 
            assignTrackerTo(%target, %p);
         else
            %p.setObjectTarget(%target);
      }
      else if( %lockPosition )
         %p.setPositionTarget( %lockPosition );
      else
         %p.setNoTarget();

      MissileSet.add(%p);         
   }
   %projectile.delete();
}													// [/soph]

function MissileLauncherImage::onWetFire(%data, %obj, %slot)
{
  // if(%obj.wep_reloading[%data])			// -soph
  //    return;						// -soph

   if(%obj.srm4active)
   {
        %obj.setImageTrigger($BackpackSlot, true);
        %obj.setImageTrigger(0, false);
       // setLoadingFlag(%data, %obj);			// -soph
        %obj.setImageTrigger($BackpackSlot, false);
       // return;					// -soph
   }

//   %data.lightStart = getSimTime();
   %useEnergyObj = %obj.getObjectMount();
   %mountDamageMod = 0;

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %weapon = %obj.getMountedImage(0).item;

//         if(%obj.client.mode[%weapon] == 0)		// -[soph]
//               %projectile = "ShoulderMissile";
//         else if(%obj.client.mode[%weapon] == 1)
//               %projectile = "FastMissile";
//         else if(%obj.client.mode[%weapon] == 2)
//               %projectile = "EMPMissile";
//         else if(%obj.client.mode[%weapon] == 3)
//               %projectile = "IncendiaryMissile";
//         else if(%obj.client.mode[%weapon] == 4)
//               %projectile = "MegaMissile";               
//         else if(%obj.client.mode[%weapon] == 5)
//               %projectile = "ShoulderMissile";	// -[/soph]
   switch ( %obj.client.mode[%weapon] )		// +[soph]
   {
         case 0:
               %projectile = "ShoulderMissile";
         case 1:
               %projectile = "FastMissile";
         case 2:
               %projectile = "EMPMissile";
         case 3:
               %projectile = "IncendiaryMissile";
         case 4:
               %projectile = "MegaMissile";        
         case 5:
               %projectile = "ShoulderMissile";
         case 6:
               handleClusterLaunch( %data , %obj , %slot );
               return;					// +[/soph]
   }

      %p = new (%data.projectileType)() {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = 0;
      };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo,1);

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;
     
   %p.setObjectTarget(0);

    if(%obj.client.mode[%weapon] == 5) // Laser Targeting mode
        %p.setObjectTarget(%obj.tetherBeacon);
    else
        MissileSet.add(%p);    

  // setLoadingFlag(%data, %obj);			// -soph
}

function MissileBarrelLarge::onFire(%data,%obj,%slot)
{
   %p = Parent::onFire(%data,%obj,%slot);

   if (%obj.getControllingClient())
   {
      // a player is controlling the turret
      %target = %obj.getLockedTarget();
   }
   else
   {
      // The ai is controlling the turret
      %target = %obj.getTargetObject();
   }

   if(%target)
   {
      if(%target.getDatablock().jetfire || %target.getDatablock().MegaDisc)
         assignTrackerTo(%target, %p);
      else
         %p.setObjectTarget(%target);
   }
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setNoTarget(); // set as unguided. Only happens when itchy trigger can't wait for lock tone.
}

function ELFGunImage::onFire(%data, %obj, %slot)
{
   %data.lightStart = getSimTime();

   %useEnergyObj = %obj.getObjectMount();
   %mountDamageMod = 0;

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %energy = %obj.getEnergyLevel();
   if(%energy < %data.minEnergy)
         return;

   if(%obj.isCloaked())
      return;

   %weapon = %obj.getMountedImage(0).item;

         if(%obj.client.mode[%weapon] == 0)
               %projectile = "BasicELF";
         else if(%obj.client.mode[%weapon] == 1)
               %projectile = "TractorElf";
         else if(%obj.client.mode[%weapon] == 2)
               %projectile = "PullElf";
         else if(%obj.client.mode[%weapon] == 3) //-Nite- fins uberelf theres some stuff done there too.
               %projectile = "UberElf";
         else if(%obj.client.mode[%weapon] == 4)
              %projectile = "PowerElf";

      %p = new (%data.projectileType)() {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

//   %obj.setEnergyLevel(%energy - %data.fireEnergy);

     if(!%p.hasTarget())
     {
          %obj.playAudio(0, ELFFireWetSound);

          %q = new ShockLanceProjectile()
          {
               dataBlock        = "BasicShocker";
               initialDirection = %obj.getMuzzleVector(%slot);
               initialPosition  = %obj.getMuzzlePoint(%slot);
               sourceObject     = %obj;
               sourceSlot       = %slot;
          };

          MissionCleanup.add(%q);
     }
   return %p;
}

function TargetingLaserImage::onMount(%this,%obj,%slot)
{
   %size = %obj.getArmorSize();
   %type = %obj.getArmorType();

   if(%type & $ArmorMask::Engineer)
   {
      if(%obj.keychain[%size] == 0)
      {
         %obj.unmountImage(%slot);
         %obj.mountImage(EBarrelSwapperImage, %slot);
         commandToClient(%obj.client, 'setRepairReticle');
         commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a barrel swapper\nThis combines all the turret barrel packs into one gun. Each mode is a different barrel.", 4, 2); // This should be interesting.....
      }
      else if(%obj.keychain[%size] == 1)
      {
         %obj.unmountImage(%slot);
         %obj.mountImage(ReassemblerImage, %slot);
         commandToClient(%obj.client, 'setRepairReticle');
         commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a reassembler\nThis is the Engineer's version of a repair pack. It repairs a lot faster.", 4, 2); // This should be interesting.....
      }
      else if(%obj.keychain[%size] == 2)
      {
         %obj.unmountImage(%slot);
         %obj.mountImage(EDisassemblerImage, %slot);
         commandToClient(%obj.client, 'setRepairReticle');
         commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a disassembler\nThis is the Engineer's special deployable object remover. Use with care.", 4, 2); // This should be interesting.....
      }
      else if(%obj.keychain[%size] == 3)
      {
         %obj.unmountImage(%slot);
         %obj.mountImage(TargetingLaserMountImage, %slot);
         commandToClient(%obj.client, 'setRepairReticle');
         commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a targeting laser\nHold steady on a target to plant a beacon", 4, 2);	// This is nothing more than your standard laser pointer." -soph	// This should be interesting.....
      }

      %obj.keychain[%size]++;

      if(%obj.keychain[%size] > 3)
         %obj.keychain[%size] = 0;

      return;
   }

   if(%obj.keychain[%size] == 0)
   {
      %obj.unmountImage(%slot);

      if(%type & $ArmorMask::BattleAngel)
          BACSLSelect(%obj, %slot);
      else
      {
          %obj.mountImage(TargetingLaserOptionImage, %slot);
          commandToClient(%obj.client, 'setRepairReticle');
         // commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a class-specific targeting laser (CSL)\nThis item is class-specific, meaning each class has it do something different.", 4, 2); // This should be interesting.....
          if( %type & $ArmorMask::Light )	// +[soph]
              commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using EMP Blast" NL "Shut down enemy shields and energy systems" NL "This weapon is specific to your armor, each class has its own" , 5 , 3 );
          else if( %type & $ArmorMask::Medium )
              commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using Personal Repulsor" NL "Reflect weapons fire; may explode if energy is low" NL "This weapon is specific to your armor, each class has its own" , 5 , 3 );
          else if( %type & $ArmorMask::Heavy )
              commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using Stimpack" NL "Pull the trigger to boost energy generation, damage dealt and damage taken" NL "This weapon is specific to your armor, each class has its own" , 5 , 3 );
          else if( %type & $ArmorMask::Blastech )
              commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using Drain ELF" NL "Leech energy from targets to replenish your own" NL "This weapon is specific to your armor, each class has its own" , 5 , 3 );
          else
              commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using a class-specific targeting laser (CSL)\nThis item is class-specific, meaning each class has it do something different." , 4 , 2 );
      }
   }
   else if(%obj.keychain[%size] == 1)
   {
      %obj.unmountImage(%slot);
      %obj.mountImage(TargetingLaserMountImage, %slot);
      commandToClient(%obj.client, 'setRepairReticle');
      commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a standard targeting laser\nHold steady on a target to plant a beacon", 4, 2);	// This is nothing more than your standard laser pointer. -soph	// This should be interesting.....
   }

   %obj.keychain[%size]++;

   if(%obj.keychain[%size] > 1)
      %obj.keychain[%size] = 0;
}

function setSoulEffectTimeout(%obj)
{
     %obj.soulEffectTimeout = false;
}

function IonCSLSelect(%obj, %slot)
{
     if(%obj.efficiencyMod)
     {
          if(%obj.soulCount < 1)
          {
               commandToClient(%obj.client, 'BottomPrint', "Not enough souls to thrust.", 4, 1);
               return;
          }

          if(%obj.soulEffectTimeout)
          {
               commandToClient(%obj.client, 'BottomPrint', "Soul Thrust is still cooling down.", 4, 1);
               return;
          }

          %obj.soulCount -= 1;

          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);

          calculateSoulBonuses(%obj, %obj.soulCount);

          serverPlay3D(PlasmaFireSound, %obj.getTransform());
          %force = %obj.getMass();
          createLifeEmitter(vectorAdd(%obj.getWorldBoxCenter(), "0 0 -0.5"), "SatchelExplosionSmokeEmitter", 300);
          %vec = vectorScale(%obj.getMuzzleVector(0), 45); // Adding to the current velocity..
          %newVec = VectorScale(%vec, %force);  //vector scale
          %obj.applyImpulse(%obj.getTransform(), %newVec); //apply speed
          %obj.soulEffectTimeout = true;
          schedule(15000, %obj, setSoulEffectTimeout, %obj);
          return;
     }
     else if(%obj.powerMod)
     {
          if(%obj.soulCount < 1)	// if(%obj.soulCount < 3) -soph
          {
               commandToClient(%obj.client, 'BottomPrint', "Not enough souls to create a Soul Shot.", 4, 1);
               return;
          }

          if(%obj.soulShotActive)
               return;

          %obj.soulCount -= 1;	// %obj.soulCount -= 3; -soph

          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);

          calculateSoulBonuses(%obj, %obj.soulCount);

          serverPlay3D(PBWSwitchSound, %obj.getTransform());
          createLifeEmitter(%obj.getMuzzlePoint(%slot), "MBExplosionEmitter", 300);
          activateDeploySensorRed(%obj);
          %obj.soulShotActive = true;
          commandToClient(%obj.client, 'BottomPrint', "Soul shot active, next Soul attack will deal 200% damage.", 4, 1);
          return;
     }
     else if(%obj.guardianMod)
     {
          if(%obj.soulCount < 4)
          {
               commandToClient(%obj.client, 'BottomPrint', "Not enough souls to activate absorb field.", 4, 1);
               return;
          }

          if(%obj.soulEffectTimeout)
          {
               commandToClient(%obj.client, 'BottomPrint', "Soul Absorb is still cooling down.", 4, 1);
               return;
          }

          %obj.soulCount -= 4;

          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);

          calculateSoulBonuses(%obj, %obj.soulCount);

          initGuardianCSL(%obj);

          %obj.soulEffectTimeout = true;
          schedule(30000, %obj, setSoulEffectTimeout, %obj);
          return;
     }
     else if(%obj.archMageMod)
     {
          if(%obj.soulCount < 2)
          {
               commandToClient(%obj.client, 'BottomPrint', "Not enough souls to create Soul Pulse Wave.", 4, 1);
               return;
          }

          if(%obj.soulEffectTimeout)
          {
               commandToClient(%obj.client, 'BottomPrint', "Soul Pulse Wave is still cooling down.", 4, 1);
               return;
          }

          %obj.soulCount -= 2;

          %graph = createGraph("*", %obj.maxSoulCount, (%obj.soulCount/%obj.maxSoulCount), "-");
          commandToClient(%obj.client, 'setAmmoHudCount', %graph);

          calculateSoulBonuses(%obj, %obj.soulCount);

          triggerPSIDetectWave(%obj.getDatablock(), %obj);

          %obj.soulEffectTimeout = true;
          schedule(10000, %obj, setSoulEffectTimeout, %obj);
          return;
     }
     else
     {
          %obj.mountImage(TargetingLaserOptionImage, %slot);
          commandToClient(%obj.client, 'setRepairReticle');
          commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a class-specific targeting laser (CSL)\nThis item is class-specific, meaning each class has it do something different.", 4, 2); // This should be interesting.....
     }
}

function initGuardianCSL(%obj)
{
     %obj.guardianAbsorb = true;
     %obj.playShieldEffect("0 0 1");
     schedule(10000, %obj, endGuardianCSL, %obj);
     serverPlay3D(JetfireTransform, %obj.getTransform());
     guardianCSLLoop(%obj);
     commandToClient(%obj.client, 'BottomPrint', ">> Soul Absorb ACTIVE <<", 4, 1);
}

function guardianCSLLoop(%obj)
{
     if(%obj.getState() !$= "Dead" && %obj.guardianAbsorb)
     {
          zapEffect(%obj, "BlueShift");

          schedule(250, %obj, guardianCSLLoop, %obj);
     }
}

function endGuardianCSL(%obj)
{
     %obj.guardianAbsorb = false;
     %obj.playShieldEffect("0 0 1");
     serverPlay3D(SynomiumOff, %obj.getTransform());

     commandToClient(%obj.client, 'BottomPrint', "Soul Absorb effect fades, 30 seconds to full recharge.", 4, 1);
}

function BACSLSelect(%obj, %slot)
{
     %obj.BAPackCSLTicker = 0 ;																									// +soph
     switch(%obj.BAPackID)
     {
          case 1:
               %obj.mountImage(ShockLanceImage, %slot);               // Shocklance
//               %obj.client.setWeaponsHudActive("Shocklance");
//               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Special Weapon Adaptor engaged...\nShocklance: Extended range module installed, all 3 modes available.", 4, 2);

          case 2:
//               %obj.mountImage(NosferatuImage, %slot);																						// -soph
               %obj.mountImage( TargetingLaserOptionImage , %slot ) ;																					// +soph
               commandToClient(%obj.client, 'setRepairReticle');      // Nosferatu
//               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a Nosferatu\nThis weapon drains enemy player armor like a repair gun set to reverse.", 4, 2);						// -soph
               commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using Mending Field Activator\nThis device produces a healing bubble, but drains all your energy.\nHold trigger to activate." , 4 , 3 ) ;	// +soph

          case 3:
               %obj.mountImage(TargetingLaserOptionImage, %slot);
               commandToClient(%obj.client, 'setRepairReticle');      // Shield extender
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a Shield Projector\nProjects shield onto target, you absorb 75% damage and they take 25%.", 4, 2);

          case 4:
               %obj.mountImage(TargetingLaserOptionImage, %slot);
               %obj.client.setWeaponsHudActive("Blaster");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using an Ammo Support Pulse\nUse this on a friendly target to replenish their ammo supply, 45 sec cooldown.", 4, 2); // This should be interesting.....

          case 5:
               %obj.mountImage(TargetingLaserOptionImage, %slot);
               %obj.client.setWeaponsHudActive("Blaster");
//               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a Defender Designator\nIncreases damage taken on enemy targets by 50% for 30 seconds.", 4, 2);						// -soph // This should be interesting.....
               commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using Turbocharger Selfdestruct Activator\nViolently overloads Turbocharger pack.\nHold trigger to activate." , 4, 3 ) ;			// +soph

          case 6:
               %obj.setImageTrigger($BackpackSlot, false);
               %time = getSimTime();			// +[soph]
               if( %obj.packWeaponTimeout > %time )
               {
                    %time = %obj.packWeaponTimeout - %time;
                    schedule(%time , 0 , "BAWeaponPackCSLMount" , %obj, %obj.BAPackID, CometCannonHHImage , %slot );
               }
               else					// +[/soph]
                    %obj.mountImage(CometCannonHHImage, %slot); // comet
               %obj.client.setWeaponsHudActive("Blaster");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Heavy Weapon Adaptor engaged\nComet Cannon: Energy usage -25%, Rate of Fire +25%.", 4, 2);

          case 7:
               %obj.setImageTrigger($BackpackSlot, false);
               %time = getSimTime();			// +[soph]
               if( %obj.packWeaponTimeout > %time )
               {
                    %time = %obj.packWeaponTimeout - %time;
                    schedule(%time , 0 , "BAWeaponPackCSLMount" , %obj, %obj.BAPackID, ShockBlasterHHImage , %slot );
               }
               else					// +[/soph]
                    %obj.mountImage(ShockBlasterHHImage, %slot); // shock
               %obj.client.setWeaponsHudActive("Plasma");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Heavy Weapon Adaptor engaged\nPhaser Blast Cannon: Weaker but faster travelling shot.", 4, 2);

          case 8:
               %obj.setImageTrigger($BackpackSlot, false);
               %time = getSimTime();			// +[soph]
               if( %obj.packWeaponTimeout > %time )
               {
                    %time = %obj.packWeaponTimeout - %time;
                    schedule(%time , 0 , "BAWeaponPackCSLMount" , %obj, %obj.BAPackID, StarHammerHHImage , %slot );
               }
               else					// +[/soph]
                    %obj.mountImage(StarHammerHHImage, %slot); // starhammer
               %obj.client.setWeaponsHudActive("MissileLauncher");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Heavy Weapon Adaptor engaged\nStarHammer: Dual-mode operation, allows for normal linear and heatseeking projectiles.", 4, 2);

          case 9:
               %obj.setImageTrigger($BackpackSlot, false);
               %time = getSimTime();			// [soph]
               if( %obj.packWeaponTimeout > %time )
               {
                    %time = %obj.packWeaponTimeout - %time;
                    schedule(%time , 0 , "BAWeaponPackCSLMount" , %obj, %obj.BAPackID, SRM4HHImage , %slot );
               }
               else					// +[/soph]
                    %obj.mountImage(SRM4HHImage, %slot); // streaksrm4
               %obj.client.setWeaponsHudActive("MissileLauncher");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Heavy Weapon Adaptor engaged\nStreak SRM4: Heat sensor built in, 75% heat required for lock.", 4, 2);

          case 10:
               %obj.setImageTrigger($BackpackSlot, false);
               %time = getSimTime();			// +[soph]
               if( %obj.packWeaponTimeout > %time )
               {
                    %time = %obj.packWeaponTimeout - %time;
                    schedule(%time , 0 , "BAWeaponPackCSLMount" , %obj, %obj.BAPackID, PulseCannonHHImage , %slot );
               }
               else					// +[/soph]
                    %obj.mountImage(PulseCannonHHImage, %slot); // dualpulsecannon
               %obj.client.setWeaponsHudActive("Mortar");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Heavy Weapon Adaptor engaged\nDual Pulse Cannons: Rapid fire spreadfire mode, uses lots of energy.", 4, 2);

          case 11:
               %obj.setImageTrigger($BackpackSlot, false);
               %time = getSimTime();			// +[soph]
               if( %obj.packWeaponTimeout > %time )
               {
                    %time = %obj.packWeaponTimeout - %time;
                    schedule(%time , 0 , "BAWeaponPackCSLMount" , %obj, %obj.BAPackID, DualGrenadeCannonHHImage , %slot );
               }
               else					// +[/soph]
                    %obj.mountImage(DualGrenadeCannonHHImage, %slot); // dualgrenadelauncher
               %obj.client.setWeaponsHudActive("GrenadeLauncher");
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Battle Angel Heavy Weapon Adaptor engaged\nDual Grenade Launcher: Cluster grenade, releases 3 grenades for 2 ammo.", 4, 2);

          case 12:
               %obj.mountImage(TargetingLaserOptionImage, %slot); // jammer
               %obj.client.setWeaponsHudActive("ELFGun");
//               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using an EM Pulse generator\nInstantly shuts down a Turret, Sensor, or Station for 10 seconds, 12 sec cooldown.", 4, 2);			// -soph
               commandToClient( %obj.client , 'BottomPrint' , "<font:times new roman:18>Now using Battle Angel Stealth Device\nBriefly cloaks the user.\nHold trigger to activate." , 4, 3 ) ;			// +soph

          case 13:
               %obj.mountImage(TargetingLaserOptionImage, %slot); // syn
               commandToClient(%obj.client, 'setRepairReticle');
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a Synomic Projector\nExtends your Synomium field onto the target, giving them the effect while active.", 4, 2);

          default:
               %obj.mountImage(TargetingLaserOptionImage, %slot);
               commandToClient(%obj.client, 'setRepairReticle');
               commandToClient(%obj.client, 'BottomPrint', "<font:times new roman:18>Now using a class-specific targeting laser (CSL)\nThis item is class-specific, meaning each class has it do something different.", 4, 2); // This should be interesting.....
     }
}

function BAWeaponPackCSLMount( %obj, %packID, %handHeld, %slot )	// +[soph]
{
   if( %obj.BAPackID == %packID )					// still has backpack - important!
      if( !%obj.getMountedImage( %slot ) )				// hasn't pulled out something else - just rude not to do this
         %obj.mountImage( %handHeld , %slot );
}									// +[/soph]

//function TargetingLaserImage::onUnmount(%this,%obj,%slot)
//{
//   echo("unmount");
//   Parent::onUnmount(%this,%obj,%slot);
//   %obj.keychain[%obj.getArmorSize()] = 0;
//}

function TargetingLaserMountImage::onFire(%data,%obj,%slot)
{
   %p = Parent::onFire(%data,%obj,%slot);
   %p.setTarget(%obj.team);

   %pt = getWords(%obj.lastProjectile.getTargetPoint(), 0, 2);		// +[soph]
   if(%pt $= "0 0 0")							// + preliminary beaconing script
      %pt = getLOSOffset(%obj, %obj.getMuzzleVector(0), 300, %obj.getMuzzlePoint(0));
   %obj.ionTAGLoop = true;						// + 
   %obj.lastPointFirePos = %pt;						// + 
   %obj.IonPainterTargetCalibrate = 0;					// + 
   TLPlaceBeaconLoop( %obj , %p );					// +[/soph]
}
	
function TLPlaceBeaconLoop( %obj , %projectile )			// +[soph]
{									// beaconing script loop
   if( %obj.ionTAGLoop )						// + 
   {									// + 
      %pt = %projectile.getTargetPoint();				// + 
      %dist = getDistance3D( %pt, %obj.lastPointFirePos );		// + 
      if( %dist < 1 )							// + 
      {									// + 
         %obj.IonPainterTargetCalibrate++;				// + 
         if( %obj.IonPainterTargetCalibrate >= 20 )			// +
         {								// + 
            TLPlaceBeacon(%obj, %obj.getMountedImage(0));		// + 
            %obj.IonPainterTargetCalibrate = 0;				// + 
            %obj.ionTAGLoop = false;					// + 
            return;							// + 
         }								// + 
      }									// + 
      else								// + 
         %obj.IonPainterTargetCalibrate = 0;				// + 
      %obj.lastPointFirePos = %pt;					// + 
      schedule( 50 , 0 , "TLPlaceBeaconLoop" , %obj , %projectile );	// + 
   }									// + 
}									// +[/soph]

function TargetingLaserMountImage::deconstruct(%data, %obj, %slot)	// +[soph]
{									// beaconing script cleanup
   %obj.ionTAGLoop = false;						// + 
   %obj.IonPainterTargetCalibrate = 0;					// + 
   %obj.lastPointFirePos = "";						// + 
   Parent::deconstruct(%data, %obj, %slot);				// + 
}									// +[/soph]

function instanceReflectObject(%obj, %dist)
{
   if(!isObject(%obj))
      return;

   InitContainerRadiusSearch(%obj.getWorldBoxCenter(), %dist, $TypeMasks::ProjectileObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.lastReflectedFrom == %obj)
         continue;

      if(isReflectableProjectile(%int, $DefaultReflectableProjectles))
      {
//         echo(%int);
//         echo(%int.getDatablock());
//         echo(%int.getDatablock().getName()); // name of projectile (discProjectile)
//         echo(%int.getDatablock().getClassName()); // name of Projectile Datablock type (LinearProjectileData)
//         echo(%int.getClassName()); // name of Projectile for spawning (LinearProjectile)
//         echo(%int.getType()); // returns type of object it is
//         %int.dump();

//         %FFRObject = new StaticShape()
//         {
//            dataBlock = mReflector;
//         };
//         MissionCleanup.add(%FFRObject);
          if(!isObject(%obj.reflectorObj))
               %obj.reflectorObj = createReflectorForObject(%obj);
               
          %FFRObject = %obj.reflectorObj;
         %FFRObject.setPosition(%int.getPosition());

         %vector = vectorNeg(%int.initialDirection);
         %x = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
         %y = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
         %z = (getRandom() - 0.5) * 2 * 3.1415926 * (48 / 1000); //0.785398;
         %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
         %vector = MatrixMulVector(%mat, %vector);

           if(!isObject(%int.sourceObject))
               %src = %int.bkSourceObject;
           else
               %src = %int.sourceObject;
               
         if( %int.originTime )						// +[soph]
         {								// +
            %airTimeMod = getSimTime() - %int.originTime ; 		// +
            %data = %int.getDatablock().getName() ;			// +
            if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
               %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
            else							// +
               %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
            %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;	// +
         }								// +[/soph]

         %p = new(%int.getClassName())()
         {
            dataBlock         = %int.getDatablock().getName();
            initialDirection  = %vector; //vectorNeg(%int.initialDirection);
            initialPosition   = %int.getPosition();
            sourceObject      = %FFRObject;
            sourceSlot        = %int.sourceSlot;
            lastReflectedFrom = %obj;
            bkSourceObject    = %obj;
            starburstMode     = %int.starburstMode;
         };
         MissionCleanup.add(%p);

         %p.sourceObject = %obj;

         if( %src.lastMortar == %int )					// if(%src.lastMortar !$= "") -soph
             %src.lastMortar = %p;         
         
         %p.originTime = %int.originTime ;				// +soph

         if(%int.trailThread)
            projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);

         if(%int.proxyThread)
            rBeginProxy(%p);

           if(%int.lanceThread)
           {        
               SBLanceRandomThread(%p);
               %p.lanceThread = true;
           }

         createLifeEmitter(%int.getPosition(), "PCRSparkEmitter", 750);

         %nrg = %obj.getEnergyLevel();

        // if(%int.getDatablock().hasDamageRadius)
         if( %int.getDatablock().directDamage * 2 <= %int.getDatablock().indirectDamage )
         {
           // if(%nrg < 40 && (getRandom() < 0.9))			// -[soph]
           //    DaiSelfDestruct(%obj, %int.sourceObject, %int.getDatablock().radiusDamageType);
           // else							// -[/soph]

            if( %nrg < 50 )						// +[soph]
               %obj.getDatablock().damageObject(%obj, %int.sourceObject, %obj.position, %int.getDatablock().indirectDamage * ( ( 50 - %nrg ) / 50 ) , %int.getDatablock().radiusDamageType, %obj.getVelocity(), false);
            if( %obj.getState() $= "Dead" )
               DaiSelfDestruct(%obj, %int.sourceObject, %int.getDatablock().radiusDamageType);
									// +[/soph]
            %obj.useEnergy(%int.getDatablock().indirectDamage * 50 );	// getRandom(20, 30)); -soph		
         }
         else
         {
           // if(%nrg < 40 && (getRandom() < 0.9))			// -[soph]
           //    DaiSelfDestruct(%obj, %int.sourceObject, %int.getDatablock().directDamageType);
           // else							// -[/soph]

            if( %nrg < 50 )						// +[soph]
               %obj.getDatablock().damageObject(%obj, %int.sourceObject, %obj.position, %int.getDatablock().directDamage * ( ( 50 - %nrg ) / 50 ) , %int.getDatablock().directDamageType, %obj.getVelocity(), false);
            if( %obj.getState() $= "Dead" )
               DaiSelfDestruct(%obj, %int.sourceObject, %int.getDatablock().directDamageType);
									// +[/soph]

            %obj.useEnergy(%int.getDatablock().directDamage * 50 );	// getRandom(20, 30)); -soph	
         }

         %int.delete();
      }
      else continue;
   }
}

function TLRFLTimeout(%obj)
{
   %obj.tlm_rfl_time = 0;
   %obj.reflectGun = false;
}

Meltdown.absDmgNrgMod = 0.25;

function serverCmdAdjustEnergyPct(%client, %pct)
{
   if(%client.SFCCAuth)
      Meltdown.absDmgNrgMod = %pct;
}

function instanceAbsorbObject(%obj, %dist)
{
   if(!isObject(%obj))
      return;

   InitContainerRadiusSearch(%obj.getWorldBoxCenter(), %dist, $TypeMasks::ProjectileObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
//      if(%int.lastAbsObj == %obj)
//         continue;

      if(isReflectableProjectile(%int, $DefaultReflectableProjectles))
      {
//         %block = new StaticShape()
//         {
//            dataBlock = AbsDefo;
//         };
//         MissionCleanup.add(%block);
//         %pos = vectorAdd(vectorScale(%int.initialDirection, 2), %int.getPosition());
//         %block.setPosition(vectorAdd(%pos, "0 0 -1"));
//         %block.startFade(1, 0, true);
//         %block.schedule(32, "setDamageState", Destroyed);
//         %int.lastAbsObj = %obj;
         %obj.zapObject();
//         spawnProjectile(%int, LinearFlareProjectile, MBDisplayCharge);

         if(%int.getDatablock().hasDamageRadius)
            %obj.damage(%obj, %obj.getPosition(), %int.getDatablock().indirectDamage, %int.getDatablock().indirectDamageType);
//            %obj.takeDamage(); //*(%dist/200)); // dist percent
//            %obj.useEnergy((%int.getDatablock().indirectDamage * getRandom(40, 60)) * Meltdown.absDmgNrgMod);
         else
            %obj.damage(%obj, %obj.getPosition(), %int.getDatablock().directDamage, %int.getDatablock().directDamageType);
//            %obj.takeDamage(%int.getDatablock().directDamage); //*(%dist/200)); // dist percent
//            %obj.useEnergy((%int.getDatablock().directDamage * getRandom(40, 60)) * Meltdown.absDmgNrgMod);

         %int.delete();
//         %block.schedule(1000, delete);
      }
      else continue;
   }
}

function TLABSTimeout(%obj)
{
   %obj.tlm_abs_time = 0;
   %obj.absorbGun = false;
}

datablock TargetProjectileData(DefenderDesignatorBeam)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 250;
   beamColor           	= "0.1 1.0 0.1";
								
   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = true;
};

datablock SniperProjectileData(AmmoPulseDetect)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "noneExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 50;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.29;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 5.0;
   lightColor          = "1 1 1";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

function AmmoPulseDetect::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
     %source = %projectile.sourceObject;

     if(!isObject(%source))
          return;

     %t = new TargetProjectile()
     {
          dataBlock        = "DefenderDesignatorBeam";
          initialDirection = %source.getMuzzleVector(0);
          initialPosition  = %source.getMuzzlePoint(0);
          sourceObject     = %source;
          sourceSlot       = 0;
          vehicleObject    = 0;
     };

     MissionCleanup.add(%t);
     %t.schedule(400, delete);

     if(%targetObject.getType() & $TypeMasks::PlayerObjectType && %targetObject.client.team == %source.client.team)
     {
         if( !%targetObject.isCloaked() )					// +soph
         {
             %targetObject.setCloaked(true);
             %targetObject.play3D(DepInvActivateSound);
             schedule( 500 , 0 , deCloak , %targetObject ) ;			// +soph
         }
//         %targetObject.schedule(500, "setCloaked", false);			// -soph
//         schedule(1100, %targetObject, "checkCloakOff", %targetObject);	// -soph
         
         if(%targetObject.isMech)
              MechRestock(%targetObject);
         else
              buyPackFavorites(%targetObject.client);
              
         %source.setEnergyLevel(0);
         %source.p_invTimeout = getSimTime();					// %source.p_invTimeout = 45000; -soph
         schedule(45000, 0, resetInvPTimeout, %source);				// schedule(%source.p_invTimeout, 0, resetInvPTimeout, %source); -soph
         messageClient(%source.client, 'MsgInvPackProjectAmmo', '\c2Sent support pulse to %1.', %targetObject.client.nameBase);
         messageClient(%targetObject.client, 'MsgInvPackRecieveAmmo', '\c2Support pulse recieved from %1.', %source.client.nameBase);
     }
}

//datablock SniperProjectileData(JammerPulseDetect)				// -[soph]
//{
//   directDamage        = 0.4;
//   hasDamageRadius     = false;
//   indirectDamage      = 0.0;
//   damageRadius        = 0.0;
//   velInheritFactor    = 1.0;
//   sound 				  = SniperRifleProjectileSound;
//   explosion           = "noneExplosion";
//   splash              = SniperSplash;
//   directDamageType    = $DamageType::Laser;
//
//   maxRifleRange       = 30;
//   rifleHeadMultiplier = 1.3;
//   beamColor           = "1 0.1 0.1";
//   fadeTime            = 1.0;
//
//   startBeamWidth		  = 0.29;
//   endBeamWidth 	     = 0.5;
//   pulseBeamWidth 	  = 1.0;
//   beamFlareAngle 	  = 3.0;
//   minFlareSize        = 0.0;
//   maxFlareSize        = 400.0;
//   pulseSpeed          = 6.0;
//   pulseLength         = 0.150;
//
//   lightRadius         = 5.0;
//   lightColor          = "1 1 1";
//
//   textureName[0]      = "special/flare";
//   textureName[1]      = "special/nonlingradient";
//   textureName[2]      = "special/laserrip01";
//   textureName[3]      = "special/laserrip02";
//   textureName[4]      = "special/laserrip03";
//   textureName[5]      = "special/laserrip04";
//   textureName[6]      = "special/laserrip05";
//   textureName[7]      = "special/laserrip06";
//   textureName[8]      = "special/laserrip07";
//   textureName[9]      = "special/laserrip08";
//   textureName[10]     = "special/laserrip09";
//   textureName[11]     = "special/sniper00";
//};										// -[/soph]

function JammerPulseDetect::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
     %source = %projectile.sourceObject;

     if(!isObject(%source))
          return;

     %t = new TargetProjectile()
     {
          dataBlock        = "DefenderDesignatorBeam";
          initialDirection = %source.getMuzzleVector(0);
          initialPosition  = %source.getMuzzlePoint(0);
          sourceObject     = %source;
          sourceSlot       = 0;
          vehicleObject    = 0;
     };

     MissionCleanup.add(%t);
     %t.schedule(400, delete);

     if(%targetObject.getType() & $TypeMasks::StaticShapeObjectType && %targetObject.client.team != %source.client.team)
     {
         if(%targetObject.getDamageState() !$= "Enabled")
         {
              messageClient(%source.client, 'MsgJammerPulseFailD', '\c2%1 is destroyed.', %targetObject.getGameName());
              return;
         }
         
         %group = %targetObject.getGroup();
         
         if(isObject(%group))
         {
              schedule(10000, %targetObject, jammerPulseReactivate, %targetObject, %group, %source);
//              %group.remove(%targetObject);
              %targetObject.getDatablock().onLosePowerDisabled(%targetObject);
              %targetObject.play3D(JetfireTransform);
              %source.jammerPulseTimeout = true;
              schedule(12000, %source, jammerPulseReset, %source);
              messageClient(%source.client, 'MsgJammerPulseSuccess', '\c2%1 successfully disabled for 10 seconds.', %targetObject.getGameName());
         }
         else
              messageClient(%source.client, 'MsgJammerPulseFail', '\c2%1 is not a powered object.', %targetObject.getGameName());
     }
}

function jammerPulseReactivate(%obj, %group, %source)
{
     messageClient(%source.client, 'MsgJammerPulseReinit', '\c2%1 back online.', %obj.getGameName());
     
     if(%targetObject.getDamageState() $= "Enabled")
          %obj.getDatablock().onGainPowerEnabled(%obj);

//     %group.add(%obj);
//     %group.powerInit();
}

function jammerPulseReset(%obj)
{
     %obj.jammerPulseTimeout = false;
}

function TargetingLaserOptionImage::onFire(%data,%obj,%slot)
{
   %armor = %obj.getArmorSize();
   %type = %obj.getArmorType();
   %data.lightStart = getSimTime();

   %vehicle = 0;
   if(%data.usesEnergy)
   {
      %energy = %obj.getEnergyLevel();

      if(%energy < %data.minEnergy)
         return;
   }

   if(%type & $ArmorMask::Medium)
   {
      if(%obj.getEnergyLevel() < 10)
      {
         %obj.setImageTrigger(%slot, false);
         return;
      }

      if(!%obj.tlm_rfl_time)
      {
         %obj.tlm_rfl_time = 50;
         schedule(%obj.tlm_rfl_time, 0, TLRFLTimeout, %obj);
         instanceReflectObject(%obj, 11);
         %obj.playShieldEffect(%obj.getMuzzleVector(0));
//         %obj.useEnergy(0.5);
         %obj.setImageTrigger(%slot, false);
         return;
      }

      %obj.setImageTrigger(%slot, false);
      return;
   }
   else if(%type & $ArmorMask::Light)
   {
      %enUse = 30;
      
      if(%obj.powerRecirculator)
         %enUse *= 0.75;

      if(%obj.getEnergyLevel() < %enUse)
         return;

      %f = new LinearFlareProjectile()
      {
         dataBlock        = MitziEMPBlast;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };
      MissionCleanup.add(%f);
      %obj.play3D(PlasmaFireSound);
      %obj.useEnergy(%enUse);
      return;
   }
   else if(%type & $ArmorMask::Heavy)
		BattleAngelOverdrive( %obj.getDataBlock() , %obj ) ;	// +soph
   else if(%type & $ArmorMask::BattleAngel)
   {
      switch(%obj.BAPackID)
      {
          case 2:							// +[soph]
              if( !%obj.BAPackCSLTicker && !%obj.isEMP )		// +
              { 							// +
                   %obj.BAPackCSLTicker++ ;				// +
                   BAPackCSLTickerLoop( %obj ) ;			// +
                   messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Charging Mending Field Pulse. Release trigger to abort.' ) ;
                   return ;						// +
              }								// +[/soph]
               
          case 3:
               if(!%obj.isShielded)
               {
                    messageClient(%obj.client, 'MsgNotShield', '\c2Shield pack must be active to extend shielding.');
                    return;
               }
               
              %projectile = "ShieldELF";
              %projectileType = "ELFProjectile";

          case 4:
               if(%obj.p_invTimeout)
               {
                    messageClient(%obj.client, 'MsgLowInvCD', '\c2Ammo pack requires %1 more seconds to cool off.', ( 45 - mFloor ( ( getSimTime() - %obj.p_invTimeout ) / 1000 ) ) );	// messageClient(%obj.client, 'MsgLowInvCD', '\c2Ammo pack is cooling off.'); -soph
                    return;
               }

              %projectile = "AmmoPulseDetect";
              %projectileType = "SniperProjectile";

          case 5:
//              if(!%obj.tlm_rfl_time)					// -[soph]
//              {
//                 %obj.tlm_rfl_time = 500;
//                 schedule(%obj.tlm_rfl_time, 0, TLRFLTimeout, %obj);
//
//                 %projectile = "DefenderDesignatorDetect";
//                 %projectileType = "SniperProjectile";		// -[/soph]
              if( !%obj.BAPackCSLTicker )				// +[soph]
              {
                   %obj.BAPackCSLTicker++ ;
                   BAPackCSLTickerLoop( %obj ) ;
                   messageClient( %obj.client , 'MsgBACSLCharge' , '\c2TURBOCHARGER SELFDESTRUCT ACTIVATED. Release trigger to abort.' ) ;
                   return;
              }								// +[/soph]
      
          case 12:
//               if(%obj.jammerPulseTimeout)				// -[soph]
//               {
//                    messageClient(%obj.client, 'MsgJammerPulseTimeout', '\c2Recharging EM pulse...');
//                    return;
//               }
//
//              %projectile = "JammerPulseDetect";
//              %projectileType = "SniperProjectile";			// -[/soph]
               if( !%obj.BAPackCSLTicker && !%obj.isCloaked() )		// +[soph]
               {
                   %obj.BAPackCSLTicker++ ;
                   BAPackCSLTickerLoop( %obj ) ;
                   messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Charging Battle Angel Stealth Device. Release trigger to abort.' ) ;
                   return;
               }							// +[/soph]

          case 13:
               if(!%obj.isSyn)
               {
                    messageClient(%obj.client, 'MsgNotSyn', '\c2Synomium Device must be active to extend field.');
                    return;
               }

              %projectile = "SynELF";
              %projectileType = "ELFProjectile";

          default:
                %enUse = 20;

                if(%obj.powerRecirculator)
                   %enUse *= 0.75;

                if(%obj.getEnergyLevel() < %enUse)
                   return;

                %vec = vectorScale(%obj.getMuzzleVector(0), 3);

                %p = new FlareProjectile()
                {
                   dataBlock        = FlareGrenadeProj;
                   initialDirection = %vec;
                   initialPosition  = %obj.getMuzzlePoint(0);
                   sourceObject     = %obj;
                   sourceSlot       = 0;
                };
                FlareSet.add(%p);
                MissionCleanup.add(%p);
                %obj.play3D(GrenadeThrowSound);
                %p.schedule(10000, delete);
                schedule(3000, %p, testVectorRedirect, %p);

                %obj.useEnergy(%enUse);
                return;

      }
   }
   else if(%type & $ArmorMask::Blastech)
   {
      %projectile = "TBELF";
      %projectileType = "ELFProjectile";
   }
   else
   {
      %projectile = "BasicTargeter";
      %projectileType = "TargetProjectile";
   }

   if(%projectileType $= "")
     return;
     
   %p = new (%projectileType)()
   {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
   };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

//   echo(%energy SPC %data.fireEnergy);
//   %obj.setEnergyLevel(%energy - %data.fireEnergy);
   %p.setTarget(%obj.team);
}

function BAPackCSLTickerLoop( %obj )
{
   if( %obj.BAPackCSLTicker )
   {
      %obj.BAPackCSLTicker++ ;
      switch( %obj.BAPackID )
      {
         case 2:
            if( %obj.BAPackCSLTicker > 20 )
            {
               messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Mending Field Pulse Activated. Healing nearby infantry.' ) ;
               HealingExplosion( %obj.getWorldBoxCenter() , 40 , %obj.getEnergyLevel() / 100 , %obj ) ;
               %obj.EMPObject( %obj , 1000 ) ;
               %obj.BAPackCSLTicker = 0 ;
            }
         case 5:
            if( %obj.BAPackCSLTicker > 40 )
            {
               messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Turbocharger detonated!' ) ;
               TurboChargerBreach( %obj );
               %obj.BAPackCSLTicker = 0 ;
            }
         case 12:
            if( %obj.BAPackCSLTicker > 30 )
            {
               %energy = %obj.getEnergyLevel() ;
               if( %energy < 50 )
                  messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Error, insufficient energy to engage stealth!' ) ;
               else if( %obj.isCloaked() )
                  messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Error, stealth field already active!' ) ;
               else
               {
                  %obj.setCloaked(true);
//                  %obj.playAudio( 0 , CloakingPackActivateSound ) ;
                  %energy *= 60 ;
                  messageClient( %obj.client , 'MsgBACSLCharge' , '\c2Battle Angel cloaked for %1 seconds!' , mCeil( %energy / 1000 )  ) ;
                  schedule( %energy , 0 , "deCloak" , %obj ) ;
               }
               %obj.BAPackCSLTicker = 0 ;
               %obj.setEnergyLevel( 0 );
            }
      }
      schedule( 50 , 0 , BAPackCSLTickerLoop , %obj ) ;
   }
}

function testVectorRedirect(%p)
{
     %p.initialDirection = "0 0 -2";
}

function ShockLanceImage::onFire(%this, %obj, %slot)
{
  %weapon = %obj.getMountedImage(0).item;

     if(%obj.client.mode[%weapon] == 0)
        %projectile = "BasicShocker";
     else if(%obj.client.mode[%weapon] == 1)
        %projectile = "FireLanceBolt";
     else if(%obj.client.mode[%weapon] == 2)
        %projectile = "PoisonBolt";

     //-Nite- sets energy used when target is hit
//      if(%obj.client.mode[%weapon] == 0)
//         %this.hitEnergy = 15;
//     else if(%obj.client.mode[%weapon] == 1)
//         %this.hitEnergy = 30;
//     else if(%obj.client.mode[%weapon] == 2)
//          %this.hitEnergy = 15;
//     else if(%obj.client.mode[%weapon] == 3)
//         %this.hitEnergy = 20;

     %enUse = 15;
     
     if(%obj.powerRecirculator)
         %enUse *= 0.75;
     else if(%obj.extendedLanceMod)
          %enUse *= 2.0;

   %useEnergyObj = %obj.getObjectMount();
   %mountDamageMod = 0;

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

  if( %obj.isCloaked() )
  {
   if( %obj.respawnCloakThread !$= "" )
   {
     Cancel(%obj.respawnCloakThread);
     %obj.setCloaked( false );
   }
   else
   {
         
     if( %obj.getEnergyLevel() > %enUse )
     {
//      %obj.setCloaked( false );
      deCloak( %obj ) ;
      %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
     }
   }
  }

  %muzzlePos = %obj.getMuzzlePoint(%slot);
  %muzzleVec = %obj.getMuzzleVector(%slot);

  if(%obj.extendedLanceMod)
     %endPos  = VectorAdd(%muzzlePos, VectorScale(%muzzleVec, 33)); //Extended Range mode - Nite-
  else
     %endPos  = VectorAdd(%muzzlePos, VectorScale(%muzzleVec, 15));// -Nite- Increased a tad to get a full 16m's outta it//%projectile.extension));

  %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
   $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType |
   $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType |
   $TypeMasks::DamagableItemObjectType ;						// +soph

  %everythingElseMask = $TypeMasks::TerrainObjectType |
             $TypeMasks::InteriorObjectType |
             $TypeMasks::ForceFieldObjectType |
             $TypeMasks::StaticObjectType |
             $TypeMasks::MoveableObjectType ;
//             $TypeMasks::DamagableItemObjectType;					// -soph

  // did I miss anything? players, vehicles, stations, gens, sensors, turrets
  %hit = ContainerRayCast(%muzzlePos, %endPos, %damageMasks | %everythingElseMask, %obj);

  %noDisplay = true;

  if(%hit)
  {
//   %obj.setEnergyLevel(%obj.getEnergyLevel() - %enUse);				// -soph

   %hitobj = getWord(%hit, 0);
   %hitpos = getWord(%hit, 1) @ " " @ getWord(%hit, 2) @ " " @ getWord(%hit, 3);

   if ( %hitObj.getType() & %damageMasks )
   {
     %obj.setEnergyLevel( %obj.getEnergyLevel() - %enUse ) ;				// +soph
     if( !%hitobj.inertialDampener && !%hitobj.getDatablock().forceSensitive )		// +soph
        %hitobj.applyImpulse(%hitpos, VectorScale(%muzzleVec, %projectile.impulse));
     %obj.playAudio(0, ShockLanceHitSound);

     // This is truly lame, but we need the sourceobject property present...
     %p = new ShockLanceProjectile() {
      dataBlock    = %projectile;
      initialDirection = %obj.getMuzzleVector(%slot);
      initialPosition = %obj.getMuzzlePoint(%slot);
      sourceObject   = %obj;
      sourceSlot    = %slot;
      targetId     = %hit;
     };
     MissionCleanup.add(%p);

     %damageMultiplier = 1.0;

     if(%hitObj.isPlayer())
     {
      // Now we see if we hit from behind...
      %forwardVec = %hitobj.getForwardVector();
      %objDir2D  = getWord(%forwardVec, 0) @ " " @ getWord(%forwardVec,1) @ " " @ "0.0";
      %objPos   = %hitObj.getPosition();
      %dif    = VectorSub(%objPos, %muzzlePos);
      %dif    = getWord(%dif, 0) @ " " @ getWord(%dif, 1) @ " 0";
      %dif    = VectorNormalize(%dif);
      %dot    = VectorDot(%dif, %objDir2D);

      // 120 Deg angle test...
      // 1.05 == 60 degrees in radians
      if (%dot >= mCos(1.05)) // rear hit
      {
         %damageMultiplier = 8 ;							// %hitObj.isShielded ? 16 : 12; -soph
         if( %hitObj.client )								// +[soph]
            %hitObj.client.rearshot = true ;						// +
      }											// +
      else if( %hitObj.client )								// +
         %hitObj.client.rearshot = false ;						// +[/soph]
     }

     if(!%damageMultiplier)
      %damageMultiplier = 3.0;

//     if(%projectile.directDamage !$= "")						// -[soph] if(%this.Projectile.DirectDamage !$= "")
//      %totalDamage = %projectile.directDamage * %damageMultiplier;			// -
//     else										// -
//      %totalDamage = 0.45 * %damageMultiplier;					// -[/soph]

     %totalDamage = %projectile.directDamage ;						// +soph

  // Vehicle Damage Modifier
   if(%vehicle)
     %totalDamage *= ( 1 + %vehicle.damageMod ) ;					// %totalDamage *= 1 + %vehicle.damageMod; -soph

	if( %obj.damageMod > 0 )							// +[soph]
		%totalDamage *= %obj.damageMod ;					// +[/soph]

     %energy = %hitObj.getEnergyLevel() ;						// +[soph]
     if( %projectile $= "BasicShocker" )						// +
        EMPObject( %hitObj , %obj , %totalDamage * %damageMultiplier * 100 ) ;		// +
     %damage = %hitObj.getDamageLevel() ;						// + fixing vehicle backlances
     %hitObj.getDataBlock().damageObject( %hitobj , %obj , %hitpos , %totalDamage , %projectile.radiusdamageType ) ;
     if( %hitObj.getDamageLevel() > %damage || %hitObj.getEnergyLevel() < %energy )	// +
     {											// +
          %totalDamage *= ( %damageMultiplier - 1 ) ;					// +
          %hitObj.getDataBlock().damageObject( %hitobj , %obj , %hitpos , %totalDamage , %projectile.radiusdamageType ) ;
     }											// +[/soph]

//     %hitObj.getDataBlock().damageObject(%hitobj, %p.sourceObject, %hitpos, %totalDamage, %projectile.radiusdamageType);	// -soph //$DamageType::ShockLance); -Nite-
     //-Nite- Now it will call the data block radiusDamageType (be that lance or burn or Emp so on) *fixes that setting someone on fire with the Heatsink packs
//     if(%obj.client.mode[%weapon] == 1 && %hitObj.getType() & $TypeMasks::PlayerObjectType() && %hitobj.team != %obj.team)
//        %hitobj.burnObject(%p.sourceObject);

   if( %projectile $= "FireLanceBolt" )						// +[soph]
      %hitobj.burnObject( %obj , 1.0 + %damageMultiplier );			// +
   else if( %projectile $= "PoisonBolt" )					// +
      %hitobj.poisonObject( %obj , %damageMultiplier ) ;			// +[/soph]
     %noDisplay = false;
   }
  }

  if( %noDisplay )
  {
   // Miss
   %obj.setEnergyLevel(%obj.getEnergyLevel() - %this.missEnergy);
   %obj.playAudio(0, ShockLanceMissSound);

   %p = new ShockLanceProjectile() {
     dataBlock    = %projectile;
     initialDirection = %obj.getMuzzleVector(%slot);
     initialPosition = %obj.getMuzzlePoint(%slot);
     sourceObject   = %obj;
     sourceSlot    = %slot;
   };
   MissionCleanup.add(%p);

  }
}

datablock AudioProfile(UndeploySound)
{
   filename    = "fx/misc/nexus_cap.wav";
   description = AudioDefault3d;
   preload = true;
};

function undeploySpawnPack(%pos, %pack)
{
     if(%pack $= "")
          return;
          
     %item = new Item()
     {
         dataBlock = %pack;
         static = true;
         rotate = false;
     };
           
     %item.schedulePop();
     %item.setPosition(%pos);
     %item.play3D("ItemThrowSound");
}

function EDisassemblerImage::onFire(%this, %obj, %slot)
{
   %muzzlePos = %obj.getMuzzlePoint(%slot);
   %muzzleVec = %obj.getMuzzleVector(%slot);

   %endPos    = VectorAdd(%muzzlePos, VectorScale(%muzzleVec, 15));

   %damageMasks = $TypeMasks::StaticShapeObjectType | $TypeMasks::ForceFieldObjectType;

   // did I miss anything? players, vehicles, stations, gens, sensors, turrets
   %hit = ContainerRayCast(%muzzlePos, %endPos, %damageMasks, %obj);

   %noDisplay = true;

   if (%hit !$= "0")
   {
//      %obj.setEnergyLevel(%obj.getEnergyLevel() - %this.hitEnergy);

      %hitobj = getWord(%hit, 0);
      %hitpos = getWord(%hit, 1) @ " " @ getWord(%hit, 2) @ " " @ getWord(%hit, 3);

      if ( %hitObj.getType() & %damageMasks )
      {
         if(%hitobj.team == %obj.team)
         {
            %obj.playAudio(0, UndeploySound);

            if(%hitObj.isMounted())
               %noDisplay = true;
            else if(%hitObj.owner $= "")
               %noDisplay = true;
            else if(%hitObj.getClassName() $= "ForceFieldBare" && %hitobj.parent) // && %hitobj.deployBase.getDamageState !$= "Enabled")
            {
               if(isObject(%hitObj.deployBase))
                  return;

               if(isObject(%hitObj.parent))
                  return;

               %hitObj.getDatablock().losePower(%hitObj);
               %hitObj.pz.setPosition("-1111 -10000 -100000");
               %hitObj.delete();
            }
            else if(%hitObj.getClassName() $= "ForceFieldBare" && %hitobj.deployBase)
            {
               if(isObject(%hitObj.deployBase))
                  return;

               if(isObject(%hitObj.parent))
                  return;

               %hitObj.getDatablock().losePower(%hitObj);
               %hitObj.pz.setPosition("-1111 -10000 -100000");
               %hitObj.delete();
            }
            else if(isObject(%hitObj.parent))
            {
               %hitObj.killOnDestroyed = true;
               %hitObj.parent.disassembled = true;
               %hitObj.DEFeedbackPercent = %hitObj.getDamagePercent();	// +soph
//               %hitObj.setDamageLevel(%hitObj.getDatablock().maxDamage);
               %hitobj.parent.setDamageState("Destroyed");
            }
            else
            {
               %hitObj.disassembled = true;
               %hitObj.killOnDestroyed = true;
               %hitObj.DEFeedbackPercent = %hitObj.getDamagePercent();	// +soph
               %hitObj.setDamageLevel(%hitObj.getDatablock().maxDamage);
               %hitobj.setDamageState("Destroyed");
            }            

            %p = new ShockLanceProjectile()
            {
               dataBlock        = "Deconstructor";
               initialDirection = %obj.getMuzzleVector(%slot);
               initialPosition  = %obj.getMuzzlePoint(%slot);
               sourceObject     = %obj;
               sourceSlot       = %slot;
               targetId         = 0;
            };
            MissionCleanup.add(%p);

            %noDisplay = false;
         }
      }
   }

   if( %noDisplay )
   {
      // Miss
//      %obj.setEnergyLevel(%obj.getEnergyLevel() - %this.missEnergy);
      %obj.playAudio(0, ShockLanceMissSound);

      %p = new ShockLanceProjectile() {
         dataBlock        = "Deconstructor";
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };
      MissionCleanup.add(%p);
   }
}

function undeployDataStuff()
{
                 %parent = %hitobj.parent;
                       
                 // loop through here to make sure we are hitting the parent
                 while(isObject(%parent))
                 {
                    %parent = %parent.parent;
                 }
                       
                 // Get all sub-objects
                 if(isObject(%parent.attachment[0]))
                 {
                    %idx = 0;
                          
                    while(%parent.attachment[%idx] !$= "")
                    {
                         if(isObject(%parent.attachment[%idx]))
                         {
                              %parent.attachment[%idx].setCloaked(true);
                              %parent.attachment[%idx].schedule(400, setPosition, "0 0 -10000");
                         }                         
     
                         %idx++;
                    }
                    
                    %parent.killOnDestroyed = true;
                    %parent.schedule(500, setDamageState, "Destroyed");
                 }
                 else
                 {
                    %hitObj.setCloaked(true);
                    %hitObj.schedule(400, setPosition, "0 0 -10000");
                    %hitObj.schedule(500, setDamageState, "Destroyed");            
                    schedule(350, 0, undeploySpawnPack, %hitObj.getWorldBoxCenter(), %hitObj.undeployPack);
                 }
}            

//               %hitObj.setDamageLevel(1);

datablock ItemData(BarrelSwapper)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = EBarrelSwapperImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a barrel swapper";
   emap = true;
};

datablock ShapeBaseImageData(EBarrelSwapperImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_targeting.dts";
   item = BarrelSwapper;
   offset = "0 0 0";
   emap = true;
   tool = true;

   usesEnergy = true;
   missEnergy = 0;
   hitEnergy  = 5;
   minEnergy  = 5;       // needs to change to be datablock's energy drain for a hit

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateSound[0] = ShockLanceSwitchSound;
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.4;
   stateFire[3] = true;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateScript[3] = "onFire";
   stateSound[3] = RepairPackActivateSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.1;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = ShockLanceReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Ready";

   stateName[6]                  = "DryFire";
   stateSound[6]                 = ShockLanceDryFireSound;
   stateTimeoutValue[6]          = 1.0;
   stateTransitionOnTimeout[6]   = "Ready";
};

function convertToUsableItemB(%int)
{
   switch(%int)
   {
      case 0:
         return "AABarrelLarge";
      case 1:
         return "ELFBarrelLarge";
      case 2:
         return "MissileBarrelLarge";
      case 3:
         return "MortarBarrelLarge";
      case 4:
         return "PlasmaBarrelLarge";
      case 5:
         return "EnergyBarrelLarge";
      case 6:
         return "ChaingunBarrelLarge";
      case 7:
         return "FlameBarrelLarge";
      case 8:
         return "PhaserBarrelLarge";
         
      default:
         return "PlasmaBarrelLarge";
   }
}

function cInitBarrelSwap(%turret, %obj)
{
   %barrel = convertToUsableItemB(%obj.client.mode[BarrelSwapper]);
   %turret.unmountImage(0);
  // %turret.mountImage(%barrel, 0, false);		// -soph

   %turret.clearTarget() ;				// +[soph]
   %turret.mountImage( %barrel , 0 , true ) ;
   if( %turret.isMounted() )
   {
      %vehicle = %turret.getObjectMount() ;
      if( %turret == %vehicle.rearTurret && !%vehicle.moduleState )
         return ;
      %turret.getObjectMount().barrel = %barrel ;
   }							// +[/soph]
}

function wInitBarrelSwap(%turret, %obj)
{
   if( %obj.client.mode[BarrelSwapper] == 0 )							// +[soph]
      messageClient(%obj.client, 'MsgTurretMount', "\c2Cannot mount AA on rear turret.");
   else if( %obj.client.mode[BarrelSwapper] == 2 )
      messageClient(%obj.client, 'MsgTurretMount', "\c2Cannot mount Missile on rear turret.");
   else if( %obj.client.mode[BarrelSwapper] == 3 && %turret == %turret.getMountNodeObject( 2 ) )
      messageClient(%obj.client, 'MsgTurretMount', "\c2Cannot mount Mortar on rear turret.");
   else
   {												// +[/soph]
      %barrel = convertToUsableItemB(%obj.client.mode[BarrelSwapper]);
      %turret.unmountImage(0);
//   %turret.mountImage(AssaultTurretParam, 0, true);
//   %turret.unmountImage(2);
//      %turret.mountImage(%barrel, 0, false);							// -soph
      %turret.clearTarget() ;									// +[soph]
      %turret.mountImage( %barrel , 0 , true ) ;
      if( %turret.getObjectMount().moduleState )
      {
         %turret.clearTarget() ;
         %turret.setTargetObject( -1 ) ;
      }
      messageClient(%obj.client, 'MsgTurretMount', "\c2Turbograv tail turret reconfigured.");	// +[/soph]
   }

//   if(%obj.client.mode[BarrelSwapper] == 2)
//   {
//      %turret.unmountImage(0);
//      %turret.mountImage(CMissileTurretParam, 0);
//   }
//   else
//   {
//      %turret.unmountImage(0);
//      %turret.mountImage(AssaultTurretParam, 0);
//   }
}

function EBarrelSwapperImage::onFire(%this, %obj, %slot)
{
   // search for a turret base in player's LOS
   %eyeVec = VectorNormalize(%obj.getMuzzleVector(0));
   %srchRange = VectorScale(%eyeVec, 10); // look 5m for a turret base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potTurret = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType );	// (%obj.getEyeTransform(), %srchEnd, $TypeMasks::StaticShapeObjectType) -soph

   if($dump && %potTurret)
   {
      %potTurret.dump();
      return;
   }

   if(%potTurret != 0)
   {
      if(%potTurret.getDatablock().getName() $= "TurretBaseLarge" || %potTurret.getDatablock().getName() $= "FreeBaseTurret" || %potTurret.isMounted())	//%potTurret.getDatablock().getName() $= "MobileTurretBase" || %potTurret.getDatablock().getName() $= "FreeBaseTurret" || %potTurret.getDatablock().getName() $= "WolfSideTurret" || %potTurret.getDatablock().getName() $= "AnniAutoTurret"  || %potTurret.getDatablock().getName() $= "BomberFlyerTailTurret")
      {
         // found a turret base, what team is it on?
         if(%potTurret.team == %obj.client.team)
         {
				if(%potTurret.getDamageState() !$= "Enabled")
				{
					// the base is destroyed
					messageClient(%obj.client, 'MsgBaseDestroyed', "\c2Turret base is disabled, cannot mount barrel.");
					%obj.setImageTrigger($BackpackSlot, false);
				}
				else
				{
		         // it's a functional turret base on our team! stick the barrel on it
//		         messageClient(%obj.client, 'MsgTurretMount', "\c2Mounting pack on turret base.");	// -soph
		         serverPlay3D(TurretPackActivateSound, %potTurret.getTransform());
//		         %potTurret.initiateBarrelSwap(%obj);
                    if( %potTurret.getDatablock().getName() $= "BomberFlyerTailTurret" )			// (%potTurret.getDatablock().getName() $= "WolfSideTurret" || %potTurret.getDatablock().getName() $= "AnniAutoTurret"  || %potTurret.getDatablock().getName() $= "BomberFlyerTailTurret") -soph
                       wInitBarrelSwap(%potTurret, %obj);
                    else
                       if( %potTurret.getDatablock().getName() $= "BomberFlyerStaticF" )
                       {
                          if( isObject( %potTurret.parent.rearTurret ) )					// if( %potTurret.parent.getMountNodeObject(2).getDatablock().getName() $= "BomberFlyerTailTurret" ) -soph
                             wInitBarrelSwap( %potTurret.parent.rearTurret , %obj );				// wInitBarrelSwap( %potTurret.parent.getMountNodeObject(2) , %obj ); -soph
                       }
                       else
                       {
                          messageClient(%obj.client, 'MsgTurretMount', "\c2Turret barrel reconfigured.");	// +soph
                          cInitBarrelSwap(%potTurret, %obj);
                       }								
                                }
         }
         else
         {
            // whoops, wrong team
            messageClient(%obj.client, 'MsgTryEnemyTurretMount', "\c2Cannot mount a barrel on an enemy turret base!");
            %obj.setImageTrigger($BackpackSlot, false);
         }
      }
      else if( %potTurret.getDatablock().getName() $= "MobileBaseVehicle" )	// +[soph]
      {
         if( %potTurret.team == %obj.client.team )
         {
            if( isObject( %potTurret.turret ) )
            {
               serverPlay3D( TurretPackActivateSound, %potTurret.turret.getTransform() );
               messageClient(%obj.client, 'MsgTurretMount', "\c2Turret barrel reconfigured.");
               cInitBarrelSwap( %potTurret.turret, %obj );
            }
            else
            {
               serverPlay3D( TurretPackActivateSound, %potTurret.getTransform() );
               messageClient(%obj.client, 'MsgTurretMount', "\c2Internal turret configuration updated.");
               %barrel = convertToUsableItemB( %obj.client.mode[ BarrelSwapper ] );
               %potTurret.barrel = %barrel;
            }
         }
      }
      else if ( %potTurret.getDatablock().getName() $= "BomberFlyer" || %potTurret.getDatablock().getName() $= "HAPCFlyer" )
      {
         if( isObject( %potTurret.rearTurret ) )
            wInitBarrelSwap( %potTurret.rearTurret , %obj );
      }										// +[/soph]
      else
      {
         // tried to mount barrel on some other turret type
         messageClient(%obj.client, 'MsgNotTurretBase', "\c2Can only mount a barrel on a turret base.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any turret
      messageClient(%obj.client, 'MsgNoTurretBase', "\c2No turret within range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function sCheckForInvs(%pl)
{
   InitContainerRadiusSearch(%pl.getPosition(), 150, $TypeMasks::StationObjectType);
   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.team == %pl.client.team && %int.getDamageState() $= "Enabled")
         return true;
   }
   return false;
}

function SVehicleAugmenterImage::onFire(%this, %obj, %slot)
{
//   if($Feature[VModNearStation])
//   {
//      if(sCheckForInvs(%obj))
//      {
         %mode = %obj.client.mode[VehicleAugmenter];

         if(%mode <= 6)
            AttemptAugmentVehicle(%data, %obj, %slot);
         else if(%mode == 7)
//            vehicleRocketModImage::onActivate(%data, %obj, %slot);
            vehicleEnergyModImage::onActivate(%data, %obj, %slot);
         else if(%mode == 8)
            vehicleChaingunModImage::onActivate(%data, %obj, %slot);
         else if(%mode == 9)
            vehicleMitziModImage::onActivate(%data, %obj, %slot);
         else if(%mode == 10)
            vehiclePhaserModImage::onActivate(%data, %obj, %slot);
         else if(%mode == 11)
            vehicleBomberModImage::onActivate(%data, %obj, %slot);
         else if(%mode == 12)
            TankInstallStarHammer(%data, %obj, %slot);
         else if(%mode == 13)
            TankInstallCometCannon(%data, %obj, %slot);
         else if(%mode == 14)
            TankInstallPBW(%data, %obj, %slot);
         else if(%mode == 15)
            TankInstallFlakMortar(%data, %obj, %slot);
         else if(%mode == 16)
            TankInstallPumaLauncher(%data, %obj, %slot);
//      }
//      else
//         messageClient(%obj.client, 'MsgVModDistLimit', "\c2You must be within 150m of one of your teams working stations in order to use this gun.");
//   }
}

function ELFProjectileData::zapTarget(%data, %projectile, %target, %targeter)
{
	%oldERate = %target.getRechargeRate();
	%target.teamDamageStateOnZap = $teamDamage;
   %teammates = %target.client.team == %targeter.client.team;

	if( %target.teamDamageStateOnZap || !%teammates )
		%target.setRechargeRate(%oldERate - %data.drainEnergy);
	else
		%target.setRechargeRate(%oldERate);

//	%oldERateT = %targeter.getRechargeRate();

//	if( %target.teamDamageStateOnZap || !%teammates )
//		%targeter.setRechargeRate(%oldERateT + %data.drainEnergy);
//	else
//		%targeter.setRechargeRate(%oldERateT);

	%projectile.checkELFStatus(%data, %target, %targeter);
}

function ELFProjectileData::unzapTarget(%data, %projectile, %target, %targeter)
{
	cancel(%projectile.ELFrecur);
	%target.stopAudio($ELFZapSound);
	%targeter.stopAudio($ELFFireSound);
	%target.zapSound = false;
	%targeter.zappingSound = false;
   %teammates = %target.client.team == %targeter.client.team;

	if(!%target.isDestroyed())
	{
		%oldERate = %target.getRechargeRate();
          if( %target.teamDamageStateOnZap || !%teammates )
			%target.setRechargeRate(%oldERate + %data.drainEnergy);
		else
			%target.setRechargeRate(%oldERate);

//		%oldERateT = %targeter.getRechargeRate();
//		if( %targeter.teamDamageStateOnZap || !%teammates )
//			%targeter.setRechargeRate(%oldERateT - %data.drainEnergy);
//		else
//			%targeter.setRechargeRate(%oldERateT);
	}

//	if(isObject(%targeter))
//	{
//		%oldERateT = %targeter.getRechargeRate();
//		if( %targeter.teamDamageStateOnZap || !%teammates )
//			%targeter.setRechargeRate(%oldERateT - %data.drainEnergy);
//		else
//			%targeter.setRechargeRate(%oldERateT);
//	}

}

function ELFProjectileData::targetDestroyedCancel(%data, %projectile, %target, %targeter)
{
   cancel(%projectile.ELFrecur);
	%target.stopAudio($ELFZapSound);
   %targeter.stopAudio($ELFFireSound);
	%target.zapSound = false;
	%targeter.zappingSound = false;
	%projectile.delete();
}

function ELFProjectile::checkELFStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
		if(%target.getDamageState() $= "Destroyed")
		{
			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
		}

      if(!%target.isEMP && !%targeter.isShielded && !%target.isMageIon)
      {
          %enGain = %targeter.powerRecirculator ? %data.drainEnergy * 2 : %data.drainEnergy;
          
          %targeter.setEnergyLevel(%targeter.getEnergyLevel() + %enGain);
      }

      %enLevel = %target.getEnergyLevel();
      if(%enLevel < 10.0 || (%ba && %enLevel < 1000.0))
      {
         %dataBlock = %target.getDataBlock();
         %dmg = %ba == true ? 1 : 0.6;
         %dataBlock.damageObject(%target, %this.sourceObject, %target.getPosition(), 0.005, %data.directDamageType);
      }
      else
      {
         %normal = "0.0 0.0 1.0";
         %target.playShieldEffect( %normal );
      }
      %this.ELFrecur = %this.schedule(32, checkELFStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}

function JugElf::checkELFStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
		if(%target.getDamageState() $= "Destroyed")
		{
			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
		}

      %enLevel = %target.getEnergyLevel();
      if(%enLevel < 10000.0)
      {
         %dataBlock = %target.getDataBlock();
         %dataBlock.damageObject(%target, %this.sourceObject, %target.getPosition(), %data.drainHealth*0.5, %data.directDamageType);
      }
      else
      {
//         %targeter.useEnergy(-5);
         %normal = "0.0 0.0 1.0";
         %targeter.playShieldEffect( %normal );
         %target.playShieldEffect( %normal );
      }
      %this.ELFrecur = %this.schedule(32, checkELFStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}

function ELFProjectile::checkPullStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
		if(%target.getDamageState() $= "Destroyed")
		{
			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
		}

		if(%target.isSyn)
//		{
//			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
//		}

        if(%targeter.getArmorType() & $ArmorMask::Heavy)
          %targeter.setEnergyLevel(%targeter.getEnergyLevel() - 0.512); // 5 / 32

        if(!%target.isSyn)
        {
//          %velMod = 1 - (vectorLen(%targeter.getVelocity()) / 100);							// -soph
          %mass = %targeter.getDatablock().mass ;									// +soph
          %velMod = 1 - ( vectorLen( %targeter.getVelocity() ) / ( 1000 / mPow( %mass , 0.47 ) ) ) ;			// +soph
          %vecDot = vectorDot(%targeter.getMuzzleVector(0), vectorNormalize(%targeter.getVelocity()));
          %vec = %targeter.getMuzzleVector(0); // aaahahahaha jetpack code!!
          %pullmod = %vecDot < 0 ? 1 : %velMod ;									// +soph
//          %force = %vecDot < 0 ? %targeter.getDatablock().mass * 2 : %targeter.getDatablock().mass * 2 * %velMod; 	// -soph // pullmod
          %tempMass = %target.getDataBlock().mass ;									// +[soph]
          %targetMass = 0 ;												// + stepdown
          while( %tempMass > 0 )											// +
          {														// +
            if( %tempMass > %mass )											// +
            {														// +
              %tempMass -= %mass ;											// +
              %targetMass += %mass ;											// +
            }														// +
            else													// +
            {														// +
              %targetMass += %tempMass ;										// +
              %tempMass = 0 ;												// +
            }														// +
            %tempMass = mPow( %tempMass , 1 / 3 ) ;									// +
          }														// +
          %force = ( %mass * 2 + mSqRt( %targetMass ) ) * 2 / 3 ;							// +[/soph]
          %newVec = VectorScale(%vec, %force);
          %which = %targeter.getObjectMount() ? %targeter.getObjectMount() : %targeter;
//          %which.applyImpulse(%which.getTransform(), %newVec);							// -soph
          %which.setVelocity( vectorAdd( %which.getVelocity() , vectorScale( %newVec , 1 / %which.getDatablock().mass ) ) ) ;	// +soph
        }


//     %force = vectorScale(velToForce(%target.getMass(), %targeter.getVelocity()), 32);
//     %target.applyImpulse(%target.getPosition(), %force);

//    %tgV = velToSingle(%targeter.getVelocity());
//    %tV = velToSingle(%target.getVelocity());
//
//    if(%tV > %tgV)
//        %force = vectorNeg(getTowForce(%targeter, %target));
//    else if(%tV < %tgV)
//        %force = vectorScale(getTowForce(%targeter, %target), 32);
//    else
//        %force = vectorScale(getTowForce(%targeter, %target), 32);
//
//    %target.applyImpulse(%target.getTransform(), %force);

//    if(isObject(%targeter.getObjectMount()))
//        %target.setRotation(%targeter.getObjectMount().getRotation());

    %this.ELFrecur = %this.schedule(32, checkPullStatus, %data, %target, %targeter);
    %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}

function ELFProjectile::checkUberStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
		if(%target.getDamageState() $= "Destroyed")
		{
			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
		}

		if(%target.isSyn)
//		{
//			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
//		}

//     %vec = vectorNeg(%targeter.getMuzzleVector(0));
//     echo("vec: "@%vec);
//     %force = velToSingle(%targeter.getVelocity()) * 10;
//     echo("force: "@%force);
//     %newVec = VectorScale(%vec, %force);
//     echo("newVec: "@%newVec);
//     %target.applyImpulse(%target.getTransform(), %newVec);

     %weapon = %targeter.getMountedImage(0).item;

     // Prevent pulling own vehicle
     %objMount = %targeter.getObjectMount();

     if(%target == %objMount && %objMount.getType() & $TypeMasks::VehicleObjectType)
          return;
     
     %tType = %target.getType();
     %mass = %targeter.getDatablock().mass;
     
//     if(%targeter.client.mode[%weapon] == 3 || %targeter.uberoverride)  //this is the PullBeam -Nite- was mode 4
//     {
     if(%tType & $TypeMasks::PlayerObjectType)
         %mass = %mass > 400 ? 400 : %mass;
     else if(%tType & $TypeMasks::VehicleObjectType)
         %mass = %mass > 400 ? 400 : %mass;
     else
         %mass = 200;

         %vec = %targeter.getMuzzleVector(0);
         %force = %mass * -0.5;
         %newVec = VectorScale(%vec, %force);
         if( %target.inertialDampener )	// mpb fix +soph
            return;
         %impulse = %target.applyImpulse(%target.getTransform(), %newVec);
//       %force = vectorScale(velToForce(%target.getMass(), %targeter.getVelocity()), 32);
//       %target.applyImpulse(%target.getPosition(), %force);
//     }
//     else if(%targeter.client.mode[%weapon] == 5)
//     {
//          %vec = vectorScale(%target.getMuzzleVector(0), 2);
//          %force = %targeter.getDatablock().mass*4;
//          %newVec = VectorScale(%vec, %force);
//          %impulse = %target.applyImpulse(%target.getTransform(), %newVec);
//     }
//     else if(%targeter.client.mode[%weapon] == 6)
//     {
//           if(%target.getDatablock().cameraLag || %target.getDatablock().jetForce)
//               return;
//
//           %target.maxScaleFactor = 10000;
//           %target.countGrow--;
//           %scVal = %target.countGrow / 75;
//
//           if(%scVal < 0.2)
//               %scVal = 0.2;
//
//           if(%scVal > 300)
//               %scVal = 300;
//
//           %target.setScale(%scVal@" "@%scVal@" "@%scVal);
//
//           if(%target.trigger)
//                %target.trigger.setScale(%scVal@" "@%scVal@" "@%scVal);
//
//           if(%target.getDatablock().barrel)
//                %target.getDatablock().barrel.setScale(%scVal@" "@%scVal@" "@%scVal);
//     }
//     else if(%targeter.client.mode[%weapon] == 7)
//     {
//           if(%target.getDatablock().cameraLag || %target.getDatablock().jetForce)
//               return;
//
//           %target.maxScaleFactor = 10000;
//           %target.countGrow++;
//           %scVal = %target.countGrow / 25;
//
//           if(%scVal > 500)
//               %scVal = 500;
//
//           if(%scVal < 0.2)
//               %scVal = 0.2;
//
//           %target.setScale(%scVal@" "@%scVal@" "@%scVal);
//
//           if(%target.trigger)
//                %target.trigger.setScale(%scVal@" "@%scVal@" "@%scVal);
//
//           if(%target.getDatablock().barrel)
//                %target.getDatablock().barrel.setScale(%scVal@" "@%scVal@" "@%scVal);
//     }
//     else if(%targeter.client.mode[%weapon] == 8)
//     {
//           %target.setScale("1 1 1");
//           %target.countGrow = 75;
//
//           if(%target.trigger)
//                %target.trigger.setScale("1 1 1");
//
//           if(%target.getDatablock().barrel)
//                %target.getDatablock().barrel.setScale("1 1 1");
//     }

     %this.ELFrecur = %this.schedule(32, checkUberStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}

function ELFProjectile::checkTractorStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target) && isObject(%targeter))
   {
		if(%target.isSyn)
//		{
//			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
//		}

          %vel = %target.getVelocity();
          %vx = getWord(%vel, 0);
          %vy = getWord(%vel, 1);
          %vz = getWord(%vel, 2) + 0.95;
          %newVel = %vx@" "@%vy@" "@%vz;
          %targeter.setVelocity(%newVel);


      %this.ELFrecur = %this.schedule(32, checkTractorStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}

function ELFProjectile::checkStasisStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
      if(%target.stasisProof)
         return;

		if(%target.isSyn)
//		{
//			%data.targetDestroyedCancel(%this, %target, %targeter);
			return;
//		}

        %targeter.useEnergy(8/32);
        %target.setVelocity("0 0 0.91");

     if((%target.getDatablock().className $= WheeledVehicleData || %target.getDatablock().className $= FlyingVehicleData || %target.getDatablock().className $= HoverVehicleData) && !%target.getDatablock().chair)
           %target.setFrozenState(true);

      %this.ELFrecur = %this.schedule(32, checkStasisStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
		{
         %target.playAudio($ELFZapSound, ELFHitTargetSound);
			%target.zapSound = true;
			%targeter.zappingSound = true;
		}
   }
   // -------------------------------------------------------
   // z0dd - ZOD, 5/27/02. Stop firing if there is no target,
   // fixes continuous fire bug.
	//else if(%targeter.zappingSound)
	//{
	//	%targeter.stopAudio($ELFFireSound);
	//	%targeter.zappingSound = false;
	//}
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
         %target.setFrozenState(false);
         %targeter.countGrow = 0;

      }
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
   // End z0dd - ZOD
   // -------------------------------------------------------
}

function ELFProjectile::checkSkyhookStatus( %this , %data , %target , %targeter )	// +[soph]
{											// +
	if( isObject( %target ) && isObject( %this ) && isObject( %targeter ) )		// +
	{										// +
		// full cancellations							// +
		if( %target.getDamageState() $= "Destroyed" )				// +
		{									// +
			%data.targetDestroyedCancel( %this , %target , %targeter ) ;	// +
			return;								// +
		}									// +
		%objMount = %targeter.getObjectMount() ;				// +
     		if( %target == %objMount && %objMount.getType() & $TypeMasks::VehicleObjectType )
          		return ;							// +
		if( %target.isMounted() )						// +
		{									// +
			%data.targetDestroyedCancel( %this , %target , %targeter ) ;	// +
			return ;							// +
		}									// +
											// +
		// partial cancellations						// +
		%damageLevel = %target.getDamageLevel() ;				// +
		if( %damageLevel > 0 )							// +
			%target.setDamageLevel( %damageLevel - 0.005 ) ;		// +
		if( %target.isSyn || %target.inertialDampener )				// +
		{									// +
			if( %this.ELFrecur )						// +
				cancel( %this.ELFrecur ) ;				// +
     			%this.ELFrecur = %this.schedule( 32 , checkSkyhookStatus , %data , %target , %targeter ) ;
      			if( %targeter.zappingSound )					// +
      			{								// +
         			%targeter.stopAudio( $ELFFireSound ) ;			// +
         			%targeter.zappingSound = false ;			// +
      			}								// +
      			if( %target.zapSound )						// +
			{								// +
         			%target.stopAudio( $ELFZapSound ) ;			// +
				%target.zapSound = false;				// +
			}								// +
			return ;							// +
		}									// +
											// +
		// tractor logic begin							// +
		%targMount = %target.getObjectMount() ;					// +
		%oldTarget = %target ;							// +
		%targetTransform = %target.getTransform() ;				// +
		if( %targMount )							// +
			%target = %targMount ;						// +
		%mass = %objMount.getDatablock().mass ;					// +
		%targetMass = %target.getDatablock().mass ;				// +
		if( %tType & $TypeMasks::PlayerObjectType && !%target.isMech )		// +
			%factor = 18 ;							// +
		else									// +
			%factor = 6 ;							// +
		if( %mass > %targetMass )						// +
			%mass = ( %mass + %targetMass + %targetMass ) / %factor  ;	// +
		else if( %mass < %targetMass )						// +
			%mass = ( %mass + %mass + %targetMass ) / %factor  ;		// +
		%tType = %target.getType() ;						// +
//		if( %tType & $TypeMasks::PlayerObjectType )				// +
//			if( %target.isMech )						// +
//         			%mass = %target.getDatablock().mass / 2 ;		// +
//			else								// +
//         			%mass = %objMount.getDatablock().mass ;			// +
//		else									// +
//		{									// +
//         		%mass = %target.getDatablock().mass ;				// +
//			if( %tType & $TypeMasks::VehicleObjectType )			// +
//			{								// +
//				%nightshadeTransform = %objMount.getTransform() ;	// +
//				%r = vectorScale( getWords( %nightshadeTransform , 3 , 5 ) , 0.0125 ) ;
//				%o = vectorNormalize( vectorAdd( getWords( %targetTransform , 3 , 5 ) , %r ) ) ;
//				%f = getWord( %targetTransform , 6 ) ; 			// +
//				%l = getWord( %nightshadeTransform , 6 ) ; 		// +
//				if( %f > %l && %l - %f > ( 3.14159265 * 2 ) )		// +
//					%f -= 0.05 ; 					// +
//				else if( %f < %l && %f > %l - ( 3.14159265 * 2 ) )	// +
//					%f += 0.05 ; 					// +
//				%target.setTransform( getWords( %targetTransform , 0 , 2 ) SPC %o SPC %f ) ;
//			}								// +
//		}									// +
											// +
		// THIS FORMULA IS SILLY						// +
//		%originPoint = %targeter.getMuzzlePoint( %this.sourceSlot ) ;		// +
//		%originVector = %targeter.getMuzzleVector( %this.sourceSlot ) ;		// +
//		%desiredPoint = vectorAdd( %originPoint , vectorScale( %originVector , 30 ) ) ;
//		%movementVector = vectorSub( %target.getWorldBoxCenter() , %desiredPoint ) ;
//		%distance = vectorLen( %movsementVector ) ;				// +
//		%distanceMod = 0 - mSqRt( mSqRt( %distance ) ) / 3 ;			// +
//											// +
//		if( %distance < 10 )							// +
//		{									// +
//			%counterVector = vectorSub( "0 0 0" , %target.getVelocity ) ;	// +
//			%counterVector = vectorScale ( %counterVector , %distanceMod / 2 ) ;
//         		%target.applyImpulse( %target.getWorldBoxCenter() , %counterVector ) ;
//		}									// +
											// +
		// hopefully this one is more sane					// +
		%originPoint = %targeter.getMuzzlePoint( %this.sourceSlot ) ;		// +
		%originVector = %targeter.getMuzzleVector( %this.sourceSlot ) ;		// +
		%standoffPoint = vectoradd( %originPoint , vectorScale( %originVector , 25 ) ) ;
		%targetCenter = getWords( %targetTransform , 0 , 2 ) ;			// +
		%targetVector = %target.getVelocity() ;					// +
		%nightshadeVector = %targeter.getObjectMount().getVelocity() ;		// +
		%targetToStandoffVector = vectorSub( %standoffPoint , %targetCenter ) ;	// +
		%syncVector = vectorSub( %nightshadeVector , %targetVector ) ;		// +
		%desiredVector = vectorAdd( %syncVector , %targetToStandoffVector ) ;	// +
											// +
		%distanceMod = ( 150 - vectorLen( vectorSub( %targetCenter , %originPoint ) ) ) / 150 ;
		%force = %mass * %distanceMod ;						// +
         	%forceVector = VectorScale( %desiredVector , %force ) ;			// +
											// +
		// needed to keep from pushing vehicles into objects			// +
		%obstructionTest = vectorAdd ( %targetCenter , vectorScale( vectorNormalize( %forceVector ) , 15 ) ) ;
		%obstructionTest = ContainerRayCast( %targetCenter , %obstructionTest , $TypeMasks::TerrainObjectType |
											$TypeMasks::InteriorObjectType | 
											$TypeMasks::ForceFieldObjectType |
											$TypeMasks::StaticObjectType , %target ) ;
		if( %obstructionTest $= "0" )						// +
			%target.setVelocity( vectorAdd( %targetVector , vectorScale( %forceVector , 1 / %targetMass ) ) ) ;
//         		%target.applyImpulse( %target.getTransform() , %forceVector ) ;	// +
											// +
      		%targeter.playAudio( $ELFFireSound , ELFGunFireSound ) ;		// +
      		if( !%target.zapSound )							// +
		{									// +
         		%target.playAudio( $ELFZapSound , ELFHitTargetSound ) ;		// +
			%target.zapSound = true ;					// +
			%targeter.zappingSound = true ;					// +
		}									// +
											// +
		if( %this.ELFrecur )							// +
			cancel( %this.ELFrecur ) ;					// +
     		%this.ELFrecur = %this.schedule( 32 , checkSkyhookStatus , %data , %oldTarget , %targeter ) ;
   	}										// +
   	else										// +
   	{										// +
      		if( %targeter.zappingSound )						// +
      		{									// +
         		%targeter.stopAudio( $ELFFireSound ) ;				// +
         		%targeter.zappingSound = false ;				// +
      		}									// +
      		%data.targetDestroyedCancel( %this , %target , %targeter ) ;		// +
      		return ;								// +
   	}										// +
}											// +[/soph]

function StasisElf::unzapTarget(%data, %projectile, %target, %targeter)
{
						// -[soph]
//     if(%target.getDatablock().className $= WheeledVehicleData || %target.getDatablock().className $= FlyingVehicleData || %target.getDatablock().className $= HoverVehicleData)
//          %target.setFrozenState(false);	// -[/soph]

     Parent::unzapTarget(%data, %projectile, %target, %targeter);
}

function PowerELF::zapTarget(%data, %projectile, %target, %targeter)
{
     %type = %target.getType();

     if(%type & $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType | $TypeMasks::SensorObjectType)
     {
          if(%target.getDamageState() $= "Enabled")
          {
               %targeter.poweringObject = %target;
               %target.setSelfPowered();
          }
     }
     
     echo(%target SPC %target.getDatablock().getName() SPC %targeter);
     %projectile.checkELFStatus(%data, %target, %targeter);
     %projectile.powerRecur = schedule(32, %projectile, PowerELFPowerLoop, %projectile, %data, %target, %targeter);
}

function PowerELF::unzapTarget(%data, %projectile, %target, %targeter)
{
	cancel(%projectile.ELFrecur);
	cancel(%projectile.powerRecur); 
	%target.stopAudio($ELFZapSound);
	%targeter.stopAudio($ELFFireSound);
	%target.zapSound = false;
	%targeter.zappingSound = false;
     %teammates = %target.client.team == %targeter.client.team;

	if(!%target.isDestroyed())
	{
          %targeter.poweringObject = 0;
          %target.clearSelfPowered();
	}
}

// For some reason, the call to ::checkElfStatus() does not work...
// screw you T2
function PowerELFPowerLoop(%projectile, %data, %target, %targeter)
{
     if(isObject(%target))
     {
          %type = %target.getType();   
          
          if(%type & $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType | $TypeMasks::SensorObjectType)
          {          
     		if(%target.getDamageState() !$= "Enabled")
     		{
                    %target.clearSelfPowered();
                    %targeter.poweringObject = 0;
     			%data.targetDestroyedCancel(%this, %target, %targeter);
     			return;
     		}
          }
          else if(%type & $TypeMasks::PlayerObjectType)
               %target.setEnergyLevel(%target.getEnergyLevel() + ((%targeter.getRechargeRate() + 0.001) / 2));
          else if(%type & $TypeMasks::VehicleObjectType)
          {
               %target.setEnergyLevel(%target.getEnergyLevel() + ((%targeter.getRechargeRate() + 0.001) / 2));
                    
               for(%i = 0; %i < 11; %i++)
               {
                    %seatObj = %target.getMountNodeObject(%i);
                    %seatType = %seatObj.getType();
                    
                    if(%seatType & $TypeMasks::TurretObjectType)
                         %target.setCapacitorLevel(%target.getCapacitorLevel() + ((%targeter.getRechargeRate() + 0.001) / 2));               
               }
          }
          
          %projectile.powerRecur = schedule(32, %projectile, PowerELFPowerLoop, %projectile, %data, %target, %targeter);
     }     
}

function PowerELF::checkELFStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
          %type = %target.getType();   
          echo((%targeter.getRechargeRate() + 0.001) / 2);
          
          if(%type & $TypeMasks::StationObjectType | $TypeMasks::TurretObjectType | $TypeMasks::SensorObjectType)
          {          
     		if(%target.getDamageState() !$= "Enabled")
     		{
                    %target.clearSelfPowered();
                    %targeter.poweringObject = 0;
     			%data.targetDestroyedCancel(%this, %target, %targeter);
     			return;
     		}
          }
          else if(%type & $TypeMasks::PlayerObjectType)
               %target.setEnergyLevel(%target.getEnergyLevel() + ((%targeter.getRechargeRate() + 0.001) / 2));
          else if(%type & $TypeMasks::VehicleObjectType)
          {
               %target.setEnergyLevel(%target.getEnergyLevel() + ((%targeter.getRechargeRate() + 0.001) / 2));
                    
               for(%i = 0; %i < 11; %i++)
               {
                    %seatObj = %target.getMountNodeObject(%i);
                    %seatType = %seatObj.getType();
                    
                    if(%seatType & $TypeMasks::TurretObjectType)
                         %target.setCapacitorLevel(%target.getCapacitorLevel() + ((%targeter.getRechargeRate() + 0.001) / 2));               
               }
          }     

          %this.ELFrecur = %this.schedule(32, checkELFStatus, %data, %target, %targeter);

          %targeter.playAudio($ELFFireSound, ELFGunFireSound);
          if(!%target.zapSound)
          {
               %target.playAudio($ELFZapSound, ELFHitTargetSound);
               %target.zapSound = true;
               %targeter.zappingSound = true;
          }
   }
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }

      %targeter.poweringObject = 0;
      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
}

//===============================================================================

function ShieldELF::zapTarget(%data, %projectile, %target, %targeter)
{
     if(isObject(%target.repulsorField))
     {
          %targeter.setImageTrigger(0, false);
          return;
     }
     
     if(%target.getType() & $TypeMasks::PlayerObjectType)
     {
          if(%target.isMech)
               return;
               
          if(%target.isShielded)
               %target.setImageTrigger($BackpackSlot, false);
               
          if(%target.BAPackID == 3)
               return;

          %target.isShielded = true;
          %target.projectedShield = %targeter;
          messageClient(%target.client, 'MsgShieldProjStart', '\c2%1 has projected their shield onto you.', %targeter.client.nameBase);
          
     	%projectile.checkShieldStatus(%data, %target, %targeter);
     }
     else if(%target.getType() & $TypeMasks::VehicleObjectType)
     {
          if(%target.shieldStrengthFactor > 0)
               return;

          %target.projectedShield = %targeter;

          %pilot = %target.getMountNodeObject(0);

          if(isObject(%pilot))
               messageClient(%pilot.client, 'MsgShieldProjStartV', '\c2%1 has projected their shield onto the vehicle.', %targeter.client.nameBase);

          %target.projectedShield = %targeter;
          
     	%projectile.checkShieldStatus(%data, %target, %targeter);
     }
}

function ELFProjectile::checkShieldStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
      if(!%targeter.isShielded)
      {
          %data.targetDestroyedCancel(%this, %target, %targeter);
          return;
      }
      
      %this.ELFrecur = %this.schedule(32, checkShieldStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);
      if(!%target.zapSound)
      {
          %target.playAudio($ELFZapSound, VehicleWakeMediumSplashSound);
          %target.zapSound = true;
          %targeter.zappingSound = true;
      }
   }
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }

      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
}

function ShieldElf::unzapTarget(%data, %projectile, %target, %targeter)
{
     if(%target.getType() & $TypeMasks::PlayerObjectType)
     {
          if(%target.isMech)
               return;

          if(%target.BAPackID == 3)
               return;
               
          %target.isShielded = false;
          %target.projectedShield = false;
          messageClient(%target.client, 'MsgShieldProjRelease', '\c2%1 has stopped projecting their shield onto you.', %targeter.client.nameBase);
     }
     else if(%target.getType() & $TypeMasks::VehicleObjectType)
     {
          if(%target.shieldStrengthFactor > 0)
               return;

          %target.projectedShield = false;
          
          %pilot = %target.getMountNodeObject(0);

          if(isObject(%pilot))
               messageClient(%pilot.client, 'MsgShieldProjReleaseV', '\c2%1 has stopped projecting their shield onto the vehicle.', %targeter.client.nameBase);
     }
     
     Parent::unzapTarget(%data, %projectile, %target, %targeter);
}

//===============================================================================

function SynELF::zapTarget(%data, %projectile, %target, %targeter)
{
     if(isObject(%target.repulsorField))
     {
          %targeter.setImageTriggr(0, false);
          return;
     }
     
     if(%target.getType() & $TypeMasks::PlayerObjectType)
     {
          messageClient(%target.client, 'MsgSynProjStart', '\c2%1 has projected their synomium field onto you.', %targeter.client.nameBase);
          
          if(%target.isSyn)
          {
               %target.isSyn++;
               return;
          }
          
          %target.startSyn = true;
          synPhase2(%target);
     	%projectile.checkSynStatus(%data, %target, %targeter);
     }
     else if(%target.getType() & $TypeMasks::VehicleObjectType)
     {
          %pilot = %target.getMountNodeObject(0);
          
          if(%pilot)
                    messageClient(%pilot.client, 'MsgSynProjStart', '\c2%1 has projected their synomium field onto you.', %targeter.client.nameBase);

          if(%target.isSyn)
          {
               %target.isSyn++;
               return;
          }

          %target.startSyn = true;
          synPhase2v(%target);
     	%projectile.checkSynStatus(%data, %target, %targeter);
     }
}

function ELFProjectile::checkSynStatus(%this, %data, %target, %targeter)
{
   if(isObject(%target))
   {
      if(!%targeter.isSyn)
      {
          %data.targetDestroyedCancel(%this, %target, %targeter);
          return;
      }
      
      %this.ELFrecur = %this.schedule(32, checkSynStatus, %data, %target, %targeter);

      %targeter.playAudio($ELFFireSound, ELFGunFireSound);

      if(!%target.zapSound)
      {
          %target.playAudio($ELFZapSound, SynomiumIdle);
		%target.zapSound = true;
		%targeter.zappingSound = true;
      }
   }
   else
   {
      if(%targeter.zappingSound)
      {
         %targeter.stopAudio($ELFFireSound);
         %targeter.zappingSound = false;
      }

      %data.targetDestroyedCancel(%this, %target, %targeter);
      return;
   }
}

function SynElf::unzapTarget(%data, %projectile, %target, %targeter)
{
     if(%target.getType() & $TypeMasks::PlayerObjectType)
     {
          messageClient(%target.client, 'MsgSynProjStop', '\c2%1 has stopped projecting their synomium field onto you.', %targeter.client.nameBase);
          
          if(%target.isSyn > 1)
          {
               %target.isSyn--;
               return;
          }
          
          %target.startSyn = false;
          %target.isSyn = false;
     }
     else if(%target.getType() & $TypeMasks::VehicleObjectType)
     {
          %pilot = %target.getMountNodeObject(0);

          if(%pilot)
               messageClient(%pilot.client, 'MsgSynProjStop', '\c2%1 has stopped projecting their synomium field onto you.', %targeter.client.nameBase);

          if(%target.isSyn > 1)
          {
               %target.isSyn--;
               return;
          }

          %target.startSyn = false;
          %target.isSyn = false;
     }
     Parent::unzapTarget(%data, %projectile, %target, %targeter);
}