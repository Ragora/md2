// Vehicle String Table

// Hull Types
//----------------------------------------------------------------------------

$VehicleHull::Wildcat = 1 << 0;
$VehicleHull::Shrike = 1 << 1;
$VehicleHull::Thundersword = 1 << 2;
$VehicleHull::Havoc = 1 << 3;
$VehicleHull::Ground = 1 << 4;

$VehicleHullName[$VehicleHull::Wildcat] = "Wildcat";
$VehicleHullName[$VehicleHull::Shrike] = "Shrike";
$VehicleHullName[$VehicleHull::Thundersword] = "Thundersword";
$VehicleHullName[$VehicleHull::Havoc] = "Havoc";
$VehicleHullName[$VehicleHull::Ground] = "Ground";

// Vehicle Types
//----------------------------------------------------------------------------

$VehicleMask::Wildcat = 1 << 0 ;					// +soph
$VehicleMask::Shrike = 1 << 1 ;						// = 1 << 0; -soph
//$VehicleMask::Valkyrie = 1 << 1;					// -soph
$VehicleMask::Nightshade = 1 << 2;
$VehicleMask::Thundersword = 1 << 3;
$VehicleMask::Havoc = 1 << 4;
$VehicleMask::Retaliator = 1 << 5;
$VehicleMask::Beowulf = 1 << 6;

$VehicleMask::All = -1;
$VehicleMask::AllDefaultShielded = $VehicleMask::All ^ $VehicleMask::Wildcat ;	// $VehicleMask::All ^ $VehicleMask::Valkyrie; -soph
$VehicleMask::AllFighters = $VehicleMask::Shrike | $VehicleMask::Valkyrie;

// Weapons
//----------------------------------------------------------------------------
$VehicleWeaponCount = 0;

$VehicleWeapon::None = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::None] = "None";
$VehicleWeaponDesc[$VehicleWeapon::None] = "No Weapon installed";
$VehicleWeaponUsesAmmo[$VehicleWeapon::None] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::None] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::None] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::None] = "";
$VehicleWeaponMask[$VehicleWeapon::None] = $VehicleMask::All;
$VehicleWeaponSlot[$VehicleWeapon::None] = -1;
$VehicleWeaponClass[$VehicleWeapon::None] = "NoneWeapon";
$VehicleWeaponCount++;

$VehicleWeapon::CatLance = $VehicleWeaponCount ;			// +[soph]
$VehicleWeaponName[ $VehicleWeapon::CatLance ] = "Catlance" ;		// +
$VehicleWeaponDesc[ $VehicleWeapon::CatLance ] = "A bike pike for the big targets." ;
$VehicleWeaponUsesAmmo[ $VehicleWeapon::CatLance ] = false ;		// +
$VehicleWeaponReloadTime[ $VehicleWeapon::CatLance ] = 5000 ;		// +
$VehicleWeaponAmmoCount[ $VehicleWeapon::CatLance ] = 250 ;		// +
$VehicleWeaponAmmo[ $VehicleWeapon::CatLance ] = "ChaingunAmmo" ;	// +
$VehicleWeaponMask[ $VehicleWeapon::CatLance ] = $VehicleMask::Wildcat ;
$VehicleWeaponSlot[ $VehicleWeapon::CatLance ] = 0 ;			// +
$VehicleWeaponClass[ $VehicleWeapon::CatLance ] = "Catlance" ;		// +
$VehicleWeaponCount++ ;							// +
									// +
$VehicleWeapon::CatRocket = $VehicleWeaponCount ;			// +
$VehicleWeaponName[ $VehicleWeapon::CatRocket ] = "Viper Missiles" ;	// +
$VehicleWeaponDesc[ $VehicleWeapon::CatRocket ] = "Ground-launched missiles to give flyers grief." ;
$VehicleWeaponUsesAmmo[ $VehicleWeapon::CatRocket ] = false ;		// +
$VehicleWeaponReloadTime[ $VehicleWeapon::CatRocket ] = 5000 ;		// +
$VehicleWeaponAmmoCount[ $VehicleWeapon::CatRocket ] = 250 ;		// +
$VehicleWeaponAmmo[ $VehicleWeapon::CatRocket ] = "ChaingunAmmo" ;	// +
$VehicleWeaponMask[ $VehicleWeapon::CatRocket ] = $VehicleMask::Wildcat ;
$VehicleWeaponSlot[ $VehicleWeapon::CatRocket ] = 0 ;			// +
$VehicleWeaponClass[ $VehicleWeapon::CatRocket ] = "CatMissile" ;	// +
$VehicleWeaponCount++ ;							// +[/soph]

$VehicleWeapon::ShrikeBlaster = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::ShrikeBlaster] = "Shrike Blasters";
$VehicleWeaponDesc[$VehicleWeapon::ShrikeBlaster] = "Medium power Anti-Air blasters.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::ShrikeBlaster] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::ShrikeBlaster] = 5000;
$VehicleWeaponAmmoCount[$VehicleWeapon::ShrikeBlaster] = 250;
$VehicleWeaponAmmo[$VehicleWeapon::ShrikeBlaster] = "ChaingunAmmo";
$VehicleWeaponMask[$VehicleWeapon::ShrikeBlaster] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::ShrikeBlaster] = 0;
$VehicleWeaponClass[$VehicleWeapon::ShrikeBlaster] = "ShrikeBlaster";
$VehicleWeaponCount++;

$VehicleWeapon::EMPCannon = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::EMPCannon] = "Dual Shrike Blaster Shotguns";
$VehicleWeaponDesc[$VehicleWeapon::EMPCannon] = "For when holding back isn't an option.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::EMPCannon] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::EMPCannon] = 5000;
$VehicleWeaponAmmoCount[$VehicleWeapon::EMPCannon] = 250;
$VehicleWeaponAmmo[$VehicleWeapon::EMPCannon] = "ChaingunAmmo";
$VehicleWeaponMask[$VehicleWeapon::EMPCannon] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::EMPCannon] = 0;
$VehicleWeaponClass[$VehicleWeapon::EMPCannon] = "BlasterShotgun";
$VehicleWeaponCount++;

$VehicleWeapon::ShrikeMSTB = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::ShrikeMSTB] = "Dual Turbo Blasters";
$VehicleWeaponDesc[$VehicleWeapon::ShrikeMSTB] = "Punches through shields and weapon repulsors.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::ShrikeMSTB] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::ShrikeMSTB] = 10000;
$VehicleWeaponAmmoCount[$VehicleWeapon::ShrikeMSTB] = 300;
$VehicleWeaponAmmo[$VehicleWeapon::ShrikeMSTB] = "PlasmaAmmo";
$VehicleWeaponMask[$VehicleWeapon::ShrikeMSTB] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::ShrikeMSTB] = 0;
$VehicleWeaponClass[$VehicleWeapon::ShrikeMSTB] = "ShrikeMSTB";
$VehicleWeaponCount++;

$VehicleWeapon::ShrikeHeavyPhaser = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::ShrikeHeavyPhaser] = "Dual Phaser Bolters";
$VehicleWeaponDesc[$VehicleWeapon::ShrikeHeavyPhaser] = "Aerial bombardment, Shrike style!";
$VehicleWeaponUsesAmmo[$VehicleWeapon::ShrikeHeavyPhaser] = false;	// true; uses energy now -soph
$VehicleWeaponReloadTime[$VehicleWeapon::ShrikeHeavyPhaser] = 8000;
$VehicleWeaponAmmoCount[$VehicleWeapon::ShrikeHeavyPhaser] = 40;
$VehicleWeaponAmmo[$VehicleWeapon::ShrikeHeavyPhaser] = "PCRAmmo";
$VehicleWeaponMask[$VehicleWeapon::ShrikeHeavyPhaser] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::ShrikeHeavyPhaser] = 0;		// 1; now a primary -soph
$VehicleWeaponClass[$VehicleWeapon::ShrikeHeavyPhaser] = "ShrikeHeavyPhaser";
$VehicleWeaponCount++;

$VehicleWeapon::TurboChaingun = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TurboChaingun] = "Dual Chainguns";
$VehicleWeaponDesc[$VehicleWeapon::TurboChaingun] = "Make it rain.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TurboChaingun] = true;
$VehicleWeaponReloadTime[$VehicleWeapon::TurboChaingun] = 8000;
$VehicleWeaponAmmoCount[$VehicleWeapon::TurboChaingun] = 300;
$VehicleWeaponAmmo[$VehicleWeapon::TurboChaingun] = "ChaingunAmmo";
$VehicleWeaponMask[$VehicleWeapon::TurboChaingun] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::TurboChaingun] = 1;
$VehicleWeaponClass[$VehicleWeapon::TurboChaingun] = "TurboChaingun";
$VehicleWeaponCount++;

$VehicleWeapon::SpikeArray = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::SpikeArray] = "Spike Array";
$VehicleWeaponDesc[$VehicleWeapon::SpikeArray] = "Hard-hitting spike cannons to attack air and ground foes.";	// = "Modified Landspike turret barrel for more speed and punch."; -soph
$VehicleWeaponUsesAmmo[$VehicleWeapon::SpikeArray] = true;
$VehicleWeaponReloadTime[$VehicleWeapon::SpikeArray] = 8000;
$VehicleWeaponAmmoCount[$VehicleWeapon::SpikeArray] = 50;
$VehicleWeaponAmmo[$VehicleWeapon::SpikeArray] = "PlasmaAmmo";
$VehicleWeaponMask[$VehicleWeapon::SpikeArray] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::SpikeArray] = 1;
$VehicleWeaponClass[$VehicleWeapon::SpikeArray] = "SpikeArray";
$VehicleWeaponCount++;

$VehicleWeapon::ShrikeHeavyPlasma = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::ShrikeHeavyPlasma] = "Dual Grenade Launchers";				// = "Dual Disc Launchers"; -soph
$VehicleWeaponDesc[$VehicleWeapon::ShrikeHeavyPlasma] = "Grenade fire support for splashing grounded targets.";	// = "Twin Spinfusors, taking mid-air to the next level"; -soph
$VehicleWeaponUsesAmmo[$VehicleWeapon::ShrikeHeavyPlasma] = true;
$VehicleWeaponReloadTime[$VehicleWeapon::ShrikeHeavyPlasma] = 8000;
$VehicleWeaponAmmoCount[$VehicleWeapon::ShrikeHeavyPlasma] = 30;
$VehicleWeaponAmmo[$VehicleWeapon::ShrikeHeavyPlasma] = "DiscAmmo";
$VehicleWeaponMask[$VehicleWeapon::ShrikeHeavyPlasma] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::ShrikeHeavyPlasma] = 1;
$VehicleWeaponClass[$VehicleWeapon::ShrikeHeavyPlasma] = "ShrikeHeavyPlasma";
$VehicleWeaponCount++;

$VehicleWeapon::Puma = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::Puma] = "TFX-19 'Puma' Missiles";
$VehicleWeaponDesc[$VehicleWeapon::Puma] = "Heat-locking missiles.  30 second rearm after launch.";	// = "High explosive seeking missiles to ensure victory. 30 sec. reload time."; -soph
$VehicleWeaponUsesAmmo[$VehicleWeapon::Puma] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::Puma] = 15000;
$VehicleWeaponAmmoCount[$VehicleWeapon::Puma] = 200;
$VehicleWeaponAmmo[$VehicleWeapon::Puma] = "PlasmaAmmo";
$VehicleWeaponMask[$VehicleWeapon::Puma] = $VehicleMask::Shrike;
$VehicleWeaponSlot[$VehicleWeapon::Puma] = 1;
$VehicleWeaponClass[$VehicleWeapon::Puma] = "Pumas";
$VehicleWeaponCount++;

$VehicleWeapon::BomberBomb = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::BomberBomb] = "Bomber 150kt Bombs";
$VehicleWeaponDesc[$VehicleWeapon::BomberBomb] = "C4 knockin' at your door.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::BomberBomb] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::BomberBomb] = 10000;
$VehicleWeaponAmmoCount[$VehicleWeapon::BomberBomb] = 300;
$VehicleWeaponAmmo[$VehicleWeapon::BomberBomb] = "PlasmaAmmo";
$VehicleWeaponMask[$VehicleWeapon::BomberBomb] = $VehicleMask::Thundersword;
$VehicleWeaponSlot[$VehicleWeapon::BomberBomb] = 0;					// 1; -soph
$VehicleWeaponClass[$VehicleWeapon::BomberBomb] = "BomberBomb";
$VehicleWeaponCount++;

$VehicleWeapon::TankAABarrel = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankAABarrel] = "Tank Anti-Air Array";
$VehicleWeaponDesc[$VehicleWeapon::TankAABarrel] = "Rail down aerial threats!";		// = "Rail down your aerial foes with this!"; -soph
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankAABarrel] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankAABarrel] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankAABarrel] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankAABarrel] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankAABarrel] = $VehicleMask::Beowulf;
$VehicleWeaponSlot[$VehicleWeapon::TankAABarrel] = 1;					// 0; -soph
$VehicleWeaponClass[$VehicleWeapon::TankAABarrel] = "TankAABarrel";
$VehicleWeaponCount++;

$VehicleWeapon::TankMissileTurret = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankMissileTurret] = "Tank Missile Turret";
$VehicleWeaponDesc[$VehicleWeapon::TankMissileTurret] = "Shoot down enemy fliers with ease!";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankMissileTurret] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankMissileTurret] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankMissileTurret] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankMissileTurret] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankMissileTurret] = $VehicleMask::Beowulf;
$VehicleWeaponSlot[$VehicleWeapon::TankMissileTurret] = 1;				// 0; -soph
$VehicleWeaponClass[$VehicleWeapon::TankMissileTurret] = "TankMissileTurret";
$VehicleWeaponCount++;

$VehicleWeapon::TankFlamethrower = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankFlamethrower] = "Tank Fireball";			// = "Tank RAXX Flamethrower";
$VehicleWeaponDesc[$VehicleWeapon::TankFlamethrower] = "Reach out and torch someone!";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankFlamethrower] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankFlamethrower] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankFlamethrower] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankFlamethrower] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankFlamethrower] = $VehicleMask::Beowulf;
$VehicleWeaponSlot[$VehicleWeapon::TankFlamethrower] = 1;				// 0; -soph
$VehicleWeaponClass[$VehicleWeapon::TankFlamethrower] = "TankFlamethrower";
$VehicleWeaponCount++;

$VehicleWeapon::TankTurboChaingun = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankTurboChaingun] = "Tank Turbo Chaingun";
$VehicleWeaponDesc[$VehicleWeapon::TankTurboChaingun] = "Rapid-fire chaingun turret.";	// = "High RoF turret mounted chaingun."; -soph
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankTurboChaingun] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankTurboChaingun] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankTurboChaingun] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankTurboChaingun] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankTurboChaingun] = $VehicleMask::Beowulf;
$VehicleWeaponSlot[$VehicleWeapon::TankTurboChaingun] = 1;				// 0; -soph
$VehicleWeaponClass[$VehicleWeapon::TankTurboChaingun] = "TankTurboChaingun";
$VehicleWeaponCount++;

$VehicleWeapon::TankEnergyTurret = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankEnergyTurret] = "Tank Phaser Turret";		// "Tank Energy Turret"; -soph
$VehicleWeaponDesc[$VehicleWeapon::TankEnergyTurret] = "Hissing green balls of death.";	// "Explosive blast of fruit-flavored ownage."; -soph
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankEnergyTurret] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankEnergyTurret] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankEnergyTurret] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankEnergyTurret] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankEnergyTurret] = $VehicleMask::Beowulf;
$VehicleWeaponSlot[$VehicleWeapon::TankEnergyTurret] = 1;				// 0; -soph
$VehicleWeaponClass[$VehicleWeapon::TankEnergyTurret] = "TankEnergyTurret";
$VehicleWeaponCount++;

$VehicleWeapon::TankCometCannon = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankCometCannon] = "Comet Cannon";			// "Tank Comet Cannon"; -soph
$VehicleWeaponDesc[$VehicleWeapon::TankCometCannon] = "Fearsome and explosive ball of energy.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankCometCannon] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankCometCannon] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankCometCannon] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankCometCannon] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankCometCannon] = $VehicleMask::Beowulf | $VehicleMask::Thundersword;
$VehicleWeaponSlot[$VehicleWeapon::TankCometCannon] = 0;				// 1; -soph
$VehicleWeaponClass[$VehicleWeapon::TankCometCannon] = "TankCometCannon";
$VehicleWeaponCount++;

$VehicleWeapon::TankStarHammer = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankStarHammer] = "Blast Hammer";			// "Tank Blast Hammer"; -soph
$VehicleWeaponDesc[$VehicleWeapon::TankStarHammer] = "Star Hammer 2.0";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankStarHammer] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankStarHammer] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankStarHammer] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankStarHammer] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankStarHammer] = $VehicleMask::Beowulf | $VehicleMask::Thundersword;
$VehicleWeaponSlot[$VehicleWeapon::TankStarHammer] = 0;	// 1; -soph
$VehicleWeaponClass[$VehicleWeapon::TankStarHammer] = "TankStarHammer";
$VehicleWeaponCount++;

$VehicleWeapon::TankPBW = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankPBW] = "Particle Beam Weapon (PBW)";	// "Tank Particle Beam Weapon (PBW)"; -soph
$VehicleWeaponDesc[$VehicleWeapon::TankPBW] = "Starsiege technology at it's finest.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankPBW] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankPBW] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankPBW] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankPBW] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankPBW] = $VehicleMask::Beowulf | $VehicleMask::Thundersword;
$VehicleWeaponSlot[$VehicleWeapon::TankPBW] = 0;	// 1; -soph
$VehicleWeaponClass[$VehicleWeapon::TankPBW] = "TankPBW";
$VehicleWeaponCount++;

//$VehicleWeapon::TankStarburst = $VehicleWeaponCount;				// removed, rolled into flak mortar -soph
//$VehicleWeaponName[$VehicleWeapon::TankStarburst] = "Starburst Cannon";	// "Tank Starburst Cannon"; -soph
//$VehicleWeaponDesc[$VehicleWeapon::TankStarburst] = "A beautiful fireworks show... of death.";
//$VehicleWeaponUsesAmmo[$VehicleWeapon::TankStarburst] = false;
//$VehicleWeaponReloadTime[$VehicleWeapon::TankStarburst] = 0;
//$VehicleWeaponAmmoCount[$VehicleWeapon::TankStarburst] = 0;
//$VehicleWeaponAmmo[$VehicleWeapon::TankStarburst] = "NullAmmo";
//$VehicleWeaponMask[$VehicleWeapon::TankStarburst] = $VehicleMask::Beowulf | $VehicleMask::Thundersword;
//$VehicleWeaponSlot[$VehicleWeapon::TankStarburst] = 0;	// 1; -soph
//$VehicleWeaponClass[$VehicleWeapon::TankStarburst] = "TankStarburst";
//$VehicleWeaponCount++;

$VehicleWeapon::TankFlakMortar = $VehicleWeaponCount;
$VehicleWeaponName[$VehicleWeapon::TankFlakMortar] = "Heavy Flak Mortar";	// "Tank Heavy Flak Mortar"; -soph
$VehicleWeaponDesc[$VehicleWeapon::TankFlakMortar] = "Press 'Fire' again while the mortar is in flight to detonate.";
$VehicleWeaponUsesAmmo[$VehicleWeapon::TankFlakMortar] = false;
$VehicleWeaponReloadTime[$VehicleWeapon::TankFlakMortar] = 0;
$VehicleWeaponAmmoCount[$VehicleWeapon::TankFlakMortar] = 0;
$VehicleWeaponAmmo[$VehicleWeapon::TankFlakMortar] = "NullAmmo";
$VehicleWeaponMask[$VehicleWeapon::TankFlakMortar] = $VehicleMask::Beowulf | $VehicleMask::Thundersword;
$VehicleWeaponSlot[$VehicleWeapon::TankFlakMortar] = 0;	// 1; -soph
$VehicleWeaponClass[$VehicleWeapon::TankFlakMortar] = "TankFlakMortar";
$VehicleWeaponCount++;

//$VehicleWeapon::TSwordREmpty = $VehicleWeaponCount;						// -[soph]
//$VehicleWeaponName[$VehicleWeapon::TSwordREmpty] = "Thundersword Tailgunner Seat";		// not needed to be on the list
//$VehicleWeaponDesc[$VehicleWeapon::TSwordREmpty] = "Talgunner seat for the Thundersword Cruiser";
//$VehicleWeaponUsesAmmo[$VehicleWeapon::TSwordREmpty] = false;
//$VehicleWeaponReloadTime[$VehicleWeapon::TSwordREmpty] = 0;
//$VehicleWeaponAmmoCount[$VehicleWeapon::TSwordREmpty] = 0;
//$VehicleWeaponAmmo[$VehicleWeapon::TSwordREmpty] = "NullAmmo";
//$VehicleWeaponMask[$VehicleWeapon::TSwordREmpty] = $VehicleMask::Thundersword;
//$VehicleWeaponSlot[$VehicleWeapon::TSwordREmpty] = 0;
//$VehicleWeaponClass[$VehicleWeapon::TSwordREmpty] = "TSwordREmpty";				// -[/soph]
//$VehicleWeaponCount++;

//$VehicleWeapon::TSwordRTurret = $VehicleWeaponCount;						// -[soph]
//$VehicleWeaponName[$VehicleWeapon::TSwordRTurret] = "Rear AA Turret";				// Switching it to a module
//$VehicleWeaponDesc[$VehicleWeapon::TSwordRTurret] = "Mounts a controllable rear turret on the Thundersword Cruiser";
//$VehicleWeaponUsesAmmo[$VehicleWeapon::TSwordRTurret] = false;
//$VehicleWeaponReloadTime[$VehicleWeapon::TSwordRTurret] = 0;
//$VehicleWeaponAmmoCount[$VehicleWeapon::TSwordRTurret] = 0;
//$VehicleWeaponAmmo[$VehicleWeapon::TSwordRTurret] = "NullAmmo";
//$VehicleWeaponMask[$VehicleWeapon::TSwordRTurret] = $VehicleMask::Thundersword;
//$VehicleWeaponSlot[$VehicleWeapon::TSwordRTurret] = 0;
//$VehicleWeaponClass[$VehicleWeapon::TSwordRTurret] = "TSwordRTurret";
//$VehicleWeaponCount++;

//$VehicleWeapon::TSwordRTurretM = $VehicleWeaponCount;
//$VehicleWeaponName[$VehicleWeapon::TSwordRTurretM] = "Rear Missile Turret";
//$VehicleWeaponDesc[$VehicleWeapon::TSwordRTurretM] = "Mounts a controllable rear turret on the Thundersword Cruiser";
//$VehicleWeaponUsesAmmo[$VehicleWeapon::TSwordRTurretM] = false;
//$VehicleWeaponReloadTime[$VehicleWeapon::TSwordRTurretM] = 0;
//$VehicleWeaponAmmoCount[$VehicleWeapon::TSwordRTurretM] = 0;
//$VehicleWeaponAmmo[$VehicleWeapon::TSwordRTurretM] = "NullAmmo";
//$VehicleWeaponMask[$VehicleWeapon::TSwordRTurretM] = $VehicleMask::Thundersword;
//$VehicleWeaponSlot[$VehicleWeapon::TSwordRTurretM] = 0;
//$VehicleWeaponClass[$VehicleWeapon::TSwordRTurretM] = "TSwordRTurretM";
//$VehicleWeaponCount++;

//$VehicleWeapon::TSwordRTurretC = $VehicleWeaponCount;
//$VehicleWeaponName[$VehicleWeapon::TSwordRTurretC] = "Rear Chaingun Turret";
//$VehicleWeaponDesc[$VehicleWeapon::TSwordRTurretC] = "Mounts a controllable rear turret on the Thundersword Cruiser";
//$VehicleWeaponUsesAmmo[$VehicleWeapon::TSwordRTurretC] = false;
//$VehicleWeaponReloadTime[$VehicleWeapon::TSwordRTurretC] = 0;
//$VehicleWeaponAmmoCount[$VehicleWeapon::TSwordRTurretC] = 0;
//$VehicleWeaponAmmo[$VehicleWeapon::TSwordRTurretC] = "NullAmmo";
//$VehicleWeaponMask[$VehicleWeapon::TSwordRTurretC] = $VehicleMask::Thundersword;
//$VehicleWeaponSlot[$VehicleWeapon::TSwordRTurretC] = 0;
//$VehicleWeaponClass[$VehicleWeapon::TSwordRTurretC] = "TSwordRTurretC";
//$VehicleWeaponCount++;

//$VehicleWeapon::TSwordRTurretP = $VehicleWeaponCount;
//$VehicleWeaponName[$VehicleWeapon::TSwordRTurretP] = "Rear Phaser Turret";
//$VehicleWeaponDesc[$VehicleWeapon::TSwordRTurretP] = "Mounts a controllable rear turret on the Thundersword Cruiser";
//$VehicleWeaponUsesAmmo[$VehicleWeapon::TSwordRTurretP] = false;
//$VehicleWeaponReloadTime[$VehicleWeapon::TSwordRTurretP] = 0;
//$VehicleWeaponAmmoCount[$VehicleWeapon::TSwordRTurretP] = 0;
//$VehicleWeaponAmmo[$VehicleWeapon::TSwordRTurretP] = "NullAmmo";
//$VehicleWeaponMask[$VehicleWeapon::TSwordRTurretP] = $VehicleMask::Thundersword;
//$VehicleWeaponSlot[$VehicleWeapon::TSwordRTurretP] = 0;
//$VehicleWeaponClass[$VehicleWeapon::TSwordRTurretP] = "TSwordRTurretP";
//$VehicleWeaponCount++;									// -[/soph]


$VehicleWeapon::TSwordPilotCannon = $VehicleWeaponCount ;					// +/soph]
$VehicleWeaponName[ $VehicleWeapon::TSwordPilotCannon ] = "Forward KM Cannons" ;		// +
$VehicleWeaponDesc[ $VehicleWeapon::TSwordPilotCannon ] = "Pilot-operated dual cannons, instead of a heavy belly weapon" ;
$VehicleWeaponUsesAmmo[ $VehicleWeapon::TSwordPilotCannon ] = false ;				// +
$VehicleWeaponReloadTime[ $VehicleWeapon::TSwordPilotCannon ] = 0 ;				// +
$VehicleWeaponAmmoCount[ $VehicleWeapon::TSwordPilotCannon ] = 0 ;				// +
$VehicleWeaponAmmo[ $VehicleWeapon::TSwordPilotCannon ] = "NullAmmo" ;				// +
$VehicleWeaponMask[ $VehicleWeapon::TSwordPilotCannon ] = $VehicleMask::Thundersword ;		// +
$VehicleWeaponSlot[ $VehicleWeapon::TSwordPilotCannon ] = 0 ;					// +
$VehicleWeaponClass[ $VehicleWeapon::TSwordPilotCannon ] = "TSwordPilotCannon" ;		// +
$VehicleWeaponCount++ ;										// +
												// +
$VehicleWeapon::TSwordBellyKM = $VehicleWeaponCount ;						// +
$VehicleWeaponName[ $VehicleWeapon::TSwordBellyKM ] = "Underbelly KM cannons" ;			// +
$VehicleWeaponDesc[ $VehicleWeapon::TSwordBellyKM ] = "Heavy energy blasters for fending off vehicles" ;
$VehicleWeaponUsesAmmo[ $VehicleWeapon::TSwordBellyKM ] = false ;				// +
$VehicleWeaponReloadTime[ $VehicleWeapon::TSwordBellyKM ] = 0 ;					// +
$VehicleWeaponAmmoCount[ $VehicleWeapon::TSwordBellyKM ] = 0 ;					// +
$VehicleWeaponAmmo[ $VehicleWeapon::TSwordBellyKM ] = "NullAmmo" ;				// +
$VehicleWeaponMask[ $VehicleWeapon::TSwordBellyKM ] = $VehicleMask::Thundersword ;		// +
$VehicleWeaponSlot[ $VehicleWeapon::TSwordBellyKM ] = 1 ;					// +
$VehicleWeaponClass[ $VehicleWeapon::TSwordBellyKM ] = "TSwordBellyKM" ;			// +
$VehicleWeaponCount++ ;										// +
												// +
$VehicleWeapon::TSwordBellyATC = $VehicleWeaponCount ;						// +
$VehicleWeaponName[ $VehicleWeapon::TSwordBellyATC ] = "Underbelly Autocannons" ;		// +
$VehicleWeaponDesc[ $VehicleWeapon::TSwordBellyATC ] = "Explosive shells for peppering ground targets" ;
$VehicleWeaponUsesAmmo[ $VehicleWeapon::TSwordBellyATC ] = false ;				// +
$VehicleWeaponReloadTime[ $VehicleWeapon::TSwordBellyATC ] = 0 ;				// +
$VehicleWeaponAmmoCount[ $VehicleWeapon::TSwordBellyATC ] = 0 ;					// +
$VehicleWeaponAmmo[ $VehicleWeapon::TSwordBellyATC ] = "NullAmmo" ;				// +
$VehicleWeaponMask[ $VehicleWeapon::TSwordBellyATC ] = $VehicleMask::Thundersword ;		// +
$VehicleWeaponSlot[ $VehicleWeapon::TSwordBellyATC ] = 1 ;					// +
$VehicleWeaponClass[ $VehicleWeapon::TSwordBellyATC ] = "TSwordBellyATC" ;			// +
$VehicleWeaponCount++ ;										// +[/soph]

// Modules
//----------------------------------------------------------------------------

$VehicleModuleCount = 0;

$VehicleModule::None = $VehicleModuleCount;
$VehicleModuleName[$VehicleModule::None] = "None";
$VehicleModuleDesc[$VehicleModule::None] = "No Module installed";
$VehicleModuleClass[$VehicleModule::None] = "NoneModule";
$VehicleModuleMask[$VehicleModule::None] = $VehicleMask::Retaliator ;				// was $VehicleMask::All  -soph
$VehicleModuleCount++;

$VehicleModule::Booster = $VehicleModuleCount;
$VehicleModuleName[$VehicleModule::Booster] = "Booster Rocket";
$VehicleModuleDesc[$VehicleModule::Booster] = "Boosts you forward slightly. 12 charges.";
$VehicleModuleClass[$VehicleModule::Booster] = "BoosterModule";
$VehicleModuleMask[$VehicleModule::Booster] = $VehicleMask::Wildcat | $VehicleMask::Shrike ;	// $VehicleMask::Shrike; -soph
$VehicleModuleCount++;

$VehicleModule::Inventory = $VehicleModuleCount;
$VehicleModuleName[$VehicleModule::Inventory] = "Inventory Station";
$VehicleModuleDesc[$VehicleModule::Inventory] = "Passenger seats are Inventory stations. Press Mine to use.";
$VehicleModuleClass[$VehicleModule::Inventory] = "InventoryModule";
$VehicleModuleMask[$VehicleModule::Inventory] = $VehicleMask::Thundersword | $VehicleMask::Havoc;	// $VehicleMask::Nightshade removed, now permanent -soph
$VehicleModuleCount++;

$VehicleModule::FlareMod = $VehicleModuleCount;
$VehicleModuleName[$VehicleModule::FlareMod] = "Flare Launcher";
$VehicleModuleDesc[$VehicleModule::FlareMod] = "Allows your vehicle to launch Flares. Limited supply.";
$VehicleModuleClass[$VehicleModule::FlareMod] = "FlareMod";
$VehicleModuleMask[$VehicleModule::FlareMod] = $VehicleMask::Wildcat | $VehicleMask::Shrike | $VehicleMask::Nightshade | $VehicleMask::Thundersword | $VehicleMask::Havoc;	// $VehicleMask::Shrike | $VehicleMask::Nightshade | $VehicleMask::Thundersword | $VehicleMask::Havoc; -soph
$VehicleModuleCount++;

$VehicleModule::SynMod = $VehicleModuleCount;
$VehicleModuleName[$VehicleModule::SynMod] = "Synomium Device";
$VehicleModuleDesc[$VehicleModule::SynMod] = "Protects vehicle from EMP and ELF effects.";
$VehicleModuleClass[$VehicleModule::SynMod] = "SynMod";
$VehicleModuleMask[$VehicleModule::SynMod] = $VehicleMask::Wildcat | $VehicleMask::Shrike ;	// $VehicleMask::Shrike; -soph
$VehicleModuleCount++;

$VehicleModule::SubspaceMod = $VehicleModuleCount;
$VehicleModuleName[$VehicleModule::SubspaceMod] = "Hull Regenerator";				// = "Subspace Regenerator"; -soph
$VehicleModuleDesc[$VehicleModule::SubspaceMod] = "Restores lost armor while at full energy";	// = "Passive 1% HP per 5 seconds, -25% recharge rate."; -soph
$VehicleModuleClass[$VehicleModule::SubspaceMod] = "SubspaceMod";
$VehicleModuleMask[$VehicleModule::SubspaceMod] = $VehicleMask::Wildcat | $VehicleMask::Shrike | $VehicleMask::Nightshade | $VehicleMask::Thundersword | $VehicleMask::Havoc ;	// $VehicleMask::All; -soph
$VehicleModuleCount++;

$VehicleModule::TSwordRTurret = $VehicleModuleCount;						// +[soph]
$VehicleModuleName[ $VehicleModule::TSwordRTurret ] = "Automatic Tail Turret";			// switched to a module
$VehicleModuleDesc[ $VehicleModule::TSwordRTurret ] = "Mounts a reconfigurable, automated tail turret.";
$VehicleModuleClass[ $VehicleModule::TSwordRTurret ] = "TSwordRTurret";				// may do something else with the ts weapon slot turret on the Thundersword Cruiser";
$VehicleModuleMask[ $VehicleModule::TSwordRTurret ] = $VehicleMask::Thundersword;
$VehicleModuleCount++;

$VehicleModule::HavocTGTurret = $VehicleModuleCount;
$VehicleModuleName[ $VehicleModule::HavocTGTurret ] = "Automatic Tail Turret";
$VehicleModuleDesc[ $VehicleModule::HavocTGTurret ] = "Mounts a reconfigurable, automated tail turret.";
$VehicleModuleClass[ $VehicleModule::HavocTGTurret ] = "HavocTGTurret";
$VehicleModuleMask[ $VehicleModule::HavocTGTurret ] = $VehicleMask::Havoc;
$VehicleModuleCount++;	

$VehicleModule::BeoPilotMitzi = $VehicleModuleCount;
$VehicleModuleName[ $VehicleModule::BeoPilotMitzi ] = "Mitzi Capacitor" ;
$VehicleModuleDesc[ $VehicleModule::BeoPilotMitzi ] = "Provides mitzi energy for the Beowulf's forward cannons." ;
$VehicleModuleClass[ $VehicleModule::BeoPilotMitzi ] = "TankPilotMitzi" ;
$VehicleModuleMask[ $VehicleModule::BeoPilotMitzi ] = $VehicleMask::Beowulf ;
$VehicleModuleCount++;

$VehicleModule::BeoPilotEMP = $VehicleModuleCount;
$VehicleModuleName[ $VehicleModule::BeoPilotEMP ] = "EMP Generator" ;
$VehicleModuleDesc[ $VehicleModule::BeoPilotEMP ] = "Charges electromagnetic blasts for the Beowulf's forward cannons." ;
$VehicleModuleClass[ $VehicleModule::BeoPilotEMP ] = "TankPilotEMP" ;
$VehicleModuleMask[ $VehicleModule::BeoPilotEMP ] = $VehicleMask::Beowulf ;
$VehicleModuleCount++;

$VehicleModule::BeoPilotRAXX = $VehicleModuleCount;
$VehicleModuleName[ $VehicleModule::BeoPilotRAXX ] = "Plasma Furnace" ;
$VehicleModuleDesc[ $VehicleModule::BeoPilotRAXX ] = "Generates plasma for the Beowulf's forward cannons." ;
$VehicleModuleClass[ $VehicleModule::BeoPilotRAXX ] = "TankPilotRAXX" ;
$VehicleModuleMask[ $VehicleModule::BeoPilotRAXX ] = $VehicleMask::Beowulf ;
$VehicleModuleCount++;										// +[/soph]

// Armors
//----------------------------------------------------------------------------

$VehicleArmorCount = 0;

$VehicleArmor::Default = $VehicleArmorCount;
$VehicleArmorName[$VehicleArmor::Default] = "Standard Armor Plating";
$VehicleArmorDesc[$VehicleArmor::Default] = "Standard armor used on all vehicles.";
$VehicleArmorMask[$VehicleArmor::Default] = $VehicleMask::All;
$VehicleArmorMassBonus[$VehicleArmor::Default] = 0;
$VehicleArmorHPBonus[$VehicleArmor::Default] = 0;
$VehicleArmorClass[$VehicleArmor::Default] = "DefaultVehicleArmor";
$VehicleArmorCount++;

$VehicleArmor::PolarizedVehicleArmor = $VehicleArmorCount;
$VehicleArmorName[$VehicleArmor::PolarizedVehicleArmor] = "Polarized Armor Plating";
$VehicleArmorDesc[$VehicleArmor::PolarizedVehicleArmor] = "Additional 33% armor, -25% recharge rate.";
$VehicleArmorMask[$VehicleArmor::PolarizedVehicleArmor] = $VehicleMask::All;
$VehicleArmorMassBonus[$VehicleArmor::PolarizedVehicleArmor] = 0;
$VehicleArmorHPBonus[$VehicleArmor::PolarizedVehicleArmor] = 0;
$VehicleArmorClass[$VehicleArmor::PolarizedVehicleArmor] = "PolarizedVehicleArmor";
$VehicleArmorCount++;

// Shields
//----------------------------------------------------------------------------

$VehicleShieldCount = 0;

$VehicleShield::None = $VehicleShieldCount;
$VehicleShieldName[$VehicleShield::None] = "No Shields";
$VehicleShieldDesc[$VehicleShield::None] = "200% recharge rate to the Vehicle and Turrets instead of shields.";
$VehicleShieldMask[$VehicleShield::None] = $VehicleMask::AllDefaultShielded ;	// $VehicleMask::All; -soph
$VehicleShieldMassBonus[$VehicleShield::None] = 0;
$VehicleShieldRechargeBonus[$VehicleShield::None] = 2.0;
$VehicleShieldWeaponBonus[$VehicleShield::None] = 0;
$VehicleShieldStrengthBonus[$VehicleShield::None] = 0.0;
$VehicleShieldClass[$VehicleShield::None] = "NoneShields";
$VehicleShieldCount++;

$VehicleShield::Standard = $VehicleShieldCount;
$VehicleShieldName[$VehicleShield::Standard] = "Standard Proton Shield Matrix";
$VehicleShieldDesc[$VehicleShield::Standard] = "Standard shielding used on all vehicles.";
$VehicleShieldMask[$VehicleShield::Standard] = $VehicleMask::All ;		// $VehicleMask::AllDefaultShielded; -soph
$VehicleShieldMassBonus[$VehicleShield::Standard] = 0;
$VehicleShieldRechargeBonus[$VehicleShield::Standard] = 1.0;
$VehicleShieldWeaponBonus[$VehicleShield::Standard] = 0;
$VehicleShieldStrengthBonus[$VehicleShield::Standard] = 1.0;
$VehicleShieldClass[$VehicleShield::Standard] = "StandardVehicleShields";
$VehicleShieldCount++;

$VehicleShield::JammerField = $VehicleShieldCount;
$VehicleShieldName[$VehicleShield::JammerField] = "Jammer Shield Modulation";
$VehicleShieldDesc[$VehicleShield::JammerField] = "Sacrifices shields to project a Sensor Jammer.";
$VehicleShieldMask[$VehicleShield::JammerField] = 0; //$VehicleMask::Shrike | $VehicleMask::Thundersword | $VehicleMask::Havoc;
$VehicleShieldMassBonus[$VehicleShield::JammerField] = 0;
$VehicleShieldRechargeBonus[$VehicleShield::JammerField] = 1.0;
$VehicleShieldWeaponBonus[$VehicleShield::JammerField] = 0;
$VehicleShieldStrengthBonus[$VehicleShield::JammerField] = 0.0;
$VehicleShieldClass[$VehicleShield::JammerField] = "JammerFieldShields";
$VehicleShieldCount++;

$VehicleShield::HeatShield = $VehicleShieldCount;
$VehicleShieldName[$VehicleShield::HeatShield] = "Plasma Dampening Field";
$VehicleShieldDesc[$VehicleShield::HeatShield] = "Sacrifices shields to nullify heat signature.";
$VehicleShieldMask[$VehicleShield::HeatShield] = 0; //$VehicleMask::AllDefaultShielded;
$VehicleShieldMassBonus[$VehicleShield::HeatShield] = 0;
$VehicleShieldRechargeBonus[$VehicleShield::HeatShield] = 1.0;
$VehicleShieldWeaponBonus[$VehicleShield::HeatShield] = 0;
$VehicleShieldStrengthBonus[$VehicleShield::HeatShield] = 0.0;
$VehicleShieldClass[$VehicleShield::HeatShield] = "HeatShields";
$VehicleShieldCount++;

$VehicleShield::RepulsorField = $VehicleShieldCount;
$VehicleShieldName[$VehicleShield::RepulsorField] = "Repulsor Shield Matrix";
$VehicleShieldDesc[$VehicleShield::RepulsorField] = "Reflects all projectiles except missiles away.";
$VehicleShieldMask[$VehicleShield::RepulsorField] = $VehicleMask::AllDefaultShielded;
$VehicleShieldMassBonus[$VehicleShield::RepulsorField] = 25;
$VehicleShieldRechargeBonus[$VehicleShield::RepulsorField] = 1.0;
$VehicleShieldWeaponBonus[$VehicleShield::RepulsorField] = 0;
$VehicleShieldStrengthBonus[$VehicleShield::RepulsorField] = 1.0;
$VehicleShieldClass[$VehicleShield::RepulsorField] = "RepulsorFieldShields";
$VehicleShieldCount++;

// Vehicle Defs
//----------------------------------------------------------------------------
$VehicleCount = 0;

$VehicleID[ $VehicleMask::Wildcat ] = $VehicleCount ;				// +[soph]
$VehicleName[ $VehicleCount ] = "Wildcat Terragrav" ;				// +
$VehicleStats[ $VehicleCount ] = "Personal hoverbike: single hardpoint" ;	// +
$VehicleData[ $VehicleCount ] = "ScoutVehicle" ;				// +
$VehicleDefaultPrimaryWep[ $VehicleCount ] = $VehicleWeapon::CatLance ;		// +
$VehicleDefaultSecondaryWep[ $VehicleCount ] = $VehicleWeapon::None ;		// +
$VehicleDefaultModule[ $VehicleCount ] = $VehicleModule::Booster ;		// +
$VehicleDefaultShield[ $VehicleCount ] = $VehicleShield::Standard ;		// +
$VehicleDefaultArmor[ $VehicleCount ] = $VehicleArmor::Default ;		// +
$VehicleCount++ ;								// +[/soph]

$VehicleID[$VehicleMask::Shrike] = $VehicleCount;
$VehicleName[$VehicleCount] = "Shrike Fighter";
$VehicleStats[$VehicleCount] = "Fighter/interceptor: two hardpoints" ;		// = "Shrike Hull, 2 weapons"; -soph
$VehicleData[$VehicleCount] = "ScoutFlyer";
$VehicleDefaultPrimaryWep[$VehicleCount] = $VehicleWeapon::ShrikeBlaster;
$VehicleDefaultSecondaryWep[$VehicleCount] = $VehicleWeapon::SpikeArray;
$VehicleDefaultModule[$VehicleCount] = $VehicleModule::FlareMod ;		// $VehicleModule::None;  -soph
$VehicleDefaultShield[$VehicleCount] = $VehicleShield::Standard;
$VehicleDefaultArmor[$VehicleCount] = $VehicleArmor::Default;
$VehicleCount++;

$VehicleID[$VehicleMask::Nightshade] = $VehicleCount;
$VehicleName[$VehicleCount] = "Nightshade Support Craft" ;			// = "Nightshade Infiltration Cruiser";
$VehicleStats[$VehicleCount] = "Support ship: skyhook, inventory, spotter" ;	// = "Thundersword Hull, 1 teleporter";
$VehicleData[$VehicleCount] = "LOSDownFlyer";
$VehicleDefaultPrimaryWep[$VehicleCount] = $VehicleWeapon::None;
$VehicleDefaultSecondaryWep[$VehicleCount] = $VehicleWeapon::None;
$VehicleDefaultModule[$VehicleCount] = $VehicleModule::FlareMod ;		// $VehicleModule::None;  -soph
$VehicleDefaultShield[$VehicleCount] = $VehicleShield::Standard;
$VehicleDefaultArmor[$VehicleCount] = $VehicleArmor::Default;
$VehicleCount++;

$VehicleID[$VehicleMask::Thundersword] = $VehicleCount;
$VehicleName[$VehicleCount] = "Thundersword Cruiser";
$VehicleStats[$VehicleCount] = "Bomber/gunship: multiple weapon mountings" ;	// = "Thundersword Hull, 1-2 manual turrets"; -soph
$VehicleData[$VehicleCount] = "BomberFlyer";
$VehicleDefaultPrimaryWep[$VehicleCount] = $VehicleWeapon::BomberBomb ;		// $VehicleWeapon::TSwordREmpty; -soph
$VehicleDefaultSecondaryWep[$VehicleCount] = $VehicleWeapon::TSwordBellyKM ;	// $VehicleWeapon::BomberBomb; -soph
$VehicleDefaultModule[$VehicleCount] = $VehicleModule::Inventory ;		// $VehicleModule::None;  -soph
$VehicleDefaultShield[$VehicleCount] = $VehicleShield::Standard;
$VehicleDefaultArmor[$VehicleCount] = $VehicleArmor::Default;
$VehicleCount++;

$VehicleID[$VehicleMask::Havoc] = $VehicleCount;
$VehicleName[$VehicleCount] = "Havoc";
$VehicleStats[$VehicleCount] = "Troop transport: room for six" ;		// = "Troop carrier, 5 passengers"; -soph
$VehicleData[$VehicleCount] = "HAPCFlyer";
$VehicleDefaultPrimaryWep[$VehicleCount] = $VehicleWeapon::None;
$VehicleDefaultSecondaryWep[$VehicleCount] = $VehicleWeapon::None;
$VehicleDefaultModule[$VehicleCount] = $VehicleModule::Inventory ;		// $VehicleModule::None;  -soph
$VehicleDefaultShield[$VehicleCount] = $VehicleShield::Standard;
$VehicleDefaultArmor[$VehicleCount] = $VehicleArmor::Default;
$VehicleCount++;

$VehicleID[$VehicleMask::Beowulf] = $VehicleCount;
$VehicleName[$VehicleCount] = "Beowulf Tank";
$VehicleStats[$VehicleCount] = "Ground assault vehicle: multiple weapon mountings" ;	// = "Ground Vehicle, 1 manual turret"; -soph
$VehicleData[$VehicleCount] = "AssaultVehicle";
$VehicleDefaultPrimaryWep[$VehicleCount] = $VehicleWeapon::TankCometCannon ;	// $VehicleWeapon::TankAABarrel; -soph
$VehicleDefaultSecondaryWep[$VehicleCount] = $VehicleWeapon::TankAABarrel ;	// $VehicleWeapon::TankCometCannon; -soph
$VehicleDefaultModule[$VehicleCount] = $VehicleModule::BeoPilotMitzi ;		// $VehicleModule::None; -soph
$VehicleDefaultShield[$VehicleCount] = $VehicleShield::Standard;
$VehicleDefaultArmor[$VehicleCount] = $VehicleArmor::Default;
$VehicleCount++;

//----------------------------------------------------------------------------
// MECH SYSTEM

// Mech String Table
//----------------------------------------------------------------------------
$MechHPPerTon = 2;

// Hull Types
//----------------------------------------------------------------------------

$MechHull::Light = 1 << 0;
$MechHull::Medium = 1 << 1;
$MechHull::Heavy = 1 << 2;

$MechHullName[$MechHull::Light] = "Light";
$MechHullName[$MechHull::Medium] = "Medium";
$MechHullName[$MechHull::Heavy] = "Heavy";

// Mech Types
//----------------------------------------------------------------------------

$MechMask::Patriot = 1 << 0;
$MechMask::Emancipator = 1 << 1;

$MechMask::All = $MechMask::Patriot | $MechMask::Emancipator; // -1

// Mech Hardpoints
//----------------------------------------------------------------------------

$MechHardpointType::None = 1 << 0;
$MechHardpointType::Projectile = 1 << 1;
$MechHardpointType::Energy = 1 << 2;
$MechHardpointType::Missile = 1 << 3;
$MechHardpointType::Omni = $MechHardpointType::Projectile | $MechHardpointType::Energy | $MechHardpointType::Missile;
$MechHardpointType::Standard = $MechHardpointType::Projectile | $MechHardpointType::Energy;

// Mech Firegroups
//----------------------------------------------------------------------------

$MechMaxFireGroups = 0;

$MechFiregroup::Alpha = 1 << $MechMaxFireGroups;
$MechFiregroupMask[$MechMaxFireGroups] = $MechFiregroup::Alpha;
$MechFiregroupID[$MechFiregroup::Alpha] = $MechMaxFireGroups;
$MechFiregroupName[$MechMaxFireGroups] = "Alpha";
$MechMaxFireGroups++;

$MechFiregroup::Beta = 1 << $MechMaxFireGroups;
$MechFiregroupMask[$MechMaxFireGroups] = $MechFiregroup::Beta;
$MechFiregroupID[$MechFiregroup::Beta] = $MechMaxFireGroups;
$MechFiregroupName[$MechMaxFireGroups] = "Beta";
$MechMaxFireGroups++;

$MechFiregroup::Gamma = 1 << $MechMaxFireGroups;
$MechFiregroupMask[$MechMaxFireGroups] = $MechFiregroup::Gamma;
$MechFiregroupID[$MechFiregroup::Gamma] = $MechMaxFireGroups;
$MechFiregroupName[$MechMaxFireGroups] = "Gamma";
$MechMaxFireGroups++;

// Weapons
//----------------------------------------------------------------------------

$MechWeaponCount = 0;

$MechWeapon::None = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Empty Hardpoint";
$MechWeaponDesc[$MechWeaponCount] = "No weapon installed on this hardpoint";
$MechWeaponShape[$MechWeaponCount] = "None";
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 0;
$MechWeaponHeat[$MechWeaponCount] = 0;
$MechWeaponWeight[$MechWeaponCount] = 0;
$MechWeaponFireTime[$MechWeaponCount] = 0;
$MechWeaponReloadTime[$MechWeaponCount] = 0;
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = -1; //$MechHardpointType::None;
$MechWeaponClass[$MechWeaponCount] = "NoMechWeapon";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::Autogun = $MechWeaponCount ;				// +[soph]
$MechWeaponName[ $MechWeaponCount ] = "Light Autogun" ;			// +
$MechWeaponDesc[ $MechWeaponCount ] = "Kinetic repeater: minimal heat output" ;
$MechWeaponShape[ $MechWeaponCount ] = "AC" ;				// +
$MechWeaponUsesAmmo[ $MechWeaponCount ] = true ;			// +
$MechWeaponAmmoCount[ $MechWeaponCount ] = 1000 ;			// +
$MechWeaponHeat[ $MechWeaponCount ] = 0.5 ;				// +
$MechWeaponWeight[ $MechWeaponCount ] = 3 ;				// +
$MechWeaponFireTime[ $MechWeaponCount ] = 200 ;				// +
$MechWeaponReloadTime[ $MechWeaponCount ] = 200 ;			// +
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +
$MechWeaponMask[ $MechWeaponCount ] = $MechMask::All ;			// +
$MechWeaponHardpoint[ $MechWeaponCount ] = $MechHardpointType::Projectile ;
$MechWeaponClass[ $MechWeaponCount ] = "Autogun" ;			// +
$MechWeaponDefaultFiregroup[ $MechWeaponCount ] = $MechFiregroup::Alpha ;
$MechWeaponCount++;							// +[/soph]

$MechWeapon::Flamer = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "RAXX Flamer";
$MechWeaponDesc[$MechWeaponCount] = "Plasma flamer: moderate heat output" ;	// "Flamethrower, for when you need to cook a Mech."; -soph
$MechWeaponShape[$MechWeaponCount] = "Plasma";
$MechWeaponUsesAmmo[$MechWeaponCount] = true ;				// = false; -soph
$MechWeaponAmmoCount[$MechWeaponCount] = 900 ;				// = 20; -soph
$MechWeaponHeat[$MechWeaponCount] = 0.5 ;				// = 2; -soph
$MechWeaponWeight[$MechWeaponCount] = 4;
$MechWeaponFireTime[$MechWeaponCount] = 50;
$MechWeaponReloadTime[$MechWeaponCount] = 50;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "Flamer";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta ;	// = $MechFiregroup::Alpha; -soph
$MechWeaponCount++;

$MechWeapon::AC5 = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "AutoCannon";
$MechWeaponDesc[$MechWeaponCount] = "Explosive cannon: rapid-fire, minimal heat" ;	// "AutoCannon, high RoF, good damage, minimal heat."; -soph
$MechWeaponShape[$MechWeaponCount] = "AC";
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 800 ;				// = 1000; -soph
$MechWeaponHeat[$MechWeaponCount] = 1;
$MechWeaponWeight[$MechWeaponCount] = 5;
$MechWeaponFireTime[$MechWeaponCount] = 200;
$MechWeaponReloadTime[$MechWeaponCount] = 200;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Projectile;
$MechWeaponClass[$MechWeaponCount] = "AC5";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::UltraAC10 = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Ultra AutoCannon";
$MechWeaponDesc[$MechWeaponCount] = "Spread autocannon: runs cool" ;	// "Shotgun + AutoCannon - Very bulky but efficient."; -soph
$MechWeaponShape[$MechWeaponCount] = "Cannon" ; 			// "AC"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 200 ;				// = 500; -soph
$MechWeaponHeat[$MechWeaponCount] = 3 ;					// = 4; -soph
$MechWeaponWeight[$MechWeaponCount] = 7 ;				// = 10; -soph
$MechWeaponFireTime[$MechWeaponCount] = 600 ;				// = 800; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 200;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Bushwacker | $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Projectile;
$MechWeaponClass[$MechWeaponCount] = "UltraAC10";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::StarHammer = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Star Hammer";
$MechWeaponDesc[$MechWeaponCount] = "High explosive cannon: moderate heat output" ;	// "High explosive powered by a pulse detonation drive."; -soph
$MechWeaponShape[$MechWeaponCount] = "Heavy" ;				// "Cannon"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 50 ;				// = 250; -soph
$MechWeaponHeat[$MechWeaponCount] = 20;
$MechWeaponWeight[$MechWeaponCount] = 14 ;				// 15; -soph
$MechWeaponFireTime[$MechWeaponCount] = 2500 ;				// 3500; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// $MechHardpointType::Projectile; -soph
$MechWeaponClass[$MechWeaponCount] = "StarHammer";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta ;	// = $MechFiregroup::Alpha; -soph
$MechWeaponCount++;

$MechWeapon::SpikeCannon = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Spike Cannon";
$MechWeaponDesc[$MechWeaponCount] = "Energy cannon: low-energy, low-temperature" ;	// "Landspike turret modified to fit on a Mech, minimal heat."; -soph
$MechWeaponShape[$MechWeaponCount] = "Cannon";
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 250 ;				// = 500; -soph
$MechWeaponHeat[$MechWeaponCount] = 3;
$MechWeaponWeight[$MechWeaponCount] = 2 ;				// 4; -soph
$MechWeaponFireTime[$MechWeaponCount] = 1250 ;				// 1500; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 4 ;				// rebal 7 ; +soph 
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Projectile;
$MechWeaponClass[$MechWeaponCount] = "SpikeCannon";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha ;	// = $MechFiregroup::Beta; -soph
$MechWeaponCount++;

$MechWeapon::PhaserCannon = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Phaser Cannon";
$MechWeaponDesc[$MechWeaponCount] = "Kinetic burst cannon: low heat, low energy draw" ;	// "High intensity compressed Phaser energy."; -soph
$MechWeaponShape[$MechWeaponCount] = "Cannon" ;				// "Plasma"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 1000;
$MechWeaponHeat[$MechWeaponCount] = 4 ;					// = 3; -soph
$MechWeaponWeight[$MechWeaponCount] = 4 ;				// = 6; -soph
$MechWeaponFireTime[$MechWeaponCount] = 900 ;				// = 1500; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 7 ;				// rebal 8 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "PhaserCannon";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::Coilgun = $MechWeaponCount ;				// +[soph]
$MechWeaponName[ $MechWeaponCount ] = "Coilgun" ;			// +
$MechWeaponDesc[ $MechWeaponCount ] = "Long-range rifle: low heat, low energy usage" ;
$MechWeaponShape[ $MechWeaponCount ] = "Cannon" ; 			// + "AC" ;
$MechWeaponUsesAmmo[ $MechWeaponCount ] = true ;			// +
$MechWeaponAmmoCount[ $MechWeaponCount ] = 125 ;			// +
$MechWeaponHeat[ $MechWeaponCount ] = 8 ;				// +
$MechWeaponWeight[ $MechWeaponCount ] = 8 ;				// +
$MechWeaponFireTime[ $MechWeaponCount ] = 2000 ;			// +
$MechWeaponReloadTime[ $MechWeaponCount ] = 250 ;			// +
$MechWeaponEnergyCost[ $MechWeaponCount ] = 19 ;			// +
$MechWeaponMask[ $MechWeaponCount ] = $MechMask::Bushwacker | $MechMask::Patriot | $MechMask::Emancipator ;
$MechWeaponHardpoint[ $MechWeaponCount ] = $MechHardpointType::Projectile ;
$MechWeaponClass[ $MechWeaponCount ] = "Coilgun" ;			// +
$MechWeaponDefaultFiregroup[ $MechWeaponCount ] = $MechFiregroup::Alpha ;
$MechWeaponCount++ ;							// +[/soph]

$MechWeapon::Gauss = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Railgun";
$MechWeaponDesc[$MechWeaponCount] = "Kinetic penetrator: moderate energy demand" ;	// "Fires extremely fast solid shells."; -soph
$MechWeaponShape[$MechWeaponCount] = "Heavy" ;				// "Cannon"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 50 ;				// = 20; -soph
$MechWeaponHeat[$MechWeaponCount] = 25 ;				// 2; -soph
$MechWeaponWeight[$MechWeaponCount] = 13 ;				// 15; -soph
$MechWeaponFireTime[$MechWeaponCount] = 5000;
$MechWeaponReloadTime[$MechWeaponCount] = 5000;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 40 ;			// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// $MechHardpointType::Projectile; -soph
$MechWeaponClass[$MechWeaponCount] = "Gauss";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha ;	// = $MechFiregroup::Beta; -soph
$MechWeaponCount++;

$MechWeapon::KM = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "KM Gattling Blaster";
$MechWeaponDesc[$MechWeaponCount] = "Rapid energy blaster: burns energy, runs hot" ;	// "AA Blaster in an automatic firing configuration."; -soph
$MechWeaponShape[$MechWeaponCount] = "AC";
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 1000;
$MechWeaponHeat[$MechWeaponCount] = 5.5 ;				// = 3; -soph
$MechWeaponWeight[$MechWeaponCount] = 3;
$MechWeaponFireTime[$MechWeaponCount] = 250;
$MechWeaponReloadTime[$MechWeaponCount] = 200;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 8 ;				// rebal 12 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "KM";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::PPC = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Particle Projector Cannon (PPC)";
$MechWeaponDesc[$MechWeaponCount] = "Lightning cannon: huge reactor drain" ;	// "The power of Lightning."; -soph
$MechWeaponShape[$MechWeaponCount] = "Laser" ;				// = "Plasma"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 240;
$MechWeaponHeat[$MechWeaponCount] = 55 ;				// rebal 45 ;	= 40; -soph
$MechWeaponWeight[$MechWeaponCount] = 5 ;				// 6; -soph
$MechWeaponFireTime[$MechWeaponCount] = 6000;
$MechWeaponReloadTime[$MechWeaponCount] = 1000;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 50 ;			// rebal 95 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "PPC";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::PlasmaCannon = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Plasma Cannon";
$MechWeaponDesc[$MechWeaponCount] = "Thermal cannon: low heat, mild reactor drain" ;	// "Plasma turret modified to fit a Mech."; -soph
$MechWeaponShape[$MechWeaponCount] = "Plasma";
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 240;
$MechWeaponHeat[$MechWeaponCount] = 8 ;					// 12; -soph
$MechWeaponWeight[$MechWeaponCount] = 6 ;				// 8; -soph
$MechWeaponFireTime[$MechWeaponCount] = 1000 ;				// 1500; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 20 ;			// rebal 17 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "PlasmaCannon";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta;	// = $MechFiregroup::Alpha; -soph
$MechWeaponCount++;

$MechWeapon::CometCannon = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Comet Cannon";
$MechWeaponDesc[$MechWeaponCount] = "Heavy energy weapon: large reactor draw" ;	// "Powerful fast moving blast of energy."; -soph
$MechWeaponShape[$MechWeaponCount] = "Heavy" ;				// "Cannon"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 250;
$MechWeaponHeat[$MechWeaponCount] = 20 ;				// 25; -soph
$MechWeaponWeight[$MechWeaponCount] = 9 ;				// 10; -soph
$MechWeaponFireTime[$MechWeaponCount] = 2000;
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 60 ;			// rebal 67 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// $MechHardpointType::Energy; -soph
$MechWeaponClass[$MechWeaponCount] = "CometCannon";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta ;	// = $MechFiregroup::Alpha; -soph
$MechWeaponCount++;

$MechWeapon::ShockBlaster = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Shock Blaster Cannon";
$MechWeaponDesc[$MechWeaponCount] = "Wide-angle lightning bomb: reactor hog" ;	// "Danger danger! High voltage!"; -soph
$MechWeaponShape[$MechWeaponCount] = "Cannon";
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 240;
$MechWeaponHeat[$MechWeaponCount] = 15;
$MechWeaponWeight[$MechWeaponCount] = 11 ;				// 12; -soph
$MechWeaponFireTime[$MechWeaponCount] = 5000;
$MechWeaponReloadTime[$MechWeaponCount] = 2000;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 70 ;			// rebal 84 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "ShockBlaster";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta ;	// = $MechFiregroup::Alpha; -soph
$MechWeaponCount++;

$MechWeapon::DoomBlaster = $MechWeaponCount ;				// +[soph]
$MechWeaponName[$MechWeaponCount] = "Trident Blaster" ;			// +
$MechWeaponDesc[$MechWeaponCount] = "Shield-piercing barrage: moderate reactor draw" ;
$MechWeaponShape[$MechWeaponCount] = "Heavy" ;				// +
$MechWeaponUsesAmmo[$MechWeaponCount] = false ;				// +
$MechWeaponAmmoCount[$MechWeaponCount] = 240 ;				// +
$MechWeaponHeat[$MechWeaponCount] = 15 ;				// +
$MechWeaponWeight[$MechWeaponCount] = 10 ;				// + rebal 11 ;
$MechWeaponFireTime[$MechWeaponCount] = 2250 ;				// +
$MechWeaponReloadTime[$MechWeaponCount] = 500 ;				// +
$MechWeaponEnergyCost[ $MechWeaponCount ] = 45 ;			// + rebal 47 ;
$MechWeaponMask[$MechWeaponCount] = $MechMask::Emancipator ;		// +
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// +
$MechWeaponClass[$MechWeaponCount] = "BlasterCannon" ;			// +
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta ;	// +
$MechWeaponCount++;							// +[/soph]

$MechWeapon::LLAS = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Beam Laser";
$MechWeaponDesc[$MechWeaponCount] = "300m energy beam: moderate heat, moderate reactor drain" ;	// = "300m range, moderate damage"; -soph
$MechWeaponShape[$MechWeaponCount] = "Laser";
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 20;
$MechWeaponHeat[$MechWeaponCount] = 12;
$MechWeaponWeight[$MechWeaponCount] = 6 ;				// = 8; -soph
$MechWeaponFireTime[$MechWeaponCount] = 3000;
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 25 ;			// rebal 29 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "LLAS";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::LPLAS = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Pulse Laser";
$MechWeaponDesc[$MechWeaponCount] = "200m energy multibeam: high heat, large reactor draw" ;	// = "150m range, high damage"; -soph
$MechWeaponShape[$MechWeaponCount] = "Laser";
$MechWeaponUsesAmmo[$MechWeaponCount] = false;
$MechWeaponAmmoCount[$MechWeaponCount] = 20;
$MechWeaponHeat[$MechWeaponCount] = 5 ;		// note: applied x4	// = 16; -soph
$MechWeaponWeight[$MechWeaponCount] = 8 ;				// = 12; -soph
$MechWeaponFireTime[$MechWeaponCount] = 2000;
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 33 ;			// rebal 38 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Energy;
$MechWeaponClass[$MechWeaponCount] = "LPLAS";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

$MechWeapon::PBW = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "Particle Beam Weapon (PBW)";
$MechWeaponDesc[$MechWeaponCount] = "Kinetic beam: massive heat & energy demands" ;	// = "Short range powerful kinetic punch beam."; -soph
$MechWeaponShape[$MechWeaponCount] = "Laser" ;				// "Cannon"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 50;
$MechWeaponHeat[$MechWeaponCount] = 40 ;				// 50; -soph
$MechWeaponWeight[$MechWeaponCount] = 12 ;				// 15; -soph
$MechWeaponFireTime[$MechWeaponCount] = 6500;
$MechWeaponReloadTime[$MechWeaponCount] = 3000;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 175 ;			// rebal 132 ; +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// $MechHardpointType::Energy; -soph
$MechWeaponClass[$MechWeaponCount] = "PBW";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Alpha;
$MechWeaponCount++;

//$MechWeapon::IonCannon = $MechWeaponCount;				// abusable -[soph]
//$MechWeaponName[$MechWeaponCount] = "Small Ion Cannon";		// -
//$MechWeaponDesc[$MechWeaponCount] = "Mobile Ion cannon, very destructive.";
//$MechWeaponShape[$MechWeaponCount] = "Missile";			// -
//$MechWeaponUsesAmmo[$MechWeaponCount] = false;			// -
//$MechWeaponAmmoCount[$MechWeaponCount] = 240;				// -
//$MechWeaponHeat[$MechWeaponCount] = 300;				// -
//$MechWeaponWeight[$MechWeaponCount] = 30;				// -
//$MechWeaponFireTime[$MechWeaponCount] = 8000;				// -
//$MechWeaponReloadTime[$MechWeaponCount] = 2000;			// -
//$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
//$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile;	// -
//$MechWeaponClass[$MechWeaponCount] = "IonCannon";			// -
//$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Gamma;
//$MechWeaponCount++;							// -[/soph]

$MechWeapon::MechMortar = $MechWeaponCount ;				// hopefully less abusable +[soph]
$MechWeaponName[$MechWeaponCount] = "Blast Mortar" ;			// +
$MechWeaponDesc[$MechWeaponCount] = "Explosive artillery: moderate heat output" ;
$MechWeaponShape[$MechWeaponCount] = "Heavy" ;				// +
$MechWeaponUsesAmmo[$MechWeaponCount] = true ;				// +
$MechWeaponAmmoCount[$MechWeaponCount] = 80 ;				// +
$MechWeaponHeat[$MechWeaponCount] = 35 ;				// + was 30, needs to be reasonably hot
$MechWeaponWeight[$MechWeaponCount] = 15 ;				// + rebal 16 ;
$MechWeaponFireTime[$MechWeaponCount] = 3000 ;				// +
$MechWeaponReloadTime[$MechWeaponCount] = 1000 ;			// +
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator ;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// +
$MechWeaponClass[$MechWeaponCount] = "BlastMortar" ;			// +
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Beta ;	// +
$MechWeaponCount++ ;							// +[/soph]

$MechWeapon::SRM4 = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "SRM4";
$MechWeaponDesc[$MechWeaponCount] = "Short Range Missile Array, 4-rack.";
$MechWeaponShape[$MechWeaponCount] = "LtMissile" ;			// "Missile"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 120;
$MechWeaponHeat[$MechWeaponCount] = 4 ;		// note: applied x5	// = 16; -soph
$MechWeaponWeight[$MechWeaponCount] = 4;
$MechWeaponFireTime[$MechWeaponCount] = 5000 ;				// = 5500; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile;
$MechWeaponClass[$MechWeaponCount] = "SRM4";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Gamma;
$MechWeaponCount++;

$MechWeapon::SRM6 = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "SRM6";
$MechWeaponDesc[$MechWeaponCount] = "Short Range Missile Array, 6-rack.";
$MechWeaponShape[$MechWeaponCount] = "LtMissile" ;			// = "Missile"; -soph
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 150 ;				// = 120; -soph
$MechWeaponHeat[$MechWeaponCount] = 4 ;		// note: applied x7	// = 20; -soph
$MechWeaponWeight[$MechWeaponCount] = 6;
$MechWeaponFireTime[$MechWeaponCount] = 6000 ;				// = 8000; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 500;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile;
$MechWeaponClass[$MechWeaponCount] = "SRM6";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Gamma;
$MechWeaponCount++;

$MechWeapon::LRM5 = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "LRM5";
$MechWeaponDesc[$MechWeaponCount] = "Long Range Missile Array, 5-rack.";
$MechWeaponShape[$MechWeaponCount] = "Missile";
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 240;
$MechWeaponHeat[$MechWeaponCount] = 5 ;		// note: applied x6	// = 30; -soph
$MechWeaponWeight[$MechWeaponCount] = 5;
$MechWeaponFireTime[$MechWeaponCount] = 6000;
$MechWeaponReloadTime[$MechWeaponCount] = 2000;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::All;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile;
$MechWeaponClass[$MechWeaponCount] = "LRM5";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Gamma;
$MechWeaponCount++;

$MechWeapon::LRM10 = $MechWeaponCount;
$MechWeaponName[$MechWeaponCount] = "LRM10";
$MechWeaponDesc[$MechWeaponCount] = "Long Range Missile Array, 10-rack.";
$MechWeaponShape[$MechWeaponCount] = "Missile";
$MechWeaponUsesAmmo[$MechWeaponCount] = true;
$MechWeaponAmmoCount[$MechWeaponCount] = 300 ;				// = 240; -soph
$MechWeaponHeat[$MechWeaponCount] = 5 ;		// note: applied x11	// = 60; -soph
$MechWeaponWeight[$MechWeaponCount] = 10;
$MechWeaponFireTime[$MechWeaponCount] = 7500 ;				// = 12000; -soph
$MechWeaponReloadTime[$MechWeaponCount] = 2000;
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +soph
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile;
$MechWeaponClass[$MechWeaponCount] = "LRM10";
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Gamma;
$MechWeaponCount++;

$MechWeapon::Raptor = $MechWeaponCount ;				// +[soph]
$MechWeaponName[$MechWeaponCount] = "Raptor CM1" ;			// +
$MechWeaponDesc[$MechWeaponCount] = "Long Range Cruise Missile." ;	// +
$MechWeaponShape[$MechWeaponCount] = "Missile" ;			// +
$MechWeaponUsesAmmo[$MechWeaponCount] = true ;				// +
$MechWeaponAmmoCount[$MechWeaponCount] = 15 ;				// +
$MechWeaponHeat[$MechWeaponCount] = 79 ;				// +
$MechWeaponWeight[$MechWeaponCount] = 9 ;				// +
$MechWeaponFireTime[$MechWeaponCount] = 11000 ;				// +
$MechWeaponReloadTime[$MechWeaponCount] = 2000 ;			// +
$MechWeaponEnergyCost[ $MechWeaponCount ] = 0 ;				// +
$MechWeaponMask[$MechWeaponCount] = $MechMask::Patriot | $MechMask::Emancipator ;
$MechWeaponHardpoint[$MechWeaponCount] = $MechHardpointType::Missile ;	// +
$MechWeaponClass[$MechWeaponCount] = "Raptor" ;				// +
$MechWeaponDefaultFiregroup[$MechWeaponCount] = $MechFiregroup::Gamma ;	// +
$MechWeaponCount++ ;							// +[/soph]

// Modules
//----------------------------------------------------------------------------

$MechModuleCount = 0;

$MechModule::None = $MechModuleCount;
$MechModuleName[$MechModuleCount] = "None";
$MechModuleDesc[$MechModuleCount] = "No Module installed";
$MechModuleClass[$MechModuleCount] = "NoneModule";
$MechModuleMask[$MechModuleCount] = $MechMask::All;
$MechModuleWeight[$MechModuleCount] = 0;
$MechModuleCount++;

$MechModule::AmmoCache = $MechModuleCount;
$MechModuleName[$MechModuleCount] = "Coolant Tank";
$MechModuleDesc[$MechModuleCount] = "Doubles coolant capacity.";
$MechModuleClass[$MechModuleCount] = "AmmoCacheModule";
$MechModuleMask[$MechModuleCount] = $MechMask::All;
$MechModuleWeight[$MechModuleCount] = 5;
$MechModuleCount++;

$MechModule::SynomiumDevice = $MechModuleCount;
$MechModuleName[$MechModuleCount] = "Synomium Device";
$MechModuleDesc[$MechModuleCount] = "Negates any EMP effects";
$MechModuleClass[$MechModuleCount] = "SynomiumModule";
$MechModuleMask[$MechModuleCount] = $MechMask::All;
$MechModuleWeight[$MechModuleCount] = 5;
$MechModuleCount++;

// Modules
//----------------------------------------------------------------------------

$MechEquipCount = 0;

//$MechEquip::None = $MechEquipCount;					// +[soph]
//$MechEquipName[$MechEquipCount] = "EMPTY";				// +
//$MechEquipDesc[$MechEquipCount] = "Empty Equipment Slot";		// +
//$MechEquipClass[$MechEquipCount] = "NoneEquip";			// +
//$MechEquipMask[$MechEquipCount] = $MechMask::All;			// +
//$MechEquipWeight[$MechEquipCount] = 0;				// +
//$MechEquipCount++;							// +[/soph]

$MechEquip::JumpJets = $MechEquipCount;
$MechEquipName[$MechEquipCount] = "Jump Jets";
$MechEquipDesc[$MechEquipCount] = "Allows your Mech to fly.";
$MechEquipClass[$MechEquipCount] = "JumpJetsEquip";
$MechEquipMask[$MechEquipCount] = $MechMask::All;
$MechEquipWeight[$MechEquipCount] = 5;
$MechEquipCount++;

$MechEquip::Shields = $MechEquipCount ;					// +[soph]
$MechEquipName[ $MechEquipCount ] = "Shield Generator" ;		// +
$MechEquipDesc[ $MechEquipCount ] = "Regenerating protection, weakens reactor" ;
$MechEquipClass[ $MechEquipCount ] = "ShieldEquip" ;			// +
$MechEquipMask[ $MechEquipCount ] = $MechMask::All ;			// +
$MechEquipWeight[ $MechEquipCount ] = 9 ;				// +
$MechEquipCount++ ;							// +
									// +
$MechEquip::Synomium = $MechEquipCount ;				// +
$MechEquipName[ $MechEquipCount ] = "Synomium Device" ;			// +
$MechEquipDesc[ $MechEquipCount ] = "Resists EMP damage, improves reactor" ;
$MechEquipClass[ $MechEquipCount ] = "SynomiumModule" ;			// +
$MechEquipMask[ $MechEquipCount ] = $MechMask::All ;			// +
$MechEquipWeight[ $MechEquipCount ] = 4 ;				// +
$MechEquipCount++ ;							// +
									// +
$MechEquip::Coolant = $MechEquipCount ;					// +
$MechEquipName[ $MechEquipCount ] = "Coolant Tank" ;			// +
$MechEquipDesc[ $MechEquipCount ] = "Doubles coolant capacity." ;	// +
$MechEquipClass[ $MechEquipCount ] = "AmmoCacheModule" ;		// +
$MechEquipMask[ $MechEquipCount ] = $MechMask::All ;			// +
$MechEquipWeight[ $MechEquipCount ] = 5 ;				// +
$MechEquipCount++ ;							// +[/soph]

// Armors
//----------------------------------------------------------------------------

$MechArmorCount = 0;

$MechArmor::Default = $MechArmorCount;
$MechArmorName[$MechArmorCount] = "Standard Tritanium Armor Plating";
$MechArmorDesc[$MechArmorCount] = "Standard armor used on all Mechs.";
$MechArmorMask[$MechArmorCount] = $MechMask::All;
$MechArmorHPPerTon[$MechArmorCount] = 50;
$MechArmorClass[$MechArmorCount] = "DefaultMechArmor";
$MechArmorCount++;

// Shields
//----------------------------------------------------------------------------

$MechShieldCount = 0;

$MechShield::Standard = $MechShieldCount;
$MechShieldName[$MechShieldCount] = "No Shields";
$MechShieldDesc[$MechShieldCount] = "Mechs do not have shields, silly.";
$MechShieldMask[$MechShieldCount] = $MechMask::All;
$MechShieldRechargeBonus[$MechShieldCount] = 0.0;
$MechShieldWeaponBonus[$MechShieldCount] = 0;
$MechShieldStrengthBonus[$MechShieldCount] = 0.0;
$MechShieldClass[$MechShieldCount] = "StandardMechShields";
$MechShieldCount++;

// Mech  Defs
//----------------------------------------------------------------------------

$MechCount = 0;

$MechMask["Emancipator"] = $MechMask::Emancipator;
$MechID[$MechMask::Emancipator] = $MechCount;
$MechName[$MechCount] = "Emancipator EAX100 Primary";			// = "Emancipator Heavy Mech"; -soph
$MechSubName[$MechCount] = "Iron";
$MechStats[$MechCount] = "100T mech, standard loadout";	// = "100T Mech, 2 Standard 4 Omni hardpoints"; -soph
$MechData[$MechCount] = "Emancipator";
$MechWeightLimit[$MechCount] = 100;
$MechHeatLimit[$MechCount] = 300;
$MechMaxWeapons[$MechCount] = 6;
$MechReactorSize[$MechMask::Emancipator] = 750 ;			// 150; -soph
$MechRechargeRate[$MechMask::Emancipator] = 15 / 32 ;			// 11 / 32; -soph
$MechHardPointSlot[$MechCount, 2] = $MechHardpointType::Omni;
$MechHardPointSlot[$MechCount, 3] = $MechHardpointType::Omni;
$MechHardPointSlot[$MechCount, 4] = $MechHardpointType::Standard;
$MechHardPointSlot[$MechCount, 5] = $MechHardpointType::Standard;
$MechHardPointSlot[$MechCount, 6] = $MechHardpointType::Omni;
$MechHardPointSlot[$MechCount, 7] = $MechHardpointType::Omni;
$MechDefaultArmorWeight[$MechCount] = 22; 
$MechDefaultHeatSinkWeight[$MechCount] = 23 ;				// 18; -soph 
$MechDefaultWeapon[$MechCount, 0] = $MechWeapon::StarHammer;
$MechDefaultWeapon[$MechCount, 1] = $MechWeapon::StarHammer;
$MechDefaultWeapon[$MechCount, 2] = $MechWeapon::SpikeCannon;
$MechDefaultWeapon[$MechCount, 3] = $MechWeapon::Autogun ;		// $MechWeapon::SpikeCannon; -soph
$MechDefaultWeapon[$MechCount, 4] = $MechWeapon::Autogun ;		// $MechWeapon::LRM5; -soph
$MechDefaultWeapon[$MechCount, 5] = $MechWeapon::LRM10 ;		// $MechWeapon::LRM5; -soph
$MechDefaultEquip[ $MechCount , $MechEquip::JumpJets ] = 0 ;		// +[soph]
$MechDefaultEquip[ $MechCount , $MechEquip::Shields ] = 1 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Synomium ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Coolant ] = 0 ;		// +[/soph]
//$MechDefaultModule[$MechCount] = $MechModule::None;
//$MechDefaultEquip[$MechCount] = $MechEquip::None;			// grr this isnt even used -soph
//$MechDefaultShield[$MechCount] = $MechShield::Standard;
$MechDefaultArmor[$MechCount] = $MechArmor::Default;
$MechCount++;

$MechName[ $MechCount ] = "Emancipator EAX100 Secondary" ;		// +[soph]
$MechSubName[ $MechCount ] = "Iron" ;					// +
$MechStats[ $MechCount ] = "100T mech, artillery loadout" ;		// +
$MechData[ $MechCount ] = "Emancipator" ;				// +
$MechWeightLimit[ $MechCount ] = 100 ;					// +
$MechHeatLimit[ $MechCount ] = 300 ;					// +
$MechMaxWeapons[ $MechCount ] = 6 ;					// +
$MechHardPointSlot[ $MechCount , 2 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 3 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 4 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 5 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 6 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 7 ] = $MechHardpointType::Omni ;	// +
$MechDefaultArmorWeight[ $MechCount ] = 29 ;				// + = 35 ;
$MechDefaultHeatSinkWeight[ $MechCount ] = 25 ;				// + = 30 ;
$MechDefaultWeapon[ $MechCount , 0 ] = $MechWeapon::Autogun ;		// + = $MechWeapon::SRM4 ;
$MechDefaultWeapon[ $MechCount , 1 ] = $MechWeapon::Autogun ;		// + = $MechWeapon::SRM4 ;
$MechDefaultWeapon[ $MechCount , 2 ] = $MechWeapon::PhaserCannon ;	// + = $MechWeapon::KM ;
$MechDefaultWeapon[ $MechCount , 3 ] = $MechWeapon::KM ;		// + = $MechWeapon::KM ;
$MechDefaultWeapon[ $MechCount , 4 ] = $MechWeapon::LRM5 ;		// +
$MechDefaultWeapon[ $MechCount , 5 ] = $MechWeapon::MechMortar ;	// +
$MechDefaultEquip[ $MechCount , $MechEquip::JumpJets ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Shields ] = 1 ;		// + = 0 ;
$MechDefaultEquip[ $MechCount , $MechEquip::Synomium ] = 1 ;		// + = 0 ;
$MechDefaultEquip[ $MechCount , $MechEquip::Coolant ] = 0 ;		// +
$MechDefaultArmor[ $MechCount ] = $MechArmor::Default ;			// +
$MechCount++ ;								// +
									// +
$MechName[ $MechCount ] = "Emancipator EAX100 Tertiary" ;		// +
$MechSubName[ $MechCount ] = "Iron" ;					// +
$MechStats[ $MechCount ] = "100T mech, brawler loadout" ;		// +
$MechData[ $MechCount ] = "Emancipator" ;				// +
$MechWeightLimit[ $MechCount ] = 100 ;					// +
$MechHeatLimit[ $MechCount ] = 300 ;					// +
$MechMaxWeapons[ $MechCount ] = 6 ;					// +
$MechHardPointSlot[ $MechCount , 2 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 3 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 4 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 5 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 6 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 7 ] = $MechHardpointType::Omni ;	// +
$MechDefaultArmorWeight[ $MechCount ] = 40 ;				// +
$MechDefaultHeatSinkWeight[ $MechCount ] = 24 ;				// +
$MechDefaultWeapon[ $MechCount, 0 ] = $MechWeapon::PlasmaCannon ;	// +
$MechDefaultWeapon[ $MechCount, 1 ] = $MechWeapon::UltraAC10 ;		// + $MechWeapon::AC5 ;
$MechDefaultWeapon[ $MechCount, 2 ] = $MechWeapon::Autogun ;		// + $MechWeapon::AC5 ;
$MechDefaultWeapon[ $MechCount, 3 ] = $MechWeapon::Autogun ;		// + $MechWeapon::AC5 ;
$MechDefaultWeapon[ $MechCount, 4 ] = $MechWeapon::CometCannon ;	// +
$MechDefaultWeapon[ $MechCount, 5 ] = $MechWeapon::SRM4 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::JumpJets ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Shields ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Synomium ] = 1 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Coolant ] = 0 ;		// +
$MechDefaultArmor[ $MechCount ] = $MechArmor::Default ;			// +
$MechCount++;								// +
									// +
$MechName[ $MechCount ] = "Emancipator EAX100 Quaternary" ;		// +
$MechSubName[ $MechCount ] = "Iron" ;					// +
$MechStats[ $MechCount ] = "100T mech, anti-air loadout" ;		// +
$MechData[ $MechCount ] = "Emancipator" ;				// +
$MechWeightLimit[ $MechCount ] = 100 ;					// +
$MechHeatLimit[ $MechCount ] = 300 ;					// +
$MechMaxWeapons[ $MechCount ] = 6 ;					// +
$MechHardPointSlot[ $MechCount , 2 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 3 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 4 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 5 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 6 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 7 ] = $MechHardpointType::Omni ;	// +
$MechDefaultArmorWeight[ $MechCount ] = 31 ;				// + = 34 ;
$MechDefaultHeatSinkWeight[ $MechCount ] = 31 ;				// + = 34 ;
$MechDefaultWeapon[ $MechCount , 0 ] = $MechWeapon::LRM5 ;		// +
$MechDefaultWeapon[ $MechCount , 1 ] = $MechWeapon::SRM6 ;		// + = $MechWeapon::LRM5 ;
$MechDefaultWeapon[ $MechCount , 2 ] = $MechWeapon::Coilgun ;		// + = $MechWeapon::PPC ;
$MechDefaultWeapon[ $MechCount , 3 ] = $MechWeapon::PPC ;		// +
$MechDefaultWeapon[ $MechCount , 4 ] = $MechWeapon::LRM5 ;		// +
$MechDefaultWeapon[ $MechCount , 5 ] = $MechWeapon::Raptor ;		// + = $MechWeapon::LRM5 ;
$MechDefaultEquip[ $MechCount , $MechEquip::JumpJets ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Shields ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Synomium ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Coolant ] = 0 ;		// +
$MechDefaultArmor[ $MechCount ] = $MechArmor::Default ;			// +
$MechCount++;								// +
									// +
$MechName[ $MechCount ] = "Emancipator EAX100 Ultralight" ;		// +
$MechSubName[ $MechCount ] = "Iron" ;					// +
$MechStats[ $MechCount ] = "100T mech, half capacity loadout" ;		// +
$MechData[ $MechCount ] = "Emancipator" ;				// +
$MechWeightLimit[ $MechCount ] = 100 ;					// +
$MechHeatLimit[ $MechCount ] = 300 ;					// +
$MechMaxWeapons[ $MechCount ] = 6 ;					// +
$MechHardPointSlot[ $MechCount , 2 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 3 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 4 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 5 ] = $MechHardpointType::Standard ;	// +
$MechHardPointSlot[ $MechCount , 6 ] = $MechHardpointType::Omni ;	// +
$MechHardPointSlot[ $MechCount , 7 ] = $MechHardpointType::Omni ;	// +
$MechDefaultArmorWeight[ $MechCount ] = 10 ;				// +
$MechDefaultHeatSinkWeight[ $MechCount ] = 12 ;				// +
$MechDefaultWeapon[ $MechCount , 0 ] = $MechWeapon::Autogun ;		// + $MechWeapon::UltraAC10 ;
$MechDefaultWeapon[ $MechCount , 1 ] = $MechWeapon::Flamer ;		// +
$MechDefaultWeapon[ $MechCount , 2 ] = $MechWeapon::Autogun ;		// + $MechWeapon::SpikeCannon ;
$MechDefaultWeapon[ $MechCount , 3 ] = $MechWeapon::SpikeCannon ;	// +
$MechDefaultWeapon[ $MechCount , 4 ] = $MechWeapon::SRM4 ;		// +
$MechDefaultWeapon[ $MechCount , 5 ] = $MechWeapon::Autogun ;		// + $MechWeapon::None ;
$MechDefaultEquip[ $MechCount , $MechEquip::JumpJets ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Shields ] = 1 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Synomium ] = 0 ;		// +
$MechDefaultEquip[ $MechCount , $MechEquip::Coolant ] = 0 ;		// +
$MechDefaultArmor[ $MechCount ] = $MechArmor::Default ;			// +
$MechCount++;								// +[soph]


// Mech Hardpoint Location Defs
//----------------------------------------------------------------------------

$MechHardPointPos["Iron", 2] = "0 -0.25 0" ;			// "0 0 0"; -soph
$MechHardPointPos["Iron", 3] = "-3.5 -0.25 0" ;			// "-3.5 0 0"; -soph
$MechHardPointPos["Iron", 4] = "-0.75 -0.25 1.5" ;		// "0 0 1.5"; -soph
$MechHardPointPos["Iron", 5] = "-2.75 -0.25 1.5" ;		// "-3.5 0 1.5"; -soph
$MechHardPointPos["Iron", 6] = "0 -0.5 2.5" ;			// "0 -0.25 2.5"; -soph
$MechHardPointPos["Iron", 7] = "-3.5 -0.5 2.5" ;		// "-3.5 -0.25 2.5"; -soph

//----------------------------------------------------------------------------
if(isObject(EngineBase)) { EngineBase.DSOFileCount++; } else { quit(); }