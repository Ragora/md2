if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// MechRocketGun
//--------------------------------------

datablock AudioProfile(MechGunActivate)
{
   filename    = "fx/weapons/grenadelauncher_activate.wav";
   description = AudioDefault3d;
   preload = true;
};
datablock AudioProfile(MechRocketFireSound)
{
     filename = "fx/Bonuses/high-level4-blazing";
     description = AudioBIGExplosion3d;
   preload = true;
};
datablock AudioProfile(MechFire2Sound)
{
     filename = "fx/Bonuses/down_passback3_rocket.wav";
     description = AudioBIGExplosion3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MechRocketGunAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   pickUpName = "some mech rockets";
};

//--------------------------------------------------------------------------
// Weapon and item
//--------------------------------------

datablock ShapeBaseImageData(MechRocketGun2Image)
{
   className = WeaponImage;
   shapeFile = "turret_missile_large.dts";
   item = MechRocketGun;
   ammo = MechRocketGunAmmo;
   offset = "-1 0 0";
   emap = true;

   projectile = MechRocket;
   projectileType = LinearProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSound[1]                    = MechGunActivate;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "Deploy";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = MechRocketFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(MechRocketGunDImage)
{
   className = WeaponImage;
   shapeFile = "turret_missile_large.dts";
   item = MechRocketGun;
   ammo = MechRocketGunAmmo;
   offset = "-1.25 0 0";
   emap = true;

   projectile = MechRocket;
   projectileType = LinearProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSound[1]                    = MechGunActivate;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "Deploy";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = MechRocketFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(MechRocketGunImage) : MechRocketGun2Image
{
   offset = "0.1 0 0";
   stateSequence[2]                 = "Deploy";
   stateScript[3] = "onFire";
};

datablock ItemData(MechRocketGun)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_missile_large.dts";
   image = MechRocketGunImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   pickUpName = "a mech rocket launcher";
   emap = true;
};

function MechRocketGunImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);

   if(%obj.client.race $= Bioderm)
      %obj.mountImage(MechRocketGunDImage, 4);
   else
      %obj.mountImage(MechRocketGun2Image, 4);
}

function MechRocketGunImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
}

function MechRocketFire(%data, %obj, %slot)
{
   %weapon = %obj.getMountedImage(0).item;
   %ammo = %obj.getInventory(%data.ammo);
   
   if(%obj.client.mode[%weapon] == 0)
   {
      %ammoUse = 2;
   
      %p = new LinearProjectile()
      {
           dataBlock        = "MechRocket";
           initialDirection = %obj.getMuzzleVector(%slot);
           initialPosition  = %obj.getMuzzlePoint(%slot);
           sourceObject     = %obj;
           sourceSlot       = %slot;
      };

      MissionCleanup.add(%p);
      
      %obj.play3d(MechRocketFireSound);
      %obj.play3d(MechFire2Sound);
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
      %ammoUse = 2;	// = 4; -soph

      %muzzlePos = %obj.getMuzzlePoint( %slot ) ;	// +[soph]
      %muzzleVec = %obj.getMuzzleVector( %slot ) ;	// +
							// +
      %p = new SeekerProjectile()			// +
      {							// +
           dataBlock        = "MechHowitzerMissile" ;	// +
           initialDirection = %muzzleVec ;		// +
           initialPosition  = %muzzlePos ;		// +
           sourceObject     = %obj ;			// +
           sourceSlot       = %slot ;			// +
      } ;						// +
							// +
      %targetPos = VectorAdd( %muzzlePos , VectorScale( %muzzleVec , 2500 ) ) ;
      %p.setPositionTarget( %targetPos ) ;		// +
      %p.positionTarget = %targetPos ;			// +
      %p.originTime = getSimTime() ;			// +[/soph]

      MissionCleanup.add(%p);
							// +[soph]
      %obj.play3d(MILFireSound);
   }
   
   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
       %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
      %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   %obj.decInventory(%data.ammo, %ammoUse / 2);
}

function DiscFireTimeoutClear(%obj)  //-Nite-
{
   %obj.fireTimeoutDisc = 0;
}

function DiscImage::onFire(%data, %obj, %slot)
{
     if(%obj.fireTimeoutDisc)  //-nite-
      return;
//   %data.lightStart = getSimTime();

//   if( %obj.station $= "" && %obj.isCloaked() )
//   {
//      if( %obj.respawnCloakThread !$= "" )
//      {
//         Cancel(%obj.respawnCloakThread);
//         %obj.setCloaked( false );
//         %obj.respawnCloakThread = "";
//      }
//      else
//      {
//         if( %obj.getEnergyLevel() > 20 )
//         {
//            %obj.setCloaked( false );
//            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
//         }
//      }
//   }

        %vehicle = 0;
        %weapon = %obj.getMountedImage(0).item;

        if(%obj.client.mode[%weapon] == 0)
        {
             %projectile = "DiscProjectile";
             %mode = "LinearProjectile";
             %obj.fireTimeoutDisc = 1000;  //should be the same as 1.25  -Nite-
             %enUse = 0;
             %ammoUse = 1;
        }                       //1250
        else if(%obj.client.mode[%weapon] == 1)
        {
             %projectile = "TurboDisc";
             %mode = "LinearProjectile";
             %obj.fireTimeoutDisc = 1250;   //should be the same as .25  -Nite-
             %enUse = 7;
             %ammoUse = 1;
        }                          //230
        else if(%obj.client.mode[%weapon] == 2)
        {
             %projectile = "PowerDiscProjectile";
             %mode = "LinearProjectile";
             %enUse = 15;
             %ammoUse = 2;
             %obj.fireTimeoutDisc = 1500; //should be same as 2.25  -Nite-
        }                                //2250

        if(%obj.powerRecirculator)
          %enUse *= 0.75;
          
     if(%ammoUse > %obj.getInventory(%data.ammo) || %enUse > %obj.getEnergyLevel())
     {
          schedule(%obj.fireTimeoutDisc, 0, "DiscFireTimeoutClear", %obj);
          return;
     }

     %p = new(%mode)()
     {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);
   // all this for emulating state stuff -Nite-
   %obj.play3D(DiscFireSound);// play fire sounds -Nite-
   if( %obj.getState() !$= "Dead" )				// +soph
      %obj.setActionThread("light_recoil");  //Play anim recoil  -Nite-
   schedule(500, %obj.play3D(DiscReloadSound),%obj);//play reload sound 500ms after we fire  -Nite-
   schedule(%obj.fireTimeoutDisc, 0, "DiscFireTimeoutClear", %obj); //Use fire time out for each mode   -Nite-
  // serverCmdPlayAnim(%obj.client,"light_recoil"); //this worked too lol  -Nite-

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo, %ammoUse);
   %obj.useEnergy(%enUse);
   
   if(%obj.client.mode[%weapon] == 1)
      %obj.applyKick(-500);
      
   if(%obj.client.mode[%weapon] == 2)
      %obj.applyKick(-1000);
}

function MechRocketGunImage::onFire(%data, %obj, %slot)
{
     %weapon = %obj.getMountedImage(0).item;
     
    // if(%obj.client.mode[%weapon] == 0)	// -soph
          %ammoUse = 2;
    // else					// -soph
    //      %ammoUse = 4;			// -soph
          
     %ammo = %obj.getInventory(%data.ammo);
     
     if(%ammoUse > %ammo)
     {
          %obj.play3D(MortarDryFireSound);
          return;
     }

     %obj.setImageTrigger(4, true);

     MechRocketFire(%data, %obj, %slot);
     MechRocketFire(%data, %obj, 4);
     
     return %obj.setImageTrigger(4, false);
}

function MechRocketGun2Image::onFire(%data,%obj,%slot)
{
}
