if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Chaingun
//--------------------------------------
//Patch File Fixed Pulse Bolt DM
//moved sounds to blaster cus its loaded first Death~bot

////////////////////////////////////////////////////////////////////////////
//--------------------------------------
// Pulse Rifle Sounds
//--------------------------------------
////////////////////////////////////////////////////////////////////////////

datablock AudioProfile(GaussExpSound)
{
   filename    = "fx/explosions/explosion.xpl10.wav";
   description = AudioExplosion3d;
};

datablock AudioProfile(PulseExpSound)
{
   filename    = "fx/armor/heavy_RF_metal.WAV";
   description = AudioClosest3d;
   preload = true;
};
//--------------------------------------------------------------------------
// Shockwave
//--------------------------------------


datablock ShockwaveData(PulseShockwave)
{
   width = 1;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 4;
   acceleration = 5;
   lifetimeMS = 1000;
   height = 0.2;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;


//   texture[0] = "special/crescent4";
   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1;

   colors[0] = "0.0 1.0 1.0 1" ;	// "0.0 1.0 0.0 1"; -soph
   colors[1] = "0.0 0.4 1.0 0.5" ;	// "0.0 1.0 0.4 0.5"; -soph
   colors[2] = "1.0 1.0 1.0 0.0" ;	// "1.0 1 1 0.0"; -soph
};

//-------------------------------------------------------------
// Explosion---------------------------------------------------
//-------------------------------------------------------------

datablock ExplosionData(PulseExplosion)
{
//   explosionShape = "mortar_explosion.dts";
//   scale          = "0.01 0.01 0.01";
   soundProfile   = PulseExpSound;
   shockwave      = PulseShockwave;
   faceViewer     = true;

   sizes[0] = "0.1 0.1 0.1";
   sizes[1] = "0.1 0.1 0.1";
   times[0]      = 0.0;
   times[1]      = 1.0;

//   shakeCamera = true;
//   camShakeFreq = "10.0 6.0 9.0";
//   camShakeAmp = "15.0 15.0 15.0";
//   camShakeDuration = 1.5;
//   camShakeRadius = 10.0;
};

//-------------------------------------
// Trail
//-------------------------------------
datablock ParticleData(LinkTrail)
{
   dragCoeffiecient     = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

//   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.1 0.9 0.9 0.5" ;		// "0.1 0.9 0.1 0.5"; -soph
   colors[1]     = "0.05 0.6 0.6 0.2" ;		// "0.05 0.6 0.05 0.2"; -soph
   colors[2]     = "0.0 0.4 0.4 0.0" ;		// "0.0 0.4 0.0 0.0"; -soph
   colors[3]     = "0.1 0.9 0.9 0.0" ;		// "0.1 0.9 0.1 0.0"; -soph
   sizes[0]      = 1.0;
   sizes[1]      = 0.5;
   sizes[2]      = 0.3;
   sizes[3]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(LinkTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 0;


   ejectionVelocity = 10;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 40;
   overrideAdvance  = true;

//   lifetimeMS       = 500;
//   particleRadius = 10.25;
   particles = "LinkTrail";
};

datablock ParticleEmitterData(MechPulseEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;

   ejectionOffset = 0;


   ejectionVelocity = 10;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 40;
   overrideAdvance  = true;

   particles = "LinkTrail";
};

//--------------------------------------------------------------------------
// Pulse
//--------------------------------------

datablock LinearFlareProjectileData(PulseBolt) 
{
   //projectileShapeName = "energy_bolt.dts";
   scale               = "0.5 0.5 0.5";
   faceViewer          = true;
   hasDamageRadius     = true;
   damageRadius        = 5.0;
   directDamage        = 0.0;
   indirectDamage      = 0.08;
   radiusDamageType  = $DamageType::Pulse; // this was wrong indirectDamageType causes no death message errors. -Nite-
   kickBackStrength    = 0.0;



   baseEmitter[0]      = LinkTrailEmitter;
   explosion           = "PulseExplosion";
   splash              = discSplash;

   dryVelocity       = 300;
   wetVelocity       = 300;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   numFlares         = 10;
   flareColor        = "0.0 0.6 0.6 1.0" ;			// "0 0.6 0.0 1"; -soph
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound        = ChaingunProjectile ;				// PulseProjectileSound; -soph
   fireSound    = AAFireSound;
//   wetFireSound = PulseFireWetSound;

   hasLight    = false;
   lightRadius = 4.0;
   lightColor  = "0 0.6 0";
};

function PulseBolt::onExplode( %data , %proj , %pos , %mod ) 	// +[soph]
{								// + from gl logic
	if( %proj.bkSourceObject )				// +
		if( isObject(%proj.bkSourceObject ) )		// +
			%proj.sourceObject = %proj.bkSourceObject ;
	if( %proj.damageMod > 0 )				// +
		EMPBurstExplosion(%proj , %pos , %data.damageRadius , %data.indirectDamage * %proj.damageMod / 2 , 0 , %proj.sourceObject , $DamageType::EMP ) ;
        else							// +
		EMPBurstExplosion(%proj , %pos , %data.damageRadius , %data.indirectDamage / 2 , 0 , %proj.sourceObject , $DamageType::EMP ) ;
	Parent::onExplode( %data , %proj , %pos , %mod ) ;	// +
}								// +[/soph]

//--------------------------------------
// Protron
//--------------------------------------

datablock ShockwaveData(ProtronWave)
{
   width = 1;
   numSegments = 24;
   numVertSegments = 4;
   velocity = -10;
   acceleration = -5.0;
   lifetimeMS = 300;
   height = 0.1;
   verticalCurve = 0.65;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 1.0";
   colors[1] = "1.0 1.0 1.0 0.5";
   colors[2] = "1.0 1.0 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ParticleData(PProtronParticle)
{
   dragCoefficient      = 1;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0;
   constantAcceleration = 1;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 48;
   textureName          = "special/crescent3";
   colors[0] = "1.0 1.0 1.0 0.666";
   colors[1] = "1.0 1.0 1.0 0.25";
   colors[2] = "1.0 1.0 1.0 0.0";
   sizes[0]      = 0.6;
   sizes[1]      = 1.2;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PProtronEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 0.5;
   ejectionOffset   = 0.0;
   thetaMin         = 1;
   thetaMax         = 89;
   phiReferenceVel  = 0;
   phiVariance      = 360;
//   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 300;
   particles = "PProtronParticle";
};

datablock ParticleData(ProtronParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 2.0;
   inheritedVelFactor   = 0;
   constantAcceleration = 1;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 100;
   textureName          = "special/crescent4";
   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.3 0.6 1.0 1.0";
   colors[2] = "0.0 0.0 1.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ProtronEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 2.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 300;
   particles = "ProtronParticle";
};

datablock ExplosionData(ProtronExplosion)
{
   soundProfile   = blasterExpSound;
   shockwave = ProtronWave;
   emitter[0] = PProtronEmitter;
};

datablock LinearFlareProjectileData(ProtronBoltR)
{
   scale = "0.4 0.4 0.4";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.08;
   damageRadius        = 4.0;
   kickBackStrength    = 0.0;
   radiusDamageType    = $DamageType::Protron;

   explosion           = "ProtronExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 325;    //225
   wetVelocity       = 300;   //200
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "1.0 0.2 0.2";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 0.1;
   size[2]           = 0.05;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1.0 0.2 0.2";
};

datablock LinearFlareProjectileData(ProtronBoltG) : ProtronBoltR
{
   flareColor        = "0.2 1.0 0.2";
   lightColor  = "0.2 1.0 0.2";
};

datablock LinearFlareProjectileData(ProtronBoltB) : ProtronBoltR
{
   flareColor        = "0.2 0.2 1";
   lightColor  = "0.2 0.2 1";
};


datablock LinearFlareProjectileData(ProtronBoltW) : ProtronBoltR
{
   flareColor        = "1 1 1";
   lightColor  = "1 1 1";
};

datablock LinearFlareProjectileData(ProtronBoltY) : ProtronBoltR
{
   flareColor        = "1 1 0";
   lightColor  = "1 1 0";
};

datablock LinearFlareProjectileData(ProtronBoltC) : ProtronBoltR
{
   flareColor        = "0 1 1";
   lightColor  = "0 1 1";
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------

datablock ParticleData( ChaingunSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -1.4;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.05;
   sizes[1]      = 0.2;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( ChaingunSplashEmitter )
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "ChaingunSplashParticle";
};


datablock SplashData(ChaingunSplash)
{
   numSegments = 10;
   ejectionFreq = 10;
   ejectionAngle = 20;
   ringLifetime = 0.4;
   lifetimeMS = 400;
   velocity = 3.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = ChaingunSplashEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Particle Effects
//--------------------------------------
datablock ParticleData(ChaingunFireParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 550;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 1.0";
   colors[1]     = "0.46 0.36 0.26 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.20;
};

datablock ParticleEmitterData(ChaingunFireEmitter)
{
   ejectionPeriodMS = 6;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "ChaingunFireParticle";
};

//--------------------------------------------------------------------------
// Explosions
//--------------------------------------
datablock ParticleData(ChaingunExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.0625;
   sizes[1]      = 0.2;
};

datablock ParticleEmitterData(ChaingunExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 0.75;
   velocityVariance = 0.25;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "ChaingunExplosionParticle1";
};

datablock ParticleData(ChaingunImpactSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.7 0.7 0.7 0.4";
   colors[2]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ChaingunImpactSmoke)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;
   velocityVariance = 0.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "ChaingunImpactSmokeParticle";
   lifetimeMS       = 50;
};

datablock ParticleData(VulcanImpactSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 400;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.7 0.7 0.7 0.4";
   colors[2]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(VulcanImpactSmoke)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;
   velocityVariance = 0.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "VulcanImpactSmokeParticle";
   lifetimeMS       = 100;
};

datablock ParticleData(ChaingunFireSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.3;
   inheritedVelFactor   = 0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 250;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.7 0.7 0.7 0.4";
   colors[2]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.1;
   sizes[1]      = 0.125;
   sizes[2]      = 0.15;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ChaingunFireSmoke)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 1;
   ejectionVelocity = 0.5;
   velocityVariance = 0.1;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 9;
   overrideAdvances = false;
   particles = "ChaingunFireSmokeParticle";
//   lifetimeMS       = 50;
};

datablock ParticleData(ChaingunSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.2;
   sizes[2]      = 0.05;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(ChaingunSparkEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 4;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "ChaingunSparks";
};

datablock ParticleData(VulcanSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 1.2;
   sizes[1]      = 0.5;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(VulcanSparkEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 8;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "VulcanSparks";
};

datablock ExplosionData(ChaingunExplosion)
{
//   soundProfile   = ChaingunImpact;

   emitter[0] = ChaingunImpactSmoke;
   emitter[1] = ChaingunSparkEmitter;

   faceViewer           = false;
};

datablock ExplosionData(VulcanExplosion)
{
//   soundProfile   = ChaingunImpact;

   emitter[0] = VulcanImpactSmoke;
   emitter[1] = VulcanSparkEmitter;

   faceViewer           = false;
};

datablock ParticleData(GaussSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.3;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 100;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 0.75;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(GaussSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 1;
   ejectionVelocity = 16;
   velocityVariance = 6.75;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 150;
   particles = "GaussSparks";
};

datablock ExplosionData(GaussCoreExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 150;

   offset = 0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(GaussExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = GaussExpSound;

   emitter[0] = ChaingunImpactSmoke;
   emitter[1] = GaussSparksEmitter;

   subExplosion[0] = GaussCoreExplosion;
      
   shakeCamera = true;
   camShakeFreq = "5.0 5.0 5.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.5;
   camShakeRadius = 5.0;
   
   faceViewer           = true;
};

datablock AudioProfile(ACExpSound)
{
   filename    = "fx/explosions/explosion.xpl10.wav";
   description = AudioExplosion3d;
};

datablock ExplosionData(ACExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = ACExpSound;

   emitter[0] = ChaingunImpactSmoke;
   emitter[1] = GaussSparksEmitter;

   subExplosion[0] = GaussCoreExplosion;

   shakeCamera = true;
   camShakeFreq = "5.0 5.0 5.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.5;
   camShakeRadius = 5.0;

   faceViewer           = true;
};

datablock ShockwaveData(ScoutChaingunHit)
{
   width = 0.5;
   numSegments = 13;
   numVertSegments = 1;
   velocity = 0.5;
   acceleration = 2.0;
   lifetimeMS = 900;
   height = 0.1;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = false;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 0.5";
   colors[2] = "0.0 0.0 1.0 0.0";
};

datablock ParticleData(ScoutChaingunExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 1.0";
   colors[2] = "0.0 0.0 1.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ScoutChaingunExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 80;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ScoutChaingunExplosionParticle1";
};

datablock ExplosionData(ScoutChaingunExplosion)
{
   soundProfile = ShrikeBlasterProjectileHitSound;
   shockwave = ScoutChaingunHit;
   emitter[0] = ScoutChaingunExplosionEmitter;
};

//--------------------------------------------------------------------------
// Particle effects
//--------------------------------------


datablock DebrisData( ShellDebris )
{
   shapeName = "weapon_chaingun_ammocasing.dts";

   lifetime = 6.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};             

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock DecalData(ChaingunDecal1)
{
   sizeX       = 0.05;
   sizeY       = 0.05;
   textureName = "special/bullethole1";
};
datablock DecalData(ChaingunDecal2) : ChaingunDecal1
{
   textureName = "special/bullethole2";
};

datablock DecalData(ChaingunDecal3) : ChaingunDecal1
{
   textureName = "special/bullethole3";
};
datablock DecalData(ChaingunDecal4) : ChaingunDecal1
{
   textureName = "special/bullethole4";
};
datablock DecalData(ChaingunDecal5) : ChaingunDecal1
{
   textureName = "special/bullethole5";
};
datablock DecalData(ChaingunDecal6) : ChaingunDecal1
{
   textureName = "special/bullethole6";
};

datablock TracerProjectileData(ChaingunBullet)
{
//   doDynamicClientHits = true;
   directDamage        = 0.0825;
   directDamageType    = $DamageType::Bullet;
   kickBackStrength    = 50;

   explosion           = "ChaingunExplosion";
   underwaterExplosion = "ChaingunUnderwaterExplosion";
   splash              = ChaingunSplash;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 500.0;
   wetVelocity       = 425.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.10;
   crossSize       = 0.20;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

function ChaingunBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
  if(detectIsWet(%position))
     serverPlay3d(getRandomChaingunWetSound(), %position);
   else
      serverPlay3d(getRandomChaingunSound(), %position);

  if(%projectile.poisonChance)
     PoisonObject( %targetObject , %projectile.sourceObject , %projectile.getDataBlock().directDamage ) ;	// +soph

  Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

datablock ParticleData(GaussBulletParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.75";
   colors[1]     = "0.2 0.1 0 0";
   sizes[0]      = 0.5;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(GaussBulletEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 1;
   ejectionVelocity = 20;
   velocityVariance = 0;
   ejectionOffset   = 0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "GaussBulletParticle";
};

//--------------------------------------------------------------------------
// Scout Projectile
//--------------------------------------
datablock TracerProjectileData(ScoutChaingunBullet)
{
   doDynamicClientHits = true;

   directDamage        = 0.15;	// 0.3 -soph
   explosion           = "ScoutChaingunExplosion";
   splash              = ChaingunSplash;
   hasDamageRadius     = true;
   indirectDamage      = 0.1;	// 0.01 -soph
   damageRadius        = 0.5;	// 3.0; -soph
   radiusDamageType    = $DamageType::ShrikeBlaster;
   directDamageType    = $DamageType::ShrikeBlaster;
   kickBackStrength  = 0.0;

   sound = ShrikeBlasterProjectileSound;

   dryVelocity       = 600.0;
   wetVelocity       = 600.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 45.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = "1.0 1.0 1.0 1.0";
	tracerTex[0]  	 = "special/shrikeBolt";
	tracerTex[1]  	 = "special/shrikeBoltCross";
	tracerWidth     = 0.55;
   crossSize       = 0.99;
   crossViewAng    = 0.990;
   renderCross     = true;

};

function ScoutChaingunBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   Parent::onCollision( %data , %projectile , %targetObject , %modifier , %position , %normal );
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(ChaingunAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some chaingun ammo";

   computeCRC = true;

};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(ChaingunImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item      = Chaingun;
   ammo 	 = ChaingunAmmo;
   emap = true;

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 3.0;

   projectileSpread = 8.0 / 1000.0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = ChaingunSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = ChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.05;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "FireSpindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   stateEmitter[6]       = "ChaingunFireSmoke";
   stateEmitterTime[6]       = 0.25;
   stateEmitterNode[6]       = 0;

   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";

   //--------------------------------------
   stateName[8]       = "FireSpindown";
   stateSound[8]      = ChaingunSpinDownSound;
   stateSpinThread[8] = SpinDown;
   stateEmitter[8]       = "ChaingunFireSmoke";
   stateEmitterTime[8]       = 0.25;
   stateEmitterNode[8]       = 0;
   //
   stateTimeoutValue[8]            = 1.0;
   stateWaitForTimeout[8]          = true;
   stateTransitionOnTimeout[8]     = "Ready";
   stateTransitionOnTriggerDown[8] = "Spinup";
};

datablock ItemData(Chaingun)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_chaingun.dts";
   image        = ChaingunImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   pickUpName   = "a chaingun";

   computeCRC = true;
   emap = true;
};

function ChaingunImage::onFire(%data, %obj, %slot)
{
   %vehicle = 0;
   %weapon = %obj.getMountedImage(0).item;

   if(%obj.client.mode[%weapon] == 1)	// all in one now -soph
   {
      %projectile = "PoisonChaingunBullet";
      %ammoUse = 3;
   }
   else
   {
      %projectile = "";
      %ammoUse = 1;
   }

   if(%ammoUse > %obj.getInventory(%data.ammo))
      return;
   else
   {
      %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 9);

      %p = new TracerProjectile() {
         dataBlock        = "ChaingunBullet";
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         poisonChance     = %obj.client.mode[%weapon] == 1;
      };
      MissionCleanup.add(%p);
   }
   %obj.lastProjectile = %p;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo, %ammoUse);
}