if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Extended Grenades
// ------------------------------------------------------------------------

datablock ParticleData(PoisonExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.30;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.3 0.7 0.7 1.0";
   colors[1]     = "0.2 0.8 0.8 0.75";
   colors[2]     = "0.1 0.3 0.3 0.5";
   colors[3]     = "0.9 0.1 0.1 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 10.0;
   sizes[3]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(PoisonExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 500;

   particles = "PoisonExplosionSmoke";

};

datablock ExplosionData(PoisonExplosion)
{
   soundProfile   = MortarExplosionSound;

   particleEmitter = PoisonExplosionSmokeEmitter ;	// +[soph]
   particleDensity = 75 ;				// +
   particleRadius = 4 ;					// +[/soph]

   emitter[0] = PoisonExplosionSmokeEmitter;
//   emitter[ 1 ] = HandGrenadeExplosionSmokeEmitter ;	// +soph
};

datablock ItemData(PoisonGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = PoisonExplosion;
   indirectDamage      = 0.39 ;				// = 0.001; -soph
   damageRadius        = 14 ;				// = 10; -soph
   radiusDamageType    = $DamageType::Poison;
   kickBackStrength    = 100;
};

datablock ItemData(PoisonGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = PoisonGrenadeThrown;
	pickUpName = "some poison gas grenades";
	isGrenade = true;
};
