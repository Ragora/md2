if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// RAXX Flamethrower
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------

//datablock AudioProfile(RAXXFireSound)	// -[soph]
//{					// redundant with ScoutFlyerThrustSound block, removed
//   filename    = "fx/vehicles/shrike_boost.wav";
//   description = AudioDefaultLooping3d;
//   preload = true;
//};

datablock ShapeBaseImageData(RAXXDecalImage)
{
   mountPoint = 1;
   shapeFile = "pack_deploy_turreti.dts";
   offset = "0.16 0 0";
   rotation = "0 1 0 180";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearProjectileData(RAXXFlame)
{
   projectileShapeName = "plasmabolt.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.1;
   damageRadius        = 6;
   radiusDamageType    = $DamageType::RAXX;
   kickBackStrength    = 1;
   baseEmitter         = SmallFlameEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter ;		// MortarBubbleEmitter; -soph

   sound 				= GrenadeProjectileSound;
   explosion           = "" ;				// "PlasmaBoltExplosion"; -soph
   splash              = PlayerSplash;

   dryVelocity       = 80;
   wetVelocity       = 10 ; 				// = -1; -soph
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 520;
   lifetimeMS        = 520;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0 ;			// = 15.0; -soph
   explodeOnWaterImpact      = false ;			// = true; -soph
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 520 ;			// = 32; -soph

    activateDelayMS = -1;//1; --- Turn off... no activate/maintain sequence on plasmabolt.dts (ST)

   hasLight    = true;
   lightRadius = 6;
   lightColor  = "1 0.8 0.01";
};


function RAXXFlame::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{							// +
	Parent::onExplode( %data , %proj , %pos , %mod ) ;
	IncendiaryExplosion( %pos , %data.damageradius , 0.4 , %proj.sourceObject ) ;
	%smokePos = vectorSub( %pos , %proj.initialPosition ) ;
	%smokePos = vectorNormalize( %smokePos ) ;	// +
	%smokePos = vectorAdd( %pos , %smokePos ) ;	// +
	%smokePos = vectorAdd( %smokePos , ( ( getRandom() - 0.5 ) / 2 ) SPC 
					   ( ( getRandom() - 0.5 ) / 2 ) SPC 
					   ( ( getRandom() - 0.5 ) / 2 ) ) ;
	createLifeEmitter( %smokePos , "HeavyDamageSmoke" , 1 ) ;
}							// +[/soph]

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(RAXXAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a butane canister";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(RAXXImage)
{
   className = WeaponImage;
   shapeFile = "turret_tank_barrelchain.dts";
   item = RAXX;
//   usesEnergy = true;
//   minEnergy = 6;
//   fireEnergy = 2;
   ammo = RAXXAmmo;

   projectile = RAXXFlame;
   projectileType = LinearProjectile;
   turboPlasmaEnabled = true;
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activation";
   stateSound[0] = PlasmaSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready"; 
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "CheckWet";	// "Fire"; underwater flamer fix -soph
   stateTransitionOnNoAmmo[3] = "NoAmmo";
   stateTransitionOnTriggerUp[3] = "Reload";
   stateTimeoutValue[3] = 0.0333333 ;		// 0.0375; -soph
   stateFire[3] = true;
//   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitter[3]       = "SmallFlameEmitter";
   stateEmitterTime[3]       = 0.2;
   stateEmitterNode[3]       = 0;
   stateSequence[3] = "Fire_Vis";
   stateSound[3] = ScoutFlyerThrustSound ;	// RAXXFireSound; redundant block -soph

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.5;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";

   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire";
};

datablock ItemData(RAXX)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_disc.dts";
   image = RAXXImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a RAXX flamethrower";
};

datablock ShapeBaseImageData(RAXXDecalAImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0 0.9 0";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(RAXXDecalBImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0 0.35 0";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(RAXXDecalCImage)
{
   shapeFile = "repair_patch.dts";
   offset = "0 0.6 0";
   rotation = "1 0 0 90";
};

function RAXXImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(RAXXDecalAImage, 4);
   %obj.mountImage(RAXXDecalBImage, 5);
   %obj.mountImage(RAXXDecalCImage, 6);
}

function RAXXImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}
