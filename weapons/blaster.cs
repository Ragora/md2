if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();
//-Nite-1501 made into a shotty type weapon
//Patch file fixed blue blaster DMtype was vbomb?
//Tweaked speed for blaster projectiles blue and reg, from 90 to 100,
//add shotty type fire for reg and blue bolts(3);]T:V
//oO-------------------------------------------------------Oo
// chaingun stuffs
//oO-------------------------------------------------------Oo
datablock AudioProfile(ChaingunSwitchSound)
{
   filename    = "fx/weapons/chaingun_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunFireSound)
{
   filename    = "fx/weapons/chaingun_fire.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(FChaingunFireSound)
{
   filename    = "fx/weapons/chaingun_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ChaingunProjectile)
{
   filename    = "fx/weapons/chaingun_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact)
{
   filename    = "fx/weapons/chaingun_impact.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact1)
{
   filename    = "fx/weapons/cg_hard1.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact2)
{
   filename    = "fx/weapons/cg_hard2.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact3)
{
   filename    = "fx/weapons/cg_metal1.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact4)
{
   filename    = "fx/weapons/cg_metal2.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact5)
{
   filename    = "fx/weapons/cg_metal3.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact6)
{
   filename    = "fx/weapons/cg_metal4.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunWaterImpact)
{
   filename    = "fx/weapons/cg_water1.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunWaterImpact2)
{
   filename    = "fx/weapons/cg_water2.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunWaterImpact3)
{
   filename    = "fx/weapons/cg_water3.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunSpinDownSound)
{
   filename    = "fx/weapons/chaingun_spindown.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunSpinUpSound)
{
   filename    = "fx/weapons/chaingun_spinup.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterProjectileSound)
{
   filename    = "fx/vehicles/shrike_blaster_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(ShrikeBlasterProjectileHitSound)
{
   filename    = "fx/vehicles/shrike_blaster_projectile_impact.wav";
   description = AudioClose3d;
   preload = true;
};
////////////////////////////////////////////////////////////////////////////
// blaster stuff
////////////////////////////////////////////////////////////////////////////
//--------------------------------------
// Default blaster
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(BlasterSwitchSound)
{
   filename    = "fx/weapons/blaster_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(BlasterFireSound)
{
   filename    = "fx/weapons/blaster_fire.WAV";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(BlasterProjectileSound)
{
   filename    = "fx/weapons/blaster_projectile.WAV";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(blasterExpSound)
{
   filename    = "fx/weapons/blaster_impact.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(BlasterDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------

datablock ParticleData( BlasterSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -1.4;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.05;
   sizes[1]      = 0.2;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( BlasterSplashEmitter )
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 4;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "BlasterSplashParticle";
};

datablock SplashData(BlasterSplash)
{
   numSegments = 15;
   ejectionFreq = 15;
   ejectionAngle = 40;
   ringLifetime = 0.35;
   lifetimeMS = 300;
   velocity = 3.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = BlasterSplashEmitter;

   colors[0] = "0.7 0.8 1.0 1.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(BlasterExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   colors[0] = "1.0 0.8 0.2 1.0";
   colors[1] = "1.0 0.4 0.2 1.0";
   colors[2] = "1.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BlasterExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 70;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "BlasterExplosionParticle1";
};

datablock ParticleData(BlasterExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   colors[0] = "1.0 0.2 0.2 1.0";
   colors[1] = "1.0 0.2 0.2 0.5";
   colors[2] = "1.0 0.0 0.0 0.0";
   sizes[0]      = 0.3;
   sizes[1]      = 0.90;
   sizes[2]      = 1.50;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BlasterExplosionEmitter2)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "BlasterExplosionParticle2";
};

datablock ExplosionData(BlasterExplosion)
{
   soundProfile   = blasterExpSound;
   emitter[0]     = BlasterExplosionEmitter;
   emitter[1]     = BlasterExplosionEmitter2;
};

datablock ExplosionData(noneExplosion)
{
   soundProfile   = "";
//   emitter[0]     = BlasterExplosionEmitter;
//   emitter[1]     = BlasterExplosionEmitter2;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock EnergyProjectileData(EnergyBolt)
{
   emitterDelay        = -1;
   directDamage        = 0.075;     //  .15
   directDamageType    = $DamageType::Blaster;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 0.5;

   explosion           = "BlasterExplosion";
   splash              = BlasterSplash;

   ignoreReflection = true;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 500;

   muzzleVelocity    = 100.0;  //   Nite 90

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = 200.0;
   wetVelocity       = 150.0;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.5 0.175 0.175";

   scale = "0.25 20.0 1.0";
   crossViewAng = 0.99;
   crossSize = 0.55;

   lifetimeMS     = 3000;
   blurLifetime   = 0.2;
   blurWidth      = 0.25;
   blurColor = "0.4 0.0 0.0 1.0";

   texture[0] = "special/blasterBolt";
   texture[1] = "special/blasterBoltCross";
};

datablock EnergyProjectileData(TurboBlasterBolt)
{
   emitterDelay        = -1;
   directDamage        = 0.15;	// 0.12 -soph     //  .15
   directDamageType    = $DamageType::Blaster;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 0.5;

   explosion           = "BlasterExplosion";
   splash              = BlasterSplash;

   ignoreReflection = true;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 500;

   muzzleVelocity    = 350.0;  //   Nite 90

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = 350.0;
   wetVelocity       = 350.0;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.5 0.175 0.175";

   scale = "0.25 20.0 1.0";
   crossViewAng = 0.99;
   crossSize = 0.55;

   lifetimeMS     = 3000;
   blurLifetime   = 0.2;
   blurWidth      = 0.25;
   blurColor = "0.4 0.0 0.0 1.0";

   texture[0] = "special/blasterBolt";
   texture[1] = "special/blasterBoltCross";
};

datablock ParticleData(BlueBlasterExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   colors[0] = "0.2 0.8 1.0 1.0";
   colors[1] = "0.2 0.4 1.0 1.0";
   colors[2] = "0.0 0.0 1.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BlueBlasterExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 70;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "BlueBlasterExplosionParticle1";
};

datablock ParticleData(BlueBlasterExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   colors[0] = "0.2 0.2 1.0 1.0";
   colors[1] = "0.2 0.2 1.0 0.5";
   colors[2] = "0.0 0.0 1.0 0.0";
   sizes[0]      = 0.3;
   sizes[1]      = 0.90;
   sizes[2]      = 1.50;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(BlueBlasterExplosionEmitter2)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "BlueBlasterExplosionParticle2";
};

datablock ExplosionData(BlueBlasterExplosion)
{
   soundProfile   = blasterExpSound;
   emitter[0]     = BlueBlasterExplosionEmitter;
   emitter[1]     = BlueBlasterExplosionEmitter2;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock EnergyProjectileData(BlueEnergyBolt)
{
   emitterDelay        = -1;
   directDamage        = 0.075;      //0.15
   directDamageType    = $DamageType::BlueBlaster;
   kickBackStrength    = 0.0;
   bubbleEmitTime      = 1.0;

   sound = BlasterProjectileSound;
   velInheritFactor    = 0.5;

   explosion           = "BlueBlasterExplosion";
   splash              = BlasterSplash;


   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 500;

   muzzleVelocity    = 100.0;    //Nite 90

   drag = 0.05;

   gravityMod        = 0.0;

   dryVelocity       = 200.0;
   wetVelocity       = 150.0;

   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.175 0.175 0.5";

   scale = "0.25 20.0 1.0";
   crossViewAng = 0.99;
   crossSize = 0.55;

   lifetimeMS     = 3000;
   blurLifetime   = 0.2;
   blurWidth      = 0.25;
   blurColor = "0.0 0.0 0.4 1.0";

   texture[0] = "special/shrikeBolt";
   texture[1] = "special/shrikeBoltCross";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(BlasterImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   item = Blaster;
//   projectile = EnergyBolt;
//   projectileType = EnergyProjectile;

   usesEnergy = true;
   fireEnergy = 2.5;
   minEnergy = 2.5;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.3; //0.5
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.2; //0.3
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
//   stateSound[3] = BlasterFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

function BlasterImage::onFire(%data, %obj, %slot)
{
   %vehicle = 0;
   %weapon = %obj.getMountedImage(0).item;
   
   if(%obj.client.mode[%weapon] == 0)
   {
       %projectile = "EnergyBolt";
       %enUse = %data.fireEnergy;
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
       %projectile = "BlueEnergyBolt";
       %enUse = %data.fireEnergy;
   }
   else if(%obj.client.mode[%weapon] == 2)
   {
       %projectile = "FissionBolt";
       %enUse = %data.fireEnergy;
   }

   %energy = %obj.getEnergyLevel();

   if(%obj.powerRecirculator)
      %enUse *= 0.75; 

   if(%energy < %enUse)
   {
       %obj.play3D(BlasterDryFireSound);
       return;
   }

  if(%obj.client.mode[%weapon] == 2)
  {
      %p = new LinearFlareProjectile()
     {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };
  }
  else
  {
      for(%i = 0; %i < 3; %i++)
      {
          %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 5);
          
          %p = new EnergyProjectile()
          {
              dataBlock        = %projectile;
              initialDirection = %vector;//%obj.getMuzzleVector(%slot);
              initialPosition  = %obj.getMuzzlePoint(%slot);
              sourceObject     = %obj;
              sourceSlot       = %slot;
              vehicleObject    = %vehicle;
          };
      }
  }

   %obj.play3D(BlasterFireSound);

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
    // else
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.setEnergyLevel(%energy - %enUse);
   return %p;
}

datablock ItemData(Blaster)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_energy.dts";
   image = BlasterImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a blaster";
};

///------------------------------------------\\\
///-------------mitzi Rumble-----------------\\\
datablock ParticleData(RBExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.15 0.2 1.0 1.0";
   colors[1]     = "0.6 0.1 1.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(RBExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "RBExplosionParticle";
};

datablock ParticleData(RBTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.15 0.2 1.0 1.0";
   colors[1]     = "0.6 0.1 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(RBTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "RBTrailParticle";
};

datablock ExplosionData(RumbleBlastExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = plasmaExpSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = RBExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock LinearFlareProjectileData(MitziRumbleBlast)
{
   scale               = "4.5 4.5 4.5";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 5.0;
   damageRadius        = 6.0;
   kickBackStrength    = 2500;
   radiusDamageType    = $DamageType::MitziTransparent;

   explosion           = "RumbleBlastExplosion";
   underwaterExplosion = "RumbleBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = RBTrailEmitter;

   dryVelocity       = 175.0;
   wetVelocity       = 175.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "0.15 0.2 1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   ignoreReflection = true;

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0.15 0.2 1";
};
