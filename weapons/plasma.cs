if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Plasma rifle
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(PlasmaSwitchSound)
{
   filename    = "fx/weapons/plasma_rifle_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(PlasmaFireSound)
{
   filename    = "fx/weapons/plasma_rifle_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PlasmaIdleSound)
{
   filename    = "fx/weapons/plasma_rifle_idle.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock AudioProfile(PlasmaReloadSound)
{
   filename    = "fx/weapons/plasma_rifle_reload.wav";
   description = Audioclosest3d;
   preload = true;
};

datablock AudioProfile(plasmaExpSound)
{
   filename    = "fx/explosions/explosion.xpl10.wav";
   description = AudioExplosion3d;
};

datablock AudioProfile(PlasmaProjectileSound)
{
   filename    = "fx/weapons/plasma_rifle_projectile.WAV";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(PlasmaDryFireSound)
{
   filename    = "fx/weapons/plasma_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(PlasmaFireWetSound)
{
   filename    = "fx/weapons/plasma_fizzle.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(HeavyPlasmaExpSound)
{
   filename    = "fx/bonuses/upward_passback1_bomb.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(PlasmaExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(PlasmaExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "PlasmaExplosionParticle";
};

datablock ParticleData(PlasmaExplosionAParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.467742;
    lifetimeMS = 2338;
    lifetimeVarianceMS = 0;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/expFlare.PNG";
    times[0] = 0;
    times[1] = 0.306452;
    times[2] = 1;
    colors[0] = "1.000000 0.320000 0.000000 1.000000";
    colors[1] = "0.200000 0.200000 0.200000 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 4.62903;
    sizes[1] = 4.00806;
    sizes[2] = 6.26613;
};

datablock ParticleEmitterData(PlasmaExplosionAEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 0;
    ejectionVelocity = 10;
    velocityVariance = 1;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 300;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "PlasmaExplosionAParticle";
};

datablock ExplosionData(PlasmaBoltExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 1.25;
   faceViewer = true;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.5;
};

datablock ShockwaveData(SuperPlasmaShockwave2D)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = 25.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.7 0.8 0.1 0.50";
   colors[1] = "0.8 0.7 0.2 0.25";
   colors[2] = "0.9 0.9 0.9 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};
//-Nite- must stay for PAC now..
datablock ExplosionData(SuperPlasmaExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;
//   particleEmitter = PlasmaExplosionEmitter;
//   particleDensity = 75;
//   particleRadius = 0.75;
   faceViewer = true;
   emitter[0] = "PlasmaExplosionAEmitter";

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MiniPlasmaExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 100;			// = 300; -soph
   particleRadius = 1.0;			// = 2.5; -soph
   faceViewer = true;

//   shockwave = SuperPlasmaShockwave2D;	// -soph

   sizes[0] = "0.1 0.1 0.1";			// "2.0 2.0 2.0"; -soph
   sizes[1] = "0.1 0.1 0.1";			// "2.0 2.0 2.0"; -soph
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ParticleData(IncendiaryDebrisSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.01;

   lifetimeMS           = 2250;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "flarebase";

   useInvAlpha =     false;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "1.0 0.775 0.3 1.0";
   colors[1]     = "0.4 0.2 0.05 0.75";
   colors[2]     = "0.3 0.3 0.3 0.5";
   colors[3]     = "0.05 0.05 0.01 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.625;
   sizes[2]      = 0.7;
   sizes[3]      = 0.49;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.7;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(IncendiaryDebrisSmokeEmitter)
{
   ejectionPeriodMS = 16;
   periodVarianceMS = 6;

   ejectionVelocity = 0.5;  // A little oomph at the back end
   velocityVariance = 0.1;

   thetaMin         = 1.0;
   thetaMax         = 8.0;

   particles = "IncendiaryDebrisSmokeParticle";
};

datablock ParticleData(IncendiaryExplosionSmoke)
{
   dragCoeffiecient     = 0.5;
   gravityCoefficient   = 0;   // rises slowly
   inheritedVelFactor   = 0;
   constantAcceleration = -0.35;

   lifetimeMS           = 1750;
   lifetimeVarianceMS   = 350;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

//   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 0.3 0.0";
   colors[1]     = "0.95 0.3 0.2 1.0";
   colors[2]     = "0.5 0.2 0.1 0.75";
   colors[3]     = "0.2 0.15 0.2 0.0";
   colors[4]     = "0.1 0.1 0.1 0.0";
   colors[5]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 5.5;
   sizes[2]      = 6.1;
   sizes[3]      = 8.5;
   sizes[4]      = 10.25;
   sizes[5]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 0.35547;
   times[3]      = 0.5235;
   times[4]      = 0.83547;
   times[5]      = 1.0;
};

datablock ParticleEmitterData(IncendiaryExplosionSmokeEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 4;

   ejectionOffset = 1.0;

   ejectionVelocity = 7;
   velocityVariance = 2;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 1000;

   particles = "IncendiaryExplosionSmoke";
};

datablock DebrisData(IncendiaryExplosionDebris)
{
   emitters[0] = IncendiaryDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 5.0;
   lifetimeVariance = 2.0;

  numBounces = 3;//1; --- variance can't be > num (ST)
   bounceVariance = 2;//3; --- 2/2 = range of 0-4
};

datablock ExplosionData(IncendiaryExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = HeavyPlasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.25;
   faceViewer = true;

   sizes[0] = "1.75 1.75 1.75";
   sizes[1] = "1.75 1.75 1.75";
   times[0] = 0.0;
   times[1] = 1.0;

   debris = IncendiaryExplosionDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 45;
   debrisNum = 6;
   debrisNumVariance = 2;
   debrisVelocity = 15.0;
   debrisVelocityVariance = 5.0;

   emitter[0] = IncendiaryExplosionSmokeEmitter;
};

datablock ExplosionData(HeavyPlasmaExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = HeavyPlasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.25;
   faceViewer = true;
//   emitter[0] = "PlasmaExplosionAEmitter";

   sizes[0] = "2.0 2.0 2.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ParticleData(FireboltFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -180.0;
   spinRandomMax = 180.0;

   animateTexture = true;
   framesPerSec = 15;

   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 4.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltFireEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 2;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "FireboltFireParticle";
};

datablock ParticleData(FireboltSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 3;
   sizes[2]      = 5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltSmokeEmitter)
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = false;

   particles = "FireboltSmokeParticle";
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData(PlasmaMist)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlasmaMistEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   lifetimeMS       = 250;
   particles = "PlasmaMist";
};

datablock ParticleData( PlasmaSplashParticle2 )
{

   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.03;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 600;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;


   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlasmaSplashEmitter2 )
{
   ejectionPeriodMS = 25;
   ejectionOffset = 0.2;
   periodVarianceMS = 0;
   ejectionVelocity = 2.25;
   velocityVariance = 0.50;
   thetaMin         = 0.0;
   thetaMax         = 30.0;
   lifetimeMS       = 250;

   particles = "PlasmaSplashParticle2";
};


datablock ParticleData( PlasmaSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlasmaSplashEmitter )
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "PlasmaSplashParticle";
};


datablock SplashData(PlasmaSplash)
{
   numSegments = 15;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = PlasmaSplashEmitter;
   emitter[1] = PlasmaSplashEmitter2;
   emitter[2] = PlasmaMistEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock ParticleData(PlasmaRifleParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = -0.9;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 550;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 1.0";
   colors[1]     = "0.46 0.36 0.26 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.20;
};

datablock ParticleEmitterData(PlasmaRifleEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 8.25;
   velocityVariance = 2;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "PlasmaRifleParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(PlasmaBolt)
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "2.0 2.0 2.0";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.45;
   damageRadius        = 4.0;
   kickBackStrength    = 0.0;
   radiusDamageType    = $DamageType::Plasma;

   explosion           = "PlasmaBoltExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 150.0;
   wetVelocity       = -1;
//   velInheritFactor  = 0.3;
   velInheritFactor  = 0.5;
//   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;


   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;
   
   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1 0.75 0.25";
};

function PlasmaBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
//     if(%targetObject.getType() & $TypeMasks::PlayerObjectType)	// -[soph]
//          if(!%targetObject.inDEDField && %targetObject.team != %projectile.sourceObject.team)
//               %targetObject.burnObject(%projectile.sourceObject);	// -[/soph]

     Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

function PlasmaBolt::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{
     Parent::onExplode( %data , %proj , %pos , %mod ) ;
     IncendiaryExplosion( %pos , %data.damageradius , 2 , %proj.sourceObject , true );
}								// +[/soph]

datablock LinearFlareProjectileData(PowerPlasmaBolt)
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "3.0 3.0 3.0";
   directDamage        = 0.0;
   directDamageType    = $DamageType::Plasma;
   hasDamageRadius     = true;
   indirectDamage      = 0.52;
   damageRadius        = 7.0;
   kickBackStrength    = 1000;
   radiusDamageType    = $DamageType::Plasma;

   explosion           = "HeavyPlasmaExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = FireboltFireEmitter;
   delayEmitter        = FireboltSmokeEmitter;

   dryVelocity       = 150;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 400;
   lifetimeMS        = 500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;

   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "1 0.75 0.25";
};

function PowerPlasmaBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
//     if(%targetObject.getType() & $TypeMasks::PlayerObjectType)			// -[soph]
//          if(!%targetObject.inDEDField && %targetObject.team != %projectile.sourceObject.team)
//               %targetObject.burnObject(%projectile.sourceObject);			// -[/soph]

     Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

function PowerPlasmaBolt::onExplode(%data, %proj, %pos, %mod)
{
     Parent::onExplode( %data , %proj , %pos , %mod ) ;

    // if(getRandom(10) < 2)								// -soph
    //      RadiusBurn(%proj, %pos, %data.damageRadius, %proj.sourceObject);		// -soph

     IncendiaryExplosion( %pos , %data.damageradius , 2 , %proj.sourceObject , true ) ;	// +soph
}

function RadiusBurn(%explosionSource, %position, %radius, %sourceObject)		// now handled by incendiaryexplosion() in mdexternal.cs -soph
{
   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType);

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      if(!isObject(%targetObject)) // bug!
         return;

      if(%targetObject.devShield)
         return;

      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

      if(%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if(%mount.getDataBlock().isProtectedMountPoint[%found] && %mount.getEnergyLevel() < 5)
               continue;
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      if(!isObject(%targetObject))
         continue;

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::StaticShapeObjectType |
                                         $TypeMasks::VehicleObjectType));
      if (%coverage == 0)
         continue;

      if(!%targetObject.inDEDField && %targetObject.team != %sourceObject.team)
            %targetObject.burnObject(%sourceObject);
   }
}

function ExBoltOld::onExplode(%data, %proj, %pos, %mod)
{
     Parent::onExplode(%data, %proj, %pos, %mod);

     %seed = getRandom(6, 3);
     for(%i = 0; %i < %seed; %i++)
     {
        %steamTime = getRandom(1) == 1 ? 2000 + getRandom(1500) : 2000 - getRandom(1500);
        %px = getWord(%pos, 0) + (getRandom(1) == 1 ? getRandom(%data.damageRadius) : getRandom(%data.damageRadius) * -1);
        %py = getWord(%pos, 1) + (getRandom(1) == 1 ? getRandom(%data.damageRadius) : getRandom(%data.damageRadius) * -1);
        %npos = %px SPC %py SPC getWord(%pos, 2);
        createLifeEmitter(%npos, PlasmaASMEmitter, %steamTime);
     }
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(PlasmaAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some plasma gun ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Plasma)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_plasma.dts";
   image = PlasmaImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a plasma gun";
};

datablock ShapeBaseImageData(PlasmaImage)
{
   className = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   item = Plasma;
   ammo = PlasmaAmmo;
   offset = "0 0 0";

   projectile = PlasmaBolt;
   projectileType = LinearFlareProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activation";
   stateSound[0] = PlasmaSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.1;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitterTime[3] = 0.2;
   stateSequence[3] = "fire";
//   stateSound[3] = PlasmaFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.4;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";
   
   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire"; 
};

function PlasmaImage::onFire(%data, %obj, %slot)
{
   %vehicle = 0;
     %ammouse = 1;
     
         %weapon = %obj.getMountedImage(0).item;

         if(%obj.client.mode[%weapon] == 0)
               %projectile = "PlasmaBolt";
         else if(%obj.client.mode[%weapon] == 1)
         {
               %projectile = "PowerPlasmaBolt";
               %ammouse = 2;
         }

       if(%obj.getInventory(%data.ammo) < %ammouse)
       {
         serverPlay3D(PlasmaFireWetSound, %obj.getTransform());
         return;
       }

     %p = new LinearFlareProjectile()
     {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if(%obj.heatShielded)
     %p.damageMod = %p.damageMod ? %p.damageMod * 1.25 : 1.25;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);
   %obj.play3D("PlasmaFireSound");
   
   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

      %obj.decInventory(%data.ammo, %ammouse);
}
