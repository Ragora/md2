if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// Extended Grenades
// ------------------------------------------------------------------------

datablock ItemData(FireGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = IncendiaryExplosion;
   indirectDamage      = 0.2;	// = 0.001; -soph
   damageRadius        = 16;	// = 12.5; -soph
   radiusDamageType    = $DamageType::Burn;
   kickBackStrength    = 100;
};

datablock ItemData(FireGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FireGrenadeThrown;
	pickUpName = "some incendiary grenades";
	isGrenade = true;
};
