if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Disc launcher   version 1.25 -Nite- Now with variable fire rates for each mode
//search my name to see changes
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(DiscSwitchSound)
{
   filename    = "fx/weapons/blaster_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(DiscLoopSound)
{
   filename    = "fx/weapons/spinfusor_idle.wav";
   description = ClosestLooping3d;
};

datablock AudioProfile(DiscFireSound)
{
   filename    = "fx/weapons/spinfusor_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(DiscReloadSound)
{
   filename    = "fx/weapons/spinfusor_reload.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(discExpSound)
{
   filename    = "fx/weapons/spinfusor_impact.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(underwaterDiscExpSound)
{
   filename    = "fx/weapons/spinfusor_impact_UW.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(discProjectileSound)
{
   filename    = "fx/weapons/spinfusor_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(DiscDryFireSound)
{
   filename    = "fx/weapons/spinfusor_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(DiscExplosionBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 750;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 1.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};
datablock ParticleEmitterData(DiscExplosionBubbleEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 3.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "DiscExplosionBubbleParticle";
};

datablock ExplosionData(UnderwaterDiscExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = underwaterDiscExpSound;

   faceViewer     = true;

   sizes[0]      = "1.3 1.3 1.3";
   sizes[1]      = "0.75 0.75 0.75";
   sizes[2]      = "0.4 0.4 0.4";
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

   emitter[0] = "DiscExplosionBubbleEmitter";

   shakeCamera = true;
   camShakeFreq = "10.0 11.0 10.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock ParticleData(DiscExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.3 0.3 0.66 1.0";
   colors[1]     = "0.3 0.3 0.66 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(DiscExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "DiscExplosionParticle";
};

datablock ExplosionData(DiscExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = discExpSound;

   faceViewer     = true;
   explosionScale = "1 1 1";

   particleEmitter = DiscExplosionEmitter;
   particleDensity = 300;
   particleRadius = 2.5;

   shakeCamera = true;
   camShakeFreq = "10.0 11.0 10.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock ParticleData(DiscDebrisSmokeParticle)
{
   dragCoeffiecient     = 0;
   gravityCoefficient   = 1;   // rises slowly
   inheritedVelFactor   = 0;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.8 0.8 1.0 0.75";
   colors[2]     = "0.2 0.2 1.0 0.5";
   colors[3]     = "0.05 0.05 1.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.7;
   sizes[2]      = 0.75;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(DiscDebrisSmokeEmitter)
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 2.5;

   ejectionVelocity = 1;  // A little oomph at the back end
   velocityVariance = 1;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   particles = "DiscDebrisSmokeParticle";
};

datablock DebrisData(DiscDebris)
{
   emitters[0] = DiscDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 100.0;
   lifetimeVariance = 30.0;

   numBounces = 4;
   bounceVariance = 3;
};

datablock ShockwaveData(SpeedDiscWave)
{
   width = 5;
   numSegments = 32;
   numVertSegments = 8;
   velocity = 30;
   acceleration = 10.0;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;
   
   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 0.8 0.5";
   colors[1] = "0.4 0.4 0.8 0.5";
   colors[2] = "0.4 0.4 0.8 0.5";
};

datablock ExplosionData(DiscSExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = discExpSound;

   faceViewer     = true;
   explosionScale = "1 1 1";

   particleEmitter = DiscExplosionEmitter;
   particleDensity = 300;
   particleRadius = 2.5;

   shockwave = SpeedDiscWave;

   shakeCamera = true;
   camShakeFreq = "10.0 11.0 10.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock ExplosionData(SmlDiscExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = discExpSound;

   faceViewer     = true;
   explosionScale = "1.0 1.0 1.0";
   particleEmitter = DiscExplosionEmitter;
   particleDensity = 200;
   particleRadius = 2.5;

//   sizes[0] = "0.25 0.25 0.25";
//   sizes[1] = "0.25 0.25 0.25";
//   times[0] = 0.0;
//   times[1] = 1.0;
   shockwave = SpeedDiscWave;

   shakeCamera = true;
   camShakeFreq = "10.0 11.0 10.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

datablock ShockwaveData(LDiscShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 10;
   acceleration = 20.0;
   lifetimeMS = 900;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.0 0.50";
   colors[1] = "0.4 0.4 1.0 0.25";
   colors[2] = "0.4 0.4 1.0 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ExplosionData(LDiscSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 100;
   offset = 4.0;
   playSpeed = 1.0;

   sizes[0] = "0.75 0.75 0.75";
   sizes[1] = "1.0  1.0  1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LDiscSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 50;
   offset = 2.0;
   playSpeed = 0.75;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.75 0.75 0.75";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LDiscSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = -1.0;
   playSpeed = 0.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(LrgDiscExplosion)
{
   soundProfile   = discExpSound;

   shockwave = LDiscShockwave;
   shockwaveOnTerrain = false;

   subExplosion[0] = LDiscSubExplosion1;
   subExplosion[1] = LDiscSubExplosion2;
   subExplosion[2] = LDiscSubExplosion3;

   particleEmitter = DiscExplosionEmitter;
   particleDensity = 400;
   particleRadius = 3.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData(DiscMist)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DiscMistEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   lifetimeMS       = 250;
   particles = "DiscMist";
};

datablock ParticleData( DiscSplashParticle2 )
{

   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.03;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 600;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;


   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( DiscSplashEmitter2 )
{
   ejectionPeriodMS = 25;
   ejectionOffset = 0.2;
   periodVarianceMS = 0;
   ejectionVelocity = 2.25;
   velocityVariance = 0.50;
   thetaMin         = 0.0;
   thetaMax         = 30.0;
   lifetimeMS       = 250;

   particles = "DiscSplashParticle2";
};


datablock ParticleData( DiscSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( DiscSplashEmitter )
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "DiscSplashParticle";
};


datablock SplashData(DiscSplash)
{
   numSegments = 15;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = DiscSplashEmitter;
   emitter[1] = DiscMistEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

datablock ParticleData(TDMStreakParticle)
{
   dragCoefficient      = 1.25;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 300;
   textureName          = "particleTest";
   colors[0]     = "0.2 0.4 1.0 0.375";
   colors[1]     = "0.1 0.2 1.0 0";
};

datablock ParticleEmitterData(TDMStreakEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 2;
   ejectionVelocity = 1;
   velocityVariance = 0.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 8;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TDMStreakParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearProjectileData(DiscProjectile)
{
   projectileShapeName = "disc.dts";
   emitterDelay        = -1;
   directDamage        = 0.25; 
   hasDamageRadius     = true;
   indirectDamage      = 0.5; 
   damageRadius        = 8.0;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 1750;

   sound 				= discProjectileSound;
   explosion           = "DiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 90;
   wetVelocity       = 50;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = 1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock LinearProjectileData(PowerDiscProjectile)
{
   projectileShapeName = "disc.dts";
   scale               = "2.5 2.5 2.5";	// +soph
   emitterDelay        = -1;
   directDamage        = 0.25; 
   hasDamageRadius     = true;
   indirectDamage      = 1.0; 
   damageRadius        = 10.0; 
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 3500;

   sound 				= discProjectileSound;
   explosion           = "LrgDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 90;
   wetVelocity       = 50;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = 1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

//function DiscProjectile::onExplode(%data, %proj, %pos, %mod)
//{
//   %cl = %proj.client;
//   %cl.setControlObject(%cl.player);
//   Parent::onExplode(%data, %proj, %pos, %mod);
//}

datablock LinearProjectileData(TurboDisc)
{
   projectileShapeName = "disc.dts";
   scale               = "1.0 1.0 1.0";
   emitterDelay        = -1;
   directDamage        = 0.125; 
   hasDamageRadius     = true;
   indirectDamage      = 0.25; 
   damageRadius        = 7.5;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 5000;

   sound 				= discProjectileSound;
   explosion           = "SmlDiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   dryVelocity       = 180;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

//   baseEmitter = TDMStreakEmitter;

   activateDelayMS = 1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock EnergyProjectileData(RipperProjectile)
{
   projectileShapeName = "disc.dts";
   emitterDelay        = -1;
   directDamage        = 0.5;
   directDamageType    = $DamageType::Ripper;
   kickBackStrength    = 250;

   grenadeElasticity = 0.998;
   grenadeFriction   = 0.0;
   armingDelayMS     = 4750;

   sound 				= discProjectileSound;
   explosion           = "DiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;
   
   velInheritFactor    = 1.0;
   muzzleVelocity    = 125.0;
   drag = 0;
   gravityMod        = 0.1;

   dryVelocity       = 200.0;
   wetVelocity       = 350.0;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = 1;
   
   hasLight    = true;

   scale = "1.2 1.2 1.2";
   crossViewAng = 0.99;
   crossSize = 0.01;

   lifetimeMS     = 5000;
   blurLifetime   = 5.0;
   blurWidth      = 0.35;
   blurColor = "0.05 0.2 1.0 0.75";

   texture[0] = "special/ELFLightning";
   texture[1] = "special/ELFLightning";
};

function RipperProjectile::onExplode(%data, %proj, %pos, %mod)
{
     Parent::onExplode(%data, %proj, %pos, %mod);
     %proj.schedule(100, delete);
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(DiscAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some spinfusor discs";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Disc)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_disc.dts";
   image = DiscImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a spinfusor";
   emap = true;
};

datablock ShapeBaseImageData(DiscImage)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   item = Disc;
   ammo = DiscAmmo;
   offset = "0 0 0";
   emap = true;

//   projectile = DiscProjectile;
//   projectileType = LinearProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.25; //was 1.25,set to faster fire rate -Nite-
   stateFire[3]                     = true;
  // stateRecoil[3]                   = LightRecoil;//dont need now  -Nite-
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   //stateSound[3]                    = DiscFireSound; //dont need now  -Nite-

   
   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; //0.5 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   //stateSound[4]                    = DiscReloadSound;  //stops the sound from looping  -Nite-

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function DiscFireTimeoutClear(%obj)  //-Nite-
{
   %obj.fireTimeoutDisc = 0;
}

function DiscImage::onFire(%data, %obj, %slot)
{
     if(%obj.fireTimeoutDisc)  //-nite-
      return;
//   %data.lightStart = getSimTime();

//   if( %obj.station $= "" && %obj.isCloaked() )
//   {
//      if( %obj.respawnCloakThread !$= "" )
//      {
//         Cancel(%obj.respawnCloakThread);
//         %obj.setCloaked( false );
//         %obj.respawnCloakThread = "";
//      }
//      else
//      {
//         if( %obj.getEnergyLevel() > 20 )
//         {
//            %obj.setCloaked( false );
//            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
//         }
//      }
//   }

        %vehicle = 0;
        %weapon = %obj.getMountedImage(0).item;

        if(%obj.client.mode[%weapon] == 0)
        {
             %projectile = "DiscProjectile";
             %mode = "LinearProjectile";
             %obj.fireTimeoutDisc = 1000;  //should be the same as 1.25  -Nite-
             %enUse = 0;
             %ammoUse = 1;
        }                       //1250
        else if(%obj.client.mode[%weapon] == 1)
        {
             %projectile = "TurboDisc";
             %mode = "LinearProjectile";
             %obj.fireTimeoutDisc = 1250;   //should be the same as .25  -Nite-
             %enUse = 7;
             %ammoUse = 1;
        }                          //230
        else if(%obj.client.mode[%weapon] == 2)
        {
             %projectile = "PowerDiscProjectile";
             %mode = "LinearProjectile";
             %enUse = 15;
             %ammoUse = 2;
             %obj.fireTimeoutDisc = 1500; //should be same as 2.25  -Nite-
        }                                //2250

        if(%obj.powerRecirculator)
          %enUse *= 0.75;
          
     if(%ammoUse > %obj.getInventory(%data.ammo) || %enUse > %obj.getEnergyLevel())
     {
          schedule(%obj.fireTimeoutDisc, 0, "DiscFireTimeoutClear", %obj);
          return;
     }

     %p = new(%mode)()
     {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);
   // all this for emulating state stuff -Nite-
   %obj.play3D(DiscFireSound);// play fire sounds -Nite-
   if( %obj.getState() !$= "Dead" )				// +soph
      %obj.setActionThread("light_recoil");  //Play anim recoil  -Nite-
   schedule(500, %obj.play3D(DiscReloadSound),%obj);//play reload sound 500ms after we fire  -Nite-
   schedule(%obj.fireTimeoutDisc, 0, "DiscFireTimeoutClear", %obj); //Use fire time out for each mode   -Nite-
  // serverCmdPlayAnim(%obj.client,"light_recoil"); //this worked too lol  -Nite-

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo, %ammoUse);
   %obj.useEnergy(%enUse);
   
   if(%obj.client.mode[%weapon] == 1)
      %obj.applyKick(-500);
      
   if(%obj.client.mode[%weapon] == 2)
      %obj.applyKick(-1000);
}

