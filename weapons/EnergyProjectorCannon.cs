if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Ecstacy Cannon
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
//datablock AudioProfile(PACFireSound)
//{
//   filename    = "fx/misc/gridjump.wav";
//   description = AudioDefault3d;
//   preload = true;
//};

datablock AudioProfile(MagMissileLauncherFireSound)
{
   filename    = "fx/weapons/grenade_flash_explode.wav";
   description = AudioDefault3d;
   preload = true;
};


datablock TracerProjectileData(EcstacyBolt)
{
   doDynamicClientHits = true;
  // isBallistic = false;
   directDamage        = 0.0; // MrKeen     ? was 0
   hasDamageRadius     = true;
   indirectDamage      = 0.11;   //022 + 010 = 032
   damageRadius        = 1.0;  //was 2.0

   explosion           = "AABulletExplosion";
   splash              = ChaingunSplash;

   directDamageType    = $DamageType::Ecstacy;
   radiusDamageType    = $DamageType::Ecstacy;
   kickBackStrength  = 0.0;

   sound = ShrikeBlasterProjectileSound;

   dryVelocity       = 300.0;     //speed increase
   wetVelocity       = 300.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   tracerLength    = 20.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = "0.2 0.5 1.0 0.8";
	tracerTex[0]  	 = "special/bluespark";
	tracerTex[1]  	 = "precipitation/snowflake010.png";
	tracerWidth     = 0.6;
   crossSize       = 0.92;
   crossViewAng    = 0.990;
   renderCross     = true;
};

//datablock SniperProjectileData(EPCPunchBeam)
//{
//   directDamage        = 0.4;
//   hasDamageRadius     = false;
//   indirectDamage      = 0.0;
//   damageRadius        = 0.0;
//   velInheritFactor    = 1.0;
//   sound 				  = SniperRifleProjectileSound;
//   explosion           = "SniperExplosion";
//   splash              = SniperSplash;
//   directDamageType    = $DamageType::PunchBeam;

//   maxRifleRange       = 500;
//   rifleHeadMultiplier = 1.3;
//   beamColor           = "1 0.1 0.1";
//   fadeTime            = 1.0;

//   startBeamWidth		  = 0.29;
//   endBeamWidth 	     = 0.5;
//   pulseBeamWidth 	  = 1.0;
//   beamFlareAngle 	  = 3.0;
//   minFlareSize        = 0.0;
//   maxFlareSize        = 400.0;
//   pulseSpeed          = 6.0;
//   pulseLength         = 0.150;

//   lightRadius         = 1.0;
//   lightColor          = "0.3 0.0 0.0";

//   textureName[0]      = "special/flare";
//   textureName[1]      = "special/nonlingradient";
//   textureName[2]      = "special/laserrip01";
//   textureName[3]      = "special/laserrip02";
//   textureName[4]      = "special/laserrip03";
//   textureName[5]      = "special/laserrip04";
//   textureName[6]      = "special/laserrip05";
//   textureName[7]      = "special/laserrip06";
//   textureName[8]      = "special/laserrip07";
//   textureName[9]      = "special/laserrip08";
//   textureName[10]     = "special/laserrip09";
//   textureName[11]     = "special/sniper00";
//};

function EPCPunchBeam::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %damageAmount = %projectile.damageFactor;

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   checkMAHit(%projectile, %targetObject, %position);

   %dist = vectorDist(%projectile.sourceObject.getWorldBoxCenter(), %position);
   %falloff = 200;
   %minMod = 0.01;

   if(%dist > %falloff)
   {
     %mod = 1 - ((%dist - %falloff) / (%data.maxRifleRange - %falloff));
     %mod = %mod < %minMod ? %minMod : %mod;
     %damageAmount *= %mod;
   }
   
   %modifier = 1;

   if(%damageAmount > (%targetObject.getDatablock().maxDamage * 1.5) && %targetObject.getType() & $TypeMasks::PlayerObjectType)
//   {
      BAOverdrivePulseUpdate(%targetObject);
//      %targetObject.startFade(500, 0, true);
//   }

   if(%damageAmount > 1.25 && %targetObject.getType() & $TypeMasks::PlayerObjectType)
   {
      %damLoc = firstWord(%targetObject.getDamageLocation(%position));
      
      if(%damLoc $= "head")
      {
         %targetObject.getOwnerClient().headShot = 1; // OMG They killed kenny!
         %modifier = 2;
      }
      else
      {
         %modifier = 1;
         %targetObject.getOwnerClient().headShot = 0;
      }
   }
   else
      %targetObject.getOwnerClient().headShot = 0;

   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(EcstacyCapacitor)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some ecstacy capacitor charges";
   cantPickup = true;

   emap = true;
};

function detectIsPlayer(%pos)
{
   InitContainerRadiusSearch(%pos, 0.005, $TypeMasks::PlayerObjectType);//look for players
    if(ContainerSearchNext()) //if i see a player return true,else no see
      return true;
   else
      return false;
}

function EcstacyBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
//   if(detectIsPlayer(%position))// is it a player? yes no?
     // if((%targetObject.team || %targetObject.client.team) != %projectile.sourceObject.team) //is a player on our team?
//         %targetObject.damage(%projectile.sourceObject, %position, %data.directDamage * %modifier, %data.directDamageType);
        // else
          // %targetObject.damage(%projectile.sourceObject, %position, %data.indirectDamage * %modifier, %data.indirectDamageType);
           
 Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(EcstacyCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_elf_large.dts";
   image = EcstacyCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "an energy projector cannon";
};

datablock ShapeBaseImageData(EcstacyCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   item = EcstacyCannon;
   ammo = EcstacyCapacitor;

   offset = "0 0.2 0.05";
   emap = true;
   usesEnergy = true;
   minEnergy = -1;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Deploy";
	stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "Ready";

   stateName[1]                     = "Ready";
   stateScript[ 1 ]                   = "onReady" ;		// +soph
   stateTransitionOnTriggerDown[1]  = "Fire";

   stateName[2]                     = "Fire";
   stateFire[2]                     = true;
   stateAllowImageChange[2]         = false;
   stateScript[2]                   = "onFire";
   stateTransitionOnTriggerUp[2]    = "Deconstruction";

   stateName[3]                     = "Deconstruction";
   stateScript[3]                   = "onRelease";
   stateTimeoutValue[3]             = 0.5;
   stateTransitionOnTimeout[3]      = "Ready";
};

datablock ShapeBaseImageData(PCDecalAImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   offset = "0.12 0.85 0";
   emap = true;
   rotation = "0 0 1 -90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";
};

datablock ShapeBaseImageData(PCDecalBImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   offset = "-0.12 0.85 0";
   emap = true;
   rotation = "0 0 1 90";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";
};

datablock ShapeBaseImageData(PCDecalCImage)
{
   shapeFile = "pack_upgrade_cloaking.dts";
   offset = "0 0.6 0.32";
   emap = true;
   rotation = calcThisRot("-1.57 0 0");//"0 0 1 180";

   stateName[0]                     = "idle";
   stateSequence[0]                 = "fire";
};

function EcstacyCannonImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this,%obj,%slot);

   %obj.mountImage(PCDecalAImage, 4);
   %obj.mountImage(PCDecalBImage, 5);
   %obj.mountImage(PCDecalCImage, 6);
   
   %obj.ecs_EcstacyOnHand = %obj.getMountedImage(0);
}

function EcstacyCannonImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);

   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

function ecstacyFire(%data, %player, %charge, %ecstacy, %offset, %isEcstacy)
{
   %data.lightStart = getSimTime();

   if(!%player.getMountedImage(0) == %isEcstacy)
        return false;
          
   if(%player.chargeProjectile)
        %player.chargeProjectile.delete();

   for(%i=0; %i < %charge; %i++)
   {
      %vector = %player.getMuzzleVector(%ecstacy);
      %x = (getRandom() - 0.5) * 2 * 3.1415926 * (%offset / 1000);
      %y = (getRandom() - 0.5) * 2 * 3.1415926 * (%offset / 1000);
      %z = (getRandom() - 0.5) * 2 * 3.1415926 * (%offset / 1000);
      %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      %vector = MatrixMulVector(%mat, %vector);

      %p = new TracerProjectile()
      {
          dataBlock        = "EcstacyBolt";
          initialDirection = %vector; // %player.getMuzzleVector(%ecstacy);
          initialPosition  = %player.getMuzzlePoint(%ecstacy);
          sourceObject     = %player;
          sourceSlot       = %ecstacy;
          vehicleObject    = 0;
      };

   %useEnergyObj = %player.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %player;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%player.damageMod)
      %p.damageMod = %player.damageMod;
   else
      %p.damageMod = 1;
      
   if(%player.synMod)
     %p.damageMod += %player.synMod;

        if (isObject(%player.lastProjectile) && %player.deleteLastProjectile)
           %player.lastProjectile.delete();

        %player.lastProjectile = %p;
        %player.deleteLastProjectile = %data.deleteLastProjectile;
        %p.ignoreReflectionsFF = true;

        MissionCleanup.add(%p);
    }

    %player.setInventory(%data.ammo,0);     // start from scratch

   // AI hook
   if(%player.client)
      %player.client.projectile = %p;

   serverPlay3D(AAFireSound, %player.getTransform());
}

function ecstacyCharge( %data , %player , %ecstacy , %display )						// (%data, %player, %ecstacy) -soph       // should work....
{
     if(!isObject(%player))
          return;
     
     %ammo = %player.getInventory(%data.ammo);
     %ammoMax = %player.getDatablock().max["EcstacyCapacitor"];
     %ecstacyOnHand = %player.ecs_EcstacyOnHand;
     %mode = %player.client.mode[%player.getMountedImage(0).item];
     
     if(%mode == 0)
          %chargeProj = "FirelanceBolt";
     else if(%mode == 1)
          %chargeProj = "BasicShocker";
     else if(%mode == 2)
          %chargeProj = "FXPulse";
     
     if(%player.ecstacy)
     {
          if(%player.getMountedImage(0) == %ecstacyOnHand)
          {
            if(!%player.isEMP)
            {
               if( %player.ecc_zapticks > 4 )								// (%player.ecc_chargeticks > 2) -soph
               {
                  %player.ecc_zapticks = 0 ;								// %player.ecc_chargeticks = 0; -soph
                  %q = new ShockLanceProjectile()
                  {
                     dataBlock        = %chargeProj;
                     initialDirection = %player.getMuzzleVector(0);
                     initialPosition  = %player.getMuzzlePoint(0);
                     sourceObject     = %player;
                     sourceSlot       = 0;
                  };
                  MissionCleanup.add(%q);
               }
               else
                  %player.ecc_zapticks++ ;								// %player.ecc_chargeticks++; -soph

               if( %player.isSyn )									// +[soph]
                  %ticks = 0 ;										// +
               else											// +
                  %ticks = 1 ;										// +
													// +
               if( %player.ecc_chargeTicks >= %ticks )							// +
               {											// +
                  %player.ecc_chargeTicks = 0 ;								// +[/soph]
                  %player.incInventory(EcstacyCapacitor, 1);

                  if(%ammo > %ammoMax)
                     %ammo = %ammoMax;   // quick non-overcharge hack
               }											// +[soph]
               else											// +
                  %player.ecc_chargeTicks++ ;								// +[/soph]
            }
            else
               %player.setInventory(EcstacyCapacitor, 0);
            
	    if( isNearForcefield( %player , 0.7 ) ) 
            {
               if( %player.inv[ EcstacyCapacitor > 2 ] )
                  %player.playAudio( 0 , ShockLanceMissSound ) ;
               %player.ecc_chargeticks = 0 ;
               %player.setInventory( EcstacyCapacitor , 0 ) ;
            }

            if( getRandom( 1 , 40 ) < mSqRt( %ammo ) )							// +[soph]
            {												// +
               %offset = vectorScale( %player.getMuzzleVector( 0 ) , 1 + ( getRandom( 5 ) / 10 ) ) ;	// +
               %offset = vectorAdd( %player.getMuzzlePoint( 0 ) , %offset ) ;				// +
               %offset = getWord( %offset , 0 ) + ( getRandom( -3 , 3 ) / 6 ) SPC getWord( %offset , 1 ) + ( getRandom( -3 , 3 ) / 6 ) SPC getWord( %offset , 2 ) + ( getRandom( -6 , 0 ) / 6 ) ;
               createLifeEmitter( %offset , BlueBlasterExplosionEmitter2 , 96 ) ;			// +
            }												// +
            %player.ecc_chargeTime = getSimTime() ;							// +
            schedule( 50 , 0 , "ecstacyCharge" , %data, %player , %ecstacy ) ;				// +[/soph]

//               if(%player.isSyn)									// -[soph]
//                    schedule(50, 0, "ecstacyCharge", %data, %player, %ecstacy);			// -
//               else											// -
//                    schedule(100, 0, "ecstacyCharge", %data, %player, %ecstacy);			// -[/soph]
          }
     }
     else
     {
          if(%player.getMountedImage(0) == %ecstacyOnHand)
          {
               if(%mode == 0)
               {
                    %charge = mFloor(%ammo / 8);    //gonna try 28 instead of 35
                    %offset = %ammo / 48; // MrKeen
                    ecstacyFire(%data, %player, %charge, %ecstacy, %offset, %ecstacyOnHand);
                    return;
               }
               else if(%mode == 1)
               {
                    %radius = %ammo * 0.06; //0.157
                    %dmg = %ammo / 85; // MrKeen
                    CLFire(%data, %player, %ammo, %ecstacy, %radius, %ecstacyOnHand, %dmg);
                    return;
               }
               else if(%mode == 2)
               {
                    LRPunchBeamFire(%data, %player, %ammo, %ecstacy, %ecstacyOnHand);
                    return;
               }
          }
     }
}

function LRPunchBeamFire(%data, %player, %charge, %ecstacy, %isEcstacy)
{
   if(!%player.getMountedImage(0) == %isEcstacy)
        return false;

   if(%player.chargeProjectile)
        %player.chargeProjectile.delete();

   %useEnergyObj = %player.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %player;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %dmgMod = 1;

   // Vehicle Damage Modifier
   if(%vehicle)
     %dmgMod += %vehicle.damageMod;

   if(%player.damageMod)
      %dmgMod += %player.damageMod - 1;

   if(%player.synMod)
     %dmgMod += %player.synMod;

      %p = new SniperProjectile()
      {
         dataBlock        = EPCPunchBeam;
         initialDirection = %player.getMuzzleVector(0);
         initialPosition  = %player.getMuzzlePoint(0);
         sourceObject     = %player;
         damageFactor     = 4.25 * (%charge / 512) * %dmgMod;
         sourceSlot       = 0;
      };
      %p.setEnergyPercentage(1.0);

      %player.lastProjectile = %p;
      MissionCleanup.add(%p);

    %player.setInventory(%data.ammo, 0);     // start from scratch

   // AI hook
   if(%player.client)
      %player.client.projectile = %p;

   serverPlay3D(MagMissileLauncherFireSound, %player.getTransform());
}

$Ecstacy::ChargeDrain = 0.17;

function EcstacyCannonImage::onReady( %data , %obj , %slot )					// EcstacyCannonImage::onFire(%data,%obj,%slot) -soph
{
     if(!%obj.ecstacy)
     {
          %ammo = %obj.getInventory(%data.ammo);
          %aPack = %player.hasAmmoPack ? 256 : 0;
          %ammoMax = %obj.getDatablock().max["EcstacyCapacitor"] + %aPack;
//          %obj.setRechargeRate(%obj.getRechargeRate() - $Ecstacy::ChargeDrain); // MrKeen

          %obj.ecstacy = true;
          %obj.incInventory(EcstacyCapacitor, 1);
          %obj.ecc_chargeticks = 0 ;								// +[soph]
          %obj.ecc_chargeTime = getSimTime() ;							// +
          schedule( 50 , 0 , "ecstacyCharge" , %data , %obj , %slot ) ;				// +[/soph]

//          if(%obj.isSyn)									// -[soph]
//               schedule(50, 0, "ecstacyCharge", %data, %obj, %slot);				// -
//          else										// -
//               schedule(100, 0, "ecstacyCharge", %data, %obj, %slot);				// -[/soph]
     }
}

function EcstacyCannonImage::onFire( %data , %obj , %slot )					// EcstacyCannonImage::onRelease(%data,%obj,%slot) -soph
{
     %obj.ecstacy = false;
//     %obj.setRechargeRate(%obj.getRechargeRate() + $Ecstacy::ChargeDrain);
}

//--------------------------------------
// Particle Accelerator Cannon
//--------------------------------------

datablock AudioProfile(PACFireSound)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioClose3d;
   preload = true;
};

function ChargeFXBolt1::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt2::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt3::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt4::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt5::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt6::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt7::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt8::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt9::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt10::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt11::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

function ChargeFXBolt12::onExplode(%data, %proj, %pos, %mod)
{
      RadiusExplosion(%proj, %pos, %proj.radius, %proj.damage, %proj.kick, %proj.sourceObject, %proj.radiusDamageType);
}

datablock LinearFlareProjectileData(ChargeFXBolt1)
{
   explosion           = "PlasmaBoltExplosion";
   splash              = DiscSplash;

   dryVelocity       = 150.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3500;
   lifetimeMS        = 3500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "1.0 1.0 1.0";
   numFlares         = 30;
   flareColor        = "0.5 0.32 0.5";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/shrikeBoltCross";
};

datablock LinearFlareProjectileData(ChargeFXBolt4)			// (ChargeFXBolt1x) -soph
{
   explosion           = "MiniPlasmaExplosion";
   splash              = DiscSplash;

   dryVelocity       = 135.0 ;						// = 150.0; -soph
   wetVelocity       = 120.0 ;						// = 150.0; -soph
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3500;
   lifetimeMS        = 3500;
   explodeOnDeath    = true ;						// false; -soph
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "4.0 4.0 4.0" ;					// = "1.0 1.0 1.0"; -soph
   numFlares         = 30;
   flareColor        = "0.5 0.32 0.5";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/shrikeBoltCross";
};

datablock LinearFlareProjectileData(ChargeFXBolt7)			// (ChargeFXBolt1y) -soph
{
   explosion           = "SuperPlasmaExplosion";
   splash              = DiscSplash;

   dryVelocity       = 120.0 ;						// = 150.0; -soph
   wetVelocity       = 90.0 ;						// = 150.0; -soph
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3500;
   lifetimeMS        = 3500;
   explodeOnDeath    = true ;						// false; -soph
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "7.0 7.0 7.0" ;					// = "1.0 1.0 1.0"; -soph
   numFlares         = 30;
   flareColor        = "0.5 0.32 0.5";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/shrikeBoltCross";
};

datablock LinearFlareProjectileData(ChargeFXBolt10)			// (ChargeFXBolt1z) -soph
{
   explosion           = "MitziBlastXExplosion";
   splash              = DiscSplash;

   dryVelocity       = 105.0 ;						// = 150.0; -soph
   wetVelocity       = 60.0 ;						// = 150.0; -soph
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3500;
   lifetimeMS        = 3500;
   explodeOnDeath    = true ;						// false; -soph
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "10.0 10.0 10.0" ;				// = "1.0 1.0 1.0"; -soph
   numFlares         = 30;
   flareColor        = "0.5 0.32 0.5";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/shrikeBoltCross";
};

datablock LinearFlareProjectileData(ChargeFXBolt2) : ChargeFXBolt1
{
   dryVelocity       = 145.0;
   wetVelocity       = 140.0;
   scale             = "2.0 2.0 2.0";
};

datablock LinearFlareProjectileData(ChargeFXBolt3) : ChargeFXBolt1
{
   dryVelocity       = 140.0;
   wetVelocity       = 130.0;
   scale             = "3.0 3.0 3.0";
};

//datablock LinearFlareProjectileData(ChargeFXBolt4) : ChargeFXBolt1x	// -[soph]
//{									// - datablock conservation
//   dryVelocity       = 135.0;						// -
//   wetVelocity       = 120.0;						// -
//   scale             = "4.0 4.0 4.0";					// -
//};									// -[/soph]

datablock LinearFlareProjectileData(ChargeFXBolt5) : ChargeFXBolt4	// : ChargeFXBolt1x -soph
{
   dryVelocity       = 130.0;
   wetVelocity       = 110.0;
   scale             = "5.0 5.0 5.0";
};

datablock LinearFlareProjectileData(ChargeFXBolt6) : ChargeFXBolt4	// : ChargeFXBolt1x -soph
{
   dryVelocity       = 125.0;
   wetVelocity       = 100.0;
   scale             = "6.0 6.0 6.0";
};

//datablock LinearFlareProjectileData(ChargeFXBolt7) : ChargeFXBolt1y	// -[soph]
//{									// - datablock conservation
//   dryVelocity       = 120.0;						// -
//   wetVelocity       = 90.0;						// -
//   scale             = "7.0 7.0 7.0";					// -
//};									// -[/soph]

datablock LinearFlareProjectileData(ChargeFXBolt8) : ChargeFXBolt7	// : ChargeFXBolt1y -soph
{
   dryVelocity       = 115.0;
   wetVelocity       = 80.0;
   scale             = "8.0 8.0 8.0";
};

datablock LinearFlareProjectileData(ChargeFXBolt9) : ChargeFXBolt7	// : ChargeFXBolt1y -soph
{
   dryVelocity       = 110.0;
   wetVelocity       = 70.0;
   scale             = "9.0 9.0 9.0";
};

//datablock LinearFlareProjectileData(ChargeFXBolt10) : ChargeFXBolt1z	// -[soph]
//{									// - datablock conservation
//   dryVelocity       = 105.0;						// -
//   wetVelocity       = 60.0;						// -
//   scale             = "10.0 10.0 10.0";				// -
//};									// -[/soph]

datablock LinearFlareProjectileData(ChargeFXBolt11) : ChargeFXBolt10	// : ChargeFXBolt1z -soph
{
   dryVelocity       = 100.0;
   wetVelocity       = 50.0;
   scale             = "11.0 11.0 11.0";
};

datablock LinearFlareProjectileData(ChargeFXBolt12) : ChargeFXBolt10	// : ChargeFXBolt1z -soph
{
   dryVelocity       = 75.0;
   wetVelocity       = 40.0;
   scale             = "12.0 12.0 12.0";
};

function CLFire(%data, %player, %charge, %CL, %radius, %isCL, %dmg)
{
   %data.lightStart = getSimTime();

   if(!%player.getMountedImage(0) == %isCL)
        return false;

   if(%radius < 5.0)
      %radius = 5.0;

   if(%radius > 15.0)
      %radius = 15.0;

     if(%charge < 35 )			// 32) -soph
          %chgProj = "ChargeFXBolt1";
     else if(%charge < 75 )		// 64) -soph
          %chgProj = "ChargeFXBolt2";
     else if(%charge < 120 )		// 126) -soph
          %chgProj = "ChargeFXBolt3";
     else if(%charge < 171 )		// 192) -soph
          %chgProj = "ChargeFXBolt4";
     else if(%charge < 227 )		// 254) -soph
          %chgProj = "ChargeFXBolt5";
     else if(%charge < 288 )		// 325) -soph
          %chgProj = "ChargeFXBolt6";
     else if(%charge < 355 )		// 375) -soph
          %chgProj = "ChargeFXBolt7";
     else if(%charge < 427 )		// 440) -soph
          %chgProj = "ChargeFXBolt8";
     else if(%charge < 504 )		// 500)	-soph
          %chgProj = "ChargeFXBolt9";
     else if(%charge < 587 )		// 575) -soph
          %chgProj = "ChargeFXBolt10";
     else if(%charge < 675 )		// 660) -soph
          %chgProj = "ChargeFXBolt11";
     else
          %chgProj = "ChargeFXBolt12";

   %p = new LinearFlareProjectile()
   {
          dataBlock        = %chgProj;
          initialDirection = %player.getMuzzleVector(%CL);
          initialPosition  = %player.getMuzzlePoint(%CL);
          sourceObject     = %player;
          sourceSlot       = %CL;
          radius           = %radius;
          damage           = %dmg;
          kick             = (%charge*15);
          scale            = %scale;
          radiusDamageType = $DamageType::PAC;
          vehicleObject    = 0;
   };

   %useEnergyObj = %player.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %player;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%player.damageMod)
      %p.damageMod = %player.damageMod;
   else
      %p.damageMod = 1;

   if(%player.synMod)
     %p.damageMod += %player.synMod;

     %player.lastProjectile = %p;
     MissionCleanup.add(%p);

     %p.radius = %radius;
     %p.damage = %dmg;
     %p.kick = (%charge*15);
     %p.setScale(%scale);
     %player.setInventory(%data.ammo,0);     // start from scratch
     %player.applyKick(%charge*-27.5);

//   %q = new LinearFlareProjectile()
//   {
//          dataBlock        = %chgProj;
//          initialDirection = %player.getMuzzleVector(%CL);
//          initialPosition  = %player.getMuzzlePoint(%CL);
//          sourceObject     = %player;
//          sourceSlot       = %CL;
//          vehicleObject    = 0;
//   };
//   MissionCleanup.add(%q);


   // AI hook
   if(%player.client)
      %player.client.projectile = %p;

//   %player.getMountedImage(4).playThread(0, "spin");
   serverPlay3D(PACFireSound, %player.getTransform());
}
