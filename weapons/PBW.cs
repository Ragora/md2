if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Particle Beam Weapon
//--------------------------------------------------------------------------

datablock AudioProfile(PBWSwitchSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PBWFireSound)
{
   filename    = "fx/misc/lightning_impact.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PBWProjectileSound)
{
   filename    = "fx/powered/turret_aa_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock AudioProfile(PBWExpSound)
{
   filename    = "fx/powered/turret_plasma_explode.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ShockwaveData(PBWShockwave)
{
   width = 6;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 40;
   acceleration = 35.0;
   lifetimeMS = 999;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.5;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 0.5";
   colors[2] = "0.0 0.0 1.0 0.0";
};

datablock ShockwaveData(PBWFireShockwave)
{
   width = 6;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 30;
   acceleration = 10.0;
   lifetimeMS = 300;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.3 1.0 1.0";
   colors[1] = "0.5 0.8 0.5 0.5";
   colors[2] = "0.0 1.0 0.0 0.0";
};

datablock ExplosionData(PBWExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = PBWExpSound;
   shockwave      = PBWShockwave;
   faceViewer     = true;
   playSpeed      = 0.3;

   sizes[0] = "2 2 2";
   sizes[1] = "2 2 2";
   times[0]      = 0.0;
   times[1]      = 1.0;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "15.0 15.0 15.0";
   camShakeDuration = 1.5;
   camShakeRadius = 10.0;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock SniperProjectileData(ParticleBeam)
{
   directDamage        = 1.389;
   velInheritFactor    = 1.0;
   faceViewer          = true;
   sound 			   = PBWProjectileSound;
   explosion           = "BlueBlasterExplosion" ;	// = "PBWExplosion"; -soph
   splash              = SniperSplash ;			// = PlasmaSplash; -soph
   directDamageType    = $DamageType::PBW;

   maxRifleRange       = 300;
   rifleHeadMultiplier = 1.25;
   beamColor           = "1 1 1"; //0.1 1 0.1";
   fadeTime            = 1.0; 

   startBeamWidth		  = 0.40; //0.05
   endBeamWidth 	     = 0.40; //0.05
   pulseBeamWidth 	  = 0.025;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 3.0;
   pulseLength         = 0.4;

   lightRadius         = 5.0;
   lightColor          = "1 1 1"; //0.1 1 0.1";

   textureName[0]   = "special/flare";
  textureName[1]   = "special/nonlingradient";
  textureName[2]   = "special/laserrip01";
  textureName[3]   = "special/laserrip02";
  textureName[4]   = "special/laserrip03";
  textureName[5]   = "special/laserrip04";
  textureName[6]   = "special/laserrip05";
  textureName[7]   = "special/laserrip06";
  textureName[8]   = "special/laserrip07";
  textureName[9]   = "special/laserrip08";
  textureName[10]   = "special/laserrip09";
  textureName[11]   = "special/shocklance_effect02";

   //textures[0] = "special/ELFBeam";    //changed this solely for gat PBW
   //textures[1] = "special/ELFLightning";
   //textureName[0]      = "special/ELFBeam";
   //textureName[1]      = "special/ELFLightning";
};

datablock TargetProjectileData(PBWLaserProj)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;
   useInvAlpha          = true;
   maxRifleRange       	= 300;
   beamColor           	= "0.2 0.3 0.9";

   startBeamWidth			= 0.40;
   pulseBeamWidth 	   = 0.85;
   beamFlareAngle 	   = 30.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 8.0;
   pulseLength         	= 0.15;

   textureName[0]      	= "special/expFlare";
   textureName[1]      	= "special/ELFBeam";
   textureName[2]      	= "special/skyLightning";
   textureName[3]      	= "special/BlueImpact";
};

datablock LinearFlareProjectileData(PBWImpactProj)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::PBW;
   kickBackStrength    = 3000;

   explosion           = "PBWExplosion";
   underwaterExplosion = "PBWExplosion";

   splash              = PlasmaSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 32;
   lifetimeMS        = 32;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 32;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function ParticleBeam::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %shooter = %projectile.sourceObject;										// +soph
	
   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

  // checkMAHit(%projectile, %targetObject, %position);								// -soph

   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);
      
//   %dist = getObjectDistance(%projectile.sourceObject, %targetObject);
//   %pct = 1 - (%dist / $PBWDamageDistance);

   %damageAmount = %projectile.damageFactor;

   %dist = vectorDist(%projectile.sourceObject.getWorldBoxCenter(), %position);
   %falloff = 50;												//  %falloff = 200; -soph
   %minMod = 0.01;

   if(%dist > %falloff)
   {
     %mod = 1 - ((%dist - %falloff) / (%data.maxRifleRange - %falloff));
     %mod = %mod < %minMod ? %minMod : %mod;
     %damageAmount *= %mod;
   }

   if(%targetObject.getType() & $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType )			// && (%targetObject.team == %projectile.sourceObject.team)) -soph
   {
      %force = %projectile.damageForce;
      if( %targetObject.getType() & $TypeMasks::VehicleObjectType )						// +[soph]
      {
         if( !%targetObject.inertialDampener )
            %force /= 6;
         zapVehicle( %targetObject , "BasicShocker" );
      }
      else
         %targetObject.zapObject();										// +[/soph]
      %p = %targetObject.getWorldBoxCenter();
      %momVec = VectorSub(%p, %position);
      %momVec = VectorNormalize(%momVec);
     // %momVec = VectorNormalize( %shooter.getMuzzleVector( %projectile.sourceSlot ) );			//  saving for if it needs a buff -soph
      %impulseVec = VectorScale(%momVec, %force);
      %targetObject.applyImpulse(%position, %impulseVec);
     // %targetObject.zapObject();										// -soph
   }
   if( %targetObject.PBWBuildup[%shooter] )									// +[soph]
      %targetObject.PBWBuildup[%shooter] += %damageAmount;
   else
      %targetObject.PBWBuildup[%shooter] = %damageAmount;
   %targetObject.setDamageFlash( %targetObject.PBWBuildup[%shooter] / 4 );

   if( %targetObject.PBWHitThread[%shooter] )
      cancel( %targetObject.PBWHitThread[%shooter] );
   else
   {
      %p = new LinearFlareProjectile()
      {
         dataBlock        = PBWImpactProj;
         initialDirection = %projectile.sourceObject.getMuzzleVector(4);
         initialPosition  = %position;
         sourceObject     = %projectile.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
   }
   %targetObject.PBWHitThread[%shooter] = schedule( 65 , 0 , PBWResolveDamage , %data , %projectile , %targetObject , %position , %targetObject.PBWBuildup[%shooter] , %shooter );
														// +[/soph]
  // if(%targetObject.isFF)											// -[soph]
  //    %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount, %data.directDamageType);
  // else
  //    %targetObject.damage(%projectile.sourceObject, %position, %damageAmount, %data.directDamageType);	// -[/soph]
}

function PBWResolveDamage( %data , %projectile , %targetObject , %position , %damageAmount , %shooter )	// +[soph]
{
  // triggers after beam breaks contact, sums damage, and applies as single hit
   %projectile.damageFactor = %damageAmount;								// semi-hack to make mahit register correctly
   checkMAHit( %projectile, %targetObject, %position );

   if(%targetObject.isFF)
      %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount, %data.directDamageType);
   else
      %targetObject.damage(%projectile.sourceObject, %position, %damageAmount, %data.directDamageType);
   %targetObject.PBWBuildup[%shooter] = 0;
   %targetObject.PBWHitThread[%shooter] = false;
}													// +[/soph]

//---------------------------------------------------------------------------

datablock SniperProjectileData(TParticleBeam)
{
   directDamage        = 1.389;
   velInheritFactor    = 1.0;
   faceViewer          = true;
   sound 			   = PBWProjectileSound;
   explosion           = "BlueBlasterExplosion" ;	// = "PBWExplosion"; -soph
   splash              = SniperSplash ;			// = PlasmaSplash; -soph
   directDamageType    = $DamageType::PBW;

   maxRifleRange       = 400;	// 300; -soph
   rifleHeadMultiplier = 1.25;
   beamColor           = "1 1 1"; //0.1 1 0.1";
   fadeTime            = 1.0; 

   startBeamWidth		  = 0.40; //0.05
   endBeamWidth 	     = 0.40; //0.05
   pulseBeamWidth 	  = 0.025;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 3.0;
   pulseLength         = 0.4;

   lightRadius         = 5.0;
   lightColor          = "1 1 1"; //0.1 1 0.1";

   textureName[0]   = "special/flare";
  textureName[1]   = "special/nonlingradient";
  textureName[2]   = "special/laserrip01";
  textureName[3]   = "special/laserrip02";
  textureName[4]   = "special/laserrip03";
  textureName[5]   = "special/laserrip04";
  textureName[6]   = "special/laserrip05";
  textureName[7]   = "special/laserrip06";
  textureName[8]   = "special/laserrip07";
  textureName[9]   = "special/laserrip08";
  textureName[10]   = "special/laserrip09";
  textureName[11]   = "special/shocklance_effect02";

   //textures[0] = "special/ELFBeam";    //changed this solely for gat PBW
   //textures[1] = "special/ELFLightning";
   //textureName[0]      = "special/ELFBeam";
   //textureName[1]      = "special/ELFLightning";
};

datablock TargetProjectileData(TPBWLaserProj)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;
   useInvAlpha          = true;
   maxRifleRange       	= 400;	// 300; -soph
   beamColor           	= "0.2 0.3 0.9";

   startBeamWidth			= 0.40;
   pulseBeamWidth 	   = 0.85;
   beamFlareAngle 	   = 30.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 8.0;
   pulseLength         	= 0.15;

   textureName[0]      	= "special/expFlare";
   textureName[1]      	= "special/ELFBeam";
   textureName[2]      	= "special/skyLightning";
   textureName[3]      	= "special/BlueImpact";
};

function TParticleBeam::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %shooter = %projectile.sourceObject;										// +[soph]
   %mount = %shooter.getObjectMount() ;										// +
   if( !%mount )												// +
      %mount = %shooter ;											// +
   if( %mount == %targetObject )										// +
      return ;													// +
   if( %mount == %targetObject.getObjectMount() )								// +
   {														// +
      %p = new ( %data.projectileType )() {									// +
         dataBlock        = "TParticleBeam" ;									// +
         initialDirection = %projectile.initialDirection ;							// +
         initialPosition  = vectorAdd( %position , %projectile.initialDirection ) ;				// +
         sourceObject     = %projectile.sourceObject ;								// +
         damageFactor     = %projectile.damageFactor ;								// +
         damageForce      = %projectile.damageForce ;								// +
         sourceSlot       = %projectile.sourceSlot ;								// +
      };													// +
      %p.sourceObject.lastProjectile = %p ;									// +
      MissionCleanup.add( %p ) ;										// +
      if( %p.sourceObject.client )										// +
         %p.sourceObject.client.projectile = %p ;								// +
      return ;													// +
   }														// +[/soph]

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

  // checkMAHit(%projectile, %targetObject, %position);								// -soph

   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);
      
//   %dist = getObjectDistance(%projectile.sourceObject, %targetObject);
//   %pct = 1 - (%dist / $PBWDamageDistance);

   %damageAmount = %projectile.damageFactor;

   %dist = vectorDist( %projectile.initialPosition , %position ) ;						// +[soph]
   %falloff = 50;												// + 67 down from 100 // from standard pbw +50
   %minMod = 0.01;												// +
														// +
   if(%dist > %falloff)												// +
   {														// +
     %mod = 1 - ((%dist - %falloff) / (%data.maxRifleRange - %falloff));					// +
     %mod = %mod < %minMod ? %minMod : %mod;									// +
     %damageAmount *= %mod;											// +
   }														// +[/soph]

   if(%targetObject.getType() & $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType )			// && (%targetObject.team == %projectile.sourceObject.team)) -soph
   {
      %force = %projectile.damageForce;
      if( %targetObject.getType() & $TypeMasks::VehicleObjectType )						// +[soph]
      {														// +
         if( !%targetObject.inertialDampener )									// +
            %force /= 6;											// +
         zapVehicle( %targetObject , "BasicShocker" );								// +
      }														// +
      else													// +
         %targetObject.zapObject();										// +[/soph]
      %p = %targetObject.getWorldBoxCenter();
      %momVec = VectorSub(%p, %position);
      %momVec = VectorNormalize(%momVec);
     // %momVec = VectorNormalize( %shooter.getMuzzleVector( %projectile.sourceSlot ) );			//  saving for if it needs a buff -soph
      %impulseVec = VectorScale(%momVec, %force);
      %targetObject.applyImpulse(%position, %impulseVec);
     // %targetObject.zapObject();										// -soph
   }
		
   if( %targetObject.PBWBuildup[%shooter] )									// +[soph]
      %targetObject.PBWBuildup[%shooter] += %damageAmount;
   else
      %targetObject.PBWBuildup[%shooter] = %damageAmount;
   %targetObject.setDamageFlash( %targetObject.PBWBuildup[%shooter] / 4 );

   if( %targetObject.PBWHitThread[%shooter] )
      cancel( %targetObject.PBWHitThread[%shooter] );
   else
   {
      %p = new LinearFlareProjectile()
      {
         dataBlock        = PBWImpactProj;
         initialDirection = %projectile.sourceObject.getMuzzleVector(4);
         initialPosition  = %position;
         sourceObject     = %projectile.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
   }
   %targetObject.PBWHitThread[%shooter] = schedule( 65 , 0 , PBWResolveDamage , %data , %projectile , %targetObject , %position , %targetObject.PBWBuildup[%shooter] , %shooter );
														// +[/soph]
  // if(%targetObject.isFF)											// -[soph]
  //    %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount, %data.directDamageType);
  // else
  //    %targetObject.damage(%projectile.sourceObject, %position, %damageAmount, %data.directDamageType);	// -[/soph]
}

datablock ItemData(PBW)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_missile.dts";
   image        = PBWImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "a particle beam weapon (PBW)";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(PBWAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some PBW power cells";
};

datablock ShapeBaseImageData(PBWImage)
{
	className = WeaponImage;
   shapeFile = "weapon_missile.dts";
   item = PBW;
   ammo = PBWAmmo;
   projectileType = SniperProjectile;
   offset = "0 0.65 0";
   muzzleFlash = PBWFireShockwave;

	usesEnergy = true;
	minEnergy = 50;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateSound[0]                    = PBWSwitchSound;
   stateTimeoutValue[0]             = 0.5;
//   stateSequence[0]                 = "Activate";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "CheckWet";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.01 ;		// = 0.25; -soph
   stateFire[3]                     = true;
//   stateRecoil[3]                   = LightRecoil;
   stateShockwave[3] = true;
   stateAllowImageChange[3]         = false;
  // stateSequence[3]                 = "reload";	// -soph
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[ 3 ]  = "LowEnergy" ;	// +soph
//   stateEmitter[3]           = "UnderwaterMineExplosionSmokeEmitter";
//   stateEmitterTime[3]       = 0.24;
//   stateEmitterNode[3]       = 0;

   stateName[4]                     = "Reload";
   stateSequence[ 4 ]               = "reload";		// +soph
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 3.0 ;		// = 2.8; -soph
   stateAllowImageChange[4]         = false;

   stateName[5]                     = "CheckWet";
   stateTransitionOnWet[5]          = "DryFire";
   stateTransitionOnNotWet[5]       = "Fire";

   stateName[6]                     = "NoAmmo";
   stateTransitionOnAmmo[6]         = "Reload";
   stateTransitionOnTriggerDown[6]  = "DryFire";
   stateSequence[6]                 = "NoAmmo";

   stateName[7]                     = "DryFire";
   stateSound[7]                    = ShockLanceDryFireSound;
   stateTimeoutValue[7]             = 0.5;
   stateTransitionOnTimeout[7]      = "Ready";

   stateName[ 8 ]                   = "LowEnergy" ;	// +[soph]
   stateSound[ 8 ]                  = ShockLanceDryFireSound ;
   stateAllowImageChange[ 8 ]       = false ;
   stateTimeoutValue[ 8 ]           = 1.0 ;
   stateTransitionOnTimeout[ 8 ]    = "Ready" ;		// +[/soph]
};

function deletePBWFireTimeout(%obj)
{
    %obj.PBWFireTimeout = false;
}

function PBWImage::onFire(%data,%obj,%slot)
{
   %weapon = %obj.getMountedImage(0).item;
   if(%obj.client.mode[%weapon] == 0)
   {
       if(%obj.PBWFireTimeout == true)
           return;

       %dmgMod = 1;
       %useEnergyObj = %obj.getObjectMount();
 
       if(!%useEnergyObj)
          %useEnergyObj = %obj;

       %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

       // Vehicle Damage Modifier
       if(%vehicle)
          %dmgMod += %vehicle.damageMod;

       if(%obj.damageMod)
          %dmgMod += %obj.damageMod - 1;

       %enUse = 150;
       
       if(%obj.powerRecirculator)
            %enUse *= 0.75;

       if(%obj.getEnergyLevel() < %enUse || %obj.getInventory(PBWAmmo) < 1)
       {
         serverPlay3D(ShockLanceDryFireSound, %obj.getTransform());
         %obj.setImageTrigger( 0 , false ) ;			// +soph
         return;
       }

//      if(!%obj.hasEnergyPack)
//      {
//         messageClient(%obj.client, 'MsgPBWNeedEnPack', '\c2You need the energy pack to operate the Particle Beam Weapon (PBW).');
//         serverPlay3D(ShockLanceDryFireSound, %obj.getTransform());
//         return;
//      }
      
      //-Nite- Moved %T before %P, might help that help no damage thing
      %t = new TargetProjectile()
      {
             dataBlock        = "PBWLaserProj";
             initialDirection = %obj.getMuzzleVector(%slot);
             initialPosition  = %obj.getMuzzlePoint(%slot);
             sourceObject     = %obj;
             sourceSlot       = %slot;
             vehicleObject    = 0;
      };
      MissionCleanup.add(%t);
      
//      %p = new (%data.projectileType)() {			// -[soph]
//         dataBlock        = "ParticleBeam";			// moving this to looping thread
//         initialDirection = %obj.getMuzzleVector(%slot);
//         initialPosition  = %obj.getMuzzlePoint(%slot);
//         sourceObject     = %obj;
//         damageFactor     = 2.25*%dmgMod;
//         damageForce      = 6750*%dmgMod;
//         sourceSlot       = %slot;
//      };
      //%t was here -Nite-
//      %obj.lastProjectile = %p;
//      MissionCleanup.add(%p);
//      serverPlay3D(underwaterDiscExpSound, %obj.getTransform());


      // AI hook
//      if(%obj.client)
//         %obj.client.projectile = %p;				// -[/soph]

       %obj.PBWFireTimeout = true;
       schedule(3000, 0, "deletePBWFireTimeout", %obj);

//      %obj.setEnergyLevel(0);
      %obj.useEnergy(%enUse);
      %obj.decInventory(PBWAmmo, 1);
      PBWFragment( %data , %obj , %slot , 15 , %dmgmod , %t );	// +soph
//      schedule(350, 0, "killit", %t);				// -soph
   }
}

function PBWFragment( %data , %obj , %slot , %fragments , %dmgmod , %t )	// +[soph]
{										// +
   %p = new (%data.projectileType)() {						// +
      dataBlock        = "ParticleBeam" ;					// +
      initialDirection = %obj.getMuzzleVector( %slot ) ;			// +
      initialPosition  = %obj.getMuzzlePoint( %slot ) ;				// +
      sourceObject     = %obj ;							// +
      damageFactor     = 0.22 * %dmgMod ;					// + 0.275	
      damageForce      = 825 * %dmgMod ;					// + 1000.0
      sourceSlot       = %slot ;						// +
   } ;										// +
										// +
   %obj.lastProjectile = %p ;							// +
   MissionCleanup.add( %p ) ;							// +
   serverPlay3D( underwaterDiscExpSound , %obj.getTransform() ) ;		// +
										// +
   // AI hook									// +
   if(%obj.client)								// +
      %obj.client.projectile = %p ;						// +
   if( %fragments-- > 0 )							// +
      schedule( 30 , 0 , PBWFragment , %data , %obj , %slot , %fragments , %dmgmod , %t );
   else										// +
      %t.delete() ;								// +
}										// +[/soph]

datablock ShapeBaseImageData(PBWDecalAImage)
{
   shapeFile = "weapon_disc.dts";
   offset = "-0.175 0.275 0.35";
   rotation = "0 1 0 225";
   emap = true;
};

datablock ShapeBaseImageData(PBWDecalBImage)
{
   shapeFile = "weapon_disc.dts";
   offset = "-0.175 0.275 0.2";
   rotation = "0 1 0 -45";
   emap = true;
};

function PBWImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(PBWDecalAImage, 4);
   %obj.mountImage(PBWDecalBImage, 5);
}

function PBWImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);

   if(%obj.getRechargeRate() == 0)
   {
      %obj.PBWFireTimeout = false;
      %obj.PBWCharging = false;
      %obj.setRechargeRate(%obj.prePBWChargeRate);
      cancel(%obj.PBWChargeFireThread);
   }
}
