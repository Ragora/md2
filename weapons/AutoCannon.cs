if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// AutoCannon v2

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearProjectileData(ReaverRocket)
{
   scale = "2.0 2.0 2.0";
   doDynamicClientHits = true;
   projectileShapeName = "grenade_projectile.dts";
   emitterDelay        = 250;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.15;
   damageRadius        = 6.0;
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 100;
   bubbleEmitTime      = 1.0;

   explosion           = "ReaverExplosion";
   underwaterExplosion = "UnderwaterReaverExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = PlasmaASMEmitter;
//   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 125;
   wetVelocity       = 75;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500; //1000
   lifetimeMS        = 1500; //1000
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};

datablock LinearFlareProjectileData(ReaverCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 12.5; // 9.5
   radiusDamageType    = $DamageType::Flak;
   kickBackStrength    = 100;

   explosion           = "ReaverExplosion";
   underwaterExplosion = "UnderwaterReaverExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(BADeathCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 12.5; // 9.5
   radiusDamageType    = $DamageType::BAReactor;
   kickBackStrength    = 100;

   explosion           = "ReaverExplosion";
   underwaterExplosion = "UnderwaterReaverExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(SHExpCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 12.5; // 9.5
   radiusDamageType    = $DamageType::StarHammer;
   kickBackStrength    = 100;

   explosion           = "ReaverExplosion";
   underwaterExplosion = "UnderwaterReaverExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock DebrisData(ReaverShellDebris)
{
   shapeName = "weapon_missile_projectile.dts";

   lifetime = 5.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(AutoCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some auto cannon ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------

datablock ShapeBaseImageData(AutoCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item      = AutoCannon;
   ammo 	 = AutoCannonAmmo;
   emap = true;

   casing              = AutoShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 3.0;

   projectileType = TracerProjectile;
   projectile = AutoCannonBullet;

   projectileSpread = 3.0 / 1000.0;
   offset = "0 0.35 0.05";

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = ChaingunSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.875;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AssaultChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "FireSpindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 3.5;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   stateEmitter[6]       = "ChaingunFireSmoke";
   stateEmitterTime[6]       = 0.25;
   stateEmitterNode[6]       = 0;

   //
   stateTimeoutValue[6]        = 3.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";

   //--------------------------------------
   stateName[8]       = "FireSpindown";
   stateSound[8]      = ChaingunSpinDownSound;
   stateSpinThread[8] = SpinDown;
   stateEmitter[8]       = "ChaingunFireSmoke";
   stateEmitterTime[8]       = 0.25;
   stateEmitterNode[8]       = 0;
   //
   stateTimeoutValue[8]            = 3.5;
   stateWaitForTimeout[8]          = false;
   stateTransitionOnTimeout[8]     = "Ready";
   stateTransitionOnTriggerDown[8] = "Spinup";
};

datablock ItemData(AutoCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_chaingun.dts";
   image = AutoCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "an autocannon";
};

datablock ShapeBaseImageData(AutoCannonDecalAImage)
{
   shapeFile = "weapon_mortar.dts";
   offset = "0 -0.2 0.05";
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(AutoCannonDecalBImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0.1 0.525 0.1";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(AutoCannonDecalCImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0.1 0.3 0.1";
   rotation = "1 0 0 90";
};

function AutoCannonImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this,%obj,%slot);

   %obj.mountImage(AutoCannonDecalAImage, 4);
   %obj.mountImage(AutoCannonDecalBImage, 5);
   %obj.mountImage(AutoCannonDecalCImage, 6);
}

function AutoCannonImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

function AutoCannon::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//--------------------------------------------------------------------------
// AutoCannon

function AutoCannonImage::onFire(%data, %obj, %slot)
{
   %weapon = %obj.getMountedImage(0).item;

   if(%obj.client.mode[%weapon] == 0)
   {
      %ammoUse = 1;
      %enUse = 0;
      %projectile = "AutoCannonBullet";
      %projectileType = "TracerProjectile";
      %spread = 3.0 / 1000.0;
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
      %ammoUse = 2;
      %enUse = 0;
      %projectile = "AutoCannonAPBullet";
      %projectileType = "TracerProjectile";
      %spread = 3.0 / 1000.0;
   }
   else if(%obj.client.mode[%weapon] == 2)
   {
      %ammoUse = 3;
      %enUse = 2;
      %projectile = "ReaverRocket";
      %projectileType = "LinearProjectile";
      %spread = 18.0 / 1000.0;
   }

   %ammo = %obj.getInventory(%data.ammo);

   if(%obj.powerRecirculator)
       %enUse *= 0.75;
       
   if(%ammoUse > %ammo || %enUse > %obj.getEnergyLevel())
   {
        %obj.play3D(ChaingunDryFireSound);
        return;
   }

   %mountDamageMod = 0;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %vector = %obj.getMuzzleVector(%slot);

      %x = (getRandom() - 0.5) * 2 * 3.1415926 * %spread;
      %y = (getRandom() - 0.5) * 2 * 3.1415926 * %spread;
      %z = (getRandom() - 0.5) * 2 * 3.1415926 * %spread;
      %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      %vector = MatrixMulVector(%mat, %vector);

      %p = new (%projectileType)()
      {
         dataBlock        = %projectile;
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

//   if(%obj.client.mode[%weapon] == 1)
//         FlareSet.add(%p);	// -soph
        
   if(%obj.client.mode[%weapon] == 2)
        schedule( 50, 0 , rBeginProxy , %p ) ;	// rBeginProxy(%p); -soph

   %obj.useEnergy(%enUse);
   %obj.decInventory(%data.ammo, %ammoUse);
}

function rProxyTrigger(%obj)
{
   if(isObject(%obj))
   {
      %p = new LinearFlareProjectile()
      {
            dataBlock        = ReaverCharge;
            initialDirection = "0 0 0";
            initialPosition  = %obj.getPosition();
            sourceObject     = %obj.sourceObject;
            sourceSlot       = 0;
      };
      MissionCleanup.add(%p);

      %p.vehicleMod = %obj.vehicleMod;

      %obj.delete();
   }
}

function rBeginProxy(%obj)
{
   if( !isObject( %obj.sourceObject ) )			// +[soph]
      if( isObject( %obj.bkSourceObject) )		// +
         %obj.sourceObject = %obj.bkSourceObject ;	// +[/soph]

   if(isObject(%obj))
   {
      %obj.proxyThread = true;

      InitContainerRadiusSearch(%obj.getPosition(), 10, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);   //%obj.getPosition(), 7, -Nite-  reduced search area
      
      while((%int = ContainerSearchNext()) != 0)
      {
        if((%int.team || %int.client.team) != %obj.sourceObject.team) //
        {
          if(%int.getType() & $TypeMasks::ProjectileObjectType)
          {
             if(%int.getDatablock().getName() $= "FlareGrenadeProj")
               rProxyTrigger(%obj);          
          }
          else
          {
            if(%int.getHeat() > 0.5) //-Nite- 0.25 increased heat needed for proxy to go off
               rProxyTrigger(%obj); //-Nite- Damn i hate this gun! but its got cool code in it!
          }
        }
      }

      schedule(100, %obj, "rBeginProxy", %obj);
   }
}
