//--------------------------------------
// Plasma Cannon
//--------------------------------------
//Patch File -Nite-1501
// Increased Damage of Plasma Cannon a Hair..
//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock ParticleData(PlasmaTurretCannonParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.85;

   lifetimeMS           = 1600;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -100.0;
   spinRandomMax = 100.0;

   animateTexture = true;
   framesPerSec = 15;

   animTexName[00]       = "special/Explosion/exp_0002";
   animTexName[01]       = "special/Explosion/exp_0004";
   animTexName[02]       = "special/Explosion/exp_0006";
   animTexName[03]       = "special/Explosion/exp_0008";
   animTexName[04]       = "special/Explosion/exp_0010";
   animTexName[05]       = "special/Explosion/exp_0012";
   animTexName[06]       = "special/Explosion/exp_0014";
   animTexName[07]       = "special/Explosion/exp_0016";
   animTexName[08]       = "special/Explosion/exp_0018";
   animTexName[09]       = "special/Explosion/exp_0020";
   animTexName[10]       = "special/Explosion/exp_0022";
   animTexName[11]       = "special/Explosion/exp_0024";
   animTexName[12]       = "special/Explosion/exp_0026";
   animTexName[13]       = "special/Explosion/exp_0028";
   animTexName[14]       = "special/Explosion/exp_0030";
   animTexName[15]       = "special/Explosion/exp_0032";
   animTexName[16]       = "special/Explosion/exp_0034";
   animTexName[17]       = "special/Explosion/exp_0036";
   animTexName[18]       = "special/Explosion/exp_0038";
   animTexName[19]       = "special/Explosion/exp_0040";
   animTexName[20]       = "special/Explosion/exp_0042";
   animTexName[21]       = "special/Explosion/exp_0044";
   animTexName[22]       = "special/Explosion/exp_0046";
   animTexName[23]       = "special/Explosion/exp_0048";
   animTexName[24]       = "special/Explosion/exp_0050";
   animTexName[25]       = "special/Explosion/exp_0052";

   colors[0]     = "0.5 0.7 1.0 1.0";
   colors[1]     = "0.2 0.5 1.0 1.0";
   colors[2]     = "0.1 0.25 1.0 0.0";
   sizes[0]      = 10.0;
   sizes[1]      = 4.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlasmaTurretCannonEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "PlasmaTurretCannonParticle";
};

//------------------------------------------------------------------------------
// Projectile Datablocks

datablock LinearFlareProjectileData(PlasmaCannonBolt)
{
  // doDynamicClientHits = true;

   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.75;
   damageRadius        = 7;
   kickBackStrength    = 2500;
   radiusDamageType    = $DamageType::PlasmaCannon;
   explosion           = PlasmaBarrelBoltExplosion;
   splash              = PlasmaSplash;

   dryVelocity       = 200;
   wetVelocity       = 5;		// -1; -soph
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2592;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0;	// 45; -soph
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 250;	// 4000; -soph

   activateDelayMS = -1;

   baseEmitter         = DiscMistEmitter;
   
   scale             = "1.5 1.5 1.5";
   numFlares         = 30;
   flareColor        = "0.1 0.3 1.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function PlasmaCannonBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
//     if(%targetObject.getType() & $TypeMasks::PlayerObjectType)		// -[soph]
//          if(!%targetObject.inDEDField && %targetObject.team != %projectile.sourceObject.team)
//               if(getRandom() < 0.25)
//                    %targetObject.burnObject(%projectile.sourceObject);	// -[/soph]

     Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

function PlasmaCannonBolt::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{
     Parent::onExplode( %data , %proj , %pos , %mod ) ;
     IncendiaryExplosion( %pos , %data.damageradius , 1.25 , %proj.sourceObject , true );
}								// +[/soph]

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(PlasmaCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_fusion_large.dts";
   item = PlasmaCannon;

   projectile = PlasmaCannonBolt;
   projectileType = LinearFlareProjectile;
   usesEnergy = true;
   fireEnergy = 25;
   minEnergy = 25;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
//   stateSequence[0] = "Deploy";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateSequence[2] = "Deploy";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 1.0;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
//   stateSound[3] = PCFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

datablock ItemData(PlasmaCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_energy.dts";
   image = PlasmaCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a plasma cannon";
};

function PlasmaCannonImage::onFire(%data, %obj, %slot)
{
   %p = Parent::onFire(%data, %obj, %slot);

  if(%p)
      %obj.play3D(PBLFireSound);
}
