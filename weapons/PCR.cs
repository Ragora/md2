if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Phaser Compression Rifle
//--------------------------------------

datablock LinearFlareProjectileData(PCRCannonBolt)
{
   scale = "2 2 2";
   faceViewer          = true;
   directDamage        = 0.16;     //  .15
   directDamageType    = $DamageType::PhaserCannon;
   hasDamageRadius     = true;
   indirectDamage      = 0.16;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::PhaserCannon;

   kickBackStrength    = 0.0;

   explosion           = "PhaserCExplosion";
   underwaterExplosion = "UnderwaterPhaserCExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 233;    //225
   wetVelocity       = 150;   //200
   velInheritFactor  = 0.8;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "0.2 1.0 0.2";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.2 1.0 0.2";
};

datablock LinearFlareProjectileData(PCRCannonBoltT)
{
   scale               = "2 0.75 0.75" ;	// "2 2 2"; -soph
   faceViewer          = true;
   directDamage        = 0.315;     //  .15
   directDamageType    = $DamageType::Phaser;
   hasDamageRadius     = true;
   indirectDamage      = 0.315;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Phaser;

   kickBackStrength    = 0.0;

   explosion           = "PhaserCExplosion";
   underwaterExplosion = "UnderwaterPhaserCExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 150;    //225
   wetVelocity       = 100;   //200
   velInheritFactor  = 0.8;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "0.2 1.0 0.2";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.2 1.0 0.2";
};

datablock LinearFlareProjectileData(PBCannonBolt)
{
   scale = "3 3 3";
   faceViewer          = true;
   directDamage        = 0.75;	// = 0.6; -soph
   directDamageType    = $DamageType::PhaserCannon;
   hasDamageRadius     = true;
   indirectDamage      = 0.75;	// = 1.0; -soph
   damageRadius        = 13.0;
   radiusDamageType    = $DamageType::PhaserCannon;

   kickBackStrength    = 0.0;

   explosion           = "PhaserCExplosion";
   underwaterExplosion = "UnderwaterPhaserCExplosion";
   splash              = PlasmaSplash;

   baseEmitter         = WadGreenTrailEmitter;

   dryVelocity       = 233;    //225
   wetVelocity       = 150;   //200
   velInheritFactor  = 0.8;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "0.2 1.0 0.2";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.2 1.0 0.2";
};

datablock LinearFlareProjectileData(PBCCannonBolt)
{
   scale = "3 3 3";
   faceViewer          = true;
   directDamage        = 0.0;	// = 0.01; -soph 
   directDamageType    = $DamageType::PhaserCannon;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 8.0;
   radiusDamageType    = $DamageType::PhaserCannon;

   kickBackStrength    = 0.0;

   explosion           = "PhaserCExplosion";
   underwaterExplosion = "UnderwaterPhaserCExplosion";
   splash              = PlasmaSplash;

   baseEmitter         = WadGreenTrailEmitter;

   dryVelocity       = 400;    //225
   wetVelocity       = 250;   //200
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "0.2 1.0 0.2";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.2 1.0 0.2";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(PCRAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "beacon.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "an elerium battery";

   emap = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(PCR)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_elf.dts";
   image = PCRImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a phaser compression rifle";
};

datablock ShapeBaseImageData(PCRImage)
{
   className = WeaponImage;
   item = PCR;
   ammo = PCRAmmo;
   emap = true;
   shapeFile = "weapon_shocklance.dts";
   offset = "-0.025 0.4 -0.15";
   
   isCapWeapon = true;
   
//   projectile = PCRBolt;
//   projectileType = TracerProjectile;
//   projectileSpread = 6 / 998;
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = GrenadeSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.1;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateScript[3]                   = "onFire";
//   stateSound[3] = ;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.25;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
//   stateSound[6]      = ;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";

   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire";
};

datablock ShapeBaseImageData(PCRDecalAImage)
{
   shapeFile = "weapon_elf.dts";
   offset = "-0.05 0.1 -0.1";
   emap = true;
};

datablock ShapeBaseImageData(PCRDecalBImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0.1 0.5 0";
   rotation = "0 1 0 90";
   emap = true;
};

datablock ShapeBaseImageData(PCRDecalCImage)
{
   shapeFile = "weapon_elf.dts";
   rotation = "0 1 0 180";
   offset = "0 0.1 0.1";
   emap = true;
};

function PCRImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(PCRDecalAImage, 4);
   %obj.mountImage(PCRDecalBImage, 5);
   %obj.mountImage(PCRDecalCImage, 6);
}

function PCRImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

function PCRImage::onFire(%data, %obj, %slot)
{
   %vehicle = 0;
   %weapon = %obj.getMountedImage(0).item;

   if(%obj.client.mode[%weapon] == 0)
   {
      %projectile = "PCRBolt";
      %mode = "TracerProjectile";
      %ammoUse = 1;
      %spread = 6 / 998;
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
      %projectile = "PCRCannonBolt";
      %mode = "LinearFlareProjectile";
      %ammoUse = 3;
      %spread = 0;
   }

   if(%ammoUse > %obj.getInventory(%data.ammo))
   {
       %obj.play3D(GrenadeDryFireSound);
       return;
   }

   %mountDamageMod = 0;
   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;
   %vector = %obj.getMuzzleVector(%slot);

   if(%spread)
   {
      %x = (getRandom() - 0.5) * 2 * 3.1415926 * %spread;
      %y = (getRandom() - 0.5) * 2 * 3.1415926 * %spread;
      %z = (getRandom() - 0.5) * 2 * 3.1415926 * %spread;
      %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      %vector = MatrixMulVector(%mat, %vector);

      %p = new (%mode)() {
         dataBlock        = %projectile;
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };
   }
   else
   {
      %p = new (%mode)() {
         dataBlock        = %projectile;
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };
   }

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %p.damageMod = 1.0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;

   if(%data.isCapWeapon && %obj.synMod)
     %p.damageMod += %obj.synMod * %obj.isSyn;

   %obj.decInventory(%data.ammo, %ammoUse);
   %obj.play3D(PCRFireSound);

   if(!%obj.chargingPCR)
      ChargePhaser(%obj, %data);
}

function ChargePhaser(%player, %datablock)       // should work....
{
   %ammo = %player.getInventory(PCRAmmo);
   %aPack = %player.hasAmmoPack ? 25 : 0;
   %aPack = %player.getDatablock().isEngineer ? 10 : %aPack;	// Engineer exception +soph   
   %ammoMax = %player.getDatablock().max[PCRAmmo] + %aPack;

   if(%ammo > -1)   // jury rig for now
   {
      if(%ammo < %ammoMax)
      {
         if(!%player.isEMP)
         {
//            %pct = %player.getEnergyPct();			// -soph

//            if(%pct >= 0.25)
               %player.incInventory(PCRAmmo, 1);

            %player.chargingPCR = true;
         }
//         else
//            %player.setInventory(PCRAmmo, 0);


         if(%pct == 1)
         {
            if(%player.isSyn)
               %mult = 500; //250
            else
               %mult = 1000; //500
         }
         else
         {
            if(%player.isSyn)
               %mult = 1000; //500
            else
               %mult = 2000; //1000
         }

         schedule(%mult, %player, "ChargePhaser", %player, %datablock);
      }
      else
         %player.chargingPCR = false;
   }
}
