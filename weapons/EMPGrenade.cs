// ------------------------------------------------------------------------
// EMP grenade (thrown by hand) script
// ------------------------------------------------------------------------

datablock ItemData(EMPGrenadeThrown)
{
	className = Weapon;
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
	explosion = ConcMortarExplosion; // MrKeen
	underwaterExplosion = UnderwaterHandGrenadeExplosion;
   indirectDamage      = 1.25;	// = 0.001; -soph
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 100;
};

datablock ItemData(EMPGrenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = EMPGrenadeThrown;
	pickUpName = "some emp grenades";
	isGrenade = true;
};
