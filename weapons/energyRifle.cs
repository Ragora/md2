if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Energy Rifle
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------

//datablock AudioProfile(SolPisDryFireSound)	// -[soph]
//{						// redundant with ShrikeBlasterDryFireSound
//   filename    = "fx/weapons/chaingun_dryfire.wav";
//   description = AudioClose3d;
//   preload = true;
//};						// -[/soph]

datablock AudioProfile(SolPisFireSound)
{
   filename    = "fx/weapons/plasma_rifle_projectile_hit.wav";
   description = AudioClose3d;
   preload = true;
};

//----------------------------------------------------
//  Explosion
//----------------------------------------------------

datablock ParticleData(UnderwaterEnergyRifleExplosionSmoke)
{
   dragCoeffiecient     = 80.0;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.025;

   constantAcceleration = -0.75;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.2 0.9 0.1 1.0";
   colors[1]     = "0.2 1.0 0.1 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterEnergyRifleExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 5.25;
   velocityVariance = 0.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "UnderwaterEnergyRifleExplosionSmoke";
};

datablock ParticleData(UnderwaterEnergyRifleSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/droplet";
   colors[0]     = "0.9 0.2 1.0 1.0";
   colors[1]     = "0.9 0.2 1.0 1.0";
   colors[2]     = "0.9 0.2 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.25;
   sizes[2]      = 0.25;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterEnergyRifleSparkEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 6.75;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterEnergyRifleSparks";
};

datablock ExplosionData(UnderwaterEnergyRifleSubExplosion1)
{
   offset = 1.0;
   emitter[0] = UnderwaterEnergyRifleExplosionSmokeEmitter;
   emitter[1] = UnderwaterEnergyRifleSparkEmitter;
};

datablock ExplosionData(UnderwaterEnergyRifleSubExplosion2)
{
   offset = 1.0;
   emitter[0] = UnderwaterEnergyRifleExplosionSmokeEmitter;
   emitter[1] = UnderwaterEnergyRifleSparkEmitter;
};

datablock ExplosionData(UnderwaterEnergyRifleExplosion)
{
   soundProfile   = GrenadeExplosionSound;

   emitter[0] = UnderwaterEnergyRifleExplosionSmokeEmitter;
   emitter[1] = UnderwaterEnergyRifleSparkEmitter;

   subExplosion[0] = UnderwaterEnergyRifleSubExplosion1;
   subExplosion[1] = UnderwaterEnergyRifleSubExplosion2;

   shakeCamera = false;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 1.0;
   camShakeRadius = 15.0;
};

datablock ParticleData( EnergyDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.5 0.2 0.1 0.8";
   colors[2]     = "0.3 0.1 0.05 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( EnergyDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "EnergyDebrisSmokeParticle";
};

datablock DebrisData(EnergyDebris)
{
   emitters[0] = EnergyDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 1;
};

datablock ParticleData(EnergyRifleExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_001";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 1.000000";
    colors[1] = "0.700787 0.192000 0.088000 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(EnergyRifleExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 8.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "EnergyRifleExplosionParticle";
};

datablock ParticleData(EnergySparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "1.000000 0.253749 0.253534 1.000000";
    colors[1] = "1.000000 0.253749 0.253534 0.524231";
    colors[2] = "1.000000 0.253749 0.253534 0.000000";
    sizes[0] = 0.733871;
    sizes[1] = 0.733871;
    sizes[2] = 0.75;
};

datablock ParticleEmitterData(EnergySparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 18;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 250;
    particles = "EnergySparksParticle";
};

datablock ExplosionData(EnergyRifleExplosion)
{
   soundProfile   = GrenadeExplosionSound;

   emitter[0] = EnergyRifleExplosionEmitter;
   emitter[1] = EnergySparksEmitter;

   debris = EnergyDebris;
   debrisThetaMin = 25;
   debrisThetaMax = 80;
   debrisNum = 6;
   debrisVelocity = 14.0;
   debrisVelocityVariance = 3.0;

   shakeCamera = false;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 1.0;
   camShakeRadius = 15.0;
};

datablock ParticleData(NRStreakParticle)
{
   dragCoefficient      = 0.375;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0;
//   constantAcceleration = 0.1;
   lifetimeMS           = 2500;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0 0 0 0";
   colors[1]     = "1 0.3 0.1 0.675";
   colors[2]     = "0 0 0 0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.575;
   sizes[2]      = 0.55;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(NRStreakEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "NRStreakParticle";
};

datablock ParticleData(NSStreakParticle)
{
   dragCoefficient      = 0.375;
   gravityCoefficient   = -0.01;
   inheritedVelFactor   = 0;
//   constantAcceleration = 0.1;
   lifetimeMS           = 2500;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0 0 0 0";
   colors[1]     = "0.3 1.0 0.1 0.675";
   colors[2]     = "0 0 0 0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.575;
   sizes[2]      = 0.55;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(NSStreakEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "NSStreakParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock LinearProjectileData(EnRifleBlast)
{
   scale = "1.5 2.0 1.5";
//   doDynamicClientHits = false;
   projectileShapeName = "energy_bolt.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::EnergyBlast;
   kickBackStrength    = 1750;

   sound 				= discProjectileSound;
   explosion           = "EnergyRifleExplosion";
   underwaterExplosion = "UnderwaterEnergyRifleExplosion";
   splash              = DiscSplash;
   baseEmitter         = NRStreakEmitter;

   dryVelocity       = 180;  // -nite- was 150
   wetVelocity       = 180 ;	// 150; consistency -soph
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 2500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 90.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "1.0 0.3 0.3";
};

datablock LinearProjectileData(TEnRifleBlast)
{
   scale = "1.5 2.0 1.5";
//   doDynamicClientHits = false;
   projectileShapeName = "chaingun_shot.dts";
   emitterDelay        = -1;
   directDamage        = 0.01;
   hasDamageRadius     = true;
   indirectDamage      = 0.1;
   damageRadius        = 7.5;
   radiusDamageType    = $DamageType::EnergyBlast;
   kickBackStrength    = 6500;	// 4000; its so weak already, might as well have ungodly kick -soph

   sound 				= discProjectileSound;
   explosion           = "UnderwaterEnergyRifleExplosion";
//   underwaterExplosion = "UnderwaterEnergyRifleExplosion";
   splash              = DiscSplash;
   baseEmitter         = NSStreakEmitter;

   dryVelocity       = 200;
   wetVelocity       = 150;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 90.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "1.0 0.3 0.3";
};

function TEnRifleBlast::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)	// +[soph]
{
     Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

     if( %targetObject.getType() & $TypeMasks::PlayerObjectType && !%targetObject.dead && !%targetObject.isShielded )	
     {													
          Game.schedule( 10 , applyConcussion , %targetObject );					// flag kick with delay so impulse doesn't hit ejected flag
          if( getRandom() > 0.33 )
               if( !%targetObject.isMounted() )
               {
                    %targetObject.setImageTrigger( 0, true );						// misfire
                    if( %targetObject.getWhiteOut() < 0.15 )
                         %targetObject.setWhiteOut( 0.15 );
               }
          %targetObject.setImageTrigger( 0, false );							// screws with trigger-hold weapon
     }
}													// +[/soph]

datablock LinearProjectileData(EnSpreadBlast)
{
   scale = "1.5 2.0 1.5";
//   doDynamicClientHits = false;
   projectileShapeName = "chaingun_shot.dts";
   emitterDelay        = -1;
   directDamage        = 0.00;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::EnergyBlast;
   kickBackStrength    = 1750;

   sound 				= discProjectileSound;
   explosion           = "EnergyRifleExplosion";	// explosion           = "UnderwaterEnergyRifleExplosion"; -soph
   underwaterExplosion = "UnderwaterEnergyRifleExplosion";	// -soph
   splash              = DiscSplash;
   baseEmitter         = NRStreakEmitter;	// baseEmitter         = NSStreakEmitter; -soph

   dryVelocity       = 150;
   wetVelocity       = 150;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 2500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 90.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "1.0 0.3 0.3";	// lightColor  = "0.3 1.0 0.1"; -soph
};

datablock DebrisData(EnRifleDebris)
{
   shapeName = "beacon.dts";

   lifetime = 3.5;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.75;
   friction = 0.1;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(EnergyRifleAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some energy rifle cartridges";
   emap = true;
};

datablock ShapeBaseImageData(EnergyRifleImage)
{
   className = WeaponImage;
   shapeFile = "weapon_missile.dts";
   item = EnergyRifle;
   ammo = EnergyRifleAmmo;
   offset = "0.375 0.7 -0.1";
   emap = true;

   casing              = EnRifleDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 12.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";
   stateSound[0] = GrenadeReloadSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.75;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Recoil";
   stateScript[3] = "onFire";
   stateSound[3] = SolPisFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.25;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;
   stateEjectShell[4]       = true;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = ShrikeBlasterDryFireSound ;	// SolPisDryFireSound; -soph
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ItemData(EnergyRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_grenade_launcher.dts";
   image = EnergyRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "an energy rifle";
};

datablock ShapeBaseImageData(ERDecalAImage)
{
//   shapeFile = "weapon_sniper.dts";
//   offset = "0 0.2 0.1";
//   rotation = "0 1 0 45";
//   emap = true;

//   stateName[0] = "Idle";
//	stateSequence[0] = "ambient";

   shapeFile = "weapon_mortar.dts";
   offset = "0.2 0.6 0.2";
   rotation = "1 0 0 180";
   emap = true;
};

datablock ShapeBaseImageData(ERDecalBImage)
{
//   shapeFile = "weapon_sniper.dts";
//   offset = "0.15 0.2 0.1";
//   rotation = "0 1 0 -45";
//   emap = true;

//   stateName[0] = "Idle";
//	stateSequence[0] = "ambient";

   shapeFile = "weapon_elf.dts";
   offset = "0.25 0.2 0.05";
   rotation = "0 1 0 0";
   emap = true;
};

datablock ShapeBaseImageData(ERDecalCImage)
{
//   shapeFile = "weapon_sniper.dts";
//   offset = "0.15 0.2 -0.1"; //.075";
//   rotation = "0 1 0 -135";
//   emap = true;

//   stateName[0] = "Idle";
//	stateSequence[0] = "ambient";
   shapeFile = "weapon_elf.dts";
   rotation = "0 1 0 180";
   offset = "0.3 0.2 0.25";
   emap = true;
};

function EnergyRifleImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
//   %obj.mountImage(ERDecalXImage, 1);
//   %obj.mountImage(ERDecalAImage, 1);
   %obj.mountImage(ERDecalAImage, 4);
   %obj.mountImage(ERDecalBImage, 5);
   %obj.mountImage(ERDecalCImage, 6);

//   %obj.createSlotExtension();
//   %obj.slotExtension[0].mountImage(RAXXDecalAImage, 0);
//   %obj.slotExtension[0].mountImage(RAXXDecalBImage, 1);
//   %obj.slotExtension[0].mountImage(RAXXDecalCImage, 2);
}

function EnergyRifleImage::onUnmount(%this,%obj,%slot)
{
//   Parent::onUnmount(%this, %obj, %slot);
//   %obj.unmountImage(1);
//   %obj.unmountImage(1);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);

//   %obj.deleteSlotExtension();
}
//-Nite-1501 tweaked ammo usage for 3 bolts fire
function EnergyRifleImage::onFire(%data, %obj, %slot)
{
//   %obj.setImageTrigger(1, true);
   %data.lightStart = getSimTime();

//   if( %obj.station $= "" && %obj.isCloaked() )
//   {
//      if( %obj.respawnCloakThread !$= "" )
//      {
//         Cancel(%obj.respawnCloakThread);
//         %obj.setCloaked( false );
//         %obj.respawnCloakThread = "";
//      }
//      else
//      {
//         if( %obj.getEnergyLevel() > 20 )
//         {
//            %obj.setCloaked( false );
//            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
//         }
//      }
//   }
   %vehicle = 0;
         %weapon = %obj.getMountedImage(0).item;
         if(%obj.client.mode[%weapon] == 0)
               %projectile = "EnRifleBlast";
         else if(%obj.client.mode[%weapon] == 1)
               %projectile = "EnSpreadBlast";
         else if(%obj.client.mode[%weapon] == 2)
               %projectile = "EnRifleBurst";

      if(%obj.client.mode[%weapon] == 1)
           %ammoUse = 3;
      else
           %ammoUse = 1;

     if(%ammoUse > %obj.getInventory(%data.ammo))
          return;

     if(%obj.client.mode[%weapon] == 0 || %obj.client.mode[%weapon] == 2)
     {
         %p = new LinearProjectile() {
            dataBlock        = %projectile;
            initialDirection = %obj.getMuzzleVector(%slot);
            initialPosition  = %obj.getMuzzlePoint(%slot);
            sourceObject     = %obj;
            sourceSlot       = %slot;
            vehicleObject    = %vehicle;
         };
         MissionCleanup.add(%p);

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
         %obj.lastProjectile = %p;
     }
     else
     {
        for(%i = 0; %i < 3; %i++)
         {
            %vector = %obj.getMuzzleVector(%slot);
            %x = (getRandom() - 0.5) * 2 * 3.1415926 * (14.5 / 1000);
            %y = (getRandom() - 0.5) * 2 * 3.1415926 * (14.5 / 1000);
            %z = (getRandom() - 0.5) * 2 * 3.1415926 * (14.5 / 1000);
            %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
            %vector = MatrixMulVector(%mat, %vector);

            %p = new LinearProjectile() {
               dataBlock        = %projectile;
               initialDirection = %vector;
               initialPosition  = %obj.getMuzzlePoint(%slot);
               sourceObject     = %obj;
               sourceSlot       = %slot;
               vehicleObject    = %vehicle;
            };
            MissionCleanup.add(%p);
            %obj.lastProjectile = %p;
         }
         
            if(!%useEnergyObj)
        %useEnergyObj = %obj;
        else
             %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

        // Vehicle Damage Modifier
        if(%vehicle)
          %p.vehicleMod = %vehicle.damageMod;

        if(%obj.damageMod)
          %p.damageMod = %obj.damageMod;
     }

      %q = new ShockLanceProjectile() {
         dataBlock        = FireLanceBolt;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };
      MissionCleanup.add(%q);
   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

    %obj.decInventory(%data.ammo,%ammoUse);
}
