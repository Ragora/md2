if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Multifusor
//--------------------------------------

datablock AudioProfile(MultifusorFireSound)
{
   filename    = "fx/weapons/TR2spinfusor_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Weapon and item
//--------------------------------------

datablock ShapeBaseImageData(Multifusor1Image)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   item = Multifusor;
   ammo = DiscAmmo;
   emap = true;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.25;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = true;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = MultifusorFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(Multifusor2Image)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   item = Multifusor;
   ammo = DiscAmmo;
   rotation = "0 1 0 90";
   emap = true;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.25;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = true;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = MultifusorFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(Multifusor3Image)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   item = Multifusor;
   ammo = DiscAmmo;
   rotation = "0 1 0 180";
   emap = true;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.25;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = true;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = MultifusorFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(Multifusor4Image)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   item = Multifusor;
   ammo = DiscAmmo;
   rotation = "0 1 0 270";
   emap = true;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.25;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = true;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = MultifusorFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(MultifusorImage)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   item = Multifusor;
   ammo = DiscAmmo;
   emap = true;

//   usesEnergy = true;
//   minEnergy = -1;

   projectileSpread = 12.0 / 1000.0;
   
   stateName[0]                     = "Activate";
	stateSound[0]                    = DiscSwitchSound;
   stateTimeoutValue[0]             = 0.01;
   stateTransitionOnTimeout[0]      = "Ready";

   stateName[1]                     = "Ready";
   stateTransitionOnTriggerDown[1]  = "Fire";

   stateName[2]                     = "Fire";
   stateFire[2]                     = true;
   stateAllowImageChange[2]         = false;
   stateScript[2]                   = "onFire";
   stateTransitionOnTriggerUp[2]    = "Deconstruction";

   stateName[3]                     = "Deconstruction";
   stateScript[3]                   = "onRelease";
   stateTimeoutValue[3]             = 0.01;
   stateTransitionOnTimeout[3]      = "Ready";
};

datablock ItemData(Multifusor)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_disc.dts";
   image = MultifusorImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   pickUpName = "a multifusor";
   emap = true;
};

function getNextMultiFusorFire(%obj)
{
      if(%obj.MultiFusorFire == 0)
         return 4;
      else if(%obj.MultiFusorFire == 4)
         return 5;
      else if(%obj.MultiFusorFire == 5)
         return 6;
      else if(%obj.MultiFusorFire == 6)
         return 0;
      else
         return 0;
}

function MultifusorImage::onMount(%this,%obj,%slot)
{
      Parent::onMount(%this, %obj, %slot);

      if(!%obj.MultiFusorFire)
         %obj.MultiFusorFire = 0;

//         %obj.mountImage(Multifusor1Image, 1);
         %obj.mountImage(Multifusor2Image, 4);
         %obj.mountImage(Multifusor3Image, 5);
         %obj.mountImage(Multifusor4Image, 6);

      serverPlay3D(DiscDryFireSound, %obj.getTransform());
}

function MultifusorImage::onUnmount(%this,%obj,%slot)
{
      Parent::onUnmount(%this, %obj, %slot);

//      %obj.unmountImage(1);
      %obj.unmountImage(4);
      %obj.unmountImage(5);
      %obj.unmountImage(6);
}

function MFFireTimeoutClear(%obj)
{
   %obj.fireTimeoutMF = 0;
}

function MultiFusorActiveFire(%player, %data)
{
   if(!%player.MultiFusorActive)
      return;

   if(%player.fireTimeoutMF)
      return;

  // if(%player.isMounted())					// -soph
  //    return;							// -soph
								// below replaces above
   if( %player.getMountedImage($WeaponSlot) != %data )		// +soph
      return;							// +soph

   %data.lightStart = getSimTime();
   %player.fireTimeoutMF = 475;

   schedule(%player.fireTimeoutMF, 0, "MFFireTimeoutClear", %player);

   if(%player.getInventory(DiscAmmo) < 1)
   {
      serverPlay3D(DiscDryFireSound, %player.getTransform());

      %player.setImageTrigger(%player.MultiFusorFire, true);
      %player.setImageTrigger(%player.MultiFusorFire, false);

      %player.MultiFusorFire = getNextMultiFusorFire(%player);
      schedule(%player.fireTimeoutMF, 0, "MultiFusorActiveFire", %player);
      return;
   }

   %weapon = %player.getMountedImage(0).item;

   if(%player.client.mode[%weapon] == 0)
   {
       %projectile = "MFDiscProjectile";
       %mode = "LinearProjectile";
   }
   else if(%player.client.mode[%weapon] == 1)
   {
       %projectile = "RipperProjectile";
       %mode = "EnergyProjectile";
   }
        
//   %vector = calcSpreadVector(%player.getMuzzleVector(%player.MultiFusorFire), 6);
   %vector = %player.getMuzzleVector(%player.MultiFusorFire);
   
   %p = new (%mode)()
   {
       dataBlock        = %projectile;
       initialDirection = %vector;
       initialPosition  = %player.getMuzzlePoint(%player.MultiFusorFire);
       sourceObject     = %player;
       sourceSlot       = %player.MultiFusorFire;
       vehicleObject    = 0;
   };
   MissionCleanup.add(%p);

   %useEnergyObj = %player.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %player;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   %player.setImageTrigger(%player.MultiFusorFire, true);
   %player.setImageTrigger(%player.MultiFusorFire, false);

   %player.decInventory(DiscAmmo,1);
   serverPlay3D(MultifusorFireSound, %player.getTransform());
   %player.MultiFusorFire = getNextMultiFusorFire(%player);

   schedule(%player.fireTimeoutMF, 0, "MultiFusorActiveFire", %player , %data );	// data +soph
}

function MultifusorImage::onFire(%data,%obj,%slot)
{
//   if(getSightInfo(%obj, 1, $TypeMasks::ForceFieldObjectType))
//      return;

   if(!%obj.MultiFusorActive || !%obj.MultiFusorActive == true)
   {
      %obj.MultiFusorActive = true;
      MultiFusorActiveFire(%obj, %data);
   }
}

function MultifusorImage::onRelease(%data,%obj,%slot)
{
     %obj.MultiFusorActive = false;
}
