if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Mitzi Blast Cannon II
//--------------------------------------

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(MBExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "1.0 1.0 0.1 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MBExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MBExplosionParticle";
};

datablock ExplosionData(MitziBlastExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = plasmaExpSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MBExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock ParticleData(MBXExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "1.0 0.2 0.1 1.0";
   colors[1]     = "0.8 0.075 0.01 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MBXExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MBXExplosionParticle";
};

datablock ExplosionData(MitziBlastXExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;

   sizes[0] = "3 3 3";
   sizes[1] = "4 4 4";
   sizes[2] = "4 4 4";

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MBXExplosionEmitter;
   particleDensity = 225;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock ExplosionData(UMitziBlastExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = plasmaExpSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MBExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;
   
   bubbleEmitter = "DiscExplosionBubbleEmitter";
};

//--------------------------------------------------------------------------
// Trail
//--------------------------------------

datablock ParticleData(MBTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.5 0.5 0.1 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MBTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MBTrailParticle";
};

datablock ParticleData(MBXTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "1.0 0.2 0.1 1.0";
   colors[1]     = "0.5 0.05 0.01 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MBXTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MBXTrailParticle";
};

datablock ParticleData(MBEMPTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.2 0.6 1.0 1.0" ;		// "0.2 1.0 0.2 1.0"; -soph
   colors[1]     = "0.1 0.3 0.9 0.0" ;		// "0.01 0.9 0.1 0.0"; -soph
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MBEMPTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MBEMPTrailParticle";
};
//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(MitziBlast)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 0.6;
   directDamageType    = $DamageType::MB;
   hasDamageRadius     = true;
   indirectDamage      = 0.45;
   damageRadius        = 14.0;
   kickBackStrength    = 1000;
   radiusDamageType    = $DamageType::MB;

   explosion           = "MitziBlastExplosion";
   underwaterExplosion = "UMitziBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MBTrailEmitter;

   dryVelocity       = 45.0;
   wetVelocity       = 75.0;       // faster uw
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 15000;
   lifetimeMS        = 15000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.8 0.9";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 1 1";
};

datablock LinearFlareProjectileData(MitziEMPBlast)
{
   scale               = "3.0 5.0 3.0";
   faceViewer          = true;
   hasDamageRadius     = true;
   indirectDamage      = 1.5;	// up from 0.9, hope it isn't too much // = 0.1; -soph
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::EMP;

   explosion           = "EMPExplosion";
   underwaterExplosion = "EMPExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MBTrailEmitter;

   dryVelocity       = 75.0;	// = 45.0; -soph
   wetVelocity       = 120.0;	// = 75.0; -soph       // faster uw
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 6000;	// up from 5000 // = 15000; -soph
   lifetimeMS        = 6000;	// up from 5000 // = 15000; -soph
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;	// = 15000; -soph

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "0.2 0.2 1.0" ;	// "0.2 1 0.2"; -soph
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0.2 0.6 1.0" ;	// "0.2 1 0.2"; -soph
};


function MitziEMPBlast::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{									// borrowed from EMP grenade
   if(%proj.bkSourceObject)
      if(isObject(%proj.bkSourceObject))
         %proj.sourceObject = %proj.bkSourceObject;

     %modifier = 1;

   if(%data.hasDamageRadius) // I lost a bomb. Do you have it?
   {
      if(%proj.vehicleMod)
           %modifier *= %proj.vehicleMod;

        if(%proj.damageMod)
          %modifier *= %proj.damageMod;
          
      if(%data.passengerDamageMod && %proj.passengerCount)
          %modifier *= 1 + (%proj.passengerCount * %data.passengerDamageMod);

      EMPBurstExplosion(%proj, %pos, %data.damageRadius, %data.indirectDamage * %modifier, %data.kickBackStrength, %proj.sourceObject, %data.radiusDamageType);
   }
}

datablock LinearFlareProjectileData(MitziBooster)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 0.001;
   hasDamageRadius     = true;
   indirectDamage      = 0.001;
   damageRadius        = 10.0;
   kickBackStrength    = 7500;
   radiusDamageType    = $DamageType::Default;

   explosion           = "ConcussionGrenadeExplosion";
   underwaterExplosion = "ConcussionGrenadeExplosion";
   splash              = PlasmaSplash;

   dryVelocity       = 150.0;
   wetVelocity       = 300.0;       // faster uw
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 5000;
   lifetimeMS        = 5000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 5000;

   activateDelayMS = -1;
   numFlares         = 0;
   flareColor        = "1 0.6 0.7";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0 0 0";
};

function MitziBooster::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
     %type = %targetObject.getType();
     if(%type & $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType)
     {
           if(%type & $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType) /// this is stupid...
               return;
               
           %data = %targetObject.getDataBlock();
           %impulse = %data.kickBackStrength;
           
           if(%impulse && %data.shouldApplyImpulse(%targetObject))
           {
              %p = %targetObject.getWorldBoxCenter();
              %momVec = VectorSub(%p, %position);
              %momVec = VectorNormalize(%momVec);
              %impulseVec = VectorScale(%momVec, %impulse * 4);
              %doImpulse = true;
           }
           else if(%type & $TypeMasks::VehicleObjectType)
           {
              %p = %targetObject.getWorldBoxCenter();
              %momVec = VectorSub(%p, %position);
              %momVec = VectorNormalize(%momVec);
              %impulseVec = VectorScale(%momVec, %impulse * 4);


              if( getWord( %momVec, 2 ) < -0.5 )
                 %momVec = "0 0 1";

              // Add obj's velocity into the momentum vector
     //         %velocity = %targetObject.getVelocity();
              //%momVec = VectorNormalize( vectorAdd( %momVec, %velocity) );
              %doImpulse = true;

              if(%targetObject.usesCPS && %targetObject.getEnergyPct() >= 0.2) // needs at least 20% shields
                 %doImpulse = false;

              if(%targetObject.devshield)
                 %doImpulse = false;
           }
           else if(%targetObject.pickupName) // for objects like packs and weapons
           {
              echo(%targetObject.pickupName);
                 %doImpulse = true;
           }
           else
           {
              %momVec = "0 0 1";
              %doImpulse = false;
           }

               if(%doImpulse)
                    %targetObject.applyImpulse(%position, %impulseVec);
     }
}

datablock LinearFlareProjectileData(MitziX2)
{
   scale               = "6.0 6.0 6.0";
   faceViewer          = true;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 1.05;	// = 0.9; -soph
   damageRadius        = 14.0;
   kickBackStrength    = 2000;
   radiusDamageType    = $DamageType::DenseMitzi;

   explosion           = "MitziBlastXExplosion";
   underwaterExplosion = "UMitziBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MBXTrailEmitter;

   dryVelocity       = 45.0;
   wetVelocity       = 75.0;       // faster uw
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 15000;
   lifetimeMS        = 15000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.1 0.0.075";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 0.1 0.075";
};

datablock ShockwaveData(MBDispSShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ShockwaveData(CCDispSShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 0.8 1.0 0.75";
   colors[1] = "0.3 0.4 1.0 0.5";
   colors[2] = "0.3 0.2 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ShockwaveData(PCDispSShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 1.0 0.6 0.75";
   colors[1] = "0.3 1.0 0.3 0.5";
   colors[2] = "0.3 1.0 0.15 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ShockwaveData(TBDispSShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.3 1.0 0.75";
   colors[1] = "1.0 0.6 1.0 0.5";
   colors[2] = "1.0 0.9 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(MBDispSExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = MBDispSShockwave;

   faceViewer = true;
};

datablock ExplosionData(CCDispSExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = CCDispSShockwave;

   faceViewer = true;
};

datablock ExplosionData(PCDispSExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = PCDispSShockwave;

   faceViewer = true;
};

datablock ExplosionData(TBDispSExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = TBDispSShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(MBDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 750;

   explosion           = "MBDispSExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(CCDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 750;

   explosion           = "CCDispSExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(PCDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 750;

   explosion           = "PCDispSExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(TBDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 1;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 750;

   explosion           = "TBDispSExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MBCannonCapacitor)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "extra mitzi capacitor charges";
   cantPickup = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(MBCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = MBCannonImage;
   ammo = MBCannonCapacitor;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a mitzi blast cannon";

   emap = true;
};

datablock ShapeBaseImageData(MBCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   item = MBCannon;
   ammo = MBCannonCapacitor;
   offset = "0 0 0";
   emap = true;

//   projectile = MitziBlast;
   projectileType = LinearFlareProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateSequence[3] = "Recoil";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.8;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
//   stateSound[3] = PlasmaFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.75;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = DiscDryFireSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateScript[5] = "onCharge";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ShapeBaseImageData(MBChargerAImage)
{
   shapeFile = "weapon_targeting.dts";
   offset = "0.075 0 0.215";
   emap = true;
};

datablock ShapeBaseImageData(MBChargerBImage)
{
   shapeFile = "weapon_targeting.dts";
   offset = "-0.1 0 -0.1";
   rotation = "0 1 0 135";
   emap = true;
};

datablock ShapeBaseImageData(MBChargerCImage)
{
   shapeFile = "weapon_targeting.dts";
   offset = "0.25 0 -0.075"; //.075";
   rotation = "0 1 0 -135";
   emap = true;
};

function MBCannonImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(MBChargerAImage, 4);
   %obj.mountImage(MBChargerBImage, 5);
   %obj.mountImage(MBChargerCImage, 6);
}

function MBCannonImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

function MBCannonImage::onCharge(%data,%obj,%slot)
{
     if(!%obj.charging)
          ChargeMBC(%obj);
}

function ChargeMBC(%player)       // should work....
{
   %ammo = %player.getInventory(MBCannonCapacitor);
   %aPack = %player.hasAmmoPack ? 50 : 0;
   %aPack = %player.getDatablock().isEngineer ? 25 : %aPack;	// Engineer exception +soph
   %ammoMax = %player.getDatablock().max[MBCannonCapacitor] + %aPack;

   if(%ammo > -1)   // jury rig for now
   {
      if(%ammo < %ammoMax)
      {
         if(!%player.isEMP)
         {
           %player.setInventory(MBCannonCapacitor, %ammo + 1);
           %player.charging = true;
         }
//         else
//           %player.setInventory(MBCannonCapacitor, 0);

         if(%player.isSyn)
         {
            if(%ammo < 50)
               schedule(250, %player, "ChargeMBC", %player);
            else if(%ammo < 75)
               schedule(500, %player, "ChargeMBC", %player);
            else
               schedule(1000, %player, "ChargeMBC", %player);	// schedule(750, %player, "ChargeMBC", %player); -soph
         }
         else
         {
            if(%ammo < 50)
               schedule(500, %player, "ChargeMBC", %player);
            else if(%ammo < 75)
               schedule(750, %player, "ChargeMBC", %player);
            else
               schedule(1500, %player, "ChargeMBC", %player);	// schedule(1000, %player, "ChargeMBC", %player); -soph
         }
      }
      else
         %player.charging = false;
   }
}

function canAutoboost(%obj, %dist)
{
   %vec = %obj.getMuzzleVector(0);
   %pos = %obj.getMuzzlePoint(0);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return true;
   else
      return false;
}

function MBCannonImage::onFire(%data, %obj, %slot)
{
//   %data.lightStart = //tSimTime();

//   if( %obj.station $= "" && %obj.isCloaked() )
//   {
//      if( %obj.respawnCloakThread !$= "" )
//      {
//         Cancel(%obj.respawnCloakThread);
//         %obj.setCloaked( false );
//         %obj.respawnCloakThread = "";
//      }
//      else
//      {
//         if( %obj.getEnergyLevel() > 20 )
//         {
//            %obj.setCloaked( false );
//            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
//         }
//      }
//   }

     %weapon = %obj.getMountedImage(0).item;
     if(%obj.client.mode[%weapon] == 0)
     {
         %projectile = MitziBlast;
         %energyUse = 10;
         %type = "Standard Mitzi Blast";
     }
     else if(%obj.client.mode[%weapon] == 1)
     {
         %projectile = MitziSingularityBlast;
         %energyUse = 5;
         %type = "Light Mitzi Blast";
     }
     else if(%obj.client.mode[%weapon] == 2)
     {
         %projectile = MitziX2;
         %energyUse = 20;
         %type = "Dense Mitzi Blast";
     }
     else if(%obj.client.mode[%weapon] == 3)
     {
         %projectile = MitziBooster;
         %energyUse = 25;
         %type = "Mitzi Booster";
     }
     else if(%obj.client.mode[%weapon] == 4)
     {
         %projectile = MitziAnnihalator;
         %energyUse = 100;
         %type = "Mitzi Annihalator";
     }
     else if(%obj.client.mode[%weapon] == 5)
     {
         %projectile = MitziRumbleBlast;
         %energyUse = 125;	// %energyUse = 150; -soph
         %type = "Modulated Shield Transparency Blast";
     }
     //-Nite- removed thremo Nuke mode
    // else if(%obj.client.mode[%weapon] == 6)
    // {
     //    %projectile = MitziNukeBlast;
     //    %energyUse = 150;
     //    %type = "Thermonuclear Blast";
    // }

     %autoModeClear = false;
     if(%obj.client.mitziAutoMode)
     {
          if(canAutoboost(%obj, 8))
          {
              %autoModeClear = true;
              %projectile = MitziBooster;
              %energyUse = 25;
              %type = "Mitzi Booster";
          }
     }

     // DarkDragonDX: Dead people can only fire Mitzi Boosters
     if ((%obj.client.mode[%weapon] != 3 && !%autoModeClear) && %obj.pdead && $votePracticeModeOn)
     {
 	bottomPrint(%obj.client, "You are out and can fire only Mitzi boosters.", 3, 1);
     	%obj.play3d(ShockLanceDryFireSound);
	return;
     }

     %nrg = %energyUse -1;

   %vehicle = 0;

//   if(%obj.client.mode[%weapon] == 4 && $Feature[AnniSafety] && Meltdown.AnniSafetyActive)
//   {
//      if(isObject(getLOSInfo(%obj, $Feature[AnniSafety, minFireDist])))
//      {
//         bottomPrint(%obj.client, "<color:ffffff>Annihalator Safety System <color:ff1313>v1.1\n<color:42dbea>You are too close to something to fire the Annihalator. Min distance is "@$Feature[AnniSafety, minfireDist]@"m", 5, 2);
//         %obj.play3d(ShockLanceDryFireSound);
//         return;
//      }
//   }

   if(%obj.getInventory(%data.ammo) < %energyUse)
   {
      if( !%obj.dead )	// +soph
         bottomPrint(%obj.client, "Not enough energy to fire "@%type@".", 3, 1);
      %obj.play3d(ShockLanceDryFireSound);
      return;
   }
   else if(%obj.client.mode[%weapon] != 4 || (%obj.client.mitziAutoMode && %autoModeClear)) // special Anni fire
   {
      %p = new LinearFlareProjectile()
      {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
     };

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
      %p.damageMod = %obj.damageMod;
   else
      %p.damageMod = 1;

   if(%obj.synMod)
     %p.damageMod += %obj.synMod * %obj.isSyn;

     if(%obj.client.mode[%weapon] == 2)
     {
        schedule(500, %p, spawnProjectile, %p, LinearFlareProjectile, MBDisplayCharge);
        schedule(500, %p, MBIrradiate, %p);
//        schedule(1000, %p, MBLanceRandomize, %p);
     }

     if(%obj.client.mode[%weapon] == 3)
        projectileTrail(%p, 250, LinearFlareProjectile, MBDisplayCharge, true);

     if(%obj.client.mode[%weapon] == 5)
     {
          spawnProjectile(%p, LinearFlareProjectile, TBDisplayCharge);
          %obj.play3D(AAFireSound);
          %obj.applyKick(-2000);
     }
   }
   else
   {
      %rnd = 1337 + getRandom(300); //1000 and random: 200
      schedule(%rnd, 0, "MBAFire", %data, %obj, %slot);
      %obj.play3d(RMSSBDeploySound); //ShockLanceDryFireSound);                                                                  //100
      bottomPrint(%obj.client, "<color:ffffff>Mitzi Control System\n<color:42dbea>Overvolting Mitzi electron emitters...", ((%rnd+200) / 1000), 2);
      return;
   }

   if(isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;
      
   %obj.play3d(PlasmaFireSound);
   %obj.decInventory(%data.ammo, %nrg);
   
   if(!%obj.charging)
      ChargeMBC(%obj);
}

function MBAFire(%data, %obj, %slot)
{
   if( isObject( getLOSInfo( %obj , 10 ) ) && !%obj.dead ) 	// (isObject(getLOSInfo(%obj, 10))) -soph
   {
      bottomPrint(%obj.client, "Not enough room to create the Annihalator blast; shutting down", 4, 1);
      %obj.play3d(ShockLanceDryFireSound);
      %obj.decInventory(%data.ammo, 25);

      if(!%obj.charging)
         ChargeMBC(%obj);

      return;
   }

   if(isObject(%obj))
   {
      if(isObject(%obj.annihalator)) // multi-fire mitzi bug
        // %obj.annihalator.delete();				// -soph
      {								// +[soph]
         %obj.play3d( ShockLanceDryFireSound) ;			// +
         return ;						// +
      }								// +[/soph]

      %p = new LinearFlareProjectile()
      {
         dataBlock        = MitziAnnihalator;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = 0;
     };

     %obj.applyKick(-2000);
     %obj.annihalator = %p;
//     if(isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
//        %obj.lastProjectile.delete();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
      %p.damageMod = %obj.damageMod;
   else
      %p.damageMod = 1;

   if(%obj.synMod)
     %p.damageMod += %obj.synMod;

//     %obj.lastProjectile = %p;
//     %obj.deleteLastProjectile = %data.deleteLastProjectile;
     MissionCleanup.add(%p);

     // AI hook
     if(%obj.client)
        %obj.client.projectile = %p;

    // ai dodging +[soph]
     %dangerPosition = vectorScale( %obj.getMuzzlePoint( %slot ) , vectorScale( %obj.getMuzzleVector( %slot ) , 5 ) ) ;
     InitContainerRadiusSearch( %obj.getPosition() , 150 , $TypeMasks::PlayerObjectType ) ;
     while( ( %player = containerSearchNext() ) != 0 )
        if( %player.client.isAIControlled() )
           %player.client.setDangerLocation( %dangerPosition , 96 ) ;

     %obj.play3d(PlasmaFireSound);
     %obj.decInventory(%data.ammo, 100);
     schedule(1300, %p, blackHoleLoop, %p);
     %p.blackholeThread = true;
     
     if(!%obj.charging)
        ChargeMBC(%obj);
   }
}

function MBLanceRandomize(%obj)
{
   if(!isObject(%obj) && (!isObject(%obj.sourceObject) || !isObject(%obj.bkSourceObject)))
        return;

//      if(%obj.bkSourceObject)
//         if(isObject(%obj.bkSourceObject))
//            %obj.sourceObject = %obj.bkSourceObject;

//      if(!%obj.lastBlinkPos)
//         %obj.lastBlinkPos = "65536 65536 65536";

//      if(%offset)
//         %pos = vectorAdd(%obj.position, vectorScale(%obj.initialDirection, %offset));
//      else
//         %pos = %obj.position;

//      if(vectorCompare(%pos, %obj.lastBlinkPos)) //same position...BUG!
//         return;

  %muzzlePos = %obj.position;
//  %muzzleVecR = vectorRandom();
         %vector = %obj.getForwardVector();

//         %x = (getRandom() - 0.5) * 2 * 3.1415926 * 3.1415926;
//         %y = (getRandom() - 0.5) * 2 * 3.1415926 * 3.1415926;
//         %z = (getRandom() - 0.5) * 2 * 3.1415926 * 3.1415926;
//         %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
//         %muzzleVec = MatrixMulVector(%mat, %vector);
//  %endPos  = VectorAdd(%muzzlePos, VectorScale(%muzzleVec, 9));

//   echo(%muzzleVec SPC "|" SPC %muzzleVecR SPC "|" SPC %muzzlePos SPC "|" SPC %endPos);

  %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::ItemObjectType;

   %FFRObject = new StaticShape()
   {
      dataBlock        = mReflector;
   };
   MissionCleanup.add(%FFRObject);
   %FFRObject.setPosition(%obj.position);
   %FFRObject.schedule(250, delete);

//   %FFRObject2 = new StaticShape()
//   {
//      dataBlock        = DeployableWarpBeacon;
//   };
//   MissionCleanup.add(%FFRObject2);
//   %FFRObject2.setScale("15 15 15");
//   %FFRObject2.setPosition(vectorAdd(%pos, vectorScale(vectorRandom(), 5)));
//   %FFRObject2.schedule(1000, delete);
   
     InitContainerRadiusSearch(%obj.position, 30, %damageMasks);

     while((%target = containerSearchNext()) != 0)
     {
//          %tgtPos = %target.getWorldBoxCenter();
          %tgtPos = %target.position;
          %hit = true; //ContainerRayCast(%obj.position, %tgtPos, %damageMasks, %obj);

          if(%hit)
          {
               %vector = VectorNormalize(VectorSub(%target.getWorldBoxCenter(), %tgtPos));
//               %target.mbr_lanceProj = new ShockLanceProjectile()
               %target.mbr_lanceProj = new ElfProjectile()
               {
//                    dataBlock        = BasicShocker2;
                    dataBlock        = BasicELF;
                    initialDirection = %vector;
                    initialPosition  = %obj.position;
                    sourceObject     = %FFRObject;
                    targetId         = %target;
                    sourceslot       = 0;
               };
               %target.mbr_lanceProj.schedule(250, delete);
               
             %target.play3D(ShockLanceHitSound);
             %target.getDataBlock().damageObject(%target, %obj.sourceObject, %target.position, 0.1, $DamageType::MB);
          }
     }

   schedule(250, %obj, MBLanceRandomize, %obj);
}

function MBIrradiate(%obj)
{
     if(!isObject(%obj) && (!isObject(%obj.sourceObject) || !isObject(%obj.bkSourceObject)))
          return;

     if(!%obj.lastBlinkPos)
          %obj.lastBlinkPos = "999 999 999";

     %pos = %obj.position;

     if(vectorCompare(%pos, %obj.lastBlinkPos))
          return;

     if( %obj.exploded )	// +soph
          return;		// +soph

     %obj.lastBlinkPos = %pos;

//     %damageMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType;
//     %hit = ContainerRayCast(%muzzlePos, %endPos, %damageMasks, %obj);

     RadiusExplosion(%obj, %obj.position, 8, 0.185, 0, %obj.sourceObject, $DamageType::DenseMitzi);
     
     schedule(200, %obj, "MBIrradiate", %obj);
}

//

function BlackHoleSuck(%obj, %size, %pos)
{
   InitContainerRadiusSearch(%pos, %size, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::CorpseObjectType);
   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %size)
         continue;

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];
      %distPct = %dist / %size;

      %coverage = calcExplosionCoverage(%pos, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::StaticShapeObjectType |
                                         $TypeMasks::ForceFieldObjectType));
      if (%coverage == 0)
         continue;

      %tgtPos = %targetObject.getWorldBoxCenter();
      %vec = vectorNormalize(vectorSub(%pos, %tgtPos));
      %strength = %targetObject.isGenuinePlayer ? 42 : 69;
      %force = (%strength * %dist) + 1/%dist;
      %forceVector = vectorScale(%vec, %force);

      if(!%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive)
          %targetObject.applyImpulse(%pos, %forceVector);
   }
}

function blackHoleLoop(%obj)
{
   if(isObject(%obj))
   {
     if(!%obj.lastBlinkPos)
         %obj.lastBlinkPos = "999 999 999";

     %pos = %obj.position;

     if(vectorCompare(%pos, %obj.lastBlinkPos))
          return;

     %obj.lastBlinkPos = %pos;
     
      %size = 45;
      BlackHoleSuck(%obj, %size, %pos);
      schedule(100, %obj, "blackHoleLoop", %obj);
   }
}
