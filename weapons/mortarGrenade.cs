// Extended Grenades
// ------------------------------------------------------------------------

datablock ItemData(MortarGrenadeThrown)
{
   shapeFile = "mortar_projectile.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = MortarExplosion;
   indirectDamage      = 1.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 2000;
};

datablock ItemData(MortarGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = MortarGrenadeThrown;
	pickUpName = "some mortars";
	isGrenade = true;
};
