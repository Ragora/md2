//--------------------------------------------------------------------------
// Laser/30.06 Sniper Rifle
// Update: now laser and bullet weapon all in one!
//--------------------------------------------------------------------------

datablock AudioProfile(SniperRifleSwitchSound)
{
   filename    = "fx/weapons/sniper_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(SniperRifleFireSound)
{
   filename    = "fx/weapons/sniper_fire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SniperRifleFireWetSound)
{
   filename    = "fx/weapons/sniper_underwater.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SniperRifleDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SniperRifleProjectileSound)
{
   filename    = "fx/weapons/sniper_miss.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SRFireSound)
{
   filename    = "fx/vehicles/tank_mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock EnergyProjectileData(Sniper3006Bullet)
{
   emitterDelay        = -1;
   directDamage        = 0.45;
   directDamageType    = $DamageType::Sniper;
   explosion           = "ChaingunExplosion";
   splash              = ChaingunSplash;
   kickBackStrength    = 5000;
   bubbleEmitTime      = 1.0;

   sound = ChaingunProjectile;
   velInheritFactor    = 0.5;

   grenadeElasticity = 0;
   grenadeFriction   = 1;
   armingDelayMS     = 250;

   muzzleVelocity    = 1500.0;
   drag = 0.05;
   gravityMod        = 0.0;

   dryVelocity       = 1500.0;
   wetVelocity       = 1400.0;

   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.5 0.175 0.175";

   scale = "0.25 20.0 1.0";
   crossSize       = 0.20;
   crossViewAng    = 0.990;

   blurLifetime   = 0;
   blurWidth      = 0;
   blurColor = "0 0 0 0";

   texture[0] = "special/tracer00";
   texture[1] = "special/tracercross";
};

function Sniper3006Bullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   if( %projectile.damageMod > 0 )							// +[soph]
      %damageMod = %projectile.damageMod ;						// +
   else											// +
      %damageMod = 1 ;									// +[/soph]

   if(%targetObject.getType() & $TypeMasks::PlayerObjectType) // much more efficient //getDatablock().getClassName() $= "PlayerData")
   {
      checkMAHit(%projectile, %targetObject, %position);
      
//      %damLoc = firstWord(%targetObject.getDamageLocation(%position));		// -[soph]
											// -
//      if(isObject(%projectile.sourceObject))						// -
//          bottomPrint(%projectile.sourceObject.client, "Hit location:" SPC %damLoc, 5, 1);
											// -
//      %p = %targetObject.getWorldBoxCenter();						// -
//      %momVec = VectorSub(%p, %position);						// -
//      %momVec = VectorNormalize(%momVec);						// -
//      %impulseVec = VectorScale(%momVec, 1500);					// -
//      %targetObject.applyImpulse(%position, %impulseVec);				// -[/soph]

      if( %targetObject.isMounted() )							// +[soph]
      {											// +
         %mount = %targetObject.getObjectMount() ;					// +
         if( %mount.team == %targetObject.team )					// +
         {										// +
            %found = -1 ;								// +
            for ( %i = 0 ; %i < %mount.getDataBlock().numMountPoints ; %i++ )		// +
            {										// +
               if ( %mount.getMountNodeObject( %i ) == %targetObject )			// +
               {									// +
                  %found = %i ;								// +
                  break ;								// +
               }									// +
            }										// +
            if( %found != -1 && %mount.getDataBlock().isProtectedMountPoint[ %found ] )	// +
               %hitMod = 2 ;								// +
         }										// +
      }											// +
      else										// +
      {											// +
         %p = %targetObject.getWorldBoxCenter() ;					// +
         %momVec = VectorSub( %p , %position ) ;					// +
         %momVec = VectorNormalize( %momVec ) ;						// +
         %impulseVec = VectorScale( %momVec , 1500 ) ;					// +
         %targetObject.applyImpulse( %position , %impulseVec ) ;			// +
      }											// +
      if( !%hitMod )									// +
      {											// +
         %damLoc = firstWord( %targetObject.getDamageLocation( %position ) ) ;		// +
         if( isObject( %projectile.sourceObject ) )					// +
            bottomPrint( %projectile.sourceObject.client , "Hit location:" SPC %damLoc , 5 , 1 ) ;
         if( %damLoc $= "head" )							// +
         {										// +
            %targetObject.getOwnerClient().headShot = 1 ;				// +
            %hitMod = 3 ;								// +
         }										// +
         else										// +
            %hitMod = 1 ;								// +
      }											// +
   }											// +
   else											// +
       %hitMod = 2 ;									// +
   %targetObject.damage( %projectile.sourceObject , %position , %hitMod * %damageMod * %data.directDamage , %data.directDamageType ) ;
   %targetObject.getOwnerClient().headShot = 0 ;					// +[/soph]

//      if(%damLoc $= "head")								// -[soph]
//      {										// -
//          %targetObject.damage(%projectile.sourceObject, %position, 2.15 * %projectile.sourceObject.damageMod, %data.directDamageType);
//          %targetObject.getOwnerClient().headShot = 1; 				// -
//      }										// -
//      else										// -
//          %targetObject.damage(%projectile.sourceObject, %position, 0.45 * %projectile.sourceObject.damageMod, %data.directDamageType);
//   }											// -
//   else										// -
//        %targetObject.damage(%projectile.sourceObject, %position, 1.0 * %projectile.sourceObject.damageMod, %data.directDamageType);
											// -[/soph]
}

datablock EnergyProjectileData(PoisonBullet)
{
   emitterDelay        = -1;
   directDamage        = 0.30 ;			// = 0.01; -soph
   directDamageType    = $DamageType::Sniper ;	// = $DamageType::Poison; -soph
   explosion           = "ChaingunExplosion";
   splash              = ChaingunSplash;
   kickBackStrength    = 1000;
   bubbleEmitTime      = 1.0;

   sound = ChaingunProjectile;
   velInheritFactor    = 0.5;

   grenadeElasticity = 0;
   grenadeFriction   = 1;
   armingDelayMS     = 250;

   muzzleVelocity    = 1500.0;
   drag = 0.05;
   gravityMod        = 0.0;

   dryVelocity       = 1500.0;
   wetVelocity       = 1400.0;

   fizzleTimeMS      = 1250;
   lifetimeMS        = 1250;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.5 0.175 0.175";

   scale = "0.25 20.0 1.0";
   crossSize       = 0.20;
   crossViewAng    = 0.990;

   blurLifetime   = 0;
   blurWidth      = 0;
   blurColor = "0 0 0 0";

   texture[0] = "special/tracer00";
   texture[1] = "special/tracercross";
};

function PoisonBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   if( %projectile.damageMod > 0 )		// +[soph]
      %damageMod = %projectile.damageMod ;	// +
   else						// +
      %damageMod = 1 ;				// +[/soph]

   if(%targetObject.getType() & $TypeMasks::PlayerObjectType) // much more efficient //getDatablock().getClassName() $= "PlayerData")
   {
//      %damLoc = firstWord(%targetObject.getDamageLocation(%position));
      if( !%targetObject.devshield )	 	// _up_ +[soph]
      {						// +
         PoisonObject( %targetObject , %projectile.sourceObject , 0.45 ) ;
						// +[/soph]
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);
         %impulseVec = VectorScale(%momVec, 500);

//      if(!%targetObject.devshield)		// ^up^ -soph
         %targetObject.applyImpulse(%position, %impulseVec);
      }

//      if(%damLoc $= "head")
          %targetObject.damage( %projectile.sourceObject , %position , %damageMod * %data.directDamage , %data.directDamageType ) ;	// %targetObject.damage(%projectile.sourceObject, %position, %data.directDamage, %data.directDamageType); -soph
   }
}

datablock TracerProjectileData(MassDriverBullet)
{
   projectileShapeName = "chaingun_shot.dts";
   doDynamicClientHits = true;

   directDamage        = 0.5 ;			// = 0.7; -soph
   directDamageType    = $DamageType::Gauss;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 2.5;
   radiusDamageType    = $DamageType::Gauss;
   kickBackStrength    = 2500;

   explosion           = "GaussExplosion";
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
   splash              = ChaingunSplash;
   sound 			   = ChaingunProjectile;

   dryVelocity       = 1000.0;
   wetVelocity       = 1000.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   baseEmitter     = GaussBulletEmitter;
   tracerLength    = 5;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
};

function MassDriverBullet::onCollision( %data , %projectile , %targetObject , %modifier , %position , %normal )	// +[soph]
{														// +
   if( %projectile.damageMod > 0 )										// +
      %damageMod = %projectile.damageMod ;									// +
   else														// +
      %damageMod = 1 ;												// +
														// +
   %hitMod = 1 ;												// +
   if(%targetObject.getType() & $TypeMasks::PlayerObjectType)							// +
   {														// +
      if( %targetObject.isMounted() || %targetObject.isTacticalMech )						// +
      {														// +
         %mount = %targetObject.getObjectMount() ;								// +
         if( %mount.team == %targetObject.team )								// +
         {													// +
            %found = -1 ;											// +
            for ( %i = 0 ; %i < %mount.getDataBlock().numMountPoints ; %i++ )					// +
            {													// +
               if ( %mount.getMountNodeObject( %i ) == %targetObject )						// +
               {												// +
                  %found = %i ;											// +
                  break ;											// +
               }												// +
            }													// +
            if( %found != -1 && %mount.getDataBlock().isProtectedMountPoint[ %found ] )				// +
               %hitMod = 1.5 ;											// +
         }													// +
      }														// +
   }														// +
   else														// +
      %hitMod = 1.5 ;												// +
   %projectile.damageMod = %damageMod * %hitMod ;								// +
   Parent::onCollision( %data , %projectile , %targetObject , %modifier , %position , %normal ) ;		// +
   %projectile.damageMod /= %hitMod ;										// +
}														// +[/soph]

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData( SniperSplashParticle )
{

   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.03;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 600;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;


   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( SniperSplashEmitter )
{
   ejectionPeriodMS = 25;
   ejectionOffset = 0.2;
   periodVarianceMS = 0;
   ejectionVelocity = 2.25;
   velocityVariance = 0.50;
   thetaMin         = 0.0;
   thetaMax         = 30.0;
   lifetimeMS       = 250;

   particles = "SniperSplashParticle";
};

datablock SplashData( SniperSplash )
{
   numSegments = 5;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = SniperSplashEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock AudioProfile(sniperExpSound)
{
   filename    = "fx/weapons/sniper_impact.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock ParticleData(SniperExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.0625;
   sizes[1]      = 0.2;
};

datablock ParticleEmitterData(SniperExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 0.75;
   velocityVariance = 0.25;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "SniperExplosionParticle1";
};

datablock ExplosionData(SniperExplosion)
{
   explosionShape = "energy_explosion.dts";
   soundProfile   = sniperExpSound;

   particleEmitter = SniperExplosionEmitter;
   particleDensity = 150;
   particleRadius = 0.25;

   faceViewer           = false;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock SniperProjectileData(BasicSniperShot)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 1000 ;		// 2000; -soph
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.145;
   endBeamWidth 	     = 0.25;
   pulseBeamWidth 	  = 0.5;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";

};

function BasicSniperShot::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %damageAmount = %projectile.damageFactor;

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   if(!%projectile.noHeadshot)
   {
       checkMAHit(%projectile, %targetObject, %position);
       
//       %damLoc = firstWord(%targetObject.getDamageLocation(%position));		// moved down, console spam -soph

       if(%targetObject.getType() & $TypeMasks::PlayerObjectType)
       {
           %damLoc = firstWord( %targetObject.getDamageLocation( %position ) ) ;	// +soph
           bottomPrint(%projectile.sourceObject.client, "Hit location:" SPC %damLoc, 5, 1);
       }
   }
   
   if(!%projectile.noHeadshot && %targetObject.getType() & $TypeMasks::PlayerObjectType)  
   {
      %damLoc = firstWord(%targetObject.getDamageLocation(%position));
      
      if( %targetObject.isMounted() )							// +[soph]
      {											// +
         %mount = %targetObject.getObjectMount() ;					// +
         if( %mount.team == %targetObject.team )					// +
         {										// +
            %found = -1 ;								// +
            for ( %i = 0 ; %i < %mount.getDataBlock().numMountPoints ; %i++ )		// +
            {										// +
               if ( %mount.getMountNodeObject( %i ) == %targetObject )			// +
               {									// +
                  %found = %i ;								// +
                  break ;								// +											// +
               }									// +
            }										// +											// +
            if( %found != -1 && %mount.getDataBlock().isProtectedMountPoint[ %found ] )	// +
               %headshotVehicleOverride = true ;					// +
         }										// +[/soph]
      }
      if( %damLoc $= "head" && !%headshotVehicleOverride )				// if( %damLoc $= "head" ) -soph
      {
         %targetObject.getOwnerClient().headShot = 1; // OMG They killed kenny!
         %modifier = 1.8 ;								// = 2; -soph
      }
      else
      {
         %modifier = 1;
         %targetObject.getOwnerClient().headShot = 0;
      }
   }
   else
      %targetObject.getOwnerClient().headShot = 0;   

   if(%targetObject.getType() & $TypeMasks::VehicleObjectType)
      %damageAmount *= 0.75;								// *= 0.5; -soph
      
   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %data.directDamageType);
   %targetObject.getOwnerClient().headShot = 0;						// +soph
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(Sniper3006Ammo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some 30.06 shells";
};

//--------------------------------------
// Rifle and item...
//--------------------------------------
datablock ItemData(SniperRifle)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_sniper.dts";
   image        = SniperRifleImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "a laser/sniper rifle";

   computeCRC = true;
};

datablock ShapeBaseImageData(SniperRifleImage)
{
	className = WeaponImage;
   shapeFile = "weapon_sniper.dts";
   item = SniperRifle;
   ammo = Sniper3006Ammo;
	armThread = looksn;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateSound[0]                    = SniperRifleSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
//   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.05;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";

   stateName[4]                     = "Reload";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.05;
   stateAllowImageChange[4]         = false;

   stateName[5]                     = "CheckWet";
   stateTransitionOnWet[5]          = "DryFire";
   stateTransitionOnNotWet[5]       = "Fire";

   stateName[6]                     = "NoAmmo";
   stateTransitionOnAmmo[6]         = "Reload";
   stateTransitionOnTriggerDown[6]  = "DryFire";

   stateName[7]                     = "DryFire";
   stateSound[7]                    = ShockLanceDryFireSound;
   stateTimeoutValue[7]             = 0.5;
   stateTransitionOnTimeout[7]      = "Ready";
};

function deleteSR36FireTimeout(%obj)
{
    %obj.SR36FireTimeout = 0;
}

function deleteSR36RFireTimeout(%obj)
{
   %obj.SR36FireTimeout = 0;
}

function deleteSR36SFireTimeout(%obj)
{
   %obj.play3d(BomberTurretDryFireSound);
}

function switchCloakDisable(%obj)
{
   if(%obj.cp_sDisable)
      %obj.cp_sDisable = false;
   else
   {
      %obj.cp_sDisable = true;
      schedule(5000, %obj, "switchCloakDisable", %obj);
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

$srDmgIncreaseFactor = 1.0;

function switchCloakDisable(%obj)
{
   if(%obj.cp_sDisable)
      %obj.cp_sDisable = false;
   else
   {
      %obj.cp_sDisable = true;
      schedule(5000, %obj, "switchCloakDisable", %obj);
//      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function SniperRifleImage::onFire(%data,%obj,%slot)
{
   %weapon = %obj.getMountedImage(0).item;

   if(%obj.isCloaked())
   {
      messageClient(%obj.client, 'msgNoCloakFire', '\c2You cannot fire the sniper rifle while cloaked.');
      return;
   }

   switchCloakDisable(%obj);

   if(%obj.client.mode[%weapon] == 0)
   {
      if(%obj.isWet)
      {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         return;
      }

      if(!%obj.hasEnergyPack)
      {
         if( %obj.dead )				// +[soph]
            %obj.setImageTrigger( %slot , false ) ;	// +
         else						// +[/soph]
            messageClient(%obj.client, 'msgSNNEPack', '\c2You need the energy pack to operate this mode.');
         // siddown Junior, you can't use it
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         return;
      }

       if(%obj.SR36FireTimeout)
           return;

       if(%obj.getEnergyLevel() < 3)
       {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         %obj.SR36FireTimeout = 500;
         schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);
         return;
       }

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

     %dmgMod = 1;

     // Vehicle Damage Modifier
     if(%vehicle)
          %dmgMod += %vehicle.damageMod;

     if(%obj.damageMod)
          %dmgMod += %obj.damageMod - 1;

     %energy = %obj.getEnergyLevel();
     %enUse = %obj.getDataBlock().maxEnergy;
     
     if(%obj.powerRecirculator)
     {
        %energy *= 0.75;
        %enUse *= 0.75; 
     }
     
     %pct = %energy / %enUse;
      
      %p = new SniperProjectile()
      {
         dataBlock        = BasicSniperShot;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         damageFactor     = 0.55 * %pct * %dmgMod;	// = 0.75 * %pct * %dmgMod; -soph
         sourceSlot       = %slot;
      };
      %p.setEnergyPercentage(%pct);


      %obj.lastProjectile = %p;
      MissionCleanup.add(%p);
      serverPlay3D(SniperRifleFireSound, %obj.getTransform());

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %obj.SR36FireTimeout = 1000;
      schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);

      %obj.useEnergy(%enUse);
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
      if(%obj.isWet)
      {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         return;
      }

       if(%obj.SR36FireTimeout)
           return;

       if(%obj.getEnergyLevel() < (%obj.getDatablock().maxEnergy * 0.2))
       {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         %obj.SR36FireTimeout = 150;
         schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);
         return;
       }

     %dmgMod = 1;

     // Vehicle Damage Modifier
     if(%vehicle)
          %dmgMod += %vehicle.damageMod;

     if(%obj.damageMod)
          %dmgMod += %obj.damageMod - 1;
      
//      %pct = (%obj.getEnergyLevel() / %obj.getDataBlock().maxEnergy) * 0.2;

     %enUse = %obj.getDataBlock().maxEnergy * 0.2;
     
     if(%obj.powerRecirculator)
        %enUse *= 0.75; 
     
     %pct = 0.2;

      %p = new SniperProjectile()
      {
         dataBlock        = BasicSniperShot;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         damageFactor     = 0.15 * %dmgMod;
         sourceSlot       = %slot;
         mode             = 1;
         noHeadshot       = true;
      };
      
      %p.setEnergyPercentage(%pct);

      %obj.lastProjectile = %p;
      MissionCleanup.add(%p);
      serverPlay3D(SniperRifleFireSound, %obj.getTransform());

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %obj.SR36FireTimeout = 150;
      schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);

      %obj.useEnergy(%enUse);
   }
   else if(%obj.client.mode[%weapon] == 2)
   {
      if(%obj.SR36FireTimeout)
         return;

      if(!%obj.getInventory(%data.ammo)) // ammo at all?!
      {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         %obj.SR36FireTimeout = 500;
         schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);
         return;
      }

      if(%obj.isCloaked())
         switchCloakDisable(%obj);

      %p = new EnergyProjectile() {
         dataBlock        = Sniper3006Bullet;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
      %obj.lastProjectile = %p;
      MissionCleanup.add(%p);
      serverPlay3D(SRFireSound, %obj.getTransform());

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %obj.SR36FireTimeout = 3500;
      schedule(%obj.SR36FireTimeout, 0, "deleteSR36RFireTimeout", %obj);
      schedule(2000, 0, "deleteSR36SFireTimeout", %obj);

      %obj.decInventory(%data.ammo, 1);
      %obj.applyKick(-100);
   }
   else if(%obj.client.mode[%weapon] == 3)
   {
      if(%obj.SR36FireTimeout)
         return;

      if(!%obj.getInventory(%data.ammo)) // ammo at all?!
      {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         %obj.SR36FireTimeout = 500;
         schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);
         return;
      }

      if(%obj.isCloaked())
         switchCloakDisable(%obj);

      %p = new EnergyProjectile() {
         dataBlock        = PoisonBullet;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
      %obj.lastProjectile = %p;
      MissionCleanup.add(%p);
      serverPlay3D(SRFireSound, %obj.getTransform());

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %obj.SR36FireTimeout = 3500;
      schedule(%obj.SR36FireTimeout, 0, "deleteSR36RFireTimeout", %obj);
      schedule(2000, 0, "deleteSR36SFireTimeout", %obj);

      %obj.decInventory(%data.ammo, 1);
   }
   else if(%obj.client.mode[%weapon] == 4)
   {
      if(%obj.SR36FireTimeout)
         return;

      if(%obj.getInventory(%data.ammo) < 2) // needs 2 ammo
      {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         %obj.SR36FireTimeout = 500;
         schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);
         return;
      }

      if(%obj.isCloaked())
         switchCloakDisable(%obj);

      %p = new TracerProjectile() {
         dataBlock        = MassDriverBullet;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

      %obj.lastProjectile = %p;
      MissionCleanup.add(%p);
      serverPlay3D(SRFireSound, %obj.getTransform());

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %obj.SR36FireTimeout = 3500;
      schedule(%obj.SR36FireTimeout, 0, "deleteSR36RFireTimeout", %obj);
      schedule(3000, 0, "deleteSR36SFireTimeout", %obj);

      %obj.decInventory(%data.ammo, 2);
      %obj.applyKick(-500);
   }
}

datablock ShapeBaseImageData(Sniper3006DecalAImage)
{
   shapeFile = "turret_belly_barrelr.dts";
   offset = "0 0.4 0.25";
   rotation = "0 0 1 180";
};

datablock ShapeBaseImageData(Sniper3006DecalBImage)
{
   shapeFile = "turret_belly_barrell.dts";
   offset = "0 0.5 0.25";
//   rotation = "0 0 1 180";
};

datablock ShapeBaseImageData(Sniper3006DecalCImage)
{
   shapeFile = "turret_belly_barrell.dts";
   offset = "0 1.25 0.1";
//   rotation = "0 1 0 180";
};

function SniperRifleImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this,%obj,%slot);
   %obj.mountImage(Sniper3006DecalAImage, 4);
   %obj.mountImage(Sniper3006DecalBImage, 5);
   %obj.mountImage(Sniper3006DecalCImage, 6);
}

function SniperRifleImage::onUnmount(%this,%obj,%slot)
{
   %obj.setImageTrigger(%slot, false);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}
