if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(MissileSwitchSound)
{
   filename    = "fx/weapons/missile_launcher_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MissileFireSound)
{
   filename    = "fx/weapons/missile_fire.WAV";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(MissileProjectileSound)
{
   filename    = "fx/weapons/missile_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(MissileReloadSound)
{
   filename    = "fx/weapons/weapon.missilereload.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MissileLockSound)
{
   filename    = "fx/weapons/missile_launcher_searching.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MissileExplosionSound)
{
   filename    = "fx/explosions/explosion.xpl23.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(MissileDryFireSound)
{
   filename    = "fx/weapons/missile_launcher_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};


//----------------------------------------------------------------------------
// Splash Debris
//----------------------------------------------------------------------------
datablock ParticleData( MDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.10;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

//   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 0.8;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MDebrisSmokeEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "MDebrisSmokeParticle";
};


datablock DebrisData( MissileSplashDebris )
{
   emitters[0] = MDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.3;
   lifetimeVariance = 0.1;

   numBounces = 1;
};


//----------------------------------------------------------------------------
// Missile smoke spike (for debris)
//----------------------------------------------------------------------------
datablock ParticleData( MissileSmokeSpike )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.6 0.6 0.6 1.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 1.0;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MissileSmokeSpikeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 3.0;  // A little oomph at the back end
   velocityVariance = 0.6;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "MissileSmokeSpike";
};


//----------------------------------------------------------------------------
// Explosion smoke particles
//----------------------------------------------------------------------------

datablock ParticleData(MissileExplosionSmoke)
{
   dragCoeffiecient     = 0.3;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 1.5;
   sizes[1]      = 4.0;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(MissileExplosionSmokeEMitter)
{
   ejectionOffset = 0.0;
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;

   ejectionVelocity = 9.75;
   velocityVariance = 0.75;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "MissileExplosionSmoke";
};

datablock ParticleData(StarburstExplosionSmoke)
{
   dragCoeffiecient     = 0.3;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 1.5;
   sizes[1]      = 4.0;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(StarburstExplosionSmokeEmitter)
{
   ejectionOffset = 6.0;
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;

   ejectionVelocity = 12.0;
   velocityVariance = 2.0;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 325;

   particles = "StarburstExplosionSmoke";
};

datablock DebrisData( MissileSpikeDebris )
{
   emitters[0] = MissileSmokeSpikeEmitter;
   explodeOnMaxBounce = true;
   elasticity = 0.4;
   friction = 0.2;
   lifetime = 0.3;
   lifetimeVariance = 0.02;
};

datablock ParticleData( StarburstDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.1 0.1 0.1 0.5";
   colors[1]     = "0.075 0.075 0.075 0.8";
   colors[2]     = "0.05 0.05 0.05 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.75;
   sizes[2]      = 0.875;
   sizes[3]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( StarburstDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "StarburstDebrisSmokeParticle";
};


datablock DebrisData(StarburstDebris)
{
   emitters[0] = StarburstDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 1.0;
   lifetimeVariance = 0.8;

   numBounces = 2;
};


//---------------------------------------------------------------------------
// Explosions
//---------------------------------------------------------------------------
datablock ExplosionData(MissileExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 1.5;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

   sizes[0] = "2 2 2";
   sizes[1] = "2 2 2";
   sizes[2] = "2 2 2";

   emitter[0] = MissileExplosionSmokeEmitter;

   debris = MissileSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 6;
   debrisVelocity = 45.0;
   debrisVelocityVariance = 6.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

datablock ExplosionData(MissileSplashExplosion)
{
   explosionShape = "disc_explosion.dts";

   faceViewer           = true;
   explosionScale = "1.0 1.0 1.0";

   debris = MissileSplashDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 80;
   debrisNum = 10;
   debrisVelocity = 10.0;
   debrisVelocityVariance = 4.0;

   sizes[0] = "0.35 0.35 0.35";
   sizes[1] = "0.15 0.15 0.15";
   sizes[2] = "0.15 0.15 0.15";
   sizes[3] = "0.15 0.15 0.15";

   times[0] = 0.0;
   times[1] = 0.333;
   times[2] = 0.666;
   times[3] = 1.0;

};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData(MissileMist)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileMistEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 6.0;
   velocityVariance = 4.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   lifetimeMS       = 250;
   particles = "MissileMist";
};



datablock ParticleData( MissileSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MissileSplashEmitter )
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 6;
   velocityVariance = 3.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "MissileSplashParticle";
};


datablock SplashData(MissileSplash)
{
   numSegments = 15;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   explosion = MissileSplashExplosion;

   texture = "special/water2";

   emitter[0] = MissileSplashEmitter;
   emitter[1] = MissileMistEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Particle effects
//--------------------------------------

datablock ParticleData( BGDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;  
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.4 0.5 1.0 1.0";
   colors[1]     = "0.3 0.3 1.0 0.5";
   colors[2]     = "0.2 0.2 0.2 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 1.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( BGDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "BGDebrisSmokeParticle";
};


datablock DebrisData( BluePassionExplosionDebris )
{
   emitters[0] = BGDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.5;
   lifetimeVariance = 0.02;

   numBounces = 1;
};            

datablock ParticleData(BluePassionExplosionSmokeParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1250;
    lifetimeVarianceMS = 0;
    useInvAlpha = 1;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_001";
    times[0] = 0;
    times[1] = 0.2;
    times[2] = 1;
    colors[0] = "0.244094 0.656000 1.000000 1.000000";
    colors[1] = "0.200000 0.300000 1.000000 1.000000";
    colors[2] = "0.000000 0.100000 1.000000 0.000000";
    sizes[0] = 1.35484;
    sizes[1] = 3;
    sizes[2] = 7;
};

datablock ParticleEmitterData(BluePassionExplosionSmokeEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 12;
    velocityVariance = 2.5;
    ejectionOffset = 0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 250;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "BluePassionExplosionSmokeParticle";
};

datablock ParticleData(BluePassionSparkParticle)
{
    dragCoefficient = 0.658537;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 500;
    lifetimeVarianceMS = 350;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/underwaterSpark.PNG";
    times[0] = 0;
    times[1] = 0.677419;
    times[2] = 1;
    colors[0] = "0.322835 0.704000 1.000000 1.000000";
    colors[1] = "0.173228 0.544000 0.864000 1.000000";
    colors[2] = "0.000000 0.712000 1.000000 0.000000";
    sizes[0] = 0.790323;
    sizes[1] = 0.790323;
    sizes[2] = 0.790323;
};

datablock ParticleEmitterData(BluePassionSparkEmitter)
{
    ejectionPeriodMS = 2;
    periodVarianceMS = 0;
    ejectionVelocity = 19;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 250;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "BluePassionSparkParticle";
};

datablock ParticleData(BluePassionCrescentParticle)
{
    dragCoefficient = 1.60976;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = -0;
    lifetimeMS = 600;
    lifetimeVarianceMS = 0;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/lightFalloffMono.png";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.173228 0.296000 1.000000 1.000000";
    colors[1] = "0.251969 0.296000 1.000000 0.500000";
    colors[2] = "0.488189 0.520000 1.000000 0.000000";
    sizes[0] = 4;
    sizes[1] = 8;
    sizes[2] = 9;
};

datablock ParticleEmitterData(BluePassionCrescentEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 40;
    velocityVariance = 5;
    ejectionOffset =   0.967742;
    thetaMin = 5;
    thetaMax = 175;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
	   lifeTimeMS = 200;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "BluePassionCrescentParticle";
};

datablock ExplosionData(BluePassionMissileExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed = 0.5;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

//   sizes[0] = "2 2 2";
// sizes[1] = "2 2 2";
//   sizes[2] = "2 2 2";

   emitter[0] = BluePassionExplosionSmokeEmitter;
   emitter[1] = BluePassionSparkEmitter;

   debris = BluePassionExplosionDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 4;
   debrisVelocity = 32.0;
   debrisVelocityVariance = 8.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 0.5;
   camShakeRadius = 7.0;
};

datablock ParticleData(HowitzerSmokeParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.02;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 100;

   textureName          = "special/flare";

   useInvAlpha = false;
   spinRandomMin = 0.0;
   spinRandomMax = 0.0;

   colors[0]     = "0.0 0.75 1.0 0.0";
   colors[1]     = "0.2 0.5 0.5 1.0";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1.3;
   sizes[1]      = 2.6;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.1;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(HowitzerSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 1.5;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "HowitzerSmokeParticle";
};

datablock ParticleData(MissileSmokeParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.02;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha = true;
   spinRandomMin = -90.0;
   spinRandomMax = 90.0;

   colors[0]     = "1.0 0.75 0.0 0.0";
   colors[1]     = "0.5 0.5 0.5 1.0";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.1;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileSmokeEmitter)
{
   ejectionPeriodMS = 20 ;	// 10; -soph
   periodVarianceMS = 0;

   ejectionVelocity = 1.5;
   velocityVariance = 0.3;

   thetaMin         = 0.0;
   thetaMax         = 50.0;

   particles = "MissileSmokeParticle";
};

datablock ParticleData(MissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 0.75 0.2 1.0";
   colors[1]     = "1.0 0.5 0.0 1.0";
   colors[2]     = "1.0 0.40 0.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "MissileFireParticle";
};

datablock ParticleData(EMPMissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "0.2 1.0 0.2 1.0";
   colors[1]     = "0.5 1.0 0.0 1.0";
   colors[2]     = "0.4 1.0 0.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(EMPMissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "EMPMissileFireParticle";
};

datablock ParticleData(MagMissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "1.0 1.0 1.0 1.0";
   colors[2]     = "1.0 1.0 1.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MagMissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "MagMissileFireParticle";
};

datablock ParticleData(IncenMissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 0.1 0.1 1.0";
   colors[1]     = "1.0 0.2 0.2 1.0";
   colors[2]     = "1.0 0.1 0.1 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(IncenMissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "IncenMissileFireParticle";
};

datablock ParticleData(ConcMissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "0.2 0.2 1.0 1.0";
   colors[1]     = "0.5 0.0 1.0 1.0";
   colors[2]     = "0.4 0.0 1.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ConcMissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "ConcMissileFireParticle";
};

datablock ParticleData(OldMMFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 500;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "0.75 0.75 1.0 1.0";
   colors[1]     = "0.5 5.0 1.0 0.75";
   colors[2]     = "0.4 0.4 1.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 2;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(OldMMFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "OldMMFireParticle";
};

datablock ParticleData(NewMissileFireParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 1;
    constantAcceleration = 0;
    lifetimeMS = 350;
    lifetimeVarianceMS = 50;
    useInvAlpha = 0;
    spinRandomMin = -135;
    spinRandomMax = 135;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.3;
    times[2] = 1;
    colors[0] = "1.000000 0.750000 0.200000 1.000000";
    colors[1] = "1.000000 0.500000 0.000000 1.000000";
    colors[2] = "1.000000 0.400000 0.000000 0.000000";
    sizes[0] = 0;
    sizes[1] = 1;
    sizes[2] = 1.5;
};

datablock ParticleEmitterData(NewMissileFireEmitter)
{
    ejectionPeriodMS = 15;
    periodVarianceMS = 0;
    ejectionVelocity = 15;
    velocityVariance = 0;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "NewMissileFireParticle";
};

datablock ParticleData(NewMissileSmokeParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0.02;
    windCoefficient = 1;
    inheritedVelFactor = 0.1;
    constantAcceleration = 0;
    lifetimeMS = 1800;
    lifetimeVarianceMS = 200;
    useInvAlpha = 0;
    spinRandomMin = -90;
    spinRandomMax = 90;
    textureName = "special/expFlare.png";
    times[0] = 0;
    times[1] = 0.1;
    times[2] = 0.28;
    times[3] = 1;
    colors[0] = "0.228346 0.450000 1.000000 1.000000";
    colors[1] = "0.228346 0.350000 0.750000 0.800000";
    colors[2] = "0.500000 0.500000 0.500000 1.000000";
    colors[3] = "0.300000 0.300000 0.300000 0.000000";
    sizes[0] = 1.0;
    sizes[1] = 1.5;
    sizes[2] = 2.0;
    sizes[3] = 2.2;
};

datablock ParticleEmitterData(NewMissileSmokeEmitter)
{
    ejectionPeriodMS = 10;
    periodVarianceMS = 0;
    ejectionVelocity = 1.5;
    velocityVariance = 0.3;
    ejectionOffset =   0.1;
    thetaMin = 0;
    thetaMax = 25;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "NewMissileSmokeParticle";
};

datablock ParticleData(MissilePuffParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 500;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 1.0 1.0 0.5";
   colors[1]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(MissilePuffEmitter)
{
   ejectionPeriodMS = 50;
   periodVarianceMS = 3;

   ejectionVelocity = 0.5;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   particles = "MissilePuffParticle";
};

datablock ParticleData(MissileLauncherExhaustParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.01;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 500;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha = true;
   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 1.0 1.0 0.5";
   colors[1]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(MissileLauncherExhaustEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 3.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 20.0;

   particles = "MissileLauncherExhaustParticle";
};

//--------------------------------------------------------------------------
// Debris
//--------------------------------------
datablock DebrisData( FlechetteDebris )
{
   shapeName = "weapon_missile_fleschette.dts";

   lifetime = 5.0;

   minSpinSpeed = -320.0;
   maxSpinSpeed = 320.0;

   elasticity = 0.2;
   friction = 0.3;

   numBounces = 3;

   gravModifier = 0.40;

   staticOnMaxBounce = true;
};

datablock ParticleData( ConcCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.1 0.5 0.8 1.0";
   colors[1]     = "0.1 0.5 0.8 0.5";
   colors[2]     = "0.1 0.5 0.8 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 8.0;
   sizes[2]      = 9.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( ConcCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ConcCrescentParticle";
};

datablock ShockwaveData(MissileShockwave)
{
   width = 5;
   numSegments = 24;
   numVertSegments = 16;
   velocity = 64;
   acceleration = 64.0;
   lifetimeMS = 350;
   height = 2.0;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 1.0";
   colors[1] = "0.8 0.5 0.2 0.75";
   colors[2] = "0.4 0.25 0.05 0.0";
};

datablock ParticleData( MissileCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.357;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 1.2;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 100;
   textureName          = "special/crescent3";
   colors[0]     = "0.75 0.7 0.2 1.0";
   colors[1]     = "0.75 0.7 0.2 0.5";
   colors[2]     = "0.75 0.7 0.2 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 4.0;
   sizes[2]      = 8.0;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MissileCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 50;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 250;
   particles = "MissileCrescentParticle";
};

datablock ParticleData(MissileSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 7.5;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 250;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileSparkEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 1;
   ejectionVelocity = 8;
   velocityVariance = 4;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 250;
   particles = "MissileSparks";
};

datablock ParticleData(MMortarExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.30;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.75 0.75 0.75 0.75";
   colors[2]     = "0.5 0.5 0.5 0.5";
   colors[3]     = "0 0 0 0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 10.0;
   sizes[3]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(MMortarExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "MMortarExplosionSmoke";
};

datablock ExplosionData(MissileSubExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   sizes[0] = "6 6 6";
   sizes[1] = "6 6 6";
   times[0] = 0.0;
   times[1] = 1.0;

   delayMS = 75;

   offset = 2.5;

   playSpeed = 0.5;
};

datablock DebrisData( LMSpikeDebris )
{
   emitters[0] = MissileSmokeSpikeEmitter;
   explodeOnMaxBounce = true;
   elasticity = 0.4;
   friction = 0.2;
   lifetime = 1.3;
   lifetimeVariance = 0.5;
};

datablock ExplosionData(LargeMissileExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.5;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

   sizes[0] = "6 6 6";
   sizes[1] = "6 6 6";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = LargeMissileSubExplosion;
   emitter[0] = MMortarExplosionSmokeEmitter;
   emitter[1] = MissileCrescentEmitter;
   emitter[2] = MissileSparkEmitter;
   shockwave = MissileShockwave;

   debris = LMSpikeDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 75;
   debrisNum = 8;
   debrisNumVariance = 2;
   debrisVelocity = 30.0;
   debrisVelocityVariance = 5.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

datablock ExplosionData(StarburstExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.6;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

   sizes[0] = "6 6 6";
   sizes[1] = "6 6 6";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = LargeMissileSubExplosion;
   emitter[0] = MMortarExplosionSmokeEmitter;
   emitter[1] = MissileCrescentEmitter;
   emitter[2] = MissileSparkEmitter;
   emitter[3] = StarburstExplosionSmokeEmitter;
   shockwave = MissileShockwave;

   debris = StarburstDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 12;
   debrisNumVariance = 6;
   debrisVelocity = 25.0;
   debrisVelocityVariance = 10.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

datablock ParticleData( SmallMissileCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0] = "1.0 0.8 0.2 1.0";
   colors[1] = "1.0 0.4 0.2 1.0";
   colors[2] = "1.0 0.0 0.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( SmallMissileCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 80;
   velocityVariance = 10.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "SmallMissileCrescentParticle";
};

datablock ParticleData(SmallMissileExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.8;
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 00;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.2 0.2 0.2 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(SmallMissileExplosionSmokeEmitter)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 0;

   ejectionVelocity = 7;
   velocityVariance = 7;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "SmallMissileExplosionSmoke";
};

//datablock ExplosionData(SmallMissileExplosion)	// unused -[soph]
//{
//   explosionShape = "effect_plasma_explosion.dts";
//   playSpeed      = 0.4;
//   sizes[0] = "3 3 3";
//   sizes[1] = "3 3 3";
//   soundProfile   = DeployablesExplosionSound;
//   faceViewer     = true;
//
//   emitter[0] = MissileSparkEmitter;
//   emitter[1] = SmallMissileExplosionSmokeEmitter;
//   emitter[2] = SmallMissileCrescentEmitter;
//
//   debris = VSpikeDebris;
//   debrisThetaMin = 10;
//   debrisThetaMax = 86;
//   debrisNum = 12;
//   debrisNumVariance = 4;
//   debrisVelocity = 60.0;
//   debrisVelocityVariance = 10.0;
//
//   shakeCamera = true;
//   camShakeFreq = "8.0 7.0 9.0";
//   camShakeAmp = "50.0 50.0 50.0";
//   camShakeDuration = 1.0;
//   camShakeRadius = 10.0;
//};							// -[/soph]

datablock DebrisData( BlowthroughDebris )		// +[soph]
{
   emitters[0]            = HowitzerSmokeEmitter ;	// NewMissileSmokeEmitter ;

   explodeOnMaxBounce     = true ;

   elasticity             = 0.4 ;
   friction               = 0.2 ;

   lifetime               = 0.15 ;
   lifetimeVariance       = 0.01 ;

   numBounces             = 1 ;
};

datablock ExplosionData( BlowthroughExplosion )
{
   soundProfile           = GrenadeExplosionSound ;

   faceViewer             = true ;
   explosionScale         = "0.8 0.8 0.8" ;

   debris                 = BlowthroughDebris ;
   debrisThetaMin         = 155 ;
   debrisThetaMax         = 190 ;
   debrisNum              = 7 ;
   debrisNumVariance      = 3 ;
   debrisVelocity         = 155.0 ;
   debrisVelocityVariance = 85.0 ;

   emitter[0]             = GrenadeSparksEmitter ;

   shakeCamera            = true ;
   camShakeFreq           = "6.0 4.0 5.0" ;
   camShakeAmp            = "15.0 15.0 15.0" ;
   camShakeDuration       = 0.5 ;
   camShakeRadius         = 6.0 ;
};							// +[/soph]

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock SeekerProjectileData(ShoulderMissile)
{
   scale = "3.0 3.0 3.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 1.0;	// = 0.9; -soph
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 2000;

   explosion           = "LargeMissileExplosion";
   underwaterExplosion = UnderwaterHandGrenadeExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = NewMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 7000;
   muzzleVelocity      = 35.0;
   maxVelocity         = 175.0;
   turningSpeed        = 120.0;
   acceleration        = 50.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 550;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock SeekerProjectileData(MegaMissile)
{
   scale = "6.0 8.0 6.0";
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 1.5;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::MegaMissile;
   kickBackStrength    = 5000;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 10000;
   muzzleVelocity      = 20.0;
   maxVelocity         = 80.0;
   turningSpeed        = 45.0;	// = 90.0; -soph
   acceleration        = 20.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 150;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock SeekerProjectileData(FastMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 0.65 ;	// = 0.6; -soph
   damageRadius        = 9;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 4000 ;	// = 2000; -soph

   explosion           = "MissileExplosion";
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = MagMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 6000 ;	// = 5000; -soph //death~bot 4000
   muzzleVelocity      = 20.0;
   maxVelocity         = 400.0;
   turningSpeed        = 1800.0;
   acceleration        = 100.0;

   proximityRadius     = 2;

   terrainAvoidanceSpeed         = 360;
   terrainScanAhead              = 50;
   terrainHeightFail             = 24;
   terrainAvoidanceRadius        = 200;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 10;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock SeekerProjectileData(EMPMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 1.5 ;	// = 0.4; -soph
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 1000;

   explosion           = "ConcMortarExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = EMPMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 6000; //death~bot 6000
   muzzleVelocity      = 20.0;
   maxVelocity         = 200.0;
   turningSpeed        = 105.0;
   acceleration        = 50.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 550;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

function EMPMissile::onExplode(%data, %proj, %pos, %mod)
{
//     if(%proj.noPlayerEMP)					// -[soph]
//          Parent::onExplode(%data, %proj, %pos, %mod);	// obsolete 
//     else
//     {							// -[/soph]
        if(%proj.bkSourceObject)
           if(isObject(%proj.bkSourceObject))
              %proj.sourceObject = %proj.bkSourceObject;
     
          %modifier = 1;
     
        if(%data.hasDamageRadius) // I lost a bomb. Do you have it?
        {
           if(%proj.vehicleMod)
                %modifier *= %proj.vehicleMod;
     
             if(%proj.damageMod)
               %modifier *= %proj.damageMod;
               
           if(%data.passengerDamageMod && %proj.passengerCount)
               %modifier *= 1 + (%proj.passengerCount * %data.passengerDamageMod);
     
           EMPBurstExplosion(%proj, %pos, %data.damageRadius, %data.indirectDamage * %modifier, %data.kickBackStrength, %proj.sourceObject, %data.radiusDamageType);
        }
//     }
}

datablock SeekerProjectileData(IncendiaryMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   directDamage        = 0.001;
   directDamageType    = $DamageType::Burn;
   hasDamageRadius     = true;
   indirectDamage      = 0.2 ;	// = 0.001; -soph
   damageRadius        = 20.0 ;	// 15.0; -soph
   radiusDamageType    = $DamageType::Burn;
   kickBackStrength    = 500;

   explosion           = "IncendiaryExplosion";
   underwaterExplosion = UnderwaterHandGrenadeExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = IncenMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 4000; //death~bot 5000
   muzzleVelocity      = 30.0;
   maxVelocity         = 140.0;
   turningSpeed        = 105.0;
   acceleration        = 50.0;

   proximityRadius     = 4;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 550;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

function IncendiaryMissile::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{
     Parent::onExplode(%data, %proj, %pos, %mod);
     IncendiaryExplosion( %pos , %data.damageradius , 5 , %proj.sourceObject );
}								// +[/soph]

datablock AudioProfile(FlashGrenadeExplosionSound)
{
   filename = "fx/explosions/grenade_flash_explode.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock SeekerProjectileData(ConcussionMissile)	// repurposed as cluster missile +[soph]
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 0.2;					// = 0.001; -soph
   damageRadius        = 8;					// = 12; -soph
   radiusDamageType    = $DamageType::Missile;			// $DamageType::Default; -soph
   kickBackStrength    = 500;					// = 36000; -soph

   explosion           = "";					// UnderwaterMortarExplosion; -soph
   splash              = MissileSplash;
   velInheritFactor    = 0.0;	// splits from missile, no inheritance +soph
  // velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = ConcMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 6000;	// = 8000; fair lifetime -soph 			//8000  Death~bot
   muzzleVelocity      = 50.0;	// = 40.0; thrown off the parent -soph 		//this..ebil..was jacked to 20 which caused
   maxVelocity         = 200.0;	// = 100.0; fast, but not too fast -soph 	// "borkage" of missile conc ability. its the only damn way juggs can get around nowadays
   turningSpeed        = 60.0;	// = 180.0; minimal maneuverability -soph
   acceleration        = 80.0;	// = 70.0; light weight, mostly fuel -soph

   proximityRadius     = 1;	// = 4; -soph

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 50;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock LinearProjectileData(GrenadeCannonRocket)
{
//   scale = "10.0 20.0 10.0";
   scale = "0.5 1.0 1.0" ;
   projectileShapeName = "bomb.dts"; //weapon_missile_projectile
   emitterDelay        = 250;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.7;	// 0.01; for repulsors -soph
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 0;
   bubbleEmitTime      = 1.0;

   explosion           = "VulcanExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 200;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3250;
   lifetimeMS        = 3250;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};

datablock GrenadeProjectileData( DualCannonGrenade )	// +[soph]
{							// +
   scale                   = "0.75 0.25 0.75" ;		// +
   projectileShapeName     = "bomb.dts" ;		// +
   emitterDelay            = 250 ;			// +
   directDamage            = 0.0 ;			// +
   hasDamageRadius         = true ;			// +
   indirectDamage          = 0.7 ;			// +
   damageRadius            = 1.0 ;			// +
   radiusDamageType        = $DamageType::Missile ;	// +
   kickBackStrength        = 2500 ;			// +
							// +
   explosion               = "VulcanExplosion" ;	// +
   velInheritFactor        = 1.0 ;			// +
   splash                  = MortarSplash ;		// +
   depthTolerance          = 10.0 ;			// +
							// +
   baseEmitter             = MissileSmokeEmitter ;	// +
   bubbleEmitter           = GrenadeBubbleEmitter ;	// +
							// +
   grenadeElasticity       = 0.1 ;			// +
   grenadeFriction         = 0.4 ;			// +
   armingDelayMS           = 300 ;			// +
   muzzleVelocity          = 200 ;			// +
   drag                    = 1.0 ;			// +
							// +
   sound                   = MortarProjectileSound ;	// +
							// +
   hasLight                = true ;			// +
   lightRadius             = 6.0 ;			// +
   lightColor              = "0.5 0.5 0.5" ;		// +
							// +
   hasLightUnderwaterColor = true ; 			// +
   underWaterLightColor    = "0.5 0.5 0.5" ;		// +
} ;							// +[/soph]

datablock LinearProjectileData(GrenadeRocket)
{
//   scale = "10.0 20.0 10.0";
   projectileShapeName = "bomb.dts"; //weapon_missile_projectile
   emitterDelay        = 250;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.7;	// 0.01; for repulsors -soph
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 0;
   bubbleEmitTime      = 1.0;

   explosion           = "VulcanExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 250;
   wetVelocity       = 150;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3250;
   lifetimeMS        = 3250;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};

datablock LinearProjectileData(MechRocket)
{
   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts"; //"vehicle_grav_scout.dts";
   emitterDelay        = -1;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.45;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::MechRocket;
   kickBackStrength    = 3000;
   bubbleEmitTime      = 1.0;

   explosion           = "BluePassionMissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 150;
   wetVelocity       = 75;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3250;
   lifetimeMS        = 3250;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock SeekerProjectileData( MechHowitzerMissile )	// +[soph]
{							// +
   casingShapeName     = "weapon_missile_casement.dts" ;
   projectileShapeName = "weapon_missile_projectile.dts" ;
   emitterDelay        = 250 ;				// +
   directDamage        = 0.5 ;				// + only here for MA detection
   directDamageType    = $DamageType::MechHowitzer ;	// +
							// +
   hasDamageRadius     = true ;				// +
   indirectDamage      = 0.20 ;				// 0.375;
   damageRadius        = 6.5 ;				// +
   radiusDamageType    = $DamageType::MechRocket ;	// +
   kickBackStrength    = 800 ;				// 2000;
   bubbleEmitTime      = 1.0 ;				// +
							// +
   explosion           = "" ;				// "MissileExplosion"; -soph
   underwaterExplosion = "" ;				// "UnderwaterHandGrenadeExplosion"; -soph
   splash              = MissileSplash ;		// +
   velInheritFactor    = 0 ;				// +
							// +
   baseEmitter         = HowitzerSmokeEmitter ;		// +
   delayEmitter        = MagMissileFireEmitter ;	// +
   puffEmitter         = MissilePuffEmitter ;		// +
   bubbleEmitter       = GrenadeBubbleEmitter ;		// +
   bubbleEmitTime      = 0.0 ;				// +
							// +
   exhaustEmitter      = MissileLauncherExhaustEmitter ;
   exhaustTimeMs       = 100 ;				// +
   exhaustNodeName     = "muzzlePoint1" ;		// +
							// +
   lifetimeMS          = 5250.0 ;			// + prev 3000m	| 5000.0 ;
   muzzleVelocity      =   55.0 ;			// + prev  75.0 |  120.0 | 100.0 ;
   maxVelocity         =  700.0 ;			// + prev 825.0 | 1320.0 | 850.0 ;
   turningSpeed        = 1800.0 ;			// +
   acceleration        =  300.0 ;			// | prev 300.0 | 333.33 | 150.0 ;
							// +
   proximityRadius     = 1 ;				// +
							// +
   terrainAvoidanceSpeed         = 0 ;			// +
   terrainScanAhead              = 0 ;			// +
   terrainHeightFail             = 0 ;			// +
   terrainAvoidanceRadius        = 0 ;			// +
							// +
   flareDistance = 0 ;					// +
   flareAngle    = 0 ;					// +
							// +
   sound = MissileProjectileSound ;			// +
							// +
   hasLight    = true ;					// +
   lightRadius = 6.0 ;					// +
   lightColor  = "0.175 0.175 0.5" ;			// +
							// +
   useFlechette = true ;				// +
   flechetteDelayMs = 10 ;				// +
   casingDeb = FlechetteDebris ;			// +
							// +
   explodeOnWaterImpact = false ;			// +
} ;							// +
							// +
function MechHowitzerMissile::onExplode( %data , %proj , %pos , %mod )
{							// +
   %projectile.exploded = true ;			// +
   if( %proj.bkSourceObject )				// +
      if( isObject( %proj.bkSourceObject ) )		// +
         %proj.sourceObject = %proj.bkSourceObject ;	// +
							// +
     %modifier = 1;					// +
							// +
   if( %proj.vehicleMod )				// +
      %modifier *= %proj.vehicleMod ;			// +
							// +
   if( %data.passengerDamageMod && %proj.passengerCount )
      %modifier *= 1 + ( %proj.passengerCount * %data.passengerDamageMod ) ;
							// +
   if( %proj.damageMod > 0 )				// +
      %modifier *= %proj.damageMod ;			// +
							// +
   if( %proj.originTime )				// +
      %airTimeMod = getSimTime() - %proj.originTime ;	// +
   else							// +
      %airTimeMod = 600 ;				// +
   if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
      %airTimeDamage = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
   else							// +
      %airTimeDamage = %data.maxVelocity / %data.muzzleVelocity ;
   %damage = %data.indirectDamage * %airTimeDamage ;	// +
   %radius = ( %data.damageRadius / %airTimeDamage ) + 1 ;
							// + hacking hit detection
   %vector = vectorScale( vectorNormalize( vectorSub ( %pos , %proj.initialPosition ) ) , 0.25 ) ;
   %adjPos = vectorAdd( %pos , %vector ) ;		// +
   %hitObject = getWord( ContainerRayCast( %pos , %adjPos , $TypeMasks::PlayerObjectType |
                                                            $TypeMasks::VehicleObjectType |
                                                            $TypeMasks::StationObjectType |
                                                            $TypeMasks::GeneratorObjectType |
                                                            $TypeMasks::SensorObjectType |
                                                            $TypeMasks::TurretObjectType , %proj.sourceObject ) , 0 ) ;
							// +
   if( %hitObject && isObject( %hitObject ) )		// +
   {							// +
      if( %damage > 0.3 )				// + hacking MA detection
         checkMAHit( %proj , %hitObject , %pos ) ;	// +
      if( !%hitObject.invincible )			// +
      {							// +
         %hitObject.damage( %proj.sourceObject , %pos , %damage , %data.directDamageType );
         %hitObject.invincible = true ;			// +   
         RadiusExplosion(%proj, %pos, %radius , %damage , %data.kickBackStrength * %airTimeDamage , %proj.sourceObject , %data.directDamageType ) ;
         %hitObject.invincible = false ;		// +
      }							// +
      else						// +
         RadiusExplosion(%proj, %pos, %radius , %damage , %data.kickBackStrength * %airTimeDamage , %proj.sourceObject , %data.directDamageType ) ;
   }							// +
   else							// +
      RadiusExplosion(%proj, %pos, %radius , %damage , %data.kickBackStrength * %airTimeDamage , %proj.sourceObject , %data.directDamageType ) ;
							// +
   %p = new LinearProjectile()				// +
   {							// +
      dataBlock        = "Blowthrough" ;		// +
      initialDirection = %vector ;			// +
      initialPosition  = %pos ;				// +
      sourceObject     = %proj.sourceObject ;		// +
      bkSourceObject   = %proj.bkSourceObject ;		// +
   };							// +
   %p.ignoreReflections = true ;			// +
   MissionCleanup.add( %p ) ;				// +
}							// +[/soph]

datablock LinearProjectileData(MechRail)
{
   scale = "10.0 20.0 10.0";
   projectileShapeName = "weapon_missile_projectile.dts"; //"vehicle_grav_scout.dts";
   emitterDelay        = -1;
   directDamage        = 2.25 ;		// = 2.2; -soph
   hasDamageRadius     = true;
//   indirectDamage      = 0.475;
//   damageRadius        = 20.0;
   directDamageType    = $DamageType::MechHowitzer;
   kickBackStrength    = 8000 ;			// = 4000; -soph
   bubbleEmitTime      = 1.0;

   explosion           = "ChaingunExplosion" ;	// = "MissileExplosion"; -soph
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = HowitzerSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 1000;
   wetVelocity       = 500;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

function xMechRail::onExplode( %data , %proj , %pos , %mod , %skip )	// +[soph]
{									// +
	if( !%proj.target )						// +
		return ;						// +
	if( %proj.damageFactor > 0 )					// +
		%direction = %proj.initialDirection ;			// +
	else								// +
	{								// +
		%direction = vectorNormalize( vectorSub( %pos , %proj.initialPosition ) ) ;
		%origin = %proj.initialPosition ;			// +
	}								// +
									// +
	%proj.exploded = true ;						// +
	%type = %proj.target.getType() ;				// +
	if( %type & $TypeMasks::VehicleObjectType )			// +
	{								// +
		%vehicle = %proj.target ;				// +
		%inVehicle = true ;					// +
	}								// +
	if( %proj.target.isMounted() )					// +
		%vehicle = %proj.target.getObjectMount() ;		// +
									// +
	%FFRObject = new StaticShape()					// +
	{								// +
		dataBlock        = mReflector ;				// +
	} ;								// +
	MissionCleanup.add( %FFRObject ) ;				// +
	%FFRObject.setPosition( vectorAdd( %pos , vectorScale( %direction , -0.1 ) ) ) ;
	%FFRObject.schedule( 32 , delete ) ;				// +
									// +
	%p = new LinearProjectile()					// +
	{								// +
		dataBlock        = "Blowthrough" ;			// +
		initialDirection = %direction ;				// +
		initialPosition  = %pos ;				// +
		sourceObject     = %FFRObject ;				// +
	} ;								// +
	%p.ignoreReflections = true ;					// +
	MissionCleanup.add( %p ) ;					// +
									// +
	%range = 1500 - getDistance3D( %pos , %origin ) - %proj.damageFactor ;
	%range /= 10 ;							// +
	%endPos = vectorAdd( %origin , vectorScale( %direction , %reach ) ) ;
	if( %range > 0 )						// +
	{								// +
		%hitPos = vectorAdd( %pos , vectorScale( %direction , 0.1 ) ) ;
		%hitObj = %proj.target ;				// +
		if( %vehicle )
		{
			%hitArray_Count = 0 ;				// +
			if( %hitObj != %vehicle )
			{
				%hitArray_Obj[ %hitArray_Count ] = %hitObj ;
				%hitArray_EntryWound[ %hitArray_Count ] = %pos ;
			}
			while( %hitObj )
			{
				%hit = ContainerRayCast( %hitPos , %endPos , $TypeMasks::PlayerObjectType |
									     $TypeMasks::VehicleObjectType |
									     $TypeMasks::StationObjectType | 
									     $TypeMasks::GeneratorObjectType |
									     $TypeMasks::SensorObjectType | 
									     $TypeMasks::TurretObjectType |
									     $TypeMasks::DamagableItemObjectType |
									     $TypeMasks::TerrainObjectType |
									     $TypeMasks::InteriorObjectType |
									     $TypeMasks::ForceFieldObjectType |
									     $TypeMasks::StaticObjectType |
									     $TypeMasks::MoveableObjectType , %hitObj ) ;
				if( %hit $= "" )
				{
					%hitObj = false ;
					%hitPos = %endPos ;
				}
				else
				{
					%hitObj = getWord( %hit , 0 ) ;
					%hitPos = getWords( %hit , 1 , 3 ) ;
					if( %hitObj == %vehicle )
						%vehicle_Entrywound = %hitPos ;
					else
					{
						if( %hitObj.getObjectMount() == %vehicle )
						{
							%hitArray_Count++ ;
//							%hitArray_Obj[ %hitArray_Count ] = %hitObj ;
							%hitArray_Entrywound[ %hitArray_Count ] = %pos ;
							%hitObj.railgun_HitArray_ID = %hitArray_Count ;
						}
						else
						{
							%ignoreObj = %hitObj ;
							%hitObj = false;
						}
					}
				}
				
			}
			if( !%hitObj )					// +
				%hitObj = true ;			// +
			%vehicle_Distance = 0 ;				// +
			%scanStart = %hitPos ;				// +
			while( %hitObj )				// +
			{						// +
				%hit = ContainerRayCast( %hitPos , %pos , $TypeMasks::PlayerObjectType |
									  $TypeMasks::VehicleObjectType |
									  $TypeMasks::StationObjectType | 
									  $TypeMasks::GeneratorObjectType |
									  $TypeMasks::SensorObjectType | 
									  $TypeMasks::TurretObjectType |
									  $TypeMasks::DamagableItemObjectType |
									  $TypeMasks::TerrainObjectType |
									  $TypeMasks::InteriorObjectType |
									  $TypeMasks::ForceFieldObjectType |
									  $TypeMasks::StaticObjectType |
									  $TypeMasks::MoveableObjectType , %ignoreObj ) ;
				if( %hit $= "" )			// +
					%hitObj = false ;		// +
				else					// +
				{					// +
					if( %scanStart == %hitPos )	// +
					{				// +
						%p = new LinearProjectile()
						{			// +
							dataBlock        = "Blowthrough" ;
							initialDirection = %direction ;
							initialPosition  = %hitPos ;
							sourceObject     = %FFRObject ;
							bksourceObject   = %proj.sourceObject ;
						} ;			// +
						%p.ignoreReflections = true ;
						MissionCleanup.add( %p ) ;
									// +
						%p = new LinearProjectile()
						{			// +
							dataBlock        = "MechRail" ;
							initialDirection = %direction ;
							initialPosition  = %hitPos ;
							sourceObject     = %FFRObject ;
							damageFactor     = %proj.damageFactor + getDistance3D( %hitPos , %pos ) ;
							bksourceObject   = %proj.sourceObject ;
						} ;			// +
						MissionCleanup.add( %p ) ;
						projectileTrail( %p , 50 , LinearFlareProjectile , GaussTrailCharge , true ) ;
					}				// +
									// +
					%hitObj = getWord( %hit , 0 ) ;	// +
					%ignoreObj = %hitObj ;		// +
					%hitPos = getWords( %hit , 1 , 3 ) ;
					if( %hitObj == %vehicle )	// +
					{				// +
						%vehicle_Penetration = getDistance3D( %hitPos , %vehicle_Entrywound ) ;
						%vehicle_Passthrough = %vehicle_Penetration ;
					}				// +
					else				// +
					{				// +
						%vehicle_Passthrough -= getDistance3D( %scanPos , %hitPos ) ;
						if( %hitObj.railgun_HitArray_ID )
						{
							%distance = getDistance3D( %hitPos , %hitArray_Entrywound[ %hitObj.railgun_HitArray_ID ] ) ;

							// TODO damage and impulse here

							if( %distance < %vehicle_Passthrough ) 
							{
								%vehicle_Penetration -= %distance ;
								%vehicle_Passthrough -= %distance ;
							}
							else
								if( %vehicle_Passthrough > 0 )
								{
									%vehicle_Penetration -= %vehicle_Passthrough ;
									%vehicle_Passthrough = 0 ;
								}
							%hitObj.railgun_HitArray_ID = 0 ;
						}
					}
				}
			}

			%force = ( %range + 50 ) / 200 ;
			%factor = mCeil( %vehicle_Penetration * 10 ) ;
			%damage = %factor * %force * %data.directDamage / 50 ;
			if( %vehicle.isFF )				// +
				%vehicle.deployBase.damage( %proj.sourceObject , %vehicle_Entrywound , %damage , %data.directDamageType ) ;
			else						// +
				%vehicle.damage( %proj.sourceObject , %vehicle_Entrywound , %damage , %data.directDamageType ) ;
			if( %vehicle_Penetration > 0 )
				if(  %vehicle.usesCPS )		// +
					if( isObject(  %vehicle.shieldCap ) &&  %vehicle.shieldCap.getDatablock().maxCapacitorEnergy )
						if(  %vehicle.shieldCap.getCapacitorLevel() )
						{			// +
							%factor *= 1 - (  %vehicle.shieldCap.getCapacitorLevel() / %targetObject.shieldCap.getDatablock().maxCapacitorEnergy );
							%impulse = %force * %factor * %data.kickBackStrength ;
							%vehicle.applyImpulse( %vehicle_Entrywound , vectorScale( %direction , %impulse ) ) ;
						}			// +

			for( %i = %hitArray_Count ; %i > 0 ; %i-- )
			{
				if( %hitArray_Obj[ %i ].railgun_HitArray_ID )
				{
					%distance = getDistance3D( %hitArray_Entrywound[ %i ] , %scanStart ) ;
					%scanStart = %hitArray_Entrywound[ %i ] ;

					%factor = mceil( %distance * 10 ) ;
					%damage = %factor * %force * %data.directDamage / 50 ;
					if( %hitObj.isFF )		// +
						%hitArray_Obj[ %i ].deployBase.damage( %proj.sourceObject , %scanStart , %damage , %data.directDamageType ) ;
					else				// +
						%hitArray_Obj[ %i ].damage( %proj.sourceObject , %scanStart , %damage , %data.directDamageType ) ;
				}
				%hitArray_Obj[ %i ].railgun_HitArray_ID = "" ;
			}
			
		}
		else
		{
			if( %proj.target.getType() & $TypeMasks::TerrainObjectType )
				%mask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::DamagableItemObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticObjectType | $TypeMasks::MoveableObjectType ;
			else
				%mask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::DamagableItemObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticObjectType | $TypeMasks::MoveableObjectType ;
			%hit = ContainerRayCast( %pos , %endPos , %mask , %proj.target ) ;
			if( %hit !$= "" )				// +
				%endPos = getWords( %hit , 1 , 3 ) ;	// +
			%hit = ContainerRayCast( %endPos, %pos , $TypeMasks::PlayerObjectType |
								 $TypeMasks::VehicleObjectType |
								 $TypeMasks::StationObjectType | 
								 $TypeMasks::GeneratorObjectType |
								 $TypeMasks::SensorObjectType | 
								 $TypeMasks::TurretObjectType |
								 $TypeMasks::DamagableItemObjectType |
								 $TypeMasks::TerrainObjectType |
								 $TypeMasks::InteriorObjectType |
								 $TypeMasks::ForceFieldObjectType |
								 $TypeMasks::StaticObjectType |
								 $TypeMasks::MoveableObjectType ) ;
			if( %hit !$= "" )				// +
				%endPos = getWords( %hit , 1 , 3 ) ;	// +
									// +
			%distance = getDistance3D( %pos , %endPos ) ;	// +
			%factor = mceil( %distance * 10 ) ;		// +
			%force = ( %range + 50 ) / 200 ;		// +
			%damage = %force * %factor * %data.directDamage / 50 ;
echo( "Penetrated " @ %distance @ "m and inflicted " @ %damage @ " damage." ) ;
			if( %proj.target.isFF )				// +
				%proj.target.deployBase.damage( %proj.sourceObject , %pos , %damage , %data.directDamageType ) ;
			else						// +
				%proj.target.damage( %proj.sourceObject , %pos , %damage , %data.directDamageType ) ;
			if( %proj.target.getType() & $TypeMasks::PlayerObjectType && %proj.target.getDataBlock().shouldApplyImpulse( %proj.target ) )
				%proj.target.applyImpulse( %pos , vectorScale( %direction , %force ) ) ;
			if( %range - %distance > 0 )			// +
			{						// +
				%p = new LinearProjectile()		// +
				{					// +
					dataBlock        = "MechRail" ;	// +
					initialDirection = %direction ;	// +
					initialPosition  = vectorAdd( %endPos , vectorScale( %direction , 0.05 ) ) ;
					sourceObject     = %FFRObject ;	// +
					damageFactor     = %proj.damageFactor + %distance * 10 ;
					bksourceObject   = %proj.sourceObject ;
				} ;					// +
				MissionCleanup.add( %p ) ;		// +
				projectileTrail( %p , 50 , LinearFlareProjectile , GaussTrailCharge , true ) ;
			}						// +
		}							// +
	}								// +
}									// +
									// +
function xMechRail::onCollision( %data , %proj , %targetObject , %mod , %pos )
{									// +
	if( %proj.bkSourceObject )					// +
		if( isObject( %proj.bkSourceObject ) )			// +
			%proj.sourceObject = %proj.bkSourceObject ;	// +
									// +
	if( %targetObject.isBlastDoor && %targetObject.team == %proj.sourceObject.team )
		moveBlastDoor( %targetObject , 1 ) ;			// +
									// +
	%proj.target = %targetObject ;					// +
	%data.onExplode( %proj , %pos , %mod , %skip ) ;		// +
}									// +[/soph]

//datablock SeekerProjectileData(PumaMissile)	// overwritten in scripts/vmods/weaponMods.cs, removed -soph
//{
//   scale = "10.0 10.0 10.0";
//   casingShapeName     = "weapon_missile_casement.dts";
//   projectileShapeName = "vehicle_grav_scout.dts";
//   hasDamageRadius     = true;
//   directDamage        = 0.0;
//   indirectDamage      = 1.6;
//   damageRadius        = 25.0;
//   radiusDamageType    = $DamageType::Missile;
//   kickBackStrength    = 5500;
//   
//   explosion           = "MortarExplosion";
//   underwaterExplosion = "UnderwaterMortarExplosion";
//   splash              = MissileSplash;
//   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
//                                 // is cranked up to full so the missile doesn't start
//                                 // out behind the player when the player is moving
//                                 // very quickly - bramage
//
//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
//   puffEmitter         = MissilePuffEmitter;
//   bubbleEmitter       = GrenadeBubbleEmitter;
//   bubbleEmitTime      = 1.0;
//
//   exhaustEmitter      = MissileLauncherExhaustEmitter;
//   exhaustTimeMs       = 100;
//   exhaustNodeName     = "muzzlePoint1";
//
//   lifetimeMS          = 8000;
//   muzzleVelocity      = 45.0;
//   maxVelocity         = 133.0;
//   turningSpeed        = 75.0;
//   acceleration        = 30.0;
//
//   proximityRadius     = 4;
//
//   terrainAvoidanceSpeed         = 180;
//   terrainScanAhead              = 25;
//   terrainHeightFail             = 12;
//   terrainAvoidanceRadius        = 100;
//
//   flareDistance = 200;
//   flareAngle    = 30;
//
//   sound = MissileProjectileSound;
//
//   hasLight    = true;
//   lightRadius = 7.0;
//   lightColor  = "0.4 0.15 0.1";
//
//   useFlechette = false;
//   flechetteDelayMs = 50;
//   casingDeb = FlechetteDebris;
//
//   explodeOnWaterImpact = false;
//};

datablock LinearProjectileData( Blowthrough )
{
   scale = "0.25 0.25 0.25";
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = 250 ;
   directDamage        = 0 ;
   hasDamageRadius     = false ;
   indirectDamage      = 0.0 ;
   damageRadius        = 0.0 ;
   radiusDamageType    = $DamageType::Missile ;
   kickBackStrength    = 0 ;
   bubbleEmitTime      = 1.0 ;

   explosion           = BlowthroughExplosion ;
   splash              = UnderwaterHandGrenadeExplosion ;
   velInheritFactor    = 1.0;

   baseEmitter         = "" ;
   delayEmitter        = "" ;
   puffEmitter         = "" ;
   bubbleEmitter       = "" ;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 32.5 ;
   wetVelocity       = 32.5 ;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 32 ;
   lifetimeMS        = 32 ;
   explodeOnDeath    = true ;
   reflectOnWaterImpactAngle = 0.0 ;
   explodeOnWaterImpact      = true ;
   deflectionOnWaterImpact   = 0.0 ;
   fizzleUnderwaterMS        = 2500 ;

   activateDelayMS = -1;

   hasLight    = true ;
   lightRadius = 6.0 ;
   lightColor  = "1.0 1.0 1.0" ;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MissileLauncherAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_missile.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some missiles";

   computeCRC = true;

};

datablock TargetProjectileData(tBasicTargeter)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 325;
   beamColor           	= "0.1 1.0 0.1";

   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = true;
};

datablock ShapeBaseImageData(RocketTLImage)
{
   className = WeaponImage;
   shapeFile = "weapon_targeting.dts";
   emap = true;
   offset = "0 -0.5 0.25";
   rotation = "0 1 0 180";
//   armThread = lookms;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(MissileLauncher)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_missile.dts";
   image = MissileLauncherImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a missile launcher";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(MissileLauncherImage)
{
   className = WeaponImage;
   shapeFile = "weapon_missile.dts";
   item = MissileLauncher;
   ammo = MissileLauncherAmmo;
   offset = "0 0 0";
   armThread = lookms;
   emap = true;

//   projectile = ShoulderMissile;
   projectileType = SeekerProjectile;

   isSeeker     = true;
   seekRadius   = 500; // 600
   maxSeekAngle = 8;
   seekTime     = 0.5;
   minSeekHeat  = 0.6;

   // only target objects outside this range
   minTargetingDistance             = 20;
   reloadTime = 2900;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = MissileSwitchSound;

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "CheckWet";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.4;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = MissileFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 2.5;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MissileDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "ActivateReady";
   
   stateName[7]                     = "CheckTarget";
   stateTransitionOnNoTarget[7]     = "DryFire";
   stateTransitionOnTarget[7]       = "Fire";
   
   stateName[8]                     = "CheckWet";
   stateTransitionOnWet[8]          = "WetFire";
   stateTransitionOnNotWet[8]       = "Fire"; // CheckTarget
   
   stateName[9]                     = "WetFire";
   stateTransitionOnNoAmmo[9]       = "NoAmmo";
   stateTransitionOnTimeout[9]      = "Reload";
   stateSound[9]                    = MissileFireSound;
   stateRecoil[3]                   = LightRecoil;
   stateTimeoutValue[9]             = 0.4;
   stateSequence[3]                 = "Fire";
   stateScript[9]                   = "onWetFire";
   stateAllowImageChange[9]         = false;
};

function MissileLauncherImage::onMount(%data,%obj,%slot)
{
     %obj.mountImage(RocketTLImage, 4);
     Parent::onMount(%data,%obj,%slot);
     
     %weapon = %obj.getMountedImage(0).item;

     if(%obj.client.mode[%weapon] == 5)
          %weapon.onCycleMode(%obj, 5);
}

function MissileLauncherImage::onUnmount(%this,%obj,%slot)
{
     if(isObject(%obj.tetherBeacon))
     {
          %obj.tetherBeacon.delete();
          %obj.tetherBeacon = "";
     }

     if(isObject(%obj.t))
          %obj.t.delete();

     %obj.tetherActive = false;
     Parent::onUnmount(%this, %obj, %slot);
     %obj.unmountImage(4);
}

function MissileLauncher::onCycleMode(%weapon, %obj, %mode)
{
   if(%mode == 5)
   {
          %mVec = %obj.getMuzzleVector(4);
          %mPos = %obj.getMuzzlePoint(4);
          %nmVec = VectorNormalize(%mVec);
          %scmVec = VectorScale(%nmVec, 325);
          %mEnd = VectorAdd(%mPos, %scmVec);
          %searchResult = containerRayCast(%mPos, %mEnd, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType, %obj);
          %raycastPt = posFrRaycast(%searchResult);

          if(isObject(%obj.tetherBeacon))
          {
               %obj.tetherBeacon.delete();
               %obj.tetherBeacon = "";
          }

          if(isObject(%obj.t))
               %obj.t.delete();

          %obj.tetherBeacon = new WayPoint()
          {
               position = %raycastPt;
               rotation = "1 0 0 0";
               scale = "1 1 1";
               name = "";
               dataBlock = "WayPointMarker";
               lockCount = "0";
               homingCount = "0";
               team = %obj.client.team;
            };

            MissionCleanup.add(%obj.tetherBeacon);

           %t = new TargetProjectile()
           {
              dataBlock        = "tBasicTargeter";
              initialDirection = %obj.getMuzzleVector(4);
              initialPosition  = %obj.getMuzzlePoint(4);
              sourceObject     = %obj;
              sourceSlot       = 4;
              vehicleObject    = 0;
           };
           MissionCleanup.add(%t);
           %obj.t = %t;

          %obj.tetherActive = true;
          TetherPositionLoop(%obj, 4);
   }
   else
   {
          %obj.tetherActive = false;

          if(isObject(%obj.tetherBeacon))
          {
               %obj.tetherBeacon.delete();
               %obj.tetherBeacon = "";
          }

          if(isObject(%obj.t))
             %obj.t.delete();
   }
}

function TetherPositionLoop(%obj, %slot)
{
     if(%obj.tetherActive)
     {
        if(!isObject(%obj.tetherBeacon))
        {
          %obj.tetherBeacon = new WayPoint()
          {
               position = %raycastPt;
               rotation = "1 0 0 0";
               scale = "1 1 1";
               name = "";
               dataBlock = "WayPointMarker";
               lockCount = "0";
               homingCount = "0";
               team = %obj.client.team;
            };

            MissionCleanup.add(%obj.tetherBeacon);
        }
        
        %mVec = %obj.getMuzzleVector(%slot);
        %mPos = %obj.getMuzzlePoint(%slot);
        %nmVec = VectorNormalize(%mVec);
        %scmVec = VectorScale(%nmVec, 325);
        %mEnd = VectorAdd(%mPos, %scmVec);
        %searchResult = containerRayCast(%mPos, %mEnd, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType, %obj);
        %raycastPt = posFrRaycast(%searchResult);

         if(!%searchresult)
         {
            %pos = %obj.getMuzzlePoint(%slot);
            %nvec = vectorNormalize(%obj.getMuzzleVector(%slot));
            %vec = vectorScale(%nvec, 325);
            %nPos = vectorAdd(%pos, %vec);
            %obj.tetherBeacon.setPosition(%nPos);
         }
         else
            %obj.tetherBeacon.setPosition(%raycastPt);

         schedule(32, %obj, TetherPositionLoop, %obj, %slot);
     }
     else
     {
          if(isObject(%obj.tetherBeacon))
          {
               %obj.tetherBeacon.delete();
               %obj.tetherBeacon = "";
          }

          if(isObject(%obj.t))
             %obj.t.delete();
     }
}
