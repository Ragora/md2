if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Blaster Cannon
//--------------------------------------

datablock ParticleData(MBlasterDebrisSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.01;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;   // ...more or less

   textureName          = "flarebase";

   useInvAlpha =     false;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "1.0 0.075 0.1 1.0";
   colors[1]     = "0.5 0.15 0.075 0.75";
   colors[2]     = "0.3 0.3 0.3 0.5";
   colors[3]     = "0.05 0.05 0.01 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.625;
   sizes[2]      = 0.7;
   sizes[3]      = 0.49;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.7;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(MBlasterDebrisSmokeEmitter)
{
   ejectionPeriodMS = 16;
   periodVarianceMS = 6;

   ejectionVelocity = 0.5;  // A little oomph at the back end
   velocityVariance = 0.1;

   thetaMin         = 1.0;
   thetaMax         = 8.0;

   particles = "MBlasterDebrisSmokeParticle";
};

datablock ParticleData(MBlasterExplosionSmoke)
{
   dragCoeffiecient     = 0.5;
   gravityCoefficient   = 0;   // rises slowly
   inheritedVelFactor   = 0;
   constantAcceleration = -0.35;

   lifetimeMS           = 1750;
   lifetimeVarianceMS   = 350;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

//   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 0.1 0.1 0.0";
   colors[1]     = "0.95 0.1 0.15 1.0";
   colors[2]     = "0.75 0.1 0.1 0.75";
   colors[3]     = "0.5 0.15 0.075 0.0";
   colors[4]     = "0.1 0.1 0.1 0.0";
   colors[5]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 5.5;
   sizes[2]      = 6.1;
   sizes[3]      = 8.5;
   sizes[4]      = 10.25;
   sizes[5]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 0.35547;
   times[3]      = 0.5235;
   times[4]      = 0.83547;
   times[5]      = 1.0;
};

datablock ParticleEmitterData(MBlasterExplosionSmokeEmitter)
{
   ejectionPeriodMS = 12;
   periodVarianceMS = 4;

   ejectionOffset = 1.0;

   ejectionVelocity = 7;
   velocityVariance = 2;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 650;

   particles = "MBlasterExplosionSmoke";
};

datablock DebrisData(MBlasterExplosionDebris)
{
   emitters[0] = MBlasterDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 3.0;
   lifetimeVariance = 2.0;

  numBounces = 3;//1; --- variance can't be > num (ST)
   bounceVariance = 2;//3; --- 2/2 = range of 0-4
};

datablock ExplosionData(MBlasterExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = HeavyPlasmaExpSound;
//   particleEmitter = PlasmaExplosionEmitter;
//   particleDensity = 150;
//   particleRadius = 3.25;
   faceViewer = true;

   sizes[0] = "1.75 1.75 1.75";
   sizes[1] = "1.75 1.75 1.75";
   times[0] = 0.0;
   times[1] = 1.0;

   debris = MBlasterExplosionDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 45;
   debrisNum = 6;
   debrisNumVariance = 2;
   debrisVelocity = 15.0;
   debrisVelocityVariance = 5.0;

   emitter[0] = MBlasterExplosionSmokeEmitter;
};

datablock ParticleData(WadBlasterTrailParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 2177;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 0.354839;
    times[2] = 1;
    colors[0] = "1.000000 0.149606 0.296000 0.451613";
    colors[1] = "0.000000 0.000000 0.000000 0.709677";
    colors[2] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 1.75;
    sizes[1] = 1.35484;
    sizes[2] = 0.8;
};

datablock ParticleEmitterData(WadBlasterTrailEmitter)
{
    ejectionPeriodMS = 9;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 13.7903;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "WadBlasterTrailParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(MegaBlasterBall)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "4.5 6.0 4.5";
   faceViewer          = true;
   directDamage        = 0.4;
   directDamageType    = $DamageType::MegaBlaster;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 9.0;	// = 10.0; -soph
   kickBackStrength    = 1500;
   radiusDamageType    = $DamageType::MegaBlaster;

   explosion           = "MBlasterExplosion";
   underwaterExplosion = "UMitziBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = WadBlasterTrailEmitter;

   dryVelocity       = 350.0;
   wetVelocity       = 350.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.1 0.1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 0.1 0.1";
};

datablock LinearFlareProjectileData( TridentBlasterBall ) : MegaBlasterBall	// +[soph]
{										// +
   scale             = "2.5 15.0 2.5" ;						// +
										// +
   directDamage      = 0.18 ;	// = 0.27 ;					// +
   indirectDamage    = 0.22 ;	// = 0.33 ;					// +
   damageRadius      = 6 ;							// +
   kickBackStrength  = 1200 ;							// +
										// +
   dryVelocity       = 300.0 ;							// +
   wetVelocity       = 270.0 ;							// +
   fizzleTimeMS      = 1250 ;							// +
   lifetimeMS        = 1750 ;							// +
										// +
   lightRadius       = 5.0 ;							// +
} ;										// +
										// +
datablock LinearFlareProjectileData( TridentBlasterBall0 ) : TridentBlasterBall // +
{										// +
   scale             = "4.0 15.0 4.0" ;						// +
										// +
   directDamage      = 0.24 ;	// = 0.32 ;					// +
   indirectDamage    = 0.30 ;	// = 0.40 ;					// +
   damageRadius      = 8 ;							// +
   kickBackStrength  = 1000 ;							// +
										// +
   dryVelocity       = TridentBlasterBall.dryVelocity * 0.96 ;			// +
   wetVelocity       = TridentBlasterBall.wetVelocity * 0.96 ;			// +
   fizzleTimeMS      = 1325 ;							// +
   lifetimeMS        = 1825 ;							// +
										// +
   lightRadius       = 7.0 ;							// +
} ;										// +
										// +
datablock LinearFlareProjectileData( TridentBlasterBall1 ) : TridentBlasterBall	// +
{										// +
   scale             = "5.5 15.0 5.0" ;						// +
										// +
   directDamage      = 0.30 ;	// = 0.37 ;					// +
   indirectDamage    = 0.38 ;	// = 0.47 ;					// +
   damageRadius      = 10 ;							// +
   kickBackStrength  = 800 ;							// +
										// +
   dryVelocity       = TridentBlasterBall.dryVelocity * 0.92 ;			// +
   wetVelocity       = TridentBlasterBall.wetVelocity * 0.92 ;			// +
   fizzleTimeMS      = 1400 ;							// +
   lifetimeMS        = 1900 ;							// +
										// +
   lightRadius       = 9.0 ;							// +
} ;										// +[/soph]

datablock ShockwaveData(MegaBlasterDispWave)
{
   width = 4.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 3;
   acceleration = -6;
   lifetimeMS = 500;
   height = 0.5;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.1 0.1 0.75";
   colors[1] = "1.0 0.05 0.05 0.5";
   colors[2] = "1.0 1.0 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(MegaBlasterDispExp)
{
   soundProfile = plasmaExpSound;
   shockwave = MegaBlasterDispWave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(MegaBlasterDispCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;//0.000; --- must be > 0 (ST)
   damageRadius        = 0;
   radiusDamageType    = $DamageType::MB;
   kickBackStrength    = 0;

   explosion           = "MegaBlasterDispExp";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(KiloBlasterBall)
{
//   projectileShapeName = "grenade_flare.dts";
//   scale               = "2.0 2.0 2.0";
   scale               = "2.0 8.0 2.0";
   faceViewer          = true;
   directDamage        = 0.5;	//0.4; -soph
   directDamageType    = $DamageType::Blaster;
   hasDamageRadius     = false;
   indirectDamage      = 0.5;
   damageRadius        = 0.0;
   kickBackStrength    = 0;
   radiusDamageType    = $DamageType::Blaster;

   explosion           = "BlasterExplosion";
//   underwaterExplosion = "UMitziBlastExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = WadBlasterTrailEmitter;

   ignoreReflection = true;	// +soph

   dryVelocity       = 350.0;
   wetVelocity       = 350.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 450;
   lifetimeMS        = 850;	//600; -soph
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "1 0.1 0.1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound      = BlasterProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "1 0.1 0.1";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(MegaBlasterCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_elf_large.dts";
   image = MegaBlasterCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a mega blaster cannon";

   emap = true;
};

datablock ShapeBaseImageData(MegaBlasterCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   item = MegaBlasterCannon;
   offset = "0.1 0.1 0";
   emap = true;

   usesEnergy = true;
   fireEnergy = 47 ;	// 75; -soph
   minEnergy = 47 ;	// 75; -soph

   projectile = MegaBlasterBall;
   projectileType = LinearFlareProjectile;
   
//   muzzleFlash = MegaBlasterDispWave;
      
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.3; //0.5
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 1.5; //0.3
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
//   stateSequence[3] = "Fire";
//   stateSound[3] = BlasterFireSound;
   stateScript[3] = "onFire";
   stateShockwave[3] = true;   
   
   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
//   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
//   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

datablock ShapeBaseImageData(MBlasterGunAImage) : MegaBlasterCannonImage
{
   shapeFile = "weapon_energy.dts";
   offset = "0.1 0.4 0";
   emap = true;

   stateSequence[3] = "Fire";   
   stateScript[3] = "";   
   stateTimeoutValue[3] = 0.75;
};

datablock ShapeBaseImageData(MBlasterGunBImage) : MegaBlasterCannonImage
{
   shapeFile = "weapon_energy.dts";
   offset = "0.1 0.4 0";
   rotation = "0 1 0 135";
   emap = true;

   stateSequence[3] = "Fire";
   stateScript[3] = "";   
   stateTimeoutValue[3] = 0.75;   
};

datablock ShapeBaseImageData(MBlasterGunCImage) : MegaBlasterCannonImage
{
   shapeFile = "weapon_energy.dts";
   offset = "0.1 0.4 0"; //.075";
   rotation = "0 1 0 -135";
   emap = true;

   stateSequence[3] = "Fire"; 
   stateScript[3] = "";   
   stateTimeoutValue[3] = 0.75;   
};

function MegaBlasterCannonImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(MBlasterGunAImage, 4);
   %obj.mountImage(MBlasterGunBImage, 5);
   %obj.mountImage(MBlasterGunCImage, 6);
}

function MegaBlasterCannonImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

function MegaBlasterCannonImage::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);
     
     if(%p)
     {
          %obj.play3D(PulseCannonFire);
          spawnProjectile(%p, LinearFlareProjectile, MegaBlasterDispCharge);
          %p.ignoreReflections = true;        
          %obj.setImageTrigger(4, true);
          %obj.setImageTrigger(5, true);
          %obj.setImageTrigger(6, true);                    
          %obj.setImageTrigger(4, false);
          %obj.setImageTrigger(5, false);
          %obj.setImageTrigger(6, false);                    
     }
}
