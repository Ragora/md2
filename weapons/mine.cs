// ----------------------------------------------
// mine script
// ----------------------------------------------

$TeamDeployableMax[MineDeployed]		= 100;

// ----------------------------------------------
// force-feedback datablocks
// ----------------------------------------------

// ----------------------------------------------
// audio datablocks
// ----------------------------------------------

datablock AudioProfile(MineDeploySound)
{
   filename = "fx/weapons/mine_deploy.wav";
   description = AudioClose3D;
   preload = true;
};

datablock AudioProfile(MineExplosionSound)
{
   filename = "fx/weapons/mine_detonate.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterMineExplosionSound)
{
   filename = "fx/weapons/mine_detonate_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(DeployablesExplosionSound)
{
   filename = "fx/explosions/deployables_explosion.wav";
   description = AudioExplosion3d;
   preload = true;
};

//datablock AudioProfile(ReaverExplosionSound)	// -[soph]
//{						// redundant with DeployablesExplosionSound
//   filename = "fx/explosions/deployables_explosion.wav";
//   description = AudioExplosion3d;
//   preload = true;
//};						// -[/soph]
//--------------------------------------------------------------------------
// Mine Particle effects
//--------------------------------------------------------------------------
datablock ParticleData(MineExplosionBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 750;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 1.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};
datablock ParticleEmitterData(MineExplosionBubbleEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 2.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MineExplosionBubbleParticle";
};
datablock ParticleData( UnderwaterMineCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0] = "0.5 0.5 1.0 1.0";
   colors[1] = "0.5 0.5 1.0 1.0";
   colors[2] = "0.5 0.5 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( UnderwaterMineCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "UnderwaterMineCrescentParticle";
};

datablock ParticleData(UnderwaterMineExplosionSmoke)
{
   dragCoeffiecient     = 105.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.025;
   constantAcceleration = -1.0;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 00;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.7 0.7 1.0 1.0";
   colors[1]     = "0.3 0.3 1.0 1.0";
   colors[2]     = "0.0 0.0 1.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(UnderwaterMineExplosionSmokeEmitter)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 0;

   ejectionVelocity = 4.25;
   velocityVariance = 1.25;

   thetaMin         = 0.0;
   thetaMax         = 80.0;

   lifetimeMS       = 250;

   particles = "UnderwaterMineExplosionSmoke";
};

datablock ExplosionData(UnderwaterMineExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.4 0.4 0.4";
   sizes[1] = "0.4 0.4 0.4";
   soundProfile   = UnderwaterMineExplosionSound;
   faceViewer     = true;

   emitter[0] = UnderwaterMineExplosionSmokeEmitter;
   emitter[1] = UnderwaterMineCrescentEmitter;
   emitter[2] = MineExplosionBubbleEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "50.0 50.0 50.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

//--------------------------------------------------------------------------
// Mine Particle effects
//--------------------------------------------------------------------------
datablock ParticleData( MineCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0] = "1.0 0.8 0.2 1.0";
   colors[1] = "1.0 0.4 0.2 1.0";
   colors[2] = "1.0 0.0 0.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MineCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "MineCrescentParticle";
};

datablock ParticleEmitterData( ProtCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "MineCrescentParticle";
};

datablock ParticleData(MineExplosionSmoke)
{
   dragCoeffiecient     = 105.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 00;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.2 0.2 0.2 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(MineExplosionSmokeEmitter)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 0;

   ejectionVelocity = 4.25;
   velocityVariance = 1.25;

   thetaMin         = 0.0;
   thetaMax         = 80.0;

   lifetimeMS       = 250;

   particles = "MineExplosionSmoke";
};

datablock ExplosionData(MineExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   soundProfile   = MineExplosionSound;
   faceViewer     = true;

   emitter[0] = MineExplosionSmokeEmitter;
   emitter[1] = MineCrescentEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "50.0 50.0 50.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

datablock ExplosionData(ReaverExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed      = 0.5;
//   sizes[0] = "0.75 0.75 0.75";
//   sizes[1] = "0.75 0.75 0.75";
   soundProfile   = DeployablesExplosionSound ;	// ReaverExplosionSound; -soph
   faceViewer     = true;

   emitter[0] = MineExplosionSmokeEmitter;
   emitter[1] = MineCrescentEmitter;

   shakeCamera = false;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 1.0;
   camShakeRadius = 15.0;
};

datablock ExplosionData(UnderwaterReaverExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.4 0.4 0.4";
   sizes[1] = "0.4 0.4 0.4";
   soundProfile   = DeployablesExplosionSound ;	// ReaverExplosionSound; -soph
   faceViewer     = true;

   emitter[0] = UnderwaterMineExplosionSmokeEmitter;
   emitter[1] = UnderwaterMineCrescentEmitter;
   emitter[2] = MineExplosionBubbleEmitter;

   shakeCamera = false;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

// ----------------------------------------------
// Item datablocks
// ----------------------------------------------

datablock ItemData(MineDeployed)
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.2;
   explosion = MineExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 0.60;	// 1.1; -soph
   damageRadius = 6.0;
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 1500;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 4.0; // how close together mines can be
	proximity = 2.5; // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 9; // try to deploy this many times before detonating
};

datablock ItemData(Mine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = MineDeployed;
	pickUpName = "some mines";
};

datablock ItemData(LargeMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "mine.dts";
   scale = "2 2 2";
   mass = 5.0 ;			// 0.75; -soph
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.1;
   explosion = ImpactMortarExplosion;
   underwaterExplosion = UnderwaterMortarExplosion;
   indirectDamage = 3.0;
   damageRadius = 15.0;
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 3000;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 8.0; // how close together mines can be
	proximity = 5; // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 9; // try to deploy this many times before detonating
};

datablock ItemData(LargeMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = LargeMineDeployed;
	pickUpName = "some large mines";
};

datablock ItemData(CloakMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.2;
   explosion = MineExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 0.7;	// 1.1; -soph
   damageRadius = 2.5 ;		// 1.0; -soph
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 1000;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 3 ;		// 4.0; -soph // how close together mines can be
	proximity = 2.5; // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 9; // try to deploy this many times before detonating
};

datablock ItemData(CloakMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = CloakMineDeployed;
	pickUpName = "some cloaking mines";
};

datablock ItemData(EMPMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.2;
   explosion = EMPExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 2.0 ;	// 0.55; -soph
   damageRadius = 7.5 ;		// 3.0; -soph
   radiusDamageType = $DamageType::EMP;
   kickBackStrength = 0;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 3 ;		// 4.0; -soph // how close together mines can be
	proximity = 2.5; // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 9; // try to deploy this many times before detonating
};

datablock ItemData(EMPMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = EMPMineDeployed;
	pickUpName = "some EMP mines";
};

datablock ItemData(IncendiaryMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.2;
   explosion = IncendiaryExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 0.2 ;	// = 0.001; -soph
   damageRadius = 10.0;
   radiusDamageType = $DamageType::Burn;
   kickBackStrength = 0;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 4.0; // how close together mines can be
	proximity = 2.5; // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 9; // try to deploy this many times before detonating
};

datablock ItemData(IncendiaryMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = IncendiaryMineDeployed;
	pickUpName = "some incendiary mines";
};

datablock ItemData(BlastechMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "mine.dts";
   scale = "0.3 0.3 0.3" ;	// +soph
   mass = 0.25 ;		// 0.75 -soph
   elasticity = 0.2;
   friction = 6.0 ;		// 0.6; -soph
   pickupRadius = 3;
   maxDamage = 0.2 ;		// 0.2;	-soph
   explosion = "" ;
   underwaterExplosion = "" ;
   indirectDamage = 0.125 ;	// 0.45; // 0.75; -soph
   damageRadius = 5.0 ;		// 6.0; -soph
   radiusDamageType = $DamageType::BTMine;
   kickBackStrength = 1500;
	aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 1.0 ;		// 4.0; -soph // how close together mines can be
	proximity = 2.0 ;	// 2.5; -soph // how close causes a detonation (by player/vehicle)
	armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 9; // try to deploy this many times before detonating
};

datablock ItemData(BlastechMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = BlastechMineDeployed;
	pickUpName = "some anti-blastech mines";
};
