// grenade (thrown by hand) script
// ------------------------------------------------------------------------

datablock ItemData(FlashGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = FlashGrenadeExplosion;
   indirectDamage      = 0.5;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 1000;

   computeCRC = true;

    maxWhiteout = 1.2;
};

datablock ItemData(FlashGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlashGrenadeThrown;
	pickUpName = "some flash grenades";
	isGrenade = true;

   computeCRC = true;

};

//--------------------------------------------------------------------------
// Functions:
//--------------------------------------------------------------------------
function FlashGrenadeThrown::onCollision( %data, %obj, %col )
{
   // Do nothing...
}

