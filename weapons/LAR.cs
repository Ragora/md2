if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Laser Assault Rifle
//--------------------------------------

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(RFLAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "beacon.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a laser assault rifle battery";

   emap = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(RFL)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_sniper.dts";
   image = RFLImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a laser assault rifle";
};

datablock ShapeBaseImageData(RFLImage)
{
   className = WeaponImage;
   item = RFL;
   ammo = RFLAmmo;
   emap = true;
   shapeFile = "weapon_sniper.dts";
   offset = "-0.025 0.725 -0.115";
   
//   projectile = RFLBolt;
//   projectileType = TracerProjectile;
//   projectileSpread = 6 / 998;
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = GrenadeSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.05;
   stateFire[3] = true;
//   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateScript[3]                   = "onFire";
//   stateSound[3] = RFLFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.001;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = GrenadeDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";

   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire";
};

datablock ShapeBaseImageData(RFLDecalAImage)
{
   shapeFile = "weapon_elf.dts";
   offset = "-0.05 0.1 -0.1";
   emap = true;
};

datablock ShapeBaseImageData(RFLDecalBImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "-0.05 0.45 0";
//   rotation = "0 1 0 0";
   rotation = calcThisRotD("0 90 90");
   emap = true;
};

datablock ShapeBaseImageData(RFLDecalCImage)
{
   shapeFile = "weapon_elf.dts";
   rotation = "0 1 0 180";
   offset = "0 0.1 0.1";
   emap = true;
};

function RFLImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(RFLDecalAImage, 4);
   %obj.mountImage(RFLDecalBImage, 5);
   %obj.mountImage(RFLDecalCImage, 6);
}

function RFLImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

function ChargeRFL(%player, %datablock)       // should work....
{
   %ammo = %player.getInventory(RFLAmmo);
   %aPack = %player.hasAmmoPack ? 25 : 0;   
   %ammoMax = %player.getDatablock().max[RFLAmmo] + %aPack;

   if(%ammo > -1)   // jury rig for now
   {
      if(%ammo < %ammoMax)
      {
         if(!%player.isEMP)
         {
            %pct = %player.getEnergyPct();

//            if(%pct >= 0.25)
               %player.incInventory(RFLAmmo, 1);

            %player.chargingRFL = true;
         }
//         else
//            %player.setInventory(RFLAmmo, 0);


         if(%pct == 1)
         {
            if(%player.isSyn)
               %mult = 375; //250
            else
               %mult = 750; //500
         }
         else
         {
            if(%player.isSyn)
               %mult = 750; //500
            else
               %mult = 1500; //1000
         }

         schedule(%mult, %player, "ChargeRFL", %player, %datablock);
      }
      else
         %player.chargingRFL = false;
   }
}

datablock SniperProjectileData(RFLShortRangeL)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 75;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.29;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

function RFLShortRangeL::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %damageAmount = %projectile.damageFactor;

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   %modifier = 1;

   if(%targetObject.getType() & $TypeMasks::PlayerObjectType)
   {
        %targetObject.getOwnerClient().headShot = 0;   

        if(%projectile.plasmaBeam && getRandom() < 0.05)
            %targetObject.burnObject(%projectile.sourceObject);
   }
   
   %ddt = %projectile.plasmaBeam ? $DamageType::RFLPlasma : $DamageType::RFL;

   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);
      
   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %ddt);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %ddt);
}

datablock SniperProjectileData(RFLLongRangeL)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "SniperExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::MechLaser;

   maxRifleRange       = 150;
   rifleHeadMultiplier = 1.3;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   startBeamWidth		  = 0.29;
   endBeamWidth 	     = 0.5;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 6.0;
   pulseLength         = 0.150;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

function RFLLongRangeL::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
   %damageAmount = %projectile.damageFactor;

   if(%projectile.bkSourceObject)
      if(isObject(%projectile.bkSourceObject))
         %projectile.sourceObject = %projectile.bkSourceObject;

   %modifier = 1;

   if(%targetObject.getType() & $TypeMasks::PlayerObjectType)
   {
        %targetObject.getOwnerClient().headShot = 0;   

        if(%projectile.plasmaBeam && getRandom() < 0.05)
            %targetObject.burnObject(%projectile.sourceObject);
   }
   
   %ddt = %projectile.plasmaBeam ? $DamageType::RFLPlasma : $DamageType::RFL;

   if(%targetObject.isBlastDoor && %targetObject.team == %projectile.sourceObject.team)
      moveBlastDoor(%targetObject, 1);
      
   if(%targetObject.isFF)
          %targetObject.deployBase.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %ddt);
   else
          %targetObject.damage(%projectile.sourceObject, %position, %damageAmount * %modifier, %ddt);
}

datablock TargetProjectileData(RFLShortBeamL)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 75;
   beamColor           	= "0.1 0.1 1.0";
								
   startBeamWidth			= 0.40;
   pulseBeamWidth 	   = 0.65;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 36.0;
   pulseLength         	= 0.15;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

datablock TargetProjectileData(RFLLongBeamL)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 150;
   beamColor           	= "0.1 1.0 0.1";
								
   startBeamWidth			= 0.45;
   pulseBeamWidth 	   = 0.8;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 40.0;
   pulseLength         	= 0.225;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = false;
};

function RFLImage::onFire(%data,%obj,%slot)
{
   %weapon = %obj.getMountedImage(0).item;

   if(true)
   {
      %mode = %obj.client.mode[%weapon];
      %strength = %mode ? 0.0625 : 0.15 ;			// = %mode ? 0.075 : 0.15; -soph
      %laserProj = %mode ? "RFLLongRangeL" : "RFLShortRangeL";
      %beamProj = %mode ? "RFLLongBeamL" : "RFLShortBeamL";
            
      %fireTime =  250;
      %beamShowTime = mFloor(%fireTime * 0.8);
      
      if(%obj.isWet)
      {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         return;
      }

      if(%obj.SR36FireTimeout)
           return;

       %ammoUse = getRandom(1, 3);
       %rnd = %mode ? %ammoUse * 2 : %ammoUse;
       
       if(%obj.getInventory("RFLAmmo") < %rnd)
       {
         serverPlay3D(SniperRifleDryFireSound, %obj.getTransform());
         %obj.SR36FireTimeout = %fireTime;
         schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);
         return;
       }

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

     %dmgMod = 1;

   // Vehicle Damage Modifier
   if(%vehicle)
     %dmgMod = 1 + %vehicle.damageMod;

   if(%obj.damageMod)
      %dmgMod += %obj.damageMod - 1;

//   if(%obj.synMod)
//     %dmgMod += %obj.synMod * %obj.isSyn;
   
      %pct = 0; //0.2 * %rnd;
      %p = new SniperProjectile()
      {
         dataBlock = %laserProj;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         damageFactor     = %strength * %rnd * %dmgMod;
         sourceSlot       = %slot;
//         plasmaBeam       = %obj.heatShielded;
      };
      %p.setEnergyPercentage(%pct);

      %obj.lastProjectile = %p;
      MissionCleanup.add(%p);

      %t = new TargetProjectile()
      {
          dataBlock        = %beamProj;
          initialDirection = %obj.getMuzzleVector(%slot);
          initialPosition  = %obj.getMuzzlePoint(%slot);
          sourceObject     = %obj;
          sourceSlot       = %slot;
          vehicleObject    = 0;
      };

      MissionCleanup.add(%t);
      %t.schedule(%beamShowTime, delete);
                     
      %sound = getRandomB() ? SniperRifleFireSound : SniperRifleFireWetSound;
      serverPlay3D(%sound, %obj.getTransform());

      // AI hook
      if(%obj.client)
         %obj.client.projectile = %p;

      %obj.SR36FireTimeout = 250;
      schedule(%obj.SR36FireTimeout, 0, "deleteSR36FireTimeout", %obj);

      %obj.decInventory("RFLAmmo", %rnd);
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
   }
   
   if(!%obj.chargingRFL)
      ChargeRFL(%obj, %data);
}
