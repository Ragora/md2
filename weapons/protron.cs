if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();


// MrKeen - moved to Chaingun.cs
//-Nite-1501 fixed dm for green pulse
//

datablock LinearFlareProjectileData(FissionBolt)
{
   directDamage        = 0.1;
   directDamageType    = $DamageType::FissionBlaster;
   doDynamicClientHits = true;

   explosion           = "SentryTurretExplosion";
   kickBackStrength  = 0.0;
   activateDelayMS = -1;

   dryVelocity       = 1500.0;
   wetVelocity       = 750.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   numFlares         = 10;
   size              = 0.35;
   flareColor        = "1 0.35 0.35";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "1 0.35 0.35";
};

datablock GrenadeProjectileData(ProtronFireSpray)	// +[soph]
{
   projectileShapeName = "turret_muzzlePoint.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.05;
   damageRadius        = 2.0;
   radiusDamageType    = $DamageType::Burn ;	// $DamageType::ProtronFire;
   kickBackStrength    = 0;
   bubbleEmitTime      = 1.0;

   sound               = PlasmaProjectileSound;
   explosion           = "";
   underwaterExplosion = "";
   velInheritFactor    = 1.0;
   splash              = GrenadeSplash;

   baseEmitter         = PlasmaRifleEmitter ;
   bubbleEmitter       = MortarBubbleEmitter ;

   lifetimeMS        = 10000;

   grenadeElasticity = 0.35;
   grenadeFriction   = 0.75;
   armingDelayMS     = 250;
   muzzleVelocity    = 15.00;
   drag = 0.75;
};

function ProtronFireSpray::onExplode(%data, %proj, %pos, %mod)
{
     createLifeEmitter( %pos , "MissileSmokeSpikeEmitter" , 100 );
     Parent::onExplode(%data, %proj, %pos, %mod);
     IncendiaryExplosion( %pos , %data.damageradius , 0.3 , %proj.sourceObject );
}

datablock LinearProjectileData(ProtronFireBolt)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   scale               = "3 3 3";
   faceViewer          = true;
   directDamage        = 0.06;
   indirectDamage      = 0.0;
   kickBackStrength    = 0.0;
   radiusDamageType    = $DamageType::ProtronFire ;

   explosion           = FlareGrenadeExplosion ;
   splash              = PlayerSplash ;
   baseEmitter         = PlasmaSplashEmitter ;
   bubbleEmitter       = MortarBubbleEmitter ;

   dryVelocity       = 225;
   wetVelocity       = 135;
   velInheritFactor  = 0.3;
   fizzleTimeMS      = 150;
   lifetimeMS        = 150;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 0;

   activateDelayMS = -1;

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "0.5 0.25 0.1 0.5";
};

function ProtronFireBolt::onExplode(%data, %proj, %pos, %mod)
{
     Parent::onExplode(%data, %proj, %pos, %mod);

     createLifeEmitter( vectorAdd( %pos , "0 0 -0.25" ) , "PlasmaExplosionEmitter" , 100 );
     IncendiaryExplosion( %pos , 0.5 , 0.27 , %proj.sourceObject );
}							// +[/soph]

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(ProtronImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item      = Protron;
   ammo 	 = ProtronAmmo;
   emap = true;

   projectileSpread = 5.8 / 1000.0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = ChaingunSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateScript[2] = "onCharge";
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateScript[3]           = "onSpinup";	// +soph
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.2;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
//   stateSound[4]            = ChaingunFireSound;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   //
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 2.5;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   //--------------------------------------
   stateName[7]       = "DryFire";
//   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ItemData(ProtronAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some protron ammo";
   cantPickup = true;
};

datablock ItemData(Protron)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_chaingun.dts";
   image        = ProtronImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   pickUpName   = "a protron";
   cantPickup = true;

   emap = true;
};

function ProtronImage::onSpinup(%data,%obj,%slot)
{
   %obj.wep_reloading[%data] = 3;
}

function ProtronImage::onCharge(%data,%obj,%slot)
{
   if(!%obj.chargingProt)
      ChargeProtron(%obj, %data);
}

function ChargeProtron(%player, %datablock)       // should work....
{
   %ammo = %player.getInventory(ProtronAmmo);
   %aPack = %player.hasAmmoPack ? 150 : 0;
   %aPack = %player.getDatablock().isEngineer ? 50 : %aPack;	// Engineer exception +soph
   %ammoMax = %player.getDatablock().max[ProtronAmmo] + %aPack;

   if( %player.getInventory(Protron) )				// +soph (%ammo > -1)   // jury rig for now
   {
      if(%ammo < %ammoMax)
      {
         if(!%player.isEMP)
         {
            %pct = %player.getEnergyPct();

//            if(%pct >= 0.25)
               %player.incInventory(ProtronAmmo, 1);

            %player.chargingProt = true;
         }

         if(%pct == 1)
         {
            if(%player.isSyn)
               %mult = 500; //250
            else
               %mult = 750; //500
         }
         else
         {
            if(%player.isSyn)
               %mult = 750; //500
            else
               %mult = 1000; //1000
         }

         schedule(%mult, %player, "ChargeProtron", %player, %datablock);
      }
      else
         %player.chargingProt = false;
   }
   else if (%ammo > -1 )					// +[soph]
   {
      schedule( 1000 , %player, "ChargeProtron", %player, %datablock);
   }
   else
      %player.chargingProt = false;				// +[/soph]
}

function ProtronImage::onFire(%data, %obj, %slot)
{
   %weapon = %obj.getMountedImage(0).item;
   %energyUse = 1;

   if(%obj.client.mode[%weapon] == 0)
   {
      %rnd = getRandom(4);
      switch(%rnd)
      {
         case 0:
         %projectile = "ProtronBoltR";

         case 1:
         %projectile = "ProtronBoltG";

         case 2:
         %projectile = "ProtronBoltB";

         case 3:
         %projectile = "ProtronBoltY";

         case 4:
         %projectile = "ProtronBoltW";

         case 5:
         %projectile = "ProtronBoltC";

         default:
         %projectile = "PulseBolt"; //lol
      }

      %energyUse = 1;
      %spreadMultiplier = 1;			// +soph
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
      %projectile = "PulseBolt";
      %energyUse = 2;
      %spreadMultiplier = 1;			// +soph
   }
   else if(%obj.client.mode[%weapon] == 2)
   {
//      %energyUse = 3;				// -soph
//      %projectile = "BlueEnergyBolt";		// -soph
	
      protronSpray( %data, %obj, %slot );	// +soph
      return;					// +Soph
   }
   else if(%obj.client.mode[%weapon] == 3)
   {
      %energyUse = 5;
      %projectile = "FissionBolt";
      %spreadMultiplier = 1;			// +soph
   }

   if(%obj.getInventory(%data.ammo) < %energyUse)
   {
      %obj.play3d(ChaingunDryFireSound);
      return;
   }

//   %obj.lightStart = getSimTime();

   %vehicle = 0;

   %vector = %obj.getMuzzleVector(%slot);
   %x = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread; 
   %y = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread; 
   %z = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread; 
   %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
   %vector = MatrixMulVector(%mat, %vector);

   if(%obj.client.mode[%weapon] == 0)
      %mode = "LinearFlareProjectile";
   else if(%obj.client.mode[%weapon] == 1)
      %mode = "LinearFlareProjectile";
   else if(%obj.client.mode[%weapon] == 2)
      %mode = "EnergyProjectile";
   else if(%obj.client.mode[%weapon] == 3)
      %mode = "LinearFlareProjectile";
      
   %p = new(%mode)()
   {
      dataBlock        = %projectile;
      initialDirection = %vector;
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
      vehicleObject    = %vehicle;
   };
   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
      %p.damageMod = %obj.damageMod;
   else
      %p.damageMod = 1;

   if(%obj.synMod)
     %p.damageMod += %obj.synMod * %obj.isSyn;

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

//   if(%obj.client.mode[%weapon] == 0)
//      %obj.useEnergy(0.93);
//      %obj.decInventory(%data.ammo,1);
//   if(%obj.client.mode[%weapon] == 1)
//      %obj.useEnergy(1.36);
//      %obj.decInventory(%data.ammo,2);
//   if(%obj.client.mode[%weapon] == 2)
//      %obj.useEnergy(4);
//      %obj.decInventory(%data.ammo,5);

   %obj.play3d(FChaingunFireSound);
   %obj.decInventory(%data.ammo, %energyUse);
//   FlareSet.add(%p);

   if(!%obj.chargingProt)
      ChargeProtron(%obj, %data);
}

function protronSpray( %data, %obj, %slot )	// +[soph]
{
   %useEnergyObj = %obj.getObjectMount() ;
   if( !%useEnergyObj )
      %useEnergyObj = %obj ;
   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0 ;
   if( %vehicle )
      %vehicleMod = %vehicle.damageMod ;

   %vector = %obj.getMuzzleVector( %slot ) ;
   %muzzlePosition = %obj.getMuzzlePoint( %slot ) ;

   %num = %obj.inv[ %data.ammo ] ;
   %obj.inv[ %data.ammo ] = 0 ;
   %obj.setInventory( %data.ammo , %num ) ;

   for( %i = 0 ; %i < 4 ; %i++ )
      schedule( %i * 32 , 0 , protronSprayShot , %data , %obj , %slot , %muzzlePosition , %vector , %vehicleMod );

   %obj.play3d( FChaingunFireSound ) ;

   if( !%obj.chargingProt )
      ChargeProtron( %obj , %data ) ;
}

function protronSprayShot( %data , %obj , %slot , %position , %vector , %vehicleMod )
{
   if( %obj.getInventory( %data.ammo ) < 1 )
      return;

   %x = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread * 2 ;
   %y = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread * 2 ;
   %z = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread * 2 ;
   %mat = MatrixCreateFromEuler( %x @ " " @ %y @ " " @ %z ) ;
   %newVector = MatrixMulVector( %mat , %vector ) ;

   if( !getRandom(4) )
   {
      %p = new GrenadeProjectile()
      {
         dataBlock        = ProtronFireSpray ;
         initialDirection = MatrixMulVector( MatrixCreateFromEuler( ( %x * 30 ) @ " " @ ( %y * 30 ) @ " " @ ( %z * 30 ) ) , %vector ) ;
         initialPosition  = %position ;
         sourceObject     = %obj ;
         sourceSlot       = %slot ;
      };

      if( %vehicleMod )
         %p.vehicleMod = %vehicleMod ;

      if( %obj.damageMod )
         %p.damageMod = %obj.damageMod ;
      else
         %p.damageMod = 1 ;

      MissionCleanup.add(%p);

      %obj.inv[ %data.ammo ]-- ;
      if( %obj.getInventory( %data.ammo ) < 1 )
         return ;
   }

   %p = new LinearProjectile()
   {
      dataBlock        = ProtronFireBolt ;
      initialDirection = %newVector ;
      initialPosition  = %position ;
      sourceObject     = %obj ;
      sourceSlot       = %slot ;
   } ;

   if( %vehicleMod )
      %p.vehicleMod = %vehicleMod ;

   if( %obj.damageMod )
      %p.damageMod = %obj.damageMod ;
   else
      %p.damageMod = 1 ;

   if( %obj.synMod )
     %p.damageMod += %obj.synMod * %obj.isSyn ;

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile )
      %obj.lastProjectile.delete() ;

   %obj.lastProjectile = %p ;
   %obj.deleteLastProjectile = %data.deleteLastProjectile ;

   if( %obj.client )
      %obj.client.projectile = %p ;

   MissionCleanup.add( %p ) ;

   %obj.inv[ %data.ammo ]-- ;
}						// +[/soph]

datablock ShapeBaseImageData(GBDecalAImage)
{
   shapeFile = "ammo_mortar.dts";
   offset = "-0.05 0.25 0.1";
   rotation = "0 0 1 90";
};

datablock ShapeBaseImageData(GBDecalBImage)
{
   shapeFile = "ammo_plasma.dts";
   offset = "-0.1 -0.25 -0.1";
   rotation = "1 0 0 90";
};

datablock ShapeBaseImageData(GBDecalCImage)
{
   shapeFile = "ammo_plasma.dts";
   offset = "0.25 -0.25 -0.1";
   rotation = "1 0 0 90";
};

function ProtronImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
//   %obj.createSlotExtension(0);
//   %obj.createSlotExtension(1);

   %obj.mountImage(GBDecalAImage, 4);
   %obj.mountImage(GBDecalBImage, 5);
   %obj.mountImage(GBDecalCImage, 6);

//   %obj.slotExtension[0].mountImage(ASRDecalAImage, 0);
//   %obj.slotExtension[0].mountImage(ASRDecalBImage, 1);
//   %obj.slotExtension[0].mountImage(ASRDecalCImage, 2);
//   %obj.slotExtension[0].mountImage(LaserCannonDecalBImage, 3);
//   %obj.slotExtension[0].mountImage(LaserCannonDecalCImage, 4);
//   %obj.slotExtension[0].mountImage(SGDecalAImage, 5);
//   %obj.slotExtension[0].mountImage(SGDecalBImage, 6);
//   %obj.slotExtension[0].mountImage(SGDecalCImage, 7);

//   %obj.slotExtension[1].mountImage(ERDecalAImage, 0);
//   %obj.slotExtension[1].mountImage(ERDecalBImage, 1);
//   %obj.slotExtension[1].mountImage(ERDecalCImage, 2);
//   %obj.slotExtension[1].mountImage(ERDecalDImage, 3);
//   %obj.slotExtension[1].mountImage(SGDecalAImage, 4);
//   %obj.slotExtension[1].mountImage(SGDecalBImage, 5);
//   %obj.slotExtension[1].mountImage(SGDecalCImage, 6);
//   %obj.slotExtension[1].mountImage(RipperDecalAImage, 7);
}

function ProtronImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);

//   %obj.deleteSlotExtension(0);
//   %obj.deleteSlotExtension(1);
}
