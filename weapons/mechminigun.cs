if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// MechMinigun
//--------------------------------------
datablock AudioProfile(MechGunActivate)
{
   filename    = "fx/weapons/grenadelauncher_activate.wav";
   description = AudioDefault3d;
   preload = true;
};
//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MechMinigunAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some mechminigun ammo";
};

datablock TracerProjectileData(MechMiniBullet)
{
   doDynamicClientHits = true;

   directDamage        = 0.1;
   directDamageType    = $DamageType::Bullet;
   explosion           = "ChaingunExplosion";
   underwaterExplosion = "ChaingunUnderwaterExplosion";
   splash              = ChaingunSplash;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   dryVelocity       = 425.0;
   wetVelocity       = 312.5;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.10;
   crossSize       = 0.20;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

function MechMiniBullet::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
  if(detectIsWet(%position))
     serverPlay3d(getRandomChaingunWetSound(), %position);
   else
      serverPlay3d(getRandomChaingunSound(), %position);

  Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(MechMinigunImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item      = MechMinigun;
   ammo 	 = MechMinigunAmmo;
   projectile = MechMiniBullet;
   projectileType = TracerProjectile;
   emap = true;

//   casing              = ShellDebris;
//   shellExitDir        = "1.0 0.3 1.0";
//   shellExitOffset     = "0.15 -0.56 -0.1";
//   shellExitVariance   = 15.0;
//   shellVelocity       = 3.0;

   projectileSpread = 7.5 / 1000.0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = MechGunActivate;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateScript[3]       = "onSpinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = ChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   stateEmitter[4]       = "ChaingunFireSmoke";
//   stateEmitterTime[4]       = 0.15;
   stateEmitterNode[4]       = 0;

   stateTimeoutValue[4]          = 0.16 ;	// = 0.193; -soph
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateScript[5]     = "onSpindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   stateScript[6]     = "onSpindown";
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(MMGun2Image) : MechMinigunImage
{
   shapeFile = "weapon_chaingun.dts";
   shellExitDir        = "-1.0 0.3 1.0";
   offset = "-1.25 0 0";
   //offset = "-2.5 0 0";
   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(MMGun3Image) : MechMinigunImage
{
   shapeFile = "weapon_chaingun.dts";
   offset = "0 0 0.3";
  // offset = "0 0 0.6";
   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(MMGun4Image) : MechMinigunImage
{
   shapeFile = "weapon_chaingun.dts";
   shellExitDir        = "-1.0 0.3 1.0";
   offset = "-1.25 0 0.3";
   //offset = "-2.5 0 0.6";
  stateScript[3] = "onFire";
};

datablock ItemData(MechMinigun)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_chaingun.dts";
   image        = MechMinigunImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   pickUpName   = "a mechminigun";

   emap = true;
};

function MechMinigunImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(MMGun2Image, 4);
   %obj.mountImage(MMGun3Image, 5);
   %obj.mountImage(MMGun4Image, 6);
}

function MechMinigunImage::onSpindown(%this,%obj,%slot)
{
   %obj.MechSpunUp = false;

   %obj.setImageTrigger(4, false);
   %obj.setImageTrigger(5, false);
   %obj.setImageTrigger(6, false);
}

function MechMinigunImage::onSpinup(%this,%obj,%slot)
{
   %obj.setImageTrigger(4, true);
   %obj.setImageTrigger(5, true);
   %obj.setImageTrigger(6, true);
}

function MechMinigunImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}
//-Nite-
function MMminiProj(%data,%obj,%slot)
{
//   if(!%obj.MechSpunUp)
//      return false;
//

  %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 12);

   %p = new TracerProjectile()
   {
      dataBlock        = "MechMiniBullet";
      initialDirection = %vector;
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
      vehicleObject    = %vehicle;
   };
   MissionCleanup.add(%p);

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   %obj.MechSpunUp = true;
//   %obj.decInventory(%data.ammo, 1);
//   %obj.lastProjectile = %p;
//   %client.projectile = %p;
}

function MechMinigunImage::onFire(%data, %obj, %slot)
{
   MMminiProj(%data,%obj,%slot);
   %obj.decInventory(%data.ammo, 4);
}

function MMGun2Image::onFire(%data,%obj,%slot)
{
   if(!%obj.MechSpunUp)
      return false;

   MMminiProj(%data,%obj,%slot);
}

function MMGun3Image::onFire(%data,%obj,%slot)
{
   if(!%obj.MechSpunUp)
      return false;

   MMminiProj(%data,%obj,%slot);
}

function MMGun4Image::onFire(%data,%obj,%slot)
{
  if(!%obj.MechSpunUp)
      return false;

   MMminiProj(%data,%obj,%slot);
}
