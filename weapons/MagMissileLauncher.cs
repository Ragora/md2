if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Mag Missile launcher
//--------------------------------------

datablock AudioProfile(MagMissileLauncherFireSound)
{
   filename    = "fx/weapons/grenade_flash_explode.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock SeekerProjectileData(MiniMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 0.6;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::ElectroMissile;
   kickBackStrength    = 1250;

   explosion           = "MBSingularityExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = OldMMFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 24000;
   muzzleVelocity      = 15.0;
   maxVelocity         = 150.0;
   turningSpeed        = 150.0;
   acceleration        = 80.0;

   proximityRadius     = 8;

   terrainAvoidanceSpeed         = 180;
   terrainScanAhead              = 25;
   terrainHeightFail             = 12;
   terrainAvoidanceRadius        = 100;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 1;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(MagMissileLauncher)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_plasma.dts";
   image = MagMissileLauncherImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a energy missile launcher";

   emap = true;
};

datablock ShapeBaseImageData(MagMissileLauncherImage)
{
   className = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   item = MagMissileLauncher;
   offset = "0 0 0";
   emap = true;

   projectile = MiniMissile;
   projectileType = SeekerProjectile;

   usesEnergy = true;
   fireEnergy = 25;
   minEnergy = 30;

   isSeeker     = true; // MrKeen
   seekRadius   = 225;
   maxSeekAngle = 12; // 8
   seekTime     = 0.4;
   minSeekHeat  = 0.6;
   useTargetAudio = false;

   // only target objects outside this range
   minTargetingDistance             = 5;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activation";
   stateSound[0]                    = BlasterSwitchSound;

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "CheckWet";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.4;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = MagMissileLauncherFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.0;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MissileDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "ActivateReady";

   stateName[7]                     = "CheckTarget";
   stateTransitionOnNoTarget[7]     = "WetFire";
   stateTransitionOnTarget[7]       = "Fire";

   stateName[8]                     = "CheckWet";
   stateTransitionOnWet[8]          = "WetFire";
   stateTransitionOnNotWet[8]       = "CheckTarget";

   stateName[9]                     = "WetFire";
   stateTransitionOnNoAmmo[9]       = "NoAmmo";
   stateTransitionOnTimeout[9]      = "Reload";
   stateSound[9]                    = MagMissileLauncherFireSound;
   stateRecoil[9]                   = LightRecoil;
   stateTimeoutValue[9]             = 1.4;
   stateSequence[9]                 = "Fire";
   stateScript[9]                   = "onWetFire";
   stateAllowImageChange[9]         = false;
};

function MagMissileLauncherImage::onFire(%data,%obj,%slot)
{
   if(!%obj.nextWeaponFire || %obj.nextWeaponFire > 5)
      %obj.nextWeaponFire = 4;

   %p = Parent::onFire(%data, %obj, %obj.nextWeaponFire);
   MissileSet.add(%p);

   %target = %obj.getLockedTarget();
   if(%target)
   {
      if(%target.getDatablock().jetfire || %target.getDatablock().MegaDisc)
         assignTrackerTo(%target, %p);
      else
         %p.setObjectTarget(%target);
   }
   else if(%obj.isLocked())
      %p.setPositionTarget(%obj.getLockedPosition());
   else
      %p.setObjectTarget(0);

   if(%obj.nextWeaponFire == 4)
      %obj.nextWeaponFire = 5;
   else if(%obj.nextWeaponFire == 5)
      %obj.nextWeaponFire = 4;
   else
      %obj.nextWeaponFire = 4;
}

function MagMissileLauncherImage::onWetFire(%data, %obj, %slot)
{
   if(!%obj.nextWeaponFire || %obj.nextWeaponFire > 5)
      %obj.nextWeaponFire = 4;

   %p = Parent::onFire(%data, %obj, %obj.nextWeaponFire);
   MissileSet.add(%p);

   %p.setObjectTarget(0);

   if(%obj.nextWeaponFire == 4)
      %obj.nextWeaponFire = 5;
   else if(%obj.nextWeaponFire == 5)
      %obj.nextWeaponFire = 4;
   else
      %obj.nextWeaponFire = 4;
}

datablock ShapeBaseImageData(MagDecalAImage)
{
   shapeFile = "weapon_disc.dts";
   offset = "0.125 0.1 0.125";
   rotation = "0 1 0 90";
   emap = true;
};

datablock ShapeBaseImageData(MagDecalBImage)
{
   shapeFile = "weapon_plasma.dts";
   offset = "0 0 0.25";
   rotation = "0 1 0 180";
   emap = true;
};

function MagMissileLauncherImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(MagDecalAImage, 4);
   %obj.mountImage(MagDecalBImage, 5);
//   %obj.mountImage(MagDecalCImage, 6);
}

function MagMissileLauncherImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
//   %obj.unmountImage(6);
}
