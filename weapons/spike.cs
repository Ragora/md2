if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Spike Rifle
//--------------------------------------

datablock LinearFlareProjectileData(SpikeRifleBoltF)
{
//   projectileShapeName = "plasmabolt.dts";
   scale               = "1.0 7.0 1.0";
   faceViewer          = true;
   directDamage        = 0.05;
   directDamageType    = $DamageType::OutdoorDepTurret;
   hasDamageRadius     = true;
   indirectDamage      = 0.35;
   damageRadius        = 4.5;
   kickBackStrength    = 1000.0;
   radiusDamageType    = $DamageType::OutdoorDepTurret;
   sound          	   = BlasterProjectileSound;
   explosion           = GaussExplosion;

   splash              = PlasmaSplash;

   dryVelocity       = 500.0;
   wetVelocity       = 275.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2500;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;

   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "special/landSpikeBoltCross";

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1 0.75 0.25";
};

datablock DebrisData(SpikeRifleDebris)
{
   shapeName = "grenade_projectile.dts";

   lifetime = 3.5;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.75;
   friction = 0.1;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(SpikeAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some spike rifle cartridges";
   emap = true;
};

datablock ShapeBaseImageData(SpikeRifleImage)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   offset = "0.2 0.7 0.125";
   rotation = "1 0 0 180";

   item = SpikeRifle;
   ammo = SpikeAmmo;
   
   emap = true;

   projectile = SpikeRifleBolt;
   projectileType = TracerProjectile;

   casing              = SpikeRifleDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 12.0;
   shellVelocity       = 10.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";
   stateSound[0] = GrenadeReloadSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.75;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Recoil";
   stateScript[3] = "onFire";
//   stateSound[3] = OBLFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.25;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
//   stateSound[4] = SolPisDryFireSound;
   stateEjectShell[4]       = true;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
//   stateSound[6]      = SolPisDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ItemData(SpikeRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = SpikeRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a spike rifle";
};

datablock ShapeBaseImageData(SpikeDecalAImage)
{
   shapeFile = "weapon_plasma.dts";
   offset = "0.3 0.4 0.025";
   rotation = "1 0 0 0";
   emap = true;
   
   usesEnergy = true;
   minEnergy = -1;

//   muzzleFlash = OutdoorTurretMuzzleFlash;
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.01;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.75;
   stateFire[3] = true;
   stateSequence[3] = "Fire";
//   stateShockwave[3] = true;   

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.2;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";   
};

datablock ShapeBaseImageData(SpikeDecalBImage)
{
   shapeFile = "weapon_sniper.dts";
   offset = "0.3 0.7 0.1";
   rotation = "0 1 0 180";
   emap = true;
};

function SpikeRifleImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   %obj.mountImage(SpikeDecalAImage, 4);
   %obj.mountImage(SpikeDecalBImage, 5);
}

function SpikeRifleImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(4);
   %obj.unmountImage(5);
}

function SpikeRifleImage::onFire(%data, %obj, %slot)
{
//   if( %obj.station $= "" && %obj.isCloaked() )
//   {
//      if( %obj.respawnCloakThread !$= "" )
//      {
//         Cancel(%obj.respawnCloakThread);
//         %obj.setCloaked( false );
//         %obj.respawnCloakThread = "";
//      }
//      else
//      {
//         if( %obj.getEnergyLevel() > 20 )
//         {
//            %obj.setCloaked( false );
//            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
//         }
//      }
//   }

         %weapon = %obj.getMountedImage(0).item;

         if(%obj.client.mode[%weapon] == 0)
               %projectile = "SpikeRifleBoltF";
         else if(%obj.client.mode[%weapon] == 1)
               %projectile = "SpikeRifleSpreadBoltF";

   %mountDamageMod = 0;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %vector = %obj.getMuzzleVector(%slot);
   
   if(%obj.client.mode[%weapon] == 1)
   {
      %spread = 2;
      
      for(%i = 0; %i < 3; %i++)
          schedule(%i*200, %obj, spikeSpreadTimer, %data, %obj, %slot, %projectile, %spread);

      %obj.setImageTrigger(4, true);
      %obj.setImageTrigger(4, false);

      return;
   }
   else
   {
      %type = "LinearFlareProjectile";

      %p = new (%type)()
      {
         dataBlock        = %projectile;
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };
      
      %obj.decInventory(%data.ammo, 1);
      %obj.play3D("OBLFireSound");
   }

   %obj.lastProjectile = %p;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %p.damageMod = 1.0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;

   %obj.setImageTrigger(4, true);
   %obj.setImageTrigger(4, false);
}

function spikeSpreadTimer(%data, %obj, %slot, %projectile, %spread)
{
   if(%obj.getInventory(%data.ammo) < 1)
   {
      %obj.play3D("SniperRifleDryFireSound");
      return;
   }

      %weapon = %obj.getMountedImage(0).item;
      %vector = %obj.getMuzzleVector(%slot);
      %x = (getRandom() - 0.5) * 2 * 3.1415926 * (%spread / 1000);
      %y = (getRandom() - 0.5) * 2 * 3.1415926 * (%spread / 1000);
      %z = (getRandom() - 0.5) * 2 * 3.1415926 * (%spread / 1000);
      %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      %vector = MatrixMulVector(%mat, %vector);

      %type = "LinearFlareProjectile";

      %p = new (%type)()
      {
         dataBlock        = %projectile;
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         passengerCount   = %vehicle.passengerCount;
         bkSourceObject   = %obj;
      };

   %obj.decInventory(%data.ammo, 1);
   
   %obj.lastProjectile = %p;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %p.damageMod = 1.0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;
     
   %obj.play3D("OBLFireSound");
}
