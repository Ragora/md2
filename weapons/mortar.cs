if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Mortar
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(MortarSwitchSound)
{
   filename    = "fx/weapons/mortar_activate.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MortarReloadSound)
{
   filename    = "fx/weapons/mortar_reload.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MortarFireSound)
{
   filename    = "fx/weapons/mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(MortarProjectileSound)
{
   filename    = "fx/weapons/mortar_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(MortarExplosionSound)
{
   filename    = "fx/weapons/mortar_explode.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(HugeExplosionSound)
{
   filename    = "fx/powered/turret_mortar_explode.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterMortarExplosionSound)
{
   filename    = "fx/weapons/mortar_explode_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(MortarDryFireSound)
{
   filename    = "fx/weapons/mortar_dryfire.wav";
   description = AudioClose3d;
   preload = true;
};

//----------------------------------------------------------------------------
// Bubbles
//----------------------------------------------------------------------------
datablock ParticleData(MortarBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 600;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.4";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 0.8;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MortarBubbleEmitter)
{
   ejectionPeriodMS = 9;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 0.1;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MortarBubbleParticle";
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData( MortarSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -1.4;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.05;
   sizes[1]      = 0.2;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MortarSplashEmitter )
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "MortarSplashParticle";
};


datablock SplashData(MortarSplash)
{
   numSegments = 10;
   ejectionFreq = 10;
   ejectionAngle = 20;
   ringLifetime = 0.4;
   lifetimeMS = 400;
   velocity = 3.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = MortarSplashEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//---------------------------------------------------------------------------
// Mortar Shockwaves
//---------------------------------------------------------------------------
datablock ShockwaveData(UnderwaterMortarShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 10;
   acceleration = 20.0;
   lifetimeMS = 900;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.0 0.50";
   colors[1] = "0.4 0.4 1.0 0.25";
   colors[2] = "0.4 0.4 1.0 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ShockwaveData(MortarShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = 20.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 1.0 0.4 0.50";
   colors[1] = "0.4 1.0 0.4 0.25";
   colors[2] = "0.4 1.0 0.4 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ShockwaveData(ImplosionShockwave)
{
   width = 8.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 30;
   acceleration = 50.0;
   lifetimeMS = 1250;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1 1 1 0.50";
   colors[1] = "1 1 1 0.25";
   colors[2] = "1 1 1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ShockwaveData(ImpactMortarShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = 20.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.85 0.8 0.0 0.5";
   colors[1] = "0.85 0.8 0.0 0.25";
   colors[2] = "0.85 0.8 0.0 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

//--------------------------------------------------------------------------
// Mortar Explosion Particle effects
//--------------------------------------
datablock ParticleData( MortarCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.7 1.0 0.7 1.0";
   colors[1]     = "0.7 1.0 0.7 0.5";
   colors[2]     = "0.7 1.0 0.7 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 8.0;
   sizes[2]      = 9.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MortarCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "MortarCrescentParticle";
};

datablock ParticleData( ImpactCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.8 0.8 0.1 1.0";
   colors[1]     = "0.8 0.8 0.1 0.5";
   colors[2]     = "0.8 0.8 0.1 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 8.0;
   sizes[2]      = 9.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( ImpactCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ImpactCrescentParticle";
};

datablock ParticleData(MortarExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.30;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.5";
   colors[3]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 10.0;
   sizes[3]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(MortarExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "MortarExplosionSmoke";

};

//---------------------------------------------------------------------------
// Underwater Explosion
//---------------------------------------------------------------------------
datablock ParticleData(UnderwaterExplosionSparks)
{
   dragCoefficient      = 0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/crescent3";
   colors[0]     = "0.4 0.4 1.0 1.0";
   colors[1]     = "0.4 0.4 1.0 1.0";
   colors[2]     = "0.4 0.4 1.0 0.0";
   sizes[0]      = 3.5;
   sizes[1]      = 3.5;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(UnderwaterExplosionSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 0;
   ejectionVelocity = 17;
   velocityVariance = 4;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterExplosionSparks";
};

datablock ParticleData(MortarExplosionBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 600;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 2.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.8;
   times[2]      = 1.0;
};
datablock ParticleEmitterData(MortarExplosionBubbleEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 7.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MortarExplosionBubbleParticle";
};

datablock ExplosionData(UnderwaterMortarSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 100;
   offset = 3.0;
   playSpeed = 1.5;

   sizes[0] = "0.75 0.75 0.75";
   sizes[1] = "1.0  1.0  1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

};             

datablock ExplosionData(UnderwaterMortarSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 50;
   offset = 3.0;
   playSpeed = 0.75;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.5 1.5 1.5";
   sizes[2] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterMortarSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "2.0 2.0 2.0";
   sizes[2] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

};

datablock ExplosionData(UnderwaterMortarExplosion)
{
   soundProfile   = UnderwaterMortarExplosionSound;

   shockwave = UnderwaterMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = UnderwaterMortarSubExplosion1;
   subExplosion[1] = UnderwaterMortarSubExplosion2;
   subExplosion[2] = UnderwaterMortarSubExplosion3;

   emitter[0] = MortarExplosionBubbleEmitter;
   emitter[1] = UnderwaterExplosionSparksEmitter;
   
   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(IonBlueExplosion)
{
   soundProfile   = UnderwaterMortarExplosionSound;

   shockwave = UnderwaterMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = UnderwaterMortarSubExplosion1;
   subExplosion[1] = UnderwaterMortarSubExplosion2;
   subExplosion[2] = UnderwaterMortarSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = UnderwaterExplosionSparksEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(BAUnderwaterExplosion)
{
   soundProfile   = BombExplosionSound;

   shockwave = UnderwaterMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = UnderwaterMortarSubExplosion1;
   subExplosion[1] = UnderwaterMortarSubExplosion2;
   subExplosion[2] = UnderwaterMortarSubExplosion3;

   emitter[0] = MortarExplosionBubbleEmitter;
   emitter[1] = UnderwaterExplosionSparksEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ParticleData( MortarDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.5 0.5 0.5 0.8";
   colors[2]     = "0.25 0.25 0.25 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.75;
   sizes[2]      = 0.875;
   sizes[3]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( MortarDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "MortarDebrisSmokeParticle";
};

datablock DebrisData(MortarDebris)
{
   emitters[0] = MortarDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 2;
};

//---------------------------------------------------------------------------
// Explosion
//---------------------------------------------------------------------------

datablock ExplosionData(MortarSubExplosion1)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;

};             

datablock ExplosionData(MortarSubExplosion2)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 5.0;

   playSpeed = 1.0;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MortarSubExplosion3)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(ImpactSubExplosion1)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "5.0 5.0 5.0";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(ImpactSubExplosion2)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 5.0;

   playSpeed = 1.0;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "5.0 5.0 5.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(ImpactSubExplosion3)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "5.0 5.0 5.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MortarExplosion)
{
   soundProfile   = MortarExplosionSound;

   shockwave = MortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = MortarSubExplosion1;
   subExplosion[1] = MortarSubExplosion2;
   subExplosion[2] = MortarSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 20.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

//datablock ExplosionData(TacMortarExplosion)	// +[soph]
//{
//   soundProfile   = HugeExplosionSound;
//
//   shockwave = MortarShockwave;
//   shockwaveOnTerrain = true;
//
//   subExplosion[0] = MortarSubExplosion1;
//   subExplosion[1] = MortarSubExplosion2;
//   subExplosion[2] = MortarSubExplosion3;
//
//   emitter[0] = MortarExplosionSmokeEmitter;
//   emitter[1] = MortarCrescentEmitter;
//
//   debris = MortarDebris;
//   debrisThetaMin = 10;
//   debrisThetaMax = 90;
//   debrisNum = 8;
//   debrisVelocity = 20.0;
//   debrisVelocityVariance = 7.0;
//
//   shakeCamera = true;
//   camShakeFreq = "8.0 9.0 7.0";
//   camShakeAmp = "100.0 100.0 100.0";
//   camShakeDuration = 1.3;
//   camShakeRadius = 25.0;
//};						// +[/soph]

datablock ExplosionData(BAExplosion)
{
   soundProfile   = HugeExplosionSound;

   shockwave = ImplosionShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(ImpactMortarExplosion)
{
   soundProfile   = HugeExplosionSound;

   shockwave = ImpactMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = ImpactCrescentEmitter;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock DebrisData(ConcMortarDebris)
{
   emitters[0] = ConcDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 2;
};

datablock ShockwaveData(EMPShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.4 0.50";
   colors[1] = "0.4 0.4 1.4 0.25";
   colors[2] = "0.4 0.4 1.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ShockwaveData(EMPShockwave2D)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.4 0.50";
   colors[1] = "0.4 0.4 1.4 0.25";
   colors[2] = "0.4 0.4 1.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(ConcMortarExplosion) // MrKeen
{
   soundProfile   = UnderwaterMortarExplosionSound;

   shockwave = EMPShockwave;

   subExplosion[0] = ConcSubExplosion1;
   subExplosion[1] = ConcSubExplosion2;
   subExplosion[2] = ConcSubExplosion3;

   emitter[0] = ConcMortarExplosionSmokeEmitter;
   emitter[1] = ConcCrescentEmitter;

   debris = ConcMortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 20.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock AudioProfile(DaiExplosionSound)
{
   filename    = "fx/vehicles/bomber_bomb_impact.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ParticleData(DaishiExFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DaishiExFireEmitter)
{
   ejectionPeriodMS = 20;
   periodVarianceMS = 1;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "DaishiExFireParticle";
};

datablock ParticleData(DaishiExSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 3;
   sizes[2]      = 5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DaishiExSmokeEmitter)
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = true;

   particles = "DaishiExSmokeParticle";
};

datablock ExplosionData(DaishiExExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile = plasmaExpSound;
   faceViewer = true;
   explosionScale = "0.4 0.4 0.4";
};

datablock DebrisData(DaishiExDebris)	// ExplosionData(DaishiExDebris)
{
   emitters[0] = DaishiExSmokeEmitter;
   emitters[1] = DaishiExFireEmitter;

   explosion = DaishiExExplosion;
   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 100.0;
   lifetimeVariance = 30.0;

   numBounces = 0;
   bounceVariance = 0;
};

datablock ExplosionData(DaishiSubExplosion3)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;

   debris = IncendiaryExplosionDebris;	// +[soph]
   debrisThetaMin = 120;
   debrisThetaMax = 180;
   debrisNum = 20;
   debrisNumVariance = 3;
   debrisVelocity = 40.0;
   debrisVelocityVariance = 25.0;	// +[/soph]

   shockwave = EMPShockwave2D;
};

datablock ExplosionData(DaiExplosion)
{
   soundProfile   = DaiExplosionSound;

   shockwave = EMPShockwave;

   subExplosion[0] = ImpactSubExplosion1;	// MortarSubExplosion1; -soph
   subExplosion[1] = ImpactSubExplosion2;	// MortarSubExplosion2; -soph
   subExplosion[2] = DaishiSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   debris = DaishiExDebris;
   debrisThetaMin = 130;	// 10; -soph
   debrisThetaMax = 170;	// 50; -soph
   debrisNum = 8;
   debrisNumVariance = 3;	// 4; -soph
   debrisVelocity = 15.0;
   debrisVelocityVariance = 5.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

//---------------------------------------------------------------------------
// Smoke particles
//---------------------------------------------------------------------------
datablock ParticleData(MortarSmokeParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  1200;
   lifetimeVarianceMS   =  200;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.7 1.0 0.7 0.5";
   colors[1]     = "0.3 0.7 0.3 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MortarSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 3;

   ejectionVelocity = 2.25;
   velocityVariance = 0.55;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "MortarSmokeParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock GrenadeProjectileData(MortarShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 2500;

   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   velInheritFactor    = 0.5;
   splash              = MortarSplash;
   depthTolerance      = 10.0; // depth at which it uses underwater explosion
   
   baseEmitter         = WadGreenTrailEmitter; //MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;	// WadBlueTrailEmitter; -soph
   
   grenadeElasticity = 0.15;
   grenadeFriction   = 0.4;
   armingDelayMS     = 2000;
   muzzleVelocity    = 63.7;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";

};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock GrenadeProjectileData(ImpactMortar)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 2500;

   explosion           = "ImpactMortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   velInheritFactor    = 0.5;
   splash              = MortarSplash;
   depthTolerance      = 10.0; // depth at which it uses underwater explosion

   baseEmitter         = WadRedTrailEmitter;
   bubbleEmitter       = MortarBubbleEmitter;	// WadBlueTrailEmitter; -soph

   grenadeElasticity = 0.15;
   grenadeFriction   = 0.4;
   armingDelayMS     = 250;
   muzzleVelocity    = 63.7;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.2 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

datablock GrenadeProjectileData(ConcussionMortar)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 4.0;	// 1.5 -soph
   damageRadius        = 32.0;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 1500;

   explosion           = "ConcMortarExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;

   baseEmitter         = WadBlueTrailEmitter;//ConcussionMortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.15;
   grenadeFriction   = 0.4;
   armingDelayMS     = 2000;
   muzzleVelocity    = 63.7;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.05 0.05 0.2";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";
};

function ConcussionMortar::onExplode(%data, %proj, %pos, %mod) 	// +[soph]
{								// from gl logic
   if(%proj.bkSourceObject)
      if(isObject(%proj.bkSourceObject))
         %proj.sourceObject = %proj.bkSourceObject;

     %modifier = 1;

   if(%data.hasDamageRadius) // I lost a bomb. Do you have it?
   {
      if(%proj.vehicleMod)
           %modifier *= %proj.vehicleMod;

        if(%proj.damageMod)
          %modifier *= %proj.damageMod;
          
      if(%data.passengerDamageMod && %proj.passengerCount)
          %modifier *= 1 + (%proj.passengerCount * %data.passengerDamageMod);

      EMPBurstExplosion(%proj, %pos, %data.damageRadius, %data.indirectDamage * %modifier, %data.kickBackStrength, %proj.sourceObject );
   }
}								// +[/soph]

datablock LinearFlareProjectileData(BAArmorBlowout)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.5;
   damageRadius        = 25;
   radiusDamageType    = $DamageType::BAReactor;
   kickBackStrength    = 3000;

   explosion           = "BAExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion" ;	// "UnderwaterBAExplosion"; -soph

   splash              = MissileSplash;
   velInheritFactor    = 1.0;				// = 0; -soph

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 500;				// = 32; -soph
   lifetimeMS        = 500;				// = 32; -soph
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 32;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function BAArmorBlowout::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{
   if( isObject( %proj.sourceObject ) )
   {
      %proj.sourceObject.setDamageFlash(0.75) ;
      %proj.sourceObject.schedule( 100 , blowup ) ;
      %proj.sourceObject.blowedup = true ;
   }
   Parent::onExplode( %data , %proj , %pos , %mod ) ;
}									// -[/soph]

datablock LinearFlareProjectileData(DaiArmorBlowout)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0;	// 2.25; -soph
   damageRadius        = 15;
   radiusDamageType    = $DamageType::DaishiReactor;
   kickBackStrength    = 6500;

   explosion           = "DaiExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion" ;	// "UnderwaterBAExplosion"; -soph

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 31.25;	// 0.1; -soph
   wetVelocity       = 31.25;	// 0.1; -soph
   fizzleTimeMS      = 32;
   lifetimeMS        = 32;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 32;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function DaiArmorBlowout::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{
   %proj.exploded = true;
   %object = %proj.sourceObject;

   %radius = mSqrt( %proj.damageFactor ) + 7;
   %damage = %proj.damageFactor / 100;

   if( %proj.bkSourceObject && !%object.invincible )
   {
      %object.invincible = true;
      RadiusExplosion( %proj , %pos , %radius , %damage , %damage * 2500 , %object , %data.radiusDamageType );
      IncendiaryExplosion( %pos , %radius * 1.15 , %radius , %object );
      %object.invincible = false;
      %object.getDatablock().damageObject( %object , %proj.bkSourceObject , %pos , %damage , %proj.variableType , 0 , false );
      BurnObject( %object , %proj.bkSourceObject , %radius );
   }
   else
   {
      RadiusExplosion( %proj , %pos , %radius , %damage , %damage * 2500 , %object , %data.radiusDamageType );
      IncendiaryExplosion( %pos , %radius * 1.15 , %radius , %object );
   }
   EMPBurstExplosion( %proj , %pos , %radius * 1.3 , %damage , 0 , %object );
}									// +[/soph]

datablock LinearFlareProjectileData(BTArmorBlowout)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 5.0;				// = 50; -soph
   damageRadius        = 30.0;				// = 3; -soph
   radiusDamageType    = $DamageType::MechReactor; 
   kickBackStrength    = 3000;

   explosion           = "BAExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion" ;	// "UnderwaterBAExplosion"; -soph

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 31.25;	// 0.1; -soph
   wetVelocity       = 31.25;	// 0.1; -soph
   fizzleTimeMS      = 32;
   lifetimeMS        = 32;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 32;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function BTArmorBlowout::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{
   %proj.exploded = true;
   if( %proj.damageFactor )
   {
      %radius = mSqrt( %proj.damageFactor ) + 10;
      %damage = %proj.damageFactor / 100;

      RadiusExplosion( %proj , %pos , %radius , %damage , %damage * 2500 , %object , %data.radiusDamageType );
   }
   else
      Parent::onExplode( %data , %proj , %pos , %mod );   
}									// +[/soph]

datablock GrenadeProjectileData(HeavyMortar)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 3.0;
   damageRadius        = 25.0;
   radiusDamageType    = $DamageType::MortarTurret;
   kickBackStrength    = 15000;

   explosion           = "MortarExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;

   baseEmitter         = MortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.03;
   grenadeFriction   = 0.4;
   armingDelayMS     = 3000;
   muzzleVelocity    = 100.0;
   drag              = 0.1;

   sound			 = MortarTurretProjectileSound;

   hasLight    = true;
   lightRadius = 3;
   lightColor  = "0.05 0.2 0.05";
};

datablock GrenadeProjectileData(MechMortar)	// +[soph]
{						// +
   scale = "2.25 2.25 2.25" ;			// +
   projectileShapeName = "mortar_projectile.dts" ;
   emitterDelay        = -1 ;			// +
   directDamage        = 0.0 ;			// +
   hasDamageRadius     = true ;			// +
   indirectDamage      = 2.2 ;			// +
   damageRadius        = 40.0 ;			// + initially 50.0 
   radiusDamageType    = $DamageType::MortarTurret;
   kickBackStrength    = 20000 ;		// + initially 25000
						// +
   projectileSpread    = 10.0 / 1000.0 ;	// +
						// +
   explosion           = "BAExplosion" ;	// +
   velInheritFactor    = 1.0 ;			// +
   splash              = MortarSplash ;		// +
						// +
   baseEmitter         = GrenadeRedSmokeEmitter ;	// + ConcDebrisSmokeEmitter ; 
   bubbleEmitter       = MortarBubbleEmitter ;	// +
						// +
   grenadeElasticity = 0.03 ;			// +
   grenadeFriction   = 0.4 ;			// +
   armingDelayMS     = 5000 ;			// +
   muzzleVelocity    = 200.0 ;			// +
   drag              = 0.1 ;			// +
						// +
   lifetimeMS        = 30000 ;			// +
						// +
   sound			 = MortarTurretProjectileSound;
						// +
   hasLight    = true ;				// +
   lightRadius = 3.5 ;				// +
   lightColor  = "0.25 0.15 0.05" ;		// + "0.05 0.15 0.25" ;
};						// +[/soph]

datablock GrenadeProjectileData(MechMortarPomf)	// +[soph]
{						// +
   projectileShapeName = "turret_muzzlePoint.dts" ;
   emitterDelay        = -1 ;			// +
   directDamage        = 0.0 ;			// +
   hasDamageRadius     = true ;			// +
   indirectDamage      = 0.001 ;		// +
   damageRadius        = 0.0 ;			// +
   radiusDamageType    = $DamageType::MortarTurret ;
   kickBackStrength    = 2500 ;			// +
						// +
   projectileSpread    = 0 / 1000.0 ;		// +
						// +
   explosion           = "BAExplosion" ;	// +
   velInheritFactor    = 0.0 ;			// +
   splash              = MortarSplash ;		// +
						// +
   baseEmitter         = GExplosionSmokeEmitter ;	// ConcMortarExplosionSmokeEmitter;
						// +
   grenadeElasticity = 0.03 ;			// +
   grenadeFriction   = 0.4 ;			// +
   armingDelayMS     = 5000 ;			// +
   muzzleVelocity    = 200.0 ;			// +
   drag              = 0.1 ;			// +
						// +
   lifetimeMS        = 500 ;			// +
						// +
   sound			 = MortarTurretProjectileSound;
						// +
   hasLight    = true ;				// +
   lightRadius = 25 ;				// +
   lightColor  = "0.5 0.3 0.1" ;		// + "0.1 0.30 0.5" ;
};						// +[/soph]

//datablock GrenadeProjectileData(ImplosionShot)	// removed from compilaton for space -soph
//{
//   projectileShapeName = "mortar_projectile.dts";
//   emitterDelay        = -1;
//   directDamage        = 0.0;
//   hasDamageRadius     = true;
//   indirectDamage      = 0.5;
//   damageRadius        = 5.0;
//   radiusDamageType    = $DamageType::Mortar;
//   kickBackStrength    = 2500;
//   
//   explosion           = "ImplosionExplosion";
//   underwaterExplosion = "ImplosionExplosion";
//   velInheritFactor    = 0.5;
//   splash              = MortarSplash;
//
//   baseEmitter         = ImplosionMortarSmokeEmitter;
//   bubbleEmitter       = MortarBubbleEmitter;
//
//   grenadeElasticity = 0.15;
//   grenadeFriction   = 0.4;
//   armingDelayMS     = 2000;
//   muzzleVelocity    = 63.7;
//   drag              = 0.1;
//
//   sound			 = MortarProjectileSound;
//
//   hasLight    = true;
//   lightRadius = 4;
//   lightColor  = "1 1 1";
//
//   hasLightUnderwaterColor = true;
//   underWaterLightColor = "1 1 1";
//};

function FXPoppin(%obj, %d)
{
   loopDamageFX2(%obj.sourceObject, %obj.position, 12, 100, 20, LinearFlareProjectile, ReaverCharge);

   if(%d)
      %obj.delete();
}

function ImplosionShot::onExplode(%data, %proj, %pos, %mod)
{
//   FXPoppin(%proj);
   beginNuke(%proj.sourceObject, %pos);
   Parent::onExplode(%data, %proj, %pos, %mod);
   return; // keep easteregg stuff away

    if($EasterEgg::Mortar::NoMortarModes)
        return;

     $waterScale = $EasterEgg::Water::WaterBlockSize;
     %scPos = getWord(%pos, 0) - ($waterScale / 2)@" "@getWord(%pos, 1) - ($waterScale / 2)@" "@getWord(%pos, 2);
     %sc1 = $WaterScale@" "@$WaterScale@" "@$EasterEgg::Water::WaterBlockHeight;
     %client = %proj.sourceObject.client;
     %weapon = %proj.sourceObject.getMountedImage(0).item;

     if(%client.mode[%weapon] == 3)
     {
          %what = new Precipitation(Precipitation)
          {
     		position = %pos;
     		rotation = "1 0 0 0";
     		scale = "1 1 1";
     		nameTag = "snow";
     		dataBlock = "Snow";
     		percentage = "1";
     		color1 = "1.000000 1.000000 1.000000 1.000000";
     		color2 = "-1.000000 0.000000 0.000000 1.000000";
     		color3 = "-1.000000 0.000000 0.000000 1.000000";
     		offsetSpeed = "0.25";
     		minVelocity = "0.25";
     		maxVelocity = "1.5";
     		maxNumDrops = "2000";
     		maxRadius = "125";
			lockCount = "0";
			homingCount = "0";
     	};
          %prolongedExposure = true;
     }
     else if(%client.mode[%weapon] == 4)
     {
          %what = new Precipitation(Precipitation)
          {
     		position = %pos;
     		rotation = "1 0 0 0";
     		scale = "1 1 1";
     		nameTag = "weather";
     		dataBlock = "Rain";
     		percentage = "1";
     		color1 = "0.600000 0.600000 0.600000 1.000000";
     		color2 = "-1.000000 0.000000 0.000000 1.000000";
     		color3 = "-1.000000 0.000000 0.000000 1.000000";
     		offsetSpeed = "0.25";
     		minVelocity = "1.25";
     		maxVelocity = "4";
     		maxNumDrops = "2000";
     		maxRadius = "80";
     	};
          %prolongedExposure = true;
     }
     else if(%client.mode[%weapon] == 5)
     {
          %what = new Lightning(Lightning)
          {
          	position = %pos;
     		rotation = "1 0 0 0";
     		scale = $EasterEgg::Lightning::DangerZone;
     		dataBlock = "DefaultStorm";
     		lockCount = "0";
     		homingCount = "0";
     		strikesPerMinute = $EasterEgg::Lightning::StrikesPerMinute;
     		strikeWidth = "2.5";
     		chanceToHitTarget = $EasterEgg::Lightning::TargetHitPercent;
     		strikeRadius = "20";
     		boltStartRadius = "20";
     		color = "1.000000 1.000000 1.000000 1.000000";
     		fadeColor = "0.100000 0.100000 1.000000 1.000000";
     		useFog = "1";
     	};
          %prolongedExposure = false;
     }
     else if(%client.mode[%weapon] == 6)
     {
          %what = new FireballAtmosphere(FireballAtmosphere)
          {
     		position = %pos;
     		rotation = "1 0 0 0";
     		scale = "1 1 1";
     		dataBlock = "fireball";
     		lockCount = "0";
     		homingCount = "0";
     		dropRadius = $EasterEgg::Meteors::DangerRadius;
     		dropsPerMinute = $EasterEgg::Meteors::StrikesPerMinute;
     		minDropAngle = "0";
     		maxDropAngle = "30";
     		startVelocity = "300";
     		dropHeight = "1000";
     		dropDir = "0.212 0.212 -0.953998";
     	};
          %prolongedExposure = false;
     }
     else if(%client.mode[%weapon] == 7)
     {
          %what = new WaterBlock()
          {
     		position = %scPos;
     		rotation = "1 0 0 0";
     		scale = %sc1;
     		liquidType = "RiverWater";
     		density = "1";
     		viscosity = "5";
     		waveMagnitude = "5.5";
     		surfaceTexture = "LiquidTiles/BlueWater";
     		surfaceOpacity = "0.6";
     		envMapTexture = "lush/skies/lushcloud1";
     		envMapIntensity = "0.2";
     		removeWetEdges = "0";
     	};
          %prolongedExposure = false;
     }
     else if(%client.mode[%weapon] == 8)
     {
          %what = new WaterBlock()
          {
     		position = %scPos;
     		rotation = "1 0 0 0";
     		scale = %sc1;
     		liquidType = "StagnantWater";
     		density = "600";
     		viscosity = "5";
     		waveMagnitude = "3";
     		surfaceTexture = "LiquidTiles/lushWater01_algae";
     		surfaceOpacity = "0.6";
     		envMapTexture = "lush/skies/lushcloud1";
     		envMapIntensity = "0.2";
     		removeWetEdges = "0";
     	};
          %prolongedExposure = true;
     }
     else if(%client.mode[%weapon] == 9)
     {
          %blockname = "MobileBaseVehicle"; //getRandom(1) ? "hapcflyer" : "mobilebaseVehicle";
          %team = 0;
          %obj = %blockName.create(%team);
          %nPos = vectorAdd(%pos, "0 0 60");
          %obj.setPosition(%nPos);
          %avoidKillit = true;
     }
     else if(%client.mode[%weapon] == 10)
     {
         for(%i = 0; %i < getRandom(5); %i++)
         {
             %pl = new Player()
             {
                 datablock = "BattleAngelMaleHumanArmor";
             };
             MissionCleanup.add(%pl);

   //          %pl.setOwnerClient(%client);
             %pl.team = %client.team;
             %rndPos = vectorRandom();
             %pl.setPosition(%pos);
   //          %pl.setTarget(%client);
   //          setTargetSensorGroup(%pl.getTarget(), %proj  .sourceObject.client.target);
             setTargetSensorData(%pl.getTarget(), PlayerSensor);
             %pl.fake = true;
             %pl.sourceClient = %client;

             %avoidKillit = true;
         }
     }
     else if(%client.mode[%weapon] == 11)
     {
        // beginNuke(%client, %pos);   -Nite- ?
//         beginMiniNuke(%client, %pos);
         %client.player.incInventory(MortarAmmo, 1);
         %avoidKillit = true;
     }
        if(!%avoidKillit)
        {
          if(%prolongedExposure)
               schedule($EasterEgg::PopTime::PopTimeJello*1000, 0, "killit", %what);
          else
               schedule($EasterEgg::PopTime::PopTimeNormalFX*1000, 0, "killit", %what);
        }
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MortarAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some mortar ammo";

   computeCRC = true;

};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Mortar)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = MortarImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a mortar gun";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(MortarImage)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   item = Mortar;
   ammo = MortarAmmo;
   offset = "0 0 0";
   emap = true;

   projectile = HeavyMortar;
   projectileType = GrenadeProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = MortarSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateSound[2] = MortarIdleSound;

   stateName[3] = "Fire";
   stateSequence[3] = "Recoil";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.8;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = MortarFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 2.0;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = MortarReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function MortarImage::onFire(%data, %obj, %slot)
{
   %vehicle = 0;
   
         %weapon = %obj.getMountedImage(0).item;
         
         if(%obj.client.mode[%weapon] == 0)
               %projectile = "MortarShot";
         else if(%obj.client.mode[%weapon] == 1)
               %projectile = "ImpactMortar";
         else if(%obj.client.mode[%weapon] == 2)
               %projectile = "ConcussionMortar";
         else if(%obj.client.mode[%weapon] == 3)
               %projectile = "HeavyMortar";               
         else
               %projectile = "ImplosionShot";

      if(%obj.client.mode[%weapon] > 1 && %obj.getInventory(%data.ammo) < 2)
          return;

      %mPt = %obj.getMuzzlePoint(0);
      %mVec = %obj.getMuzzleVector(0);

      if(isObject(getLOSInfo(%obj, 0.5))) // Prevent objects from going through
         %intpos = getVectorOffset(%mPt, %mVec, -0.5);
      else
         %intpos = %mpt;

      %p = new GrenadeProjectile()
      {
         dataBlock        = %projectile;
         initialDirection = %mVec;
         initialPosition  = %intpos;
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
      };

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   if(%obj.client.mode[%weapon] > 1)
        %obj.decInventory(%data.ammo, 2);
   else
        %obj.decInventory(%data.ammo, 1);   
}
