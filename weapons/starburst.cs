if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------
// Starburst Cannon
//--------------------------------------
//-Nite- went with something different for sb hits.
//i added somthing different here
//
datablock AudioProfile(StarburstFireSound)
{
   filename    = "fx/powered/turret_mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(StarburstMount)
{
   filename    = "fx/vehicles/bomber_turret_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearProjectileData(StarburstShellI)
{
   projectileShapeName = "grenade.dts";
   emitterDelay        = 250;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.1;//0.0 --- must be < 0 (ST)
   damageRadius        = 5.0;//0.0 --- must be < 0 (ST)
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;
   bubbleEmitTime      = 1.0;

   explosion           = "StarburstExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = PulseCannonTrailEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   velInheritFactor  = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
   
   expProj     = "PlasmaBomb";
   mortarCount = 15;
};

datablock GrenadeProjectileData(StarburstShell)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;	// = 0.1; upped to trigger MA logic -soph
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 500;

   explosion = HandGrenadeExplosion;
   underwaterExplosion = UnderwaterHandGrenadeExplosion;

   velInheritFactor    = 1.0;
   splash              = MortarSplash;

//   baseEmitter         = ImplosionMortarSmokeEmitter;
//   baseEmitter         = MissileSmokeEmitter;
   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;

   bubbleEmitter       = MortarBubbleEmitter;

   grenadeElasticity = 0.15;
   grenadeFriction   = 0.4;
   armingDelayMS     = 250;
   muzzleVelocity    = 47.2;
   drag              = 0.1;

   sound			 = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "1 1 1";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "1 1 1";
};

datablock LinearProjectileData(DiscProjectileB) :  DiscProjectile
{
    indirectDamage = 0.2;
    kickBackStrength    = 1050 ;	// 500; -soph
};

//-Nite-1501 player check again
function detectIsPlayer(%pos)
{
   InitContainerRadiusSearch(%pos, 1.5, $TypeMasks::PlayerObjectType);//look for players
    if(ContainerSearchNext()) //if i see a player return true,else no see
      return true;
   else
      return false;
}

//-Nite-1501 Tweaked down the damage a bit modifier
//did  return call for players...if i hit a player return and apply damage instead
//this helps in the instant gib stuff..
function StarburstShellI::onExplode(%data, %proj, %pos, %mod)
{
   %player = %proj.client;
   %team = %player.team;

   //This i made to try something different for balancing out a weapon which is near impossible to balance.
   //Simple run down..Dectect the player, if function returns true or false do the following.
   //If true apply radius damage instead of projs damage from spawn..
//   if(!%proj.readyexp)
//   {
//      RadiusExplosion(%proj, %pos, 12, 0.75, %data.kickBackStrength, %proj.sourceObject, %data.radiusDamageType);
//      return;
//   }

   %count = 0;
   InitContainerRadiusSearch(%pos, 35, $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::ProjectileObjectType); // get them arrayed

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.team != %team && (%int.getType() & $TypeMasks::VehicleObjectType || %int.getType() & $TypeMasks::PlayerObjectType) && %int.getHeat() > 0.5)
      {
         %playerArray[%count] = %int;
         %count++;
      }
      else if(%int.getClassName() $= "FlareProjectile")
      {
         %playerArray[%count] = %int;
         %count++;
      }
   }

   if(!%count || %proj.StarburstMode == 3) // default
   {
      %maxSeed = 40;
      %minSeed = 20;

      if(%proj.StarburstMode $= "")
         %proj.StarburstMode = 0;

      if(%proj.StarburstMode == 0)
      {
         %projectile = "ChaingunBullet";
         %projectileType = "TracerProjectile";
//         %maxSeed = 40;  //65							// -soph
//         %minSeed = 25;							// -soph
         %seed = 36 ;								// +soph
   //      %spread = 3.1415926;
   //      %vector = "0 0 1";
         %modifier = 1.5; // 25
      }
      else if(%proj.StarburstMode == 1)
      {
         %projectile = "RAXXFlame";
         %projectileType = "LinearProjectile";
//         %maxSeed = 30;							// -soph
//         %minSeed = 15;							// -soph
         %seed = 30 ;								// +soph
         %modifier = 1.5 ; 							// 2.0; // 7 -soph
         IncendiaryExplosion( %pos , %data.damageradius , 2 , %proj.source );	// +soph
      }
      else if(%proj.StarburstMode == 2)
      {
         %projectile = "DiscProjectileB";
         %projectileType = "LinearProjectile";
//         %maxSeed = 15;  //35							// -soph
//         %minSeed = 7;							// -soph
         %seed = 15 ;								// +soph
         %modifier = 1.0; // 25     5

      }
      else if(%proj.StarburstMode == 3)
      {
         %projectile = "BasicGrenade";
         %projectileType = "GrenadeProjectile";
//         %maxSeed = 45;							// -soph
//         %minSeed = 30;							// -soph
         %seed = 36 ;								// +soph
         %modifier = 1.0; // 7
      }

//      %seed = getRandom(%maxSeed, %minSeed);					// -soph
      %spread = 3.1415926;
      %spread4 = 0.064;
      %vector = "0 0 1";
      %vector4 = "0 0 -1";
      %mode = %proj.StarburstMode;

      %FFRObject = new StaticShape()
      {
         dataBlock        = mReflector;
      };
      MissionCleanup.add(%FFRObject);
      %FFRObject.setPosition(%pos);
      %FFRObject.schedule(32, delete);

      for(%i = 0; %i < %seed; %i++)
      {
         %tspread = %mode != 5 ? %spread : %spread4;
         %x = (getRandom() - 0.5) * 2 * 3.1415926 * %tspread;  //3.1415926;
         %y = (getRandom() - 0.5) * 2 * 3.1415926 * %tspread; //3.1415926;
         %z = (getRandom() - 0.5) * 2 * 3.1415926 * %tspread; //1.5707963;//3.1415926;
         %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
         %vector = %mode == 5 ? %vector4 : %vector;
         %vector = MatrixMulVector(%mat, %vector);

         %p = new (%projectileType)() // MrKeen
         {
            dataBlock        = %projectile;
            initialDirection = %vector;
            initialPosition  = %pos;
            sourceObject     = %FFRObject;
            sourceSlot       = 0;
            vehicleObject    = 0;
            bkSourceObject   = %proj.source;
            pDmgMod          = %modifier;

            starburstSpawned = true;
         };

         MissionCleanup.add(%p);

          %p.vehicleMod = %projectile.vehicleMod;
          %p.damageMod = %projectile.damageMod;
      }
   }
   else
   {
      if(%proj.StarburstMode $= "")
         %proj.StarburstMode = 0;

      %maxSeed = 60;
      %minSeed = 20;

      if(%proj.StarburstMode == 0)
      {
         %projectile = "ChaingunBullet";
         %projectileType = "TracerProjectile";
//         %maxSeed = 40;  //65							// -soph
//         %minSeed = 15;							// -soph
         %seed = 36 ;								// +soph
//         %spread = 3.1415926;
//         %vector = "0 0 1";
         %modifier = 1.5; // 25
         %speedAdjust = 1.1;
      }
      else if(%proj.StarburstMode == 1)
      {
         %projectile = "RAXXFlame";
         %projectileType = "LinearProjectile";
//         %maxSeed = 25;							// -soph
//         %minSeed = 12;							// -soph
         %seed = 30 ;								// +soph
         %modifier = 1.5 ;							// 2.0; // 7 -soph
         %speedAdjust = 30;
      }
      else if(%proj.StarburstMode == 2)
      {
         %projectile = "DiscProjectileB";
         %projectileType = "LinearProjectile";
//         %maxSeed = 15;  //35							// -soph
//         %minSeed = 8;							// -soph
         %seed = 15 ;								// +soph
         %modifier = 1.0; // 25     5
         %speedAdjust = 25;
      }

//      %maxSeed /= mFloor(%count);						// -[soph]
//      %minSeed /= mFloor(%count);
      %remainder = %seed % %count ;						// +soph
      %seed = mFloor( %seed / %count ) ;					// +soph

//      %spread = 3.1415926;							// -[/soph]
//      %spread4 = 0.064;							// no longer used? 
      %vector = "0 0 1";
//      %vector4 = "0 0 -1";							// -soph

      if(!%speedAdjust)
         %speedAdjust = 2;

      for(%idx = 0; %idx < %count; %idx++)
      {

//         %seed = getRandom(%maxSeed, %minSeed);				// -soph
//         %mode = %proj.StarburstMode;						// doesn't do anything? -soph

         %FFRObject = new StaticShape()
         {
            dataBlock        = mReflector;
         };
         MissionCleanup.add(%FFRObject);
         %FFRObject.setPosition(%pos);
         %FFRObject.schedule(32, delete);

         for(%i=0; %i<%seed; %i++)
         {
            %vect = getVectorFromPoints(%pos, %playerArray[%idx].getWorldBoxCenter());
            %vector = vectorScale(calcSpreadVector(%vect, 48), 10);

            %p = new (%projectileType)() // MrKeen
            {
               dataBlock        = %projectile;
               initialDirection = %vector;
               initialPosition  = %pos;
               sourceObject     = %FFRObject;
               sourceSlot       = 0;
               vehicleObject    = 0;
               bkSourceObject   = %proj.source;
               pDmgMod          = %modifier;

               starburstSpawned = true;
            };

            MissionCleanup.add(%p);
            %p.vehicleMod = %projectile.vehicleMod;
            %p.damageMod = %projectile.damageMod;
         }
      }
      for( %i = 0 ; %i < %remainder ; %i++ )					// +[soph]
      {										// +
         %p = new ( %projectileType )()						// +
         {									// +
            dataBlock        = %projectile ;					// +
            initialDirection = vectorScale( calcSpreadVector( "0 0 1" , 1000 ) , 10 ) ;
            initialPosition  = %pos ;						// +
            sourceObject     = %FFRObject ;					// +
            sourceSlot       = 0 ;						// +
            vehicleObject    = 0 ;						// +
            bkSourceObject   = %proj.source ;					// +
            pDmgMod          = %modifier ;					// +
										// +
            starburstSpawned = true ;						// +
         } ;									// +
         MissionCleanup.add( %p ) ;						// +
         %p.vehicleMod = %projectile.vehicleMod ;				// +
         %p.damageMod = %projectile.damageMod ;					// +
      }										// +
      if(%proj.StarburstMode == 1)						// +
         IncendiaryExplosion( %pos , %data.damageradius , 1.5 , %proj.source );	// +[/soph]
   }
   Parent::onExplode(%data, %proj, %pos, %mod);
}

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(StarburstAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some starburst ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Starburst)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = StarburstImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a starburst cannon";

   emap = true;
};

datablock ShapeBaseImageData(StarburstImage)
{
   className = WeaponImage;
   shapeFile = "turret_mortar_large.dts";
   item = Starburst;
   ammo = StarburstAmmo;
   emap = true;

//   usesEnergy = true;
//   fireEnergy = -1;
//   minEnergy = -1;   
   
   projectile = StarburstShell;
   projectileType = GrenadeProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = StarburstMount;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateSound[2] = MortarIdleSound;
   stateSequence[2] = "Deploy";
   
   stateName[3] = "Fire";
   stateSequence[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.3;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
//   stateSound[3] = StarburstFireSound;
//   stateShockwave[3] = true;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.05;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
//   stateSound[4] = MortarReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateScript[6] = "onDryFire";	// let's try this +soph
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

function sbtimerready(%obj)
{
   if(isObject(%obj))
      %obj.readyexp = true;
}

function SBFlakTrigger(%obj)
{
   if(isObject(%obj))
   {
         %pos = %obj.getWorldBoxCenter();
         %FFRObject = new StaticShape()
         {
            dataBlock = mReflector;
         };
         MissionCleanup.add(%FFRObject);
         %FFRObject.setPosition(%pos);
         %FFRObject.schedule(32, delete);

      %p = new LinearProjectile()
      {
         dataBlock        = StarburstShellI;
         initialDirection = "0 0 0";
         initialPosition  = %pos;
         sourceObject     = %FFRObject;
         source           = %obj.sourceObject;
         sourceSlot       = 0;
         vehicleMod       = %obj.vehicleMod;
         damageMod        = %obj.damageMod;
         bkSourceObject   = %obj.sourceObject;
      };
      MissionCleanup.add(%p);

     %p.client = %obj.client;
//     %p.client.player = %obj.client.player;
//     %p.client.player.slot = %slot;
     %p.StarburstMode = %obj.StarburstMode;

      %obj.delete();
   }
}

function SBEnableFlak(%obj)
{
   if(!%obj.flakEnabled)
   {
      %obj.play3d(MortarReloadSound);
      %obj.flakEnabled = true;
   }
}

function StarburstImage::onFire(%data, %obj, %slot)
{
//   %data.lightStart = getSimTime();

   if(isObject(%obj.lastMortar))
   {
      SBFlakTrigger(%obj.lastMortar);
      return;
   }

   if(!%obj.flakEnabled)
       return;

   %vehicle = 0;

     %weapon = %obj.getMountedImage(0).item;
     
     if(%obj.client.mode[%weapon] == 2)
        %ammoUse = 2;
     else if(%obj.client.mode[%weapon] == 3)
          %ammoUse = 3;
     else
          %ammoUse = 1;

     %ammo = %obj.getInventory(StarburstAmmo);
     
     if(%ammoUse > %ammo)
          return;
          
      %p = new GrenadeProjectile() {
         dataBlock        = "StarburstShell";
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = %vehicle;
         bkSourceObject   = %obj;
      };

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

     %p.client = %obj.client;
//     %p.client.player = %obj.client.player;
//     %p.client.player.slot = %slot;
     %p.StarburstMode = %obj.client.mode[%weapon];
//     %proj = %p.getDatablock();
     %obj.lastMortar = %p;
     
     %obj.play3d(StarburstFireSound);
     
   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;


   %obj.decInventory(StarburstAmmo, %ammoUse);
   
   %obj.flakEnabled = false;
   if(%ammo == 1)
      schedule(1500, %p, SBFlakTrigger, %p);
      
   schedule(2800, %obj, "SBEnableFlak", %obj);
}

function StarburstImage::onDryFire(%data, %obj, %slot)	// +[soph]
{							// let's see if this works
   if(isObject(%obj.lastMortar))
   {
      SBFlakTrigger(%obj.lastMortar);
      return;
   }
}							// +[/soph]

//datablock ShapeBaseImageData(SBDecalAImage)
//{
//   shapeFile = "weapon_mortar.dts";
//   offset = "-0.2 0 0.3";
//   rotation = "1 0 0 180";
//   emap = true;
//};

//datablock ShapeBaseImageData(SBDecalBImage)
//{
//   shapeFile = "stackable4m.dts";
//   offset = "-0.1 0.3 0.3";
//   rotation = "1 0 0 90";
//   emap = true;
//};

//datablock ShapeBaseImageData(SBDecalB2Image)
//{
//   shapeFile = "stackable4l.dts";
//   offset = "-0.1 0.3 0.3";
//   rotation = "1 0 0 90";
//   emap = true;
//};

function StarburstImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);

   %obj.flakEnabled = true;
   
   %weapon = %obj.getMountedImage(0).item;
//   if(%obj.client.mode[%weapon] <= 1)
//   {
//      %obj.mountImage(SBDecalAImage, 4);
//      %obj.mountImage(SBDecalBImage, 5);
//   }
//   else
//   {
//      %obj.mountImage(SBDecalAImage, 4);
//      %obj.mountImage(SBDecalB2Image, 5);
//   }
}

function StarburstImage::onUnmount(%this,%obj,%slot)
{
   Parent::onUnmount(%this, %obj, %slot);
   %obj.unmountImage(4);
   %obj.unmountImage(5);
//   %obj.unmountImage(6);
}
