//--------------------------------------------------------------------------
// ELF Turret barrel
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------------------------------------------

datablock AudioProfile(EBLSwitchSound)
{
   filename    = "fx/powered/turret_light_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(EBLFireSound)
{
   filename    = "fx/weapons/ELF_fire.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock TracerProjectileData(EnergyBlasterShot)
{
   doDynamicClientHits = true;

   projectileShapeName = "energy_bolt.dts";
   directDamage        = 0.2;
   directDamageType    = $DamageType::AATurret;
   explosion           = "AABulletExplosion";
   splash              = ChaingunSplash;

   dryVelocity       = 600.0;
   wetVelocity       = 600.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 3000;

   tracerLength    = 20;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 1 1 1";
	tracerTex[0]  	 = "special/shrikeBolt";
	tracerTex[1]  	 = "special/shrikeBoltCross";
	tracerWidth     = 0.55;
   crossSize       = 0.99;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

//--------------------------------------------------------------------------
// ELF Turret Image
//--------------------------------------------------------------------------

datablock TurretImageData(ELFBarrelLarge)
{
   shapeFile = "turret_elf_large.dts";
   item      = ELFBarrelLargePack;

   projectile = EnergyBlasterShot;
   projectileType = TracerProjectile;
//   deleteLastProjectile = false;

   // Turret parameters
   activationMS      = 500;
   deactivateDelayMS = 100;
   thinkTimeMS       = 100;
   degPerSecTheta    = 580;
   degPerSecPhi      = 960;
   attackRadius      = 75;

//   yawVariance          = 30.0; // these will smooth out the elf tracking code.
//   pitchVariance        = 30.0; // more or less just tolerances
   
   projectileSpread = 12.0 / 1000.0;

   usesEnergy = true;
   minEnergy = 25;
   fireEnergy = 0;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   stateTimeoutValue[0]        = 0.05;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";
   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";
   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";
   //--------------------------------------
   stateName[4]             = "Fire";
   stateSpinThread[4]       = FullSpeed;
   stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
//   stateSound[4]            = ;
   stateSequence[4]         = "Fire";
   // IMPORTANT! The stateTimeoutValue below has been replaced by fireTimeOut
   // above.
   stateTimeoutValue[4]          = 1.0;
   stateTransitionOnTimeout[4]   = "checkState";
   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSpinThread[5] = SpinDown;
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";
   //--------------------------------------
   stateName[6]       = "EmptySpindown";
//   stateSound[6]      = ChaingunSpindownSound;
   stateSpinThread[6] = SpinDown;
   stateTransitionOnAmmo[6]   = "Ready";
   stateTimeoutValue[6]        = 0.01;
   stateTransitionOnTimeout[6] = "NoAmmo";
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ShrikeBlasterDryFireSound;
   stateTransitionOnTriggerUp[7] = "NoAmmo";
   stateTimeoutValue[7]        = 0.25;
   stateTransitionOnTimeout[7] = "NoAmmo";

   stateName[8] = "checkState";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
   stateTimeoutValue[8]          = 0.01;
   stateTransitionOnTimeout[8]   = "ready";
};

function ELFBarrelLarge::onFire(%data,%obj,%slot)
{
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   Parent::onFire(%data,%obj,%slot);
   
   %obj.play3D(AAFireSound);
   %obj.useEnergy(25);
}
