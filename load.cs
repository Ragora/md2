function serverCmdTeamMessageSent(%client, %text)
{
   // Special case handling - won't be able to tell if /me is used in team-only or global context if used in a plugin
   if(getSubStr(%text, 0, 4) $= "/me ")
   {
      chatMessageTeam(%client, %client.team, '\c1* %1 %2', %client.namebase, getSubStr(%text, 4, $Host::MaxMessageLen));

      if($Host::LogTeamChat)
           logChat("[TEAM "@%client.team@"]" SPC %client.nameBase@":" SPC %text);
           
      return;
   }
   
   if(getSubStr(%text, 0, 1) $= "/" && getSubStr(%text, 1, 1) !$= "")
   {
      Xi.evalCommand(%client, getSubStr(%text, 1, $Host::MaxMessageLen));

      return;
   }

   if(strlen(%text) >= $Host::MaxMessageLen)
      %text = getSubStr(%text, 0, $Host::MaxMessageLen);

   %name = %client.showRank ? addTaggedString($StatRankID[getTrackedStat(%client.guid, "rank")] SPC getTaggedString(%client.name)) : %client.name;
   
   chatMessageTeam(%client, %client.team, '\c3%1: %2', %client.namebase, %text);

   if($Host::LogTeamChat)
   {
      if(%client.guid !$= "")
          logChat("[TEAM "@%client.team@"]" SPC %client.nameBase@":" SPC %text);
   }
}

function serverCmdMessageSent(%client, %text)
{
   // Special case handling - won't be able to tell if /me is used in team-only or global context if used in a plugin
   if(getSubStr(%text, 0, 4) $= "/me ")
   {
      chatMessageAll(%client, '\c1* %1 %2', %client.namebase, getSubStr(%text, 4, $Host::MaxMessageLen));

      if($Host::LogChat)
         logChat("[CHAT] "@%client.nameBase@":" SPC %text);
         
      return;
   }

   if(getSubStr(%text, 0, 1) $= "/" && getSubStr(%text, 1, 1) !$= "")
   {
      Xi.evalCommand(%client, getSubStr(%text, 1, $Host::MaxMessageLen));

      return;
   }

   if(strlen(%text) >= $Host::MaxMessageLen)
      %text = getSubStr(%text, 0, $Host::MaxMessageLen);

   %name = %client.showRank ? addTaggedString($StatRankID[getTrackedStat(%client.guid, "rank")] SPC getTaggedString(%client.name)) : %client.name;
   
   chatMessageAll(%client, '\c4%1: %2', %name, %text);

   if($Host::LogChat)
   {
      if(%client.guid !$= "")
          logChat("[CHAT] "@%client.nameBase@":" SPC %text);
   }
}

function cannedChatMessageAll( %sender, %msgString, %name, %string, %keys )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;

   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
      cannedChatMessageClient( ClientGroup.getObject(%i), %sender, %msgString, %name, %string, %keys );
      
   if($Host::LogChat)
   {
      if(%client.guid !$= "")
         logChat("[VOICE]" SPC "["@%keys@"]" SPC %sender.nameBase@":" SPC %string);
   }
}

function cannedChatMessageTeam( %sender, %team, %msgString, %name, %string, %keys )
{
   if ( ( %msgString $= "" ) || spamAlert( %sender ) )
      return;

   %count = ClientGroup.getCount();
   for ( %i = 0; %i < %count; %i++ )
   {
      %obj = ClientGroup.getObject( %i );
      if ( %obj.team == %sender.team )
         cannedChatMessageClient( %obj, %sender, %msgString, %name, %string, %keys );
   }

   if($Host::LogTeamChat)
   {
      if(%client.guid !$= "")
         logChat("[T"@%team@" VOICE]" SPC "["@%keys@"]" SPC %sender.nameBase@":" SPC %string);
   }
}