// ------------------------------------------------------------------
// SHIELD PACK
// can be used by any armor type
// while activated, absorbs damage at cost of energy

datablock AudioProfile(ShieldPackActivateSound)
{
	filename = "fx/packs/shield_on.wav";
	description = ClosestLooping3d;
    preload = true;
};

datablock ShapeBaseImageData(ShieldPackImage)
{
   shapeFile = "pack_upgrade_shield.dts";
   item = ShieldPack;
   mountPoint = 1;
   offset = "0 0 0";

   usesEnergy = true;
   minEnergy = 3;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
	stateSound[1] = ShieldPackActivateSound;
   stateEnergyDrain[1] = 8.666;
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(ShieldPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_shield.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "ShieldPackImage";
	pickUpName = "a shield pack";

   computeCRC = true;
};

function ShieldPackImage::onMount(%data, %obj, %node)
{
     %obj.BAPackID = 3;
}

function ShieldPackImage::onUnmount(%data, %obj, %node)
{
   %obj.setImageTrigger(%node, false);
   %obj.isShielded = "";
     %obj.BAPackID = 0;

   if( %obj.bassActive )	// +soph
      stopBASS( %obj ) ;	// +soph
}

function ShieldPackImage::onActivate(%data, %obj, %slot)
{
   if(%obj.universalResistFactor < 1.0)
   {
       messageClient(%obj.client, 'MsgShieldPackErr', '\c2Error: Cannot form shield over Heavy armor.');
       %obj.setImageTrigger(%slot, false);
       return;
   }

   if(%obj.overdriveMode)
   {
       %obj.setImageTrigger(%slot, false);
       return;
   }

   if(%obj.baShieldSystem && %obj.inDEDField)
   {
        %obj.bassActive = true;
        initBASS(%obj);
        messageClient(%obj.client, 'MsgShieldBASSOn', '\c2BASS activated, shields extended.');	// '\c2Battle Angel Shield System (BASS) activated.'); -soph
   }
   else if(%obj.baShieldSystem)
   {
        bottomPrint(%cl, "Battle Angel Shield System requires a Defense+ Device in range to operate.", 5, 1);
        messageClient(%obj.client, 'MsgShieldPackOn', '\c2Shield pack on.');
   }
   else
        messageClient(%obj.client, 'MsgShieldPackOn', '\c2Shield pack on.');
           
   %obj.isShielded = true;
   commandToClient( %obj.client, 'setShieldIconOn' );
}

function ShieldPackImage::onDeactivate(%data, %obj, %slot)
{
   %obj.isShielded = "";			// +soph

   if(%obj.baShieldSystem && %obj.bassActive)
   {
        messageClient(%obj.client, 'MsgShieldBASSOff', '\c2BASS deactivated, shields off.');	// '\c2Battle Angel Shield System (BASS) deactivated.'); -soph
        %obj.bassActive = false;
        stopBASS(%obj);
   }
   else
        messageClient(%obj.client, 'MsgShieldPackOff', '\c2Shield pack off.');
           
  // %obj.isShielded = "";			// ^ -soph

   %obj.setImageTrigger(%slot,false);
   commandToClient( %obj.client, 'setShieldIconOff' );
}

function ShieldPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}

//datablock StaticShapeData(DaishiDeployPad)	// -[soph]
//{
//   shapeFile = "nexuscap.dts";
//};

//function DaishiDeployPad::hasDismountOverrides()
//{
//     return false;
//}						// -[/soph]

function mReflector::hasDismountOverrides()
{
     return false;
}

function initBASS(%obj)
{
     %cl = %obj.client;
     
     if(%obj.dead)
          return;

    // if(%obj.isFlagInArea(15, 15))				// -[soph]
    // {
    //      bottomPrint(%cl, "Cannot activate Battle Angel Shield System within 15m of a flag.", 5, 1);
    //      %obj.setImageTrigger($BackpackSlot, false);
    //      return;
    // }							// -[/soph]

    // if(%obj.isMounted())					// -[soph]	
    // {
    //      bottomPrint(%cl, "Cannot activate Battle Angel Shield System while in a vehicle.", 5, 1);
    //      %obj.setImageTrigger($BackpackSlot, false);
    //      return;
    // }							// -[/soph]

     if( %obj.getEnergyPct() < 0.05 )				// +[soph]
     {
          bottomPrint( %cl , "Insufficient energy to engage Battle Angel Shield System." , 5 , 1 ) ;
          %obj.setImageTrigger( $BackpackSlot , false ) ;
          return ;
     }								// +[/soph]

    // %obj.deployPos = %obj.getPosition();			// -soph

     if( isObject( %obj.daishiRepulsor ) )			// +[soph]
     {
         %obj.unmountObject( $flagslot ) ;
         %obj.daishiRepulsor.delete() ;
     }								// +[/soph]

     %field = new StaticShape()
     {
          scale = "4 4 4";
          dataBlock = "WarpBubble";
     };

    // %field.setPosition(%obj.getPosition());			// -soph
     %field.playThread($DeployThread, "deploy");
     %obj.daishiRepulsor = %field;
     %obj.mountObject( %field , $flagslot ) ;			// +soph
//     %obj.daishiRepulsor.startFade(1, 0, true);

    // %base = new StaticShape()				// -[soph]
    // {
    //      dataBlock = "mReflector";
    // };

    // %base2 = new StaticShape()
    // {
    //      dataBlock = "DaishiDeployPad";
    //      scale = "0.25 0.25 0.25";
    // };

    // %base2.startFade(1, 0, true);
    // %base.setPosition(vectorAdd(%obj.getPosition(), "0 0 0.001"));
    // %base2.setPosition(%obj.getPosition());
    // %obj.basePad = %base;
    // %obj.basePad2 = %base2;
    // %base.mountObject(%obj, 1);
    // %base.daishiUnmountBypass = true;
    // %base.noJump = true;
     
//     %obj.shieldStrengthFactor += %obj.inDEDField ? 0 : -0.5;

    // commandToClient(%cl,'SetDefaultVehicleKeys', true);
    // commandToClient(%cl,'SetPilotVehicleKeys', true);
    // commandToClient(%cl,'SetPassengerVehicleKeys', true);	// -[/soph]
     schedule($Host::RepulsorTickRate, %obj, updateBASSRepulsorThread, %obj);
     
     bottomPrint(%cl, "Battle Angel Shield System activated, shields extended outwards.", 5, 1);	// (BASS) activated, shields extended outwards.", 5, 1); -soph
}

function updateBASSRepulsorThread(%obj)
{
     if(%obj.bassActive)
     {
          daishiReflectProjectiles(%obj, 12);
          %obj.mountObject( %obj.daishiRepulsor, $flagslot );	// +[soph]
          if( %obj.getEnergyPct() < 0.05 || !%obj.inDEDField )
               return stopBASS(%obj);
          else		  					// +[/soph]
               schedule($Host::RepulsorTickRate, %obj, updateBASSRepulsorThread, %obj);
     }
}

function daishiReflectProjectiles(%obj, %radius)
{
   %reflectType = $DefaultReflectableProjectles;
   %idm = 1.0;
   %ddm = 1.0;

  // if(%obj.getEnergyPct() < 0.05)	// moved into loop
  //   return;				// -soph

   InitContainerRadiusSearch(%obj.getWorldBoxCenter(), %radius, $TypeMasks::ProjectileObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
//      if(vectorDist(%int.getWorldBoxCenter(), %obj.getWorldBoxCenter()) < (%radius * 0.8)) // within 80% of the total radius of the reflector
//         %int.noreflect = true;

      if(%int.lastReflectedFrom == %obj)
         continue;

      if(%int.sourceObject == %obj || %int.bkSourceObject == %obj)
          continue;

      if(isReflectableProjectile(%int, %reflectType))
      {
          if(!isObject(%obj.reflectorObj))
               %obj.reflectorObj = createReflectorForObject(%obj);
               
          %FFRObject = %obj.reflectorObj;
     
         %FFRObject.setPosition(%int.getPosition());

         if(!%int.noreflect)
         {
            %pVec = VectorNormalize(%int.initialDirection);				// +[soph]
            %ctrPos = VectorAdd(%obj.getPosition(), "0 0 3");				// logic from deployable repulsor
            %vecFromCtr = VectorNormalize(VectorSub(%int.getPosition(), %ctrPos)); // --- Acts as surface normal (ST)

            // --- Thanks to Mostlikely
            %flip = VectorDot(%vecFromCtr, %pVec);
            %flip = VectorScale(%vecFromCtr, %flip);
            %newVec = VectorAdd(%pVec, VectorScale(%flip, -2));
            // --- ...again LOL (ST)
            %newPos = VectorAdd(%int.getPosition(), %newVec);
   // --- End reflection angle (ST)							// +[/soph]

            if(%int.getClassName() $= "BombProjectile")
               %newVec = vectorScale(calcSpreadVector(vectorNeg(%int.initialDirection), 6), 350);
//            else									// -soph
//                 %newVec = calcSpreadVector(vectorNeg(%int.initialDirection), 6);	// -soph

           if(!isObject(%int.sourceObject))
               %src = %int.bkSourceObject;
           else
               %src = %int.sourceObject;

            if( %int.originTime )						// +[soph]
            {									// +
               %airTimeMod = getSimTime() - %int.originTime ; 			// +
               %data = %int.getDatablock().getName() ;				// +
               if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
                  %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
               else								// +
                  %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
               %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;		// +
            }									// +[/soph]

            %p = new(%int.getClassName())()
            {
               dataBlock         = %int.getDatablock().getName();
               initialDirection  = %newVec;
               initialPosition   = %int.getPosition();
               sourceObject      = %FFRObject;
               sourceSlot        = %int.sourceSlot;
               lastReflectedFrom = %obj;
               bkSourceObject    = %obj;
               starburstMode     = %int.starburstMode;
               damageMod         = %int.damageMod;
               vehicleMod        = %int.vehicleMod;
            };
            MissionCleanup.add(%p);

            %p.sourceObject = %obj;
  
            if( %src.lastMortar == %int )					// if(%src.lastMortar !$= "") -soph
                %src.lastMortar = %p;

            %p.originTime = %int.originTime ;					// +soph

            if(%int.trailThread)
               projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);

            if(%int.proxyThread)
               rBeginProxy(%p);

           if(%int.lanceThread)
           {        
               SBLanceRandomThread(%p);
               %p.lanceThread = true;
           }

            createLifeEmitter(%newPos, "PCRSparkEmitter", 750);

            if(%obj.getState() !$= "Dead")
            {
               if(isObject(%src))
               {
                    %mod = %obj.deployedMode ? 0.5 : 1.0;
                    
                    if( %int.getDatablock().hasDamageRadius &&  %int.getDatablock().indirectDamage > ( 2 * %int.getDatablock().directDamage ) )						// if(%int.getDatablock().hasDamageRadius) -soph
                    {
                      // %obj.getDataBlock().damageObject(%obj, %src, %obj.getPosition(), %int.getDatablock().indirectDamage * %idm * %mod, %int.getDatablock().radiusDamageType);					// -soph
                       %damage = %int.getDatablock().indirectDamage * %idm * %mod / 2;													// +[soph]
                       %damage += %obj.linkedShieldSource.getDataBlock().checkShields( %obj.linkedShieldSource , %obj.getPosition() , %damage , %int.getDatablock().radiusDamageType );	// half to D+D shields, half + balance to player's
                       %obj.getDataBlock().checkShields( %obj , %obj.getPosition() , %damage , %int.getDatablock().radiusDamageType );							// +[/soph]
                    }
                    else
                    {
                      // %obj.getDataBlock().damageObject(%obj, %src, %obj.getPosition(), %int.getDatablock().directDamage * %idm * %mod, %int.getDatablock().directDamageType);	// -soph
                       %damage = %int.getDatablock().directDamage * %idm * %mod / 2;													// +[soph]
                       %damage += %obj.linkedShieldSource.getDataBlock().checkShields( %obj.linkedShieldSource , %obj.getPosition() , %damage , %int.getDatablock().directDamageType );	// half to D+D shields, half + balance to player's
                       %obj.getDataBlock().checkShields( %obj , %obj.getPosition() , %damage , %int.getDatablock().directDamageType );							// +[/soph]
                    }
                    %obj.playShieldEffect("0 0 1");
               }
            }

            %int.delete();
//            %FFRObject.schedule(32, delete);
         }
      }
      else continue;
   }
}

function stopBASS(%obj)
{
     %cl = %obj.client;

     if(isObject(%obj.daishiRepulsor))
     {
          %obj.unmountObject( $flagslot );
          %obj.daishiRepulsor.delete();
     }

     if(isObject(%obj.basePad))
     {
          %obj.basePad.unmountObject(1);
          %obj.basePad.delete();
          %obj.basePad2.delete();
     }

    // %obj.shieldStrengthFactor += %obj.inDEDField ? 0 : 0.5;	// i think this is obsolete -soph
     %obj.bassActive = false;					// +[soph]					
     if( %obj.isShielded )
     {
        messageClient(%obj.client, 'MsgShieldBASSOff', '\c2BASS field collapsed, reverting to standard shielding.');
        bottomPrint(%cl, "BASS deactivated, shields down.", 5, 1);
        bassIdle( %obj );
     }								// +[/soph]
//     commandToClient(%cl,'SetDefaultVehicleKeys', false);
//     commandToClient(%cl,'SetPilotVehicleKeys', false);
//     commandToClient(%cl,'SetPassengerVehicleKeys', false);
     else    							// +soph
        bottomPrint(%cl, "BASS deactivated, shields down.", 5, 1);
}

function bassIdle( %obj )					// +[soph]
{
     if( %obj.isShielded )
     {
          if( !%obj.bassActive && %obj.getEnergyPct() > 0.05 && %obj.inDEDField )
          {
               messageClient( %obj.client , 'MsgShieldBASSOn' , '\c2BASS restored, expanding protection field.' ) ;
               %obj.bassActive = true ;
               initBASS( %obj ) ;
          }
          else
               schedule( $Host::RepulsorTickRate , %obj , bassIdle , %obj ) ;
     }
}		  						// +[/soph] 