if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------
// Polarized Armor Pack

//datablock ShapeBaseImageData(PolarArmorPlatePackImage)	// -[soph]
//{								// - functionality changed
////   shapeFile = "pack_barrel_elf.dts";			// -
//   shapeFile = "turret_muzzlepoint.dts";			// - 
//   item = PolarArmorPlatePack;				// - 
//   mountPoint = 1;						// - 
//   mass = 38;							// - 
//   rotation = "0 1 0 180";					// - 
//};								// -[/soph]

datablock ItemData(PolarArmorPlatePack)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "pack_barrel_elf.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

   image = "PolarArmorPlatePackImage";
	pickUpName = "a polarized armor plate";
};

function PolarArmorPlatePackImage::onMount(%data, %obj, %node)
{
//     %obj.universalResistFactor = 0.5;
//     %obj.armorPlate = true;
//     %obj.client.updateSensorPackText("Grav");
}

function PolarArmorPlatePackImage::onUnmount(%data, %obj, %node)
{
//     %obj.universalResistFactor = 1.0;
//     %obj.armorPlate = false;
//     %obj.client.updateSensorPackText(0);
}

function PolarArmorPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
