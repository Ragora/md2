if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//datablock ShapeBaseImageData(RMSScatterpackAmmoPackImage)
//{
//   shapeFile = "stackable5m.dts";
//   item = RMSScatterpackAmmoPack;
//   mountPoint = 1;
//   offset = "0 0 0";
//   rotation = "1 0 0 0";
//
//   usesEnergy = true;
//   minEnergy = 1;
//
//	stateName[0] = "Idle";
//	stateTransitionOnTriggerDown[0] = "Activate";
//	
//	stateName[1] = "Activate";
//	stateScript[1] = "onActivate";
//	stateSequence[1] = "fire";
//	stateTransitionOnTriggerUp[1] = "Deactivate";
//   stateTransitionOnNoAmmo[1] = "Deactivate";
//
//	stateName[2] = "Deactivate";
//	stateScript[2] = "onDeactivate";
//	stateTransitionOnTimeout[2] = "Idle";
//};

//datablock ItemData(RMSScatterpackAmmoPack)
//{
//   className = Pack;
//   catagory = "Packs";
//   shapeFile = "stackable5m.dts";
//   mass = 1;
//   elasticity = 0.2;
//   friction = 0.6;
//   pickupRadius = 2;
//   rotate = true;
//   image = "RMSScatterpackAmmoPackImage";
//	pickUpName = "a RMS scatterpack missile pack";
//};

function RMSScatterpackAmmoPackImage::onMount(%data, %obj, %node)
{
}

function RMSScatterpackAmmoPackImage::onUnmount(%data, %obj, %node)
{
	%obj.setImageTrigger(%node, false);
}

function checkRMSCanLoad(%pl)
{
   InitContainerRadiusSearch(%pl.getPosition(), 10, $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.team == %pl.client.team && %int.isRMS)
         return %int;
      else if(%int.getDatablock().isNightshade && %int.team == %pl.client.team)
         return isObject($RMSSilo[%pl.client.team]) ? $RMSSilo[%pl.client.team] : 0;
   }

   return false;
}

function RMSScatterpackAmmoPackImage::onActivate(%data, %obj, %slot)
{
   %RMS = checkRMSCanLoad(%obj);

   if(%RMS)
   {
      if(%RMS == 1)
      {
         messageClient(%obj.client, 'MsgRMSBatteryUseless', '\c2The Nuke Silo is already loaded.');
         %obj.setImageTrigger(%slot, false);
         return;
      }
      else
      {
         messageClient(%obj.client, 'MsgRMSBatteryLoaded', '\c2Loading Nuke Silo.');
         %obj.unmountImage(%slot);
         %obj.decInventory(%data.item, 1);
         %RMS.isLoaded = true;
         %RMS.loadType = 1;
         %RMS.lockduration = 10 * 60000;
         %RMS.play3D(MBLSwitchSound);
         return;
      }
   }
   else
   {
      messageClient(%obj.client, 'MsgRMSBatteryRange', '\c2There is no Nuke Silo or Nightshade in range.');
      %obj.setImageTrigger(%slot, false);
      return;
   }
}

function RMSScatterpackAmmoPackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger(%slot,false);
}

function RMSScatterpackAmmoPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
