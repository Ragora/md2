// ------------------------------------------------------------------
// ENERGY PACK
// can be used by any armor type
// does not have to be activated
// increases the user's energy recharge rate
// -Nite-1501
//Added a booster function to the energy pack..to emulate T:V epack
//not very powerful uses all the players energy, and needs 95% to be fired
datablock ShapeBaseImageData(EnergyPackImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   item = EnergyPack;
   mountPoint = 1;
   offset = "0 0 0";
   rechargeRateBoost = 0.23;
   
    usesEnergy = true;
    minEnergy = -1;
	//stateName[0] = "default";
	//stateSequence[0] = "activation";
    stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";

	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
 	stateTransitionOnTriggerUp[1] = "Deactivate";
    stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(EnergyPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_energy.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "EnergyPackImage";
	pickUpName = "an energy pack";

   computeCRC = true;

};

function EnergyPackImage::onMount(%data, %obj, %node)
{
	%obj.setRechargeRate(%obj.getRechargeRate() + %data.rechargeRateBoost); // 0.256 + 0.15 = 0.271
   %obj.hasEnergyPack = true; // set for sniper check
  // echo(%obj.getRechargeRate());
     %obj.BAPackID = 1;
}

function EnergyPackImage::onUnmount(%data, %obj, %node)
{
	%obj.setRechargeRate(%obj.getRechargeRate() - %data.rechargeRateBoost);
   %obj.hasEnergyPack = "";
     %obj.BAPackID = 0;
}

function EnergyPackImage::onActivate(%data, %obj, %slot)
{
   //call function
   ArmorBoosterMod(%obj);
 // %obj.useEnergy = true;//(%maxEnergy);
}

function EnergyPackImage::onDeactivate(%data, %obj, %slot)
{
  ArmorBoosterMod(%obj);
}

function EnergyPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
// Function Booster for armor
function ArmorBoosterMod(%obj)
{
  if(%obj.overdriveMode)
  {
     messageClient(%obj.client, 'MsgPurgeNoOD', '\c2Cannot purge in overdrive mode.');
     %obj.setImageTrigger($BackpackSlot, false);
     return;
  }
     
 // echo(%obj.getDatablock().maxEnergy);
 //get armors max energy output
  %maxEnergy = %obj.getDatablock().maxEnergy;//getEnergyLevel();
  //compare evergy levels to max energy, +5 so that check works smoother
  if(%obj.getEnergyLevel() >= %maxEnergy+5)
  {
     messageClient(%obj.client, 'MsgPurgeNoGood', '\c2You need full energy to purge.');
     %obj.setImageTrigger($BackpackSlot, false);     
     return;
  }
  //compare energy if levels are within 95% of max allow the fire else print message
  if(%obj.getEnergyLevel() <= %maxEnergy/1.1)
  {
     messageClient(%obj.client, 'MsgPurgeNoGood', '\c2You need full energy to purge.');
     %obj.setImageTrigger($BackpackSlot, false);
     return;
  }     
  //play message when jets are fired
  messageClient(%obj.client, 'MsgPurgeGood', '\c2Armor energy cache purged.');
  //ArmorBoosterMod(%obj);
  //play sound
  serverPlay3D(PlasmaFireSound, %obj.getTransform());
  %force = %obj.getMass(); // get armor mass

  if(%mass > 100)
      %mass = 100;

 // echo(%obj.getMass());
  createLifeEmitter(vectorAdd(%obj.getWorldBoxCenter(), "0 0 -0.5"), "SatchelExplosionSmokeEmitter", 300);
//  spawnProjectile(%obj, LinearFlareProjectile, MBDisplayCharge);

       %vec = vectorScale(%obj.getMuzzleVector(0), 45); // Adding to the current velocity..
   //get look vector
  %newVec = VectorScale(%vec, %force);  //vector scale
  %obj.applyImpulse(%obj.getTransform(), %newVec); //apply speed
  %obj.useEnergy(%maxEnergy);  //use armors max energy
}

