if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(BlasterCannonPackImage)
{
   shapeFile = "camera.dts";
   item = BlasterCannonPack;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = calcThisRotD("90 0 0");
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(BlasterCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "camera.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "BlasterCannonPackImage";
	pickUpName = "a mitzi light cannon";
};

datablock ShapeBaseImageData(BlasterCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "-0.5 0 0.2";
   rotation = "0 1 0 32";
   item = BlasterCannonPack;
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.5;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = BlasterFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(BlasterCannonDImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "-0.5 0 0.2";
   rotation = "0 1 0 32";
   item = BlasterCannonPack;
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.5;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = BlasterFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function BlasterCannonPackImage::onMount(%this,%obj,%slot)
{
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(BlasterCannonDImage, 7);
   else
      %obj.mountImage(BlasterCannonImage, 7);
}

function BlasterCannonPackImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(7);
}

function SBPFireTimeoutClear(%obj)
{
   %obj.fireTimeoutSBP = 0;
}

function BlasterCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;	// _here_ +soph
   if(%obj.fireTimeoutSBP)
      return;

   if(%obj.getInventory("MBCannonCapacitor") > 4)
   {
//      %p = new LinearProjectile()
      %p = new LinearFlareProjectile()
      {
//         dataBlock        = GalletBolt;
         dataBlock        = MitziSingularityBlast;
         initialDirection = %obj.getMuzzleVector(7);
         initialPosition  = %obj.getMuzzlePoint(7);
         sourceObject     = %obj;
         sourceSlot       = 7;
      };
      MissionCleanup.add(%p);
      %p.lastReflectedFrom = %obj;
      
   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
//      %obj.useEnergy(15);
      %obj.setImageTrigger(7, true);
      %obj.play3D(PlasmaFireSound);
//      %obj.play3D(StarburstFireSound);
      %obj.fireTimeoutSBP = 1000; //-Nite-  increased fire rate  was 1000 //MrKeen - changed it back
      schedule(%obj.fireTimeoutSBP, 0, "SBPFireTimeoutClear", %obj);
      %obj.decInventory("MBCannonCapacitor", 5);

      if(!%obj.charging)
        ChargeMBC(%obj);      
   }
   
   %obj.setImageTrigger(7, false);
   %obj.setImageTrigger(%slot, false);
}

function BlasterCannonPackImage::onDeactivate(%data, %obj, %slot)
{
   %obj.setImageTrigger(7, false);
	%obj.setImageTrigger(%slot,false);
}

function BlasterCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
