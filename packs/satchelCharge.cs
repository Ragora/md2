if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Satchel Charge pack
// can be used by any armor type
// when activated, throws the pack -- when activated again (before
// picking up another pack), detonates with a BIG explosion.

Meltdown.satchelFix = false;

//--------------------------------------------------------------------------
// Sounds

datablock AudioProfile(SatchelChargeActivateSound)
{
   filename    = "fx/packs/satchel_pack_activate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock AudioProfile(SatchelChargeExplosionSound)
{
   filename = "fx/packs/satchel_pack_detonate.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(SatchelChargePreExplosionSound)
{
   filename    = "fx/explosions/explosion.xpl03.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterSatchelChargeExplosionSound)
{
   filename    = "fx/weapons/mortar_explode_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

//----------------------------------------------------------------------------
// Satchel Debris
//----------------------------------------------------------------------------
datablock ParticleData( SDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;  
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.4 0.4 0.4 1.0";
   colors[1]     = "0.3 0.3 0.3 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 2.0;
   sizes[2]      = 3.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( SDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "SDebrisSmokeParticle";
};


datablock DebrisData( SatchelDebris )
{
   emitters[0] = SDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.3;
   lifetimeVariance = 0.02;
};             

//----------------------------------------------------------------------------
// Bubbles
//----------------------------------------------------------------------------
datablock ParticleData(SatchelBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 600;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 2.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.8;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SatchelBubbleEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 7.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MortarExplosionBubbleParticle";
};

//--------------------------------------------------------------------------
// Satchel Explosion Particle effects
//--------------------------------------
datablock ParticleData(SatchelExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.0;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.2 0.2 0.2 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 7.0;
   sizes[1]      = 17.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.4;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(SatchelExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 14.25;
   velocityVariance = 2.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 200;

   particles = "SatchelExplosionSmoke";
};

datablock ParticleData(UnderwaterSatchelExplosionSmoke)
{
   dragCoeffiecient     = 105.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.025;

   constantAcceleration = -1.0;
   
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.4 0.4 1.0 1.0";
   colors[1]     = "0.4 0.4 1.0 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 7.0;
   sizes[1]      = 17.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterSatchelExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 14.25;
   velocityVariance = 2.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 200;

   particles = "UnderwaterSatchelExplosionSmoke";
};


datablock ParticleData(SatchelSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "special/bigSpark";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(SatchelSparksEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 20.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "SatchelSparks";
};

datablock ParticleData(UnderwaterSatchelSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/underwaterSpark";
   colors[0]     = "0.6 0.6 1.0 1.0";
   colors[1]     = "0.6 0.6 1.0 1.0";
   colors[2]     = "0.6 0.6 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.75;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(UnderwaterSatchelSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 0;
   ejectionVelocity = 30;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 70;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterSatchelSparks";
};


//---------------------------------------------------------------------------
// Explosion
//---------------------------------------------------------------------------

datablock ExplosionData(SatchelSubExplosion)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.5 0.5 0.5";

   debris = SatchelDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 80;
   debrisNum = 8;
   debrisVelocity = 60.0;
   debrisVelocityVariance = 15.0;

   lifetimeMS = 1000;
   delayMS = 0;

   emitter[0] = SatchelExplosionSmokeEmitter;
   emitter[1] = SatchelSparksEmitter;

   offset = 0.0;

   playSpeed = 1.5;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "3.0 3.0 3.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SatchelSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.7 0.7 0.7";

   debris = SatchelDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisVelocity = 60.0;
   debrisVelocityVariance = 15.0;

   lifetimeMS = 1000;
   delayMS = 50;

   emitter[0] = SatchelExplosionSmokeEmitter;
   emitter[1] = SatchelSparksEmitter;

   offset = 9.0;

   playSpeed = 1.5;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SatchelSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "1.0 1.0 1.0";

   debris = SatchelDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisVelocity = 60.0;
   debrisVelocityVariance = 15.0;

   lifetimeMS = 2000;
   delayMS = 100;

   emitter[0] = SatchelExplosionSmokeEmitter;
   emitter[1] = SatchelSparksEmitter;

   offset = 9.0;

   playSpeed = 2.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SatchelMainExplosion)
{
   soundProfile = SatchelChargePreExplosionSound;
   
   subExplosion[0] = SatchelSubExplosion;
   subExplosion[1] = SatchelSubExplosion2;
   subExplosion[2] = SatchelSubExplosion3;
};

//---------------------------------------------------------------------------
// Underwater Explosion
//---------------------------------------------------------------------------

datablock ExplosionData(UnderwaterSatchelSubExplosion)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.5 0.5 0.5";


   lifetimeMS = 1000;
   delayMS = 0;

   emitter[0] = UnderwaterSatchelExplosionSmokeEmitter;
   emitter[1] = UnderwaterSatchelSparksEmitter;
   emitter[2] = SatchelBubbleEmitter;

   offset = 0.0;

   playSpeed = 0.75;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "2.5 2.5 2.5";
   sizes[2] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterSatchelSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "0.7 0.7 0.7";


   lifetimeMS = 1000;
   delayMS = 50;

   emitter[0] = UnderwaterSatchelExplosionSmokeEmitter;
   emitter[1] = UnderwaterSatchelSparksEmitter;
   emitter[2] = SatchelBubbleEmitter;

   offset = 9.0;

   playSpeed = 0.75;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "0.75 0.75 0.75";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterSatchelSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   explosionScale = "1.0 1.0 1.0";


   lifetimeMS = 2000;
   delayMS = 100;

   emitter[0] = UnderwaterSatchelExplosionSmokeEmitter;
   emitter[1] = UnderwaterSatchelSparksEmitter;
   emitter[2] = SatchelBubbleEmitter;

   offset = 9.0;

   playSpeed = 1.25;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterSatchelMainExplosion)
{
   soundProfile = UnderwaterSatchelChargeExplosionSound;

   subExplosion[0] = UnderwaterSatchelSubExplosion;
   subExplosion[1] = UnderwaterSatchelSubExplosion2;
   subExplosion[2] = UnderwaterSatchelSubExplosion3;
};

//--------------------------------------------------------------------------
// Projectile

//-------------------------------------------------------------------------
// shapebase datablocks
datablock ShapeBaseImageData(SatchelChargeImage)
{
   shapeFile = "pack_upgrade_satchel.dts";
   item = SatchelCharge;
   mountPoint = 1;
   offset = "0 0 0";
   emap = true;
};

datablock ItemData(SatchelCharge)
{
   className = Pack;
   catagory = "Packs";
   image = SatchelChargeImage;
   shapeFile = "pack_upgrade_satchel.dts";

   explosion = SatchelMainExplosion ;				// +soph
   underwaterExplosion = UnderwaterSatchelMainExplosion ;	// +soph

   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   pickUpName = "a satchel charge pack";

   computeCRC = true;
};

Meltdown.satchelFix = true;

datablock ItemData(SatchelChargeThrown)
{
   shapeFile = "pack_upgrade_satchel.dts";
   explosion = SatchelMainExplosion;
   underwaterExplosion = UnderwaterSatchelMainExplosion;
   mass = 1.2;
   elasticity = 0 ;						// = 0.1; -soph
   friction = 99 ;						// = 0.9; -soph
   rotate = true ;						// false; -soph
   pickupRadius = 0;
   noTimeout = true;
   armDelay = 3000;
   maxDamage = 0.5;

   kickBackStrength    = 4000;

   computeCRC = true;
};

//--------------------------------------------------------------------------

function SatchelCharge::onUse(%this, %obj)
{
   %item = new Item() {
      dataBlock = SatchelChargeThrown;
      rotation = "0 0 1 " @ (getRandom() * 360);
   };
   MissionCleanup.add(%item);
   // take pack out of inventory and unmount image
   %obj.decInventory(SatchelCharge, 1);
   %obj.throwObject(%item);
   //error("throwing satchel charge #" @ %item);
   %obj.thrownChargeId = %item;
   %item.sourceObject = %obj;
   %item.armed = false;
   %item.damaged = 0.0;
   %item.thwart = false;
   // arm itself 3 seconds after being thrown
   %item.chargeType = %obj.client.satchelMode;
   schedule(%item.getDatablock().armDelay, %item, "initArmSatchelCharge", %item);
   messageClient(%obj.client, 'MsgSatchelChargePlaced', "\c2Satchel charge deployed.");
}

function satchelObserveCheck(%obj, %cl)
{
   if(isObject(%cl))
   {
      if(isObject(%obj))
         schedule(1250, 0, "satchelObserveCheck", %obj, %cl);
      else
         %cl.setControlObject(%cl.player);
   }
}

function initArmSatchelCharge(%satchel)
{
   // "deet deet deet" sound
   %satchel.playAudio(1, SatchelChargeActivateSound);
   // also need to play "antenna extending" animation
   %satchel.playThread(0, "deploy");
   %satchel.playThread(1, "activate");

   // delay the actual arming until after sound is done playing
   schedule( 2200, 0, "armSatchelCharge", %satchel );
}

function armSatchelCharge(%satchel)
{
   %satchel.armed = true;
   commandToClient( %satchel.sourceObject.client, 'setSatchelArmed' );

   if(%satchel.chargeType == 2 || %satchel.chargeType == 3)
   {
      satchelObserveCheck(%satchel.sourceObject, %satchel.sourceObject.client);
      %satchel.sourceObject.client.camera.setTransform(%satchel.getTransform());
      %satchel.sourceObject.client.camera.getDataBlock().setMode( %satchel.sourceObject.client.camera, "pre-game", %satchel);
      %satchel.sourceObject.client.setControlObject(%satchel.sourceObject.client.camera);
   }
}

function detonateSatchelCharge(%player)
{
   %satchelCharge = %player.thrownChargeId;
   // can't detonate the satchel charge if it isn't armed
   if(!%satchelCharge.armed)
      return;

   //error("Detonating satchel charge #" @ %satchelCharge @ " for player #" @ %player);

   if(%satchelCharge.getDamageState() !$= Destroyed)
   {   
      %satchelCharge.setDamageState(Destroyed);
      %satchelCharge.blowup(); 
   }
   
//   if(isObject(%player))   				// -soph
//      %player.thrownChargeId = 0;			// -soph
   
   // Clear the player's HUD:
   %player.client.clearBackpackIcon();   
}

function SatchelChargeThrown::onEnterLiquid(%data, %obj, %coverage, %type)
{
   // lava types
   if(%type >=4 && %type <= 6)
   {
      if(%obj.getDamageState() !$= "Destroyed")
      {
         %obj.armed = true;
         detonateSatchelCharge(%obj.sourceObject);
         return;
      }
   }
   
   // quickSand   
   if(%type == 7)
      if(isObject(%obj.sourceObject))
         %obj.sourceObject.thrownChargeId = 0;
      
  Parent::onEnterLiquid(%data, %obj, %coverage, %type);
}

function SatchelChargeImage::onMount(%data, %obj, %node)
{
   %obj.thrownChargeId = 0;
}

function SatchelChargeImage::onUnmount(%data, %obj, %node)
{
}

function sRadiusExplosion(%explosionSource, %position, %radius, %damage, %impulse, %sourceObject, %damageType)
{
//   error("RadiusExplosion("@%explosionSource@", "@%position@", "@%radius@", "@%damage@", "@%impulse@", "@%sourceObject@", "@%damageType@" )");

   InitContainerRadiusSearch(%position, %radius, $TypeMasks::PlayerObjectType      |
                                                 $TypeMasks::VehicleObjectType     |
                                                 $TypeMasks::StaticShapeObjectType |
                                                 $TypeMasks::TurretObjectType      |
                                                 $TypeMasks::ForceFieldObjectType  |
                                                 $TypeMasks::CorpseObjectType      |
                                                 $TypeMasks::ItemObjectType);

   %numTargets = 0;
   while ((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

      if(%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if(%mount.getDataBlock().isProtectedMountPoint[%found] && %mount.getEnergyLevel() < 5)
            {
               continue;
            }
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%position, %targetObject,
                                        ($TypeMasks::InteriorObjectType |
                                         $TypeMasks::TerrainObjectType |
                                         $TypeMasks::ForceFieldObjectType |
                                         $TypeMasks::StaticShapeObjectType |
                                         $TypeMasks::VehicleObjectType));
      if (%coverage == 0)
         continue;

      //if ( $splashTest )
         %amount = (1.0 - ((%dist / %radius) * 0.75)) * %coverage * %damage;
      //else
         //%amount = (1.0 - (%dist / %radius)) * %coverage * %damage;

      //error( "damage: " @ %amount @ " at distance: " @ %dist @ " radius: " @ %radius @ " maxDamage: " @ %damage );

      %data = %targetObject.getDataBlock();
      %className = %data.className;

      if (%impulse && %data.shouldApplyImpulse(%targetObject))
      {
         if(%targetObject.getClassName() $= Player)
         {
            %p = %targetObject.getWorldBoxCenter();
            %momVec = VectorSub(%p, %position);
            %momVec = VectorNormalize(%momVec);
            %impulseVec = VectorScale(%momVec, (0.1*%impulse) * (1.0 - (%dist / %radius)));
            %doImpulse = true;
         }
         else
         {
            %p = %targetObject.getWorldBoxCenter();
            %momVec = VectorSub(%p, %position);
            %momVec = VectorNormalize(%momVec);
            %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));
            %doImpulse = true;
         }
      }
      else if( %className $= WheeledVehicleData || %className $= FlyingVehicleData || %className $= HoverVehicleData )
      {
         %p = %targetObject.getWorldBoxCenter();
         %momVec = VectorSub(%p, %position);
         %momVec = VectorNormalize(%momVec);
         %impulseVec = VectorScale(%momVec, %impulse * (1.0 - (%dist / %radius)));


         if( getWord( %momVec, 2 ) < -0.5 )
            %momVec = "0 0 1";

         // Add obj's velocity into the momentum vector
         %velocity = %targetObject.getVelocity();
         //%momVec = VectorNormalize( vectorAdd( %momVec, %velocity) );
         %doImpulse = true;
      }
      else
      {
         %momVec = "0 0 1";
         %doImpulse = false;
      }

      if(%amount > 0)
      {
         if(isObject(%targetObject.deployBase))
         {
            if(%targetObject.isFF)
               %targetObject.deployBase.damage(%sourceObject, %position, %amount, %damageType);
            else
               %targetObject.damage(%sourceObject, %position, %amount, %damageType);
         }
         else
            %targetObject.damage(%sourceObject, %position, %amount, %damageType);
      }
      else if( %explosionSource.getDataBlock().getName() $= "ConcussionGrenadeThrown" && %data.getClassName() $= "PlayerData" )
	  {
         %data.applyConcussion( %dist, %radius, %sourceObject, %targetObject );

 	  	if(!$teamDamage && %sourceObject != %targetObject && %sourceObject.client.team == %targetObject.client.team)
 	  	{
			messageClient(%targetObject.client, 'msgTeamConcussionGrenade', '\c1You were hit by %1\'s concussion grenade.', getTaggedString(%sourceObject.client.name));
		}
	  }
      if( %doImpulse && !%targetObject.inertialDampener && !%targetObject.getDatablock().forceSensitive)
         %targetObject.applyImpulse(%position, %impulseVec);
   }
}

function SatchelChargeThrown::onDestroyed(%this, %object, %lastState)
{
   if(%object.kaboom)
      return;
   else
   {
      %object.kaboom = true;

      if( isObject( %object.sourceObject ) )   		// +soph
         %object.sourceObject.thrownChargeId = 0 ;	// +soph

      // the "thwart" flag is set if the charge is destroyed with weapons rather
      // than detonated. A less damaging explosion, but visually the same scale.
      if(%object.thwart)
      {
         messageClient(%object.sourceObject.client, 'msgSatchelChargeShot', "\c2Satchel charge shot!");
         if(%object.chargeType == 0)
         {
            %dmgRadius = 20;
            %dmgMod = 1.25 ;	// = 2.5; -soph
            %expImpulse = 1500;
            %dmgType = $DamageType::SatchelCharge;
         }
         else if(%object.chargeType == 1)
         {
            %dmgRadius = 10;
            %dmgMod = 0.001;
            %expImpulse = 20000;
            %dmgType = 127;
         }
         else if(%object.chargeType == 2)
         {
            %dmgRadius = 35;
            %dmgMod = 5.0;
            %expImpulse = 2500;
            %dmgType = $DamageType::SatchelCharge;
         }
         else if(%object.chargeType == 3)
         {
            %dmgRadius = 10;
            %dmgMod = 0.001;
            %expImpulse = 20000;
            %dmgType = 127;
         }
         else if(%object.chargeType == 4)
         {
            %dmgRadius = 0;
            %dmgMod = 0.0;
            %expImpulse = 0;
            %dmgType = $DamageType::Default;
            BlackHoleTrigger(%object);
         }
      }
      else
      {
         messageClient(%object.sourceObject.client, 'msgSatchelChargeDetonate', "\c2Satchel charge detonated!");
         if(%object.chargeType == 0)
         {
            %dmgRadius = 40;
            %dmgMod = 6.0;
            %expImpulse = 15000;
            %dmgType = $DamageType::SatchelCharge;
            schedule( 50 , 0 , "RadiusExplosion" , %object , %object.getPosition() , %dmgRadius / 2 , %dmgMod / 2 , %expImpulse / 2 , %object.sourceObject , %dmgType );	// +soph
         }
         else if(%object.chargeType == 1)
         {
            %dmgRadius = 10;
            %dmgMod = 0.001;
            %expImpulse = 20000;
            %dmgType = 127;
         }
         else if(%object.chargeType == 2)
         {
            %dmgRadius = 35;
            %dmgMod = 5.0;
            %expImpulse = 2500;
            %dmgType = $DamageType::SatchelCharge;
         }
         else if(%object.chargeType == 3)
         {
            %dmgRadius = 10;
            %dmgMod = 0.001;
            %expImpulse = 20000;
            %dmgType = 127;
         }
         else if(%object.chargeType == 4)
         {
            %dmgRadius = 0;
            %dmgMod = 0.0;
            %expImpulse = 0;
            %dmgType = $DamageType::Default;
            BlackHoleTrigger(%object);
         }
      }

      %object.blowingUp = true;
//      sRadiusExplosion(%object, %object.getPosition(), %dmgRadius, %dmgMod, %expImpulse, %object.sourceObject, %dmgType);	// -soph
      RadiusExplosion(%object, %object.getPosition(), %dmgRadius, %dmgMod, %expImpulse, %object.sourceObject, %dmgType);	// +soph

      %object.schedule(500, "delete");
   }
}

function SatchelChargeThrown::onCollision(%data,%obj,%col)
{
   // Do nothing...
}

function SatchelChargeThrown::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   if (!%object.blowingUp)
   {
      %targetObject.damaged += %amount;

      if(%targetObject.damaged >= %targetObject.getDataBlock().maxDamage && 
         %targetObject.getDamageState() !$= Destroyed)
      {   
         %targetObject.thwart = true;
         %targetObject.setDamageState(Destroyed);
         %targetObject.blowup(); 
         
         // clear the player's HUD:
         if( isObject( %targetObject.sourceObject ) )	// +[soph]
         {						// +
            %obj.sourceObject.thrownChargeId = 0 ;	// +[/soph]
            %targetObject.sourceObject.client.clearBackPackIcon();
         }
      }
   }
}

function SatchelCharge::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function SatchelCharge::damageObject( %data , %object , %source , %position , %amount , %type )	// +[soph]
{												// +
   if( !%object.blowingup && %amount > 0.35 )							// +
   {												// +
      %object.blowingup = true ;								// +
      %objectPosition = %object.getWorldBoxCenter() ;						// +
      %object.setDamageState( Destroyed ) ;							// +
//      %object.blowup() ; 									// +
      RadiusExplosion( %object , %objectPosition, 20 , 0.625 , 3000 , %source , $DamageType::SatchelCharge ) ;
      if( %object.isStatic() )									// +
      {												// +
         %object.respawn() ;									// +
         %obj.setDamageState( Enabled ) ;							// +
      }												// +
      else											// +
         %object.schedule( 32 , delete ) ;							// +
   }												// +
}												// +[/soph]