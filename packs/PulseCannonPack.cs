// ------------------------------------------------------------------

datablock ShapeBaseImageData(PulseCannonPackImage)
{
   shapeFile = "pack_barrel_fusion.dts";
   item = PulseCannonPack;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(PulseCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_barrel_fusion.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "PulseCannonPackImage";
	pickUpName = "dual pulse cannons";
};

datablock ShapeBaseImageData(PulseCannon2DImage)
{
   className = WeaponImage;
   shapeFile = "turret_mortar_large.dts";
   item = Blaster;
   offset = "-0.5 0 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   
   projectile = BomberFusionBolt;
   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.25;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire"; 
//   stateSound[3]                    = PulseCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(PulseCannonDImage) : PulseCannon2DImage
{
   offset = "0.5 0 0.5";
   stateSequence[2]                 = "Deploy";
   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(PulseCannon2Image)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   item = Blaster;
   offset = "-0.5 0 0.45";
   rotation = "0 1 0 45";
   emap = true;
   mountPoint = 1;
   
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   
   projectile = BomberFusionBolt;
   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.25;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "recoil";
//   stateSound[3]                    = PulseCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(PulseCannonImage) : PulseCannon2Image
{
   offset = "0.35 0 0.6";
   rotation = "0 1 0 -45";

   stateSequence[2]                 = "Deploy";
   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(PulseCannonHHImage) : PulseCannonImage
{
   item = "";	// +soph
   usesEnergy = true;
   fireEnergy = 10;	// fireEnergy = 12;
   minEnergy = 10;	// minEnergy = 12;

   shapeFile = "TR2weapon_mortar.dts";
   offset = "0.25 -0.25 0";
   rotation = calcThisRotD("180 0 180"); //"1 0 0 180";
   emap = true;
   mountPoint = 0;

   projectile = BomberFusionBolt;
   projectileType = LinearFlareProjectile;

   projectileSpread = 12.0 / 1000.0;
   
   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.2;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "recoil";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;
//   stateSound[3]                    = PulseCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.05;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
};

function PulseCannonHHImage::onMount(%this,%obj,%slot)
{
     Parent::onMount(%this,%obj,%slot);

   %obj.unmountImage(1);
   %obj.unmountImage(7);
   %obj.weaponPackDisabled = true;	// %obj.fireTimeoutPC = true; -soph
   %obj.play3D("PBLSwitchSound");
}

function PulseCannonHHImage::throwWeapon( %this ) {}		// +soph

function PulseCannonHHImage::onUnmount(%this,%obj,%slot)
{
     Parent::onUnmount(%this,%obj,%slot);

     if(%obj.client.race $= Bioderm)
     {
          %obj.mountImage(PulseCannonDImage, 1);
          %obj.mountImage(PulseCannon2DImage, 7);
     }
     else
     {
          %obj.mountImage(PulseCannonImage, 1);
          %obj.mountImage(PulseCannon2Image, 7);
     }

//   %obj.fireTimeoutPC = false;			// -soph
     %obj.weaponPackDisabled = false;
}

function PulseCannonHHImage::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);

     if(%p)
     {
          %obj.play3d(PulseCannonFire);
          %obj.packWeaponTimeout = getSimTime() + 500;
     }
}

function PulseCannonPackImage::onMount(%this,%obj,%slot)
{
     %obj.BAPackID = 10;
     
   if(%obj.client.race $= Bioderm)
   {
      %obj.mountImage(PulseCannonDImage, 1);
      %obj.mountImage(PulseCannon2DImage, 7);
   }
   else
   {
      %obj.mountImage(PulseCannonImage, 1);
      %obj.mountImage(PulseCannon2Image, 7);
   }
   if( !%obj.packWeaponTimeout )			// +[soph]
      %obj.packWeaponTimeout = getSimTime();
   else
      %obj.weaponPackDisabled = false;			// +[/soph]
}

function PulseCannonPackImage::onUnmount(%this,%obj,%slot)
{
     %obj.BAPackID = 0;
   %obj.unmountImage(1);
   %obj.unmountImage(7);
}

function PCFireTimeoutClear(%obj)	// no longer used -soph
{
   if( !%obj.fireTimeoutPC )	// if(%obj.fireTimeoutPC == 1) -soph
     return;
     
//   %obj.fireTimeoutPC = 0;	// -soph
   %obj.play3D(PlasmaReloadSound);
}

function PulseCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;				// _here_ +soph
   %time = getSimTime();						// +soph
   if( %obj.weaponPackDisabled || %obj.packWeaponTimeout > %time )	// if(%obj.fireTimeoutPC) -soph
      return;

   if(%obj.invPackBlockTime > getSimTime())
      return;
      
   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
       %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;
   %enUse = 25;		// %enUse = 30; -soph
   
   if(%obj.powerRecirculator)
      %enUse *= 0.75;
      
   if(%obj.getEnergyLevel() >= %enUse)
   {
        %obj.setImageTrigger(1, true);
        %obj.setImageTrigger(7, true);

        %p = new LinearFlareProjectile()
        {
              dataBlock        = BomberFusionBolt;
              initialDirection = %obj.getMuzzleVector(1);
              initialPosition  = %obj.getMuzzlePoint(1);
              sourceObject     = %obj;
              sourceSlot       = 1;
        };
        MissionCleanup.add(%p);
        
        %q = new LinearFlareProjectile()
        {
              dataBlock        = BomberFusionBolt;
              initialDirection = %obj.getMuzzleVector(7);
              initialPosition  = %obj.getMuzzlePoint(7);
              sourceObject     = %obj;
              sourceSlot       = 7;
        };
        MissionCleanup.add(%q);

        // Vehicle Damage Modifier
        if(%vehicle)
        {
          %p.vehicleMod = %vehicle.damageMod;
          %q.vehicleMod = %vehicle.damageMod;
        }
        
        if(%obj.damageMod)
        {
          %p.damageMod = %obj.damageMod;
          %q.damageMod = %obj.damageMod;
        }          
      
        %obj.useEnergy(%enUse);
        %obj.play3D(PulseCannonFire);
        %obj.packWeaponTimeout = %time + 750;				// %obj.fireTimeoutPC = 500; -soph
//        schedule(%obj.fireTimeoutPC, 0, "PCFireTimeoutClear", %obj);	// -soph
        %obj.schedule( 500 , play3D , "PBLSwitchSound" );		// +[soph]
       %obj.setImageTrigger(1, false);
       %obj.setImageTrigger(7, false);					// +[/soph]
   }
   else
   {
        %obj.packWeaponTimeout = %time + 750;				// %obj.fireTimeoutPC = 500; -soph
//        schedule(%obj.fireTimeoutPC, 0, "PCFireTimeoutClear", %obj);	// -soph
         
          %obj.play3D(MortarDryFireSound);
          %obj.setImageTrigger(1, false);
          %obj.setImageTrigger(7, false);
   }
      
   %obj.setImageTrigger(%slot, false);
}

function PulseCannonPackImage::onDeactivate(%data, %obj, %slot)
{
   %obj.setImageTrigger(1, false);
   %obj.setImageTrigger(7, false);
}

function PulseCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}

function PulseCannonImage::onFire(%data, %obj, %slot)
{
     //
}

function PulseCannon2Image::onFire(%data,%obj,%slot)
{
     //
}

function PulseCannonDImage::onFire(%data, %obj, %slot)
{
     //
}

function PulseCannon2DImage::onFire(%data,%obj,%slot)
{
     //
}
