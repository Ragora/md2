if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------
// VectorPort PACK
// can be used by any armor type
// while activated, absorbs damage at cost of energy

datablock ShapeBaseImageData(VectorPortDecalImage)
{
   mountPoint = 1;
   shapeFile = "ammo_mortar.dts";
   offset = "0.15 -0.275 0";
   rotation = "0 1 0 90";
};

datablock ShapeBaseImageData(VectorPortPackImage)
{
   shapeFile = "ammo_mortar.dts";
   item = VectorPortPack;
   mountPoint = 1;

   offset = "-0.15 -0.275 0";
   rotation = "0 1 0 90";

   usesEnergy = true;
   minEnergy = 3;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
//	stateSound[1] = VectorPortPackActivateSound;
//   stateEnergyDrain[1] = 9;
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(VectorPortPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "VectorPortPackImage";
	pickUpName = "a vectorport pack";
};

function VectorPortPackImage::onMount(%data, %obj, %node)
{
   %obj.mountImage(VectorPortDecalImage, 7);
   %obj.wearingVP = true;
   
   setTargetSensorData(%obj.client.target, JammerSensorObjectPassive);
   %obj.setJammerFX( true );
}

function VectorPortPackImage::onUnmount(%data, %obj, %node)
{
   %obj.setImageTrigger(%node, false);
   %obj.unmountImage(7);
   %obj.wearingVP = false;
   
   setTargetSensorData(%obj.client.target, PlayerSensor);
   %obj.setJammerFX( false );
}

function getLOSInfoVP(%obj, %dist)
{
   %vec = %obj.getMuzzleVector(0);
   %pos = %obj.getMuzzlePoint(0);
   %nVec = VectorNormalize(%vec);
   %scVec = VectorScale(%nVec, %dist);
   %end = VectorAdd(%pos, %scVec);
   %result = containerRayCast(%pos, %end, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return %target;
   else
      return false;
}

function getMuzzleRaycastPtVP(%obj, %slot, %dist)
{
     %vec = %obj.getMuzzleVector(%slot);
     %pos = %obj.getMuzzlePoint(%slot);
     %nVec = VectorNormalize(%vec);
     %scVec = VectorScale(%nVec, %dist);
     %end = VectorAdd(%pos, %scVec);
     %searchResult = containerRayCast(%pos, %end, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType, %obj);
     %raycastPt = posFrRaycast(%searchResult);
     %raycastPt.raycast = %searchResult;
     
     return %raycastPt;
}

function VectorPortPackImage::onActivate(%data, %obj, %slot)
{
   if(%obj.vectorPortUsed)
   {
      commandToClient(%obj.client, 'BottomPrint', "The VectorPort is recharging, please wait.", 3, 1);
      %obj.setImageTrigger(%slot, false);
      return;
   }
   else
   {
      %point = getLOSInfoVP(%obj, 150);

      if(%point)
      {
        // ejectFlag(%obj);	// -soph
         if(%point.getType() & $TypeMasks::PlayerObjectType)
         {
              %epos = %point.getWorldBoxCenter();
              %evec = vectorNeg(vectorScale(%point.getForwardVector(), 1.5));
              %end = vectorAdd(%epos, %evec);
              %rot = %point.getRotation();
         }
         else
              %end = getMuzzleRaycastPtVP(%obj, 0, 150);

         %pos = %obj.getPosition();
         %dist = getDistance3D(%pos, %end);
         %distPct = %dist / 150;
         %maxEn = %obj.getMaxEnergy();
         %enUse = %obj.powerRecirculator ? %maxEn * 0.75 * %distPct : %maxEn * %distPct;
//         %cooldownTime = 5000 + (10000 * %distPct);

         if(%obj.getEnergyLevel() < %enUse)
         {
              commandToClient(%obj.client, 'BottomPrint', "Not enough energy to VectorPort this distance ("@mCeil(%dist)@")", 3, 1);
              %obj.vectorPortUsed = true;         
              schedule(1000, 0, "resetVectorPort", %obj);
              %obj.setImageTrigger(%slot, false);
              return;
         }

         if( %obj.holdingFlag )	// +[soph]
         {			// drop the flag halfway to allow it to be vectored out of boxes
              %obj.setPosition( VectorAdd( %obj.getMuzzlePoint( %slot ) , VectorScale( VectorNormalize( %obj.getMuzzleVector( %slot ) ) , %dist / 2 ) ) );
              ejectFlag(%obj);	// ^ lol holy crap ^
         }			// +[/soph]

         %obj.setPosition(%end);
         %obj.setVelocity("0 0 0");
         %obj.zapObject();
         %obj.useEnergy(%enUse);

         if(%rot !$= "")
              %obj.setRotation(%rot);

         %obj.vectorPortUsed = true;         
         vpGlitchCheck(%obj);
         commandToClient(%obj.client, 'BottomPrint', "Distance travelled: "@mCeil(%dist)@"m ("@mCeil(%distPct * 100)@"%)", 4, 1);
         schedule(3000, 0, "resetVectorPort", %obj);
      }
      else
         commandToClient(%obj.client, 'BottomPrint', "No teleport point in range.", 3, 1);

      %obj.setImageTrigger(%slot, false);
   }
}

function vpGlitchCheck(%obj)
{
   if( %obj.getState() $= "Dead" )
   {
      %obj.blowup();
      return;
   }
   %position = %obj.getWorldBoxCenter();
   InitContainerRadiusSearch( getWord( %position , 0 ) @ " " @ getWord( %position , 1 ) @ " " @ ( getWord( %position , 2 ) + 1 ) , 0.1, $TypeMasks::StaticShapeObjectType);

   while( ( %found = containerSearchNext() ) != 0 )	// +[soph]
      if( %found.getType() & ( $TypeMasks::StationObjectType ) )
         continue;
      else
      {
        // %obj.setInventory( SoulShieldMod , 0 );
         %obj.setDamageFlash( 0.5 + %obj.getDamageFlash() );
         %obj.getDatablock().damageObject( %obj , %obj , %obj.position , 0.01 , $DamageType::VectorAccident , %obj.getVelocity() , false );
         %obj.zapObject();
         if( !%obj.isMounted() )			// vectorjacking check
            schedule( 100 , 0 , vpGlitchCheck , %obj );
         break;
      }							// +[/soph]

//   if(ContainerSearchNext() != 0)	// -[soph]
//   {
//      %obj.takeDamage(10000);
//   }					// -[/soph]
}

function resetVectorPort(%obj)
{
   if(isObject(%obj))
   {
      %obj.vectorPortUsed = false;
//      if(%obj.wearingVP)
//         commandToClient(%obj.client, 'BottomPrint', "The VectorPort is ready.", 3, 1);
   }
}

function VectorPortPackImage::onDeactivate(%data, %obj, %slot)
{
   %obj.setImageTrigger(%slot, false);
}

function VectorPortPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}