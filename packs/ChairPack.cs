if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------

datablock ShapeBaseImageData(MegaDiscPackImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   item = MegaDiscPack;
   mountPoint = 1;
   offset = "0 0 0";
   isLarge = false;

   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
	stateSound[1] = MegaDiscTransform;
//   stateEnergyDrain[1] = 9;
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(MegaDiscPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_energy.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   isLarge = false;
   pickupRadius = 2;
   image = "MegaDiscPackImage";
	pickUpName = "a MegaDisc pack";
};

function MegaDiscPackImage::onMount(%data, %obj, %node)
{
}

function MegaDiscPackImage::onUnmount(%data, %obj, %node)
{
	%obj.setImageTrigger(%node, false);
}

datablock FlyingVehicleData(MegaDisc) : ShrikeDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_scout.dts";
   multipassenger = false;

   debrisShapeName = "vehicle_grav_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   cameraMaxDist = 10.0;
   cameraOffset = 0.7;
   cameraLag = 0.5;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
   explosion = DiscVehicleExplosion;
	explosionDamage = 0.9;
	explosionRadius = 10;
   isDisc = true;

   MegaDisc = 1;
   forceSensitive = true;

   maxDamage = 1.0;
   destroyedLevel = 1.0;

   isShielded = false;
   energyPerDamagePoint = 75;
   maxEnergy = 75;      // Afterburner and any energy weapon pool
   minDrag = 30;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.256;

   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 400;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't` whack out at very slow speeds
   maxForwardSpeed = 100;

   // Maneuvering
   maxSteeringAngle = 3;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 8;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 6;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 16.4 * 150;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1100;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 550;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 2;        // Height off the ground at rest
   createHoverHeight = 2;  // Height off the ground when created

   // Turbo Jet
   jetForce = 32 * 100 * 2.0;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 1;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 1.5;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 0.5;

   // Rigid body
   mass = 180;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 35;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 55;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 40;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.06;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 23.0;
   collDamageMultiplier   = 0.02;

   //
   minTrailSpeed = 999;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = FlyerJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = ArmorJetSound;
   engineSound = ScoutFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 10.0;
   mediumSplashSoundVelocity = 15.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 10.0;

   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound;

   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = SmallHeavyDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -1.5 0.5 ";
   damageLevelTolerance[0] = 0.3;
   damageLevelTolerance[1] = 0.7;
   numDmgEmitterAreas = 1;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_scout_grey";
   targetNameTag = 'Bean-with-Bacon';
   targetTypeTag = 'MegaDisc';
   sensorData = VehiclePulseSensor;

   runningLight[0] = ShrikeLight1;

   shieldEffectScale = "0.937 1.125 0.60";
};

datablock StaticShapeData(DiscDecal)
{
   mountPoint = 1;
   shapeFile = "disc.dts";
//   emap = true;
   maxDamage = MegaDisc.maxDamage;
   destroyedLevel = MegaDisc.destroyedLevel;
};

datablock StaticShapeData(MountDiscDecal)
{
   mountPoint = 1;
   shapeFile = "ammo_missile.dts";
   emap = true;
   maxDamage = MegaDisc.maxDamage;
   destroyedLevel = MegaDisc.destroyedLevel;
};

function DiscDecal::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function DiscDecal::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function MountDiscDecal::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function MountDiscDecal::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function MegaDisc::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

    %decal = new StaticShape()
    {
        scale = "10 14 14";
        dataBlock = "MountDiscDecal";
    };
    %obj.mountObject(%decal, 1);

    %d3cal = new StaticShape()
    {
        scale = "10 12 10";
        dataBlock = "DiscDecal";
    };
    %obj.mountObject(%d3cal, 2);
    %d3cal.playThread(0, "maintain");
    %d3cal.playThread(1, "ambient");

   %obj.schedule(50, "playThread", $ActivateThread, "activate");
}

function MegaDisc::deleteAllMounted(%data, %obj)
{
   %decal = %obj.getMountNodeObject(1);
   if(!%decal)
      return;

   %decal.delete();

   %decal = %obj.getMountNodeObject(2);
   if(!%decal)
      return;

   %decal.delete();
}

function MegaDisc::playerMounted(%data, %obj, %player, %node)
{
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);

      if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function MegaDisc::playerDismounted(%data, %obj, %player)
{
   setTargetSensorGroup(%obj.getTarget(), %obj.team);
   MegaDiscDisable(%player, %obj);
}

function cantMegaDisc(%pl)
{
     InitContainerRadiusSearch(%pl.getPosition(), 5, $TypeMasks::InteriorObjectType);

     while ((%int = ContainerSearchNext()) != 0)
     {
         %subStr = getSubStr(%int.interiorFile, 1, 4);
         if(%subStr !$= "rock" && %subStr !$= "spir" && %subStr !$= "misc")
            return true;
     }
}

function MegaDiscEnable(%pl)
{
  if(!cantMegaDisc(%pl))
  {
      ejectFlag(%pl);

      %vehicle = new FlyingVehicle()
      {
         dataBlock  = "MegaDisc";
         respawn    = "0";
         teamBought = %pl.client.Team;
         team = %pl.client.Team;
      };

      %vel = %pl.getVelocity();
      %vehicle.startFade(1, 0, true);
      %vehicle.setTransform(%pl.getTransform()); // modifyTransform(%pl.getTransform(), "0 0 1.5 0 0 0 0"));
      %pl.getDataBlock().onCollision(%pl, %vehicle, 0);
      %data = %vehicle.getDatablock();
      serverPlay3D("JetfireTransform", %pl.getTransform());
      %iVec = vectorScale(%vel, %vehicle.getMass());
      %vehicle.applyImpulse(%vehicle.getTransform(), %iVec);

   if((%data.sensorData !$= "") && (%vehicle.getTarget() != -1))
      setTargetSensorData(%vehicle.getTarget(), %data.sensorData);
   %vehicle.setRechargeRate(%data.rechargeRate);
   %vehicle.setEnergyLevel(%pl.getEnergylevel());

   if(%vehicle.mountable || %vehicle.mountable $= "")
      %data.isMountable(%vehicle, true);
   else
      %data.isMountable(%vehicle, false);

     %vehicle.setSelfPowered();

     %vehicle.team = %plyr.client.Team;
     %vehicle.owner = %plyr.client;
     MissionCleanup.add(%vehicle);
   }
   else
        messageClient(%obj.client, 'MsgCantDeployNearBuilding', '\c2You cannot deploy on, near or in a building or structure.');
}

function MegaDiscDisable(%pl, %vehicle)
{
   checkTrackerObj(%vehicle, %pl);

   if(!%vehicle.prevDest)
   {
     %pl.startFade(100, 0, false);
     %pl.setTransform(%vehicle.getTransform());
//     %pl.setEnergyLevel(%pl.getDatablock().maxEnergy);
     
//     %vehicle.setTransform("10000 10000 10000 1 0 0 0");
//     %vehicle.applyDamage(10000);
//     %vehicle.delete();
     %vehicle.getDatablock().deleteAllMounted(%vehicle);
     schedule(100, %vehicle, setPosition, %vehicle, vectorAdd(%vehicle.position, "40 -27 10000"));
     %vehicle.schedule(2000, "delete");
     serverPlay3D("JetfireTransform", %pl.getTransform());
     %pl.setImageTrigger(2,false);
//     messageClient(%pl.client, 'MsgMegaDiscOff', '\c2MegaDiscTransform() -> Bipedal');

   }
}

function MegaDiscPackImage::onActivate(%data, %obj, %slot)
{
  if((!cantMegaDisc(%obj) || (%obj.getEnergyLevel() > 50 && !cantMegaDisc(%obj))) && !%obj.jM)
  {
        %obj.jM = true;
        MegaDiscEnable(%obj);
  }
  else
  {
        messageClient(%obj.client, 'MsgCantDeployNearBuilding10', '\c2You cannot deploy on/near/in a building for at least 10m');
        %obj.setImageTrigger(%slot,false);
  }
}

function MegaDiscPackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger(%slot,false);
   %obj.jM = false;
}

function MegaDiscPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}

