//--------------------------------------------------------------------------
//
// Flame Barrel pack (RAXX turret)
//
//--------------------------------------------------------------------------
datablock LinearFlareProjectileData(PowerPlasmaBoltV)
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "3.0 3.0 3.0";
   directDamage        = 0.0;
   directDamageType    = $DamageType::Plasma;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 5.0 ;	// 3.0; -soph
   kickBackStrength    = 0;
   radiusDamageType    = $DamageType::Plasma;

   explosion           = "HeavyPlasmaExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = FireboltFireEmitter;
   delayEmitter        = FireboltSmokeEmitter;

   dryVelocity       = 250;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1200;
   lifetimeMS        = 1500;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;

   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "1 0.75 0.25";
};

function PowerPlasmaBoltV::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
//     if(%targetObject.getType() & $TypeMasks::PlayerObjectType)	// -[soph]
//          if(!%targetObject.inDEDField && %targetObject.team != %projectile.sourceObject.team)
//               %targetObject.burnObject(%projectile.sourceObject);	// -[/soph]

     Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);
}

function PowerPlasmaBoltV::onExplode(%data, %proj, %pos, %mod)	// +[soph]
{
     Parent::onExplode(%data, %proj, %pos, %mod);
     IncendiaryExplosion( %pos , %data.damageradius , 2.0 , %proj.sourceObject );
}								// +[/soph]

datablock TurretImageData(FlameBarrelLarge)
{
   shapeFile = "TR2weapon_grenade_launcher.dts";
   item      = FlameBarrelLargePack;

   projectile = PowerPlasmaBoltV;
   projectileType = LinearFlareProjectile;
   usesEnergy = true;
   fireEnergy = 10;
   minEnergy = 15;
   emap = true;
   projectileSpread = 6 / 1000;
   
   // Turret parameters
   activationMS      = 500;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 100;
   degPerSecTheta    = 300;
   degPerSecPhi      = 500;
   attackRadius      = 100;

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activate";
   stateSound[1]                 = PBLSwitchSound;
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.5;
   stateFire[3]                = true;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateEmitter[3]             = "SmallFlameEmitter";
   stateSound[3]               = MBLFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   //stateTimeoutValue[4]          = 0.0375;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};

function FlameBarrelLarge::onFire( %data , %obj , %slot )	// +[soph]
{								// +
   %p = Parent::onFire( %data , %obj , %slot ) ;		// +
   if( %p )							// +
   {  								// +
      %useEnergyObj = %obj.getObjectMount() ; 			// +
      if( !%useEnergyObj )					// +
         %useEnergyObj = %obj ;					// +
      %vehicle = %useEnergyObj .getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0 ;
      if( %vehicle )
         if( %obj.getControllingClient() && %obj.getControllingClient() == %vehicle.getMountNodeObject( 1 ).client )
         {							// +
            %p.damageMod = 2 ;					// +
            %obj.useEnergy( %data.fireEnergy ) ;		// +
         }							// +
								// +
      %vector = %obj.getMuzzleVector( %slot ) ;			// +
      for( %i = 0 ; %i < getRandom( 4 ) + 2 ; %i++ )		// +
      {								// +
         %p = new GrenadeProjectile() {				// +
            dataBlock        = "ProtronFireSpray" ;		// +
            initialDirection = vectorScale( calcSpreadVector( %vector , 250 ) , 0.5 + ( getRandom( 10 ) / 5 ) ) ;
            initialPosition  = %obj.getMuzzlePoint( %slot ) ;	// +
            sourceObject     = %obj ;				// +
            sourceSlot       = %slot ;				// +
            vehicleObject    = %vehicle ;			// +
            bkSourceObject   = %obj ;				// +
         };							// +
         %p.damageMod = 1 ;					// +
         MissionCleanup.add( %p ) ;				// +
      }								// +
   }								// +
}								// +[/soph]

datablock ShapeBaseImageData(FlameBarrelPackImage)
{
   mass = 15;

   shapeFile  = "pack_barrel_fusion.dts";
   item       = FlameBarrelPack;
   mountPoint = 1;
   offset     = "0 0 0";
	turretBarrel = "FlameBarrelLarge";

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";

	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeOut[2] = "Idle";

	isLarge = true;
};

datablock ItemData(FlameBarrelPack)
{
   className    = Pack;
   catagory     = "Packs";
   shapeFile    = "pack_barrel_fusion.dts";
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   rotate       = true;
   image        = "FlameBarrelPackImage";
	pickUpName = "an Flame(RAXX) barrel pack";
};

// Multi Images dont work well with Turrets =[
//This is the cause of the UE's -Nite-
//function FlameBarrelLarge::onMount(%this,%obj,%slot)
//{
//   commandToClient( %obj.client, 'BottomPrint', "<font:times new roman:18>Now using a RAXX Flamethrower\nThis is a deadly short-range flamethrower. Use it against a jetpack for a surprise!", 5, 2);
//   %obj.setImageAmmo(%slot,true);
//   %obj.mountImage(RAXXDecalAImage, 4);
//   %obj.mountImage(RAXXDecalBImage, 5);
//   %obj.mountImage(RAXXDecalCImage, 6);
//}
//
//function FlameBarrelLarge::onUnmount(%this,%obj,%slot)
//{
//   %obj.setImageTrigger(%slot, false);
//   %obj.setImageTrigger($BackpackSlot, false);
//   %obj.unmountImage(4);
//   %obj.unmountImage(5);
//   %obj.unmountImage(6);
//}

function FlameBarrelPackImage::onActivate(%data, %obj, %slot)
{
	checkTurretMount(%data, %obj, %slot);
}

function FlameBarrelPackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger($BackpackSlot, false);
}

function FlameBarrelPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
