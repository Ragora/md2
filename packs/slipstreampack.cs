if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------
// Slipstream IX Pack

//datablock AudioProfile(SlipstreamExitSound)
//{
//   filename    = "fx/misc/nexus_cap.wav";
//   description = AudioDefault3d;
//   preload = true;
//};

//datablock AudioProfile(SlipstreamEnterSound)
//{
//   filename    = "fx/powered/dep_inv_station.wav";
//   description = AudioDefault3d;
//   preload = true;
//};

//datablock AudioProfile(HoleFadeSound)
//{
//   filename    = "fx/vehicles/inventory_pad_appear.wav";
//   description = AudioDefault3d;
//   preload = true;
//};

datablock ShapeBaseImageData(SlipstreamPackImage)
{
   shapeFile = "pack_upgrade_sensorjammer.dts";
   item = SlipstreamPack;
   mountPoint = 1;
   offset = "0 0 0";
   mass = 20;

   gun = PortalGun;
      
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";

	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(SlipstreamPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_sensorjammer.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "SlipstreamPackImage";
	pickUpName = "a portal generator";
};

datablock LinearFlareProjectileData(PortalBall)
{
   scale = "2 2 2";
   faceViewer          = true;
   directDamage        = 0.0;     //  .15
   directDamageType    = $DamageType::PhaserCannon;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   radiusDamageType    = $DamageType::PhaserCannon;
   
   kickBackStrength    = 0.0;

   sound          	   = BlasterProjectileSound;
   explosion           = PlasmaBoltExplosion;
   
   splash              = PlasmaSplash;

   dryVelocity       = 300;    //225
   wetVelocity       = 300;   //200
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 900;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = 1;

   numFlares         = 35;
   flareColor        = "1.0 0.2 0.2";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   size[0]           = 0.5;
   size[1]           = 1.25;
   size[2]           = 2.0;

   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1.0 0.2 0.2";
};

datablock ShapeBaseImageData(PortalGun)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   item = SlipstreamPack;
//   ammo = LaserCannonCapacitor;
//   armThread = "lookMS";

   projectile = PortalBall;
   projectileType = LinearFlareProjectile;

//   projectileSpread = 4.5 / 1000.0;
   
//   offset = "0 0.2 0.05";
   emap = true;
   usesEnergy = true;
   minEnergy = 12;
   fireEnergy = 12;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = TargetingLaserSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
//   stateSound[4]            = AssaultChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
//   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.95;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "FireSpindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 0.01;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   stateEmitter[6]       = "ChaingunFireSmoke";
   stateEmitterTime[6]       = 0.25;
   stateEmitterNode[6]       = 0;

   //
   stateTimeoutValue[6]        = 3.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";

   //--------------------------------------
   stateName[8]       = "FireSpindown";
   stateSound[8]      = ChaingunSpinDownSound;
   stateSpinThread[8] = SpinDown;
   stateEmitter[8]       = "ChaingunFireSmoke";
   stateEmitterTime[8]       = 0.25;
   stateEmitterNode[8]       = 0;
   //
   stateTimeoutValue[8]            = 3.5;
   stateWaitForTimeout[8]          = false;
   stateTransitionOnTimeout[8]     = "Ready";
   stateTransitionOnTriggerDown[8] = "Spinup";
};

function PortalGun::onFire(%data,%obj,%slot)
{
     %p = Parent::onFire(%data,%obj,%slot);
     
     if(%p)
          %p.portalMode = %obj.client.mode[%obj.getMountedImage(0).item];
}

function SlipstreamPackImage::onMount(%data, %obj, %node)
{
   %obj.wormholegen = true;     
}

function SlipstreamPackImage::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}

function SlipstreamPackImage::onUnmount(%data, %obj, %node)
{
//    %obj.unmountImage(7);
   %obj.wormholegen = false;
    
   // dismount the LaserCannon gun if the player had it mounted
   // need the extra "if" statement to avoid a console error message
   if(%obj.getMountedImage($WeaponSlot))
      if(%obj.getMountedImage($WeaponSlot).getName() $= "PortalGun")
         %obj.unmountImage($WeaponSlot);
}

function SlipstreamPackImage::onActivate(%data, %obj, %slot)
{
   // don't activate the pack if player is piloting a vehicle
   if(%obj.isPilot())
   {
      %obj.setImageTrigger(%slot, false);
      return;
   }

   if(!isObject(%obj.getMountedImage($WeaponSlot)) || %obj.getMountedImage($WeaponSlot).getName() !$= "PortalGun")
   {
      // unmount any weapon the player may have been holding
      if(%obj.getMountedImage($WeaponSlot))
         %obj.unmountImage($WeaponSlot);

      // make sure player's arm thread is "look"
      %obj.setArmThread(look);

      // mount the LaserCannon gun
      %obj.mountImage(PortalGun, $WeaponSlot);
//      commandToClient(%obj.client, 'setRepairReticle');
      %obj.client.setWeaponsHudActive("ELFGun");
   }
}

function SlipstreamPackImage::onDeactivate(%data, %obj, %slot)
{
   //called when the player hits the "pack" key again (toggle)
   %obj.setImageTrigger(%slot, false);
   if(%obj.getMountedImage($WeaponSlot).getName() $= "PortalGun")
      %obj.unmountImage($WeaponSlot);
}

function PortalGun::onMount(%this,%obj,%slot)
{
   commandToClient( %obj.client, 'BottomPrint', "<font:times new roman:18>Now using a Portal Gun\nPoint and shoot to create a portal.", 5, 2);
   %obj.setImageAmmo(%slot,true);
}

function PortalGun::onUnmount(%this,%obj,%slot)
{
   %obj.setImageTrigger(%slot, false);
   %obj.setImageTrigger($BackpackSlot, false);
}

function disableIgnoreDetection(%obj)
{
   %obj.ignoreDetection = false;
//     %obj.lastWormholedObject = 0;
}

function deleteWormhole(%hole)
{
   if(isObject(%hole))
   {
      %end = %hole.end;
      %end.delete();
      %hole.delete();
   }
}

function PortalBall::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
     %player = %projectile.sourceObject;

     if(!isObject(%player))
          return;

     if(%player.client.outOfBounds)
          return;

     %type = %targetObject.getType();
     %projectile.ignoreDetection = true;
     
     if(%type & $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType)
     {
          if(%type & $TypeMasks::PlayerObjectType)
               return;
               
          %entryPoint = isObject(%player.portalEntry) ? %player.portalEntry : 0;
          %exitPoint = isObject(%player.portalExit) ? %player.portalExit : 0;
          
          // Select which portal to replace
          %portal = %projectile.portalMode;
          
//          if(%entryPoint && %exitPoint)
//               %portal = %player.lastPortalSpawn ? 0 : 1;
//          else 
//               %portal = %entryPoint ? 0 : 1;
               
          %player.lastPortalSpawn = %portal;
          
          if(!%portal)
          {
               if(isObject(%player.portalEntry))
                    %wormhole = %player.portalEntry;
               else
               {
                    %wormhole = new StaticShape()
                    {
                         dataBlock        = mReflector;
                    };
                    MissionCleanup.add(%wormhole);
               }

               // todo: rotate the portal based on the surface normal
//               %wormhole.setRotation(%someMathInvolving_normal);               
               // Push the portal slightly off the surface
               %nPos = vectorAdd(%position, vectorScale(%normal, 1.5));
               %wormhole.setPosition(%nPos);

               // Begin the portal detection thread
               %wormhole.exitVector = %normal;
               %wormhole.parent = %player;
               %wormhole.pos = %nPos;
               
               %player.portalEntry = %wormhole;

               if(isObject(%player.portalExit))
                    linkPortals(%wormhole, %player.portalExit);
               else
                    %wormhole.end = 0;
                                   
               if(!%wormhole.detectThread)         
               {
                    %wormhole.detectThread = true;
                    wormholeDetect(%wormhole);    
               }      
          }
          else
          {
               if(isObject(%player.portalExit))
                    %wormhole = %player.portalExit;
               else
               {
                    %wormhole = new StaticShape()
                    {
                         dataBlock        = mReflector;
                    };
                    MissionCleanup.add(%wormhole);
               }

               // todo: rotate the portal based on the surface normal
//               %wormhole.setRotation(%someMathInvolving_normal);               
               // Push the portal slightly off the surface
               %nPos = vectorAdd(%position, vectorScale(%normal, 1.5));
               %wormhole.setPosition(%nPos);

               // Begin the portal detection thread
               %wormhole.exitVector = %normal;
               %wormhole.parent = %player;
               %wormhole.pos = %nPos;
               
               %player.portalExit = %wormhole;

               if(isObject(%player.portalEntry))
                    linkPortals(%wormhole, %player.portalEntry);
               else
                    %wormhole.end = 0;
               
               if(!%wormhole.detectThread)         
               {
                    %wormhole.detectThread = true;
                    wormholeDetect(%wormhole);    
               }      
          }
     }
}

function linkPortals(%p1, %p2)
{
     %p1.end = %p2;
     %p2.end = %p1;
}

function wormholeDetect(%obj)
{
   if(isObject(%obj))
   {
      if(!isObject(%obj.parent))
      {
          %obj.delete();
          return;
      }
      else if(!%obj.parent.wormholegen)
      {
          %obj.delete();
          return;      
      }
          
      %pos = %obj.pos;
      createLifeEmitter(%pos, "Teleporter1Emitter", $Host::WormholeTickRate);

      InitContainerRadiusSearch(%pos, 6, $TypeMasks::PlayerObjectType | $TypeMasks::CorpseObjectType | $TypeMasks::ProjectileObjectType); // | $TypeMasks::ItemObjectType);

       while((%int = ContainerSearchNext()) != 0)
       {
          if( !isObject( %int ) || %int.exploded )	// if(!isObject(%int)) -soph
              continue;
            
          if(%int.ignoreDetection)
              continue;

          if(%int.lastWormholedObject == %obj)
              continue;

          // Prevent stack overflows
          if(%obj.trafficCount > 32)
              continue;

          // Spit them back out if no connecting portal
          %endPos = %obj.end ? %obj.end.pos : %obj.pos;
          %exitVec = %obj.end ? %obj.end.exitVector : %obj.exitVector;
          %endObj = %obj.end ? %obj.end : %obj;
          
          if(%int.getType() & $TypeMasks::PlayerObjectType)
          {
               if(%int.isMech)
                    continue;
                    
               %int.setVelocity(vectorScale(%exitVec, vectorLen(%int.getVelocity())));
          if( %int.team == %obj.team )
                    ejectFlag(%int);
               schedule(1500, %int, disableIgnoreDetection, %int);
//               %int.lastWormholedObject = %obj;
               %int.ignoreDetection = true;
               %obj.trafficCount++;
          }
          else if(%int.sourceObject || %int.bkSourceObject)
          {
               instanceSSCopyObject(%int, %endPos, %endObj, %exitVec);
               %obj.trafficCount++;               
               continue;
          }
        
          %int.setPosition(%endPos);
      }

      %obj.trafficCount = 0;
      schedule($Host::WormholeTickRate, %obj, wormholeDetect, %obj);
   }
}

function instanceSSCopyObject(%int, %end, %obj, %exitVec)
{
   if(!isObject(%int))
      return;

   if(isReflectableProjectile(%int))
   {
          if(!isObject(%obj.reflectorObj))
               %obj.reflectorObj = createReflectorForObject(%obj);

          if(%int.getClassName() $= "BombProjectile")
               %exitVec = vectorScale(%exitVec, getRandom(150, 225));
               
          %FFRObject = %obj.reflectorObj;
     
         %FFRObject.setPosition(%end);

         %sourceObject = isObject(%int.sourceObject) ? %int.sourceObject : %int.bkSourceObject;

         if( %int.originTime )						// +[soph]
         {								// +
            %airTimeMod = getSimTime() - %int.originTime ; 		// +
            %data = %int.getDatablock().getName() ;			// +
            if( %airTimeMod < ( 1000 * ( %data.maxVelocity - %data.muzzleVelocity ) / %data.acceleration ) )
               %airTimeMultiplier = ( %airTimeMod / ( 1000 * %data.muzzleVelocity / %data.acceleration ) ) + 1 ;
            else							// +
               %airTimeMultiplier = %data.maxVelocity / %data.muzzleVelocity ;
            %newVec = vectorScale( %newVec , %airTimeMultiplier ) ;	// +
         }								// +[/soph]

         %p = new(%int.getClassName())()
         {
            dataBlock        = %int.getDatablock().getName();
            initialDirection = %exitVec; //%int.initialDirection; //vectorNeg(%int.initialDirection);
            initialPosition  = %end;
            sourceObject     = %FFRObject;
            sourceSlot       = %int.sourceSlot;
            vehicleObject    = %int.vehicleObject;
            lastWormholedObject = %obj;
            starburstMode = %int.starburstMode;
            bkSourceObject = %sourceObject;
            damageMod = %int.damageMod;
            vehicleMod = %int.vehicleMod;
            projPortalCount = %int.projPortalCount + 1;
         };
         MissionCleanup.add(%p);
         %p.sourceObject = %int.sourceObject;
         
         if(%int.trailThread && %p.projPortalCount < 4)
            projectileTrail(%p, %int.trailThreadTime, %int.trailThreadProjType, %int.trailThreadProj, false, %int.trailThreadOffset);

         if(%int.proxyThread)
            rBeginProxy(%p);

         if( %src.lastMortar == %int)					// if(%src.lastMortar !$= "") -soph
            %src.lastMortar = %p;

         %p.originTime = %int.originTime ;				// +soph

         if(%int.blackholeThread)
            blackHoleLoop(%p);
            
         if(%int.lanceThread)
         {        
               SBLanceRandomThread(%p);
               %p.lanceThread = true;
         }
           
      %int.delete();
   }
}

