// ------------------------------------------------------------------

datablock ShapeBaseImageData(CometCannonPackImage)
{
   shapeFile = "pack_upgrade_cloaking.dts";
   item = CometCannonPack;
   mountPoint = 1;
   offset = "0 0 0";

   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(CometCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_cloaking.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "CometCannonPackImage";
	pickUpName = "a comet cannon";
};

datablock ShapeBaseImageData(CometCannonImage)
{
   className = WeaponImage;
   item = CometCannonPack;
   shapeFile = "turret_elf_large.dts";
   offset = "-0.5 -0.1 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateSequence[1]                 = "Deploy";   
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.25;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
//   stateDirection[3]                = true;
//   stateSound[3]                    = CometCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.05;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(CometCannonDImage) : CometCannonImage
{
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   
   shapeFile = "turret_elf_large.dts"; //turret_mortar_large.dts";
   offset = "-0.5 0.3 0.3";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   
//   stateSequence[2]                 = "Deploy";
//   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(CometCannonHHImage) : CometCannonImage
{
   item = "";	// +soph
   usesEnergy = true;
   fireEnergy = 45;	// fireEnergy = 34;	-soph
   minEnergy = 45;	// minEnergy = 34;	-soph

//   shapeFile = "turret_elf_large.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
   emap = true;
   mountPoint = 0;

   projectile = CometCannonBlast;
   projectileType = LinearFlareProjectile;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;
//   stateSound[3]                    = CometCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 2.5;	// = 0.05; -soph
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
};

function CometCannonHHImage::onMount(%this,%obj,%slot)
{
     Parent::onMount(%this,%obj,%slot);
     
   %obj.unmountImage(7);
   %obj.weaponPackDisabled = true;	// %obj.fireTimeoutCC = true; -soph
   %obj.play3d("PBLSwitchSound");
}

function CometCannonHHImage::throwWeapon( %this ) {}		// +soph

function CometCannonHHImage::onUnmount(%this,%obj,%slot)
{
     Parent::onUnmount(%this,%obj,%slot);
     
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(CometCannonDImage, 7);
   else
      %obj.mountImage(CometCannonImage, 7);

   %obj.weaponPackDisabled = false;	// %obj.fireTimeoutCC = false; -soph
}

function CometCannonHHImage::onFire(%data, %obj, %slot)
{
     %p = Parent::onFire(%data, %obj, %slot);
     
     if(%p)
     {
          spawnProjectile(%p, LinearFlareProjectile, CCDisplayCharge);
          projectileTrail(%p, 150, LinearFlareProjectile, GaussTrailCharge, true);
          %obj.play3d(StarHammerFireSound);
          %obj.play3D(AAFireSound);
          %obj.schedule(1000, play3D, "PBWSwitchSound");
          %obj.packWeaponTimeout = %time + 2750;	// 1500 +soph
     }
}

function CometCannonPackImage::onMount(%this,%obj,%slot)
{
     %obj.BAPackID = 6;
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(CometCannonDImage, 7);
   else
      %obj.mountImage(CometCannonImage, 7);
   if( !%obj.packWeaponTimeout )			// [soph]
      %obj.packWeaponTimeout = getSimTime();
   else
      %obj.weaponPackDisabled = false;			// [/soph]
}

function CometCannonPackImage::onUnmount(%this,%obj,%slot)
{
     %obj.BAPackID = 0;
   %obj.unmountImage(7);
}

function CCFireTimeoutClear(%obj)	// no longer used by anything
{
   if( !%obj.fireTimeoutCC )	// if(%obj.fireTimeoutCC == 1) -soph
     return;
     
//   %obj.fireTimeoutCC = 0;	// -soph
   %obj.play3d(PBWSwitchSound);
}

function CometCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;				// _here_ +soph
   %time = getSimTime();						// +soph
   if( %obj.weaponPackDisabled || %obj.packWeaponTimeout > %time )	// if(%obj.fireTimeoutCC) -soph
      return;

   if(%obj.invPackBlockTime > %time )					// if(%obj.invPackBlockTime > getSimTime()) -soph
      return;
      
   %enUse = 60;								// %enUse = 45; -soph
   
   if(%obj.powerRecirculator)
      %enUse *= 0.75;

   if(%obj.getEnergyLevel() >= %enUse)
   {
      %obj.setImageTrigger(7, true);   
      
      %p = new LinearFlareProjectile()
      {
         dataBlock        = CometCannonBlast;
         initialDirection = %obj.getMuzzleVector(7);
         initialPosition  = %obj.getMuzzlePoint(7);
         sourceObject     = %obj;
         sourceSlot       = 7;
      };
      MissionCleanup.add(%p);

      %obj.useEnergy(%enUse);

      %useEnergyObj = %obj.getObjectMount();

      if(!%useEnergyObj)
           %useEnergyObj = %obj;

      %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

      // Vehicle Damage Modifier
      if(%vehicle)
           %p.vehicleMod = %vehicle.damageMod;

      if(%obj.damageMod)
           %p.damageMod = %obj.damageMod;
     
      %obj.setImageTrigger(7, true);
      spawnProjectile(%p, LinearFlareProjectile, CCDisplayCharge);
      projectileTrail(%p, 150, LinearFlareProjectile, GaussTrailCharge, true);
      %obj.play3d(StarHammerFireSound);
      %obj.packWeaponTimeout = %time + 3750;				// %obj.fireTimeoutCC = 1500; -soph
//      schedule(%obj.fireTimeoutCC, 0, "CCFireTimeoutClear", %obj);	// -soph
//      projectileTrail(%p, 50, LinearFlareProjectile, MBDisplayCharge);
      %obj.schedule( 4700 , play3d , "PBWSwitchSound" );		// +soph
      
      %obj.play3D(AAFireSound);
//      %obj.applyKick(-2000);
   }

   %obj.setImageTrigger(%slot, false);
   %obj.setImageTrigger(7, false);
}

function CometCannonPackImage::onDeactivate(%data, %obj, %slot)
{
}

function CometCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
