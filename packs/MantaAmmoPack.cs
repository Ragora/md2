if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(MANTACapacitorPackImage)
{
   shapeFile = "bomb.dts";
   item = MANTACapacitorPack;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "1 0 0 90";

   usesEnergy = true;
   minEnergy = 1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(MANTACapacitorPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "bomb.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "MANTACapacitorPackImage";
	pickUpName = "a manta ion cannon battery";
};

function MANTACapacitorPackImage::onMount(%data, %obj, %node)
{
}

function MANTACapacitorPackImage::onUnmount(%data, %obj, %node)
{
	%obj.setImageTrigger(%node, false);
}

function checkMantaCanLoad(%pl)
{
   InitContainerRadiusSearch(%pl.getPosition(), 10, $TypeMasks::StaticShapeObjectType | $TypeMasks::VehicleObjectType);

   while((%int = ContainerSearchNext()) != 0)
   {
      if(%int.team == %pl.client.team && %int.isManta)
         return %int;
      else if(%int.getDatablock().isNightshade && %int.team == %pl.client.team)
         return isObject($MantaSat[%pl.client.team]) ? $MantaSat[%pl.client.team] : 0;
   }
   
   return false;
}

function MANTACapacitorPackImage::onActivate(%data, %obj, %slot)
{
   %manta = checkMantaCanLoad(%obj);

   if(%manta)
   {
//      if(%manta == 1)
//      {
//         messageClient(%obj.client, 'MsgMantaBatteryUseless', '\c2The MANTA is already loaded.');
//         %obj.setImageTrigger(%slot, false);
//         return;
//      }
//      else
//      {
         messageClient(%obj.client, 'MsgMantaBatteryLoaded', '\c2Reloaded MANTA with Ion Blast.');
         %obj.unmountImage(%slot);
         %obj.decInventory(%data.item, 1);
         %manta.isLoaded = true;
         %manta.loadType = 1;
         %manta.cooldownTime = 60000;
         %manta.play3D(MBLSwitchSound);
         return;
//      }
   }
   else
   {
      messageClient(%obj.client, 'MsgMantaBatteryRange', '\c2There is no MANTA in range.');
      %obj.setImageTrigger(%slot, false);
      return;
   }
}

function MANTACapacitorPackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger(%slot,false);
}

function MANTACapacitorPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
