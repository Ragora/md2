// ------------------------------------------------------------------

datablock ShapeBaseImageData(AutoCCannonPackImage)
{
   shapeFile = "ammo_chaingun.dts";
   item = AutoCCannonPack;
   ammo = ACPackAmmo;
   
   mountPoint = 1;
   offset = "0 0 0.25";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
     stateSequence[0] = "fire";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(AutoCCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "AutoCCannonPackImage";
	pickUpName = "a heavy autocannon";
};

datablock ItemData(ACPackAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "extra autocannon burst shells";
   cantPickup = false;	// cantPickup = true; -soph
};

datablock ShapeBaseImageData(AutoCCannonImage)
{
   className = WeaponImage;
   item = Blaster;
   shapeFile = "weapon_energy.dts";
   offset = "-0.4 -0.2 0.2";
   rotation = "0 1 0 45";
   emap = true;
   mountPoint = 1;

   ammo = ACPackAmmo;

   usesEnergy = false;
   fireEnergy = 20;
   minEnergy = 20;
   
//   projectile = BomberFusionBolt;
//   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.25;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = AutoCCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(AutoCCannonDImage) : AutoCCannonImage
{
   shapeFile = "weapon_energy.dts"; //turret_mortar_large.dts";
   offset = "-0.4 -0.2 0.2";
   rotation = "0 1 0 45";
   emap = true;
   mountPoint = 1;

//   stateSequence[2]                 = "Deploy";
//   stateScript[3] = "onFire";
};

function AutoCCannonPackImage::onMount(%this,%obj,%slot)
{
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(AutoCCannonDImage, 7);
   else
      %obj.mountImage(AutoCCannonImage, 7);
      
   schedule(32, %obj, ACUpdateAmmoCount, %obj);
}

function ACUpdateAmmoCount(%obj)
{
     %obj.client.updateSensorPackText(%obj.getInventory("ACPackAmmo"));
}

function AutoCCannonPackImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(7);
}

function AutoCCFireTimeoutClear(%obj)
{
   %obj.fireTimeoutACP = 0;
}

function AutoCCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;	// _here_ +soph
   if(%obj.fireTimeoutACP)
      return;

  // %obj.setImageTrigger(0, false);

   if(%obj.getInventory("ACPackAmmo") > 4)
   {
      staggerFireACP(%data, %obj, 7, 0);

      %obj.setImageTrigger(7, true);
      %obj.fireTimeoutACP = 1500;
      %obj.schedule(750, play3d, PlasmaFireWetSound);
      schedule(%obj.fireTimeoutACP, 0, "AutoCCFireTimeoutClear", %obj);
      %obj.decInventory("ACPackAmmo", 5);
      %obj.client.updateSensorPackText(%obj.getInventory("ACPackAmmo"));
      %obj.play3D(MBLFireSound);         
   }

   %obj.setImageTrigger(7, false);
   %obj.setImageTrigger(%slot,false);
}

function staggerFireACP(%data, %obj, %slot, %sequence)
{
     while(%sequence < 5)
     {
           %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 5);
           
           %p = new TracerProjectile()
           {
              dataBlock        = AutoCannonBullet;
              initialDirection = %vector;
              initialPosition  = %obj.getMuzzlePoint(%slot);
              sourceObject     = %obj;
              sourceSlot       = %slot;
           };
           MissionCleanup.add(%p);
           %p.lastReflectedFrom = %obj;
           
             %useEnergyObj = %obj.getObjectMount();

             if(!%useEnergyObj)
                  %useEnergyObj = %obj;

             %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

             // Vehicle Damage Modifier
          if(%vehicle)
              %p.vehicleMod = %vehicle.damageMod;

          if(%obj.damageMod)
              %p.damageMod = %obj.damageMod;

          %sequence++;
     }
}

function AutoCCannonPackImage::onDeactivate(%data, %obj, %slot)
{

}

function AutoCCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
