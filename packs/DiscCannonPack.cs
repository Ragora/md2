if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(DiscCannonPackImage)
{
   shapeFile = "ammo_disc.dts";
   item = DiscCannonPack;
   ammo = DiscPackAmmo;
   mountPoint = 1;
   offset = "0 -0.2 0";
   rotation = calcThisRotD("90 180 0");
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(DiscPackAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "extra disc pack shells";
   cantPickup = true;
};

datablock ItemData(DiscCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "DiscCannonPackImage";
	pickUpName = "a disc cannon";
};

datablock ShapeBaseImageData(DiscCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   offset = "-0.35 -0.2 0.1";
   rotation = "0 1 0 90";
   item = DiscCannonPack;
   ammo = DiscPackAmmo;
   emap = true;
   mountPoint = 1;

   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "discSpin";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(DiscCannonDImage)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   offset = "-0.35 0 0.2";
   rotation = "0 1 0 45";
   item = DiscCannonPack;
   emap = true;
   mountPoint = 1;

   ammo = DiscAmmo;
   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "discSpin";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function DiscCannonPackImage::onMount(%this,%obj,%slot)
{
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(DiscCannonDImage, 7);
   else
      %obj.mountImage(DiscCannonImage, 7);
      
   schedule(32, %obj, DCUpdateAmmoCount, %obj);
}

function DCUpdateAmmoCount(%obj)
{
     %obj.client.updateSensorPackText(%obj.getInventory("DiscPackAmmo"));
}

function DiscCannonPackImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(7);
}

function DiscCFireTimeoutClear(%obj)
{
   %obj.fireTimeoutDiscC = 0;
   %obj.play3D(DiscReloadSound);
}

function DiscCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;	// _here_ +soph
   if(%obj.fireTimeoutDiscC)
      return;

   if(%obj.getInventory("DiscPackAmmo"))
   {
      %p = new LinearProjectile()
      {
         dataBlock        = DiscProjectile;
         initialDirection = %obj.getMuzzleVector(7);
         initialPosition  = %obj.getMuzzlePoint(7);
         sourceObject     = %obj;
         sourceSlot       = 7;
      };
      MissionCleanup.add(%p);

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

      %obj.decInventory("DiscPackAmmo", 1);
      %obj.client.updateSensorPackText(%obj.getInventory("DiscPackAmmo"));
      
      %obj.setImageTrigger(7, true);
      %obj.play3D(DiscFireSound);
      %obj.fireTimeoutDiscC = 1400; //-Nite-  increased fire rate  was 1000 //MrKeen - changed it back
      schedule(%obj.fireTimeoutDiscC, 0, "DiscCFireTimeoutClear", %obj);
      %obj.setImageTrigger(7, false);      
   }
   else
   {
          %obj.play3D(DiscDryFireSound);
          %obj.setImageTrigger(%slot, false);          
          return;
   }
   
   %obj.setImageTrigger(7, false);
   %obj.setImageTrigger(%slot, false);
}

function DiscCannonPackImage::onDeactivate(%data, %obj, %slot)
{
   %obj.setImageTrigger(7, false);
	%obj.setImageTrigger(%slot,false);
}

function DiscCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
