if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();
//Patch File
// ------------------------------------------------------------------
// Synomium Pack

datablock AudioProfile(SynomiumIdle)
{
	filename = "fx/misc/nexus_idle.wav";
	description = ClosestLooping3d;
    preload = true;
};

datablock AudioProfile(SynomiumEngage)
{
	filename = "fx/weapons/chaingun_start.wav";
	description = AudioDefault3D;
    preload = true;
};

datablock AudioProfile(SynomiumOff)
{
	filename = "fx/weapons/chaingun_off.wav";
	description = AudioDefault3D;
    preload = true;
};

datablock ShapeBaseImageData(SynDecalImage)
{
   mountPoint = 1;
   shapeFile = "pack_upgrade_satchel.dts";
   offset = "0 -0.15 0.025";
};

datablock ShapeBaseImageData(SynomiumPackImage)
{
   shapeFile = "pack_upgrade_ammo.dts";
   item = SynomiumPack;
   mountPoint = 1;
   offset = "0 0 0";

   rechargeRateBoost = 0.15;

   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateSequence[1] = "fire";
	stateSound[1] = SynomiumIdle;
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(SynomiumPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_ammo.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "SynomiumPackImage";
	pickUpName = "a synomium device";

};

function synPhase2(%obj)
{
     if(%obj.startSyn)
     {
          %obj.synCount++;
          %obj.isSyn = true;
//
//           %obj.setRechargeRate(%obj.getRechargeRate() + %data.rechargeRateBoost); //-Nite-  0.2635  0.271 is ePack old 0.486 is epack new O_o

          %obj.hasEnergyPack = false;
          %obj.enPack = true;
          %obj.stasisProof = true;

          if(%obj.synCount >= 4)
          {
                  %obj.zapObject();

                  if(getRandom(10) >= 8)
                     %obj.zap2Object();
             
               %obj.synCount = 0;
          }
          
          schedule(100, 0, "synPhase2", %obj); // 32   //100
     }
     else
     {
          %obj.stasisProof = false;
          %obj.hasEnergyPack = false;
          %obj.enPack = false;

//         if(!%obj.getDataBlock().isBlastech || %obj.overdriveMode)
//             %obj.setRechargeRate(0.256);
     }
}

function synPhase2v(%obj)
{
     if(%obj.startSyn)
     {
          %obj.synCount++;
          %obj.isSyn = true;
          %obj.synMod = 0.2;
          %obj.stasisProof = true;

          if(%obj.synCount >= 4)
          {
                  zapVehicle(%obj, FXZap);

                  if(getRandom(10) >= 8)
                      zapVehicle(%obj, FXPulse);

               %obj.synCount = 0;
          }

          schedule(100, 0, "synPhase2v", %obj);
     }
     else
     {
          %obj.stasisProof = false;
          %obj.synMod = 0.0;
     }
}


function synomise(%obj)
{
     if(%obj.startSyn)
        schedule(1000, 0, "synPhase2", %obj);
}

function SynomiumPackImage::onMount(%data, %obj, %node)
{
     %obj.BAPackID = 13;
   %obj.isSyn = false;
   %obj.startSyn = false;
   %obj.mountImage(SynDecalImage, 1);
   %obj.schedule( 32 , setImageTrigger , $BackpackSlot , true );	// +soph
}

function SynomiumPackImage::onUnmount(%data, %obj, %node)
{
     %obj.BAPackID = 0;
	%obj.setImageTrigger(%node, false);
    %obj.isSyn = false;
    %obj.startSyn = false;
    %obj.unmountImage(1);
    %obj.synMod = 0.0;
}

function SynomiumPackImage::onActivate(%data, %obj, %slot)
{
   messageClient(%obj.client, 'MsgSynT', '\c2Synomium Device Engaged.');
   %obj.play3D(SynomiumEngage);
   %obj.isSyn = false;
   %obj.startSyn = true;
   %obj.synSlot = %slot;
   %obj.playShieldEffect("0 0 1");

   synomise(%obj);

	if( %obj.getArmorType() & $ArmorMask::Blastech || %obj.isMageIon )				// +[soph]
		%obj.setRechargeRate( %obj.getRechargeRate() + ( %data.rechargeRateBoost * 2 / 3 ) ) ;	// +
	else												// +
		%obj.setRechargeRate( %obj.getRechargeRate() + %data.rechargeRateBoost ) ;		// +[/soph]
}

function SynomiumPackImage::onDeactivate(%data, %obj, %slot)
{
     messageClient(%obj.client, 'MsgSynF', '\c2Synomium Device Deactivated.');
	  %obj.setImageTrigger(%slot,false);
     %obj.play3D(SynomiumOff);
     %obj.isSyn = false;
     %obj.startSyn = false;
     %obj.playShieldEffect("0 0 1");
     %obj.synMod = 0.0;
     
	if( %obj.getArmorType() & $ArmorMask::Blastech || %obj.isMageIon )				// +[soph]
		%obj.setRechargeRate( %obj.getRechargeRate() - ( %data.rechargeRateBoost * 2 / 3 ) ) ;	// +
	else												// +
		%obj.setRechargeRate( %obj.getRechargeRate() - %data.rechargeRateBoost ) ;		// +[/soph]
}

function disableSyn(%obj)
{
   if(%obj.isSyn)
   {
     messageClient(%obj.client, 'MsgSynF', '\c2Synomium Device Deactivated.');
	  %obj.setImageTrigger($BackpackSlot,false);
     %obj.play3D(SynomiumOff);
     %obj.isSyn = false;
     %obj.startSyn = false;
     %obj.playShieldEffect("0 0 1");

		if( %obj.getArmorType() & $ArmorMask::Blastech || %obj.isMageIon )			// +[soph]
			%obj.setRechargeRate( %obj.getRechargeRate() - ( %data.rechargeRateBoost * 2 / 3 ) ) ;
		else											// +
			%obj.setRechargeRate( %obj.getRechargeRate() - %data.rechargeRateBoost ) ;	// +[/soph]
   }
}

function SynomiumPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}

