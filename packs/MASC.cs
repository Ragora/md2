// ------------------------------------------------------------------
// Battle Angel Overdrive datablocks

$MASCLandMod = 3.5;
$MASCWaterMod = 3.75;

datablock PlayerData(BattleAngelMaleHumanMASC) : BattleAngelMaleHumanArmor
{
    maxForwardSpeed = 7.5*Meltdown.RunSpeedMod*$MASCLandMod;
   maxBackwardSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;
   maxSideSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterBackwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterSideSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
};

datablock PlayerData(BattleAngelFemaleHumanMASC) : BattleAngelFemaleHumanArmor
{
   maxForwardSpeed = 7.5*Meltdown.RunSpeedMod*$MASCLandMod;
   maxBackwardSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;
   maxSideSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterBackwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterSideSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
};

datablock PlayerData(BattleAngelMaleBiodermMASC) : BattleAngelMaleBiodermArmor
{
   maxForwardSpeed = 7.5*Meltdown.RunSpeedMod*$MASCLandMod;
   maxBackwardSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;
   maxSideSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterBackwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterSideSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
};

//----------------------------------------------------------------------
// Jugg Stimpack blocks

datablock PlayerData(HeavyMaleHumanMASC) : HeavyMaleHumanArmor
{
   jetForce = 22.5 * 225;
   horizResistFactor = 0.2;
   maxJetForwardSpeed = 40*Meltdown.JetSpeedMod;
   
   maxForwardSpeed = 7.5*Meltdown.RunSpeedMod*$MASCLandMod;
   maxBackwardSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;
   maxSideSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterBackwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterSideSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;

   energyPerDamagePoint = 150.0;	// +soph
};

datablock PlayerData(HeavyFemaleHumanMASC) : HeavyFemaleHumanArmor
{
   jetForce = 22.5 * 225;
   horizResistFactor = 0.2;
   maxJetForwardSpeed = 40*Meltdown.JetSpeedMod;

   maxForwardSpeed = 7.5*Meltdown.RunSpeedMod*$MASCLandMod;
   maxBackwardSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;
   maxSideSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterBackwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterSideSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;

   energyPerDamagePoint = 150.0;	// +soph
};

datablock PlayerData(HeavyMaleBiodermMASC) : HeavyMaleBiodermArmor
{
   jetForce = 22.5 * 225;
   horizResistFactor = 0.2;
   maxJetForwardSpeed = 40*Meltdown.JetSpeedMod;

   maxForwardSpeed = 7.5*Meltdown.RunSpeedMod*$MASCLandMod;
   maxBackwardSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;
   maxSideSpeed = 7*Meltdown.RunSpeedMod*$MASCLandMod;

   maxUnderwaterForwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterBackwardSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;
   maxUnderwaterSideSpeed = 5*Meltdown.WaterRunSpeedMod*$MASCLandMod;

   energyPerDamagePoint = 150.0;	// +soph
};
