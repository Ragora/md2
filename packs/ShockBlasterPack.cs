// ------------------------------------------------------------------

datablock LinearFlareProjectileData(ShockBlasterBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0.0;
   directDamageType    = $DamageType::ShockCannon;
   hasDamageRadius     = true;
   indirectDamage      = 0.53;
   damageRadius        = 5.0;
   kickBackStrength    = 2500;
   radiusDamageType    = $DamageType::ShockCannon;
   explosion           = EnergyRifleExplosion;
   splash              = PlasmaSplash;

   dryVelocity       = 88.0;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 6000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "2.5 2.5 2.5";
   numFlares         = 20;
   flareColor        = "0.6 0.1 0.8";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ItemData(PCCAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "beacon.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a heavy elerium battery";

   emap = true;
};

datablock ShapeBaseImageData(ShockBlasterPackImage)
{
   shapeFile = "pack_barrel_elf.dts";
   item = ShockBlasterPack;
   ammo = PCCAmmo;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(ShockBlasterPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_barrel_elf.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "ShockBlasterPackImage";
	pickUpName = "a phaser blast cannon";
};

datablock ShapeBaseImageData(ShockBlasterImage)
{
   className = WeaponImage;
   item = ShockBlasterPack;
   shapeFile = "turret_fusion_large.dts";
   offset = "-0.5 -0.1 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   ammo = PCCAmmo;
   
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   
//   projectile = BomberFusionBolt;
//   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateSequence[1]                 = "Deploy";   
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.8;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
//   stateDirection[3]                = true;
//   stateSound[3]                    = ShockBlasterFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.2;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(ShockBlasterDImage) : ShockBlasterImage
{
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   
   shapeFile = "turret_fusion_large.dts"; //turret_mortar_large.dts";
   offset = "-0.5 0.3 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   
//   stateSequence[2]                 = "Deploy";
//   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(ShockBlasterHHImage) : ShockBlasterImage
{
   item = "";	// +soph
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   ammo = PCCAmmo;
   
//   shapeFile = "turret_elf_large.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
   emap = true;
   mountPoint = 0;

   projectile = PBCCannonBolt;
   projectileType = LinearFlareProjectile;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.8;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;
//   stateSound[3]                    = CometCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.2;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
};

function ShockBlasterHHImage::onMount(%this,%obj,%slot)
{
     Parent::onMount(%this,%obj,%slot);

   %obj.unmountImage(7);
   %obj.weaponPackDisabled = true;	// %obj.fireTimeoutSBL = true; -soph
   %obj.play3D("PBLSwitchSound");
}

function ShockBlasterHHImage::throwWeapon( %this ) {}		// +soph

function ShockBlasterHHImage::onUnmount(%this,%obj,%slot)
{
     Parent::onUnmount(%this,%obj,%slot);

   if(%obj.client.race $= Bioderm)
      %obj.mountImage(ShockBlasterDImage, 7);
   else
      %obj.mountImage(ShockBlasterImage, 7);

   %obj.weaponPackDisabled = false;	// %obj.fireTimeoutSBL = false; -soph
}

function ShockBlasterHHImage::onFire(%data, %obj, %slot)
{
   if( %obj.inv[ PCCAmmo ] > 14 )			// if(%obj.getInventory(PCCAmmo) > 0) -soph
   {
         %p = Parent::onFire(%data, %obj, %slot);

         if(%p)
         {
              %p.triggerConcussion = true;
              %obj.decInventory( "PCCAmmo" , 13 ) ;	// %obj.decInventory("PCCAmmo", 1); -soph
              spawnProjectile(%p, LinearFlareProjectile, PCDisplayCharge);
              %obj.play3d(PCRFireSound);
              %obj.schedule(1800, play3D, "ShockLanceReloadSound");
              %obj.client.updateSensorPackText(%obj.getInventory("PCCAmmo"));
              %obj.packWeaponTimeout = %time + 3000;	// 2000 +soph
         }
    }

    if(!%obj.chargingPCC)
         ChargePhaserC(%obj, %data);
}

function ShockBlasterPackImage::onMount(%this,%obj,%slot)
{
     %obj.BAPackID = 7;
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(ShockBlasterDImage, 7);
   else
      %obj.mountImage(ShockBlasterImage, 7);
      
   schedule(100, %obj, updatePCCAmmo, %obj);
   if( !%obj.packWeaponTimeout )			// +[soph]
      %obj.packWeaponTimeout = getSimTime();		// +
   else							// +
      %obj.weaponPackDisabled = false;			// +[/soph]
}

function updatePCCAmmo(%obj)
{
   %obj.client.updateSensorPackText(%obj.getInventory("PCCAmmo"));
}

function ShockBlasterPackImage::onUnmount(%this,%obj,%slot)
{
     %obj.BAPackID = 0;
   %obj.unmountImage(7);
   %obj.chargingPCC = false;
}

function SBLFireTimeoutClear(%obj)	// no longer used -soph
{
   if( !%obj.fireTimeoutSBL )	// if(%obj.fireTimeoutSBL == 1) -soph
     return;
     
//   %obj.fireTimeoutSBL = 0;	// -soph
   %obj.play3d(ShockLanceReloadSound);
}

function ShockBlasterPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;				// _here_ +soph
   %time = getSimTime();						// +soph
   if( %obj.weaponPackDisabled || %obj.packWeaponTimeout > %time )	// if(%obj.fireTimeoutSBL) -soph
      return;

   if(%obj.invPackBlockTime > %time )					// if(%obj.invPackBlockTime > getSimTime()) -soph
      return;
      
   %enUse = 0;

   if( %obj.getInventory( PCCAmmo ) >= 15 )				// if(%obj.getInventory(PCCAmmo) > 0) 
   {
      %obj.setImageTrigger(7, true);   
      
      %p = new LinearFlareProjectile()
      {
         dataBlock        = PBCannonBolt;
         initialDirection = %obj.getMuzzleVector(7);
         initialPosition  = %obj.getMuzzlePoint(7);
         sourceObject     = %obj;
         sourceSlot       = 7;
      };
      MissionCleanup.add(%p);

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
      %obj.setImageTrigger(7, true);
      %obj.packWeaponTimeout = %time + 3000;				// %obj.fireTimeoutSBL = 2000; -soph
//      schedule(%obj.fireTimeoutSBL, 0, "SBLFireTimeoutClear", %obj);	// -soph
      %obj.schedule( 2500 , play3D , "PBLSwitchSound" );		// +soph

      %obj.decInventory( "PCCAmmo" , 15 ) ;				// %obj.decInventory("PCCAmmo", 1); -soph
      spawnProjectile(%p, LinearFlareProjectile, PCDisplayCharge);
      %obj.play3d(PCRFireSound);
      %obj.client.updateSensorPackText(%obj.getInventory("PCCAmmo"));
   }

   %obj.setImageTrigger(%slot, false);
   %obj.setImageTrigger(7, false);

   if(!%obj.chargingPCC)
      ChargePhaserC(%obj, %data);
}

function ChargePhaserC(%player, %datablock)       // should work....
{
   %ammo = %player.getInventory(PCCAmmo);
   %ammoMax = %player.getDatablock().max[PCCAmmo];

   if(%player.BAPackID != 7)
   {
      %player.chargingPCC = false;
      return;
   }

   if(%ammo > -1)   // jury rig for now
   {
      if(%ammo < %ammoMax)
      {
         if(!%player.isEMP)
         {
            %pct = %player.getEnergyPct();

//            if(%pct >= 0.25)
               %player.incInventory(PCCAmmo, 1);
               %player.client.updateSensorPackText(%player.getInventory("PCCAmmo"));
               %player.chargingPCC = true;
         }
//         else
//         {
//            %player.setInventory(PCCAmmo, 0);
//            %player.client.updateSensorPackText(%player.getInventory("PCCAmmo"));
//         }

         if(%pct == 1)
         {
            if(%player.isSyn)
               %mult = 300 ;	// = 4000; -soph //250 
            else
               %mult = 600 ;	// = 6000; -soph //500
         }
         else
         {
            if(%player.isSyn)
               %mult = 400 ;	// = 8000; -soph //500
            else
               %mult = 800 ;	// = 12000; -soph //1000
         }

         schedule(%mult, %player, "ChargePhaserC", %player, %datablock);
      }
      else
         %player.chargingPCC = false;
   }
}

function ShockBlasterPackImage::onDeactivate(%data, %obj, %slot)
{
}

function ShockBlasterPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
