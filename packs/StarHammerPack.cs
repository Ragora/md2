if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//STARHAMMER ------------------------------------------------------------------

datablock AudioProfile(StarHammerFireSound)
{
   filename    = "fx/misc/launcher.wav";
//   description = AudioDefault3d;
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(StarHammerReloadSound)
{
   filename    = "fx/misc/cannonstart.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock LinearProjectileData(SBRocket)
{
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.7;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::StarHammer;
   kickBackStrength    = 4000;
   bubbleEmitTime      = 1.0;

   explosion           = "ImpactMortarExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 0.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 150;
   wetVelocity       = 75;
   velInheritFactor  = 0.01;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock LinearProjectileData(BHRocket)
{
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 1.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 30.0;
   radiusDamageType    = $DamageType::BlastHammer;
   kickBackStrength    = 8000;
   bubbleEmitTime      = 1.0;

//   explosion           = "ImpactMortarExplosion";
   explosion           = SatchelMainExplosion;
   underwaterExplosion = UnderwaterSatchelMainExplosion;
   splash              = MissileSplash;
   velInheritFactor    = 0.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 175;
   wetVelocity       = 100;
   velInheritFactor  = 0.01;
   fizzleTimeMS      = 3000;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

datablock SeekerProjectileData(SBMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   directDamage        = 0.0 ;	// 0.7; -soph
   hasDamageRadius     = true;
   indirectDamage      = 1.7 ;	// 1.0; -soph
   damageRadius        = 14 ;	// 15.0; -soph
   radiusDamageType    = $DamageType::StarHammer;
   kickBackStrength    = 4000;
   bubbleEmitTime      = 1.0;

   explosion           = "ImpactMortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 0.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

//   baseEmitter         = NewMissileSmokeEmitter;
//   delayEmitter        = MagMissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 300;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 3000;
   muzzleVelocity      = 150 ;	// 10.0; -soph
   maxVelocity         = 150.0;
   turningSpeed        = 45.0;
   acceleration        = 999.0;

   proximityRadius     = 2;

   terrainAvoidanceSpeed         = 360;
   terrainScanAhead              = 50;
   terrainHeightFail             = 0;
   terrainAvoidanceRadius        = 200;

   flareDistance = 200;
   flareAngle    = 30;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 10;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(StarHammerAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some star hammer shells";
};

datablock ShapeBaseImageData(StarHammerPackImage)
{
   shapeFile = "pack_upgrade_ammo.dts";
   item = StarHammerPack;
   ammo = StarHammerAmmo;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(StarHammerPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_ammo.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "StarHammerPackImage";
	pickUpName = "a star hammer";
};


datablock ShapeBaseImageData(StarHammerImage)
{
   className = WeaponImage;
   shapeFile = "turret_missile_large.dts";
   offset = "-0.5 -0.1 0.5";
   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;
   item = StarHammerPack;

   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;
   ammo = StarHammerAmmo;
   
   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.0;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(StarHammerDImage) : StarHammerImage
{
   offset = "-0.5 0.5 0.5";
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(StarHammerHHImage) : StarHammerImage
{
   item = "";	// +soph
//   usesEnergy = true;
//   fireEnergy = 34;
//   minEnergy = 34;

//   shapeFile = "turret_elf_large.dts";

   isSeeker     = true;
   seekRadius   = 500;
   maxSeekAngle = 8;
   seekTime     = 0.75;
   minSeekHeat  = 0.6;

   // only target objects outside this range
   minTargetingDistance             = 15;
   
   offset = "0 0 0";
   rotation = "0 0 1 0";
   emap = true;
   mountPoint = 0;

   projectile = SBRocket;
   projectileType = LinearProjectile;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 2.5;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;
//   stateSound[3]                    = StarHammerFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 2.5;	// = 1.0; -soph
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
};

function StarHammerHHImage::onMount(%this,%obj,%slot)
{
     Parent::onMount(%this,%obj,%slot);

   %obj.unmountImage(7);
   %obj.weaponPackDisabled = true;	// %obj.fireTimeoutSHC = 0; -soph
   %obj.play3D("MILSwitchSound");
}

function StarHammerHHImage::throwWeapon( %this ) {}		// +soph

function StarHammerHHImage::onUnmount(%this,%obj,%slot)
{
     Parent::onUnmount(%this,%obj,%slot);

   if(%obj.client.race $= Bioderm)
      %obj.mountImage(StarHammerDImage, 7);
   else
      %obj.mountImage(StarHammerImage, 7);

//   %obj.fireTimeoutSHC = false;	// -soph
   %obj.weaponPackDisabled = false;	// +soph
}

function StarHammerHHImage::onFire(%data, %obj, %slot)
{
   %useEnergyObj = %obj.getObjectMount();
   %mountDamageMod = 0;

   if(!%useEnergyObj)
        %useEnergyObj = %obj.getObjectMount();
   else
        %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %target = %obj.getLockedTarget();

   if(%target)
   {
      %projectile = "SBMissile";
      %projectileType = "SeekerProjectile";
   }
   else
   {
      %projectile = "SBRocket";
      %projectileType = "LinearProjectile";
   }

      %p = new (%projectileType)()
      {
         dataBlock        = %projectile;
         initialDirection = %obj.getMuzzleVector(%slot);
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
         vehicleObject    = 0;
      };

   if (isObject(%obj.lastProjectile) && %obj.deleteLastProjectile)
      %obj.lastProjectile.delete();

   %obj.lastProjectile = %p;
   %obj.deleteLastProjectile = %data.deleteLastProjectile;
   MissionCleanup.add(%p);

   // AI hook
   if(%obj.client)
      %obj.client.projectile = %p;

   %obj.decInventory(%data.ammo, 1);

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;

   if(%data.projectile.ignoreReflect)
     %p.ignoreReflect = true;

   if(%target)
   {
      if(%target.getDatablock().jetfire) // || %target.getDatablock().MegaDisc)
         assignTrackerTo(%target, %p);
      else
         %p.setObjectTarget(%target);
   }
//   else if(%obj.isLocked())
//      %p.setPositionTarget(%obj.getLockedPosition());
//   else
//      %p.setNoTarget();

   spawnProjectile(%p, LinearFlareProjectile, MBDisplayCharge);
   projectileTrail(%p, 75, LinearFlareProjectile, SHExpCharge, true);
   %obj.play3d(StarHammerFireSound);
   %obj.client.updateSensorPackText(%obj.getInventory("StarHammerAmmo"));
//     %p = Parent::onFire(%data, %obj, %slot);
//     %obj.play3D(MortarDryFireSound);
   %obj.schedule(5000, play3D, "StarHammerReloadSound");	// 3500 -soph
   %obj.packWeaponTimeout = %time + 5000;			// +soph
}

function StarHammerPackImage::onMount(%this,%obj,%slot)
{
     %obj.BAPackID = 8;
     
    if(%obj.client.race $= Bioderm)
      %obj.mountImage(StarHammerDImage, 7);
   else
      %obj.mountImage(StarHammerImage, 7);
   if( !%obj.packWeaponTimeout )			// [soph]
      %obj.packWeaponTimeout = getSimTime();
   else
      %obj.weaponPackDisabled = false;			// [/soph]
   schedule(32, %obj, SHUpdateAmmoCount, %obj);
}

function SHUpdateAmmoCount(%obj)
{
     %obj.client.updateSensorPackText(%obj.getInventory("StarHammerAmmo"));
}

function StarHammerPackImage::onUnmount(%this,%obj,%slot)
{
     %obj.BAPackID = 0;
   %obj.unmountImage(7);
   %obj.client.updateSensorPackText(0);
}

function SHCFireTimeoutClear(%obj)	// no longer used -soph
{
   if( !%obj.fireTimeoutSHC )	// if(%obj.fireTimeoutSHC == 1) -soph
     return;
     
//   %obj.fireTimeoutSHC = 0;	// -soph
   %obj.play3d(StarHammerReloadSound);
}

function StarHammerPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;				// _here_ +soph
   %time = getSimTime();						// +soph
   if( %obj.weaponPackDisabled || %obj.packWeaponTimeout > %time )	// if(%obj.fireTimeoutSHC) -soph
      return;

   if(%obj.invPackBlockTime > %time )				// if(%obj.invPackBlockTime > getSimTime()) -soph
      return;
      
//   %obj.setImageTrigger(0, false);					// -soph

   if(%obj.getInventory("StarHammerAmmo"))
   {
      %p = new LinearProjectile()
      {
         dataBlock        = SBRocket;
         initialDirection = %obj.getMuzzleVector(7);
         initialPosition  = %obj.getMuzzlePoint(7);
         sourceObject     = %obj;
         sourceSlot       = 7;
      };
      MissionCleanup.add(%p);

      %obj.decInventory("StarHammerAmmo", 1);
      %obj.client.updateSensorPackText(%obj.getInventory("StarHammerAmmo"));
      
      %useEnergyObj = %obj.getObjectMount();

      if(!%useEnergyObj)
          %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
      %obj.setImageTrigger(7, true);
      spawnProjectile(%p, LinearFlareProjectile, MBDisplayCharge);
      projectileTrail(%p, 75, LinearFlareProjectile, ReaverCharge, true);
      %obj.play3d(StarHammerFireSound);
      %obj.packWeaponTimeout = %time + 5000;				// %obj.fireTimeoutSHC = 3500; -soph
//      schedule(%obj.fireTimeoutSHC, 0, "SHCFireTimeoutClear", %obj);	// -soph
      %obj.schedule(6500, play3D, "StarHammerReloadSound");		// +soph
   }
   else
   {
      %obj.packWeaponTimeout = %time + 1250;				// %obj.fireTimeoutSHC = 1250; -soph
//      schedule(%obj.fireTimeoutSHC, 0, "SHCFireTimeoutClear", %obj);	// -soph
      %obj.play3D(MortarDryFireSound);
   }
   
   %obj.setImageTrigger(%slot, false);
   %obj.setImageTrigger(7, false);
}

function StarHammerPackImage::onDeactivate(%data, %obj, %slot)
{
//   %obj.setImageTrigger(7, false);
//	%obj.setImageTrigger(%slot,false);
}

function StarHammerPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
