if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(GrenadeCannonPackImage)
{
   shapeFile = "ammo_grenade.dts";
   item = GrenadeCannonPack;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = calcThisRotD("90 0 0");
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(GrenadeCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "GrenadeCannonPackImage";
	pickUpName = "a grenade cannon";
};

datablock ShapeBaseImageData(GrenadeCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_grenade_launcher.dts";
   offset = "-0.5 -0.5 0.15";
   rotation = "0 1 0 32";
   item = GrenadeCannonPack;
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.75;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = GrenadeFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(GrenadeCannonDImage)
{
   className = WeaponImage;
   shapeFile = "weapon_grenade_launcher.dts";
   offset = "-0.5 -0.1 0.1";
   rotation = "0 1 0 32";
   item = GrenadeCannonPack;
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.75;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = GrenadeFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function GrenadeCannonPackImage::onMount(%this,%obj,%slot)
{
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(GrenadeCannonDImage, 7);
   else
      %obj.mountImage(GrenadeCannonImage, 7);

   schedule(32, %obj, GPUpdateAmmoCount, %obj , true );
}

function GPUpdateAmmoCount(%obj)
{
   if(%obj.getInventory("FireGrenade"))
       %grenType = "FireGrenade";
   else if(%obj.getInventory("EMPGrenade"))
       %grenType = "EMPGrenade";
   else if(%obj.getInventory("Grenade"))
       %grenType = "Grenade";
   else if(%obj.getInventory("PoisonGrenade"))
       %grenType = "PoisonGrenade";
   else if(%obj.getInventory("ConcussionGrenade"))
       %grenType = "ConcussionGrenade";
   else if(%obj.getInventory("FlashGrenade"))
       %grenType = "FlashGrenade";
   if( %grenType !$= "" )								// +soph
      %obj.gpackType = %grenType;
   GrenadeCAmmoUpdate(%obj);
}

function GrenadeCannonPackImage::onUnmount(%this,%obj,%slot)
{
   %obj.gpackType = "";
   %obj.unmountImage(7);
}

function GrenadeCAmmoUpdate(%obj)
{
   cancel( %obj.gpackType ) ;
   if(isObject(%obj) && %obj.gpackType !$= "")
   {
       %count = %obj.getInventory( %obj.gpackType ) ;					// +soph
       %obj.client.updateSensorPackText( %count ) ;					// .updateSensorPackText(%obj.getInventory(%obj.gpackType)); -soph
       if( %count > 0 )									// +soph
          %obj.gpackSchedule = schedule( 500 , %obj , GrenadeCAmmoUpdate , %obj ) ;	// schedule(2000, %obj, GrenadeCAmmoUpdate, %obj); -soph
       else										// +[soph]
          if( %obj.BAPackID == 11 )							// +
             %obj.gpackType = schedule( 250 , %obj , DGPUpdateAmmoCount , %obj ) ;	// +
          else 										// +
             %obj.gpackType = schedule( 250 , %obj , GPUpdateAmmoCount , %obj ) ;	// +[/soph]
   }
}

function GrenadeCFireTimeoutClear(%obj)
{
   if(%obj.fireTimeoutGrenadeC == 1)
     return;
     
   %obj.fireTimeoutGrenadeC = 0;
   %obj.play3D(BomberBombDryFireSound);
}

function GrenadeCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;		// _here_ +soph
   if(%obj.fireTimeoutGrenadeC)
      return;

   if(%obj.gpackType $= "")
   {
        %obj.play3D(MortarDryFireSound);
        return;
   }

   %grenType = %obj.gpackType;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;
   
   if(%obj.getInventory(%grenType))
   {
     %p = new LinearProjectile()
     {
//          dataBlock        = "GrenadeRocket";
          dataBlock        = "GrenadeCannonRocket";          
          initialDirection = %obj.getMuzzleVector(7);
          initialPosition  = %obj.getMuzzlePoint(7);
          sourceObject     = %obj;
          sourceSlot       = 7;
          starburstMode    = %grenType;
          damageMod        = %obj.damageMod;
          vehicleMod       = %obj.vehicleMod;
     };
     
     MissionCleanup.add(%p);
     %p.lastReflectedFrom = %obj;
     
     %obj.decInventory(%grenType, 1);
     %obj.play3D(MILFireSound);
     %obj.client.updateSensorPackText(%obj.getInventory(%obj.gpackType));
     
     %obj.setImageTrigger(7, true);
     %obj.fireTimeoutGrenadeC = 3000; //-Nite-  increased fire rate  was 1000 //MrKeen - changed it back
     schedule(%obj.fireTimeoutGrenadeC, 0, "GrenadeCFireTimeoutClear", %obj);
   }
   else
   {
          %obj.play3D(MortarDryFireSound);
          return;
   }

   %obj.setImageTrigger(7, false);
   %obj.setImageTrigger(%slot, false);
}

function GrenadeCannonPackImage::onDeactivate(%data, %obj, %slot)
{
//     %obj.setImageTrigger(7, false);
//     %obj.setImageTrigger(%slot, false);
}

function GrenadeCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
