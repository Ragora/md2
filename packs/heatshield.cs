if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------
// HeatShield PACK
// can be used by any armor type

datablock ShapeBaseImageData(HSDecalImage)
{
   mountPoint = 1;
   shapeFile = "ammo_plasma.dts";
   offset = "0.2 -0.2 -0.6";
//   rotation = "0 1 0 90";
   emap = 1;
};

datablock ShapeBaseImageData(HeatShieldPackImage)
{
   shapeFile = "ammo_plasma.dts";
   item = HeatShieldPack;
   mountPoint = 1;
   offset = "-0.2 -0.2 -0.6";
   emap = 1;

   stateName[0] = "default";
   stateSequence[0] = "ambient";
};

datablock ItemData(HeatShieldPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "HeatShieldPackImage";
	pickUpName = "a turbocharger pack";
};
function HeatShieldPackImage::onMount(%data, %obj, %node)
{
     %obj.BAPackID = 5;
     %obj.mountImage(HSDecalImage, 7);
     %obj.damageMod += 0.2;
}

function HeatShieldPackImage::onUnmount(%data, %obj, %node)
{
     %obj.BAPackID = 0;
     %obj.unmountImage(7);
     %obj.damageMod -= 0.2;
}

function HeatShieldPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}

function HeatShieldPack::damageObject( %data , %object , %source , %position , %amount , %type )
{
   if( %amount > 0.35 )
   {
      %p = new LinearFlareProjectile()
      {
         dataBlock        = DaiArmorBlowout;
         initialDirection = "0 0 1";
         initialPosition  = getWord( %object.getWorldBoxCenter() , 0) SPC getWord( %object.getWorldBoxCenter() , 1 ) SPC ( getWord( %object.getWorldBoxCenter() , 2 ) + 1 );
         sourceObject     = %source ;
         sourceSlot       = 0 ;
         damageFactor     = 45 ;
      };
      MissionCleanup.add(%p);
      %object.delete();
   }
}