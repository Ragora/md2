// ------------------------------------------------------------------
// AMMO PACK
// can be used by any armor type
// uses no energy, does not have to be activated
// increases the max amount of non-energy ammo carried

// put vars here for ammo max counts with ammo pack

$AmmoItem[0] = PlasmaAmmo;
$AmmoItem[1] = ChaingunAmmo;
$AmmoItem[2] = DiscAmmo;
$AmmoItem[3] = GrenadeLauncherAmmo;
$AmmoItem[4] = MortarAmmo;
$AmmoItem[5] = MissileLauncherAmmo;
$AmmoItem[6] = RepairKit;
$AmmoItem[7] = EnergyRifleAmmo;
$AmmoItem[8] = PCRAmmo;
$AmmoItem[9] = RAXXAmmo;
$AmmoItem[10] = Sniper3006Ammo;
$AmmoItem[11] = StarburstAmmo;
$AmmoItem[12] = MechMinigunAmmo;
$AmmoItem[13] = MechRocketGunAmmo;
$AmmoItem[14] = AutoCannonAmmo;
$AmmoItem[15] = MBCannonCapacitor;
$AmmoItem[16] = ProtronAmmo;
$AmmoItem[17] = PCRAmmo;
$AmmoItem[18] = EcstacyCapacitor;
$AmmoItem[19] = RFLAmmo;
$AmmoItem[20] = PCCAmmo;
$AmmoItem[21] = SpikeAmmo;

$numAmmoItems = 22;

$grenAmmoType[0] = Grenade;
$grenAmmoType[1] = ConcussionGrenade;
$grenAmmoType[2] = FlashGrenade;
$grenAmmoType[3] = FlareGrenade;
$grenAmmoType[4] = MortarGrenade;
$grenAmmoType[5] = EMPGrenade;
$grenAmmoType[6] = FireGrenade;
$grenAmmoType[7] = PoisonGrenade;

$numGrenTypes = 8;

$OtherType[0] = RepairKit;
$OtherType[1] = Mine;
$OtherType[2] = LargeMine;
$OtherType[3] = CloakMine;
$OtherType[4] = EMPMine;
$OtherType[5] = IncendiaryMine;
$OtherType[6] = BlastechMine;

$numOtherTypes = 7;

datablock ShapeBaseImageData(AmmoPackImage)
{
   shapeFile = "pack_upgrade_ammo.dts";
   item = AmmoPack;
   mountPoint = 1;
   offset = "0 0 0";
   
//   stateName[0] = "default";
//   stateSequence[0] = "activation";
//   stateScript[0] = "onLoop";

   usesEnergy = true;
   minEnergy = 25;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";

	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
//	stateSound[1] = InventoryPackActivateSound;
	stateTransitionOnTriggerUp[1] = "Deactivate";
     stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(AmmoPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_ammo.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "AmmoPackImage";
	pickUpName = "an ammo pack";

   computeCRC = true;


//   lightType = "PulsingLight";
//   lightColor = "0.2 0.4 0.0 1.0";
//   lightTime = "1200";
//   lightRadius = "1.0";

	max[PlasmaAmmo] = 50 ;		// = 60; -soph
	max[ChaingunAmmo] = 300;
	max[DiscAmmo] = 40 ;		// = 50; -soph
	max[GrenadeLauncherAmmo] = 35;
	max[MortarAmmo] = 15 ;		// = 20; -soph
	max[MissileLauncherAmmo] = 16;
	max[Grenade] = 10;
	max[ConcussionGrenade] = 6 ;	// = 10; -soph
	max[FlashGrenade] = 6 ;		// = 10; -soph
	max[FlareGrenade] = 7 ;		// = 10; -soph
	max[MortarGrenade] = 2 ;	// = 5; -soph
	max[EMPGrenade] = 7 ;		// 10; -soph
	max[FireGrenade] = 6 ;		// = 10; -soph
	max[PoisonGrenade] = 9 ;	// = 10; -soph

	max[Mine] = 4 ;			// = 5; -soph
	max[ LargeMine ]	= 1 ;	// +[soph]
	max[ CloakMine ]	= 4 ;	// +
	max[ EMPMine ]		= 4 ;	// +
	max[ IncendiaryMine ]	= 4 ;	// +
	max[ BlastechMine ]	= 3 ;	// +[/soph]

	max[RepairKit] = 1;

	max[EnergyRifleAmmo] = 35;
  	max[Sniper3006Ammo] = 10;
  	max[StarburstAmmo] = 8;
  	max[MechMinigunAmmo] = 250;
  	max[MechRocketGunAmmo] = 30;	// = 40; -soph
  	max[AutoCannonAmmo] = 250;
  	max[RAXXAmmo] = 300;
  	max[SpikeAmmo] = 25;

  	max[MBCannonCapacitor] = 50;
  	max[ProtronAmmo] = 200 ;	// 150; +soph
  	max[PCRAmmo] = 25;
  	max[EcstacyCapacitor] = 256;
  	max[PCCAmmo] = 25;
  	max[RFLAmmo] = 50;
	max[PBWAmmo] = 8 ;		// +soph
};

function AmmoPack::onPickup(%this,%pack,%player,%amount)
{
     %player.BAPackID = 4;
     
	// %this = AmmoPack datablock
	// %pack = AmmoPack object number
	// %player = player
	// %amount = 1

     %data = %player.getDatablock();					// +soph
     for (%idx = 0; %idx < $numAmmoItems; %idx++)
     {
          %ammo = $AmmoItem[%idx];
          if (%pack.inv[%ammo] > 0)
          {
               %amount = %pack.getInventory(%ammo);
               %player.incInventory(%ammo,%amount);
               if ( %data.isEngineer )					// [soph]
               {							// engineer exception
                    %packmax = AmmoPack.max[%ammo];
                    if( %packmax < 20 )
                         %packmax = 2;
                    else if( %packmax < 50 )
                         %packmax = 10;
                    else if( %packmax < 100 )
                         %packmax = 25;
                    else
                         %packmax = 50;
                    %max = %data.max[%ammo] + %packmax;
                    if ( %player.getInventory( %ammo ) > %max )
                         %player.setInventory( %ammo , %max );
               }							// [/soph]
               %pack.setInventory(%ammo,0);
          }
          else if(%pack.inv[%ammo] == -1)
          {
               // this particular type of ammo has already been exhausted for this pack;
               // don't give the player any
          }
          else
          {
               // Assume it's full if no inventory has been assigned
               %player.incInventory(%ammo,%this.max[%ammo]);
               if ( %data.isEngineer )					// [soph]
               {							// engineer exception
                    %packmax = AmmoPack.max[%ammo];
                    if( %packmax < 20 )
                         %packmax = 2;
                    else if( %packmax < 50 )
                         %packmax = 10;
                    else if( %packmax < 100 )
                        %packmax = 25;
                    else
                         %packmax = 50;
                    %max = %data.max[%ammo] + %packmax;
                    if ( %player.getInventory( %ammo ) > %max )
                         %player.setInventory( %ammo , %max );
               }							// [/soph]
          }	
     }

     if(!%player.charging)				// [soph]
     {							// plugging hole
          %player.charging = true;  //case MB		// charging weapons would not charge if they were full before pickup
          ChargeMBC(%player, %data);			// this should kickstart them again
     }							// logic structure copied from below
  
     if(!%player.chargingProt)
     {
          %player.chargingProt = true; //case Protron
          ChargeProtron(%player, %data);	
     }
     if(!%player.chargingPCR)
     {
          %player.chargingPCR = true; //case Phaser
          ChargePhaser(%player);
     }
     if(!%player.chargingPCC)
     {
          %player.chargingPCC = true; //case Phaser
          ChargePhaserC(%player);
     }
     if(!%player.chargingRFL)
     {
          %player.chargingRFL = true; //case Phaser
          ChargeRFL(%player);
     }							// [/soph]

	// now check what type grenades (if any) player is carrying
	%gFound = false;
	for (%i = 0; %i < $numGrenTypes; %i++)
	{
		%gType = $grenAmmoType[%i];
		if(%player.inv[%gType] > 0)
		{
			%gFound = true;
			%playerGrenType = %gType;
			break;
		}
	}
	// if player doesn't have any grenades at all, give 'em whatever the ammo pack has
	if(!%gFound)
	{
		%given = false;
		for(%j = 0; %j < $numGrenTypes; %j++)
		{
			%packGren = $grenAmmoType[%j];
			if(%pack.inv[%packGren] > 0)
			{
				// pack has some of these grenades
	         %player.incInventory(%packGren, %pack.getInventory(%packGren));
	         %pack.setInventory(%packGren, 0);
				%given = true;
				break;
			}
			else if(%pack.inv[%packGren] == -1)
			{
				// the pack doesn't have any of this type of grenades
			}
			else
			{
				// pack has full complement of this grenade type
	         %player.incInventory(%packGren, %this.max[%packGren]);
				%given = true;
			}
			if(%given)
				break;
		}
	}
	else
	{
		// player had some amount of grenades before picking up pack
		if(%pack.inv[%playerGrenType] > 0)
		{
			// pack has 1 or more of this type of grenade
         %player.incInventory(%playerGrenType, %pack.getInventory(%playerGrenType));
         %pack.setInventory(%playerGrenType, 0);
		}
		else if(%pack.inv[%playerGrenType] == -1)
		{
			// sorry Chester, the pack is out of these grenades.
		}
		else
		{
			// pack is uninitialized for this grenade type -- meaning it has full count
			%player.incInventory(%playerGrenType, %this.max[%playerGrenType]);
		}
	}
	// now see if player had mines selected and if they're allowed in this mission type
	%mineFav = %player.client.favorites[getField(%player.client.mineIndex, 0)];
	if ( ( $InvBanList[$CurrentMissionType, "Mine"] !$= "1" )
	  && !( ( %mineFav $= "EMPTY" ) || ( %mineFav $= "INVALID" ) ) )
	{
		// player has selected mines, and they are legal in this mission type
		if(%pack.inv[Mine] > 0)
		{
			// and the pack has some mines in it! bonus!
			%player.incInventory(Mine, %pack.getInventory(Mine));
			%pack.setInventory(Mine, 0);
		}
		else if(%pack.inv[Mine] == -1)
		{
			// no mines left in the pack. do nothing.
		}
		else
		{
			// assume it's full of mines if no inventory has been assigned
			%player.incInventory(Mine,%this.max[Mine]);
		}
	}
}

function AmmoPack::onThrow(%this,%pack,%player)
{
	// %this = AmmoPack datablock
	// %pack = AmmoPack object number
	// %player = player

	%player.throwAmmoPack = 1;
	dropAmmoPack(%pack, %player);
	// do the normal ItemData::onThrow stuff -- sound and schedule deletion
   serverPlay3D(ItemThrowSound, %player.getTransform());
   %pack.schedulePop();
}

function AmmoPack::onInventory(%this,%player,%value)
{
	// %this = AmmoPack
	// %player = player
	// %value = 1 if gaining a pack, 0 if losing a pack

	// the below test is necessary because this function is called everytime the ammo
	// pack gains or loses an item
	if(%player.isPlayer())
	{
		if(!%value)
          {
			if(%player.throwAmmoPack == 1)
			{
				%player.throwAmmoPack = 0;
				// ammo stuff already handled by AmmoPack::onThrow
			}
			else
			{
				// ammo pack was sold at inventory station -- no need to worry about
				// setting the ammo in the pack object (it doesn't exist any more)
				dropAmmoPack(-1, %player);
			}
   
               %player.hasAmmoPack = false;
               %player.BAPackID = 0;
          }
          else
          {
               %player.hasAmmoPack = true;          
               %player.BAPackID = 4;
          }
	}

     Pack::onInventory(%this,%player,%value);
}

function dropAmmoPack(%packObj, %player)
{
     %player.BAPackID = 0;
	// %packObj = Ammo Pack object number if pack is being thrown, -1 if sold at inv station
	// %player = player object

	for(%i = 0; %i < $numAmmoItems; %i++)
	{
		%ammo = $AmmoItem[%i];
		// %pAmmo == how much of this ammo type the player has
		%pAmmo = %player.getInventory(%ammo);
		// %pMax == how much of this ammo type the player's datablock says can be carried
		%pMax = %player.getDatablock().max[%ammo];
		if(%pAmmo > %pMax)
		{
			// if player has more of this ammo type than datablock's max...
			if(%packObj > 0)
			{
				// put ammo that player can't carry anymore in pack
				%packObj.setInventory(%ammo, %pAmmo - %pMax);
			}
			// set player to max for this ammo type
			%player.setInventory(%ammo, %pMax);
		}
		else
		{
			if(%packObj > 0)
			{
				// the pack gets -1 of this ammo type -- else it'll assume it's full
				// can't use setInventory() because it won't allow values less than 1
				%packObj.inv[%ammo] = -1;
			}
		}
	}
	// and, of course, a pass for the grenades. Whee.
	%gotGren = false;
	// check to see what kind of grenades the player has.
	for(%j = 0; %j < $numGrenTypes; %j++)
	{
		%gType = $grenAmmoType[%j];
		if(%player.inv[%gType] > 0)
		{
			%gotGren = true;
			%playerGren = %gType;
			break;
		}
		else
		{
			// ammo pack only carries type of grenades that player who bought it (or picked
			// it up) had at the time -- all else are zeroed out (value of -1)
			if(%packObj > 0)
				%packObj.inv[%gType] = -1;
		}
	}
	if(%gotGren)
	{
		%pAmmo = %player.getInventory(%playerGren);
		%pMax = %player.getDatablock().max[%playerGren];
		if(%pAmmo > %pMax)
		{
			if(%packObj > 0)
				%packObj.setInventory(%playerGren, %pAmmo - %pMax);
			%player.setInventory(%playerGren, %pMax);
		}
		else
			if(%packObj > 0)
				%packObj.inv[%playerGren] = -1;
	}
	else
	{
		// player doesn't have any grenades at all. nothing needs to be done here.
	}
	// crap. forgot the mines.
	if(%player.getInventory(Mine) > %player.getDatablock().max[Mine])
	{
		// if player has more mines than datablock's max...
		if(%packObj > 0)
		{
			// put mines that player can't carry anymore in pack
			%packObj.setInventory(Mine, %player.getInventory(Mine) - %player.getDatablock().max[Mine]);
		}
		// set player to max mines
		%player.setInventory(Mine, %player.getDatablock().max[Mine]);
	}
	else
	{
		if(%packObj > 0)
		{
			// the pack gets -1 for mines -- else it'll assume it's full
			// can't use setInventory() because it won't allow values less than 1
			%packObj.inv[Mine] = -1;
		}
	}
}

function resetInvPTimeout(%obj)
{
   %obj.p_invTimeout = 0;
   messageClient(%obj.client, 'MsgInvPackRegenAmmoD', '\c2Ammo pack is done cooling off.');
}

function checkCloakOff(%obj)
{
   if(%obj.isCloaked())
      %obj.setCloaked(false);
}

function buyPackFavorites(%client)
{
   %player = %client.player;
   %prevPack = %player.getMountedImage($BackpackSlot);
   %player.setInventory( %prevPack.item, 0 );
   %player.clearInventory();
   %client.setWeaponsHudClearAll();
   %cmt = $CurrentMissionType;
   %data = %player.getDatablock();	// -soph
   if(!%client.player.charging)
   {
        %player.charging = true; //case MB
        ChargeMBC(%player, %data);	// ChargeMBC(%client.player, %client.player.getDatablock()); -soph
   }

   if(!%player.chargingProt)
   {
        %player.chargingProt = true; //case Protron
        ChargeProtron(%player, %data);	// ChargeProtron(%client.player, %client.player.getDatablock()); -soph
   }

   if(!%player.chargingPCR)
   {
        %player.chargingPCR = true; //case Phaser
        ChargePhaser(%player);
   }

   if(!%player.chargingPCC)
   {
        %player.chargingPCC = true; //case Phaser
        ChargePhaserC(%player);
   }

   if(!%player.chargingRFL)
   {
        %player.chargingRFL = true; //case Phaser
        ChargeRFL(%player);
   }

   // only new ammo, not anything else
   %weapCount = 0;
   for ( %i = 0; %i < getFieldCount( %client.weaponIndex ); %i++ )
   {
      %inv = $NameToInv[%client.favorites[getField( %client.weaponIndex, %i )]];
      if ( !($InvBanList[DeployInv, %inv]) )
      {
         %player.setInventory( %inv, 1 );
         // increment weapon count if current armor can hold this weapon
         if(%data.max[%inv] > 0)	// if(%player.getDatablock().max[%inv] > 0) -soph
            %weapCount++;

         // z0dd - ZOD, 9/13/02. Streamlining
         if ( %inv.image.ammo !$= "" )
            if( %data.isEngineer )								// [soph]
            {										  	// engineer exception
               %packmax = AmmoPack.max[%inv.image.ammo];
               if( %packmax < 20 )
                  %packmax = 2;
               else if( %packmax < 50 )
                  %packmax = 10;
               else if( %packmax < 100 )
                  %packmax = 25;
               else
                  %packmax = 50;
               %player.setInventory( %inv.image.ammo, %data.max[%inv.image.ammo] + %packmax );
            }
            else										// [/soph]
               %player.setInventory( %inv.image.ammo, 2500 );

         if(%inv $= "MBCannon") // hack - would give 2000 ammo to MB :o lol
            %client.player.setInventory( MBCannonCapacitor, 50 );

         if(%inv $= "Protron")
            %client.player.setInventory( ProtronAmmo, 100 );

         if(%inv $= "PCR")
            %client.player.setInventory( PCRAmmo, mFloor(%client.player.getDatablock().max[PCRAmmo] / 2) );

         if(%weapCount >= %player.getDatablock().maxWeapons)
            break;
      }
   }

   %player.weaponCount = %weapCount;

   // give player the grenades and mines they chose, beacons, and a repair kit
   for ( %i = 0; %i < getFieldCount( %client.grenadeIndex ); %i++)
   {
      %GInv = $NameToInv[%client.favorites[getField( %client.grenadeIndex, %i )]];
      if ( !($InvBanList[DeployInv, %GInv]) )
         %player.setInventory( %GInv, 30 );
   }

   // if player is buying cameras, show how many are already deployed
   if(%client.favorites[%client.grenadeIndex] $= "Deployable Camera")
   {
      %maxDep = $TeamDeployableMax[DeployedCamera];
      %depSoFar = $TeamDeployedCount[%client.player.team, DeployedCamera];
      if(Game.numTeams > 1)
         %msTxt = "Your team has "@%depSoFar@" of "@%maxDep@" Deployable Cameras placed.";
      else
         %msTxt = "You have placed "@%depSoFar@" of "@%maxDep@" Deployable Cameras.";
      messageClient(%client, 'MsgTeamDepObjCount', %msTxt);
   }

   for ( %i = 0; %i < getFieldCount( %client.mineIndex ); %i++ )
   {
      %MInv = $NameToInv[%client.favorites[getField( %client.mineIndex, %i )]];
      if ( !($InvBanList[DeployInv, %MInv]) )
         %player.setInventory( %MInv, 30 );
   }

   if ( !($InvBanList[DeployInv, Beacon]) && !($InvBanList[%cmt, Beacon]) )
      %player.setInventory( Beacon, 1000 );
   if ( !($InvBanList[DeployInv, RepairKit]) && !($InvBanList[%cmt, RepairKit]) )
      %player.setInventory( RepairKit, 10 );
//   if ( !($InvBanList[DeployInv, TargetingLaser]) && !($InvBanList[%cmt, TargetingLaser]) )
   %player.setInventory( TargetingLaser, 1 );

   %player.setInventory( %prevPack.item, 1 );
   invAmmoPackPass(%client);
   
   if(%client.player.currentArmorMod !$= "")
      %client.player.setInventory(%client.player.currentArmorMod, 1);   
}

function invAmmoPackPass(%client)
{
   // "normal" ammo stuff (everything but mines and grenades)
   %player = %client.player;					// [soph]
   %data = %player.getDatablock();	
   %pack = %player.getMountedImage($BackpackSlot);		// [/soph]
   for ( %idx = 0; %idx < $numAmmoItems; %idx++ ) 
   {
      %ammo = $AmmoItem[%idx];
      %player.incInventory(%ammo, AmmoPack.max[%ammo]);		// [soph]
      if ( %data.isEngineer )					// engineer exception 
      {
           %packmax = AmmoPack.max[%ammo];
           if( %packmax < 20 )
                %packmax = 2;
           else if( %packmax < 50 )
                %packmax = 10;
           else if( %packmax < 100 )
                %packmax = 25;
           else
                %packmax = 50;
           %max = %data.max[%ammo] + %packmax;
           if ( %player.getInventory( %ammo ) > %max )
                %player.setInventory( %ammo , %max );
      }								// [/soph]
      if(%ammo $= "MBCannonCapacitor") // hack - would give 2000 ammo to MB :o lol
           %client.player.setInventory( MBCannonCapacitor, 50 );

      if(%ammo $= "ProtronAmmo")
           %client.player.setInventory( ProtronAmmo, mFloor( %client.player.getInventory( ProtronAmmo ) / 2 ) ) ;	// 100 ); -soph
      else														// +soph
      if(%ammo $= "PCRAmmo")
           %client.player.setInventory( PCRAmmo, mFloor( %client.player.getInventory( PCRAmmo ) / 2 ) ) ;		// mFloor(%client.player.getDatablock().max[PCRAmmo] / 2) );	-soph

      else if( %ammo $= "PCCAmmo" )											// +[soph]
           %client.player.setInventory( PCCAmmo , mFloor( %client.player.getInventory( PCCAmmo ) / 2 ) ) ;		// +
      else if( %ammo $= "RFLAmmo" )											// +
           %client.player.setInventory( RFLAmmo , mFloor( %client.player.getInventory( RFLAmmo ) / 2 ) ) ;		// +[/soph]


   }
   //our good friends, the grenade family *SIGH*
   // first find out what type of grenade the player has selected
   %grenFav = %client.favorites[getField(%client.grenadeIndex, 0)];
   if((%grenFav !$= "EMPTY") && (%grenFav !$= "INVALID"))
      %client.player.incInventory($NameToInv[%grenFav], AmmoPack.max[$NameToInv[%grenFav]]);
   // now the same check for mines
   %mineFav = %client.favorites[getField(%client.mineIndex, 0)];
   if((%mineFav !$= "EMPTY") && (%mineFav !$= "INVALID") && !($InvBanList[%cmt, Mine]))
      %client.player.incInventory($NameToInv[%mineFav], AmmoPack.max[$NameToInv[%mineFav]]);
}

function AmmoPackImage::onActivate(%data, %obj, %slot)
{
   if(%obj.overdriveMode)
   {
       %obj.setImageTrigger(%slot, false);
       return;
   }
   
   if(!%obj.p_invTimeout)
   {
      if(%obj.getDatablock().isBlastech && %obj.getEnergyLevel() > 1800)
      {
         %obj.setCloaked(true);
         %obj.play3D(DepInvActivateSound);
         %obj.schedule(500, "setCloaked", false);
         schedule(1100, %obj, "checkCloakOff", %obj);
         buyPackFavorites(%obj.client);
         %obj.setEnergyLevel(%obj.getEnergyLevel() - 1800);
  	    %obj.setImageTrigger(%slot,false);
         %obj.p_invTimeout = getSimTime();			// %obj.p_invTimeout = 45000; [soph]
         schedule(45000, 0, resetInvPTimeout, %obj);		// schedule(%obj.p_invTimeout, 0, resetInvPTimeout, %obj); [/soph]
         messageClient(%obj.client, 'MsgInvPackRegenAmmo', '\c2Ammo pack is now updating inventory. Cooling off phase initiated (45 sec).');
      }
      else if(!%obj.isEMP && getEnergyPct(%obj) == 1) //&& checkForInvs(%obj)
      {
         %obj.setCloaked(true);
         %obj.play3D(DepInvActivateSound);
         %obj.schedule(500, "setCloaked", false);
         schedule(1100, %obj, "checkCloakOff", %obj);
         buyPackFavorites(%obj.client);
         %obj.setEnergyLevel(0);
      	%obj.setImageTrigger(%slot,false);
         %obj.p_invTimeout = getSimTime();			// %obj.p_invTimeout = 45000; [soph]
         schedule(45000, 0, resetInvPTimeout, %obj);		// schedule(%obj.p_invTimeout, 0, resetInvPTimeout, %obj); [/soph]
         messageClient(%obj.client, 'MsgInvPackRegenAmmo', '\c2Ammo pack is now updating inventory. Cooling off phase initiated (45 sec).');
      }
      else //if(getEnergyPct(%obj) < 1)
      {
         if(%obj.getDatablock().isBlastech)
             // messageClient(%obj.client, 'MsgLowPowerBTInvRange', '\c2You need to have at least 1800 KW to regenerate your inventory.');	// -soph
              %msTxt = "You need to have at least 1800 KW to regenerate your inventory.";							// +soph
         else
             // messageClient(%obj.client, 'MsgLowPowerInvRange', '\c2You need to have full energy to regenerate your inventory.');		// -soph
              %msTxt = "You need to have full energy to regenerate your inventory.";								// +soph
         messageClient( %obj.client , 'MsgLowPowerInvRange' , '\c2%1' , %msTxt);								// +soph
                       
         %obj.setImageTrigger(%slot,false);
         return;
      }
//      else
//      {
//         InitContainerRadiusSearch(%obj.getPosition(), 10000, $TypeMasks::StationObjectType);
//         while((%int = ContainerSearchNext()) != 0)
//         {
//            if(%int.team == %pl.client.team && %int.getDamageState() $= "Enabled")
//               %obj.d_invdist = mFloor(getObjectDistance(%obj, %int))@"m";
//            else
//               %obj.d_invdist = "unusable";
//         }
//
//         messageClient(%obj.client, 'MsgInvOutOfRange', '\c2You must be within 300m of one of your teams working stations in order to use this pack. Closest inventory is %1.', %obj.d_invdist);
//         %obj.setImageTrigger(%slot,false);
//      }
   }
   else
   {
      messageClient(%obj.client, 'MsgLowInvCD', '\c2Ammo pack requires %1 more seconds to cool off.', ( 45 - mFloor ( ( getSimTime() - %obj.p_invTimeout ) / 1000 ) ) );	// messageClient(%obj.client, 'MsgLowInvCD', '\c2Ammo pack is cooling off.'); -soph
      %obj.setImageTrigger(%slot,false);
   }
}

function AmmoPackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger(%slot,false);
}
