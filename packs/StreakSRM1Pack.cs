if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();


datablock ShapeBaseImageData(SRM1PackImage)
{
   shapeFile = "pack_barrel_missile.dts";
   item = SRM1Pack;
   ammo = SRM4Ammo;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(SRM1Pack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_barrel_missile.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "SRM1PackImage";
	pickUpName = "a Streak SRM1 Launcher";
};

datablock ShapeBaseImageData(SRM1Image)
{
   className = WeaponImage;
   shapeFile = "weapon_missile.dts";
   offset = "-0.4 0.5 0.2";
   rotation = "0 1 0 45";
   emap = true;
   mountPoint = 1;
   item = SRM1Pack;

   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;
   ammo = SRM4Ammo;
   
   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.0;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function SRM1PackImage::onMount( %this ,%obj , %slot )
{
   %obj.mountImage( SRM1Image , 1 ) ;

   %obj.srm4active = true ;				// re-uses this
   if( !%obj.packWeaponTimeout )
      %obj.packWeaponTimeout = getSimTime() ;
   else
      %obj.weaponPackDisabled = false ;

   schedule( 32 , %obj , SRM4UpdateAmmoCount , %obj ) ;	// re-uses SRM4 script
}

function SRM1PackImage::onUnmount( %this , %obj , %slot )
{
   %obj.unmountImage( 1 ) ;
   %obj.client.updateSensorPackText( 0 ) ;
   %obj.srm4active = false ;   
}

function SRM1PackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;
   %time = getSimTime() ;
   if( %obj.packWeaponTimeout > %time )	
      return ;

   %weapon = %obj.getMountedImage(0).item ;
   if( %weapon $= "MissileLauncher" )
   {
      %cluster = false ;
      switch ( %obj.client.mode[ %weapon ] )
      {
            case 0 :
                  %projectile = "ShoulderMissile" ;
                  %nerf = 1 / 2 ;					// + 50 + 100 = 150 
            case 1 :							// +
                  %projectile = "FastMissile" ;				// +
                  %nerf = 1 / 2 ;					// + 32.5 + 65 = 97.5
            case 2 :							// +
                  %projectile = "EMPMissile" ;				// +
                  %nerf = 1 / 2 ;					// + 75 + 150 = 225
            case 3 :							// +
                  %projectile = "IncendiaryMissile" ;			// +
                  %nerf = 1 / 2 ;					// + 10 + 20 = 30
            case 4 :							// +
                  %projectile = "MegaMissile" ;  			// +
                  %nerf = 1 / 2 ;					// + 75 + 150 = 225
            case 5 :							// +
                  %projectile = "ShoulderMissile" ;			// +
                  %nerf = 2 / 5 ;					// + 40 + 100 = 140
            case 6 :							// +
                  %projectile = "ShoulderMissile" ;			// +
                  %nerf = 1 / 2 ;					// + 10*4 + 80 = 120
                  %cluster = true ;
      }	
      if( !%obj.packFireLink )						// + if no link, create one
      {
         %obj.packFireLink = true ;
         %obj.setImageTrigger( 0 , true ) ;
         %obj.packFireLink = true ;
      }
   }
   else
   {
      %nerf = 1 ;
      %projectile = "StreakSRM4Missile" ;
   }
   %obj.setImageTrigger( 0 , false ) ;

   if( %obj.getInventory( "SRM4Ammo" ) )
   {
      %start = %obj.packFireLink ? 200 : 0 ;
      if( %cluster )
         schedule( %start , 0 , SRMPackFireStaggered , %obj , 1 , %projectile , %nerf , 1 , 1 ) ;
      else
         schedule( %start , 0 , SRMPackFireStaggered , %obj , 1 , %projectile , %nerf , 0 , 1 ) ;

      %obj.setImageTrigger( 1, true ) ;
      %obj.play3d( SRM4FireSound ) ;
      %obj.packWeaponTimeout = %time + 4000 ;
   }
   else
   {
      %obj.packWeaponTimeout = %time + 1250 ;
      %obj.play3D( MortarDryFireSound ) ;
   }
   %obj.setImageTrigger( %slot , false ) ;
   %obj.setImageTrigger( 1 , false ) ;
}

function SRM1PackImage::onDeactivate( %data , %obj , %slot ) { }

function SRM1Pack::onPickup( %this , %obj , %shape , %amount ) { }