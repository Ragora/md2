// ------------------------------------------------------------------

datablock ShapeBaseImageData(PlasmaCannonWPackImage)
{
   shapeFile = "ammo_plasma.dts";
   item = PlasmaCannonPack;
   mountPoint = 1;
   offset = "0 -0.2 -0.5";
   rotation = "0 1 0 0";
   
   usesEnergy = true;
   minEnergy = 1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(PlasmaCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "PlasmaCannonWPackImage";
	pickUpName = "a heavy plasma cannon";
};

datablock ShapeBaseImageData(PlasmaCannonPackImage)
{
   className = WeaponImage;
   item = Blaster;
   shapeFile = "weapon_plasma.dts";
   offset = "-0.4 -0.35 0.2";
   rotation = "0 1 0 45";
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;
   
//   projectile = BomberFusionBolt;
//   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.25;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = PlasmaCannonPackFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(PlasmaCannonPackDImage) : PlasmaCannonPackImage
{
//   shapeFile = "weapon_plasma.dts"; //turret_mortar_large.dts";
   offset = "-0.4 0 0.2";
   rotation = "0 1 0 32";
   emap = true;
   mountPoint = 1;
   fireEnergy = -1;
   minEnergy = -1;
   
//   stateSequence[2]                 = "Deploy";
//   stateScript[3] = "";
};

function PlasmaCannonWPackImage::onMount(%this,%obj,%slot)
{
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(PlasmaCannonPackDImage, 7);
   else
      %obj.mountImage(PlasmaCannonPackImage, 7);
}

function PlasmaCannonWPackImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(7);
}

function PlasmaCFireTimeoutClear(%obj)
{
   %obj.fireTimeoutHPC = 0;
   %obj.play3d(PlasmaReloadSound);
}

function PlasmaCannonWPackImage::onActivate(%data, %obj, %slot)
{
   if(%obj.fireTimeoutHPC)
      return;

   %obj.setImageTrigger(0, false);

   if(%obj.getEnergyLevel() >= 25)
   {
      %p = new LinearFlareProjectile()
      {
         dataBlock        = dPlasmaBarrelBolt;
         initialDirection = %obj.getMuzzleVector(7);
         initialPosition  = %obj.getMuzzlePoint(7);
         sourceObject     = %obj;
         sourceSlot       = 7;
      };
      MissionCleanup.add(%p);

      %obj.useEnergy(25);

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   // Vehicle Damage Modifier
   if(%vehicle)
     %p.vehicleMod = %vehicle.damageMod;

   if(%obj.damageMod)
     %p.damageMod = %obj.damageMod;
     
      %obj.setImageTrigger(7, true);
      %obj.fireTimeoutHPC = 900;
      schedule(%obj.fireTimeoutHPC, 0, "PlasmaCFireTimeoutClear", %obj);
      %obj.play3D(PBLFireSound);
   }

     %obj.setImageTrigger(7, false);
     %obj.setImageTrigger(%slot,false);
}

function PlasmaCannonWPackImage::onDeactivate(%data, %obj, %slot)
{
//     %obj.setImageTrigger(7, false);
//     %obj.setImageTrigger(%slot,false);
}

function PlasmaCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
