if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//SRM4 ------------------------------------------------------------------

datablock SeekerProjectileData(StreakSRM4Missile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   directDamage        = 0.0;
   indirectDamage      = 0.65;	// 0.6; -soph
   damageRadius        = 5.0;	// = 3.5; still less than Fast -soph
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 750;

   flareDistance = 225;
   flareAngle    = 30;
   minSeekHeat   = 0.4;

   explosion           = "MissileExplosion";
   velInheritFactor    = 1.0;

   baseEmitter         = NewMissileSmokeEmitter;
   delayEmitter        = MagMissileFireEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;

   lifetimeMS          = 3000;
   muzzleVelocity      = 20.0;
   turningSpeed        = 250 ;	// = 850.0; -soph
   acceleration        = 125.0;
   maxVelocity         = 233.0;
   
   proximityRadius     = 4;

   terrainAvoidanceSpeed = 180;
   terrainScanAhead      = 25;
   terrainHeightFail     = 12;
   terrainAvoidanceRadius = 100;

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(SRM4Ammo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some SRM missiles";	// = "some SRM4 missiles"; -soph
};

datablock ShapeBaseImageData(SRM4PackImage)
{
   shapeFile = "pack_barrel_missile.dts";
   item = SRM4Pack;
   ammo = SRM4Ammo;
   mountPoint = 1;
   offset = "0 0 0";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(SRM4Pack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_barrel_missile.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "SRM4PackImage";
	pickUpName = "a Streak SRM4 Launcher";
};

datablock ShapeBaseImageData(SRM4Image)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   offset = "-0.5 0.65 0.5";
   rotation = "0 0 1 180";
   emap = true;
   mountPoint = 1;
   item = SRM4Pack;

   usesEnergy = false;
   fireEnergy = -1;
   minEnergy = -1;
   ammo = SRM4Ammo;
   
   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = DiscFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 1.0;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(SRM4DImage) : SRM4Image
{
   offset = "-0.5 0.5 0.5";
   rotation = "0 1 0 180";
};

datablock ShapeBaseImageData(SRM4HHImage) : SRM4Image
{
   item = "";	// +soph
//   usesEnergy = true;
//   fireEnergy = 34;
//   minEnergy = 34;

   shapeFile = "TR2weapon_mortar.dts";

   isSeeker     = true;
   seekRadius   = 500; // 600
   maxSeekAngle = 8;
   seekTime     = 0.5;
   minSeekHeat  = 0.5;	// = 0.6; -soph

   // only target objects outside this range
   minTargetingDistance             = 5;
   
   offset = "0.25 0.6 0";
//   rotation = "0 0 1 0";
   emap = true;
   mountPoint = 0;

   projectile = StreakSRM4Missile;
   projectileType = LinearFlareProjectile;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;
//   stateSound[3]                    = SRM4Fire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 5.0;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
};

function SRM4HHImage::onMount(%this,%obj,%slot)
{
     Parent::onMount(%this,%obj,%slot);

   %obj.unmountImage(7);
//   %obj.fireTimeoutSRM4 = true;
   %obj.play3D("MBLSwitchSound");
   
//   if(%obj.fireTimeoutSRM4 > 1)
//      %obj.fireTimeoutSRM4 = 2;
//   else
    %obj.weaponPackDisabled = true;	// %obj.fireTimeoutSRM4 = true; -soph
}

function SRM4HHImage::throwWeapon( %this ) {}		// +soph

function SRM4HHImage::onUnmount(%this,%obj,%slot)
{
     Parent::onUnmount(%this,%obj,%slot);

   if(%obj.client.race $= Bioderm)
      %obj.mountImage(SRM4DImage, 7);
   else
      %obj.mountImage(SRM4Image, 7);

//   %obj.fireTimeoutSRM4 = false;	// -soph
   %obj.weaponPackDisabled = false;	// +soph
}

function SRM4HHImage::onFire(%data, %obj, %slot)
{
   if(%obj.fireTimeoutSRM4 == 2)
      return;
      
   for(%i = 1; %i < 5; %i++)
      schedule(%i*250, 0, SRMPackFireStaggered, %obj, %slot, "StreakSRM4Missile" , 0 , 16 );	// (%i*250, 0, SRMPackFireStaggered, %obj, %slot, "StreakSRM4Missile"); -soph
      
   %obj.schedule(5000, play3D, "MILSwitchSound");
   %obj.packWeaponTimeout = %time + 6000;	// +soph
}

function SRM4PackImage::onMount(%this,%obj,%slot)
{
     %obj.BAPackID = 9;
     
    if(%obj.client.race $= Bioderm)
      %obj.mountImage(SRM4DImage, 7);
   else
      %obj.mountImage(SRM4Image, 7);

   %obj.srm4active = true;
   if( !%obj.packWeaponTimeout )			// [soph]
      %obj.packWeaponTimeout = getSimTime();
   else
      %obj.weaponPackDisabled = false;			// [/soph]

   schedule(32, %obj, SRM4UpdateAmmoCount, %obj);
}

function SRM4UpdateAmmoCount(%obj)
{
     %obj.client.updateSensorPackText(%obj.getInventory("SRM4Ammo"));
}

function SRM4PackImage::onUnmount(%this,%obj,%slot)
{
     %obj.BAPackID = 0;
   %obj.unmountImage(7);
   %obj.client.updateSensorPackText(0);
   %obj.srm4active = false;   
}

function SRM4FireTimeoutClear(%obj)
{
   if( !%obj.fireTimeoutSRM4 )	// if(%obj.fireTimeoutSRM4 == 1) -soph
     return;
     
//   %obj.fireTimeoutSRM4 = 0;	// -soph
   %obj.play3d(MILSwitchSound);
}

function SRM4PackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;				// _here_ +soph
   %time = getSimTime() ;						// +soph
   if( %obj.weaponPackDisabled || %obj.packWeaponTimeout > %time )	// if(%obj.fireTimeoutSRM4) -soph
      return;

  // %obj.setImageTrigger(0, false);					// moved down -soph

   %weapon = %obj.getMountedImage(0).item ;				// +[soph]
   if( %weapon $= "MissileLauncher" )					// +
   {									// +
      %cluster = false ;						// +
      switch ( %obj.client.mode[ %weapon ] )				// +
      {									// +
            case 0 :							// +
                  %projectile = "ShoulderMissile" ;			// + "StreakSRM4Missile";
                  %nerf = 1 / 2 ;					// + 50 x4 + 100 = 300 
            case 1 :							// +
                  %projectile = "FastMissile" ;				// +
                  %nerf = 1 / 2 ;					// + 32.5 x4 + 65 = 195 
            case 2 :							// +
                  %projectile = "EMPMissile" ;				// +
                  %nerf = 1 / 2 ;					// + 75 x4 + 150 = 450
            case 3 :							// +
                  %projectile = "IncendiaryMissile" ;			// +
                  %nerf = 1 / 2 ;					// + 10 x4 + 20 = 60
            case 4 :							// +
                  %projectile = "MegaMissile" ;  			// +
                  %nerf = 1 / 2 ;					// + 75 x4 + 150 = 450
            case 5 :							// +
                  %projectile = "ShoulderMissile" ;			// +
                  %nerf = 2 / 5 ;					// + 40 x4 + 100 = 260
            case 6 :							// +
                  %projectile = "ShoulderMissile" ;			// +
                  %nerf = 1 / 2 ;					// + 10*4 x4 + 80 = 260
                  %cluster = true ;					// +
      }									// +
      if( !%obj.packFireLink )						// + if no link, create one
      {									// +
         %obj.packFireLink = true ;					// +
         %obj.setImageTrigger( 0 , true ) ;				// +
         %obj.packFireLink = false ;					// +
      }									// +
   }									// +
   else									// +
   {									// +
      %nerf = 1 ;							// +
      %projectile = "StreakSRM4Missile" ;				// +
   }									// +[/soph]
   %obj.setImageTrigger(0, false);

   %shots = %obj.getInventory( "SRM4Ammo" ) ;				// +[soph]
   if( %shots > 4 ) 							// +
      %shots = 4 ;							// +[/soph]

   if( %shots )								// (%obj.getInventory("SRM4Ammo")) -soph
   {
     // %projectile = "StreakSRM4Missile";

      %start = %obj.packFireLink ? 1 : 0 ;					// +[soph]
      if( %cluster )								// +
         for( %i = %start ; %i < %start + %shots ; %i++ )			// +
            schedule(%i*250, 0, SRMPackFireStaggered, %obj, 7, %projectile, %nerf, %i + 1 , %i + 1 );	
      else									// +[/soph]
         for( %i = %start ; %i < %start + %shots ; %i++ )						// for(%i=0;%i<4;%i++) -soph
            schedule(%i*250, 0, SRMPackFireStaggered, %obj, 7, %projectile, %nerf, 0 , %i + 1 );	// schedule(%i*250, 0, SRMPackFireStaggered, %obj, 7, %projectile); -soph 

      %obj.setImageTrigger(7, true);
      %obj.play3d(SRM4FireSound);
      %obj.packWeaponTimeout = %time + 6000;					// %obj.fireTimeoutSRM4 = 6000; -soph
//      schedule(%obj.fireTimeoutSRM4, 0, "SRM4FireTimeoutClear", %obj);	// -soph
   }
   else
   {
      %obj.packWeaponTimeout = %time + 1250;					// %obj.fireTimeoutSRM4 = 1250; -soph
//      schedule(%obj.fireTimeoutSRM4, 0, "SRM4FireTimeoutClear", %obj);	// -soph
      %obj.play3D(MortarDryFireSound);
   }
   
   %obj.setImageTrigger(%slot, false);
   %obj.setImageTrigger(7, false);
}

function SRMPackFireStaggered(%obj, %slot, %proj, %nerf, %num, %spread)		// added spread +soph
{
   if(isObject(%obj))
   {
      %target = %obj.getLockedTarget();						// here now +soph

      %muzzleVec = calcSpreadVector(%obj.getMuzzleVector(%slot), %spread);	// spread 32 -soph
      %muzzlePos = %obj.getMuzzlePoint(%slot);					// +[soph]
      if ( !%target && !%obj.isLocked() )					// +
      {										// +
          %endPos = VectorAdd( %muzzlePos , VectorScale( %muzzleVec , 500 ) );	// +
          %manualTarget = ContainerRayCast( %muzzlePos , %endPos, $TypeMasks::EverythingType, %obj);
          if( %manualTarget && isObject( %manualTarget ) && %manualTarget.getType() & ( $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::TurretObjectType ) )
              if( %manualTarget.getHeat() >= 0.5 && %manualTarget.team != %obj.team )
              {									// +
                  %muzzleVec = calcSpreadVector(%obj.getMuzzleVector(%slot), %spread * 4 );	
                  %target = %manualTarget;					// +
              }									// +
      }										// +[/soph]

      %p = new SeekerProjectile()
      {
         dataBlock        = %proj;
         initialDirection = %muzzleVec;
         initialPosition  = %muzzlePos;						// %obj.getMuzzlePoint(%slot); -soph
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };
      MissionCleanup.add(%p);

      %tether = %obj.tetherBeacon;		 
//      %target = %obj.getLockedTarget();					// moved up -soph

      if(isObject(%tether))
          %p.setObjectTarget( %tether );
      else if(%target)
      {
           if(%target.getDatablock().jetfire)
               assignTrackerTo(%target, %p);
           else
              %p.setObjectTarget( %target );
      }
      else if(%obj.isLocked())
          %p.setPositionTarget(%obj.getLockedPosition());
      else
          %p.setNoTarget();

      %useEnergyObj = %obj.getObjectMount();

      if(!%useEnergyObj)
          %useEnergyObj = %obj;

      %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

      // Vehicle Damage Modifier
      if(%vehicle)
          %p.vehicleMod = %vehicle.damageMod * %nerf;				// %p.vehicleMod = %vehicle.damageMod; -soph

      if(%obj.damageMod)
          %p.damageMod = %obj.damageMod * %nerf;				// %p.damageMod = %obj.damageMod; -soph
      else
          %p.damageMod = 2 / 3;

      if( %num )								// cluster +[soph]
          schedule( 750 + ( %num * 250 ) , 0 , clusterMissileBurst , %data , %obj , %target , %obj.isLocked() ? %obj.getLockedPosition() : false , %p );
										// +[/soph]
      %obj.decInventory("SRM4Ammo", 1);
      %obj.client.updateSensorPackText(%obj.getInventory("SRM4Ammo"));
      %obj.play3d(MILFireSound);
   }
}

function SRM4PackImage::onDeactivate(%data, %obj, %slot)
{
//   %obj.setImageTrigger(7, false);
//	%obj.setImageTrigger(%slot,false);
}

function SRM4Pack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
