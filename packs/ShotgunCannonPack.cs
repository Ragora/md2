if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(ShotgunCannonPackImage)
{
   shapeFile = "ammo_mine.dts";
   item = ShotgunCannonPack;
   mountPoint = 1;
   offset = "0 -0.2 0.3";
   rotation = "0 1 0 180";
   
   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(ShotgunCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "ShotgunCannonPackImage";
	pickUpName = "a shotgun cannon";
};

datablock ShapeBaseImageData(ShotgunCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "-0.35 -0.175 0.25";
   rotation = "0 1 0 45";
   item = ShotgunCannonPack;
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "fire";
//   stateSound[3]                    = ShotgunFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(ShotgunCannonDImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "-0.35 0.1 0.2";
   rotation = "0 1 0 45";
   item = ShotgunCannonPack;
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";
   stateSequence[0]                 = "Deploy";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.1;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = ShotgunFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.075;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
//   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
//   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

function ShotgunCannonPackImage::onMount(%this,%obj,%slot)
{
   if(%obj.client.race $= Bioderm)
      %obj.mountImage(ShotgunCannonDImage, 7);
   else
      %obj.mountImage(ShotgunCannonImage, 7);
}

function ShotgunCannonPackImage::onUnmount(%this,%obj,%slot)
{
   %obj.unmountImage(7);
}

function ShotgunCFireTimeoutClear(%obj)
{
   %obj.fireTimeoutShotgunC = 0;
   %obj.play3D(MortarReloadSound);
}

function ShotgunCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;	// _here_ +soph
   if(%obj.fireTimeoutShotgunC)
      return;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
       %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   %enUse = 10;
   
   if(%obj.powerRecirculator)
      %enUse *= 0.75;
      
   if(%obj.getEnergyLevel() >= %enUse)
   {
      for(%i = 0; %i < 8; %i++)
      {
           %vector = calcSpreadVector(%obj.getMuzzleVector(7), 10);
           
           %p = new LinearFlareProjectile()
           {
              dataBlock        = IndoorTurretBolt;
              initialDirection = %vector;
              initialPosition  = %obj.getMuzzlePoint(7);
              sourceObject     = %obj;
              sourceSlot       = 7;
           };
           MissionCleanup.add(%p);

        // Vehicle Damage Modifier
        if(%vehicle)
          %p.vehicleMod = %vehicle.damageMod;

        if(%obj.damageMod)
          %p.damageMod = %obj.damageMod;
      }
      
      %obj.useEnergy(%enUse);
      %obj.setImageTrigger(7, true);
      %obj.play3D(MortarFireSound);
      %obj.fireTimeoutShotgunC = 750; //-Nite-  increased fire rate  was 1000 //MrKeen - changed it back
      schedule(%obj.fireTimeoutShotgunC, 0, "ShotgunCFireTimeoutClear", %obj);
   }
   else
   {
          %obj.fireTimeoutShotgunC = 750; //-Nite-  increased fire rate  was 1000 //MrKeen - changed it back
          schedule(%obj.fireTimeoutShotgunC, 0, "ShotgunCFireTimeoutClear", %obj);
         
          %obj.play3D(MortarDryFireSound);
          %obj.setImageTrigger(7, false);
          %obj.setImageTrigger(%slot,false);          

          return;
   }
   
   %obj.setImageTrigger(7, false);
   %obj.setImageTrigger(%slot,false);
}

function ShotgunCannonPackImage::onDeactivate(%data, %obj, %slot)
{
//   %obj.setImageTrigger(7, false);
//	%obj.setImageTrigger(%slot,false);
}

function ShotgunCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
