if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// Blaster Gattling Gun

datablock ItemData(LaserCannonGun)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_chaingun.dts";
   image = LaserCannonGunImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a blaster gattling gun";
   emap = true;
};

datablock ShapeBaseImageData(LaserCannonGunImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item = LaserCannonGun;

   projectile = MoonBikeEnergyBullet;
   projectileType = TracerProjectile;
//   enPackDamageBonus = true;		// removing energy pack damage bonus -soph
   
   projectileSpread = 4.5 / 1000.0;
   
   offset = "0 0.2 0.05";
   emap = true;
   usesEnergy = true;
   minEnergy = 4;
   fireEnergy = 4;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = TargetingLaserSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.325;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
//   stateSound[4]            = PulseCannonFire;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.125;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "FireSpindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;	// = 3.5; -soph
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   stateEmitter[6]       = "ChaingunFireSmoke";
   stateEmitterTime[6]       = 0.25;
   stateEmitterNode[6]       = 0;

   //
   stateTimeoutValue[6]        = 3.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";

   //--------------------------------------
   stateName[8]       = "FireSpindown";
   stateSound[8]      = ChaingunSpinDownSound;
   stateSpinThread[8] = SpinDown;
   stateEmitter[8]       = "ChaingunFireSmoke";
   stateEmitterTime[8]       = 0.25;
   stateEmitterNode[8]       = 0;
   //
   stateTimeoutValue[8]            = 3.5;
   stateWaitForTimeout[8]          = false;
   stateTransitionOnTimeout[8]     = "Ready";
   stateTransitionOnTriggerDown[8] = "Spinup";
};

function LaserCannonGunImage::onFire(%data, %obj, %slot)
{
   %p = Parent::onFire(%data, %obj, %slot);

  if(%p)
      %obj.play3D(PulseCannonFire);
}

datablock ShapeBaseImageData(LaserCannonDecalAImage)
{
   shapeFile = "weapon_sniper.dts";
   offset = "0.075 0.3 0.15";
};

datablock ShapeBaseImageData(LaserCannonDecalBImage)
{
   shapeFile = "weapon_mortar.dts";
   offset = "0 -0.2 0.05";
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(LaserCannonDecalCImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   offset = "0.05 0.325 0.225";
   rotation = "1 0 0 90";
};

function LaserCannonGunImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this, %obj, %slot);
   
   %obj.play3D(TargetingLaserSwitchSound);
   %obj.mountImage(LaserCannonDecalAImage, 4);
   %obj.mountImage(LaserCannonDecalBImage, 5);
   %obj.mountImage(LaserCannonDecalCImage, 6);
}

function LaserCannonGunImage::onUnmount(%this,%obj,%slot)
{
   %obj.setImageTrigger(%slot, false);

   %obj.unmountImage(4);
   %obj.unmountImage(5);
   %obj.unmountImage(6);
}

