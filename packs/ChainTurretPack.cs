if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

//--------------------------------------------------------------------------
// 
// Chaingun barrel pack
// 
//--------------------------------------------------------------------------


datablock TurretImageData(ChaingunBarrelLarge)
{
   shapeFile = "turret_tank_barrelchain.dts";
   item      = ChainBarrelPack;

   projectile = TurretChaingunBullet;
   projectileType = TracerProjectile;
   usesEnergy = true;
   fireEnergy = 2;
   minEnergy = 10;
   emap = true;
   projectileSpread = 5 / 1000;

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 5.0;
   
   // Turret parameters
   activationMS         = 1000;
   deactivateDelayMS    = 500;
   thinkTimeMS          = 200;
   degPerSecTheta       = 600;
   degPerSecPhi         = 1080;
   attackRadius         = 140;

   // State transitions
   stateName[0]                     = "Activate";
   stateTransitionOnNotLoaded[0]    = "Dead";
   stateTransitionOnLoaded[0]       = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateSequence[1]                 = "Activate";
   stateSound[1]                    = AASwitchSound;
   stateTimeoutValue[1]             = 1.0;
   stateTransitionOnTimeout[1]      = "Ready";
   stateTransitionOnNotLoaded[1]    = "Deactivate";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";
   stateScript[ 1 ]                 = "onActivateReady" ;	// +soph

   stateName[2]                     = "Ready";
   stateTransitionOnNotLoaded[2]    = "Deactivate";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.032 ;			// = 0.075; -soph
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire_vis";
   stateEjectShell[3]               = true;      
//   stateSound[3]                    = ChainTFireSound;	// -soph
   stateScript[3]                   = "onFire";

   stateName[4]                     = "Reload";
//   stateTimeoutValue[4]             = 0.025;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
//   stateTransitionOnTimeout[4]      = "Ready";
   stateTransitionOnTriggerDown[ 4 ]  = "Fire2" ;		// +soph
   stateTransitionOnTriggerUp[ 4 ]    = "Ready" ;		// +soph
   stateTransitionOnNotLoaded[4]    = "Deactivate";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";

   stateName[5]                     = "Fire2";
   stateTransitionOnTimeout[5]      = "Reload2";
   stateTimeoutValue[5]             = 0.032 ;			// = 0.1; -soph
   stateFire[5]                     = false ;			// = true; -soph
//   stateRecoil[5]                   = LightRecoil;		// -soph
   stateAllowImageChange[5]         = false;
//   stateSequence[5]                 = "Fire_vis";		// -soph
//   stateSound[5]                    = ChainTFireSound;	// -soph
   stateScript[5]                   = "onFire";

   stateName[6]                     = "Reload2";
//   stateTimeoutValue[6]             = 0.15;			// -soph
   stateAllowImageChange[6]         = false;
//   stateSequence[6]                 = "Reload";		// -soph
//   stateTransitionOnTimeout[6]      = "Ready";		// -soph
   stateTransitionOnTriggerDown[ 6 ]  = "Fire" ;		// +soph
   stateTransitionOnTriggerUp[ 6 ]    = "Ready" ;		// +soph
   stateTransitionOnNotLoaded[6]    = "Deactivate";
   stateTransitionOnNoAmmo[6]       = "NoAmmo";

   stateName[7]                     = "Deactivate";
   stateSequence[7]                 = "Activate";
   stateDirection[7]                = false;
   stateTimeoutValue[7]             = 1.0;
   stateTransitionOnLoaded[7]       = "ActivateReady";
   stateTransitionOnTimeout[7]      = "Dead";

   stateName[8]                    = "Dead";
   stateTransitionOnLoaded[8]      = "ActivateReady";

   stateName[9]                    = "NoAmmo";
   stateTransitionOnAmmo[9]        = "Reload";
   stateSequence[9]                = "NoAmmo";
};

datablock ShapeBaseImageData(ChainBarrelPackImage)
{
   mass = 15;

   shapeFile  = "pack_barrel_aa.dts";
   item       = ChainBarrelPack;
   mountPoint = 1;
   offset     = "0 0 0";
	turretBarrel = "ChaingunBarrelLarge";

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";

	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeOut[2] = "Idle";

	isLarge = true;
};

datablock ItemData(ChainBarrelPack)
{
   className    = Pack;
   catagory     = "Packs";
   shapeFile    = "pack_barrel_mortar.dts";
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   rotate       = true;
   image        = "ChainBarrelPackImage";
	pickUpName = "a Chaingun barrel pack";
};

function ChainBarrelPackImage::onActivate(%data, %obj, %slot)
{
	checkTurretMount(%data, %obj, %slot);
}

function xChaingunBarrelLarge::onFire(%data,%obj,%slot)
{
   if(%obj.getEnergyLevel() > %data.fireEnergy)
   {
      %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 9);

      %p = new (%data.projectileType)() {
         dataBlock        = "TurretChaingunBullet";
         initialDirection = %vector;
         initialPosition  = %obj.getMuzzlePoint(%slot);
         sourceObject     = %obj;
         sourceSlot       = %slot;
      };
      MissionCleanup.add(%p);
      %obj.useEnergy(%data.fireEnergy);
   }
}

function ChainBarrelPackImage::onFire(%data,%obj,%slot)
{
      %seed = getRandom(6);
      if(%seed == 0)
         %projectile = "TurretChaingunBullet";
      else if(%seed == 1)
         %projectile = "TurretChaingunBullet1";
      else if(%seed == 2)
         %projectile = "TurretChaingunBullet2";
      else if(%seed == 3)
         %projectile = "TurretChaingunBullet3";
      else if(%seed == 4)
         %projectile = "TurretChaingunBullet4";
      else if(%seed == 5)
         %projectile = "TurretChaingunBullet5";
      else
         %projectile = "TurretChaingunBullet6";

      %vector = %obj.getMuzzleVector(%slot);
      %x = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread;
      %y = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread;
      %z = (getRandom() - 0.5) * 2 * 3.1415926 * %data.projectileSpread;
      %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
      %vector = MatrixMulVector(%mat, %vector);

   %p = new (%data.projectileType)() {
      dataBlock        = %projectile;
      initialDirection = %vector;
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
   };
   MissionCleanup.add(%p);
   %obj.useEnergy(%data.fireEnergy);
}

function ChaingunBarrelLarge::onActivateReady( %data , %obj , %slot )	// +[soph]
{									// +
   %obj.chainBarrel_sequenceState = 9 ;					// +
   %obj.chainBarrel_sequenceTick = 0 ;					// +
   %obj.chainBarrel_sequenceTime = getSimTime() ;			// +
}									// +
									// +
function ChaingunBarrelLarge::onFire( %data , %obj , %slot )		// +
{									// +
   %time = getSimTime() ;						// +
   %tickReduce = mFloor( ( %time - %obj.chainBarrel_sequenceTime ) / 200 ) ;
   if( %tickReduce )							// +
   {									// +
      %obj.chainBarrel_sequenceTick = 0 ;				// +
      %obj.chainBarrel_sequenceState += %tickReduce ;			// +
      if( %obj.chainBarrel_sequenceState > 9 )				// +
         %obj.chainBarrel_sequenceState = 9 ;				// +
   }									// +
   %obj.chainBarrel_sequenceTime = %time ;				// +
   %obj.chainBarrel_sequenceTick -= 2 ;					// + 
   if( %obj.chainBarrel_sequenceTick <= 0 )				// +
   {									// +
      Parent::onFire( %data , %obj , %slot ) ;				// +
      serverPlay3D( ChaingunImpact2 , %obj.getMuzzlePoint( %slot ) ) ;	// +
      %obj.chainBarrel_sequenceTick += %obj.chainBarrel_sequenceState ;	// + 
      if( %obj.chainBarrel_sequenceState > 1.5 )			// +
         %obj.chainBarrel_sequenceState *= 0.96 ;      			// +
      else								// +
         %obj.chainBarrel_sequenceState = 1.5 ;      			// +
   }									// +
}									// +[/soph]

function ChainBarrelPackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger($BackpackSlot, false);
}

function ChainBarrelPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
