if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------

datablock AudioProfile(JetfireTransform)
{
	filename = "fx/powered/inv_pad_off.wav";
	description = AudioDefault3d;
    preload = true;
};

datablock FlyingVehicleData(JetFire) : ShrikeDamageProfile
{
   spawnOffset = "0 0 2";

   catagory = "Vehicles";
   shapeFile = "vehicle_grav_scout.dts";
   multipassenger = false;

   debrisShapeName = "vehicle_grav_scout_debris.dts";
   debris = ShapeDebris;
   renderWhenDestroyed = false;

   drag    = 0.15;
   density = 1.0;

   mountPose[0] = scoutRoot;
   cameraMaxDist = 10.0;
   cameraOffset = 0.7;
   cameraLag = 0.5;
   numMountPoints = 1;
   isProtectedMountPoint[0] = true;
//   mountPose[0] = sitting;	// -soph
   explosion = VehicleExplosion;
	explosionDamage = 1.0;	// = 0.5; -soph
	explosionRadius = 5.0;

   jetFire = 1;
   lightOnly = 1;
   forceSensitive = true;

   maxDamage = 0.75;
   destroyedLevel = 0.75;

   isShielded = true;		// = false; -soph
   energyPerDamagePoint = 250;	// = 75; -soph
   maxEnergy = LightMaleHumanArmor.maxEnergy;      // Afterburner and any energy weapon pool
   minDrag = 30;           // Linear Drag (eventually slows you down when not thrusting...constant drag)
   rotationalDrag = 900;        // Anguler Drag (dampens the drift after you stop moving the mouse...also tumble drag)
   rechargeRate = 0.256;

   maxAutoSpeed = 15;       // Autostabilizer kicks in when less than this speed. (meters/second)
   autoAngularForce = 400;       // Angular stabilizer force (this force levels you out when autostabilizer kicks in)
   autoLinearForce = 300;        // Linear stabilzer force (this slows you down when autostabilizer kicks in)
   autoInputDamping = 0.95;      // Dampen control input so you don't` whack out at very slow speeds
   maxForwardSpeed = 100;

   // Maneuvering
   maxSteeringAngle = 4;    // Max radiens you can rotate the wheel. Smaller number is more maneuverable.
   horizontalSurfaceForce = 8;   // Horizontal center "wing" (provides "bite" into the wind for climbing/diving and turning)
   verticalSurfaceForce = 6;     // Vertical center "wing" (controls side slip. lower numbers make MORE slide.)
   maneuveringForce = 16.4 * 150;      // Horizontal jets (W,S,D,A key thrust)
   steeringForce = 1200;         // Steering jets (force applied when you move the mouse)
   steeringRollForce = 550;      // Steering jets (how much you heel over when you turn)
   rollForce = 4;                // Auto-roll (self-correction to right you after you roll/invert)
   hoverHeight = 2;        // Height off the ground at rest
   createHoverHeight = 2;  // Height off the ground when created

   // Turbo Jet
   jetForce = 32 * 150 * 2.0;      // Afterburner thrust (this is in addition to normal thrust)
   minJetEnergy = 1;     // Afterburner can't be used if below this threshhold.
   jetEnergyDrain = 0.9;       // Energy use of the afterburners (low number is less drain...can be fractional)                                                                                                                                                                                                                                                                                          // Auto stabilize speed
   vertThrustMultiple = 1.5;

   // Rigid body
   mass = 180;        // Mass of the vehicle
   bodyFriction = 0;     // Don't mess with this.
   bodyRestitution = 0.5;   // When you hit the ground, how much you rebound. (between 0 and 1)
   minRollSpeed = 0;     // Don't mess with this.
   softImpactSpeed = 35;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 55;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 40;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.06;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 23.0;
   collDamageMultiplier   = 0.02;

   //
   minTrailSpeed = 999;      // The speed your contrail shows up at.
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = FlyerJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = ArmorJetSound;
   engineSound = ScoutFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 10.0;
   mediumSplashSoundVelocity = 15.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 10.0;

   exitingWater      = VehicleExitWaterMediumSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterMediumSound;
   waterWakeSound    = VehicleWakeMediumSplashSound;

   dustEmitter = VehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 1.0;

   damageEmitter[0] = SmallLightDamageSmoke;
   damageEmitter[1] = SmallHeavyDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "0.0 -1.5 0.5 ";
   damageLevelTolerance[0] = 0.3;
   damageLevelTolerance[1] = 0.7;
   numDmgEmitterAreas = 1;

   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingScoutIcon;
   cmdMiniIconName = "commander/MiniIcons/com_scout_grey";
   targetNameTag = 'Jetfire';
   targetTypeTag = 'Transformer';
   sensorData = VehiclePulseSensor;

   runningLight[0] = ShrikeLight1;

   shieldEffectScale = "0.937 1.125 0.60";
   
   max["ChaingunAmmo"] = 1000;
   numWeapons = 2;
};

datablock ShapeBaseImageData(TurbocatFrontDecal)
{
   className = WeaponImage;
   shapeFile = "TR2weapon_chaingun.dts";
   mountPoint = "10";
   offset = "-0.15 0.9 0";
   rotation = "1 0 0 0";

   ammo = ChaingunAmmo;
   projectile = ChaingunBullet;
   projectileType = TracerProjectile;
   emap = true;

   casing              = ShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 4.0;

   projectileSpread = 6.0 / 1000.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   //
   stateTimeoutValue[3]          = 0.01;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = ChaingunFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.05;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
//   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = false;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
//   stateScript[7]           = "onDryFire";
   stateTransitionOnTimeout[7] = "NoAmmo";
};

datablock ShapeBaseImageData(JFBlasterImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   mountPoint = "10";
   offset = "0 1.0 -0.15";
   rotation = "0 1 0 180";

   projectile = TurboBlasterBolt;
   projectileType = EnergyProjectile;

//   ammo = ChaingunAmmo;
   usesEnergy = true;
   fireEnergy = 3;
   minEnergy = 3;
   
   emap = true;

//   casing              = ShellDebris;
//   shellExitDir        = "1.0 0.3 1.0";
//   shellExitOffset     = "0.15 -0.56 -0.1";
//   shellExitVariance   = 15.0;
//   shellVelocity       = 4.0;

//   projectileSpread = 6.0 / 1000.0; // z0dd - ZOD, 8/6/02. Was: 8.0 / 1000.0
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.3; //0.5
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.2; //0.3
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateSound[3] = BlasterFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

function TurbocatFrontDecal::onFire(%data, %obj, %slot)
{
     Parent::onFire(%data, %obj, %slot);

     %obj.getDatablock().updateAmmoCount(%obj, %obj.getMountNodeObject(0).client, %data.ammo);
}

datablock ShapeBaseImageData(HBDecal1)// : MainAPEImage
{
   offset = "0.8 0 -0.5";
   rotation = calcThisRotD("180 90 0"); //"1 0 0 180";
   shapeFile = "TR2weapon_mortar.dts";   // hjehje

//   stateEmitter[3]       = "BikeContrailEmitter"; // emitter
//   stateEmitterTime[3]       = 0.053; // 100ms
//   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(HBDecal2)// : MainAPEImage
{
   offset = "-0.8 0 -0.275";
   rotation = calcThisRotD("180 -90 0"); //"1 0 0 180";
   shapeFile = "TR2weapon_mortar.dts";   // hjehje

//   stateEmitter[3]       = "BikeContrailEmitter"; // emitter
//   stateEmitterTime[3]       = 0.053; // 100ms
//   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
};

datablock ShapeBaseImageData(HBEngine1)
{
   shapeFile = "turret_assaulttank_plasma.dts";
   offset = "0.53 -0.80 -0.144";
   rotation = calcThisRotD("0 -45 180"); //"0 1 0 -45"; // 90

   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
};

datablock ShapeBaseImageData(HBEngine2)
{
   shapeFile = "turret_assaulttank_plasma.dts";
   offset = "-0.53 -0.80 -0.144";
   rotation = calcThisRotD("0 45 180"); //"0 1 0 45";

   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
};

//datablock ShapeBaseImageData(HBBomberImage)
//{
//   shapeFile = "turret_belly_base.dts";
//};

datablock ShapeBaseImageData(HBEngineParam)
{
   mountPoint = 1;
   shapeFile = "turret_assaulttank_plasma.dts";

   offset = "0 -3.0 0"; //.125 .075 -0.2
   rotation = "0 0 1 180"; //"1 0 45 90";
};

function JetFire::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
  
   %obj.mountImage(TurbocatFrontDecal, 2);
   %obj.mountImage(JFBlasterImage, 4);      
   %obj.mountImage(HBEngineParam, 1);
//   %obj.mountImage(HBBomberImage, 3);
   %obj.mountImage(HBDecal1, 0);
   %obj.mountImage(HBDecal2, 6);
   %obj.mountImage(HBEngine1, 1);
   %obj.mountImage(HBEngine2, 7);
   %obj.selectedWeapon = 1;

//   HBContrailLoop(%obj);

   %obj.weapon[1, Display] = true;
   %obj.weapon[1, Name] = "Turbo Chaingun";
   %obj.weapon[1, Description] = "Uses your Chaingun's ammo; faster fire rate, tighter spread.";	// "Uses your Chaingun for ammo, 200% fire speed."; -soph

   %obj.weapon[2, Display] = true;
   %obj.weapon[2, Name] = "Red Turbo Blaster";
   %obj.weapon[2, Description] = "Uses your energy pool. 30% damage bonus.";
   
   %obj.veh_description = "Your armor converted to Vehicle form. Healthkits and flares still work.";	// will work in JetFire mode."; -soph
}

function JetFire::playerMounted(%data, %obj, %player, %node)
{
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);
   commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
   %obj.owner = %player.client;

   %obj.setInventory("ChaingunAmmo", %player.getInventory("ChaingunAmmo"));
   %player.client.setWeaponsHudActive("Chaingun");
   %data.schedule(32, updateAmmoCount, %obj, %player.client, "ChaingunAmmo");

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function JetFire::onTrigger(%data, %obj, %trigger, %state)
{
     switch( %trigger )
     {
          case 0:
               if(%obj.selectedWeapon == 1)
               {
                    %obj.setImageTrigger(4, false);

                    switch(%state)
                    {
                        case 0:
                        %obj.fireWeapon = false;
                        %obj.setImageTrigger(2, false);
                        case 1:
                        %obj.fireWeapon = true;
                        %obj.setImageTrigger(2, true);
                    }
               }
               else
               {
                    %obj.setImageTrigger(2, false);

                    switch(%state)
                    {
                        case 0:
                        %obj.fireWeapon = false;
                        %obj.setImageTrigger(4, false);
                        case 1:
                        %obj.fireWeapon = true;
                        %obj.setImageTrigger(4, true);
                    }
               }
          case 4:	// +[soph]
               %player = %obj.getMountNodeObject( 0 );
               if ( %state == 1 )
                    %player.grenTimer = 1;
               else
               {
                    if ( %player.grenTimer != 0 )
                    {
                         if( %player.inv[FlareGrenade] )
                         {
                              %player.decInventory( FlareGrenade , 1 );	// +soph
                              %f = new FlareProjectile()
                              {
                                     dataBlock        = FlareGrenadeProj;
                                     initialDirection = getRandomT() SPC getRandomT() SPC getRandomT();
                                     initialPosition  = %obj.getWorldBoxCenter();
                                     sourceObject     = %obj;
                                     sourceSlot       = 0;
                              };
                              FlareSet.add(%f);
                              MissionCleanup.add(%f);
                              %f.schedule(10000, "delete");
                         }
                         %player.grenTimer = 0;
                         bottomPrint( %player.client , "Flare released." SPC %player.inv[ FlareGrenade ] SPC "flares remaining." , 3 , 1 ) ;
                    }
               }	// +[/soph]
     }
}

function HBBobmerImage::onFire(%data,%obj,%slot)
{
   %vector = calcSpreadVector(%obj.getMuzzleVector(%slot), 1);

   %p = new LinearProjectile() {
      dataBlock        = "BikeFlareBomb";
      initialDirection = %vector;
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
      vehicleObject    = %vehicle;
   };
   MissionCleanup.add(%p);
//   FlareSet.add(%p);

   %obj.useEnergy(%data.fireEnergy);
}

function BikeFlareBomb::onExplode(%data, %proj, %pos, %mod)
{
     %projectile = "FlareGrenadeProj";
     %projectileType = "FlareProjectile";

     for(%i = 0; %i < 4; %i++)
     {
           %projectileSpread = 2.88;
           %vector = "1 2 1";  // 00-1 it was
           %x = (getRandom() - 0.5) * 2 * 3.1415926 * %projectileSpread;
           %y = (getRandom() - 0.5) * 2 * 3.1415926 * %projectileSpread;
           %z = (getRandom() - 0.5) * 2 * 3.1415926 * %projectileSpread;
           %mat = MatrixCreateFromEuler(%x @ " " @ %y @ " " @ %z);
           %vector = MatrixMulVector(%mat, %vector);

           %p = new (%projectileType)()
           {
              dataBlock        = %projectile;
              initialDirection = %vector;
              initialPosition  = %pos;
              sourceObject     = %proj.sourceObject;
              sourceSlot       = -1;
              vehicleObject    = 0;
           };

           MissionCleanup.add(%p);
     }
     
     Parent::onExplode(%data, %proj, %pos, %mod);
}

function JetFire::playerDismounted(%data, %obj, %player)
{
   %obj.fireWeapon = false;
   %obj.setImageTrigger(2, false);
   setTargetSensorGroup(%obj.getTarget(), %obj.team);
   jetfireDisable(%player, %obj);
   %player.setInventory("ChaingunAmmo", %obj.getInventory("ChaingunAmmo")); 
}

function cantJetfire(%pl)
{
     InitContainerRadiusSearch(%pl.getPosition(), 3, $TypeMasks::InteriorObjectType);

     while ((%int = ContainerSearchNext()) != 0)
     {
         %subStr = getSubStr(%int.interiorFile, 1, 4);
         if(%subStr !$= "rock" && %subStr !$= "spir" && %subStr !$= "misc")
            return true;
     }
}

function JetFire::deleteAllMounted(%data, %obj)
{
   %decal = %obj.getMountNodeObject(1);
   if(!%decal)
      return;

   %decal.delete();
}

function jetfireEnable(%pl)
{
  if(!cantJetfire(%pl))
  {
      if(%pl.transvmount && checkTimeExceeded(%pl.transvmount, 5000))
         return;

      if(%pl.isMounted())
      {
         messageClient(%pl.client, 'MsgCantJBug1', '\c2You cannot transform while in a vehicle.');
         return;
      }

     ejectFlag(%pl);
     %pl.startFade(100, 0, true);
     messageClient(%pl.client, 'MsgJetfireTransV', '\c2JetFire Transform -> Vehicle Mode');
     
//     %pl.inJetfire = true;
     
      %vehicle = new FlyingVehicle()
      {
         dataBlock  = "JetFire";
         respawn    = "0";
         teamBought = %pl.client.team;
         team = %pl.client.team;
      };
      
      %pl.soulshieldException = true;
      %vel = %pl.getVelocity();
      %vehicle.setTransform(modifyTransform(%pl.getTransform(), "0 0 1.5 0 0 0 0"));
      %pl.getDataBlock().onCollision(%pl, %vehicle, 0);
      %data = %vehicle.getDatablock();
      serverPlay3D("JetfireTransform", %pl.getTransform());

     %dmg = getDamagePercent(%pl.getDatablock().maxDamage, %pl.getDamageLevel());
     %vehicle.setDamageLevel(%dmg * %vehicle.getDatablock().maxDamage);
      %iVec = vectorScale(%vel, %vehicle.getMass());
      %vehicle.applyImpulse(%vehicle.getTransform(), %iVec);

   if((%data.sensorData !$= "") && (%vehicle.getTarget() != -1))
      setTargetSensorData(%vehicle.getTarget(), %data.sensorData);
   %vehicle.setRechargeRate(%data.rechargeRate);
   %vehicle.setEnergyLevel(%pl.getEnergylevel());

   switch$( %pl.getMountedImage( $BackpackSlot ).item )		// +[soph]
   {
      case "EnergyPack":
         %vehicle.setRechargeRate( %vehicle.getRechargeRate() + 0.23 ) ;

      case "ShieldPack":
         %vehicle.shieldStrengthFactor = 1/3 ;

      case "RepairPack":
         vSubspaceModTick( %vehicle.getDatablock() , %vehicle ) ;

      case "SensorJammerPack":
         setTargetSensorData( %vehicle.getTarget() , JammerSensorObjectActive ) ;
         %vehicle.setJammerFX( true ) ;
         %vehicle.isJammed = true ;
         %vehicle.jammerState = true ;

      case "SynomiumPack":
         if( !%pl.isSyn )
         {
            %vehicle.play3D( SynomiumEngage ) ;
            %vehicle.playShieldEffect( "0 0 1" ) ;
         }
         %vehicle.startSyn = true ;
         %vehicle.setRechargeRate( %vehicle.getRechargeRate() + 0.15 ) ;
         synPhase2v( %vehicle ) ;
   }								// +[/soph]

   if(%vehicle.mountable || %vehicle.mountable $= "")
      %data.isMountable(%vehicle, true);
   else
      %data.isMountable(%vehicle, false);

     %vehicle.setSelfPowered();
     %vehicle.damageMod = %pl.damageMod;

     %vehicle.team = %pl.client.Team;	// used nonexistant %plyr -soph
     %vehicle.owner = %pl.client;	// used nonexistant %plyr -soph
     MissionCleanup.add(%vehicle);
     
     jfPilotCheck(%vehicle);
   }
   else
        messageClient(%pl.client, 'MsgCantDeployNearBuilding', '\c2You cannot transform on, near or in a building or structure.');
}

function jetfireDisable(%pl, %vehicle)
{
   checkTrackerObj(%vehicle, %pl);

   if(!%vehicle.prevDest)
   {
     %pl.startFade(100, 0, false);
     %pl.setTransform(%vehicle.getTransform());
     %pl.setEnergyLevel(%vehicle.getEnergyLevel());
     %dmg = getDamagePercent( %vehicle.getDatablock().maxDamage , %vehicle.getDamageLevel() );	// -soph
     %pl.setDamageLevel( %dmg * %pl.getDatablock().maxDamage );					// -soph

//     %pl.inJetfire = false;
     
//     %vehicle.setTransform("10000 10000 10000 1 0 0 0");
//     %vehicle.applyDamage(10000);
//     %vehicle.delete();
     %vehicle.getDatablock().deleteAllMounted(%vehicle);
     schedule(100, %vehicle, setPosition, %vehicle, vectorAdd(%vehicle.position, vectorAdd(getRandom(1000) SPC getRandom(1000) SPC "0", "40 -27 10000")));
     %vehicle.schedule(1000, "delete");
     serverPlay3D("JetfireTransform", %pl.getTransform());
     %pl.setImageTrigger(2,false);
     messageClient(%pl.client, 'MsgJetfireTransB', '\c2JetFire Transform -> Bipedal Mode');
     %pl.soulshieldException = false;
     %pl.transvmount = getSimTime();
   }
}

function jfPilotCheck(%obj)
{
     if(isObject(%obj))
     {
          %pilot = %obj.getMountNodeObject(0);							// +soph
          if( %pilot )										// if(%obj.getMountNodeObject(0)) -soph
          {											// +[soph]
               %dmg = getDamagePercent( %obj.getDatablock().maxDamage , %obj.getDamageLevel() );
               %pilot.setDamageLevel( %dmg * %pilot.getDatablock().maxDamage );			// +[/soph]
               schedule(1000, %obj, jfPilotCheck, %obj);
          }
          else
               %obj.delete();
     }
}

function JetfirePackImage::onActivate(%data, %obj, %slot)
{
  if(!cantJetfire(%obj) && !%obj.jM)
  {
        %obj.jM = true;
        jetfireEnable(%obj);
  }
  else
  {
        messageClient(%obj.client, 'MsgCantDeployNearBuilding10', '\c2You cannot deploy a jetfire on/near/in a building for at least 5m');
        %obj.setImageTrigger(%slot,false);
  }
}

function JetfirePackImage::onDeactivate(%data, %obj, %slot)
{
	%obj.setImageTrigger(%slot,false);
   %obj.jM = false;
}