if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

datablock ShapeBaseImageData(DualGrenadeCannonPackImage)
{
   shapeFile = "turret_belly_base.dts";
   item = DualGrenadeCannonPack;
   mountPoint = 1;
   offset = "0 0 -0.2";
   rotation = "75 0 0 1";

   usesEnergy = true;
   minEnergy = -1;

	stateName[0] = "Idle";
	stateTransitionOnTriggerDown[0] = "Activate";
     stateSequence[0] = "activate";
   	
	stateName[1] = "Activate";
	stateScript[1] = "onActivate";
	stateTransitionOnTriggerUp[1] = "Deactivate";
   stateTransitionOnNoAmmo[1] = "Deactivate";

	stateName[2] = "Deactivate";
	stateScript[2] = "onDeactivate";
	stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(DualGrenadeCannonPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   image = "DualGrenadeCannonPackImage";
	pickUpName = "a grenade cannon";
};

// ------------------------------------------------------------------
// Shoulder Guns

datablock ShapeBaseImageData(DualGrenadeCannon2DImage)
{
//   className = WeaponImage;
   shapeFile = "weapon_grenade_launcher.dts";
   item = DualGrenadeCannonPack;
   offset = "-0.5 0 0.35";
//   rotation = "0 1 0 180";
   emap = true;
   mountPoint = 1;

   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

//   projectile = BomberFusionBolt;
//   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.75;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = DaishiCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(DualGrenadeCannonDImage) : DualGrenadeCannon2DImage
{
   offset = "0.5 0 0.35";
//   stateSequence[2]                 = "Deploy";
//   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(DualGrenadeCannon2Image)
{
//   className = WeaponImage;
   shapeFile = "weapon_grenade_launcher.dts"; //"TR2weapon_mortar.dts";
   offset = "-0.6 -0.125 0.35";
//   rotation = "0 1 0 -155";
   emap = true;
   mountPoint = 1;
   item = DualGrenadeCannonPack;
   
   usesEnergy = true;
   fireEnergy = -1;
   minEnergy = -1;

//   projectile = BomberFusionBolt;
//   projectileType = LinearFlareProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
//   stateSequence[2]                 = "Deploy";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.75;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
//   stateSound[3]                    = DaishiCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.25;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MortarDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(DualGrenadeCannonImage) : DualGrenadeCannon2Image
{
   offset = "0.6 -0.125 0.35";
//   rotation = "0 1 0 155";
   
//   stateSequence[2]                 = "Deploy";
//   stateScript[3] = "onFire";
};

datablock ShapeBaseImageData(DualGrenadeCannonHHImage) : DualGrenadeCannonImage
{
   item = "";	// +soph
//   usesEnergy = true;
//   fireEnergy = 34;
//   minEnergy = 34;

   shapeFile = "TR2weapon_grenade_launcher.dts";
   offset = "0 -0.5 0.25";
   rotation = calcThisRotD("180 0 180"); //"1 0 0 180";
   emap = true;
   mountPoint = 0;

   projectile = DualCannonGrenade ;	// GrenadeRocket; -soph
   projectileType = GrenadeProjectile ;	// LinearProjectile; -soph

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "deploy";
   stateScript[3]                   = "onFire";
//   stateDirection[3]                = true;
//   stateSound[3]                    = DualGrenadeCannonFire;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 4.0;
   stateAllowImageChange[4]         = false;
//   stateSequence[4]                 = "Reload";
};

function DualGrenadeCannonHHImage::onMount(%this,%obj,%slot)
{
//     Parent::onMount(%this,%obj,%slot);

   %obj.unmountImage(1);
   %obj.unmountImage(7);
   %obj.weaponPackDisabled = true;	// %obj.fireTimeoutGrenadeC = true; -soph
   %obj.play3D("MechGunActivate");
}

function DualGrenadeCannonHHImage::throwWeapon( %this ) {}		// +soph

function DualGrenadeCannonHHImage::onUnmount(%this,%obj,%slot)
{
//     Parent::onUnmount(%this,%obj,%slot);

   if(%obj.client.race $= Bioderm)
   {
      %obj.mountImage(DualGrenadeCannonDImage, 1);
      %obj.mountImage(DualGrenadeCannon2DImage, 7);
   }
   else
   {
      %obj.mountImage(DualGrenadeCannonImage, 1);
      %obj.mountImage(DualGrenadeCannon2Image, 7);
   }

//   %obj.fireTimeoutGrenadeC = false;				// -soph
   %obj.weaponPackDisabled = false;				// +soph
//   %obj.setImageTrigger($BackpackSlot, false);		// -soph
   %obj.setImageTrigger(1, false);
   %obj.setImageTrigger(7, false);
}

function DualGrenadeCannonHHImage::onFire(%data, %obj, %slot)
{
     if(%obj.gpackType $= "")
     {
          %obj.play3D(MortarDryFireSound);
          return;
     }

     %grenType = %obj.gpackType;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;

   if(%obj.getInventory(%grenType) > 1)
   {
     %p = new LinearProjectile()
     {
          dataBlock        = "GrenadeRocket";
          initialDirection = %obj.getMuzzleVector(%slot);
          initialPosition  = %obj.getMuzzlePoint(%slot);
          sourceObject     = %obj;
          sourceSlot       = %slot;
          starburstMode    = %grenType;
          damageMod        = %obj.damageMod;
          vehicleMod       = %obj.vehicleMod;
          tri              = true;
     };
     MissionCleanup.add(%p);
     %p.lastReflectedFrom = %obj;

     %obj.decInventory(%grenType, 2);
     %obj.play3D(MILFireSound);
     %obj.schedule(4000, play3D, "BomberBombDryFireSound");
     %obj.client.updateSensorPackText(%obj.getInventory(%obj.gpackType));
     %obj.packWeaponTimeout = %time + 5000;			// +soph
   }
   else
   {
      %obj.play3D(MortarDryFireSound);
      return;
   }
}

function DualGrenadeCannonPackImage::onMount(%this,%obj,%slot)
{
   %obj.BAPackID = 11;
   
   if(%obj.client.race $= Bioderm)
   {
      %obj.mountImage(DualGrenadeCannonDImage, 1);
      %obj.mountImage(DualGrenadeCannon2DImage, 7);
   }
   else
   {
      %obj.mountImage(DualGrenadeCannonImage, 1);
      %obj.mountImage(DualGrenadeCannon2Image, 7);
   }
   
   if( !%obj.packWeaponTimeout )				// +[soph]
      %obj.packWeaponTimeout = getSimTime() ;			// +
   else								// +
      %obj.weaponPackDisabled = false;				// +[/soph]
   schedule(32, %obj, DGPUpdateAmmoCount, %obj);
}

function DGPUpdateAmmoCount(%obj)
{
   if(%obj.getInventory("FireGrenade"))
       %grenType = "FireGrenade";
   else if(%obj.getInventory("EMPGrenade"))
       %grenType = "EMPGrenade";
   else if(%obj.getInventory("Grenade"))
       %grenType = "Grenade";
   else if(%obj.getInventory("PoisonGrenade"))
       %grenType = "PoisonGrenade";
   else if(%obj.getInventory("ConcussionGrenade"))
       %grenType = "ConcussionGrenade";
   else if(%obj.getInventory("FlashGrenade"))
       %grenType = "FlashGrenade";
   else if(%obj.mortarNadeFitting && %obj.getInventory("MortarGrenade"))
       %grenType = "MortarGrenade";       
  // else							// -soph
  //     return;						// -soph
   if( %grenType !$= "" )    					// +soph
      %obj.gpackType = %grenType;
   GrenadeCAmmoUpdate(%obj);
}

function DualGrenadeCannonPackImage::onUnmount(%this,%obj,%slot)
{
     %obj.BAPackID = 0;
   %obj.gpackType = "";
   %obj.unmountImage(1);
   %obj.unmountImage(7);
}

function DualGrenadeCannonPackImage::onActivate(%data, %obj, %slot)
{
   %obj.setImageTrigger( %slot , false ) ;				// _here_ +soph
   %time = getSimTime();						// +soph
   if( %obj.weaponPackDisabled || %obj.packWeaponTimeout > %time )	// if(%obj.fireTimeoutGrenadeC) -soph
      return;

   if(%obj.invPackBlockTime > %time )					// if(%obj.invPackBlockTime > getSimTime()) -soph
      return;

     if(%obj.gpackType $= "")
     {
          %obj.play3D(MortarDryFireSound);
          return;
     }

     %grenType = %obj.gpackType;

   %useEnergyObj = %obj.getObjectMount();

   if(!%useEnergyObj)
        %useEnergyObj = %obj;

   %vehicle = %useEnergyObj.getType() & $TypeMasks::VehicleObjectType ? %useEnergyObj : 0;
   
   if(%obj.getInventory(%grenType) > 1)
   {
     %obj.setImageTrigger(1, true);
     %obj.setImageTrigger(7, true);
     
     %p = new GrenadeProjectile()					// LinearProjectile() -soph
     {
          dataBlock        = DualCannonGrenade ;			// "GrenadeRocket"; -soph
          initialDirection = %obj.getEyeVector() ;			// %obj.getMuzzleVector(1); -soph
          initialPosition  = %obj.getMuzzlePoint(1);
          sourceObject     = %obj;
          sourceSlot       = 1;
          starburstMode    = %grenType;
          damageMod        = %obj.damageMod + 0.15;
          vehicleMod       = %obj.vehicleMod;
          tri              = false;
     };
     MissionCleanup.add(%p);
     %p.lastReflectedFrom = %obj;
     
     %p = new GrenadeProjectile()					// LinearProjectile() -soph
     {
          dataBlock        = DualCannonGrenade ;			// "GrenadeRocket"; -soph
          initialDirection = %obj.getEyeVector() ;			// %obj.getMuzzleVector(7); -soph
          initialPosition  = %obj.getMuzzlePoint(7);
          sourceObject     = %obj;
          sourceSlot       = 7;
          starburstMode    = %grenType;
          damageMod        = %obj.damageMod + 0.15;
          vehicleMod       = %obj.vehicleMod;
          tri              = false;
     };
     MissionCleanup.add(%p);
     %p.lastReflectedFrom = %obj;
     
     %obj.applyKick(-2500);

     %obj.setImageTrigger(3, false);
     %obj.setImageTrigger(7, false);
     %obj.decInventory(%grenType, 2);
     %obj.play3D(MILFireSound);
     %obj.packWeaponTimeout = getSimTime() + 5000;			// %obj.fireTimeoutGrenadeC = 5000; -soph
//     schedule(%obj.fireTimeoutGrenadeC, 0, "GrenadeCFireTimeoutClear", %obj);	// -soph
   }
   else
   {
          %obj.play3D(MortarDryFireSound);
          return;
   }

   %obj.setImageTrigger(%slot, false);
}

function DualCannonGrenade::onExplode( %data , %proj , %pos , %mod )	// +[soph]
{									// +
     handleGrenadeRocketExplode( %data , %proj , %pos , %mod ) ;	// +
}									// +[/soph]

function GrenadeRocket::onExplode(%data, %proj, %pos, %mod)
{
     handleGrenadeRocketExplode(%data, %proj, %pos, %mod);
}

function GrenadeCannonRocket::onExplode(%data, %proj, %pos, %mod)
{
     handleGrenadeRocketExplode(%data, %proj, %pos, %mod);
}

function handleGrenadeRocketExplode(%data, %proj, %pos, %mod)
{
   if(%proj.bkSourceObject)
      if(isObject(%proj.bkSourceObject))
         %proj.sourceObject = %proj.bkSourceObject;

   %modifier = 1;
   %gren = %proj.starburstMode.thrownItem;
   
   if(!isObject(%proj) || !isObject(%proj.sourceObject))
       return;

   if(%proj.tri)
     %loop = 2 ;							// 3; +soph
   else
     %loop = 1;
     
   for(%i = 0; %i < %loop; %i++)
   {
        %item = new Item()
        {
           dataBlock = %gren;
           rotation = "0 0 1 0";
           static = false;
           sourceObject = %proj.sourceObject;
           damageMod        = %proj.damageMod;
           vehicleMod       = %proj.vehicleMod;
        };

        if(!isObject(%item))
          return;

        %item.setPosition(%pos);
        MissionCleanup.add(%item);
        AIGrenadeThrown(%item);

        if(%gren $= "EMPGrenadeThrown")
           %item.empGren = true;

        if(%gren $= "ConcussionGrenadeThrown")
           %item.concNade = true;

        if(%gren $= "FlashGrenadeThrown")
           %item.flashNade = true;

        if(%gren $= "FireGrenadeThrown")	// +soph
           %item.fireNade = true;		// +soph

        %item.detThread = schedule( 128 + ( %i * 32 ) , %item, CannonDetGrenade, %item);	// was 150 -soph
   }
}

function CannonDetGrenade(%obj)
{
   if(!isObject(%obj))
       return;
     
   %obj.setDamageState(Destroyed);
   %data = %obj.getDataBlock();

   if(%obj.empGren)
        EMPBurstExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage * %obj.damageMod, %data.kickBackStrength * %obj.damageMod, %obj.sourceObject, %data.radiusDamageType);
   else if(%obj.concNade)
   {
        %obj.triggerConcussion = true;
        RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage * %obj.damageMod, %data.kickBackStrength * %obj.damageMod, %obj.sourceObject, %data.radiusDamageType);
   }
   else if(%obj.flashNade)
        detonateFlashGrenade(%obj);
   else if(%obj.fireNade)			// +[soph]
   {
        RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage * %obj.damageMod, %data.kickBackStrength * %obj.damageMod, %obj.sourceObject, %data.radiusDamageType);
        IncendiaryExplosion( %obj.getPosition() , %data.damageRadius , 5 , %obj.sourceObject );
   }						// +[/soph]
   else
        RadiusExplosion(%obj, %obj.getPosition(), %data.damageRadius, %data.indirectDamage * %obj.damageMod, %data.kickBackStrength * %obj.damageMod, %obj.sourceObject, %data.radiusDamageType);

   %obj.schedule(32, startFade, 150, 0, true);
   %obj.schedule(500, delete);
}

function DualGrenadeCannonPackImage::onDeactivate(%data, %obj, %slot)
{
//     %obj.setImageTrigger(7, false);
//     %obj.setImageTrigger(%slot, false);
}

function DualGrenadeCannonPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
