if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// ------------------------------------------------------------------
// Gravitron Armor Pack

datablock ShapeBaseImageData(GravArmorPlatePackImage)
{
//   shapeFile = "pack_barrel_fusion.dts";
   shapeFile = "turret_muzzlepoint.dts";
   item = GravArmorPlatePack;
   mountPoint = 1;
   mass = -20;
   rotation = "0 1 0 180";
};

datablock ItemData(GravArmorPlatePack)
{
   className = ArmorMod;
   catagory = "ArmorMod";
   shapeFile = "pack_barrel_fusion.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

   image = "GravArmorPlatePackImage";
	pickUpName = "a gravitron armor plate";
};

function GravArmorPlatePackImage::onMount(%data, %obj, %node)
{
//     %obj.universalResistFactor = 1.5;
//     %obj.armorPlate = true;
//     schedule(32, 0, gravUpdateText, %obj);
}

function GravArmorPlatePackImage::onUnmount(%data, %obj, %node)
{
//     %obj.universalResistFactor = 1.0;
//     %obj.armorPlate = false;
//     commandToClient(%obj.client, 'updatePackText', 0);
}

function GravArmorPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
