//--------------------------------------------------------------------------
// Repair Pack
// can be used by any armor type
// when activated, gives user a "repair gun" that can be used to
// repair a damaged object or player. If there is no target in
// range for the repair gun, the user is repaired.

//--------------------------------------------------------------------------
// Sounds & feedback effects

datablock AudioProfile(RepairPackActivateSound)
{
   filename    = "fx/packs/packs.repairPackOn.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(RepairPackFireSound)
{
   filename    = "fx/packs/repair_use.wav";
   description = CloseLooping3d;
   preload = true;
};

datablock AudioProfile(ReassemblerFireSound)
{
   filename    = "fx/vehicles/wake_shrike_n_tank.wav";
   description = CloseLooping3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile

datablock RepairProjectileData(DefaultRepairBeam)
{
   sound = RepairPackFireSound;

   beamRange      = 10;
   beamWidth      = 0.15;
   numSegments    = 20;
   texRepeat      = 0.20;
   blurFreq       = 10.0;
   blurLifetime   = 1.0;
   cutoffAngle    = 25.0;
   
   textures[0]   = "special/redbump2";
   textures[1]   = "special/redflare";
};

datablock RepairProjectileData(ReassemblerRepairBeam)
{
   sound = ReassemblerFireSound;

   beamRange      = 20;
   beamWidth      = 0.15;
   numSegments    = 20;
   texRepeat      = 0.20;
   blurFreq       = 10.0;
   blurLifetime   = 1.0;
   cutoffAngle    = 35.0;

   textures[0]   = "special/ELFBeam";
   textures[1]   = "special/BlueImpact";
};

//datablock RepairProjectileData(NosferatuRepairBeam)	// replaced -[soph]
//{
//   sound = RepairPackFireSound;
//
//   beamRange      = 50;
//   beamWidth      = 0.2;
//   numSegments    = 25;
//   texRepeat      = 0.20;
//   blurFreq       = 20.0;
//   blurLifetime   = 0.5;
//   cutoffAngle    = 45.0;
//
//   textures[0]   = "liquidTiles/LushWater01";
//   textures[1]   = "special/flare";
//};							// -[/soph]

//-------------------------------------------------------------------------
// shapebase datablocks

datablock ShapeBaseImageData(RepairPackImage)
{
   shapeFile = "pack_upgrade_repair.dts";
   item = RepairPack;
   mountPoint = 1;
   offset = "0 0 0";
   emap = true;

   gun = RepairGunImage;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateSequence[1] = "fire";
   stateSound[1] = RepairPackActivateSound;
   stateTransitionOnTriggerUp[1] = "Deactivate";

   stateName[2] = "Deactivate";
   stateScript[2] = "onDeactivate";
   stateTransitionOnTimeout[2] = "Idle";   
};

datablock ItemData(RepairPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_repair.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "RepairPackImage";
   pickUpName = "a repair pack";

   lightOnlyStatic = true;
   lightType = "PulsingLight";
   lightColor = "1 0 0 1";
   lightTime = 1200;
   lightRadius = 4;

   computeCRC = true;
   emap = true;
};

//--------------------------------------------------------------------------
// Repair Gun

datablock ShapeBaseImageData(RepairGunImage)
{
   shapeFile = "weapon_repair.dts";
   offset = "0 0 0";

   usesEnergy = true;
   minEnergy = 3;
   cutOffEnergy = 3.1;
   emap = true;

   repairFactorPlayer = 0.002; // <--- attention DaveG!
   repairFactorObject = 0.005;	// = 0.004 -soph // <--- attention DaveG!

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.25;

   stateName[1] = "ActivateReady";
   stateScript[1] = "onActivateReady";
   stateSpinThread[1] = Stop;
   stateTransitionOnAmmo[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "ActivateReady";

   stateName[2] = "Ready";
   stateSpinThread[2] = Stop;
   stateTransitionOnNoAmmo[2] = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Validate";

   stateName[3] = "Validate";
   stateTransitionOnTimeout[3] = "Validate";
   stateTimeoutValue[3] = 0.2;
   stateEnergyDrain[3] = 3;
   stateSpinThread[3] = SpinUp;
   stateScript[3] = "onValidate";
   stateIgnoreLoadedForReady[3] = true;
   stateTransitionOnLoaded[3] = "Repair";
   stateTransitionOnNoAmmo[3] = "Deactivate";
   stateTransitionOnTriggerUp[3] = "Deactivate";

   stateName[4] = "Repair";
   stateSound[4] = RepairPackFireSound;
   stateScript[4] = "onRepair";
   stateSpinThread[4] = FullSpeed;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "activate";
   stateFire[4] = true;
   stateEnergyDrain[4] = 9;
   stateTimeoutValue[4] = 0.2;
   stateTransitionOnTimeOut[4] = "Repair";
   stateTransitionOnNoAmmo[4] = "Deactivate";
   stateTransitionOnTriggerUp[4] = "Deactivate";
   stateTransitionOnNotLoaded[4] = "Validate";

   stateName[5] = "Deactivate";
   stateScript[5] = "onDeactivate";
   stateSpinThread[5] = SpinDown;
   stateSequence[5] = "activate";
   stateDirection[5] = false;
   stateTimeoutValue[5] = 0.2;
   stateTransitionOnTimeout[5] = "ActivateReady";
};

function RepairPackImage::onMount(%data, %obj, %node)
{
//   Parent::onMount(%data, %obj, %node);
   %obj.repairKitFactor = 3.0;
     %obj.BAPackID = 2;
}

function RepairPackImage::onUnmount(%data, %obj, %node)
{
   // dismount the repair gun if the player had it mounted
   // need the extra "if" statement to avoid a console error message
   if(%obj.getMountedImage($WeaponSlot))
      if(%obj.getMountedImage($WeaponSlot).getName() $= "RepairGunImage")
         %obj.unmountImage($WeaponSlot);
   // if the player was repairing something when the pack was thrown, stop repairing it
   if(%obj.repairing != 0)
      stopRepairing(%obj);
      
   %obj.repairKitFactor = 1.0;
     %obj.BAPackID = 0;
}
  
function RepairPackImage::onActivate(%data, %obj, %slot)
{
   // don't activate the pack if player is piloting a vehicle
   if(%obj.isPilot())
   {
      %obj.setImageTrigger(%slot, false);
      return;
   }

   if(!isObject(%obj.getMountedImage($WeaponSlot)) || %obj.getMountedImage($WeaponSlot).getName() !$= "RepairGunImage")
   {
      messageClient(%obj.client, 'MsgRepairPackOn', '\c2Repair pack activated.');

      // make sure player's arm thread is "look"
      %obj.setArmThread(look);

      // mount the repair gun
      %obj.mountImage(RepairGunImage, $WeaponSlot);
      // clientCmdsetRepairReticle found in hud.cs
      commandToClient(%obj.client, 'setRepairReticle');
   }
}

function RepairPackImage::onDeactivate(%data, %obj, %slot)
{
   //called when the player hits the "pack" key again (toggle)
   %obj.setImageTrigger(%slot, false);
   // if repair gun was mounted, unmount it
   if(%obj.getMountedImage($WeaponSlot).getName() $= "RepairGunImage")
      %obj.unmountImage($WeaponSlot);
}

function RepairGunImage::onMount(%this,%obj,%slot)
{
   %obj.setImageAmmo(%slot,true);
   if ( !isDemo() )
      commandToClient( %obj.client, 'setRepairPackIconOn' );
}

function RepairGunImage::onUnmount(%this,%obj,%slot)
{
   // called when player switches to another weapon

   // stop repairing whatever player was repairing
   if(%obj.repairing)
      stopRepairing(%obj);

   %obj.setImageTrigger(%slot, false);
   // "turn off" the repair pack -- player needs to hit the "pack" key to
   // activate the repair gun again
   %obj.setImageTrigger($BackpackSlot, false);
   if ( !isDemo() )
      commandToClient( %obj.client, 'setRepairPackIconOff' );
}

function RepairGunImage::onActivateReady(%this,%obj,%slot)
{
   %obj.errMsgSent = false;
   %obj.selfRepairing = false;
   %obj.repairing = 0;
   %obj.setImageLoaded(%slot, false);
}

function RepairGunImage::onValidate(%this,%obj,%slot)
{
   // this = repairgunimage datablock
   // obj = player wielding the repair gun
   // slot = weapon slot

   if(%obj.getEnergyLevel() <= %this.cutOffEnergy)
   {
      stopRepairing(%obj);
      return;
   }
   %repGun = %obj.getMountedImage(%slot);
   // muzVec is the vector coming from the repair gun's "muzzle"
   %muzVec = %obj.getMuzzleVector(%slot);
   // muzNVec = normalized muzVec
   %muzNVec = VectorNormalize(%muzVec);
   %repairRange = DefaultRepairBeam.beamRange;
   // scale muzNVec to the range the repair beam can reach
   %muzScaled = VectorScale(%muzNVec, %repairRange);
   // muzPoint = the actual point of the gun's "muzzle"
   %muzPoint = %obj.getMuzzlePoint(%slot);
   // rangeEnd = muzzle point + length of beam
   %rangeEnd = VectorAdd(%muzPoint, %muzScaled);
   // search for just about anything that can be damaged as well as interiors
   %searchMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
      $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::InteriorObjectType; 
   // search for objects within the beam's range that fit the masks above
   %scanTarg = ContainerRayCast(%muzPoint, %rangeEnd, %searchMasks, %obj);
   // screen out interiors
   if(%scanTarg && !(%scanTarg.getType() & $TypeMasks::InteriorObjectType))
   {
      // a target in range was found
      %repTgt = firstWord(%scanTarg);
      // is the prospective target damaged?
      if(%repTgt.notRepairable)
      {
         // this is an object that cant be repaired at all
         // -- mission specific flag set on the object
         if(!%obj.errMsgSent)
         {
            messageClient(%obj.client, 'MsgRepairPackIrrepairable', '\c2Target is not repairable.', %repTgt);
            %obj.errMsgSent = true;
         }
         // if player was repairing something, stop the repairs -- we're done
         if(%obj.repairing)
            stopRepairing(%obj);
            
         return;
      }
      else if(%repTgt.getDamageLevel())
      {
         // yes, it's damaged
         if(%repTgt != %obj.repairing)
         {
            if(isObject(%obj.repairing))
               stopRepairing(%obj);

            %obj.repairing = %repTgt;
         }
         // setting imageLoaded to true sends us to repair state (function onRepair)
         %obj.setImageLoaded(%slot, true);
      }
      else if(%repTgt.isEMP || %repTgt.isFlaming || %repTgt.isPoisoned)
      {
         // yes, it's damaged
         if(%repTgt != %obj.repairing)
         {
            if(isObject(%obj.repairing))
               stopRepairing(%obj);

            %obj.repairing = %repTgt;
         }
         // setting imageLoaded to true sends us to repair state (function onRepair)
         %obj.setImageLoaded(%slot, true);
      }
      else
      {
         // there is a target in range, but it's not damaged
         if(!%obj.errMsgSent)
         {
            // if the target isn't damaged, send a message to that effect only once
            messageClient(%obj.client, 'MsgRepairPackNotDamaged', '\c2Target is not damaged.', %repTgt);
            %obj.errMsgSent = true;
         }
         // if player was repairing something, stop the repairs -- we're done
         if(%obj.repairing)
            stopRepairing(%obj);
      }
   }

   //AI hack - too many things influence the aiming, so I'm going to force the repair object for bots only
   else if (%obj.client.isAIControlled() && isObject(%obj.client.repairObject))
   {
      %repTgt = %obj.client.repairObject;
      %repPoint = %repTgt.getAIRepairPoint();
      if (%repPoint $= "0 0 0")
         %repPoint = %repTgt.getWorldBoxCenter();
      %repTgtVector = VectorNormalize(VectorSub(%muzPoint, %repPoint));
      %aimVector = VectorNormalize(VectorSub(%muzPoint, %rangeEnd));

      //if the dot product is very close (ie. we're aiming in the right direction)
      if (VectorDot(%repTgtVector, %aimVector) > 0.85)
      {
         //do an LOS to make sure nothing is in the way...
         %scanTarg = ContainerRayCast(%muzPoint, %repPoint, %searchMasks, %obj);
         if (firstWord(%scanTarg) == %repTgt)
         {
            // yes, it's damaged

            if(isObject(%obj.repairing))
               stopRepairing(%obj);

            %obj.repairing = %repTgt;
            // setting imageLoaded to true sends us to repair state (function onRepair)
            %obj.setImageLoaded(%slot, true);
         }
      }
   }
   else if(%obj.getDamageLevel())
   {
      // there is no target in range, but the player is damaged
      // check to see if we were repairing something before -- if so, stop repairing old target
      if(%obj.repairing != 0)
         if(%obj.repairing != %obj)
            stopRepairing(%obj);
      if(isObject(%obj.repairing))
         stopRepairing(%obj);
      
      %obj.repairing = %obj;
      // quick, to onRepair!
      %obj.setImageLoaded(%slot, true);
   }
   else
   {
      // there is no target in range, and the player isn't damaged
      if(!%obj.errMsgSent)
      {
         // send an error message only once
         messageClient(%obj.client, 'MsgRepairPackNoTarget', '\c2No target to repair.');
         %obj.errMsgSent = true;
      }
      stopRepairing(%obj);
   }
}

function RepairGunImage::onRepair(%this,%obj,%slot)
{
   // this = repairgunimage datablock
   // obj = player wielding the repair gun
   // slot = weapon slot

   if(%obj.getEnergyLevel() <= %this.cutOffEnergy)
   {
      stopRepairing(%obj);
      return;
   }
   // reset the flag that indicates an error message has been sent
   %obj.errMsgSent = false;
   %target = %obj.repairing;
   if(!%target)
   {
      // no target -- whoops! never mind
      stopRepairing(%obj);
   }
   else
   {
      %target.repairedBy = %obj.client;  //keep track of who last repaired this item
      if(%obj.repairing == %obj)
      {
         // player is self-repairing
         if(%obj.getDamageLevel())
         {
            if(!%obj.selfRepairing)
            {
               // no need for a projectile, just send a message and up the repair rate
               messageClient(%obj.client, 'MsgRepairPackPlayerSelfRepair', '\c2Repairing self.');
               %obj.selfRepairing = true;
               startRepairing(%obj, true);
            }
         }
         else
         {
            messageClient(%obj.client, 'MsgRepairPackSelfDone', '\c2Repairs completed on self.');
            stopRepairing(%obj);
            %obj.errMsgSent = true;
         }
      }
      else
      {
         // make sure we still have a target -- more vector fun!!!
         %muzVec      = %obj.getMuzzleVector(%slot);
         %muzNVec     = VectorNormalize(%muzVec);
         %repairRange = DefaultRepairBeam.beamRange;
         %muzScaled   = VectorScale(%muzNVec, %repairRange);
         %muzPoint    = %obj.getMuzzlePoint(%slot);
         %rangeEnd    = VectorAdd(%muzPoint, %muzScaled);

         %searchMasks = $TypeMasks::PlayerObjectType   | $TypeMasks::VehicleObjectType     | 
                        $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType;

         //AI hack to help "fudge" the repairing stuff...
         if (%obj.client.isAIControlled() && isObject(%obj.client.repairObject) && %obj.client.repairObject == %obj.repairing)
         {
            %repTgt = %obj.client.repairObject;
            %repPoint = %repTgt.getAIRepairPoint();
            if (%repPoint $= "0 0 0")
               %repPoint = %repTgt.getWorldBoxCenter();
            %repTgtVector = VectorNormalize(VectorSub(%muzPoint, %repPoint));
            %aimVector = VectorNormalize(VectorSub(%muzPoint, %rangeEnd));
 
            //if the dot product is very close (ie. we're aiming in the right direction)
            if (VectorDot(%repTgtVector, %aimVector) > 0.85)
               %scanTarg = ContainerRayCast(%muzPoint, %repPoint, %searchMasks, %obj);
         }
         else
            %scanTarg = ContainerRayCast(%muzPoint, %rangeEnd, %searchMasks, %obj);

         if (%scanTarg)
         {
            %pos = getWords(%scanTarg, 1, 3);
            %obstructMask = $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType;
            %obstruction = ContainerRayCast(%muzPoint, %pos, %obstructMask, %obj);
            if (%obstruction)
               %scanTarg = "0";
         }

         if(%scanTarg)
         {
            // there's still a target out there
            %repTgt = firstWord(%scanTarg);
            // is the target damaged?
            if(%repTgt.getDamageLevel())
            {
               if(%repTgt != %obj.repairing)
               {
                  // the target is not the same as the one we were just repairing
                  // stop repairing old target, start repairing new target
                  stopRepairing(%obj);
                  if(isObject(%obj.repairing))
                     stopRepairing(%obj);
 
                  %obj.repairing = %repTgt;
                  
                  // extract the name of what player is repairing based on what it is
                  // if it's a player, it's the player's name (duh)
                  // if it's an object, look for a nametag
                  // if object has no nametag, just say what it is (e.g. generatorLarge)
                  if(%repTgt.getClassName() $= Player)
                     %tgtName = getTaggedString(%repTgt.client.name);
                  else if(%repTgt.getGameName() !$= "")
                     %tgtName = %repTgt.getGameName();
                  else
                     %tgtName = %repTgt.getDatablock().getName();
                  messageClient(%obj.client, 'MsgRepairPackRepairingObj', '\c2Repairing %1.', %tgtName, %repTgt);
                  startRepairing(%obj, false);
               }
               else
               {
                  // it's the same target as last time
                  // changed to fix "2 players can't repair same object" bug
                  if(%obj.repairProjectile == 0)
                  {
                     if(%repTgt.getClassName() $= Player)
                        %tgtName = getTaggedString(%repTgt.client.name);
                     else if(%repTgt.getGameName() !$= "")
                        %tgtName = %repTgt.getGameName();
                     else
                        %tgtName = %repTgt.getDatablock().getName();
                     messageClient(%obj.client, 'MsgRepairPackRepairingObj', '\c2Repairing %1.', %tgtName, %repTgt);
                     startRepairing(%obj, false);
                  }
                  
                  %parent = %repTgt.parent;
                  
                  // loop through here to make sure we are hitting the parent
                  while(isObject(%parent))
                  {
                      %parent = %parent.parent;
                  }
                  
                  // Repair all sub-objects
                  if(isObject(%parent.attachment[0]))
                  {
                      %idx = 0;
                      %dmgLevel = %parent.getDamageLevel();
                      
                      while(%parent.attachment[%idx] !$= "")
                      {
                         if(isObject(%parent.attachment[%idx]))
                              %parent.attachment[%idx].setDamageLevel(%dmgLevel);
                              
                         %idx++;
                      }
                  }
               }
            }
            else
            {
               %rateOfRepair = %this.repairFactorObject;
               if(%repTgt.getClassName() $= Player)
               {
                  %tgtName = getTaggedString(%repTgt.client.name);
                  %rateOfRepair = %this.repairFactorPlayer;
               }
               else if(%repTgt.getGameName() !$= "")
                  %tgtName = %repTgt.getGameName();
               else
                  %tgtName = %repTgt.getDatablock().getName();
               if(%repTgt != %obj.repairing)
               {
                  // it isn't the same object we were repairing previously
                  messageClient(%obj.client, 'MsgRepairPackNotDamaged', '\c2%1 is not damaged.', %tgtName);
               }
               else
               {
                  // same target, but not damaged -- we must be done
                  messageClient(%obj.client, 'MsgRepairPackDone', '\c2Repairs completed.');
                  Game.objectRepaired(%repTgt, %tgtName);
               }
               %obj.errMsgSent = true;
               stopRepairing(%obj);
            }
         }
         else
         {
            // whoops, we lost our target
            messageClient(%obj.client, 'MsgRepairPackLostTarget', '\c2Repair target no longer in range.');
            stopRepairing(%obj);
         }
      }
   }
}

function RepairGunImage::onDeactivate(%this,%obj,%slot)
{
   stopRepairing(%obj);
}

function stopRepairing(%player)
{
   // %player = the player who was using the repair pack

   if(%player.selfRepairing)
   {
      // there is no projectile for self-repairing
      %player.setRepairRate(%player.getRepairRate() - %player.repairingRate);
      %player.selfRepairing = false;
   }
   else if(%player.repairing > 0)
   {
      // player was repairing something else
      //if(%player.repairing.beingRepaired > 0)
      //{
         // don't decrement this stuff if it's already at 0 -- though it shouldn't be
         //%player.repairing.beingRepaired--;
         %player.repairing.setRepairRate(%player.repairing.getRepairRate() - %player.repairingRate);
      //}
      if(%player.repairProjectile > 0)
      {
         // is there a repair projectile? delete it
         %player.repairProjectile.delete();
         %player.repairProjectile = 0;
      }
   }
   %player.repairing = 0;
   %player.repairingRate = 0;
   %player.setImageTrigger($WeaponSlot, false);
   %player.setImageLoaded($WeaponSlot, false);
}

function startRepairing(%player, %self)
{
   // %player = the player who was using the repair pack
   // %self = boolean -- is player repairing him/herself?

   if(%self)
   {
      %modifier = %player.inDEDField ? 0.25 : 0;
      // one repair, hold the projectile
      %player.setRepairRate(%player.getRepairRate() + RepairGunImage.repairFactorPlayer );	//  + %modifier); d+d rep bug -soph
      %player.selfRepairing = true;
      %player.repairingRate = RepairGunImage.repairFactorPlayer; 				//  + %modifier; d+d rep bug -soph
      
         %player.isFlaming = 0;
         %player.burnTime = 0;
         %player.isEMP = 0;
         %player.EMPTime = 0;
   }
   else
   {
      //if(%player.repairing.beingRepaired $= "")
      //   %player.repairing.beingRepaired = 1;
      //else
      //   %player.repairing.beingRepaired++;

      //AI hack...
      if (%player.client.isAIControlled() && %player.client.repairObject == %player.repairing)
      {
         %initialPosition  = %player.getMuzzlePoint($WeaponSlot);
         %initialDirection = VectorSub(%initialPosition, %player.repairing.getWorldBoxCenter());
      }
      else
      {
         %initialDirection = %player.getMuzzleVector($WeaponSlot);
         %initialPosition  = %player.getMuzzlePoint($WeaponSlot);
      }
      if(%player.repairing.getClassName() $= Player)
         %repRate = RepairGunImage.repairFactorPlayer;
      else
         %repRate = RepairGunImage.repairFactorObject;
         
         %repRate = %player.repairing.isDEDevice ? %repRate * 0.1 : %repRate;

      %player.repairing.setRepairRate(%player.repairing.getRepairRate() + %repRate);

         %player.repairing.isFlaming = 0;
         %player.repairing.burnTime = 0;
         %player.repairing.isEMP = 0;
         %player.repairing.EMPTime = 0;

      %player.repairingRate = %repRate;
      %player.repairProjectile = new RepairProjectile() {
         dataBlock = DefaultRepairBeam;
         initialDirection = %initialDirection;
         initialPosition  = %initialPosition;
         sourceObject     = %player;
         sourceSlot       = $WeaponSlot;
         targetObject     = %player.repairing;
      };
      MissionCleanup.add(%player.repairProjectile);
   }
}

function RepairPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//--------------------------------------------------------------------------
// Reassembler

datablock ShapeBaseImageData(ReassemblerImage)
{
   shapeFile = "weapon_repair.dts";
   offset = "0 0 0";

   usesEnergy = true;
   minEnergy = 3;
   cutOffEnergy = 3.1;
   emap = true;
   tool = true;

   repairFactorPlayer = 0.015;	// = 0.02; -soph
   repairFactorObject = 0.010;	// = 0.024; -soph

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.25;

   stateName[1] = "ActivateReady";
   stateScript[1] = "onActivateReady";
   stateSpinThread[1] = Stop;
   stateTransitionOnAmmo[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "ActivateReady";

   stateName[2] = "Ready";
   stateSpinThread[2] = Stop;
   stateTransitionOnNoAmmo[2] = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Validate";

   stateName[3] = "Validate";
   stateTransitionOnTimeout[3] = "Validate";
   stateTimeoutValue[3] = 0.2;
   stateEnergyDrain[3] = 3;
   stateSpinThread[3] = SpinUp;
   stateScript[3] = "onValidate";
   stateIgnoreLoadedForReady[3] = true;
   stateTransitionOnLoaded[3] = "Repair";
   stateTransitionOnNoAmmo[3] = "Deactivate";
   stateTransitionOnTriggerUp[3] = "Deactivate";

   stateName[4] = "Repair";
   stateSound[4] = RepairPackFireSound;
   stateScript[4] = "onRepair";
   stateSpinThread[4] = FullSpeed;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "activate";
   stateFire[4] = true;
   stateEnergyDrain[4] = 9;
   stateTimeoutValue[4] = 0.2;
   stateTransitionOnTimeOut[4] = "Repair";
   stateTransitionOnNoAmmo[4] = "Deactivate";
   stateTransitionOnTriggerUp[4] = "Deactivate";
   stateTransitionOnNotLoaded[4] = "Validate";

   stateName[5] = "Deactivate";
   stateScript[5] = "onDeactivate";
   stateSpinThread[5] = SpinDown;
   stateSequence[5] = "activate";
   stateDirection[5] = false;
   stateTimeoutValue[5] = 0.2;
   stateTransitionOnTimeout[5] = "ActivateReady";
};

datablock ItemData(Reassembler)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_repair.dts";
   image = ReassemblerImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a reassembler";
};

function ReassemblerImage::onMount(%this,%obj,%slot)
{
   %obj.play3d(RepairPackActivateSound);
   %obj.repairPackMounted = true;
   %obj.setImageAmmo(%slot,true);
}

function ReassemblerImage::onUnmount(%this,%obj,%slot)
{
   if(%obj.repairing)
      stopRepairing(%obj);

   %obj.repairPackMounted = false;
   %obj.setImageTrigger(%slot, false);
}

function ReassemblerImage::onActivateReady(%this,%obj,%slot)
{
   %obj.errMsgSent = false;
   %obj.selfRepairing = false;
   %obj.repairing = 0;
   %obj.setImageLoaded(%slot, false);
}

function ReassemblerImage::onValidate(%this,%obj,%slot)
{
   if(%obj.getEnergyLevel() <= %this.cutOffEnergy)
   {
      stopRepairing(%obj);
      return;
   }
   %repGun = %obj.getMountedImage(%slot);
   // muzVec is the vector coming from the repair gun's "muzzle"
   %muzVec = %obj.getMuzzleVector(%slot);
   // muzNVec = normalized muzVec
   %muzNVec = VectorNormalize(%muzVec);
   %repairRange = DefaultRepairBeam.beamRange;
   // scale muzNVec to the range the repair beam can reach
   %muzScaled = VectorScale(%muzNVec, %repairRange);
   // muzPoint = the actual point of the gun's "muzzle"
   %muzPoint = %obj.getMuzzlePoint(%slot);
   // rangeEnd = muzzle point + length of beam
   %rangeEnd = VectorAdd(%muzPoint, %muzScaled);
   // search for just about anything that can be damaged as well as interiors
   %searchMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType |
      $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::InteriorObjectType;
   // search for objects within the beam's range that fit the masks above
   %scanTarg = ContainerRayCast(%muzPoint, %rangeEnd, %searchMasks, %obj);
   // screen out interiors
   if(%scanTarg && !(%scanTarg.getType() & $TypeMasks::InteriorObjectType))
   {
      // a target in range was found
      %repTgt = firstWord(%scanTarg);
      // is the prospective target damaged?
      if(%repTgt.getDamageLevel())
      {
         // yes, it's damaged
         if(%repTgt != %obj.repairing)
         {
            if(isObject(%obj.repairing))
               stopRepairing(%obj);

            %obj.repairing = %repTgt;
         }
         // setting imageLoaded to true sends us to repair state (function onRepair)
         %obj.setImageLoaded(%slot, true);
      }
      else if(%repTgt.isEMP || %repTgt.isFlaming || %repTgt.isPoisoned)
      {
         // yes, it's damaged
         if(%repTgt != %obj.repairing)
         {
            if(isObject(%obj.repairing))
               stopRepairing(%obj);

            %obj.repairing = %repTgt;
         }
         // setting imageLoaded to true sends us to repair state (function onRepair)
         %obj.setImageLoaded(%slot, true);
      }
      else
      {
         // there is a target in range, but it's not damaged
         if(!%obj.errMsgSent)
         {
            // if the target isn't damaged, send a message to that effect only once
            messageClient(%obj.client, 'MsgRepairPackNotDamaged', '\c2Target is not damaged.', %repTgt);
            %obj.errMsgSent = true;
         }
         // if player was repairing something, stop the repairs -- we're done
         if(%obj.repairing)
            stopRepairing(%obj);
      }
   }

   //AI hack - too many things influence the aiming, so I'm going to force the repair object for bots only
   else if (%obj.client.isAIControlled() && isObject(%obj.client.repairObject))
   {
      %repTgt = %obj.client.repairObject;
      %repPoint = %repTgt.getAIRepairPoint();
      if (%repPoint $= "0 0 0")
         %repPoint = %repTgt.getWorldBoxCenter();
      %repTgtVector = VectorNormalize(VectorSub(%muzPoint, %repPoint));
      %aimVector = VectorNormalize(VectorSub(%muzPoint, %rangeEnd));

      //if the dot product is very close (ie. we're aiming in the right direction)
      if (VectorDot(%repTgtVector, %aimVector) > 0.85)
      {
         //do an LOS to make sure nothing is in the way...
         %scanTarg = ContainerRayCast(%muzPoint, %repPoint, %searchMasks, %obj);
         if (firstWord(%scanTarg) == %repTgt)
         {
            // yes, it's damaged

            if(isObject(%obj.repairing))
               stopRepairing(%obj);

            %obj.repairing = %repTgt;
            // setting imageLoaded to true sends us to repair state (function onRepair)
            %obj.setImageLoaded(%slot, true);
         }
      }
   }
   else if(%obj.getDamageLevel())
   {
      // there is no target in range, but the player is damaged
      // check to see if we were repairing something before -- if so, stop repairing old target
      if(%obj.repairing != 0)
         if(%obj.repairing != %obj)
            stopRepairing(%obj);
      if(isObject(%obj.repairing))
         stopRepairing(%obj);

      %obj.repairing = %obj;
      // quick, to onRepair!
      %obj.setImageLoaded(%slot, true);
   }
   else
   {
      // there is no target in range, and the player isn't damaged
      if(!%obj.errMsgSent)
      {
         // send an error message only once
         messageClient(%obj.client, 'MsgRepairPackNoTarget', '\c2No target to repair.');
         %obj.errMsgSent = true;
      }
      stopRepairing(%obj);
   }
}

function ReassemblerImage::onRepair(%this,%obj,%slot)
{
   // this = repairgunimage datablock
   // obj = player wielding the repair gun
   // slot = weapon slot

   if(%obj.getEnergyLevel() <= %this.cutOffEnergy)
   {
      stopRepairing(%obj);
      return;
   }
   // reset the flag that indicates an error message has been sent
   %obj.errMsgSent = false;
   %target = %obj.repairing;
   if(!%target)
   {
      // no target -- whoops! never mind
      stopRepairing(%obj);
   }
   else
   {
      %target.repairedBy = %obj.client;  //keep track of who last repaired this item
      if(%obj.repairing == %obj)
      {
         // player is self-repairing
         if(%obj.getDamageLevel())
         {
            if(!%obj.selfRepairing)
            {
               // no need for a projectile, just send a message and up the repair rate
               messageClient(%obj.client, 'MsgRepairPackPlayerSelfRepair', '\c2Repairing self.');
               %obj.selfRepairing = true;
               rStartRepairing(%obj, true);
            }
         }
         else
         {
            messageClient(%obj.client, 'MsgRepairPackSelfDone', '\c2Repairs completed on self.');
            stopRepairing(%obj);
            %obj.errMsgSent = true;
         }
      }
      else
      {
         // make sure we still have a target -- more vector fun!!!
         %muzVec      = %obj.getMuzzleVector(%slot);
         %muzNVec     = VectorNormalize(%muzVec);
         %repairRange = DefaultRepairBeam.beamRange;
         %muzScaled   = VectorScale(%muzNVec, %repairRange);
         %muzPoint    = %obj.getMuzzlePoint(%slot);
         %rangeEnd    = VectorAdd(%muzPoint, %muzScaled);

         %searchMasks = $TypeMasks::PlayerObjectType   | $TypeMasks::VehicleObjectType     |
                        $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType;

         //AI hack to help "fudge" the repairing stuff...
         if (%obj.client.isAIControlled() && isObject(%obj.client.repairObject) && %obj.client.repairObject == %obj.repairing)
         {
            %repTgt = %obj.client.repairObject;
            %repPoint = %repTgt.getAIRepairPoint();
            if (%repPoint $= "0 0 0")
               %repPoint = %repTgt.getWorldBoxCenter();
            %repTgtVector = VectorNormalize(VectorSub(%muzPoint, %repPoint));
            %aimVector = VectorNormalize(VectorSub(%muzPoint, %rangeEnd));

            //if the dot product is very close (ie. we're aiming in the right direction)
            if (VectorDot(%repTgtVector, %aimVector) > 0.85)
               %scanTarg = ContainerRayCast(%muzPoint, %repPoint, %searchMasks, %obj);
         }
         else
            %scanTarg = ContainerRayCast(%muzPoint, %rangeEnd, %searchMasks, %obj);

         if (%scanTarg)
         {
            %pos = getWords(%scanTarg, 1, 3);
            %obstructMask = $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType;
            %obstruction = ContainerRayCast(%muzPoint, %pos, %obstructMask, %obj);
            if (%obstruction)
               %scanTarg = "0";
         }

         if(%scanTarg)
         {
            // there's still a target out there
            %repTgt = firstWord(%scanTarg);
            // is the target damaged?

            if(%repTgt.notRepairable)
            {
               // this is an object that cant be repaired at all
               // -- mission specific flag set on the object
               if(!%obj.errMsgSent)
               {
                  messageClient(%obj.client, 'MsgRepairPackIrrepairable', '\c2Target is not repairable.', %repTgt);
                  %obj.errMsgSent = true;
               }
               // if player was repairing something, stop the repairs -- we're done
               if(%obj.repairing)
                  stopRepairing(%obj);
            }

            if(%repTgt.getDamageLevel())
            {
               if(%repTgt != %obj.repairing)
               {
                  // the target is not the same as the one we were just repairing
                  // stop repairing old target, start repairing new target
                  stopRepairing(%obj);
                  if(isObject(%obj.repairing))
                     stopRepairing(%obj);

                  %obj.repairing = %repTgt;
                  // extract the name of what player is repairing based on what it is
                  // if it's a player, it's the player's name (duh)
                  // if it's an object, look for a nametag
                  // if object has no nametag, just say what it is (e.g. generatorLarge)
                  if(%repTgt.getClassName() $= Player)
                     %tgtName = getTaggedString(%repTgt.client.name);
                  else if(%repTgt.getGameName() !$= "")
                     %tgtName = %repTgt.getGameName();
                  else
                     %tgtName = %repTgt.getDatablock().getName();
                  messageClient(%obj.client, 'MsgRepairPackRepairingObj', '\c2Repairing %1.', %tgtName, %repTgt);
                  rStartRepairing(%obj, false);
               }
               else
               {
                  // it's the same target as last time
                  // changed to fix "2 players can't repair same object" bug
                  if(%obj.repairProjectile == 0)
                  {
                     if(%repTgt.getClassName() $= Player)
                        %tgtName = getTaggedString(%repTgt.client.name);
                     else if(%repTgt.getGameName() !$= "")
                        %tgtName = %repTgt.getGameName();
                     else
                        %tgtName = %repTgt.getDatablock().getName();
                     messageClient(%obj.client, 'MsgRepairPackRepairingObj', '\c2Repairing %1.', %tgtName, %repTgt);
                     rStartRepairing(%obj, false);
                  }
                  
                  %parent = %repTgt.parent;
                  
                  // loop through here to make sure we are hitting the parent
                  while(isObject(%parent))
                  {
                      %parent = %parent.parent;
                  }
                  
                  // Repair all sub-objects
                  if(isObject(%parent.attachment[0]))
                  {
                      %idx = 0;
                      %dmgLevel = %parent.getDamageLevel();
                      
                      while(%parent.attachment[%idx] !$= "")
                      {
                         if(isObject(%parent.attachment[%idx]))
                              %parent.attachment[%idx].setDamageLevel(%dmgLevel);
                              
                         %idx++;
                      }
                  }
               }
            }
            else
            {
               %rateOfRepair = %this.repairFactorObject;
               if(%repTgt.getClassName() $= Player)
               {
                  %tgtName = getTaggedString(%repTgt.client.name);
                  %rateOfRepair = %this.repairFactorPlayer;
               }
               else if(%repTgt.getGameName() !$= "")
                  %tgtName = %repTgt.getGameName();
               else
                  %tgtName = %repTgt.getDatablock().getName();
               if(%repTgt != %obj.repairing)
               {
                  // it isn't the same object we were repairing previously
                  messageClient(%obj.client, 'MsgRepairPackNotDamaged', '\c2%1 is not damaged.', %tgtName);
               }
               else
               {
                  // same target, but not damaged -- we must be done
                  messageClient(%obj.client, 'MsgRepairPackDone', '\c2Repairs completed.');
                  Game.objectRepaired(%repTgt, %tgtName);
               }
               %obj.errMsgSent = true;
               stopRepairing(%obj);
            }
         }
         else
         {
            // whoops, we lost our target
            messageClient(%obj.client, 'MsgRepairPackLostTarget', '\c2Repair target no longer in range.');
            stopRepairing(%obj);
         }
      }
   }
}

function ReassemblerImage::onDeactivate(%this,%obj,%slot)
{
   stopRepairing(%obj);
}

function rStartRepairing(%player, %self)
{
   // %player = the player who was using the repair pack
   // %self = boolean -- is player repairing him/herself?

   if(%self)
   {
      %modifier = %player.inDEDField ? 0.1 : 0.05;	// %modifier = %player.inDEDField ? 1.0 : 0.02; d+d rep bug -soph
      // one repair, hold the projectile
      %player.setRepairRate(%player.getRepairRate() + (ReassemblerImage.repairFactorPlayer * %modifier));
      %player.selfRepairing = true;
      %player.repairingRate = ReassemblerImage.repairFactorPlayer * %modifier;
      
      %player.isFlaming = 0;
      %player.burnTime = 0;
      %player.isEMP = 0;
      %player.EMPTime = 0;
   }
   else
   {
      //if(%player.repairing.beingRepaired $= "")
      //   %player.repairing.beingRepaired = 1;
      //else
      //   %player.repairing.beingRepaired++;

      //AI hack...
      if (%player.client.isAIControlled() && %player.client.repairObject == %player.repairing)
      {
         %initialPosition  = %player.getMuzzlePoint($WeaponSlot);
         %initialDirection = VectorSub(%initialPosition, %player.repairing.getWorldBoxCenter());
      }
      else
      {
         %initialDirection = %player.getMuzzleVector($WeaponSlot);
         %initialPosition  = %player.getMuzzlePoint($WeaponSlot);
      }
      if(%player.repairing.getClassName() $= Player)
         %repRate = ReassemblerImage.repairFactorPlayer;
      else
      {
         %modifier = %player.repairing.isDEDevice ? 0.1 : 1;
         %repRate = ReassemblerImage.repairFactorObject * %modifier;
      }
      %player.repairing.setRepairRate(%player.repairing.getRepairRate() + %repRate);

         %player.repairing.isFlaming = 0;
         %player.repairing.burnTime = 0;
         %player.repairing.isEMP = 0;
         %player.repairing.EMPTime = 0;

           %player.repairingRate = %repRate;
      %player.repairProjectile = new RepairProjectile() {
         dataBlock = ReassemblerRepairBeam;
         initialDirection = %initialDirection;
         initialPosition  = %initialPosition;
         sourceObject     = %player;
         sourceSlot       = $WeaponSlot;
         targetObject     = %player.repairing;
      };
      // ----------------------------------------------------
      // z0dd - ZOD, 5/27/02. Fix lingering projectile bug
      if(isObject(%player.lastProjectile))
         %player.lastProjectile.delete();

      %player.lastProjectile = %player.repairProjectile;
      // End z0dd - ZOD
      // ----------------------------------------------------
      MissionCleanup.add(%player.repairProjectile);
   }
}

//--------------------------------------------------------------------------
// Nosferatu

//datablock ShapeBaseImageData(NosferatuImage)	// replaced -[soph]
//{
//   shapeFile = "weapon_repair.dts";
//   offset = "0 0 0";
//
//   usesEnergy = true;
//   minEnergy = 3;
//   cutOffEnergy = 3.1;
//   emap = true;
//   tool = true;
//
//   drainFactorPlayer = 0.1;
//   drainFactorObject = 0.15;
//
//   stateName[0] = "Activate";
//   stateTransitionOnTimeout[0] = "ActivateReady";
//   stateTimeoutValue[0] = 0.25;
//
//   stateName[1] = "ActivateReady";
//   stateScript[1] = "onActivateReady";
//   stateSpinThread[1] = Stop;
//   stateTransitionOnAmmo[1] = "Ready";
//   stateTransitionOnNoAmmo[1] = "ActivateReady";
//
//   stateName[2] = "Ready";
//   stateSpinThread[2] = Stop;
//   stateTransitionOnNoAmmo[2] = "Deactivate";
//   stateTransitionOnTriggerDown[2] = "Validate";
//
//   stateName[3] = "Validate";
//   stateTransitionOnTimeout[3] = "Validate";
//   stateTimeoutValue[3] = 0.2;
//   stateEnergyDrain[3] = 4;
//   stateSpinThread[3] = SpinUp;
//   stateScript[3] = "onValidate";
//   stateIgnoreLoadedForReady[3] = true;
//   stateTransitionOnLoaded[3] = "Repair";
//   stateTransitionOnNoAmmo[3] = "Deactivate";
//   stateTransitionOnTriggerUp[3] = "Deactivate";
//
//   stateName[4] = "Repair";
//   stateSound[4] = RepairPackFireSound;
//   stateScript[4] = "onRepair";
//   stateSpinThread[4] = FullSpeed;
//   stateAllowImageChange[4] = false;
//   stateSequence[4] = "activate";
//   stateFire[4] = true;
//   stateEnergyDrain[4] = 18;
//   stateTimeoutValue[4] = 0.2;
//   stateTransitionOnTimeOut[4] = "Repair";
//   stateTransitionOnNoAmmo[4] = "Deactivate";
//   stateTransitionOnTriggerUp[4] = "Deactivate";
//   stateTransitionOnNotLoaded[4] = "Validate";
//
//   stateName[5] = "Deactivate";
//   stateScript[5] = "onDeactivate";
//   stateSpinThread[5] = SpinDown;
//   stateSequence[5] = "activate";
//   stateDirection[5] = false;
//   stateTimeoutValue[5] = 0.2;
//   stateTransitionOnTimeout[5] = "ActivateReady";
//};

//datablock ItemData(Nosferatu)
//{
//   className = Weapon;
//   catagory = "Spawn Items";
//   shapeFile = "weapon_repair.dts";
//   image = NosferatuImage;
//   mass = 1;
//   elasticity = 0.2;
//   friction = 0.6;
//   pickupRadius = 2;
//	pickUpName = "a Nosferatu";
//};						// -[/soph]

function NosferatuImage::onMount(%this,%obj,%slot)
{
   %obj.play3d(RepairPackActivateSound);
   %obj.setImageAmmo(%slot, true);
}

function NosferatuImage::onUnmount(%this,%obj,%slot)
{
   if(%obj.nosf)
      stopNosfing(%obj);

   %obj.setImageTrigger(%slot, false);
}

function NosferatuImage::onActivateReady(%this,%obj,%slot)
{
   %obj.errMsgSent = false;
   %obj.nosf = 0;
   %obj.setImageLoaded(%slot, false);
}

function NosferatuImage::onValidate(%this,%obj,%slot)
{															// +[/soph]
   if(%obj.getEnergyLevel() <= %this.cutOffEnergy)
   {
      stopNosfing(%obj);
      return;
   }

   %repGun = %obj.getMountedImage(%slot);
   // muzVec is the vector coming from the repair gun's "muzzle"
   %muzVec = %obj.getMuzzleVector(%slot);
   // muzNVec = normalized muzVec
   %muzNVec = VectorNormalize(%muzVec);
   %repairRange = NosferatuRepairBeam.beamRange;
   // scale muzNVec to the range the repair beam can reach
   %muzScaled = VectorScale(%muzNVec, %repairRange);
   // muzPoint = the actual point of the gun's "muzzle"
   %muzPoint = %obj.getMuzzlePoint(%slot);
   // rangeEnd = muzzle point + length of beam
   %rangeEnd = VectorAdd(%muzPoint, %muzScaled);
   // search for just about anything that can be damaged as well as interiors
   %searchMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
   // search for objects within the beam's range that fit the masks above
   %scanTarg = ContainerRayCast(%muzPoint, %rangeEnd, %searchMasks, %obj);

   // screen out interiors
   if(%scanTarg && !(%scanTarg.getType() & $TypeMasks::InteriorObjectType))
   {
      // a target in range was found
      %repTgt = firstWord(%scanTarg);

      if(%repTgt.team != %obj.team)
      {
         // not same team, grab it
         if(%repTgt != %obj.nosf)
         {
            if(isObject(%obj.nosf))
               stopNosfing(%obj);

            %obj.nosf = %repTgt;
         }
         // setting imageLoaded to true sends us to repair state (function onRepair)
         %obj.setImageLoaded(%slot, true);
      }
      else
      {
         // there is a target in range, same team
         if(!%obj.errMsgSent)
         {
            // if the target isn't damaged, send a message to that effect only once
            messageClient(%obj.client, 'MsgNosfSameTeamDrain', '\c2Target is not enemy.', %repTgt);
            %obj.errMsgSent = true;
         }
         // if player was repairing something, stop the repairs -- we're done
         if(%obj.nosf)
            stopNosfing(%obj);
      }
   }
   else
   {
      // there is no target in range, and the player isn't damaged
      if(!%obj.errMsgSent)
      {
         // send an error message only once
         messageClient(%obj.client, 'MsgNosfNoTarget', '\c2No target to drain.');
         %obj.errMsgSent = true;
      }
      stopNosfing(%obj);
   }
}

function NosferatuImage::onRepair(%this,%obj,%slot)
{
   // this = repairgunimage datablock
   // obj = player wielding the repair gun
   // slot = weapon slot

   if(%obj.getEnergyLevel() <= %this.cutOffEnergy)
   {
      stopNosfing(%obj);
      return;
   }
   // reset the flag that indicates an error message has been sent
   %obj.errMsgSent = false;
   %target = %obj.nosf;

   if(!%target)
   {
      // no target -- whoops! never mind
      stopNosfing(%obj);
   }
   else
   {
         // make sure we still have a target -- more vector fun!!!
         %muzVec      = %obj.getMuzzleVector(%slot);
         %muzNVec     = VectorNormalize(%muzVec);
         %repairRange = DefaultRepairBeam.beamRange;
         %muzScaled   = VectorScale(%muzNVec, %repairRange);
         %muzPoint    = %obj.getMuzzlePoint(%slot);
         %rangeEnd    = VectorAdd(%muzPoint, %muzScaled);
         %searchMasks = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
         %scanTarg = ContainerRayCast(%muzPoint, %rangeEnd, %searchMasks, %obj);

         if (%scanTarg)
         {
            %pos = getWords(%scanTarg, 1, 3);
            %obstructMask = $TypeMasks::InteriorObjectType | $TypeMasks::TerrainObjectType;
            %obstruction = ContainerRayCast(%muzPoint, %pos, %obstructMask, %obj);

            if (%obstruction)
               %scanTarg = "0";
         }

         if(%scanTarg)
         {
            // there's still a target out there
            %repTgt = firstWord(%scanTarg);
            // is the target damaged?

            if(%repTgt.team == %obj.team)
            {
               // this is an object that cant be repaired at all
               // -- mission specific flag set on the object
               if(!%obj.errMsgSent)
               {
                  messageClient(%obj.client, 'MsgNosfSameTeam', '\c2Target is on the same team.', %repTgt);
                  %obj.errMsgSent = true;
               }
               // if player was repairing something, stop the repairs -- we're done
               if(%obj.nosf)
                  stopNosfing(%obj);
            }

//            if(%repTgt.getDamageLevel())
//            {
               if(%repTgt != %obj.nosf)
               {
                  // the target is not the same as the one we were just repairing
                  // stop repairing old target, start repairing new target
                  stopNosfing(%obj);

                  if(isObject(%obj.nosf))
                     stopNosfing(%obj);

                  %obj.nosf = %repTgt;
                  // extract the name of what player is repairing based on what it is
                  // if it's a player, it's the player's name (duh)
                  // if it's an object, look for a nametag
                  // if object has no nametag, just say what it is (e.g. generatorLarge)
                  if(%repTgt.getClassName() $= Player)
                     %tgtName = getTaggedString(%repTgt.client.name);
                  else if(%repTgt.getGameName() !$= "")
                     %tgtName = %repTgt.getGameName();
                  else
                     %tgtName = %repTgt.getDatablock().getName();

                  messageClient(%obj.client, 'MsgNosfDrainObj', '\c2Nosferatu active on %1.', %tgtName, %repTgt);
                  rstartNosfing(%obj, false);
               }
               else
               {
                  // it's the same target as last time
                  // changed to fix "2 players can't repair same object" bug
                  if(%obj.repairProjectile == 0)
                  {
                     if(%repTgt.getClassName() $= Player)
                        %tgtName = getTaggedString(%repTgt.client.name);
                     else if(%repTgt.getGameName() !$= "")
                        %tgtName = %repTgt.getGameName();
                     else
                        %tgtName = %repTgt.getDatablock().getName();

                     messageClient(%obj.client, 'MsgNosfDrainObj', '\c2Nosferatu active on %1.', %tgtName, %repTgt);
                     rstartNosfing(%obj, false);
                  }

                  // Do our damage here:
                  if(isObject(%repTgt) && !%repTgt.dead)
                  {
                       %repTgt.damage(%obj, %repTgt.getWorldBoxCenter(), %this.drainFactorPlayer, $DamageType::Nosferatu);

                       %damage = %obj.getDamageLevel();

                       if(%damage)
                            %obj.setDamageLevel(%damage - (%this.drainFactorPlayer / 2));
                  }
                  else
                    stopNosfing(%obj);
               }
//            }
//            else
//               stopNosfing(%obj);
         }
         else
         {
            // whoops, we lost our target
            messageClient(%obj.client, 'MsgNosfLostTarget', '\c2Nosferatu target no longer in range.');
            stopNosfing(%obj);
         }
      }
   }
//}

function NosferatuImage::onDeactivate(%this,%obj,%slot)
{
   stopNosfing(%obj);
}

function rstartNosfing(%player, %self)
{
   // %player = the player who was using the repair pack
   // %self = boolean -- is player repairing him/herself?

      %initialDirection = %player.getMuzzleVector($WeaponSlot);
      %initialPosition  = %player.getMuzzlePoint($WeaponSlot);

//      %player.nosf.setRepairRate(%player.nosf.getRepairRate());

      %player.repairProjectile = new RepairProjectile()
      {
         dataBlock = NosferatuRepairBeam;
         initialDirection = %initialDirection;
         initialPosition  = %initialPosition;
         sourceObject     = %player;
         sourceSlot       = $WeaponSlot;
         targetObject     = %player.nosf;
      };

      if(isObject(%player.lastProjectile))
         %player.lastProjectile.delete();

      %player.lastProjectile = %player.repairProjectile;

      MissionCleanup.add(%player.repairProjectile);
}

function stopNosfing(%player)
{
   // %player = the player who was using the repair pack

   if(%player.nosf > 0)
   {
      // player was repairing something else
      //if(%player.repairing.beingRepaired > 0)
      //{
         // don't decrement this stuff if it's already at 0 -- though it shouldn't be
         //%player.repairing.beingRepaired--;
//         %player.repairing.setRepairRate(%player.repairing.getRepairRate() - %player.repairingRate);
      //}
      if(%player.repairProjectile > 0)
      {
         // is there a repair projectile? delete it
         %player.repairProjectile.delete();
         %player.repairProjectile = 0;
      }
   }

   %player.nosf = 0;
   %player.setImageTrigger($WeaponSlot, false);
   %player.setImageLoaded($WeaponSlot, false);
}

//--------------------------------------------------------------------------
// Mend Pulse

datablock ShockwaveData( MendPulseShockwave )	// +[soph]
{
   width = 0 ;
   numSegments = 32 ;
   numVertSegments = 24 ;
   velocity = 200 ;
   acceleration = -800 ;
   lifetimeMS = 600 ;
   height = 30.0 ;
   verticalCurve = 5 ;
   is2D = false ;

   texture[0] = "special/shockLightning03" ;
   texture[1] = "special/gradient" ;
   texWrap = 5.0 ;

   times[0] = 0.0 ;
   times[1] = 0.5 ;
   times[2] = 1.0 ;

   colors[0] = "1.0 0.15 0.35 0.5" ;
   colors[1] = "1.0 0.15 0.35 0.8" ;
   colors[2] = "1.0 0.15 0.35 0.3" ;

   mapToTerrain = false ;
   orientToNormal = false ;
   renderBottom = true ;
};

datablock ExplosionData( MendPulseExplosion )
{
   soundProfile   = RepairPackActivateSound ;

   shockwave = MendPulseShockwave ;

   emitter[0] = EnergyRifleExplosionEmitter ;
   emitter[1] = EnergySparksEmitter ;

   shakeCamera = true ;
   camShakeFreq = "0.25 0.5 0.5" ;
   camShakeAmp = "1.0 1.0 1.0" ;
   camShakeDuration = 0.5 ;
   camShakeRadius = 40.0 ;
};

datablock LinearFlareProjectileData( MendPulseProjectile )
{
   projectileShapeName = "turret_muzzlePoint.dts" ;
   faceViewer          = false ;
   directDamage        = 0.0 ;
   hasDamageRadius     = true ;
   indirectDamage      = 0.0 ;
   damageRadius        = 0.0 ;
   radiusDamageType    = $DamageType::ShieldPulse ; 
   kickBackStrength    = 4000 ;

   explosion           = MendPulseExplosion ;
   underwaterExplosion = MendPulseExplosion ;

   splash              = MissileSplash ;
   velInheritFactor    = 0 ;

   dryVelocity       = 150 ;
   wetVelocity       = 150 ;
   fizzleTimeMS      = 35 ;
   lifetimeMS        = 35 ;
   explodeOnDeath    = true ;
   reflectOnWaterImpactAngle = 90 ;
   explodeOnWaterImpact      = false ;
   deflectionOnWaterImpact   = 0.0 ;
   fizzleUnderwaterMS        = 35 ;

   activateDelayMS = -1 ;

   numFlares         = 0 ;
   flareColor        = "0 0 0" ;
   flareModTexture   = "flaremod" ;
   flareBaseTexture  = "flarebase" ;
};						// +[/soph]
