if(isObject(Meltdown))
   Meltdown.DSOFileCount++;
else
   quit();

// MANTA-HOPIC Ion Tagger

datablock ShapeBaseImageData(IonTAGPackImage)
{
   shapeFile = "pack_upgrade_energy.dts";
   item = IonTAGPack;
   mountPoint = 1;
//   offset = "-0.175 -0.2 0.2";
//   rotation = "0 1 0 180";
   emap = true;

   gun = IonTAGGunImage;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";
stateSequence[0] = "activation";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
stateSequence[1] = "activation";
   stateSound[1] = TargetingIonTAGSwitchSound;
   stateTransitionOnTriggerUp[1] = "Deactivate";

   stateName[2] = "Deactivate";
   stateScript[2] = "onDeactivate";
   stateTransitionOnTimeout[2] = "Idle";
};

datablock ItemData(IonTAGPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "repair_patch.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "IonTAGPackImage";
   pickUpName = "a MANTA TAG Pack";

   emap = true;
};

function IonTAGPackImage::onUnmount(%data, %obj, %node)
{
   // dismount the IonTAGGun gun if the player had it mounted
   // need the extra "if" statement to avoid a console error message
   if(%obj.getMountedImage($WeaponSlot))
      if(%obj.getMountedImage($WeaponSlot).getName() $= "IonTAGGunImage")
         %obj.unmountImage($WeaponSlot);
}

function IonTAGPackImage::onActivate(%data, %obj, %slot)
{
   // don't activate the pack if player is piloting a vehicle
   if(%obj.isPilot())
   {
      %obj.setImageTrigger(%slot, false);
      return;
   }

   if(!isObject(%obj.getMountedImage($WeaponSlot)) || %obj.getMountedImage($WeaponSlot).getName() !$= "IonTAGGunImage")
   {
      // unmount any weapon the player may have been holding
      if(%obj.getMountedImage($WeaponSlot))
         %obj.unmountImage($WeaponSlot);

      // make sure player's arm thread is "look"
      %obj.setArmThread(look);

      // mount the IonTAGGun gun
      %obj.mountImage(IonTAGGunImage, $WeaponSlot);
//      commandToClient(%obj.client, 'setRepairReticle');
      %obj.client.setWeaponsHudActive("Chaingun");
   }
}

function IonTAGPackImage::onDeactivate(%data, %obj, %slot)
{
   //called when the player hits the "pack" key again (toggle)
   %obj.setImageTrigger(%slot, false);
   if(%obj.getMountedImage($WeaponSlot).getName() $= "IonTAGGunImage")
      %obj.unmountImage($WeaponSlot);
}

//--------------------------------------------------------------------------
// Ion Gun

datablock ItemData(IonTAGGun)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_targeting.dts";
   image = IonTAGGunImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   pickUpName = "a MANTA Ion Cannon Tagger";
   emap = true;
};

datablock ShapeBaseImageData(IonTAGGunImage)
{
   className = WeaponImage;

   shapeFile = "weapon_targeting.dts";
   item = IonTAGGun;
   offset = "0 0 0";

   projectile = IonTAGBeam;
   projectileType = TargetProjectile;
   deleteLastProjectile = true;

   usesEnergy = true;
   minEnergy = -1;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateEnergyDrain[3]              = 0;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
   stateSound[3]                    = TargetingLaserPaintSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

$IonPainterCalibrateTime = 8000;
$RMSPainterCalibrateTime = 12000 ;	// +soph

function IonRMSErrorReset(%obj)
{
   if(isObject(%obj))
      %obj.IonRMSError = "";
}

function IonRMSReset(%obj, %text, %time)
{
   if(isObject(%obj))
   {
      %obj.IonRMSError = %text SPC %time;
      schedule(%time, 0, IonRMSErrorReset, %obj);
   }
}

$TypeMasks::MantaLOSType = $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::StaticObjectType;
$TypeMasks::CheckArea =  $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType;

function errorResetMR(%obj, %text, %time)
{
   if(%obj.IonRMSError)
   {
      MRDisplayError(%obj);
      return;
   }

   if(%text !$= "")
      commandToClient(%obj.client, 'BottomPrint', %text, %time, 1);

   %obj.setImageTrigger(0, false);
   IonRMSReset(%obj, %text, %time*1000);
}

function MRDisplayError(%obj)
{
   %text = getWord(%obj.IonRMSError, 0);
   %time = getWord(%obj.IonRMSError, 1);

   if(%text !$= "")
      commandToClient(%obj.client, 'BottomPrint', %text, %time, 1);

   %obj.setImageTrigger(0, false);
}

function findGround(%pos, %radius)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::CheckArea);

   if((%obj = ContainerSearchNext()))
      return %obj;
   else
      return false;
}
$MantaReloadTime = 2 * 60000;

function IonTAGGunImage::onFire(%data,%obj,%slot)
{
   if(%obj.wep_reloading[%data])
      return;

   %mode = %obj.getMountedImage(0).item;

//   %id = 0;					// -soph
   %id = %obj.client.mode[%mode] ;		// +soph
   %proj = "";

//   if($MantaSat[%obj.client.team].reloading)	// -[soph]
//   {
//       errorResetMR(%obj, "MANTA Ion Cannon is cooling down...", 4);
//       return;
//   }						// -[/soph]

//   if(%obj.client.mode[%mode] == 0)
   if( !%id )
   {
      %weapon = $MantaSat[%obj.client.team];
//      %id = 1;
      %proj = "IonTAGBeam";
   }
   else 					// +[soph]
   {
      %weapon = $RMSSilo[ %obj.client.team ] ;
      %proj = "RMSTAGBeam" ;
   }

   if( isObject( %weapon ) )
   {
      if( %weapon.reloading )
      { 
         %message = %id ? "RMS Silo is fuelling." : "MANTA Ion Cannon is cooling down..." ;
         errorResetMR( %obj , %message , 4 ) ;
         return ;
      }
      else if( !%weapon.isLoaded )
      { 
         %message = %id ? "RMS Silo warheads are not yet ready." : "The MANTA is not loaded with a battery." ;
         errorResetMR( %obj , %message , 4 ) ;
         return ;
      }
   }
   else
   {
      %message = %id ? "There is no RMS silo deployed." : "There is no MANTA Satellite in orbit." ;
      errorResetMR( %obj , %message , 4 ) ;
      return ;
   }						// +[/soph]

   if(%obj.IonRMSError)
   {
      MRDisplayError(%obj);
      return;
   }

   if(%obj.ionTagBeacon)
   {
      %obj.ionTagBeacon.delete();
      %obj.ionTagBeacon = "";
   }

   %p = new TargetProjectile()
   {
      dataBlock        = %proj;
      initialDirection = %obj.getMuzzleVector(%slot);
      initialPosition  = %obj.getMuzzlePoint(%slot);
      sourceObject     = %obj;
      sourceSlot       = %slot;
      vehicleObject    = 0;
   };
   MissionCleanup.add(%p);
   %obj.mrtarg = %p;
   %obj.lastProjectile = %p;

//   %p = Parent::onFire(%data, %obj, %slot);
   %p.setTarget(%obj.client.team);

   if(isObject(%weapon))
   {
      if(!%weapon.isLoaded)
      {
         errorResetMR(%obj, %text, 3);
         return;
      }

      %pt = getWords(%obj.lastProjectile.getTargetPoint(), 0, 2);
        
      IonTAGFireLoop(%obj);
      if(%pt $= "0 0 0")
         %pt = getLOSOffset(%obj, %obj.getMuzzleVector(0), 300, %obj.getMuzzlePoint(0));

   //  %pt2 = vectorAdd(vectorScale(vectorNormalize(vectorSub($MantaSat[%obj.client.team], %pt)), 1), %pt);
  //    %result = containerRayCast($MantaSat[%obj.client.team].getPosition(), %pt2, $TypeMasks::MantaLOSType, $MantaSat[%obj.client.team]);
   //   %target = getWord(%result, 0);

   //   if(!%target)
   //   {
      %obj.ionTAGLoop = true;
     // %obj.lastPointFirePos = %pt; 											// -soph
      %obj.lastPointFirePos = vectorAdd( %obj.getMuzzlePoint( 0 ) , vectorScale( %obj.getMuzzleVector( 0 ) , 300 ) ) ;	// +[soph]
      %hit = ContainerRayCast( %obj.getMuzzlePoint( 0 ) , %obj.lastPointFirePos , $TypeMasks::InteriorObjectType |
                                                                                  $TypeMasks::TerrainObjectType |
                                                                                  $TypeMasks::ForceFieldObjectType |
                                                                                  $TypeMasks::StaticShapeObjectType |
                                                                                  $TypeMasks::VehicleObjectType |
                                                                                  $TypeMasks::PlayerObjectType , %obj ) ;
      if( %hit )
         %obj.lastPointFirePos = getWord( %hit , 1 ) SPC getWord( %hit , 2 ) SPC getWord( %hit , 3 ) ;			// +[/soph]

      %obj.lastPointVector = %obj.getMuzzleVector(0);

      IonTAGFireLoop( %obj , %id , %p ) ;										// (%obj); -soph
//      }
 //    else
  //    {
 //        %obj.setImageTrigger(0, false);
        // %obj.IonTagInvalidTimeout = true;

        // schedule(1500, 0, IonTagInvalidTimeoutReset, %obj);
 //     }
   }
   else
   {
//      %error = %id == 1 ? "There is no MANTA Satellite in orbit." : "There is no RMS Silo deployed.";
      %error = !%id ? "There is no MANTA Satellite in orbit." : "There is no RMS Silo deployed.";
      errorResetMR(%obj, %error, 4); //commandToClient(%obj.client, 'BottomPrint', ".", 4, 1);
   }
}

function IonTAGFireLoop( %obj , %id , %proj )										// (%obj) -soph
{
   %weapon = %obj.getMountedImage(0).item;

//   if(%obj.client.mode[%weapon] == 0)											// -soph

   %obj.client.mode[%weapon] = %id ;											// +[soph]
   %pointVec = %obj.getMuzzleVector(0);											// _here_
   %pointEnd = vectorAdd( %obj.getMuzzlePoint( 0 ) , vectorScale( %pointVec , 300 ) ) ;
   %hit = ContainerRayCast( %obj.getMuzzlePoint( 0 ) , %pointEnd , $TypeMasks::InteriorObjectType |
                                                                   $TypeMasks::TerrainObjectType |
                                                                   $TypeMasks::ForceFieldObjectType |
                                                                   $TypeMasks::StaticShapeObjectType |
                                                                   $TypeMasks::VehicleObjectType |
                                                                   $TypeMasks::PlayerObjectType , %obj ) ;
   if( %hit )
      %pointEnd = getWord( %hit , 1 ) SPC getWord( %hit , 2 ) SPC getWord( %hit , 3 ) ;

   if( !%id )														// +[/soph]
   {
      %weapon = $MantaSat[%obj.client.team];
//      %id = 1;													// -soph
      %lockduration = $IonPainterCalibrateTime;
   }
   else															// +[soph]
   {
      %weapon = $RMSSilo[ %obj.client.team ] ;
      %lockduration = $RMSPainterCalibrateTime ;
      %distance = getDistance3D( %pointEnd , %weapon.getPosition() ) ; 
      if( %distance < $RMSMinimumRange )
         %lockDuration = ( ( %lockDuration / 4 ) * %distance / $RMSMinimumRange ) + 1000 ;
   }															// +[/soph]

   if(isObject(%obj) && isObject(%weapon))
   {
      if(%obj.IonTAGLoop)
      {
         if(%obj.IonRMSError)
         {
            MRDisplayError(%obj);
            return;
         }

         %pos = %obj.lastPointFirePos;
//         %pointEnd = %obj.lastProjectile.getTargetPoint();								// -soph
//         %pointVec = %obj.getMuzzleVector(0);										// ^up^ -soph

//         if(!%weapon.lockDuration)
//            %weapon.lockDuration = 13000;

//         echo(%weapon.lockDuration);

         if( getDistance3D( %pointEnd , %pos ) < 0.25 )									// if(vectorCompare(%obj.lastPointVector, %pointVec)) -soph
         {
            %obj.IonPainterTargetCount++;

//            if(%obj.IonPainterTargetCount >= 10)									// -[soph]
//            {														// -
//               %obj.IonPainterTargetCalibrate++;									// -
//               %obj.IonPainterTargetCount = 0;									// -
//            }														// -
//															// -
//            if(%obj.IonPainterTargetCalibrate >= (%lockDuration / 1000))						// -[/soph]
            if( %obj.IonPainterTargetCount >= %lockDuration / 100 )
            {
               %obj.IonPainterFireReady = true;
               %obj.IonPainterTargetCalibrate = 0;
            }
         }
         else
         {
            %obj.IonPainterTargetCount = 0;
            %obj.IonPainterTargetCalibrate = 0;
            %obj.IonPainterFireReady = false;

            %text = "Painter lock lost, target sequence reset." ;							// %text = %id == 1 ? "The MANTA is not loaded with a battery; resetting..." : "The RMS is not loaded with missiles; resetting..."; -soph
            errorResetMR(%obj, %text, 3);
            return;
         }

         if(%obj.IonPainterFireReady)
         {
            commandToClient(%obj.client, 'BottomPrint', "Coordinates of impact: " SPC getWords(%pos, 0, 2), 4, 1);
            IonTAGGunImage::deconstruct( IonTAGGunImage , %obj , 0 ) ;							// +soph
            %obj.setImageTrigger($BackpackSlot, false);
         }
         else if(!%obj.IonTagFireTimeout)
         {										// +soph
            %xFactor = mFloor( getWord(%pos, 0) + getRandomT( %lockduration / 40 / %obj.IonPainterTargetCount ) ) ;	// getRandomT(500); -soph	// getRandomT((%obj.IonPainterTargetCalibrate - ($IonPainterCalibrateTime / 1000)));
            %yFactor = mFloor( getWord(%pos, 1) + getRandomT( %lockduration / 40 / %obj.IonPainterTargetCount ) ) ;	// getRandomT(500); -soph	// getRandomT((%obj.IonPainterTargetCalibrate - ($IonPainterCalibrateTime / 1000)));
            %zFactor = mFloor( getWord(%pos, 2) + getRandomT( %lockduration / 40 / %obj.IonPainterTargetCount ) ) ;	// getRandomT(500); -soph	// getRandomT((%obj.IonPainterTargetCalibrate - ($IonPainterCalibrateTime / 1000)));
            %calibratePos = %xFactor SPC %yFactor SPC %zFactor;

//            commandToClient(%obj.client, 'BottomPrint', "Calibrating fire coordinates:" SPC %calibratePos, 1.5, 1);	// -soph
            %ms = %lockDuration - ( %obj.IonPainterTargetCount * 100 ) ;						// +[soph]
            %ds = mCeil( %ms / 100 ) ;
            %seconds = mFloor( %ds / 10 ) ;
            %decimal = %ds - ( %seconds * 10 ) ;
            commandToClient( %obj.client , 'BottomPrint' , "0" @ %seconds @ "." @ %decimal @ "0 seconds to lock\nTarget coordinates:" SPC %calibratePos , 1.5 , 2 ) ;
         }														// +[/soph]

         %obj.lastPointFirePos = %pointEnd ;										// %obj.lastPointFirePos = %pos; -soph
         %obj.lastPointVector = %pointVec;

         schedule(100, 0, IonTAGFireLoop, %obj , %id ) ;								// (100, 0, IonTAGFireLoop, %obj); -soph
      }
   }
}

function IonTAGGunImage::deconstruct(%data, %obj, %slot)	// -soph
{
   %obj.IonTAGLoop = false;

   if(%obj.ionTagBeacon)
   {
      %obj.ionTagBeacon.delete();
      %obj.ionTagBeacon = "";
   }

//   if(%obj.IonRMSError)
//   {
//      MRDisplayError(%obj);
//      return;
//   }

   %weapon = %obj.getMountedImage(0).item;

   if(%obj.client.mode[%weapon] == 0)
   {
      %weapon = $MantaSat[%obj.client.team];
      %id = 1;
   }
   else if(%obj.client.mode[%weapon] == 1)
   {
      %weapon = $RMSSilo[%obj.client.team];
      %id = 2;
   }

   if(isObject(%weapon)) // is there a satellite?
   {
         if(%obj.IonPainterFireReady) // are we ready to fire?
         {
            %obj.IonPainterFireReady = false;
            %pos = %obj.lastPointFirePos;

            if(%id == 1)
            {
               %obj.ionTagBeacon = new BeaconObject()
               {
                  dataBlock = "TAGBeacon";
                  beaconType = "vehicle";
                  position = %pos;
               };

               %obj.ionTagBeacon.playThread($AmbientThread, "ambient");
               %obj.ionTagBeacon.team = %obj.client.team;
               %obj.ionTagBeacon.sourceObject = %obj;
               %obj.ionTagBeacon.setTarget(%obj.client.team);
               MissionCleanup.add(%obj.ionTagBeacon);

               %beacon = %obj.ionTagBeacon;
               %obj.ionTagBeacon = "";
              // %beacon.schedule(5000, delete);						// moving down -soph

//               echo("manta satellite exists");
               %pos2 = vectorAdd($MantaSat[%obj.client.team].getPosition(), "0 0 -10");
               %pointDir = getVectorFromPoints(%pos2, %pos);
//               %hpos = getVectorOffset(%pos2, %pointDir, -20); //vectorAdd(%pos, "0 0 -20");
               %beacon.schedule( ( getDistance3D( %pos , %pos2 ) / 500 ) - 50 , delete);	// get the beacon out of the way +soph

               %FFRObject = new StaticShape()
               {
                  dataBlock        = mReflector;
               };
               MissionCleanup.add(%FFRObject);
               %FFRObject.setPosition(%pos2);
               %FFRObject.schedule(32, delete);

               if(%weapon.loadType == 1)
                  MANTAFireIonBlast(%obj, %FFRObject, %pointdir, %pos2);
               else if(%weapon.loadType == 2)
                  MANTAFireIonNuke(%obj, %FFRObject, %pointdir, %pos2);
                  
               cooldownMR(%obj.client.team, 2, 1);
               %weapon.isLoaded = false;							// +soph
            }
            else if(%id == 2)
            {
               %weapon.targetPos = %pos;
//               sequenceRMSS(%weapon, %obj);							// -soph
//               cooldownMR(%obj.client.team, 30, 0);						// -soph
               RMSSQuadFire( %weapon , %obj , %pos );						// +soph
            }

//            %weapon.ammo -= %ammocost;
            %obj.IonPainterFireReady = false;
            %obj.IonPainterTargetCount = 0;
            %obj.IonPainterTargetCalibrate = 0;

//            %weapon.isLoaded = false;								// -soph
            %weapon.loadType = 0;
            %weapon.lockduration = 0;
         }
//         else if(%obj.IonTagFireTimeout)
//            commandToClient(%obj.client, 'BottomPrint', "Coordinates not similar to last, resetting.", 4, 1);
//         else if(%obj.IonTagInvalidTimeout)
//            commandToClient(%obj.client, 'BottomPrint', "Target coordinates are blocked by or in an object/building/part of terrain.", 4, 1);
//         else if(%obj.IonTagNoAmmo)
//            commandToClient(%obj.client, 'BottomPrint', "MANTA is not loaded with a battery; resetting.", 4, 1);
//         else
//            commandToClient(%obj.client, 'BottomPrint', %id SPC "coordinates not calibrated.", 4, 1);
//      else
//         commandToClient(%obj.client, 'BottomPrint', %id SPC "is recharging, please wait "@(%chargeTime / 1000) - %weapon.MASCharge@" seconds.", 4, 1);
   }

   %obj.IonPainterTargetCount = 0;								// +[soph]
   %obj.IonPainterTargetCalibrate = 0;
   %obj.IonPainterFireReady = false;								// +[/soph]

   if(isObject(%obj.mrtarg))
      %obj.mrtarg.delete();

   Parent::deconstruct(%data, %obj, %slot);
}

datablock StaticShapeData(TAGBeacon)
{
   shapeFile = "turret_muzzlepoint.dts";
   targetNameTag = 'beacon';
   isInvincible = true;

   dynamicType = $TypeMasks::SensorObjectType;
};

datablock StaticShapeData(SuperWeaponBeacon)
{
   shapeFile = "turret_muzzlepoint.dts";
   targetNameTag = 'beacon';
   isInvincible = true;

   dynamicType = $TypeMasks::SensorObjectType;
};

function MANTAFireIonBlast(%obj, %FFRObject, %pointdir, %pos2)
{
   %p = new EnergyProjectile()
   {
      dataBlock        = IONBeam;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %h = new LinearFlareProjectile()
   {
      dataBlock        = IONBeamHead;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%h);

   schedule(750, 0, "projectileTrail", %h, 50, LinearFlareProjectile, IonCannonTrailCharge);
}

function MANTAFireIonNuke(%obj, %FFRObject, %pointdir, %pos2)
{
   %p = new EnergyProjectile()
   {
      dataBlock        = IONBeam;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
   };
   MissionCleanup.add(%p);

   %h = new LinearFlareProjectile()
   {
      dataBlock        = IONBeamHead;
      initialDirection = %pointDir; //"0 0 -1";
      initialPosition  = %pos2;
      sourceObject     = %FFRObject;
      sourceSlot       = 0;
      vehicleObject    = 0;
      bkSourceObject   = %obj;
      ignoreReflections = true;
      ionNuke = true;
   };
   MissionCleanup.add(%h);
}

function IonTAGGunImage::onMount(%this,%obj,%slot)
{
   Parent::onMount(%this,%obj,%slot);
//   commandToClient( %obj.client, 'BottomPrint', "<font:times new roman:18>Now using a MANTA Ion Targeter\nThis targets positions for the MANTA Ion Cannon to fire at.", 4, 2);
   commandToClient( %obj.client, 'BottomPrint', "<font:times new roman:18>Now using a MANTA/RMS Targeter\nThis tags targets for the MANTA Ion Cannon and Remote Missile Silo.", 4, 2);
}

function IonTAGGunImage::onUnmount(%this,%obj,%slot)
{
   %obj.setImageTrigger(%slot, false);
   %obj.setImageTrigger($BackpackSlot, false);
   setLoadingFlag(%this, %obj, 1000);
}

function IonTAGPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}
